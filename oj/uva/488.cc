#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main()
{
    string line;
    getline(cin, line);
    int numCases;
    istringstream is(line);
    is >> numCases;
    getline(cin, line); // blank line

    for (int l=0; l<numCases; ++l) {
        getline(cin, line);
        istringstream is(line);
        int amp;
        is >> amp;
        getline(cin, line);
        istringstream is2(line);
        int freq;
        is2 >> freq;
        for (int i=0; i<freq; ++i) {
            for (int j=1; j<=amp; ++j) {
                for (int k=0; k<j; ++k) {
                    cout << j;
                }
                cout << endl;
            }
            for (int j=amp-1; j>=1; --j) {
                for (int k=0; k<j; ++k) {
                    cout << j;
                }
                cout << endl;
            }
            if (i < (freq-1))
                cout << endl;
        }
        getline(cin, line);
        if (l < (numCases-1))
            cout << endl;
    }

    return 0;
}
