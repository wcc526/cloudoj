Const
    Limit      = 100;

Var
    A , B ,
    C          : array[0..Limit] of extended;
    answer     : extended;
    i ,
    N , M , CC : longint;

Begin
    readln(N , M , CC);
    while (N + M <> 0) do
      begin
          C[0] := 1;
          for i := 1 to CC do C[i] := C[i - 1] * (CC - i + 1) / i;
          A[0] := 1;
          for i := 1 to CC do A[i] := A[i - 1] * N / (N + M);
          B[0] := 1;
          for i := 1 to CC do B[i] := B[i - 1] * M / (N + M);
          answer := 0;
          for i := 0 to CC do
            if not odd(i) then
              answer := answer + C[i] * A[i] * B[CC - i];
          writeln(answer : 0 : 7);
          readln(N , M , CC);
      end;
End.