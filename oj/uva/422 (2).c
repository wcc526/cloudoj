#include <stdio.h>

int main()
{
  int s, u, v, w, n;
  char m[100][101], c[101];

  scanf("%d", &n);
  for (u = 0; u < n; ++u)
    scanf("%s", m[u]);

  scanf("%*[^A-Z0-9]");
  while (scanf("%100[A-Z]%n", c, &s) == 1) {
    scanf("%*[^A-Z0-9]");
    for (u = 0; u < n; ++u) {
      for (v = 0; v < n; ++v) {
        if (m[v][u] == c[0]) {
          if (u + s <= n) {
            for (w = 1; w < s; ++w)
              if (m[v][u + w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v + 1, u + w);
              goto d;
            }
          }
          if (v + s <= n) {
            for (w = 1; w < s; ++w)
              if (m[v + w][u] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v + w, u + 1);
              goto d;
            }
          }
          if (u - s + 1 >= 0) {
            for (w = 1; w < s; ++w)
              if (m[v][u - w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v + 1, u - w + 2);
              goto d;
            }
          }
          if (v - s + 1 >= 0) {
            for (w = 1; w < s; ++w)
              if (m[v - w][u] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v - w + 2, u + 1);
              goto d;
            }
          }
          if (u + s <= n && v + s <= n) {
            for (w = 1; w < s; ++w)
              if (m[v + w][u + w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v + w, u + w);
              goto d;
            }
          }
          if (u + s <= n && v - s + 1 >= 0) {
            for (w = 1; w < s; ++w)
              if (m[v - w][u + w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v - w + 2, u + w);
              goto d;
            }
          }
          if (u - s + 1 >= 0 && v + s <= n) {
            for (w = 1; w < s; ++w)
              if (m[v + w][u - w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v + w, u - w + 2);
              goto d;
            }
          }
          if (u - s + 1 >= 0 && v - s + 1 >= 0) {
            for (w = 1; w < s; ++w)
              if (m[v - w][u - w] ^ c[w])
                break;
            if (w == s) {
              printf("%d,%d %d,%d\n", v + 1, u + 1, v - w + 2, u - w + 2);
              goto d;
            }
          }
        }
      }
    }
    puts("Not found");
    continue;
  d:;
  }

  return 0;
}
