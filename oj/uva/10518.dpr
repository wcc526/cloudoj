{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10518.in';
    OutFile    = 'p10518.out';

Type
    Tmatrix    = array[1..3 , 1..3] of longint;

Var
    n , backup : extended;
    b , answer ,
    nowCase    : longint;

function init : boolean;
begin
    inc(nowCase);
    read(n , b);
    backup := n;
    init := (b <> 0);
end;

procedure times(ma , mb : Tmatrix; var mc : Tmatrix);
var
    i , j , k  : longint;
begin
    fillchar(mc , sizeof(mc) , 0);
    for k := 1 to 3 do
      for i := 1 to 3 do
        for j := 1 to 3 do
          begin
              inc(mc[i , j] , ma[i , k] * mb[k , j]);
              mc[i , j] := (mc[i , j] mod b + b) mod b;
          end;
end;

procedure work;
var
    a , power  : Tmatrix;
begin
    fillchar(a , sizeof(a) , 0); fillchar(power , sizeof(power) , 0);
    a[1 , 1] := -1; a[1 , 2] := 1; a[1 , 3] := 1;
    power[1 , 2] := 1; power[2 , 1] := 1; power[2 , 2] := 1; power[3 , 2] := 1; power[3 , 3] := 1;
    while n <> 0 do
      begin
          if int(n / 2) * 2 <> n
            then times(a , power , a);
          times(power , power , power);
          n := int(n / 2);
      end;
    answer := a[1 , 2];
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , backup : 0 : 0 , ' ' , b , ' ' , answer); 
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      nowCase := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
