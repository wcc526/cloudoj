{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}

Var
    totCase ,
    nowCase , 
    x1 , y1 ,
    x2 , y2 ,
    answer ,
    t1 , t2    : longint;
    
Begin
    readln(totCase);
    for nowCase := 1 to totCase do
      begin
          readln(x1 , y1 , x2 , y2);
          t1 := x1 + y1; t2 := x2 + y2;
          if t1 <> t2
            then begin
                     answer := (t1 + t2 + 2) * (t2 - t1 - 1) div 2;
                     inc(answer , y1 + x2);
                     inc(answer);
                 end
            else answer := abs(x1 - x2);
          writeln('Case ' , nowCase , ': ' , answer);
      end;
End.
