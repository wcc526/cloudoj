Const
    InFile     = 'p10746.in';
    OutFile    = 'p10746.out';
    Limit      = 20;

Type
    Tmap       = array[1..Limit * 2 + 2 , 1..Limit * 2 + 2] of extended;
    TC         = array[1..Limit * 2 + 1 , 1..Limit * 2 + 2] of longint;

Var
    map        : Tmap;
    C          : TC;
    answer     : extended;
    M ,
    N , S , T  : longint;

function init : boolean;
type
    Tmincost   = array[1..Limit * 2 + 2] of extended;
var
    i , j      : longint;
    mincost    : Tmincost;
begin
    readln(N , M);
    if N + M = 0 then exit(false);
    fillchar(map , sizeof(map) , 0); fillchar(C , sizeof(C) , 0);
    init := true;
    for i := 1 to M do mincost[i] := 1e10;
    for i := 1 to N do
      for j := 1 to M do
        begin
            C[i + 1 , j + M + 1] := 1;
            read(map[i + 1 , j + M + 1]);
            if map[i + 1 , j + M + 1] > mincost[j] then
              mincost[j] := map[i + 1 , j + M + 1];
        end;
    for i := N + 1 to M do
      for j := 1 to M do
        begin
            C[i + 1 , j] := 1;
            map[i + 1 , j + M + 1] := mincost[j];
        end;
    for i := 2 to M + 1 do
      for j := M + 2 to M * 2 + 1 do
        map[j , i] := -map[i , j];
    S := 1; T := M * 2 + 2;
    for i := 2 to N + 1 do C[S , i] := 1;
    for i := M + 2 to M * 2 + 1 do C[i , T] := 1;
end;

procedure extend;
type
    Tshortest  = array[1..Limit * 2 + 2] of
                   record
                       shortest              : extended;
                       father                : longint;
                   end;
var
    shortest   : Tshortest;
    i , j      : longint;
    changed    : boolean;
    tmp        : extended;
begin
    for i := 1 to T do
      begin
          shortest[i].shortest := 1e10;
          shortest[i].father := 0;
      end;
    shortest[1].shortest := 0;
    repeat
      changed := false;
      for i := 1 to T do
        if shortest[i].shortest < 1e10 then
          for j := 1 to T do
            if C[i , j] > 0 then
              begin
                  tmp := shortest[i].shortest + map[i , j];
                  if shortest[j].shortest > tmp then
                    begin
                        shortest[j].shortest := shortest[i].shortest + map[i , j];
                        shortest[j].father := i;
                        changed := true;
                    end;
              end;
    until not changed;
    i := T;
    answer := answer + shortest[i].shortest;
    while i <> S do
      begin
          j := shortest[i].father;
          dec(C[j , i]); inc(C[i , j]);
          i := j;
      end;
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := 1 to N do
      extend;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          writeln(answer / N + 1e-10 : 0 : 2);
      end;
End.