Const
    InFile     = 'p10737.in';
    OutFile    = 'p10737.out';
    Limit      = 100;

Type
    Tmatrix    = array[1..Limit , 1..Limit + 1] of longint;
    Tdata      = array[1..Limit] of longint;

Var
    matrix     : Tmatrix;
    answer ,
    data       : Tdata;
    N , P , M  : longint;
    noanswer   : boolean;

procedure read_num(var num : longint);
var
    ch         : char;
begin
    read(ch); while ch = ' ' do read(ch);
    if ch = '?'
      then num := -1
      else begin
               num := 0;
               while ch <> ' ' do
                 begin
                     num := num * 10 + ord(ch) - ord('0');
                     if eoln then ch := ' ' else read(ch);
                 end;
           end;
end;

function init : boolean;
var
    i          : longint;
begin
    fillchar(matrix , sizeof(matrix) , 0);
    fillchar(data , sizeof(data) , 0);
    readln(P , N); M := 0;
    if N > p then N := p;
    if P = 0 then exit(false);
    for i := 1 to P do begin read_num(data[i]); if data[i] = -1 then inc(M); end;
    readln; exit(true);
end;

procedure add(var a : longint; b : longint);
begin
    inc(a , b);
    a := a mod p;
    if a < 0 then inc(a , p);
end;

procedure Get_Equation;
var
    i , j , k ,
    count , tot: longint;
    sum        : Tdata;
begin
    count := 0;
    for i := 1 to P do
      begin
          fillchar(sum , sizeof(sum) , 0); sum[i] := 1;
          if data[i] = -1 then inc(count);
          for j := 1 to N do
            begin
                tot := 0; for k := j to P do inc(tot , sum[k]);
                if data[i] = -1
                  then add(matrix[j , count] , tot)
                  else add(matrix[j , M + 1] , -tot * data[i]);
                for k := P - 1 downto j do add(sum[k] , sum[k + 1]);
                sum[j] := 0;
            end;
      end;
end;

procedure swap(var a , b : longint);
var
    tmp        : longint;
begin
    tmp := a; a := b; b := tmp;
end;

function find(start : longint) : boolean;
var
    i , j , k  : longint;
begin
    for i := start to N do
      for j := start to M do
        if matrix[i , j] <> 0 then
          begin
              for k := 1 to M + 1 do
                swap(matrix[start , k] , matrix[i , k]);
              for k := 1 to N do
                swap(matrix[k , start] , matrix[k , j]);
              exit(true);
          end;
    exit(false);
end;

procedure euclid_extend(a , b , c : longint; var x , y : longint);
var
    divnum , t : longint;
begin
    if a = 0
      then begin x := 0; y := c div b; end
      else begin
               divnum := b div a;
               euclid_extend(b mod a , a , c , y , t);
               x := t - divnum * y;
           end;
end;

function divide(a , b : longint) : longint;
var
    x , y      : longint;
begin
    euclid_extend(b , p , a , x , y);
    x := x mod p; if x < 0 then inc(x , p);
    exit(x);
end;

procedure work;
var
    i , j , k ,
    t , sum    : longint;
begin
    Get_Equation;
    i := 1;
    while i <= N do
      begin
          if not find(i) then break;
          for j := i + 1 to N do
            if matrix[j , i] <> 0 then
              begin
                  k := divide(matrix[j , i] , matrix[i , i]);
                  for t := i to M + 1 do
                    add(matrix[j , t] , -matrix[i , t] * k);
              end;
          inc(i);
      end;
    noanswer := true;
    for j := i to N do if matrix[j , M + 1] <> 0 then exit;
    fillchar(answer , sizeof(answer) , 0);
    noanswer := false;
    for j := i - 1 downto 1 do
      begin
          sum := matrix[j , M + 1];
          for k := j + 1 to M do
            add(sum , -answer[k] * matrix[j , k]);
          answer[j] := divide(sum , matrix[j , j]);
      end;
end;

procedure out;
var
    i , count  : longint;
begin
    if noanswer
      then writeln('Invalid message!')
      else begin
               count := 0;
               for i := 1 to P do
                 begin
                     if data[i] = -1
                       then begin inc(count); write(answer[count]); end
                       else write(data[i]);
                     if i <> P then write(' ');
                 end;
               writeln;
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          out;
      end;
End.
