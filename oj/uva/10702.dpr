{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10702.in';
    OutFile    = 'p10702.out';
    Limit      = 100;
    LimitTimes = 1000;
    maximum    = 1e20;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Topt       = array[0..1 , 1..Limit] of extended;
    Tdata      = array[1..Limit] of longint;

Var
    map        : Tmap;
    opt        : Topt;
    data       : Tdata;
    answer     : extended;
    now , 
    N , S , T ,
    M          : longint;

function init : boolean;
var
    i , j      : longint;
begin
    readln(N , S , T , M);
    if N + S + T + M = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 for j := 1 to N do
                   read(map[i , j]);
               for i := 1 to T do
                 read(data[i]);
           end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    now := 0;
    for i := 1 to N do opt[now , i] := -maximum;
    opt[now , S] := 0; 
    for i := 1 to M do
      begin
          now := 1 - now;
          for j := 1 to N do opt[now , j] := -maximum;
          for j := 1 to N do
            if opt[1 - now , j] >= 0 then
              for k := 1 to N do
                if (j <> k) and (opt[1 - now , j] + map[j , k] > opt[now , k]) then
                  opt[now , k] := opt[1 - now , j] + map[j , k];
      end;
    answer := -maximum;
    for i := 1 to T do
      if opt[now , data[i]] > answer then
        answer := opt[now , data[i]];
end;

procedure out;
begin
    writeln(answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
