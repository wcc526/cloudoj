Const
    InFile     = 'p10443.in';
    Limit      = 100;

Type
    Tdata      = array[1..2 , 0..Limit + 1 , 0..Limit + 1] of char;
    Tfather    = array[#0..#255] of char;

Var
    data       : Tdata;
    father     : Tfather;
    now , T ,
    N , M , D  : longint;

procedure init;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , #0); dec(T);
    readln(N , M , D);
    for i := 1 to N do
      begin
          for j := 1 to M do
            read(data[1 , i , j]);
          readln;
      end;
end;

procedure work;
var
    i , x , y ,
    dx , dy    : longint;
begin
    father['R'] := 'P'; father['P'] := 'S'; father['S'] := 'R';
    now := 1;
    for i := 1 to D do
      begin
          now := 3 - now;
          for x := 1 to N do
            for y := 1 to M do
              begin
                  data[now , x , y] := data[3 - now , x , y];
                  for dx := -1 to 1 do
                    for dy := -1 to 1 do
                      if abs(dx) + abs(dy) = 1 then
                        if data[3 - now , x + dx , y + dy] = father[data[3 - now , x , y]] then
                          data[now , x , y] := father[data[3 - now , x , y]];
              end;
      end;
end;

procedure out;
var
    i , j      : longint;
begin
    for i := 1 to N do
      begin
          for j := 1 to M do
            write(data[now , i , j]);
          writeln;
      end;
    if T <> 0 then writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.