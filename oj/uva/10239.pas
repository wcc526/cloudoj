Const
    InFile     = 'p10239.in';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of comp;
    Topt       = array[0..Limit] of comp;

Var
    Height ,
    Width      : Tdata;
    opt        : Topt;
    answer     : double;
    N          : longint;
    W          : double;

function init : boolean;
var
    i          : longint;
    H1 , W1    : double;
begin
    readln(N , W);
    W := round(W * 1e4);
    if N = 0
      then exit(false)
      else begin
               for i := 1 to N do
                 begin
                     read(H1 , W1);
                     Height[i] := round(H1 * 1e4);
                     Width[i] := round(W1 * 1e4);
                 end;
               exit(true);
           end;
end;

procedure work;
var
    i , j      : longint;
    dH , dW    : comp;
begin
    opt[0] := 0;
    for i := 1 to N do
      begin
          j := i; dH := 0; dW := 0; opt[i] := 1e15;
          repeat
            dW := dW + Width[j]; if Height[j] > dH then dH := Height[j];
            if opt[i] > opt[j - 1] + dH then
              opt[i] := opt[j - 1] + dH;
            dec(j);
          until (j = 0) or (dW + Width[j] > W);
      end;
    answer := opt[N];
    answer := answer / 1e4;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            writeln(answer : 0 : 4);
        end;
//    Close(INPUT);
End.