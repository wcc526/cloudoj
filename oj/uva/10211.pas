Var
    N , i ,
    B , D      : longint;
    find       : boolean;

procedure RightMost;
var
    tB , tD ,
    c1 , c2 ,
    max , tmp ,
    i          : longint;
begin
    i := 2; tB := B; tD := D; max := 0;
    while (i <= tB) and (i <= tD) do
      begin
          c1 := 0; c2 := 0;
          while (tB mod i = 0) do begin tB := tB div i; inc(c1); end;
          while (tD mod i = 0) do begin tD := tD div i; inc(c2); end;
          if c2 <> 0 then
            if c1 = 0
              then exit
              else begin
                       tmp := (c2 - 1) div c1 + 1;
                       if tmp > max then max := tmp;
                   end;
          inc(i);
      end;
    if tD <> 1 then exit;
    find := true;
    writeln('Rightmost ' , max);
end;

procedure AddALL;
var
    i , num    : longint;
begin
    num := 1;
    for i := 1 to 1000 do
      begin
          num := (num * B) mod D;
          if num = 1 then
            begin
                find := true;
                writeln('Add all ' , i);
                exit;
            end;
      end;
end;

procedure Alternate;
var
    i , num    : longint;
begin
    num := 1;
    for i := 1 to 1000 do
      begin
          num := (num * B) mod D;
          if num = D - 1 then
            begin
                find := true;
                writeln('Alternate ' , i , ' change sign');
                exit;
            end;
      end;
end;

Begin
    readln(N);
    for i := 1 to N do
      begin
          readln(B , D);
          find := false;
          RightMost;
          AddALL;
          Alternate;
          if not find then
            writeln('condition not found.');
          if i <> N then writeln;
      end;
End.