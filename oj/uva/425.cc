#include <cstdio>
#include <cstring>
#include <set>
#include <string>
#include <unistd.h>

using namespace std;

int main()
{
  set<string> words;
  char ciphertext[100], salt[3], temp[100];
  int i, j;

  scanf("%s", ciphertext);
  salt[0] = ciphertext[0], salt[1] = ciphertext[1], salt[2] = 0;

  scanf("%*[^A-Za-z]");
  while (scanf("%[A-Za-z]", temp) == 1) {
    scanf("%*[^A-Za-z]");
    j = strlen(temp);
    if (j > 1 && j < 6) {
      for (i = j; i--;)
        temp[i] = temp[i] | 0x20;
      words.insert(string(temp));
    }
  }

  for (set<string>::iterator u = words.begin(); u != words.end(); ++u) {
    i = u->length();
    strcpy(temp, u->c_str());
    for (set<string>::iterator v = words.begin(); v != words.end(); ++v) {
      if (i + v->length() == 7) {
        strcpy(temp + i + 1, v->c_str());
        for (j = 0; j < 10; j += 2) {
          temp[i] = '0' + j;
          if (strcmp(ciphertext, crypt(temp, salt)) == 0) {
            puts(temp);
            return 0;
          }
        }
      }
    }
  }

  return 0;
}
