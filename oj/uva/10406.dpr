{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10406.in';
    OutFile    = 'p10406.out';
    Limit      = 1000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    N          : longint;
    d , answer : extended;

function init : boolean;
var
    i          : longint;
begin
    read(d , N);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do read(data[i].x , data[i].y);
               readln;
           end;
end;

function dist(p1 , p2 : Tpoint): extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function cross(p1 , p2 , p3 : Tpoint) : extended;
begin
    cross := (p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y);
end;

function angle(p1 , p2 , p3 : Tpoint): extended;
var
    cosC , sinC ,
    c , a , b  : extended;
begin
    c := dist(p1 , p3); a := dist(p1 , p2); b := dist(p2 , p3);
    cosC := (a * a + b * b - c * c) / (2 * a * b);
    if abs(cosC) <= minimum
      then angle := pi / 2
      else begin
               sinC := sqrt(abs(1 - cosC * cosC));
               if cosC > 0
                 then angle := arctan(sinC / cosC)
                 else angle := pi - arctan(-sinC / cosC);
           end;
end;

procedure work;
var
    i          : longint;
    A , B , L  : extended;
    p1 , p2 ,
    p3 , p4    : Tpoint;
begin
    answer := 0;
    for i := 2 to N - 1 do
      answer := answer + abs(cross(data[1] , data[i] , data[i + 1])) / 2;
    for i := 1 to N do
      begin
          p1 := data[(i + N - 2) mod N + 1];
          p2 := data[i];
          p3 := data[i mod N + 1];
          p4 := data[(i + 1) mod N + 1];
          L := dist(p2 , p3);
          A := angle(p1 , p2 , p3) / 2;
          B := angle(p2 , p3 , p4) / 2;
          answer := answer - (L + L - d / sin(A) * cos(A) - d / sin(B) * cos(B)) * d / 2;
      end;
end;

procedure out;
begin
    writeln(answer : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
