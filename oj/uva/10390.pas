Const
    InFile     = 'p10390.in';
    Limit      = 26;

Type
    Tcount     = array['a'..'z'] of longint;
    Tkey       = record
                     name     : char;
                     count    : longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    count      : Tcount;
    cases ,
    totnum ,
    totshare   : longint;

procedure main;
var
    j ,
    i , num    : longint;
    t          : Tkey;
    ch , tmp ,
    c2         : char;
begin
    dec(Cases);
    totnum := 0; totshare := 0;
    fillchar(count , sizeof(count) , 0);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to Limit do
      data[i].name := chr(i + ord('A') - 1);

    readln;
    while not eoln do
      begin
          read(ch);
          inc(count[ch]);
      end;
    readln;
    while not eoln do
      begin
          read(ch , tmp);
          while not eoln do
            begin
                read(c2 , tmp);
                read(tmp); num := 0;
                while (tmp in ['0'..'9']) do
                  begin
                      num := num * 10 + ord(tmp) - ord('0');
                      if eoln then tmp := ' ' else read(tmp);
                  end;
                i := 0;
                if count[c2] = num then i := 2;
                if abs(count[c2] - num) = 1 then i := 1;
                inc(totshare , i); inc(totnum); inc(data[ord(ch) - ord('A') + 1].count , i);
            end;
          readln;
      end;
    for i := 1 to Limit do
      if data[i].count > 0 then
        writeln(data[i].name , ' ' , data[i].count * 2.00 * totnum / totshare : 0 : 2);
    if cases > 0 then writeln;
end;


Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        main;
//    Close(INPUT);
End.