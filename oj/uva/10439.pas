Type
    Tpoint     = record
                     x , y    : extended;
                 end;

Var
    T , i , j  : longint;
    A , B , C  : Tpoint;
    AA , AB , AC
               : extended;

function arccos(cosA : extended) : extended;
begin
    if abs(cosA) <= 1e-8
      then exit(pi / 2)
      else if cosA > 0
             then exit(arctan(sqrt(abs(1 - sqr(cosA))) / cosA))
             else exit(pi - arccos(-cosA));
end;

function dist(A , B : Tpoint) : extended;
begin
    exit(sqrt(sqr(A.x - B.x) + sqr(A.y - B.y)));
end;

function Get_Angle(A , B , C : Tpoint) : extended;
var
    LA , LB , LC ,
    cosA       : extended;
begin
    LA := dist(B , C); LB := dist(C , A); LC := dist(A , B);
    cosA := (sqr(LB) + sqr(LC) - sqr(LA)) / (2 * LB * LC);
    exit(arccos(cosA));
end;

function check_int(num : extended) : boolean;
begin
    exit(abs(round(num) - num) <= 1e-4);
end;

Begin
    readln(T);
    for i := 1 to T do
      begin
          readln(A.x , A.y , B.x , B.y , C.x , C.y);
          AA := Get_Angle(B , A , C);
          AB := Get_Angle(A , B , C);
          AC := Get_Angle(B , C , A);
          for j := 3 to 200 do
            if check_int(AA / pi * j) and check_int(AB / pi * j) and check_int(AC / pi * j) then
              begin
                  writeln(j);
                  break;
              end;
      end;
End.