#include <stdio.h>
#include <string.h>

int main()
{
  unsigned int i, n, r, x, y;
  char p[65000];

  memset(p, 1, sizeof(p));
  for (x = 2; x < 256; ++x)
    if (p[x])
      for (y = x * x; y < 65000; y += x)
        p[y] = 0;

  while (scanf("%u", &n) == 1 && n) {
    if (p[n])
      goto not;
    for (i = 2; i < n; ++i) {
      for (r = 1, x = i, y = n; y; y >>= 1, x = x * x % n)
        if (y & 1)
          r = r * x % n;
      if (r ^ i)
        goto not;
    }
    printf("The number %u is a Carmichael number.\n", n);
    continue;
  not:
    printf("%u is normal.\n", n);
  }

  return 0;
}
