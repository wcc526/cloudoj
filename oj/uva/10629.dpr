{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
{$R-,Q-,S-}
Const
    InFile     = 'p10629.in';
    OutFile    = 'p10629.out';
    Limit      = 52;
    LimitSave  = 50;
    Step       = 1000;

Type
    Tdata      = array[1..Limit] of
                   record
                       position , R , G      : longint;
                   end;
    Topt       = array[1..Limit , 1..LimitSave] of
                   record
                       opt                   : extended;
                       ok                    : boolean;
                   end;

Var
    data       : Tdata;
    opt        : Topt;
    L , N      : longint;

function init : boolean;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    readln(L , N);
    init := false;
    if L = 0 then exit;
    init := true;
    for i := 1 to N do
      with data[i + 1] do
        readln(position , R , G);
    inc(N);
    data[N + 1].position := L;
end;

procedure test_speed(S1 , S , T , i , p1 , j , p2 , time1 : longint);
var
    v , tmp    : extended;
    k , tot    : longint;
begin
    if opt[j , p2].ok
      then begin
               v := S / T;
               for k := i + 1 to j - 1 do
                 begin
                     tot := data[k].R + data[k].G;
                     tmp := (data[k].position - data[i].position) / v + time1;
                     tmp := tmp - int(tmp / tot) * tot;
                     if (tmp > 0) and (tmp < data[k].R) then exit;
                 end;
               tmp := S1 * (1 / (v * v) - 0.1 / v + 1) + opt[j , p2].opt;
               if not opt[i , p1].ok or (opt[i , p1].opt > tmp)
                 then begin
                          opt[i , p1].ok := true;
                          opt[i , p1].opt := tmp;
                      end;
           end;
end;

procedure process;
var
    i , j ,
    p1 , p2 ,
    time1 ,
    time2      : longint;
begin
    fillchar(opt , sizeof(opt) , 0);
    opt[N + 1 , 1].ok := true;
    for i := N downto 1 do
      for p1 := 1 to LimitSave do
        begin
            time1 := (p1 div 2) * data[i].R + ((p1 - 1) div 2) * data[i].G;
            test_speed(data[N + 1].position - data[i].position , (data[N + 1].position - data[i].position) * 20 , data[N + 1].position - data[i].position , i , p1 , N + 1 , 1 , time1);
            for j := i + 1 to N do
              for p2 := LimitSave downto 1 do
                begin
                    time2 := (p2 div 2) * data[j].R + ((p2 - 1) div 2) * data[j].G;
                    if time2 > time1
                      then test_speed(data[j].position - data[i].position , data[j].position - data[i].position , time2 - time1 , i , p1 , j , p2 , time1)
                      else break;
                end;
        end;
end;

procedure work;
begin
    process;
end;

procedure out;
begin
    writeln(opt[1 , 1].opt : 0 : 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
