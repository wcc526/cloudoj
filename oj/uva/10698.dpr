{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10698.in';
    OutFile    = 'p10698.out';
    Limit      = 100;
    LimitLen   = 30;

Type
    Tstr       = string[LimitLen];
    Tkey       = record
                     name                                                  : Tstr;
                     Played , Points , scored , suffered , index           : longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    N          : longint;
    first      : boolean;

function find(s : Tstr) : longint;
var
    i          : longint;
begin
    find := 0;
    for i := 1 to N do
      if data[i].name = s then
        begin
            find := i;
            exit;
        end;
end;

function init : boolean;
var
    p1 , p2 ,
    x , y ,
    G , i      : longint;
    ch         : char;
    s1 , s2    : Tstr;
begin
    readln(N , G);
    if N + G = 0
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do
                 begin
                     data[i].index := i;
                     readln(data[i].name);
                     while data[i].name[1] = ' ' do delete(data[i].name , 1 , 1);
                     while data[i].name[length(data[i].name)] = ' ' do delete(data[i].name , length(data[i].name) , 1);
                 end;
               for i := 1 to G do
                 begin
                     s1 := ''; s2 := '';
                     read(ch); while ch = ' ' do read(ch);
                     while ch <> ' ' do begin s1 := s1 + ch; read(ch); end;
                     read(p1);
                     read(ch); while ch <> '-' do read(ch);
                     read(p2);
                     read(ch); while ch = ' ' do read(ch);
                     while ch <> ' ' do begin s2 := s2 + ch; if eoln then break else read(ch); end;
                     readln;
                     x := find(s1); y := find(s2);
                     inc(data[x].scored , p1); inc(data[x].suffered , p2);
                     inc(data[y].suffered , p1); inc(data[y].scored , p2);
                     inc(data[x].played); inc(data[y].played);
                     if p1 = p2
                       then begin inc(data[x].points); inc(data[y].points); end
                       else if p1 > p2
                              then inc(data[x].points , 3)
                              else inc(data[y].points , 3);
                 end;
           end;
end;

function bigger(key1 , key2 : Tkey) : boolean;
begin
    if key1.Points <> key2.points
      then bigger := key1.points > key2.points
      else if key1.scored - key1.suffered <> key2.scored - key2.suffered
             then bigger := key1.scored - key1.suffered > key2.scored - key2.suffered
             else if key1.scored <> key2.scored
                    then bigger := key1.scored > key2.scored
                    else bigger := upcase(key1.name) < upcase(key2.name);
end;

procedure sort;
var
    i , j , max
               : longint;
    tmp        : Tkey;
begin
    for i := 1 to N do
      begin
          max := i;
          for j := i + 1 to N do
            if bigger(data[j] , data[max]) then
              max := j;
          tmp := data[max]; data[max] := data[i]; data[i] := tmp;
      end;
end;

procedure out;
var
    i          : longint;
begin
    if not first then writeln;
    first := false;
    for i := 1 to N do
      begin
          if (i <> 1) and (data[i].points = data[i - 1].Points) and (data[i].scored = data[i - 1].scored) and (data[i].suffered = data[i - 1].suffered)
            then write('    ')
            else write(i : 2 , '. ');
          write(data[i].name : 15 , data[i].Points : 4 , data[i].Played : 4 , data[i].scored : 4 , data[i].suffered : 4 , data[i].scored - data[i].suffered : 4);
          if data[i].Played = 0
            then write('    N/A')
            else write(data[i].points / data[i].played / 3 * 100 : 7 : 2);
          writeln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      first := true;
      while init do
        begin
            sort;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
