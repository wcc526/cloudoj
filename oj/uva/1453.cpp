#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<algorithm>
using namespace std;
const double EP = 1e-8;
const int MAXN = 100001;
struct point {
    double x, y;
}zero;

double cross(point a, point b, point c){
    return (a.x - c.x) * (b.y-c.y) - (a.y-c.y) * (b.x-c.x);
}
double mul(point a,point b,point c){
    return (a.x-c.x)*(b.x-c.x) + (a.y-c.y)*(b.y-c.y);
}
double dist(point a,point b){
    return (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y);
}
point pp;
point res[4*MAXN];
//返回数组存放在res数组中从0开始，top为顶点数
int cmp(const void *aa, const void *bb) {
    point *cc = (point *) aa;
    point *dd = (point *) bb;
    double kk = cross(*cc, *dd, pp);
    if (kk < -EP) return 1;
    else if (fabs(kk) < EP && dist(*cc, pp) > dist(*dd, pp) - EP) return 1;
    else return -1;
}
int graham(point ps[], int n) {
    int top, i, u = 0;
    for (i = 1; i <= n - 1; i++) if ((ps[i].y < ps[u].y) || (ps[i].y == ps[u].y && ps[i].x < ps[u].x)) u = i;
    pp = ps[u];
    ps[u] = ps[0];
    ps[0] = pp;
    qsort(ps + 1, n - 1, sizeof (point), cmp);
    for (i = 0; i <= 1; i++) res[i] = ps[i];
    top = 1;
    for (i = 2; i <= n - 1; i++) {
        while (cross(ps[i], res[top], res[top - 1]) > -EP) {
            if (top == 0)break;
            top--;
        }
        top++;
        res[top] = ps[i];
    }
    return top + 1;
}

double diameter(point poly[], int n) {
    int i, j;
    //point mid;
    double d = 0.0;
    for (i = 0; i < n; ++i) {
        for(j = i+1; j<n; ++j)
            if( d < dist(poly[i],poly[j]) )
                d = dist(poly[i],poly[j]);
    }
    return d;
}

point pos[4*MAXN];
int main(){
    int T;
    scanf("%d",&T);
    int N,PN;
    while(T--){
        PN = 0;
        scanf("%d",&N);
        for(int i=0; i<N; ++i){
            double x,y,w;
            scanf("%lf%lf%lf",&x,&y,&w);
            //printf("x=%f %f %f\n",x,y,w);
            pos[PN].x = x;
            pos[PN++].y = y;

            pos[PN].x = x + w;
            pos[PN++].y = y;

            pos[PN].x = x;
            pos[PN++].y = y+w;

            pos[PN].x = x+w;
            pos[PN++].y = y+w;
        }
        N = graham(pos, PN);

        /*
        printf("N=%d\n",N);
        for(int i=0; i<N; ++i)
            printf("i=%d  x=%.2f  y=%.2f\n",i,res[i].x,res[i].y);
        */

        printf("%.0f\n",diameter(res,N));
    }
    return 0;
}
