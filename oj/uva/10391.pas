Const
    InFile     = 'p10391.in';
    Limit      = 120000;

Type
    Tstr       = string[30];
    Tdata      = array[1..Limit] of Tstr;
    Tok        = array[1..Limit] of boolean;

Var
    data       : Tdata;
    ok         : Tok;
    N          : longint;

procedure init;
begin
//    assign(INPUT , InFile); ReSet(INPUT);
      N := 0;
      while not eof do
        begin
            inc(N);
            readln(data[N]);
        end;
//    Close(INPUT);
end;

function binary_find(s : Tstr) : boolean;
var
    start , stop ,
    mid        : longint;
begin
    start := 1; stop := N;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if data[mid] = s
            then exit(true)
            else if data[mid] < s
                   then start := mid + 1
                   else stop := mid - 1;
      end;
    exit(false);
end;

procedure workout;
var
    i , j      : longint;
begin
    ok[1] := false;
    for i := 2 to N do
      begin
          for j := 1 to length(data[i]) - 1 do
            if binary_find(copy(data[i] , 1 , j)) then
              if binary_find(copy(data[i] , j + 1 , length(data[i]) - j))
                then begin writeln(data[i]); break; end;
      end;
end;

Begin
    init;
    workout;
End.