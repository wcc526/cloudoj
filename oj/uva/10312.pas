Const
    Limit      = 26;

Type
    Topt       = array[1..Limit] of int64;

Var
    opt , f , g: Topt;
    N , tot , i: longint;

procedure pre_process;
var
    i , j      : longint;
begin
    opt[1] := 0; opt[2] := 0; g[1] := 1; g[2] := 1;

    for i := 3 to Limit do
      begin
          opt[i] := 0;
          for j := 2 to i - 1 do
            if i - j = 1
              then opt[i] := opt[i] + g[j] * g[i - j]
              else opt[i] := opt[i] + g[j] * g[i - j] * 2;
          g[i] := opt[i] + 1;
          for j := 2 to i - 1 do g[i] := g[i] + g[j] + opt[j];
      end;
    f[1] := 1; f[2] := 1;
    for i := 3 to Limit do
      begin
          f[i] := 0;
          for j := i - 1 downto 1 do
            f[i] := f[i] + f[j] * f[i - j];
      end;
end;

Begin
    pre_process;
    while not eof do
      begin
          readln(N);
          writeln(g[N] - f[N]);
      end;
End.
