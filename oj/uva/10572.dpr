{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{ $R+,Q+,S+}
Const
    InFile     = 'p10572.in';
    OutFile    = 'p10572.out';
    Limit      = 8;
    LimitSave  = 2500;

Type
    Tstate     = record
                     num , connect           : longint;
                 end;
    Tstates    = array[1..LimitSave] of Tstate;
    Tline      = array[1..Limit] of longint;
    Tindex     = array[0..1 shl Limit] of longint;
    Topt       = array[1..Limit , 1..LimitSave] of longint;
    Tstrategy  = array[1..Limit , 1..LimitSave] of longint;
    Tmap       = array[1..Limit] of longint;
    Tcheck     = array[0..1 shl Limit , 0..1 shl Limit] of boolean;
    TNewState  = array[1..LimitSave , 0..1 shl Limit] of longint;
    TchkNewState
               = array[1..LimitSave , 0..1 shl Limit] of boolean;
    Tgraph     = array[1..Limit * 2] of
                   record
                       tot    : longint;
                       data   : array[1..Limit * 2] of longint;
                   end;
    Tvisited   = array[1..Limit * 2] of boolean;

Var
    states     : Tstates;
    line       : Tline;
    index      : Tindex;
    opt        : Topt;
    strategy   : Tstrategy;
    cover , lab: Tmap;
    check      : Tcheck;
    GNewState ,
    GNewState_backup_8 ,
    GNewState_backup_7
               : Tnewstate;
    chkNewState ,
    chknewstate_backup_8 ,
    chknewstate_backup_7
               : TchkNewState;
    graph      : Tgraph;
    visited    : Tvisited;
    N , M ,
    tot , mask ,
    cases      : longint;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
    dec(Cases);
    readln(M , N);
    mask := 1 shl N - 1;
    fillchar(cover , sizeof(cover) , 0);
    fillchar(lab , sizeof(lab) , 0);
    for i := M downto 1 do
      begin
          for j := 1 to N do
            begin
                cover[i] := cover[i] * 2; lab[i] := lab[i] * 2;
                read(ch);
                case ch of
                  '.'         : ;
                  'o'         : inc(cover[i]);
                  '#'         : begin inc(cover[i]); inc(lab[i]); end;
                end;
            end;
          readln;
      end;
end;

procedure decode(num , base : longint; var line : Tline);
var
    i          : longint;
begin
    fillchar(line , sizeof(line) , 0);
    for i := N downto 1 do
      begin
          line[i] := num mod base;
          num := num div base;
      end;
end;

procedure code(line : Tline; base : longint; var num : longint);
var
    i          : longint;
begin
    num := 0;
    for i := 1 to N do
      num := num * base + line[i];
end;

procedure dfs(num , step : longint; connect : Tline);
var
    i , j      : longint;
    ok         : boolean;
begin
    if step > N
      then begin
               inc(tot);
               states[tot].num := num; code(connect , N , states[tot].connect);
           end
      else begin
               if (step <> 1) and (line[step - 1] = line[step])
                 then begin
                          connect[step] := step - 1;
                          dfs(num , step + 1 , connect);
                          connect[step] := 0;
                      end
                 else begin
                          dfs(num , step + 1 , connect);
                          for i := 1 to step - 1 do
                            if (line[i] = line[step]) and (line[i] <> line[i + 1]) then
                              begin
                                  ok := true;
                                  for j := 1 to step - 1 do
                                    if connect[j] <> 0 then
                                      if (i < j) and (i > connect[j]) then
                                        begin
                                            ok := false;
                                            break;
                                        end;
                                  if ok then
                                    begin
                                        connect[step] := i;
                                        dfs(num , step + 1 , connect);
                                        connect[step] := 0;
                                    end;
                              end;
                      end;
           end;
end;

procedure Get_States;
var
    i          : longint;
    connect    : Tline;
begin
    fillchar(states , sizeof(states) , 0);

    fillchar(index , sizeof(index) , 0);
    tot := 0;
    for i := 0 to mask do
      begin
          decode(i , 2 , line);
          index[i] := tot + 1;
          fillchar(connect , sizeof(connect) , 0);
          dfs(i , 1 , connect);
      end;
end;

function dfs_1(p : longint) : boolean;
var
    i          : longint;
begin
    visited[p] := true;
    if p > N
      then dfs_1 := true
      else begin
               for i := 1 to graph[p].tot do
                 if not visited[graph[p].data[i]] then
                   if dfs_1(graph[p].data[i]) then
                     begin
                         dfs_1 := true;
                         exit;
                     end;
               dfs_1 := false;
           end;
end;

function dfs_2(p , first : longint) : longint;
var
    tmp , 
    i , res    : longint;
begin
    visited[p] := true;
    if (p < first) and (p > N) then res := p else res := N;
    for i := 1 to graph[p].tot do
      if not visited[graph[p].data[i]] then
        begin
            tmp := dfs_2(graph[p].data[i] , first);
            if tmp > res then res := tmp;
        end;
    dfs_2 := res;
end;

function Get_New_State(source , num : longint; var newstate : longint) : boolean;
var
    line1 ,
    line2 ,
    connect1 ,
    connect2   : Tline;
    connectnum ,
    i          : longint;
begin
    for i := 1 to N * 2 do graph[i].tot := 0;
    decode(states[source].num , 2 , line1);
    decode(num , 2 , line2);
    decode(states[source].connect , N , connect1);
    for i := 1 to N do
      if connect1[i] <> 0 then
        begin
            inc(graph[i].tot); graph[i].data[graph[i].tot] := connect1[i];
            inc(graph[connect1[i]].tot); graph[connect1[i]].data[graph[connect1[i]].tot] := i;
        end;
    for i := 1 to N do
      if line1[i] = line2[i] then
        begin
            inc(graph[i].tot); graph[i].data[graph[i].tot] := i + N;
            inc(graph[i + N].tot); graph[i + N].data[graph[i + N].tot] := i;
        end;
    for i := 1 to N - 1 do
      if line2[i] = line2[i + 1] then
        begin
            inc(graph[i + N].tot); graph[i + N].data[graph[i + N].tot] := i + N + 1;
            inc(graph[i + N + 1].tot); graph[i + N + 1].data[graph[i + N + 1].tot] := i + N;
        end;

    Get_New_State := false;
    for i := 1 to N do
      begin
          fillchar(visited , sizeof(visited) , 0);
          if not dfs_1(i) then exit;
      end;

    Get_New_State := true;

    fillchar(connect2 , sizeof(connect2) , 0);
    for i := 2 to N do
      begin
          fillchar(visited , sizeof(visited) , 0);
          connect2[i] := dfs_2(i + N , i + N) - N;
      end;
    code(connect2 , N , connectnum);

    newstate := index[num];
    while states[newstate].connect <> connectnum do inc(newstate);
end;

procedure pre_process;
var
    j , k      : longint;
begin
    N := 7; mask := 1 shl 7 - 1;     
    Get_States;
    for j := 1 to tot do
      for k := 0 to mask do
        chkNewState_Backup_7[j , k] := Get_New_State(j , k , Gnewstate_backup_7[j , k]);

    N := 8; mask := 1 shl 8 - 1;
    Get_States;
    for j := 1 to tot do
      for k := 0 to mask do
        chkNewState_Backup_8[j , k] := Get_New_State(j , k , Gnewstate_backup_8[j , k]);
end;

procedure work;
var
    p1 , p2 ,
    i , j , k ,
    newstate   : longint;
begin
    fillchar(opt , sizeof(opt) , 0);
    fillchar(strategy , sizeof(strategy) , 0);
    fillchar(check , sizeof(check) , 0);
    for i := 0 to mask do
      for j := 0 to mask do
        begin
            p1 := i; p2 := j; check[i , j] := true;
            for k := 1 to N - 1 do
              if (p1 mod 2 = p2 mod 2) and (p1 mod 2 = p1 div 2 mod 2) and (p2 mod 2 = p2 div 2 mod 2)
                then begin
                         check[i , j] := false;
                         break;
                     end
                else begin p1 := p1 div 2; p2 := p2 div 2; end;
        end;
    Get_States;       
    for i := 0 to mask do
      if (lab[1] xor i) and cover[1] = 0 then
        opt[1 , index[i]] := 1;

    if N = 7 then
      begin
          chkNewstate := chkNewstate_Backup_7;
          Gnewstate := GNewstate_Backup_7;
      end;
    if N = 8 then
      begin
          chkNewstate := chkNewstate_Backup_8;
          Gnewstate := GNewstate_Backup_8;
      end;
    if N < 7 then
      for j := 1 to tot do
        for k := 0 to mask do
          chkNewState[j , k] := check[states[j].num , k] and Get_New_State(j , k , Gnewstate[j , k]);
    for i := 2 to M do
      for j := 1 to tot do
        if opt[i - 1 , j] <> 0 then
          for k := 0 to mask do
            if check[states[j].num , k] and ((lab[i] xor k) and cover[i] = 0) then
              begin
                  if chkNewState[j , k] then
                    begin
                       newstate := Gnewstate[j , k];
                       inc(opt[i , newstate] , opt[i - 1 , j]);
                       strategy[i , newstate] := j;
                    end;
              end;
end;

function check_state(p , special : longint) : boolean;
var
    line ,
    connect    : Tline;
    i , j      : longint;
begin
    decode(states[p].num , 2 , line);
    decode(states[p].connect , N , connect);
    check_state := false;
    for i := 2 to N do
      for j := i - 1 downto 1 do
        if (line[i] = line[j]) and (line[j] <> special) then
          if connect[i] <> j
            then exit
            else break;
    check_state := true;
end;

procedure out;
var
    lastsign , 
    i , answer ,
    p , j , st : longint;
begin
    answer := 0; p := 0; lastsign := -1;
    for i := tot downto 1 do 
      if opt[M , i] <> 0 then
        begin
            if check_state(i , -1) then
              begin
                  inc(answer , opt[M , i]);
                  if p = 0 then
                    p := i;
              end;
        end;
    for i := 1 to tot do
      if opt[M - 1 , i] <> 0 then
        begin
            if check_state(i , 0) then
              if ((0 xor lab[M]) and cover[M] = 0) and check[states[i].num , 0]
                then begin p := i; lastsign := 0; inc(answer , opt[M - 1 , i]); end;
            if check_state(i , 1) then
              if ((mask xor lab[M]) and cover[M] = 0) and check[states[i].num , mask]
                then begin p := i; lastsign := 1; inc(answer , opt[M - 1, i]); end;
        end;
    writeln(answer);
    if answer <> 0 then
      begin
          st := M;
          if lastsign <> -1 then
            begin
                dec(st);
                if lastsign = 0
                  then for j := 1 to N do write('o')
                  else for j := 1 to N do write('#');
                writeln;
            end;
          for i := st downto 1 do
            begin
                decode(states[p].num , 2 , line);
                for j := 1 to N do
                  if line[j] = 0
                    then write('o')
                    else write('#');
                writeln;
                p := strategy[i , p];
            end;
      end;
    if cases <> 0 then writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
