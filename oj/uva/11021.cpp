/*
 * uva_11021.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
#include <cstdio>
#include <cmath>
const int MXN=1000+10;
const int MXM=1000+10;
int n,k,m;
double P[MXN],f[MXM];
int main()
{
	int T;
	scanf("%d",&T);
	for(int kase=1;kase<=T;++kase)
	{
		scanf("%d%d%d",&n,&k,&m);
		for(int i=0;i<n;++i)
			scanf("%lf",&P[i]);
		f[0]=0;
		f[1]=P[0];
		for(int i=2;i<=m;++i)
		{
			f[i]=0;
			for(int j=0;j<n;++j)
				f[i]+=P[j]*pow(f[i-1],j);
		}
		printf("Case #%d: %.7lf\n",kase,pow(f[m],k));
	}
	return 0;
}






