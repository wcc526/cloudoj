{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10423.in';
    OutFile    = 'p10423.out';
    Limit      = 60;

Type
    Tkey       = record
                     time     : longint;
                     num      : string;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    cases , answer , 
    N , st , T : longint;

function init : boolean;
var
    P , i ,
    time       : longint;
    ch         : char;
    s          : string;
begin
    inc(Cases);
    readln(P);
    if p = 0
      then init := false
      else begin
               init := true;
               N := 0;
               for i := 1 to p do
                 begin
                     read(ch); s := '';
                     while ch <> ':' do
                       begin
                           s := s + ch;
                           read(ch);
                       end;
                     read(time);
                     while time <> -1 do
                       begin
                           inc(N);
                           data[N].time := time; data[N].num := s;
                           read(time);
                       end;
                     readln;
                 end;
               readln(st , T);
           end;
end;

procedure work;
var
    i , j , k  : longint;
    tmp        : Tkey;
begin
    for i := 1 to N do
      for j := i + 1 to N do
        if data[i].time > data[j].time then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;
    k := 1;
    for i := 1 to N do
      if st <= data[i].time then
        begin
            k := i; break;
        end;
    answer := (k + T - 2 + N) mod N + 1;
end;

procedure out;
begin
    writeln('Case ' , cases , ': Peter arrives at stop ' , T , ' by tram ' , data[answer].num , '.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
