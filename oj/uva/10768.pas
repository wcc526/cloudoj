{$I-}
Const
    InFile     = 'p10768.in';
    OutFile    = 'p10768.out';
    Limit      = 20;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;

Var
    map        : Tmap;
    answer     : boolean;
    N          : longint;

procedure init;
var
    i , M ,
    p1 , p2    : longint;
begin
    fillchar(map , sizeof(map) , 0);
    readln(N , M);
    for i := 1 to M do
      begin
          readln(p1 , p2);
          if p1 <> p2 then
            begin
                map[p1 , p2] := true;
                map[p2 , p1] := true;
            end;
      end;
end;

procedure work;
var
    count ,
    p1 , p2 ,
    a , b , c ,
    d , e , f ,
    i , j      : longint;
    change     : boolean;
begin
    repeat
      change := false;
      for i := 1 to N do
        begin
            count := 0;
            map[i , i] := false;
            for j := 1 to N do
              if map[i , j] then
                inc(count);
            if count = 1 then
              begin
                  for j := 1 to N do
                    begin
                        map[i , j] := false; map[j , i] := false;
                    end;
                  change := true;
              end;
            if count = 2 then
              begin
                  change := true;
                  p1 := 0; p2 := 0;
                  for j := 1 to N do
                    if map[i , j] then
                      if p1 = 0
                        then p1 := j
                        else p2 := j;
                  map[p1 , p2] := true; map[p2 , p1] := true;
                  map[p1 , i] := false; map[i , p1] := false;
                  map[p2 , i] := false; map[i , p2] := false;
              end;
        end;
    until not change;

    answer := false;
    for a := 1 to N do
      for b := a + 1 to N do
        if map[a , b] then
          for c := b + 1 to N do
            if map[a , c] and map[b , c] then
              for d := c + 1 to N do
                if map[a , d] and map[b , d] and map[c , d] then
                  for e := d + 1 to N do
                    if map[a , e] and map[b , e] and map[c , e] and map[d , e] then
                      exit;

    for a := 1 to N do
      for b := a + 1 to N do
        for c := b + 1 to N do
          for d := 1 to N do
            if map[a , d] and map[b , d] and map[c , d] then
              for e := d + 1 to N do
                if map[a , e] and map[b , e] and map[c , e] then
                  for f := e + 1 to N do
                    if map[a , f] and map[b , f] and map[c , f] then
                      exit;

    answer := true;
end;

procedure out;
begin
    if answer
      then writeln('YES')
      else writeln('NO');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            if (N = 0) then break;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
