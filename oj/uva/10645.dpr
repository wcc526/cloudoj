{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10645.in';
    OutFile    = 'p10645.out';
    LimitK     = 30;
    LimitM     = 100;
    Limit      = 50;

Type
    Topt       = array[0..LimitK , 0..Limit , 0..LimitM] of
                   record
                       benefit , father , K  : longint;
                   end;
    Tdata      = array[1..Limit] of
                   record
                       cost , benefit        : longint;
                   end;

Var
    data       : Tdata;
    opt        : Topt;
    K , N , M  : longint;

function init : boolean;
var
    i          : longint;
begin
    read(K , N , M);
    if K = 0
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               fillchar(opt , sizeof(opt) , $FF);
               for i := 1 to N do
                 with data[i] do
                   begin
                       read(cost , benefit);
                       benefit := benefit * 2;
                   end;
           end;
end;

procedure work;
var
    i , j , j1 ,
    p          : longint;
begin
    opt[0 , 0 , 0].benefit := 0;
    for i := 1 to K do
      for j := 1 to N do
        for j1 := 0 to N do
          if j1 <> j then
            for p := 0 to M do
              begin
                  if opt[i - 1 , j1 , p].benefit <> -1 then
                    if p + data[j].cost <= M then
                      if (opt[i , j , p + data[j].cost].benefit = -1) or (opt[i , j , p + data[j].cost].benefit < opt[i - 1 , j1 , p].benefit + data[j].benefit) then
                        begin
                            opt[i , j , p + data[j].cost].benefit := opt[i - 1 , j1 , p].benefit + data[j].benefit;
                            opt[i , j , p + data[j].cost].K := i - 1;
                            opt[i , j , p + data[j].cost].father := j1;
                        end;
                  if (i > 1) and (opt[i - 2 , j1 , p].benefit <> -1) then
                    if p + data[j].cost * 2 <= M then
                      if (opt[i , j , p + data[j].cost * 2].benefit = -1) or (opt[i , j , p + data[j].cost * 2].benefit < opt[i - 2 , j1 , p].benefit + data[j].benefit * 3 div 2) then
                        begin
                            opt[i , j , p + data[j].cost * 2].benefit := opt[i - 2 , j1 , p].benefit + data[j].benefit * 3 div 2;
                            opt[i , j , p + data[j].cost * 2].K := i - 2;
                            opt[i , j , p + data[j].cost * 2].father := j1;
                        end;
              end
          else
            for p := 0 to M do
              if opt[i - 1 , j1 , p].benefit <> -1 then
                if p + data[j].cost <= M then
                  if (opt[i , j , p + data[j].cost].benefit = -1) or (opt[i , j , p + data[j].cost].benefit < opt[i - 1 , j1 , p].benefit) then
                    begin
                        opt[i , j , p + data[j].cost].benefit := opt[i - 1 , j1 , p].benefit;
                        opt[i , j , p + data[j].cost].K := i - 1;
                        opt[i , j , p + data[j].cost].father := j1;
                    end;
end;

procedure out;
var
    newj , newi ,
    p , j , max ,
    i          : longint;
    find       : boolean;
begin
    max := -1;
    for p := 0 to M do
      for j := 1 to N do
        if opt[K , j , p].benefit > max then
          max := opt[K , j , p].benefit;

    if max = -1 then begin writeln('0.0'); exit; end;
    p := 0;
    while p <= M do
      begin
          find := false;
          for j := 1 to N do
            if opt[K , j , p].benefit = max then
              begin
                  find := true;
                  break;
              end;
          if find then break;
          inc(p);
      end;
    writeln(max / 2 : 0 : 1);
    
    i := K;
    while i <> 0 do
      begin
          newj := opt[i , j , p].father;
          newi := opt[i , j , p].K;
          if opt[i , j , p].K = i - 1
            then begin write(j , ' '); dec(p , data[j].cost); end
            else begin write(j , ' ' , j , ' '); dec(p , data[j].cost * 2); end;
          j := newj;
          i := newi;
      end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
