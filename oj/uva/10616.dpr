{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10616.in';
    OutFile    = 'p10616.out';
    Limit      = 200;
    LimitD     = 20;
    LimitM     = 10;

Type
    Tdata      = array[1..Limit] of longint;
    Topt       = array[0..Limit , 0..LimitD , 0..LimitM] of comp;

Var
    data       : Tdata;
    opt        : Topt;
    nowCase , N ,
    Q , D , M  : longint;

function init : boolean;
var
    i          : longint;
begin
    inc(nowCase);
    read(N , Q);
    init := false;
    if (N = 0) and (Q = 0) then exit;
    init := true;
    for i := 1 to N do
      read(data[i]);
end;

procedure process;
var
    i , j , k  : longint;
begin
    fillchar(opt , sizeof(opt) , 0);
    opt[0 , 0 , 0] := 1;
    for i := 0 to N - 1 do
      for j := 0 to D - 1 do
        for k := 0 to M do
          begin
              if k < M
                then opt[i + 1 , (j + data[i + 1] mod D + D) mod D , k + 1] := opt[i + 1 , (j + data[i + 1] mod D + D) mod D , k + 1] + opt[i , j , k];
              opt[i + 1 , j , k] := opt[i + 1 , j , k] + opt[i , j , k];
          end;
end;

procedure workout;
var
    i          : longint;
begin
    writeln('SET ' , nowCase , ':');
    for i := 1 to Q do
      begin
          read(D , M);
          process;
          writeln('QUERY ' , i , ': ' , opt[N , 0 , M] : 0 : 0);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      nowCase := 0;
      while init do
        workout;
//    Close(OUTPUT);
//    Close(INPUT);
End.
