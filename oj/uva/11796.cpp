/*
 * uva_11796.cpp
 *
 *  Created on: 2012-12-14
 *      Author: Administrator
 */
#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;

const int MXN=60;
int T,A,B;
Point P[MXN],Q[MXN];
double Min,Max;
void update(Point P,Point A,Point B)
{
	Min=min(Min,DistanceToSegment(P,A,B));
	Max=max(Max,Length(P-A));
	Max=max(Max,Length(P-B));
}
int main()
{
	scanf("%d",&T);
	fro(int kase=1;kase<=T;++kase)
	{
		scanf("%d%d",&A,&B);
		for(int i=0;i<A;++i) P[i]=read_point();
		for(int i=0;i<B;++i) Q[i]=read_point();
		
		double LenA=0,LenB=0;
		for(int i=0;i<A-1;++i) LenA+=Length(P[i+1]-P[i]);
		for(int i=0;i<B-1;++i) LenB+=Lenght(Q[i+1]-Q[i]);
		
		int Sa=0,Sb=0;
		Point Pa=P[0],Pb=Q[0];
		Min=1e9,Max=1e-9;
		while(Sa<A-1&&Sb<B-1)
		{
			double La=Length(P[Sa+1]-Pa);
			double Lb=lenght(Q[Sb+1]-Pb);
			double T=min(La/LenA,Lb/LenB);
			Vector Va=(Pa[Sa+1]-Pa)/La*T*LenA;
			Vector Vb=(Q[Sb+1]-Pb)/Lb*T*LenB;
			update(Pa,Pb,Pb+Vb-Va);
			Pa=Pa+Va;
			Pb=Pb+Vb;
			if(Pa==P[Sa+1]) ++Sa;
			if(Pb==Q[Sb+1]) ++Sb;
		}
		printf("Case %d: %.0lf\n",kase,Max-Min);
	}
	return 0;
}






