#include <stdio.h>

void r(char *x, int i)
{
  switch (getchar()) {
  case 'p':
    x[i] = 2;
    r(x, (i << 2) + 1);
    r(x, (i << 2) + 2);
    r(x, (i << 2) + 3);
    r(x, (i << 2) + 4);
    break;
  case 'f':
    x[i] = 1;
    break;
  case 'e':
    x[i] = 0;
    break;
  }
}

void g(char *x, char *z, int i)
{
  if (x[i] == 2)
    z[i] = 2,
      g(x, z, (i << 2) + 1),
      g(x, z, (i << 2) + 2),
      g(x, z, (i << 2) + 3),
      g(x, z, (i << 2) + 4);
  else
    z[i] = x[i];
}

void f(char *x, char *y, char *z, int i)
{
  if (x[i] == 1 || y[i] == 1)
    z[i] = 1;
  else if (x[i] == 0)
    g(y, z, i);
  else if (y[i] == 0)
    g(x, z, i);
  else
    z[i] = 2,
      f(x, y, z, (i << 2) + 1),
      f(x, y, z, (i << 2) + 2),
      f(x, y, z, (i << 2) + 3),
      f(x, y, z, (i << 2) + 4);
}

int c(char *x, int i, int b)
{
  if (x[i] == 2)
    return
      c(x, (i << 2) + 1, b >> 2) +
      c(x, (i << 2) + 2, b >> 2) +
      c(x, (i << 2) + 3, b >> 2) +
      c(x, (i << 2) + 4, b >> 2);
  else if (x[i] == 1)
    return b;
  else
    return 0;
}

int main()
{
  int t;
  char x[2048], y[2048], z[2048];

  scanf("%d\n", &t);
  while (t--) {
    r(x, 0), getchar();
    r(y, 0), getchar();
    f(x, y, z, 0);
    printf("There are %d black pixels.\n", c(z, 0, 1 << 10));
  }

  return 0;
}
