#include <stdio.h>

int main()
{
  int c, n;

  for (;;) {
    for (n = 0; (c = getchar()) >= '0' && c <= '9'; n += c - '0');
    if ((c >= 'A' && c <= 'Z') || c == '*')
      while (n--)
        putchar(c);
    else if (c == 'b')
      while (n--)
        putchar(' ');
    else if (c == '!' || c == '\n')
      putchar('\n');
    else if (c == EOF)
      break;
  }

  return 0;
}
