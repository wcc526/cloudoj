Const
    InFile     = 'p10380.in';
    Limit      = 50;
    LimitP     = Limit * Limit;

Type
    Tdata      = array[1..Limit , 1..Limit , 1..2] of longint;
    Tmap       = array[1..LimitP , 1..LimitP] of smallint;
    Tcount     = array[1..Limit] of longint;
    Tvisited   = array[1..LimitP] of boolean;

Var
    mark , data: Tdata;
    C          : Tmap;
    count      : Tcount;
    visited    : Tvisited;
    cases ,
    answer , tot ,
    S , T , Flow ,
    N , P , M  : longint;

procedure init;
var
    i , j      : longint;
    ch1 , ch2 ,
    ch3        : char;
begin
    fillchar(C , sizeof(C) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(count , sizeof(count) , 0);
    dec(Cases);
    readln(N , M);
    for i := 1 to N do
      begin
          for j := 1 to i - 1 do
            begin
                read(ch1 , ch2 , ch3);
                if ch1 = '-' then data[i , j , 1] := -1 else data[i , j , 1] := ord(ch1) - ord('0');
                if ch2 = '-' then data[i , j , 2] := -1 else data[i , j , 2] := ord(ch2) - ord('0');
            end;
          readln;
      end;
end;

function dfs(s : longint) : boolean;
var
    i , start ,
    stop      : longint;
begin
    if s = T then begin inc(Flow); exit(true); end;
    visited[s] := true;
    if s = N + 1 then begin start := N + 3; stop := P; end;
    if s = N + 2 then begin start := 1; stop := N; end;
    if s > N + 2 then begin start := 1; stop := N + 1; end;
    if S < N + 1 then begin start := N + 2; stop := P; end;
    for i := start to stop do
      if (C[s , i] > 0) and not visited[i] then
        if dfs(i) then
          begin
              dec(C[s , i]); inc(C[i , s]);
              exit(true);
          end;
    exit(false);
end;

procedure work;
var
    k ,
    i , j , max: longint;
begin
    tot := 0;
    P := N + 2; S := N + 1; T := N + 2;
    for i := 1 to N do
      for j := 1 to i - 1 do
        for k := 1 to 2 do
          if data[i , j , k] <> -1
            then begin
                     inc(count[i] , data[i , j , k]);
                     inc(count[j] , 1 - data[i , j , k]);
                 end
            else begin
                     if (i = M) or (j = M) then
                       begin
                           if i = M then data[i , j , k] := 1 else data[i , j , k] := 0;
                           inc(count[M]); continue;
                       end;
                     inc(P);
                     mark[i , j , k] := P;
                     C[P , i] := 1; C[P , j] := 1;
                     C[S , P] := 1; inc(tot);
                 end;
    max := 0;
    for i := 1 to N do if (i <> M) and (max < count[i]) then max := count[i];
    for i := 1 to N do if i <> M then C[i , T] := max - count[i];

    Flow := 0; answer := count[M] - max;
    while answer >= 0 do
      begin
          fillchar(visited , sizeof(visited) , 0);
          while (Flow < tot) and dfs(S) do
            fillchar(visited , sizeof(visited) , 0);
          if Flow = tot then break;
          dec(answer); if answer < 0 then break;
          for i := 1 to N do if i <> M then inc(C[i , T]);
      end;
end;

procedure out;
var
    i , j , num ,
    ni , nj ,
    p1 , p2    : longint;
begin
    if (Flow < tot) or (answer < 0)
      then writeln('Player ' , M , ' can''t win!')
      else begin
               writeln('Player ' , M , ' can win with ' , answer , ' point(s).');
               writeln;
               for i := 1 to N do
                 begin
                     num := 0;
                     for j := 1 to N do
                       if i = j
                         then write('-- ')
                         else begin
                                  if i < j
                                    then begin ni := j; nj := i; end
                                    else begin ni := i; nj := j; end;
                                  if data[ni , nj , 1] = -1 then p1 := 1 - C[mark[ni , nj , 1] , ni] else p1 := data[ni , nj , 1];
                                  if data[ni , nj , 2] = -1 then p2 := 1 - C[mark[ni , nj , 2] , ni] else p2 := data[ni , nj , 2];
                                  if i < j then begin p1 := 1 - p1; p2 := 1 - p2; end;
                                  write(p1 , p2 , ' ');
                                  inc(num , p1 + p2);
                              end;
                     writeln(': ' , num);
                 end;
           end;
    if Cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
