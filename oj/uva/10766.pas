{$R-,Q-,S-}
Const
    InFile     = 'p10766.in';
    OutFile    = 'p10766.out';
    Limit      = 50;
    LimitLen   = 5;
    Prime      = 1000000000000037;

Type
    Tmatrix    = array[1..Limit , 1..Limit] of extended;

Var
    adjacency  : Tmatrix;
    modulo ,
    answer     : extended;
    N          : longint;

procedure init;
var
    i , j , M ,
    p1 , p2    : longint;
begin
    readln(N , M , j);
    for i := 1 to N do
      for j := 1 to N do
        if i <> j then adjacency[i , j] := 1 else adjacency[i , j] := N - 1;
    for i := 1 to M do
      begin
          readln(p1 , p2);
          if p1 = p2 then continue;
          if adjacency[p1 , p2] = 1 then
            begin
                adjacency[p1 , p2] := 0;
                adjacency[p2 , p1] := 0;
                adjacency[p1 , p1] := adjacency[p1 , p1] - 1;
                adjacency[p2 , p2] := adjacency[p2 , p2] - 1;
            end;
      end;
    for i := 1 to N do
      for j := 1 to N do
        if i <> j then
          adjacency[i , j] := {Prime} - adjacency[i , j];
end;

procedure swap(var a , b : extended);
var
    tmp        : extended;
begin
    tmp := a; a := b; b := tmp;
end;

function find(p : longint) : boolean;
var
    i , j , k  : longint;
begin
    find := false;
    for i := p to N do
      for j := p to N do
        if adjacency[i , j] <> 0 then
          begin
              find := true;
              for k := 2 to N do
                swap(adjacency[i , k] , adjacency[p , k]);
              for k := 2 to N do
                swap(adjacency[k , j] , adjacency[k , p]);
              exit;
          end;
end;

procedure euclid_extend(a , b , c : extended; var x , y : extended);
var
    t ,
    p , q      : extended;
begin
    if a = 0
      then begin
               x := 0; y := c / b;
           end
      else begin
               p := int(b / a);
               q := b - p * a;
               euclid_extend(q , a , c , y , t);
               x := t - y * p;
               x := x - int(x / abs(b)) * abs(b);
               y := (c - a * x) / b;
           end;
end;

function modulo_divide(a , b , modulo : extended) : extended;
var
    x , y      : extended;
begin
    euclid_extend(b , modulo , a , x , y);
    x := x - int(x / modulo) * modulo;
    if x < 0 then x := x + modulo;
    exit(x);
end;

function times(a , b , modulo : extended) : extended;
var
    power ,
    res        : extended;
begin
    res := 0; power := a;
    while b <> 0 do
      begin
          if int(b / 2) * 2 <> b then
            begin
                res := res + power;
                res := res - int(res / modulo) * modulo;
            end;
          power := power + power;
          power := power - int(power / modulo) * modulo;
          b := int(b / 2);
      end;
    times := res;
end;

procedure work;
var
    i , j , k  : longint;
    t1 , t2 ,
    t3         : extended;
begin
    answer := 0;
    for i := 2 to N do
      begin
          if not find(i) then exit;
          for j := i + 1 to N do
            if adjacency[j , i] <> 0 then
              begin
  //                t3 := Modulo_divide(adjacency[j , i] , adjacency[i , i] , modulo);

                  t3 := adjacency[j , i] / adjacency[i , i];
                  for k := i to N do
                    begin
{                        t2 := times(t3 , adjacency[i , k] , modulo);
                        t1 := adjacency[j , k] - t2;
                        t1 := t1 - int(t1 / modulo) * modulo;
                        if t1 < 0 then t1 := t1 + modulo;
                        adjacency[j , k] := t1;}
                        adjacency[j , k] := adjacency[j , k] - adjacency[i , k] * t3;
                    end;
              end;
      end;

    answer := 1;
    for i := 2 to N do
      begin
          answer := answer * adjacency[i , i];
//          answer := answer - int(answer / modulo) * modulo;
      end;
end;

procedure out;
begin
    writeln(answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    modulo := prime;
    while not eof do
      begin
          init;
          work;
          out;
      end;
End.
