Const
    InFile     = 'p10258.in';
    Limit      = 100;

Type
    Tkey       = record
                     ok                      : boolean;
                     name , problem ,
                     penalty                 : longint;
                     solved                  : array[1..Limit] of longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    cases      : longint;

function compare(k1 , k2 : Tkey) : longint;
begin
    if k1.problem <> k2.problem
      then exit(k1.problem - k2.problem)
      else if k1.penalty <> k2.penalty
             then exit(k2.penalty - k1.penalty)
             else exit(k2.name - k1.name);
end;

procedure main;
var
    i , j ,
    contestant ,
    problem ,
    time       : longint;
    ch         : char;
    tmp        : Tkey;
begin
    fillchar(data , sizeof(data) , 0);
    for contestant := 1 to Limit do
      data[contestant].name := contestant;
    readln;
    while not eoln do
      begin
          read(contestant , problem , time);
          repeat read(ch); until ch <> ' ';
          data[contestant].ok := true;
          if ch = 'I'
            then if data[contestant].solved[problem] <= 0
                   then dec(data[contestant].solved[problem]);
          if ch = 'C'
            then if data[contestant].solved[problem] <= 0
                   then begin
                            data[contestant].solved[problem] := abs(data[contestant].solved[problem]) + 1;
                            inc(data[contestant].penalty , 20 * (data[contestant].solved[problem] - 1) + time);
                            inc(data[contestant].problem);
                        end;
          readln;
      end;
    for i := 1 to Limit do
      for j := i + 1 to Limit do
        if compare(data[i] , data[j]) < 0
          then begin
                   tmp := data[i]; data[i] := data[j]; data[j] := tmp;
               end;
    for i := 1 to Limit do
      if data[i].ok then
        writeln(data[i].name , ' ' , data[i].problem , ' ' , data[i].penalty);
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          main;
          if cases <> 0 then writeln;
      end;
End.