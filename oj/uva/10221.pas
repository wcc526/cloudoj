Var
    S , A      : extended;
    st         : string[255];

Begin
    while not eof do
      begin
          readln(S , A , st);
          S := S + 6440;
          if pos('min' , st) <> 0 then A := A / 60;
          A := A / 180 * pi;
          if A > pi then A := 2 * pi - A;
          writeln(A * S : 0 : 6 , ' ' , S * sin(A / 2) * 2 + 1e-10 : 0 : 6);
      end;
End.