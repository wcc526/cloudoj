Const
    InFile     = 'p10455.in';
    OutFile    = 'p10455.out';
    Limit      = 100;
    LimitLen   = 6;

Type
    Tstr       = string[LimitLen];
    Tdata      = record
                     tot      : longint;
                     data     : array[1..Limit] of Tstr;
                 end;
    Tsum       = array[1..LimitLen] of qword;

Var
    data       : Tdata;
    source     : Tstr;
    sum        : Tsum;
    N , Cases  : longint;
    count      : qword;

procedure init;
var
    ch         : char;
begin
    dec(Cases);
    N := 0; source := '';
    read(ch);
    while ch <> ' ' do begin source := source + ch; inc(N); read(ch); end;
    readln(count);
end;

procedure pre_process;
var
    i          : longint;
begin
    sum[1] := 1;
    for i := 2 to 6 do
      sum[i] := sum[i - 1] * sum[i - 1] * i;
end;

procedure insert(var s : Tstr; position : longint; ch : char);
begin
    s := copy(s , 1 , position - 1) + ch + copy(s , position , length(s) - position + 1);
end;

procedure dfs_print(start : Tstr; N : longint; count : qword; var res : Tdata);
var
    ch         : char;
    upper , down
               : qword;
    position ,
    p , i      : longint;
begin
    if N = 1
      then begin
               if start = '0'
                 then begin
                         res.data[res.tot + 1] := '0'; res.data[res.tot + 2] := '1';
                      end
                 else begin
                         res.data[res.tot + 1] := '1'; res.data[res.tot + 2] := '0';
                      end;
               inc(res.tot , 2);
               exit;
           end;
    position := (count - 1) div (sum[N - 1] * sum[N - 1]) + 1;
    upper := (count - 1) mod (sum[N - 1] * sum[N - 1]) div sum[N - 1] + 1;
    down := (count - 1) mod sum[N - 1] + 1;
    ch := start[position]; delete(start , position , 1);
    p := res.tot;
    dfs_print(start , N - 1 , upper , res);
    start := res.data[res.tot];
    for i := p + 1 to res.tot do
      insert(res.data[i] , position , ch);
    p := res.tot;
    if ch = '0' then ch := '1' else ch := '0';
    dfs_print(start , N - 1 , down , res);
    for i := p + 1 to res.tot do
      insert(res.data[i] , position , ch);
end;

procedure out;
var
    i          : longint;
begin
    for i := 1 to data.tot do
      writeln(data.data[i]);
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    pre_process;
    readln(cases);
    while cases > 0 do
      begin
          init;
          data.tot := 0;
          dfs_print(source , N , count , data);
          out;
      end;
End.