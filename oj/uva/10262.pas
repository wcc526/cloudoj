Const
    InFile     = 'p10262.in';
    Limit      = 1010;

Type
    Tstr       = object
                     len      : longint;
                     data     : array[1..Limit] of char;
                     procedure readin;
                     procedure print;
                 end;

Var
    sA , sB ,
    A , B      : Tstr;

procedure Tstr.readin;
begin
    fillchar(self , sizeof(self) , 0);
    while not eoln do begin inc(len); read(data[len]); end;
    readln;
end;

procedure Tstr.print;
var
    i          : longint;
begin
    for i := 1 to Len do write(data[i]);
    writeln;
end;

procedure init;
begin
    A.readin; B.readin;
end;

function same(const sA , sB : Tstr) : boolean;
var
    i          : longint;
begin
    if sA.len <> sB.len
      then exit(false)
      else for i := 1 to sA.len do
             if sA.data[i] <> sB.data[i]
               then exit(false);
    exit(true);
end;

procedure Get_Ans(Len : longint; const A : Tstr; var sA : Tstr);
var
    i , j      : longint;
begin
    sA.len := -1; i := Len + 1; j := A.len;
    while i < j do
      begin
          if A.data[i] <> A.data[j] then exit;
          inc(i); dec(j);
      end;
    for i := 1 to Len do sA.data[Len - i + 1] := A.data[i];
    sA.Len := Len;
end;

function chooseA : boolean;
var
    i          : longint;
begin
    for i := 1 to sA.len do
      if sA.data[i] <> sB.data[i]
        then exit(sA.data[i] < sB.data[i]);
    exit(true);
end;

procedure workout;
var
    i          : longint;
    tmp        : Tstr;
begin
    if same(A , B) then
      begin writeln('No solution.'); exit; end;
    if A.len > B.len
      then begin tmp := A; A := B; B := tmp; end;
    i := 0;
    while i <= A.len do
      begin
          Get_Ans(i , A , sA);
          Get_Ans(i , B , sB);
          if not same(sA , sB)
            then begin
                     if (sA.len >= 0) and (sB.len >= 0)
                       then if chooseA
                              then sA.print
                              else sB.print
                       else if sA.len >= 0
                              then sA.print
                              else sB.print;
                     exit;
                 end;
          inc(i);
      end;
    sA.len := A.len + 1;
    for i := 1 to A.len do sA.data[A.len - i + 2] := A.data[i];
    if B.data[sA.len] = 'a' then sA.data[1] := 'b' else sA.data[1] := 'a';
    sA.print;
end;

Begin
    while not eof do
      begin
          init;
          workout;
      end;
End.