Const
    InFile     = 'p10269.in';
    Limit      = 100;
    LimitK     = 10;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tshortest  = array[1..Limit , 0..LimitK] of longint;
    Tvisited   = array[1..Limit , 0..LimitK] of boolean;

Var
    map        : Tmap;
    shortest   : Tshortest;
    visited    : Tvisited;
    answer ,
    A , B , K , M ,
    L , cases  : longint;

procedure init;
var
    i , Len ,
    p1 , p2    : longint;
begin
    dec(Cases);
    fillchar(map , sizeof(map) , $3F);
    readln(A , B , M , L , K);
    for i := 1 to A + B do map[i , i] := 0;
    for i := 1 to M do
      begin
          read(p1 , p2 , Len);
          if Len < map[p1 , p2]
            then begin
                     map[p1 , p2] := Len;
                     map[p2 , p1] := Len;
                 end;
      end;
end;

procedure work;
var
    i , j , p ,
    mini , minj ,
    tmp        : longint;
begin
    fillchar(shortest , sizeof(shortest) , $3F);
    fillchar(visited , sizeof(visited) , 0);
    for p := 1 to A do
      for i := 1 to A + B do
        for j := 1 to A + B do
          if map[i , j] > map[i , p] + map[p , j] then
            map[i , j] := map[i , p] + map[p , j];

    shortest[A + B , 0] := 0;
    while true do
      begin
          mini := 0;
          for i := 1 to A + B do
            for j := 0 to K do
              if not visited[i , j] then
                if (mini = 0) or (shortest[mini , minj] > shortest[i , j]) then
                  begin mini := i; minj := j; end;

          if mini = 1
            then begin answer := shortest[mini , minj]; break; end;
          visited[mini , minj] := true;

          for i := 1 to A + B do
            begin
                tmp := map[mini , i] + shortest[mini , minj];
                if shortest[i , minj] > tmp then shortest[i , minj] := tmp;

                if (minj < K) and (map[mini , i] <= L) then
                  if shortest[i , minj + 1] > shortest[mini , minj]
                    then shortest[i , minj + 1] := shortest[mini , minj];
            end;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            writeln(answer);
        end;
//    Close(INPUT);
End.