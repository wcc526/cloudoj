#include <stdio.h>
#include <string.h>

int abs(int x)
{
  return x < 0 ? -x : x;
}

int gcd(int u, int v)
{
  int g = 1, t;

  while (!(u & 1 || v & 1))
    u >>= 1, v >>= 1, g <<= 1;
  while (u) {
    if (!(u & 1))
      u >>= 1;
    else if (!(v & 1))
      v >>= 1;
    else {
      t = abs(u - v) >> 1;
      if (u < v)
        v = t;
      else
        u = t;
    }
  }
  return g * v;
}

int main()
{
  int b, c, i, j, k, n;
  unsigned short f[100], p[4798];
  unsigned char s[46431];

  memset(s, 0xFF, sizeof(s));
  for (i = 2; i < 1025; ++i)
    if (s[i])
      for (j = i * i; j < sizeof(s); j += i)
        s[j] = 0;
  for (n = 0, i = 1; n < sizeof(p) / sizeof(*p) && i < sizeof(s); ++i)
    if (s[i])
      p[n++] = i;

  while (scanf("%d", &n) == 1 && n) {
    if (n == -2147483648) {
      puts("31");
      continue;
    }
    if (n < 0)
      b = 1, n = -n;
    else
      b = 0;
    for (c = j = 0, i = 1; i < 4798 && p[i] <= n; ++i, j = 0) {
      if (n % p[i] == 0) {
        do
          n /= p[i], ++j;
        while (n % p[i] == 0);
        f[c++] = j;
      }
    }
    for (k = f[0], i = 1; i < c; ++i)
      k = gcd(k, f[i]);
    if (b)
      while (k && (k & 1) == 0)
        k >>= 1;
    printf("%d\n", c && n == 1 ? k : 1);
  }

  return 0;
}
