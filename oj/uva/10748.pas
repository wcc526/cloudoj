{ $R-,Q-,S-}
Const
    InFile     = 'p10748.in';
    OutFile    = 'p10748.out';
    Limit      = 50;
    LimitN     = 30;
    LimitSave  = 36000;
    LimitHash  = 1699997;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tvisited   = array[-Limit * 2..Limit * 2 , -Limit * 2..Limit * 2] of longint;
    Tcango     = array[0..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..LimitSave] of Tpoint;
                   end;
    THash      = array[0..LimitHash] of
                   record
                       signal : byte;
                       p      : Tpoint;
                   end;

Var
    hash       : Thash;
    cango      : Tcango;
    visited    : Tvisited;
    answer , T ,
    x0 , y0 ,
    i , K , j ,
    N , tot    : longint;

procedure dfs(x , y , step , p : longint);
var
    deltax , deltay ,
    i          : longint;
begin
    if visited[x , y] >= step then exit;
    if visited[x , y] = -1 then
      with cango[p] do
        begin
           inc(tot); data[tot].x := x; data[tot].y := y;
        end;
    visited[x , y] := step;
    if step = 0 then exit;
    for i := 1 to 2 do
      begin
          deltax := i; deltay := 3 - i;
          dfs(x + deltax , y + deltay , step - 1 , p);
          dfs(x + deltax , y - deltay , step - 1 , p);
          dfs(x - deltax , y + deltay , step - 1 , p);
          dfs(x - deltax , y - deltay , step - 1 , p);
      end;
end;

function compare(p1 , p2 : Tpoint) : longint;
begin
    if p1.x <> p2.x
      then exit(p1.x - p2.x)
      else exit(p1.y - p2.y);
end;

{procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (compare(data[stop] , key) > 0) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(data[start] , key) < 0) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;
 }
function add_hash(x , y : longint) : boolean;
var
    hashnum    : longint;
begin
    x := x mod LimitHash; y := y mod LimitHash;
    hashnum := (x + y * 1000) mod LimitHash;
    if hashnum < 0 then inc(hashnum , LimitHash);
    while hash[hashnum].signal = T do
      begin
          if (hash[hashnum].p.x = x) and (hash[hashnum].p.y = y) then
            exit(false);
          hashnum := hashnum + 1;
          if hashnum = LimitHash then hashnum := 0;
      end;
    hash[hashnum].p.x := x; hash[hashnum].p.y := y;
    hash[hashnum].signal := T;
    exit(true);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      fillchar(cango , sizeof(cango) , 0);
      for i := 0 to Limit do
        begin
            fillchar(visited , sizeof(visited) , $FF);
            dfs(0 , 0 , i , i);
        end;
      readln(N);
      fillchar(hash , sizeof(hash) , 0);
      T := 0;
      while N <> 0 do
        begin
            tot := 0;
            inc(T);
            answer := 0;
            for i := 1 to N do
              begin
                  read(x0 , y0 , K);
                  for j := 1 to cango[K].tot do
                    if add_hash(cango[K].data[j].x + x0 , cango[K].data[j].y + y0) then
                      inc(answer);
              end;
            writeln(answer);
            readln(N);
        end;
End.