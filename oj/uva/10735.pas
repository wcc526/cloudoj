Const
    InFile     = 'p10735.in';
    OutFile    = 'p10735.out';
    Limit      = 100;
    LimitEdge  = 500;
    LimitPoints= Limit + LimitEdge + 2;

Type
    Tedge      = record
                     p1 , p2  : longint;
                     del ,
                     directed : boolean;
                 end;
    Tdata      = array[1..LimitEdge] of Tedge;
    Tmap       = array[1..LimitPoints , 1..LimitPoints] of longint;
    Tdegree    = array[1..Limit] of longint;
    Tvisited   = array[1..LimitPoints] of boolean;

Var
    data       : Tdata;
    C          : Tmap;
    totdegree ,
    indegree   : Tdegree;
    visited    : Tvisited;
    maxflow ,
    S , T , test ,
    V , E      : longint;
    noanswer   : boolean;

procedure init;
var
    ch         : char;
    i          : longint;
begin
    readln(V , E);
    dec(Test);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to E do
      with data[i] do
        begin
            read(p1 , p2);
            repeat read(ch); until ch <> ' ';
            if ch = 'U' then directed := false else directed := true;
        end;
end;

function Build_Graph : boolean;
var
    i          : longint;
begin
    fillchar(C , sizeof(C) , 0);
    fillchar(totdegree , sizeof(totdegree) , 0);
    fillchar(indegree , sizeof(indegree) , 0);

    S := 1; T := V + E + 2;
    for i := 1 to E do
      begin
          if not data[i].directed
            then begin
                     C[S , i + 1] := 1;
                     C[i + 1 , data[i].p1 + E + 1] := 1;
                     C[i + 1 , data[i].p2 + E + 1] := 1;
                 end
            else inc(indegree[data[i].p2]);
          inc(totdegree[data[i].p1]); inc(totdegree[data[i].p2]);
      end;
    maxflow := 0;
    for i := 1 to V do
      if odd(totdegree[i]) or (indegree[i] * 2 > totdegree[i]) then
        exit(false);
    for i := 1 to V do
      begin
          C[i + E + 1 , T] := totdegree[i] div 2 - indegree[i];
          inc(maxflow , C[i + E + 1 , T]);
      end;
    exit(true);
end;

function dfs(p : longint) : boolean;
var
    i          : longint;
begin
    visited[p] := true;
    if p = T then exit(true);
    for i := 1 to T do
      if not visited[i] and (C[p , i] > 0) then
        if dfs(i) then
          begin
              dec(C[p , i]); inc(C[i , p]);
              exit(true);
          end;
    exit(false);
end;

procedure dfs_find(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    for i := 1 to E do
      begin
          if (data[i].p1 = p) and not visited[data[i].p2] then dfs_find(data[i].p2);
          if (data[i].p2 = p) and not visited[data[i].p1] then dfs_find(data[i].p1);
      end;
end;

procedure dfs_print(p : longint);
var
    i          : longint;
begin
    for i := 1 to E do
      if not data[i].del and (data[i].p2 = p) then
        begin
            data[i].del := true;
            dfs_print(data[i].p1);
        end;
    write(p , ' ');
end;

procedure work;
var
    i , tmp    : longint;
begin
    noanswer := true;
    if not Build_Graph then exit;
    for i := 1 to maxflow do
      begin
          fillchar(visited , sizeof(visited) , 0);
          if not dfs(S) then exit;
      end;
    for i := 1 to E do
      if not data[i].directed then
        if C[i + 1 , data[i].p1 + E + 1] = 0 then
          begin
              tmp := data[i].p1; data[i].p1 := data[i].p2; data[i].p2 := tmp;
          end;
    fillchar(visited , sizeof(visited) , 0);
    dfs_find(1);
    for i := 1 to V do if not visited[i] then exit;
    dfs_print(1);
    writeln;
    noanswer := false;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(Test);
    while Test > 0 do
      begin
          init;
          work;
          if noanswer then writeln('No euler circuit exist');
          if Test <> 0 then writeln;
      end;
End.