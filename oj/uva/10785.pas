Const
    vowel      : string[26] = 'AUEOI';
    consonant  : string[26] = 'JSBKTCLDMVNWFXGPYHQZR';

Var
    T , i , N ,
    j , p1 , p2 ,
    L1 , L2    : longint;
    tmp        : char;
    ans        : string[255];

Begin
    readln(T);
    for i := 1 to T do
      begin
          write('Case ' , i , ': ');
          readln(N);
          ans := '';
          p1 := 1; p2 := 1; L1 := 21; L2 := 5;
          for j := 1 to N do
            if odd(j)
              then begin
                       dec(L1); ans := ans + vowel[p1];
                       if L1 = 0 then begin L1 := 21; inc(p1); end;
                   end
              else begin
                       dec(L2); ans := ans + consonant[p2];
                       if L2 = 0 then begin L2 := 5; inc(p2); end;
                   end;
          for j := 1 to N do
            for p1 := 1 to N do
              for p2 := p1 + 1 to N do
                if not odd(p1 + p2) then
                  if ans[p1] > ans[p2] then
                    begin
                        tmp := ans[p1]; ans[p1] := ans[p2]; ans[p2] := tmp;
                    end;
          writeln(ans);
      end;
End.