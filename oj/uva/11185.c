#include <stdio.h>

int g(void)
{
  int c, r;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  for (; (c = getc(stdin)) >= '0'; r = r * 10 + c - '0');
  return r;
}

int main()
{
  char r[21];
  int n, v;
  while ((v = g()) >= 0) {
    r[n = 20] = 0;
    do
      r[--n] = v % 3 + '0';
    while (v /= 3);
    puts(r + n);
  }
  return 0;
}
