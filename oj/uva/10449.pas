{$I-}
Const
    InFile     = 'p10449.in';
    OutFile    = 'p10449.out';
    Limit      = 200;
    maximum    = 1e15;

Type
    Tdata      = array[1..Limit] of extended;
    Tmap       = array[1..Limit , 1..Limit] of boolean;

Var
    data ,
    shortest   : Tdata;
    map        : Tmap;
    count ,
    i , Q , p ,
    N          : longint;

function init : boolean;
var
    i , p1 , p2 ,
    M          : longint;
begin
    read(N);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do begin read(data[i]); shortest[i] := maximum; end;
    read(M);
    fillchar(map , sizeof(map) , 0);
    for i := 1 to M do
      begin
          read(p1 , p2);
          if (p1 <= N) and (p2 <= N) then
            map[p1 , p2] := true;
      end;
    readln;
end;

procedure work;
var
    i , j , k  : longint;
    tmp        : extended;
begin
    shortest[1] := 0;
    for i := 1 to N + 1 do
      for j := 1 to N do if shortest[j] < maximum then
        for k := 1 to N do
          if map[j , k] then
            begin
                tmp := shortest[j] + sqr(data[k] - data[j]) * (data[k] - data[j]);
                if tmp < shortest[k] then shortest[k] := tmp;
            end;
    for j := 1 to N do if shortest[j] < maximum then
      for k := 1 to N do
        if map[j , k] then
          begin
              tmp := shortest[j] + sqr(data[k] - data[j]) * (data[k] - data[j]);
              if tmp < shortest[k] then shortest[k] := -maximum;
          end;
    for i := 1 to N do
      for j := 1 to N do
        if shortest[j] = -maximum then
          for k := 1 to N do
            if map[j , k] then
              shortest[k] := -maximum;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    count := 0;
    while not eof do
      begin
          if not init then break;
          inc(Count);
          writeln('Set #' , count);
          work;
          readln(Q);
          for i := 1 to Q do
            begin
                readln(p);
                if shortest[p] >= maximum
                  then writeln('?')
                  else if shortest[p] < 3
                         then writeln('?')
                         else writeln(shortest[p] : 0 : 0);
            end;
      end;
End.