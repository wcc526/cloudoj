{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
{$R-,Q-,S-}
Const
    InFile     = 'p10628.in';
    OutFile    = 'p10628.out';
    H          = 8;
    W          = 12;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[0..H + 1 , 0..W + 1] of longint;
    Tcovered   = array[0..H + 1 , 0..W + 1] of boolean;
    Tblocks    = array[1..14] of Tpoint;
    Tvisited   = array[1..7 , 1..7] of boolean;
    Tbelong    = array[1..14] of longint;
    Tcorner    = array[0..H + 1 , 0..W + 1] of boolean;
    Tcorners   = array[1..14] of longint;

Var
    map        : Tdata;
    covered    : Tcovered;
    blocks     : Tblocks;      
    visited    : Tvisited;
    belong     : Tbelong;
    corner     : Tcorner;
    corners    : Tcorners;
    Cases ,
    answer     : longint;

procedure init;
var
    i , j      : longint;
    c          : char;
begin
    dec(Cases);
    while eoln do readln;
    for i := 1 to H do
      begin
          for j := 1 to W do
            begin
                read(c);
                map[i , j] := ord(c <> '.');
            end;
          readln;
      end;
end;

procedure Generate;
var
    i , j , tot: longint;
begin
    fillchar(corner , sizeof(corner) , 0);
    fillchar(corners , sizeof(corners) , 0);
    tot := 0;
    for i := 1 to H do
      for j := 1 to W do
        if map[i , j] = 1 then
          begin
              inc(tot);
              blocks[tot].x := i; blocks[tot].y := j;
              map[i , j] := 0; map[i , j + 1] := 0;
              map[i + 1 , j] := 0; map[i + 1 , j + 1] := 0;
          end;
    for i := 1 to 14 do
      with blocks[i] do
        begin
            map[x , y] := i; map[x + 1 , y] := i;
            map[x , y + 1] := i; map[x + 1 , y + 1] := i;
        end;
    for i := 1 to H do
      for j := 1 to W do
        if map[i , j] <> 0 then
          begin
              tot := 0;
              if map[i , j + 1] = 0 then inc(tot);
              if map[i + 1 , j] = 0 then inc(tot);
              if map[i , j - 1] = 0 then inc(tot);
              if map[i - 1 , j] = 0 then inc(tot);
              if tot >= 2 then corner[i , j] := true;
              if corner[i , j] then inc(corners[map[i , j]]);
          end;
end;

procedure make_ans(x , y : longint);
var
    nx , ny ,
    p1 , p2    : longint;
begin
    if y = W
      then begin nx := x + 1; ny := 1; end
      else begin nx := x; ny := y + 1; end;
    if x > H
      then inc(answer)
      else if (map[x , y] = 0) or covered[x , y]
             then make_ans(nx , ny)
             else begin
                      p1 := belong[map[x , y]];
                      if (y < W) and (map[x , y + 1] <> 0) and not covered[x , y + 1] then
                        begin
                            p2 := belong[map[x , y + 1]];
                            if not visited[p1 , p2] then
                              begin
                                  visited[p1 , p2] := true; visited[p2 , p1] := true;
                                  covered[x , y] := true; covered[x , y + 1] := true;
                                  make_ans(nx , ny);
                                  covered[x , y] := false; covered[x , y + 1] := false;
                                  visited[p1 , p2] := false; visited[p2 , p1] := false;
                              end;
                        end;
                      if (x < H) and (map[x + 1 , y] <> 0) and not covered[x + 1 , y] then
                        begin
                            p2 := belong[map[x + 1 , y]];
                            if not visited[p1 , p2] then
                              begin
                                  visited[p1 , p2] := true; visited[p2 , p1] := true;
                                  covered[x , y] := true; covered[x + 1 , y] := true;
                                  make_ans(nx , ny);
                                  covered[x , y] := false; covered[x + 1 , y] := false;
                                  visited[p1 , p2] := false; visited[p2 , p1] := false;
                              end;
                        end;
                  end;
end;

procedure dfs(p , number : longint);
var
    i          : longint;
begin
    if number > 7
      then make_ans(1 , 1) 
      else if belong[p] = 0
             then begin
                      belong[p] := number;
                      for i := p + 1 to 14 do
                        if belong[i] = 0 then
                          begin
                              belong[i] := number;
                              if (corners[i] <= 2) and (corners[p] <= 2) and ((corners[i] = 0) or (corners[p] = 0))
                                then dfs(p , number + 1);
                              belong[i] := 0;
                          end;
                      belong[p] := 0;
                  end
             else dfs(p + 1 , number);
end;

procedure work;
begin
    Generate;

    fillchar(belong , sizeof(belong) , 0);
    fillchar(covered , sizeof(covered) , 0);
    fillchar(visited , sizeof(visited) , 0);
    answer := 0;
    dfs(1 , 1);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
