#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int f;
  unsigned char v;
} frequency;

void sort(frequency *c)
{
  frequency t;
  int i, j;
  for (j = 2; j <= 26; ++j) {
    t = c[j];
    i = j - 1;
    while (i > 0 && c[i].f < t.f) {
      c[i + 1] = c[i];
      --i;
    }
    c[i + 1] = t;
  }
}

int main(void)
{
  unsigned char *b = 0, p = 0, m[256];
  int l, n, s;
  frequency k[27], e[27], *c = k;

  memset(k, 0, sizeof k);
  memset(e, 0, sizeof e);

  for (n = 1; n <= 26; ++n)
    k[n].v = e[n].v = n | 0x40;

  for (n = 256; n--;)
    m[n] = n;

  n = -1;
  do
    b = realloc(b, (++n + 1) << 10);
  while ((l = fread(b + (n << 10), 1, 1024, stdin)) == 1024);
  l += (n << 10);

  for (n = 0; n < l; ++n) {
    if (p == '\n' && b[n] == '\n' && c == k) {
      c = e;
      s = n;
    }
    p = b[n];
    if (isalpha(p))
      ++c[p & 0x9F].f;
  }

  sort(k);
  sort(e);

  for (n = 1; n <= 26; ++n) {
    m[e[n].v] = k[n].v;
    m[e[n].v | 0x20] = k[n].v | 0x20;
  }

  for (n = s; ++n < l;)
    putc(m[b[n]], stdout);

  free(b);

  return 0;
}
