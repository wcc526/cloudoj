Const
    Limit      = 100;

Type
    Tdata      = array[1..Limit , 1..Limit] of boolean;
    Tdegree    = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    degree     : Tdegree;
    N          : longint;

function init : boolean;
var
    i , p1 , p2 ,
    M          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    read(N , M);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to M do
      begin
          read(p1 , p2);
          data[p1 , p2] := true;
      end;
end;

procedure workout;
var
    i , j , min
               : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(degree , sizeof(degree) , 0);
    for i := 1 to N do
      for j := 1 to N do
        if data[i , j] then
          inc(degree[j]);
    for i := 1 to N do
      begin
          min := 0;
          for j := 1 to N do
            if not visited[j] and (degree[j] = 0) then
              begin min := j; break; end;
          write(min , ' ');
          visited[min] := true;
          for j := 1 to N do
            if not visited[j] and data[min , j] then
              dec(degree[j]);
      end;
    writeln;
end;

Begin
    while init do
      workout;
End.