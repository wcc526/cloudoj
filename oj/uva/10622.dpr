{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10622.in';
    OutFile    = 'p10622.out';
    minimum    = 1e-6;

Var
    N , tmp ,
    num        : extended;
    i , j      : longint;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N <> 0 do
        begin
            for i := 35 downto 1 do
              begin
                  tmp := round(exp(ln(abs(N)) / i));
                  if odd(i) and (N < 0) then tmp := -tmp;
                  num := 1;
                  for j := 1 to i do
                    num := num * tmp;
                  if abs(num - N) <= minimum then
                    begin
                        writeln(i);
                        break;
                    end;
              end;
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
