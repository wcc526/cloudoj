{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10602.in';
    OutFile    = 'p10602.out';
    Limit      = 100;
    LimitLen   = 200;

Type
    Tstr       = string[LimitLen];
    Tdata      = array[1..Limit] of Tstr;
    Tcommon    = array[1..Limit] of longint;
    Tmin       = array[1..Limit , 1..Limit] of longint;
    Topt       = array[1..Limit , 1..Limit] of
                   record
                       opt , strategy        : longint;
                   end;

Var
    data       : Tdata;
    min        : Tmin;
    common     : Tcommon;
    opt        : Topt;
    N , T , fp : longint;

procedure init;
var
    i , p      : longint;
    s , first  : Tstr;
begin
    dec(T);
    fillchar(min , sizeof(min) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    fillchar(data , sizeof(data) , 0);
    fillchar(common , sizeof(common) , 0);
    readln(N);
    for i := 1 to N do
      begin
          readln(s);
          if i = 1 then first := s;
          p := i;
          while (p > 1) and (data[p - 1] > s) do
            begin data[p] := data[p - 1]; dec(p); end;
          data[p] := s;
      end;
    fp := 1;
    while data[fp] <> first do inc(fp);
end;

procedure better(var p , strategy : longint; n1 , n2 , nstrategy : longint);
begin
    if (n1 <> -1) and (n2 <> -1) then
      if (p = -1) or (p > n1 + n2) then
        begin
            p := n1 + n2;
            strategy := nstrategy;
        end;
end;

procedure work;
var
    i , j      : longint;
begin
    for i := 1 to N - 1 do
      while (common[i] < length(data[i])) and (common[i] < length(data[i + 1]))
        and (data[i , common[i] + 1] = data[i + 1 , common[i] + 1]) do inc(common[i]);
    opt[fp , fp].opt := length(data[fp]);
    for i := 1 to N - 1 do
      begin
          min[i , i] := common[i];
          for j := i + 1 to N - 1 do
            if min[i , j - 1] < common[j]
              then min[i , j] := min[i , j - 1]
              else min[i , j] := common[j];
      end;

    for i := fp downto 1 do
      for j := fp to N do
        if (i <> fp) or (j <> fp) then
          begin
              better(opt[i , j].opt , opt[i , j].strategy , opt[j - 1 , i].opt , length(data[j]) - min[i , j - 1] , 1);
              better(opt[i , j].opt , opt[i , j].strategy , opt[i , j - 1].opt , length(data[j]) - min[j - 1 , j - 1] , 2);

              better(opt[j , i].opt , opt[j , i].strategy , opt[j , i + 1].opt , length(data[i]) - min[i , i] , 1);
              better(opt[j , i].opt , opt[j , i].strategy , opt[i + 1 , j].opt , length(data[i]) - min[i , j - 1] , 2);
          end;
end;

procedure print(i , j : longint);
begin
    if (i <> fp) or (j <> fp)
      then if i < j
             then if opt[i , j].strategy = 1
                    then print(j - 1 , i)
                    else print(i , j - 1)
             else if opt[i , j].strategy = 1
                    then print(i , j + 1)
                    else print(j + 1 , i);
    writeln(data[j]);
end;

procedure out;
var
    i , j      : longint;
begin
    if (opt[1 , N].opt <> -1) and ((opt[N , 1].opt = -1) or (opt[1 , N].opt < opt[N , 1].opt))
      then begin i := 1; j := N; end
      else begin i := N; j := 1; end;
    writeln(opt[i , j].opt);
    print(i , j);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
