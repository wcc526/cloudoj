Const
    InFile     = 'p10379.in';
    Limit      = 100;

Type
    Topt       = array[0..Limit] of double;
    Tstrategy  = array[1..Limit] of longint;

Var
    opt ,
    fuel , time: Topt;
    strategy   : Tstrategy;
    First , count ,
    Laps       : longint;
    T0 , deltaT ,
    C0 , deltaC ,
    spend , extra ,
    answer
               : double;

procedure init;
begin
    readln(Laps , T0 , deltaT , C0 , deltaC , spend , extra);
end;

procedure work;
var
    i , j      : longint;
    tmp        : double;
begin
    fillchar(opt , sizeof(opt) , 0);
    fillchar(fuel , sizeof(fuel) , 0);
    fillchar(time , sizeof(time) , 0);
    for i := 1 to Laps do
      begin
          fuel[i] := fuel[i - 1] + (C0 + fuel[i - 1] * deltaC) / (1 - deltaC);
          time[i] := time[i - 1] + T0 + fuel[i] * deltaT;
      end;
    for i := 1 to Laps do
      begin
          opt[i] := 1e50;
          for j := i downto 1 do
            begin
                tmp := time[j] + spend + extra * fuel[j] + opt[i - j];
                if tmp < opt[i] then
                  begin
                      opt[i] := tmp;
                      strategy[i] := j;
                  end;
            end;
      end;
    answer := 1e50;
    for i := Laps downto 1 do
      begin
          tmp := time[i] + opt[Laps - i];
          if tmp < answer then
            begin
                First := i;
                answer := tmp;
            end;
      end;
    i := Laps - First; count := 0;
    while i > 0 do
      begin
          inc(Count); dec(i , strategy[i]);
      end;
    writeln(Laps , ' ' , T0 : 0 : 3 , ' ' , deltaT : 0 : 3 , ' ' ,
           C0 : 0 : 3 , ' ' , deltaC : 0 : 3 , ' ' ,
           spend : 0 : 3 , ' ' , extra : 0 : 3);
    writeln(answer : 0 : 3 , ' ' , fuel[First] : 0 : 3 , ' ' , count);
    i := Laps - First;
    while i > 0 do
      begin
          writeln(Laps - i , ' ' , fuel[strategy[i]] : 0 : 3);
          dec(i , strategy[i]);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
        end;
//    Close(INPUT);
End.