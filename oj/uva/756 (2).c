#include <stdio.h>

int main()
{
  int c = 0, p, e, i, d, r;

  while (scanf("%d %d %d %d", &p, &e, &i, &d) == 4 &&
         p > -1 && e > -1 && i > -1 && d > -1) {
    p %= 23, e %= 28, i %= 33;
    p = p * 5544 % 21252;
    e = 21252 - e * 6831 % 21252;
    i = i * 1288 % 21252;
    r = (p + e + i) % 21252 - d;
    printf("Case %d: the next triple peak occurs in %d days.\n",
           ++c, r > 0 ? r : r + 21252);
  }

  return 0;
}
