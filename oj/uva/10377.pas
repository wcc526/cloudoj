Const
    InFile     = 'p10377.in';
    OutFile    = 'p10377.out';
    Limit      = 100;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , 1 , 0 , -1);

Type
    Tdata      = array[1..Limit , 1..Limit] of char;

Var
    data       : Tdata;
    cases ,
    D , x , y ,
    N , M      : longint;
    ch         : char;

procedure init;
var
    i , j      : longint;
begin
    readln(N , M);
    for i := 1 to N do
      begin
          for j := 1 to M do
            read(data[i , j]);
          readln;
      end;
end;

procedure go;
var
    nx , ny    : longint;
begin
    nx := x + dirx[D]; ny := y + diry[D];
    if data[nx , ny] = ' ' then begin x := nx; y := ny; end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          init;
          read(x , y); D := 1;
          while true do
            begin
                read(ch);
                if ch = 'R' then D := D mod 4 + 1;
                if ch = 'L' then D := (D + 2) mod 4 + 1;
                if ch = 'F' then go;
                if ch = 'Q' then break;
            end;
          readln;
          write(x , ' ' , y , ' ');
          case D of
            1  : writeln('N');
            2  : writeln('E');
            3  : writeln('S');
            4  : writeln('W');
          end;
          if cases > 0 then writeln;
      end;
End.