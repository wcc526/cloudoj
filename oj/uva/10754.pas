Const
    InFile     = 'p10754.in';
    OutFile    = 'p10754.out';
    Limit      = 26;

Type
    Tmatrix    = array[1..Limit , 1..Limit] of extended;

Var
    A , source : Tmatrix;
    K , T      : longint;
    M , N      : extended;

function _MOD(a , b : extended) : extended;
var
    res        : extended;
begin
    res := a - int(a / b) * b;
    if res < 0 then res := res + b;
    exit(res);
end;

procedure init;
var
    i          : longint;
begin
    dec(T);
    fillchar(A , sizeof(A) , 0);
    fillchar(source , sizeof(source) , 0);
    readln(K , M , N);
    A[1 , 1] := 1;
    for i := 2 to K do A[i + 1 , i] := 1;
    for i := K + 1 downto 1 do
      begin
          read(A[i , K + 1]);
          A[i , K + 1] := _MOD(A[i , K + 1] , M);
      end;
    for i := 1 to K do
      begin
          read(source[1 , i + 1]);
          source[1 , i + 1] := _MOD(source[1 , i + 1] , M);
      end;
    source[1 , 1] := 1;
end;

procedure times(A , B : Tmatrix; var C : Tmatrix);
var
    i , j , p  : longint;
begin
    fillchar(C , sizeof(C) , 0);
    for i := 1 to K + 1 do
      for j := 1 to K + 1 do
        for p := 1 to K + 1 do
          C[i , j] := _MOD(c[i , j] + A[i , p] * B[p , j] , M);
end;

procedure workout;
var
    power      : Tmatrix;
begin
    N := N + 1;
    if N <= K then begin writeln(source[1 , trunc(N) + 1] : 0 : 0); exit; end;
    power := A;
    N := N - K;
    while N <> 0 do
      begin
          if int(N / 2) * 2 <> N
            then times(source , power , source);
          times(power , power , power);
          N := int(N / 2);
      end;
    writeln(source[1 , K + 1] : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            workout;
            if T <> 0 then writeln;
        end;
End.
