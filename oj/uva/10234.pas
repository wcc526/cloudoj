Const
    InFile     = 'p10234.in';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of char;
    Tsorted    = array[1..Limit] of longint;

Var
    data       : Tdata;
    times ,
    sorted     : Tsorted;
    N , M ,
    Len , max  : longint;

function lowcase(ch : char) : char;
begin
    if (ch >= 'A') and (ch <= 'Z')
      then lowcase := chr(ord(ch) - ord('A') + ord('a'))
      else lowcase := ch;
end;

procedure init;
begin
    Len := 0;
    while not eoln do
      begin inc(Len); read(data[Len]); data[Len] := lowcase(data[Len]); end;
    readln;
end;

function compare(i , j : longint) : longint;
var
    k          : longint;
begin
    for k := 0 to M - 1 do
      if data[i + k] <> data[j + k]
        then exit(ord(data[i + k]) - ord(data[j + k]));
    exit(0);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := sorted[tmp]; sorted[tmp] := sorted[start];
    while start < stop do
      begin
          while (start < stop) and (compare(sorted[stop] , key) > 0) do dec(stop);
          sorted[start] := sorted[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(sorted[start] , key) < 0) do inc(start);
          sorted[stop] := sorted[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    sorted[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure out;
var
    i          : longint;
begin
    write(times[max] , ' ');
    for i := 0 to M - 1 do
      write(data[sorted[max] + i]);
    writeln;
end;

procedure workout;
var
    i , j , count
               : longint;
begin
    readln(N);
    for count := 1 to N do
      begin
          readln(M);
          for j := 1 to Len - M + 1 do sorted[j] := j;
          qk_sort(1 , Len - M + 1);
          i := 1; j := 2; times[i] := 1;
          for j := 2 to Len - M + 1 do
            if compare(sorted[i] , sorted[j]) = 0
              then inc(times[i])
              else begin inc(i); sorted[i] := sorted[j]; times[i] := 1; end;
          max := 0;
          for j := 1 to i do
            if (max = 0) or (times[j] > times[max]) then
              max := j;
          out;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(INPUT);
End.