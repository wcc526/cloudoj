#include <cstdio>
int pow2[32]={1};
int main()
{
	int n;
	//freopen("in.txt","r",stdin);
	int kase=1;
	for(int i=1;i<32;++i)
	{
		pow2[i]=pow2[i-1]*2;
	}
	while(~scanf("%d",&n))
	{
		if(n<0) break;
		int ans;
		for(ans=0;ans<32;++ans)
		{
			if(pow2[ans]>=n) 
				break;
		}
		if(pow2[ans]<n)
			++ans;
		printf("Case %d: %d\n",kase++,ans);
	}
	return 0;
}
