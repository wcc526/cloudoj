Const
    InFile     = 'p10713.in';
    OutFile    = 'p10713.out';
    minimum    = 1e-8;
    name       : array[0..7] of string
               = ('east' , 'northeast' , 'north' , 'northwest' ,
                  'west' , 'southwest' , 'south' , 'southeast');

Type
     Tpoint    = record
                     x , y    : extended;
                 end;

Var
     R         : extended;
     now , target
               : Tpoint;
     first     : boolean;

function init : boolean;
begin
    read(R);
    if R < 0
      then exit(false)
      else begin
               readln(now.x , now.y , target.x , target.y);
               exit(true);
           end;
end;

function Get_Fastest(p : Tpoint) : extended;
var
    delta ,
    A , B , C ,
    res        : extended;
begin
    A := sqr(p.x) + sqr(p.y);
    B := 2 * (now.x * p.x + now.y * p.y);
    C := sqr(now.x) + sqr(now.y) - sqr(R);
    delta := sqrt(abs(B * B - 4 * A * C));
    res := (-B + delta) / 2 / A;
    exit(res);
end;

procedure print(n1 , n2 : longint; p1 , p2 : Tpoint);
var
    tmp ,
    k1 , k2    : extended;
begin
    tmp := p1.x * p2.y - p1.y * p2.x;
    k1 := (target.x * p2.y - target.y * p2.x) / tmp;
    k2 := (target.y * p1.x - target.x * p1.y) / tmp;
    while k1 + k2 > minimum * 2 do
      begin
          tmp := Get_Fastest(p1);
          if (tmp + minimum >= k1) and (k1 >= minimum) then
            begin
                writeln(name[n1] , ' ' , k1 : 0 : 10);
                now.x := now.x + k1 * p1.x; now.y := now.y + k1 * p1.y;
                k1 := 0;
            end;

          tmp := Get_Fastest(p2);
          if (tmp + minimum >= k2) and (k2 >= minimum) then
            begin
                writeln(name[n2] , ' ' , k2 : 0 : 10);
                now.x := now.x + k2 * p2.x; now.y := now.y + k2 * p2.y;
                k2 := 0;
            end;
      end;
end;

function dot(p1 , p2 : Tpoint) : extended;
begin
    exit(p1.x * p2.x + p1.y * p2.y);
end;

function cross(p1 , p2 : Tpoint) : extended;
begin
    exit(p1.x * p2.y - p1.y * p2.x);
end;

procedure work;
var
    i          : longint;
    p1 , p2    : Tpoint;
begin
    target.x := target.x - now.x; target.y := target.y - now.y;
    for i := 0 to 7 do
      begin
          p1.x := cos(i / 4 * pi); p1.y := sin(i / 4 * pi);
          p2.x := cos((i + 1) / 4 * pi); p2.y := sin((i + 1) / 4 * pi);
          if (dot(p1 , target) > 0) and (dot(p2 , target) > 0) then
            if cross(p1 , target) * cross(target , p2) >= -minimum then
              begin
                  print(i , (i + 1) mod 8 , p1 , p2);
                  exit;
              end;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
    first := true;
    while init do
      begin
          if first then first := false else writeln;
          work;
      end;
//    Close(OUTPUT);
End.
