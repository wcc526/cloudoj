{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
Var
    T , N      : extended;
    A          : longint;
    first      : boolean;

procedure print(num : extended);
begin
    if num <> 0 then
      begin
          print(int(num / 10));
          write(num - int(num / 10) * 10 : 0 : 0);
      end;
end;
          
Begin
    readln(T);
    while T <> 0 do
      begin
          first := true;
          for A := 9 downto 0 do
            if int((10 * T - A) / 9) * 9 = 10 * T - A
              then begin
                       N := (10 * T - A) / 9;
                       if not first then write(' ');
                       first := false;
                       print(N);
                   end;
          writeln;
          readln(T);
      end;
End.