#include <stdio.h>

int main(void)
{
  int m, s, k;

  while (scanf("%d %d", &s, &m) == 2) {
    printf("%10d%10d    ", s, m);
    k = 0;
    while ((m & 1) == 0 && (s & 1) == 0) {
      m >>= 1;
      s >>= 1;
      k += 1;
    }
    do {
      if ((m & 1) == 0)
        m >>= 1;
      else if ((s & 1) == 0)
        s >>= 1;
      else if (m >= s)
        m = (m - s) >> 1;
      else
        s = (s - m) >> 1;
    } while (m > 0);
    printf("%s Choice\n\n", s << k == 1 ? "Good" : "Bad");
  }

  return 0;
}
