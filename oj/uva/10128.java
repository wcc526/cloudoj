import java.util.Scanner;

public class Main {

	public static void main(String[] args)  {
		Scanner s = new Scanner(System.in);
		
		calculate();
		
		int cases = s.nextInt();
		for (int i = 0; i < cases; i++) {
			int n = s.nextInt();
			int j = s.nextInt();
			int l = s.nextInt();

			
			int x = 0;
			
			if ( j <= 13 && l <= 13 ) {
				x=a[n][j][l];
			}
			
			System.out.println(x);
		}
		
	}

	static int[][][] a = new int[14][14][14];

	static void calculate() {
		a[1][1][1] = 1;

		for (int i = 2; i <= 13; i++) {

			for (int j = 1; j <= i; j++) {

				int max_r = i - j + 1;

				for (int k = 1; k <= max_r; k++) {
					a[i][j][k] = a[i - 1][j - 1][k] + a[i - 1][j][k - 1] + (i - 2) * a[i - 1][j][k];
				}

			}
		}
	}
}

