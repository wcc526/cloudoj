#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;

const int oo = 2147483647;
const int maxn = 1010;

int Ti[maxn], Mi[maxn][maxn];
bool kill[maxn];

int gcd(int a, int b) {
    int r;

    while (b != 0) {
        r = a % b;
        a = b;
        b = r;
    }

    return a;
}

int lcm(int a, int b) {
    return a * b / gcd(a, b);
}

int countLcm(int N) {
    int ret = 1;

    for (int i = 0; i < N; i ++) {
        if (!kill[i]) {
            ret = lcm(ret, Ti[i]);
        }
    }

    return ret;
}

int main() {
    //freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);
   
    int T, N, C, D;
    int tot, id, Min, day;
    bool flag, loop;

    scanf("%d", &T);

    while (T --) {
        scanf("%d", &N);

        for (int i = 0; i < N; i ++) {
            scanf("%d", &Ti[i]);
            for (int j = 0; j < Ti[i]; j ++)
                scanf("%d", &Mi[i][j]);
        }

        memset(kill, false, sizeof(kill));

        C = N; D = -1; tot = 0; loop = true;
        while (loop) {
            day = countLcm(N);
           
            loop = false;
            for (int d = 0; d < day; d ++) {
                Min = oo; flag = false;
                for (int i = 0; i < N; i ++) {
                    if (!kill[i]) {
                        if (Min > Mi[i][d % Ti[i]]) {
                            flag = true;
                            Min = Mi[i][d % Ti[i]];
                            id = i;
                        }
                        else if (Min == Mi[i][d % Ti[i]]) flag = false;
                    }
                }

                if (flag) {
                    C --;
                    D = tot + d;
                    kill[id] = true;
                    loop = true;
                }
            }

            tot += day;
        }

        printf("%d %d\n", C, D + 1);
    }

    return 0;
}