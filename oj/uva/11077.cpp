/*
 * uva_11077.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
#include <cstdio>
#include <cstring>
const int MXN=30;
unsigned long long f[MXN][MXN];
int main()
{
	//freopen("in.txt","r",stdin);
	memset(f,0,sizeof(f));
	f[1][0]=1;
	for(int i=2;i<=21;++i)
		for(int j=0;j<i;++j)
		{
			f[i][j]=f[i-1][j];
			if(j>0) f[i][j]+=f[i-1][j-1]*(i-1);
		}
	int n,k;
	while(~scanf("%d%d",&n,&k)&&n)
		printf("%llu\n",f[n][k]);
	return 0;
}






