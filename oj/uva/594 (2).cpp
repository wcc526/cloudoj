/**
 * UVa 594 One Little, Two Little, Three Little Endians
 * Author: chchwy
 * Last Modified: 2011.03.29
 * Blog: http://chchwy.blogspot.com
 */
#include<cstdio>
using namespace std;

int to_big(int little) {

    char * p = (char*) &little;	
    swap(p[0],p[3]);
    swap(p[1],p[2]);
    return little;
	/*
	int big;
	char * plittle = (char*) little;
	char * pbig = (char*) big;
	pbig[0] = plittle[3];
	pbig[1] = plittle[2];
	pbig[2] = plittle[1];
	pbig[3] = plittle[0];
	return big;
	*/
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("594.in","r",stdin);
#endif
    int little;
    while(scanf("%d", &little)==1) {
        printf("%d converts to %d\n", little, to_big(little));
    }
    return 0;
}
