Const
    minimum    = 1e-6;

Var
    answer ,
    A , B , C  : extended;
    T          : longint;

procedure process(a , b , c : extended);
var
    cosA ,
    p , area ,
    h , x      : extended;
begin
    if (sqr(b) > sqr(a) + sqr(c) + minimum) or (sqr(c) > sqr(a) + sqr(b) + minimum)
      then begin
               if b > c then begin x := b; b := c; c := x; end;
               cosA := (sqr(c) + sqr(a) - sqr(b)) / 2 / a / c;
               x := a / cosA * sqrt(abs(1 - sqr(cosA)));
               process(a , a / cosA , x);
           end
      else begin
               p := (a + b + c) / 2;
               area := sqrt(p * (p - a) * (p - b) * (p - c));
               h := area * 2 / a;
               x := a * h / (a + h);
               if x * x > answer then answer := x * x;
           end;
end;

Begin
    readln(T);
    while T > 0 do
      begin
          dec(T); readln(A , B , C);
          answer := 0;
          process(A , B , C);
          process(B , C , A);
          process(C , B , A);
          writeln(answer : 0 : 6);
      end;
End.