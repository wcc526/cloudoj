#include <stdio.h>

int main()
{
  int n, t;

  for (; scanf("%d", &n) == 1 && n; printf("%d\n", t))
    for (t = n; t > 9; t = n)
      for (n = 0; t; n += t % 10, t /= 10);

  return 0;
}
