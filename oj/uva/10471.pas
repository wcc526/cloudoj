Const
    InFile     = 'p10471.in';
    OutFile    = 'p10471.out';

Var
    i ,
    N , count  : longint;

procedure dfs_print(step , father : longint);
var
    now , i    : longint;
begin
    inc(count);
    now := count;
    if father <> 0 then writeln(father , ' ' , now);
    for i := 0 to step - 1 do
      dfs_print(i , now);
end;

Begin
    while not eof do
      begin
          readln(N);
          writeln(1 shl (N - 1) , ' ' , 1 shl (N - 1) - 1);
          count := 0;
          dfs_print(N - 1 , 0);
          for i := 1 shl (N - 1) downto 1 do
            write(i , ' ');
          writeln;
      end;
End.