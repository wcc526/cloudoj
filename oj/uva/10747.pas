Const
    InFile     = 'p10747.in';
    OutFile    = 'p10747.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    answer ,
    N , K      : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(N , K);
    if (N + K) = 0 then exit(false);
    for i := 1 to N do read(data[i]);
    exit(true);
end;

function compare_abs(p1 , p2 : longint) : longint;
begin
    if abs(p1) <> abs(p2)
      then exit(abs(p1) - abs(p2))
      else exit(p1 - p2);
end;

procedure qk_pass_by_abs(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (compare_abs(data[stop] , key) > 0) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare_abs(data[start] , key) < 0) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort_by_abs(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_by_abs(start , stop , mid);
          qk_sort_by_abs(start , mid - 1);
          qk_sort_by_abs(mid + 1 , stop);
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i , count ,
    p1 , p2 ,
    q1 , q2    : longint;
    zeroans    : boolean;
begin
    qk_sort_by_abs(1 , N);
    if (data[N - K + 1] = 0) then
      begin
          qk_sort(1 , N);
          answer := 0;
          for i := N - K + 1 to N do inc(answer , data[i]);
          exit;
      end;
    count := 0;
    for i := N - K + 1 to N do
      if data[i] < 0 then
        inc(count);
    if odd(count) then
      begin
          p1 := 0;
          for i := N - K + 1 to N do if data[i] > 0 then begin p1 := i; break; end;
          p2 := 0;
          for i := N - K + 1 to N do if data[i] < 0 then begin p2 := i; break; end;
          q1 := 0;
          for i := N - K downto 1 do if data[i] < 0 then begin q1 := i; break; end;
          q2 := 0;
          for i := N - K downto 1 do if data[i] > 0 then begin q2 := i; break; end;
          if (p1 = 0) or (q1 = 0)
            then begin p1 := p2; q1 := q2; end
            else if (p2 <> 0) and (q2 <> 0) then
                   if abs(data[p1] * data[q2]) >= abs(data[p2] * data[q1]) then
                     begin p1 := p2; q1 := q2; end;
      end
    else
      begin
          answer := 0;
          for i := N - K + 1 to N do inc(answer , data[i]);
          exit;
      end;
    if (data[N - K + 1] = 0) or (p1 = 0) or (q1 = 0) then
      begin
          qk_sort(1 , N);
          answer := 0;
          for i := N - K + 1 to N do inc(answer , data[i]);
          exit;
      end
    else
      begin
          answer := 0;
          for i := N - K + 1 to N do inc(answer , data[i]);
          dec(answer , data[p1]); inc(answer , data[q1]);
          exit;
      end;
end;

procedure out;
begin
    writeln(answer)
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          out;
      end;
End.