{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10653.in';
    OutFile    = 'p10653.out';
    Limit      = 1000;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tshortest  = array[1..Limit , 1..Limit] of longint;
    Tqueue     = array[1..Limit * Limit] of
                   record
                       x , y  : longint;
                   end;

Var
    map        : Tmap;
    shortest   : Tshortest;
    queue      : Tqueue;
    x0 , y0 ,
    x1 , y1 , 
    R , C ,
    answer     : longint;

function init : boolean;
var
    tot , sum , j , 
    i , p , q  : longint;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(shortest , sizeof(shortest) , $FF);
    fillchar(queue , sizeof(queue) , 0);
    readln(R , C);
    if (R = 0) and (C = 0) then begin init := false; exit; end;
    init := true;
    readln(tot);
    for i := 1 to tot do
      begin
          read(p); inc(p);
          read(sum);
          for j := 1 to sum do
            begin
                read(q); inc(q);
                map[p , q] := true;
            end;
      end;
    readln(x0 , y0 , x1 , y1);
    inc(x0); inc(y0); inc(x1); inc(y1);
end;

procedure work;
var
    open , closed ,
    x , y , nx ,
    ny , i     : longint;
begin
    open := 1; closed := 1; queue[1].x := x0; queue[1].y := y0;
    shortest[x0 , y0] := 0;
    while open <= closed do
      begin
          if shortest[x1 , y1] <> -1 then break;
          x := queue[open].x; y := queue[open].y;
          for i := 1 to 4 do
            begin
                nx := x + dirx[i]; ny := y + diry[i];
                if (nx <= R) and (ny <= C) and (nx >= 1) and (ny >= 1) then
                  if not map[nx , ny] and (shortest[nx , ny] = -1) then
                    begin
                        shortest[nx , ny] := shortest[x , y] + 1;
                        inc(closed);
                        queue[closed].x := nx; queue[closed].y := ny;
                    end;
            end;
          inc(open);
      end;
    answer := shortest[x1 , y1];
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
