{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10569.in';
    OutFile    = 'p10569.out';

Var
    nowCase ,
    totCase ,
    N          : longint;

procedure print(num , times : longint);
var
    i          : longint;
begin
    write(num);
    for i := 1 to times do write('0');
end;

procedure main;
var
    i , 
    times      : longint;
begin
    read(totCase); nowCase := 1;
    while nowCase <= totCase do
      begin
          read(N);
          write('Case ' , nowCase , ':');
          if N = 1
            then write(' 1 1')
            else if N = 2
                   then write(' -1 0 0')
                   else begin
                            times := (N - 1) div 2;
                            write(' '); print(7 , times); 
                            if odd(N)
                              then write(' 7 54 57')
                              else write(' 11 16 48 61');
                            for i := 1 to times - 1 do
                              begin
                                  write(' '); print(54 , i);
                                  write(' '); print(57 , i);
                              end;
                        end;
          writeln;
          inc(nowCase);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      main;
//    Close(OUTPUT);
//    Close(INPUT);
End.
