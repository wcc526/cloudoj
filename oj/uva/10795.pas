Const
    InFile     = 'p10795.in';
    OutFile    = 'p10795.out';
    Limit      = 60;

Type
    Tdata      = array[1..Limit] of longint;
    Tpower     = array[0..Limit] of extended;

Var
    A , B      : Tdata;
    power      : Tpower;
    answer     : extended;
    N          : longint;
    Cases      : longint;

function init : boolean;
var
    i          : longint;
begin
    inc(Cases);
    read(N);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do read(A[i]);
    for i := 1 to N do read(B[i]);
end;

procedure work;
var
    i , p1 , p2: longint;
begin
    power[0] := 1;
    for i := 1 to Limit do power[i] := power[i - 1] * 2;
    answer := 0;
    i := N;
    while i > 0 do
      if A[i] = B[i] then
        dec(i)
      else
        break;
    if i = 0 then exit;
    answer := answer + 1;
    p1 := 6 - A[i] - B[i]; p2 := p1;
    dec(i); if i = 0 then exit;
    while i > 0 do
      begin
          if A[i] <> p1
            then begin
                     answer := answer + power[i - 1];
                     p1 := 6 - p1 - A[i];
                 end;
          if B[i] <> p2
            then begin
                     answer := answer + power[i - 1];
                     p2 := 6 - p2 - B[i];
                 end;
          dec(i);
      end;
end;

procedure print(num : extended);
begin
    if num <> 0 then
      begin
          print(int(num / 10));
          write(num - int(num / 10) * 10 : 0 : 0);
      end;
end;

procedure out;
begin
    write('Case ' , cases , ': ');
    print(answer);
    if answer = 0 then write(0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      Cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.