{$R-,Q-,S-,I-}
Const
    InFile     = 'p10498.in';
    OutFile    = 'p10498.out';
    Limit      = 40;
    minimum    = 1e-10;

Type
    Tnums      = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tarr       = array[1..Limit + 1] of double;
    Tmatrix    = object
                     tot      : longint;
                     data     : array[1..Limit] of Tarr;
                     main     : Tnums;
                     visited  : Tvisited;
                     function add(equa : Tarr) : boolean;
                     procedure del_last;
                     procedure Get_Ans;
                 end;
    Tdata      = array[1..Limit] of double;

Var
    cost       : Tdata;
    source ,
    matrix     : Tmatrix;
    answer     : Tdata;
    tN , count ,
    N , M      : longint;
    bestans    : double;

function init : boolean;
var
    i , j      : longint;
begin
    bestans := 0;
    if eof then exit(false);
    read(M , N);
    if (N = 0) or (M = 0) then exit(false);
    init := true;
    fillchar(cost , sizeof(cost) , 0);
    fillchar(source , sizeof(source) , 0);
    for i := 1 to M do read(cost[i]);
    for i := 1 to N do
      begin
          for j := 1 to M + 1 do read(source.data[i , j]);
      end;
    readln;
    tN := N;
    for i := 1 to M do
      begin
          inc(N); source.data[N , i] := 1;
      end;
end;

function zero(num : double) : boolean;
begin
    exit(abs(num) <= minimum);
end;

procedure swap(var a , b : double);
var
    tmp        : double;
begin
    tmp := a; a := b; b := tmp;
end;

function Tmatrix.add(equa : Tarr) : boolean;
var
    max ,
    i , j      : longint;
    t          : double;
begin
    inc(tot); data[tot] := equa;
    for i := 1 to tot - 1 do
      begin
          t := data[tot , main[i]] / data[i , main[i]];
          for j := 1 to M + 1 do
            data[tot , j] := data[tot , j] - data[i , j] * t;
      end;
    max := 0;
    for i := 1 to M do
      if not visited[i] then
        if (max = 0) or (abs(data[tot , i]) > abs(data[tot , max])) then
          max := i;
    if (max = 0) or zero(data[tot , max]) then begin dec(tot); exit(false); end;
    add := true; visited[max] := true; main[tot] := max;
end;

procedure Tmatrix.del_last;
begin
    visited[main[tot]] := false; main[tot] := 0; dec(tot);
end;

procedure Tmatrix.Get_Ans;
var
    i , j      : longint;
    t          : double;
begin
    fillchar(answer , sizeof(answer) , 0);
    for i := tot downto 1 do
      begin
          t := data[i , M + 1];
          for j := 1 to M do
            if j <> main[i] then
              t := t - answer[j] * data[i , j];
          answer[main[i]] := t / data[i , main[i]];
      end;
    for i := 1 to M do if answer[i] < -minimum then exit;
    for i := 1 to tN do
      begin
          t := 0;
          for j := 1 to M do
            t := t + answer[j] * source.data[i , j];
          if t > source.data[i , M + 1] + minimum then exit;
      end;
    t := 0;
    for i := 1 to M do t := t + cost[i] * answer[i];
    if t > bestans then
      bestans := t;
end;

procedure dfs(step , tot : longint);
var
    i          : longint;
begin
    if N - step + 1 + tot < M then exit;
    if tot >= M then
      begin
          matrix.Get_Ans;
          exit;
      end;
    for i := step to N do
      if matrix.add(source.data[i]) then
        begin
            dfs(i + 1 , tot + 1);
            matrix.del_last;
        end;
end;

procedure work;
begin
    dfs(1 , 0);
end;

procedure out;
begin
    bestans := bestans * tN;
    writeln('Nasa can spend ' , int(bestans + 1 - minimum) : 0 : 0 , ' taka.');
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
     count := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.
