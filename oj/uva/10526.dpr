{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{ $R+,Q+,S+}
Const
    InFile     = 'p10526.in';
    OutFile    = 'p10526.out';
    Limit      = 50000;

Type
    Tstr       = array[1..Limit] of char;
    Tdata      = array[1..Limit] of longint;

Var
    A , B      : Tstr;
    maxmatch ,
    data       : Tdata;
    k , LA , LB ,
    tot , cases: longint;

procedure read_str(var A : Tstr; var LA : longint; endstr : string);
var
    s          : string;
    i          : longint;
begin
    readln(s);
    LA := 0;
    readln(s);
    while s <> endstr do
      begin
          for i := 1 to length(s) do begin inc(LA); A[LA] := s[i]; end;
          inc(LA); A[LA] := #10;
          readln(s);
      end;
end;

function init : boolean;
begin
    readln(k);
    if k = 0
      then init := false
      else begin
               init := true;
               read_str(A , LA , 'END TDP CODEBASE');
               read_str(B , LB , 'END JCN CODEBASE');               
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and ((maxmatch[data[stop]] < maxmatch[key]) or (maxmatch[data[stop]] = maxmatch[key]) and (data[stop] > key)) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and ((maxmatch[data[start]] > maxmatch[key]) or (maxmatch[data[start]] = maxmatch[key]) and (data[start] < key)) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop); 
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function cmp_str(p1 , p2 : longint) : longint;
var
    i          : longint;
begin
    cmp_str := 0;
    if p1 = p2 then exit;
    i := 0;
    while (p1 + i <= LA) and (p2 + i <= LA) do
      if A[p1 + i] <> A[p2 + i]
        then begin
                 if A[p1 + i] < A[p2 + i]
                   then cmp_str := -1
                   else cmp_str := 1;
                 exit;
             end
        else inc(i);
    if p1 + i > LA
      then cmp_str := -1
      else cmp_str := 1;
end;

procedure qk_pass_str(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (cmp_str(data[stop] , key) > 0) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (cmp_str(data[start] , key) < 0) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort_str(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_str(start , stop , mid);
          qk_sort_str(start , mid - 1);
          qk_sort_str(mid + 1 , stop);
      end;
end;

procedure get_len(p1 , p2 : longint; var len , code : longint);
var
    i          : longint;
begin
    i := 0; code := 0;
    while (p1 + i <= LA) and (p2 + i <= LB) do
      if A[p1 + i] <> B[p2 + i]
        then begin
                 if A[p1 + i] < B[p2 + i]
                   then code := -1
                   else code := 1;
                 len := i; exit;
             end
        else inc(i);
    len := i;
    if (p1 + i > LA) and (p2 + i > LB)
      then
      else if p1 + i > LA
             then code := -1
             else code := 1;
end;

procedure work;
var
    i , len , code ,
    st , ed ,
    mid        : longint;
begin
    fillchar(maxmatch , sizeof(maxmatch) , 0);
    for i := 1 to LA do data[i] := i;
    qk_sort_str(1 , LA);
    for i := 1 to LB do
      begin
          st := 1; ed := LA;
          while st <= ed do
            begin
                mid := (st + ed) div 2;
                get_len(data[mid] , i , len , code);
                if len > maxmatch[i] then maxmatch[i] := len;
                if code = 0
                  then break
                  else if code < 0
                         then st := mid + 1
                         else ed := mid - 1;
            end;
      end;
      
    tot := 0;
    for i := 1 to LB do
      if (maxmatch[i] <> 0) and ((i = 1) or (maxmatch[i - 1] - 1 < maxmatch[i])) then
        begin
            inc(tot);
            data[tot] := i;
        end;
    qk_sort(1 , tot);
end;

procedure out;
var
    i , j      : longint;
begin
    inc(cases);
    if cases > 1 then writeln;
    writeln('CASE ' , cases);
    for i := 1 to tot do
      if i <= k then
        begin
            writeln('INFRINGING SEGMENT ' , i , ' LENGTH ' , maxmatch[data[i]] , ' POSITION ' , data[i] - 1);
            for j := data[i] to maxmatch[data[i]] + data[i] - 1 do
              write(B[j]);
            writeln;
        end;
end;

Begin
    cases := 0;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
