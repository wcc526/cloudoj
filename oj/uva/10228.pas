Const
    InFile     = 'p10228.in';
    Limit      = 10;
    LimitExtend= 10;
    LimitSave  = Limit * LimitExtend;
    LimitN     = 100;
    minimum    = 1e-6;

Type
    Tkey       = record
                     x , y , tot             : double;
                 end;
    Tdata      = object
                     tot      : longint;
                     data     : array[1..LimitSave] of Tkey;
                     procedure add(x , y : double);
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                 end;
    Topt       = array[0..1] of Tdata;
    Tpoints    = array[1..LimitN] of Tkey;

Var
    opt        : Topt;
    points     : Tpoints;
    N , cases ,
    answer     : longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases); readln(N);
    for i := 1 to N do
      readln(points[i].x , points[i].y);
end;

procedure Tdata.add(x , y : double);
var
    i          : longint;
begin
    inc(tot); data[tot].x := x; data[tot].y := y; data[tot].tot := 0;
    for i := 1 to N do
      data[tot].tot := data[tot].tot + sqrt(sqr(x - points[i].x) + sqr(y - points[i].y));
end;

procedure Tdata.qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].tot > key.tot) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].tot < key.tot) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure Tdata.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    Len , x , y: double;
    now , i , j: longint;
begin
    now := 0; opt[now].tot := 0; Len := 10000; x := 5000; y := 5000;
    for i := 1 to LimitSave do
      opt[now].add(x + (random - 0.5) * Len , y + (random - 0.5) * Len);
    while Len > 0.1 do
      begin
          opt[now].qk_sort(1 , opt[now].tot); Len := Len / 3;
          opt[1 - now].tot := 0;
          for i := 1 to Limit do
            begin
                x := opt[now].data[i].x; y := opt[now].data[i].y;
                for j := 1 to LimitExtend do
                  opt[1 - now].add(x + (random - 0.5) * Len , y + (random - 0.5) * Len);
            end;
          now := 1 - now;
      end;
    opt[now].qk_sort(1 , opt[now].tot);
    answer := round(opt[now].data[1].tot + minimum);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            writeln(answer);
            if cases <> 0 then writeln;
        end;
//    Close(INPUT);
End.