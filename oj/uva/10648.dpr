{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10648.in';
    OutFile    = 'p10648.out';
    Limit      = 100;

Type
    Tdata      = array[1..Limit , 1..Limit] of double;

Var
    data       : Tdata;
    cases , 
    N , M      : longint;

function init : boolean;
begin
    read(N);
    inc(cases);
    if N = -1
      then init := false
      else begin init := true; read(M); write('Case ' , cases , ': '); end;
end;

procedure work;
var
    i , j      : longint;
begin
    if M = 0 then begin writeln(0.0 : 0 : 7); exit; end;
    fillchar(data , sizeof(data) , 0);
    data[1 , 1] := 1; for j := 2 to N do data[1 , j] := data[1 , j - 1] / M;
    for i := 2 to M do
      for j := i to N do
        data[i , j] := data[i - 1 , j - 1] * (M - i + 1) / M + data[i , j - 1] * i / M;
end;

procedure out;
begin
    if M <> 0 then
      writeln(1 - data[M , N] : 0 : 7);
end;

Begin
    cases := 0;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
