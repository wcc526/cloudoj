#include <stdio.h>

int main(void)
{
  int k, n, t, p, v[71][71];

  for (n = 71; n--; ) {
    v[n][0] = v[1][n] = 1;
    v[n][1] = n;
  }

  for (n = 2; n <= 70; ++n) {
    for (t = 2; t <= 70; ++t) {
      v[n][t] = 0;
      for (p = t + 1; p--; )
        v[n][t] += v[n - 1][p];
    }
  }

  scanf("%d", &k);

  while (k--) {
    scanf("%d %d %d", &n, &t, &p);
    t -= n * p;
    printf("%d\n", v[n][t]);
  }

  return 0;
}
