{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10710.in';
    OutFile    = 'p10710.out';

Var
    N , i      : longint;
    power ,
    tmp        : extended;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N <> -1 do
        begin
            tmp := 1; power := 2; i := N - 1;
            while i <> 0 do
              begin
                  if odd(i)
                    then tmp := tmp * power - int(tmp * power / N) * N;
                  power := power * power - int(power * power / N) * N;
                  i := i div 2;
              end;
            if tmp = 1
              then writeln(N , ' is a Jimmy-number')
              else writeln(N , ' is not a Jimmy-number');
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.

