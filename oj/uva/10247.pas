Const
    Limit      = 4000;
    LimitP     = 1000;
    LimitLen   = 10000;

Type
    Tprimes    = array[1..LimitP] of longint;
    Tcount     = array[1..LimitP] of longint;
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure times(num : longint);
                     procedure print;
                 end;

Var
    primes     : Tprimes;
    count      : Tcount;
    answer     : lint;
    tot ,
    P , K , D  : longint;

function check(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to trunc(sqrt(num)) do
      if num mod i = 0
        then exit(false);
    exit(true);
end;

procedure pre_process;
var
    i          : longint;
begin
    P := 0;
    for i := 2 to Limit do
      if check(i) then
        begin
            inc(P); primes[P] := i;
        end;
end;

procedure add_sub(num , delta : longint);
var
    i          : longint;
begin
    i := 1;
    while num <> 1 do
      if num mod primes[i] = 0
        then begin inc(count[i] , delta); num := num div primes[i]; end
        else inc(i);
end;

procedure add(num , delta : longint);
var
    i          : longint;
begin
    for i := 2 to num do
      add_sub(i , delta);
end;

procedure work;
var
    j ,
    i , delta  : longint;
begin
    fillchar(count , sizeof(count) , 0);
    tot := 1;
    for i := 1 to D do tot := tot * K + 1;
    delta := 1;
    for i := D downto 1 do
      begin
          tot := (tot - 1) div K;
          add(tot * K , delta);
          add(tot , - K * delta);
          delta := delta * K;
      end;
    answer.init; answer.data[1] := 1;
    for i := 1 to P do
      for j := 1 to count[i] do
        answer.times(primes[i]);
end;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.times(num : longint);
var
    jw , tmp ,
    i          : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do
      write(data[i]);
end;

procedure out;
begin
    answer.print;
    writeln;
end;

Begin
    pre_process;
    while not eof do
      begin
          readln(K , D);
          work;
          out;
      end;
End.