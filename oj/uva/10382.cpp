#include<stdio.h>
#include<string.h>
#include<math.h>
#include<algorithm>
#define MAXN 10010
const double eps = 1e-10;
int N, M;
double L, W;
struct Seg {
	double x, y;
	bool operator <(const Seg &t) const {
		return x < t.x;
	}
} seg[MAXN];
int dcmp(double x) {
	return (x > eps) - (x < -eps);
}
double sqr(double x) {
	return x * x;
}
void input() {
	M = 0;
	for (int i = 0; i < N; i++) {
		double p, r;
		scanf("%lf%lf", &p, &r);
		if (dcmp(2 * r - W) <= 0)
			continue;
		double l = sqrt(sqr(r) - sqr(W * 0.5));
		seg[M++]=Seg{p-l,p+l};
	}
	std::sort(seg, seg + M);
}
int process() {
	int cnt = 0;
	double x = 0, y = 0;
	for (int i = 0; i < M; i++) {
		if (dcmp(seg[i].x - x) > 0) {
			if (dcmp(seg[i].x - y) > 0)
				return -1;
			++cnt, x = y;
			if (dcmp(x - L) >= 0)
				return cnt;
		}
		y = std::max(y, seg[i].y);
	}
	if (dcmp(y - L) < 0)
		return -1;
	return cnt + 1;
}
int main() {
	//freopen("in.txt","r",stdin);
	while (scanf("%d%lf%lf", &N, &L, &W) == 3) {
		input();
		printf("%d\n", process());
	}
	return 0;
}
