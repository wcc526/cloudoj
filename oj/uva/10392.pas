Const
    Limit      = 100000;

Type
    Tprime     = array[1..1000000] of boolean;
    Tprimes    = array[1..Limit] of longint;

Var
    prime      : Tprime;
    primes     : Tprimes;
    P          : longint;
    N          : extended;

procedure Get_Primes;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    for i := 2 to 1000000 do
      if prime[i] then
        begin
            inc(P); primes[P] := i;
            for j := 2 to 1000000 div i do
              prime[i * j] := false;
        end;
end;

procedure print(N : extended);
begin
    if N = 0
      then
      else begin
               print(int(N / 10));
               write(N - int(N / 10) * 10 : 0 : 0);
           end;
end;

procedure main;
var
    i          : longint;
begin
    readln(N);
    while N > 0 do
      begin
          for i := 1 to P do
            begin
                if N = 1 then break;
                while int(N / primes[i]) * primes[i] = N do
                  begin
                      N := N / primes[i];
                      writeln('    ' , primes[i]);
                  end;
            end;
          if N > 1 then begin write('    '); print(N); writeln; end;
          writeln;
          readln(N);
      end;
end;

Begin
    Get_Primes;
    Main;
End.