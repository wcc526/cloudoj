Const
    InFile     = 'p10226.in';
    Limit      = 10000;
    LimitLen   = 35;
    nilT       = 0;

Type
    Tstr       = string[LimitLen];
    Tnode      = record
                     father , Left , Right ,
                     frequency               : longint;
                     key                     : Tstr;
                 end;
    Ttree      = object
                     tot , rootT             : longint;
                     data                    : array[1..Limit] of Tnode;
                     procedure init;
                     procedure Right_Rotate(p : longint);
                     procedure Left_Rotate(p : longint);
                     procedure Rotate(p : longint);
                     procedure Splay(p : longint);
                     procedure Find_Insert(s : Tstr);
                 end;

Var
    tree       : Ttree;
    cases ,
    count      : longint;

procedure Ttree.init;
begin
    fillchar(tree , sizeof(tree) , 0);
end;

procedure Ttree.Right_Rotate(p : longint);
var
    x , y      : longint;
begin
    x := data[p].Left; y := data[x].Right;
    if data[p].father = nilT
      then rootT := x
      else if data[data[p].father].Left = p
             then data[data[p].father].Left := x
             else data[data[p].father].Right := y;
    data[x].father := data[p].father;
    data[x].Right := p; data[p].father := x;
    data[p].Left := y; data[y].father := p;
end;

procedure Ttree.Left_Rotate(p : longint);
var
    x , y      : longint;
begin
    x := data[p].Right; y := data[x].Left;
    if data[p].father = nilT
      then rootT := x
      else if data[data[p].father].Right = p
             then data[data[p].father].Right := x
             else data[data[p].father].Left := y;
    data[x].father := data[p].father;
    data[x].Left := p; data[p].father := x;
    data[p].Right := y; data[y].father := p;
end;

procedure Ttree.Rotate(p : longint);
begin
    if data[data[p].father].Left = p
      then Right_Rotate(data[p].father)
      else Left_Rotate(data[p].father);
end;

procedure Ttree.Splay(p : longint);
var
    y          : longint;
begin
    while data[p].father <> nilT do
      begin
          y := data[p].father;
          if y = rootT
            then Rotate(p)
            else if (data[y].Left = p) = (data[data[y].father].Left = y)
                   then begin Rotate(y); Rotate(p); end
                   else begin Rotate(p); Rotate(p); end;
      end;
end;

procedure Ttree.Find_Insert(s : Tstr);
var
    key        : Tnode;
    p , fp     : longint;
begin
    fillchar(key , sizeof(key) , 0); key.key := s;
    if rootT = 0
      then begin inc(tot); rootT := tot; data[rootT] := key; inc(data[tot].frequency); exit; end;
    p := rootT; fp := data[p].father;
    while p <> nilT do
      begin
          fp := p;
          if data[p].key = s
            then break
            else if data[p].key < s
                   then p := data[p].Right
                   else p := data[p].Left;
      end;
    if p = nilT
      then begin
               inc(tot); p := tot; data[tot] := key;
               if data[fp].key < s
                 then data[fp].Right := tot
                 else data[fp].Left := tot;
           end;
    inc(data[p].frequency);
    Splay(p);
end;

procedure dfs_print(p : longint);
begin
    if p <> 0 then
      begin
          dfs_print(tree.data[p].Left);
          writeln(tree.data[p].key , ' ' , tree.data[p].frequency / count * 100 : 0 : 4);
          dfs_print(tree.data[p].Right);
      end;
end;

procedure main;
var
    s          : Tstr;
begin
    tree.init;
    dec(Cases);
    readln; count := 0;
    while not eof and not eoln do
      begin
          readln(s); inc(count);
          tree.Find_Insert(s);
      end;
    dfs_print(tree.rootT);
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        main;
//    Close(INPUT);
End.
