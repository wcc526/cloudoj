{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10699.in';
    OutFile    = 'p10699.out';
    Limit      = 1000000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    N          : longint;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    data[1] := 0;
    for i := 2 to Limit do
      if data[i] = 0 then
        for j := 1 to Limit div i do
          inc(data[j * i]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      readln(N);
      while N <> 0 do
        begin
            writeln(N , ' : ' , data[N]);
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
