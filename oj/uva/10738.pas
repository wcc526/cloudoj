Const
    Limit      = 1000000;

Type
    Tprime     = array[1..Limit] of boolean;
    Tdata      = array[0..Limit] of longint;

Var
    prime      : Tprime;
    factor ,
    mu , sum   : Tdata;
    N          : longint;

procedure process;
var
    i , j      : longint;
begin
    fillchar(mu , sizeof(mu) , 0);
    fillchar(prime , sizeof(prime) , 1);
    mu[1] := 1; prime[1] := false;
    for i := 2 to Limit do mu[i] := 1;
    for i := 2 to Limit do
      if prime[i]
        then for j := 1 to Limit div i do
               begin
                   factor[i * j] := i;
                   prime[i * j] := false;
               end;
    for i := 2 to Limit do
      if not prime[i] then
        if factor[i] = factor[i div factor[i]]
          then mu[i] := 0
          else mu[i] := -mu[i div factor[i]];
    sum[0] := 0;
    for i := 1 to Limit do sum[i] := sum[i - 1] + mu[i];
end;

Begin
    process;
    readln(N);
    while N <> 0 do
      begin
          writeln(N : 8 , mu[N] : 8 , sum[N] : 8);
          readln(N);
      end;
End.