#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;
const int N = 755;
int a[N][N];
int n;
struct Item{
	int s,b;
	bool operator < (const Item &u)const {
		return s > u.s;
	}
};

void merge(int a[],int b[],int c[]){
	priority_queue<Item> q;
	for(int i = 0;i < n;i++){
		Item item;
		item.s = a[i] + b[0];
		item.b = 0;
		q.push(item);
	}
	for(int i = 0;i < n;i++){
		Item item = q.top();q.pop();
		c[i] = item.s;
		int o = item.b;
		if(o + 1 < n){
			item.s += b[o + 1] - b[o],item.b++;
			q.push(item);
		}
	}

}


int main()
{
	//freopen("in","r",stdin);
	while(scanf("%d",&n) != EOF){
		for(int i = 0;i < n;i++){
			for(int j = 0;j < n;j++)
				scanf("%d",&a[i][j]);
			sort(a[i],a[i] + n);
		}
		for(int i = 1;i < n;i++)
			merge(a[0],a[i],a[0]);
		for(int i = 0;i < n;i++)
			if(i) printf(" %d",a[0][i]); else printf("%d",a[0][0]);
		puts("");
	}
	return 0;
}