#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
const int mod = 20071027;
struct node{
    int next[26];
    int f;
}trie[210000];
char str[310000];
char s[110];
int tot=0;
void insert(){
    int root=0;
    for(int i=0;s[i];i++){
        s[i]=s[i]-'a';
        if(trie[root].next[s[i]]==-1)trie[root].next[s[i]]=tot++;
        root=trie[root].next[s[i]];
    }
    trie[root].f=1;
}
int dp[310000];
int kases=1;
int main(){
    while(scanf("%s",str+1)!=EOF){
        int n;
        scanf("%d",&n);
        memset(trie,-1,sizeof(trie));
        tot=1;
        for(int i=0;i<n;i++){
            scanf("%s",s);
            insert();
        }
        memset(dp,0,sizeof(dp));
        dp[0]=1;
        str[0]=1;
        for(int i=0;str[i];i++){
            //printf("%d\n",dp[i]);
            int root=0;
            for(int j=1;str[i+j];j++){
                root = trie[root].next[str[i+j]-'a'];
                if(root==-1)break;
                if(trie[root].f==1){
                    dp[i+j]+=dp[i];
                    while(dp[i+j]>mod)dp[i+j]-=mod;
                }
            }
        }
        int l=strlen(str+1);
        printf("Case %d: %d\n",kases++,dp[l]);
    }
    return 0;
}
