Const
    LimitLen   = 100;
    Limit      = 100;

Type
    lint       = object
                     signal   : longint;
                     Len      : longint;
                     data     : array[1..Limit] of longint;
                     procedure init;
                     function readin : boolean;
                     procedure wipe_zero;
                     procedure add_sub(num1 , num2 : lint);
                     procedure minus_sub(num1 , num2 : lint);
                     function compare(num : lint) : longint;
                     procedure add(num1 , num2 : lint);
                     procedure minus(num1 , num2 : lint);
                     procedure times(num1 , num2 : lint);
                     procedure print;
                     procedure GetNum(num : longint);
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    x , answer : lint;
    data       : Tdata;
    N          : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1; signal := 1;
end;

function lint.readin : boolean;
var
    ch         : char;
    i , tmp    : longint;
begin
    init; len := 0;
    repeat if eoln then exit(false); read(ch); until ch <> ' ';
    readin := true;
    while ch <> ' ' do
      begin
          if ch = '-' then signal := -signal;
          if ch in ['0'..'9'] then
            begin
                inc(len); data[len] := ord(ch) - ord('0');
            end;
          if eoln then ch := ' ' else read(ch);
      end;
    for i := 1 to len div 2 do
      begin
          tmp := data[i]; data[i] := data[len - i + 1];
          data[len - i + 1] := tmp;
      end;
end;

procedure lint.wipe_zero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.add_sub(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    fillchar(data , sizeof(data) , 0);
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.minus_sub(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    fillchar(data , sizeof(data) , 0);
    jw := 0;
    for i := 1 to num1.len do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0; if tmp < 0 then begin inc(tmp , 10); inc(jw); end;
          data[i] := tmp;
      end;
    len := num1.len;
    wipe_zero;
end;

function lint.compare(num : lint) : longint;
var
    i          : longint;
begin
    if num.len <> len
      then exit(len - num.len)
      else for i := len downto 1 do
             if data[i] <> num.data[i] then
               exit(data[i] - num.data[i]);
    exit(0);
end;

procedure lint.add(num1 , num2 : lint);
begin
    if num1.signal = num2.signal
      then begin
               self.add_sub(num1 , num2);
               signal := num1.signal;
           end
      else if num1.compare(num2) > 0
             then begin
                      self.minus_sub(num1 , num2);
                      signal := num1.signal;
                  end
             else begin
                      self.minus_sub(num2 , num1);
                      signal := num2.signal;
                  end;
end;

procedure lint.minus(num1 , num2 : lint);
begin
    num2.signal := -num2.signal;
    self.add(num1 , num2);
end;

procedure lint.times(num1 , num2 : lint);
var
    jw , i ,
    j          : longint;
begin
    init;
    for i := 1 to num1.len do
      for j := 1 to num2.len do
        inc(data[i + j - 1] , num1.data[i] * num2.data[j]);
    for i := 1 to num1.len + num2.len do
      begin
          inc(data[i + 1] , data[i] div 10);
          data[i] := data[i] mod 10;
      end;
    len := num1.len + num2.len;
    signal := num1.signal * num2.signal;
    wipe_zero;
end;

procedure lint.print;
var
    i          : longint;
begin
    if (signal < 0) and ((len > 1) or (data[1] > 0)) then write('-');
    for i := len downto 1 do write(data[i]);
end;

procedure lint.getnum(num : longint);
begin
    init; len := 0;
    while num <> 0 do
      begin
          inc(len); data[len] := num mod 10; num := num div 10;
      end;
    if len = 0 then inc(len);
end;

procedure init;
var
    i          : longint;
    tmp        : lint;
begin
    x.readin; readln;
    N := -1;
    while not eoln do
      begin
          inc(N);
          if not data[N].readin then begin dec(N); break; end;
      end;
    readln;
    if N = -1 then while true do;
    for i := 0 to N div 2 do
      begin
          tmp := data[i]; data[i] := data[N - i]; data[N - i] := tmp;
      end;
end;

procedure work;
var
    i          : longint;
    tmp        : lint;
begin
    answer.init;
    for i := N downto 1 do
      begin
          answer.times(answer , x);
          tmp.GetNum(i);
          tmp.times(tmp , data[i]);
          answer.add(answer , tmp);
      end;
end;

Begin
    assign(INPUT , 'p10268.in'); ReSet(INPUT);
    while not eof do
      begin
          init;
          work;
          if N <> -1 then
            answer.print;
          writeln;
      end;
//    Close(INPUT);
End.