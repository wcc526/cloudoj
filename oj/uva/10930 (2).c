#include <stdio.h>

int main(void)
{
  int c = 0, i, j, n, t[1001], v[30];
  *t = 1;

  while (scanf("%d", &n) == 1) {
    for (i = 1; i <= 1000; ++i)
      t[i] = 0;
    for (i = 0; i < n; ++i)
      scanf("%d", v + i);
    printf("Case #%d:", ++c);
    for (i = 0; i < n; ++i)
      printf(" %d", v[i]);
    puts("");

    for (i = 0; i < n; ++i) {
      if (t[v[i]] || (i && v[i - 1] > v[i]))
        goto na;
      for (j = 1000 - v[i]; j >= 0; --j)
        if (t[j])
          t[j + v[i]] = 1;
    }

    puts("This is an A-sequence.");
    continue;
  na:
    puts("This is not an A-sequence.");
  }

  return 0;
}
