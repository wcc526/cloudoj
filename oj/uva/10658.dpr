{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10658.in';
    OutFile    = 'p10658.out';
    Limit      = 65;

Type
    Tpower     = array[0..Limit] of extended;

Var
    power      : Tpower;
    answer     : extended;
    N , T      : longint;

procedure pre_process;
var
    i          : longint;
begin
    power[0] := 1;
    for i := 1 to Limit do power[i] := power[i - 1] * 2;
end;

procedure init;
begin
    dec(T);
    readln(N);
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := N - 2 downto 0 do
      if i mod 3 <> (N - 1) mod 3 then
        answer := answer + power[i];
end;

procedure print(num : extended);
begin
    if num <> 0
      then begin
               print(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure out;
begin
    print(answer);
    if answer = 0 then write(0);
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
//    Close(OUTPUT);
End.
