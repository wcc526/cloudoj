Const
    Limit      = 100;
    LimitLen   = 250;

Type
    lint       = object
                     Len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure multi(num : longint);
                     procedure minus(num : lint);
                     procedure add(num : lint);
                     procedure print;
                 end;
    Tanswer    = array[4..128 , 1..Limit] of lint;

Var
    i ,
    B , N      : longint;
    answer     : Tanswer;
    tmp ,
    pre , now  : lint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.multi(num : longint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (jw <> 0) or (i <= len) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.add(num : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (i <= num.len) or (jw <> 0) do
      begin
          tmp := data[i] + num.data[i] + jw;
          data[i] := tmp mod 10;
          jw := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.minus(num : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num.len) or (jw <> 0) do
      begin
          tmp := data[i] - num.data[i] - jw;
          jw := 0; if tmp < 0 then begin inc(tmp , 10); jw := 1; end;
          data[i] := tmp;
          inc(i);
      end;
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do
      write(data[i]);
    writeln;
end;

Begin
    for B := 4 to 128 do
      begin
          pre.init; pre.data[1] := 1; pre.multi(B);
          now.init; now.data[1] := 1; now.multi(B * B - 1);
          answer[B , 1] := pre; answer[B , 2] := now;
          for i := 3 to Limit do
            begin
                tmp := now;
                tmp.multi(B);
                tmp.minus(pre);
                pre := now; now := tmp;
                answer[B , i] := now;
            end;
      end;
    readln(B , N);
    while B + N <> 0 do
      begin
          if N = 1
            then writeln(B - 1)
            else if N = 2
                   then writeln((B - 1) * B - 1)
                   else begin
                            now := answer[B , N];
                            now.minus(answer[B , N - 1]);
                            now.print;
                        end;
          readln(B , N);
      end;
End.