Const
    InFile     = 'p10750.in';
    OutFile    = 'p10750.out';
    Limit      = 10000;

Type
    Tpoint     = record
                     x , y    : extended;
                     x0 , y0  : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    minp       : Tpoint;
    mindist    : extended;
    N , T      : longint;

procedure init;
var
    i          : longint;
begin
    dec(T);
    readln(N);
    for i := 1 to N do
      with data[i] do
        begin
            read(x0 , y0);
            x := cos(1) * x0 - sin(1) * y0;
            y := sin(1) * x0 + cos(1) * y0;
        end;
end;

function compare(const p1 , p2 : Tpoint) : extended;
begin
    if p1.x <> p2.x
      then compare := p1.x - p2.x
      else compare := p1.y - p2.y;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (compare(data[stop] , key) > 0) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(data[start] , key) < 0) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    qk_sort(1 , N);
    mindist := maxlongint;
    j := 1;
    for i := 2 to N do
      begin
          while sqr(data[i].x - data[j].x) >= mindist do inc(j);
          for k := j to i - 1 do
            if sqr(data[i].x - data[k].x) + sqr(data[i].y - data[k].y) < mindist then
              begin
                  mindist := sqr(data[i].x - data[k].x) + sqr(data[i].y - data[k].y);
                  minp := data[i];
              end;
      end;
end;

procedure out;
begin
    writeln(minp.x0 , '.000 ' , minp.y0 , '.000');
    if T <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.