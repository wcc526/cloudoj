{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10523.in';
    OutFile    = 'p10523.out';
    LimitLen   = 200;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure clearzero;
                     procedure add(num1 , num2 : lint);
                     procedure multi(num1 , num2 : lint);
                     procedure transfer(num : longint);
                     procedure print;
                 end;

Var
    answer ,
    power , A1 : lint;
    N , A      : longint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 0;
end;

procedure lint.clearzero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.add(num1 , num2 : lint);
var
    i , tmp ,
    jw         : longint;
begin
    init;
    jw := 0; i := 1;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.multi(num1 , num2 : lint);
var
    i , j , tmp ,
    jw         : longint;
begin
    init;
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := num1.data[i] * num2.data[j] + jw + data[i + j - 1];
                jw := tmp div 10;
                data[i + j - 1] := tmp mod 10;
                inc(j);
            end;
          if i + j - 2 > len then len := i + j - 2;
      end;
    clearzero;
end;

procedure lint.transfer(num : longint);
begin
    init;
    while num <> 0 do
      begin
          inc(len); data[len] := num mod 10; num := num div 10;
      end;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

procedure init;
begin
    readln(N , A);
end;

procedure work;
var
    tmp , product
               : lint;
    i          : longint;
begin
    A1.transfer(A);
    answer.init; answer.len := 1; power.transfer(1); power.data[1] := 1;
    for i := 1 to N do
      begin
          power.multi(power , A1);
          tmp.transfer(i);
          product.multi(power , tmp);
          answer.add(answer , product);
      end;
end;

procedure out;
begin
    answer.print;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
