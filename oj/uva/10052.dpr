{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10052.in';
    OutFile    = 'p10052.out';
    Limit      = 300;
    LimitLen   = 15;
    LimitStep  = 1000000;

Type
    Tname      = string[Limitlen];
    Tpeople    = array[1..Limit] of Tname;
    Tsubarr    = array[1..Limit] of boolean;
    Tmap       = array[1..Limit] of ^Tsubarr;
    Tsequence  = array[1..Limit] of integer;

Var
    people     : Tpeople;
    map        : Tmap;
    visited    : Tsubarr;
    seq , degree ,
    answer     : Tsequence;
    N , Cases ,
    totCase    : integer;
    Times      : longint;

procedure init;
var
    M , i ,
    p1 , p2 ,
    where      : integer;
    s          : string;
    s1 , s2    : Tname;
begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(N , M);
      for i := 1 to N do fillchar(map[i]^ , sizeof(map[i]^) , 0);
      for i := 1 to N do readln(people[i]);
      for i := 1 to M do
        begin
            readln(s); where := pos(' ' , s);
            s1 := copy(s , 1 , where - 1);
            s2 := copy(s , where + 1 , length(s) - where);
            while s1[1] = ' ' do delete(s1 , 1 , 1);
            while s1[length(s1)] = ' ' do delete(s1 , length(s1) , 1);
            while s2[1] = ' ' do delete(s2 , 1 , 1);
            while s2[length(s1)] = ' ' do delete(s2 , length(s2) , 1);
            p1 := 1; while people[p1] <> s1 do inc(p1);
            p2 := 1; while people[p2] <> s2 do inc(p2);
            map[p1]^[p2] := true;
            map[p2]^[p1] := true;
        end;
//    Close(INPUT);
end;

procedure Get_Peo;
var
    i , j , min: integer;
    tot        : integer;
    num        : array[1..Limit] of integer;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(degree , sizeof(degree) , 0);
{    for i := 1 to N do
      for j := 1 to N do
        if map[i]^[j] then
          inc(degree[i]);}
    for i := 1 to N do
      begin
          min := 0; tot := 0;
          for j := 1 to N do
            if not visited[j] then
              if (min = 0) or (degree[j] > degree[min])
                then begin
                         tot := 1; num[tot] := j; min := j;
                     end
                else if degree[j] = degree[min]
                       then begin
                                inc(tot);
                                num[tot] := j;
                            end;
          min := num[random(tot) + 1];
          visited[min] := true;
          seq[i] := min;
          for j := 1 to N do
            if not visited[j] then
              if map[min]^[j] then
                inc(degree[j]);
      end;
end;

procedure Get_NEW_Peo;
var
    min        : longint;
    i , j ,
    p          : integer;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(degree , sizeof(degree) , 0);
    for i := 1 to N do
      for j := 1 to N do
        if map[i]^[j] then
          inc(degree[i]);
    for i := 1 to N do
      begin
          min := 0;
          for j := 1 to N do
            if not visited[j] then
              inc(min , degree[j] + 1);
          min := random(min);
          dec(min);
          if min < 0 then min := 0;
          p := 0;
          for j := 1 to N do
            if not visited[j] then
            if degree[j] + 1 > min then
              begin
                  p := j; break;
              end
            else dec(min , degree[j] + 1);
          visited[p] := true;
          seq[i] := p;
          for j := 1 to N do
            if not visited[j] then
              if map[p]^[j] then
                dec(degree[j]);
    end;
end;

procedure Get_Sequence;
var
    i , p1 , p2 ,
    tmp        : integer;
begin
    for i := 1 to N do seq[i] := i;
    for i := 1 to N * 2 do
      begin
          p1 := random(N) + 1; p2 := random(N) + 1;
          tmp := seq[p1]; seq[p1] := seq[p2]; seq[p2] := tmp;
      end;
end;

function Get_Answer(top : integer) : boolean;
var
    i , p , j ,
    tot , tmp  : integer;
    color      : array[1..4] of boolean;
begin
    fillchar(answer , sizeof(answer) , 0);
    fillchar(visited , sizeof(visited) , 0);
    Get_Answer := false;
    for i := 1 to N do
      begin
          fillchar(color , sizeof(color) , 0);
          p := seq[i];
          tot := 4;
          for j := 1 to N do
            if visited[j] and map[p]^[j] then
              begin
                  tmp := answer[j];
                  if not color[tmp] then
                    begin
                        color[tmp] := true;
                        dec(tot); if tot = 0 then exit;
                    end;
              end;
          if tot > top then tot := top;
          tot := random(tot) + 1; j := 1;
          while tot > 0 do
            begin
                if not color[j] then
                  begin
                      dec(tot); if tot = 0 then break;
                  end;
                inc(j);
            end;
          answer[p] := j;
          visited[p] := true;
      end;
    Get_Answer := true;
end;

procedure work;
var
    i          : longint;
begin
    Get_PEO;
    if Get_Answer(1)
      then exit;
    if Get_Answer(4)
      then exit;
    Times := LimitStep div (longint(N) * N);
    if times > 500 then times := 500;
    for i := 1 to Times do
      begin
{          Get_Sequence;
          if Get_Answer(1)
            then exit;
          if Get_Answer(2)
            then exit;
          if Get_Answer(4)
            then exit;    }
          GET_PEO;
          if Get_Answer(1)
            then exit;
          if Get_Answer(2)
            then exit;
          if Get_Answer(3)
            then exit;
          if Get_Answer(4)
            then exit;
      end;                          
{    writeln('Cannot Find Answer!');
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      writeln('Cannot Find Answer!');
    Close(OUTPUT);}
    halt;
end;

procedure out;
var
    i , j , sum
               : integer;
    first      : boolean;
begin
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      for i := 1 to 4 do
        begin
            sum := 0;
            for j := 1 to N do
              if answer[j] = i then inc(sum);
            writeln(sum);
            first := true;
            for j := 1 to N do
              if answer[j] = i then
                begin
                    if not first then write(' ');
                    first := false;
                    write(people[j]);
                end;
            writeln;
        end;
//    Close(OUTPUT);
end;

Begin
    for Cases := 1 to Limit do new(map[Cases]); 
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase);
      for Cases := 1 to totCase do
        begin
            if Cases <> 1 then writeln;
            writeln('Case #' , Cases);
            init;
            work;
            out;
        end;
    Close(INPUT);
    Close(OUTPUT);
End.