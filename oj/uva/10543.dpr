{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10543.in';
    OutFile    = 'p10543.out';
    Limit      = 50;
    LimitK     = 20;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[1..Limit , 1..LimitK] of boolean;
    Tqueue     = array[1..Limit * LimitK] of
                   record
                       p , num               : longint;
                   end;

Var
    queue      : Tqueue;
    visited    : Tvisited;
    map        : Tmap;
    N , K ,
    answer     : longint;

function init : boolean;
var
    M , i ,
    p1 , p2    : longint;
begin
    read(N , M , K);
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(map , sizeof(map) , 0);
               fillchar(visited , sizeof(visited) , 0);
               fillchar(queue , sizeof(queue) , 0);
               for i := 1 to M do
                 begin
                     read(p1 , p2);
                     inc(p1); inc(p2);
                     if p1 <> p2 then
                       map[p1 , p2] := true;
                 end;
           end;
end;

procedure work;
var
    open , closed ,
    i          : longint;
begin
    open := 1; closed := 1;
    queue[1].p := 1; queue[1].num := 1;
    visited[1 , 1] := true;
    while open <= closed do
      begin
          if (queue[open].p = N) and (queue[open].num >= K) then
            begin answer := queue[open].num; exit; end;

          if (queue[open].num + 1 <= LimitK) then
            for i := 1 to N do
              if map[queue[open].p , i] then
                if not visited[i , queue[open].num + 1] then
                  begin
                      visited[i , queue[open].num + 1] := true;
                      inc(closed);
                      queue[closed].p := i;
                      queue[closed].num := queue[open].num + 1;
                  end;
          inc(open);
      end;
    answer := -1;
end;

procedure out;
begin
    if N = 1 then
      begin writeln(3); exit; end;
    if answer = -1
      then writeln('LOSER')
      else writeln(answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
