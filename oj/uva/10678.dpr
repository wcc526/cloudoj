{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10678.in';
    OutFile    = 'p10678.out';

Var
    N          : longint;
    D , L ,
    a , b , c  : extended;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N > 0 do
        begin
            dec(N);
            readln(D , L);
            c := D / 2; a := L / 2;
            b := sqrt(a * a - c * c);
            writeln(a * b * pi : 0 : 3);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
