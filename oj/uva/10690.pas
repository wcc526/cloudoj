{$R-,Q-,S-,I-}
Const
    InFile     = 'p10690.in';
    OutFile    = 'p10690.out';
    Limit      = 100;
    LimitHash  = 1999997;

Type
    Tkey       = record
                     T        : byte;
                     p , q    : shortint;
                     sum      : smallint;
                 end;
    Thash      = array[0..LimitHash] of Tkey;
    Tdata      = array[1..Limit] of longint;
    Tsubsum    = array[0..Limit] of longint;

Var
    hash       : Thash;
    data       : Tdata;
    subsum     : Tsubsum;
    cases ,
    minimum ,
    answer ,
    N , M , tot: longint;

procedure init;
var
    i          : longint;
begin
    N := 0; M := 0;
    readln(N , M);
    inc(Cases);
    N := N + M; if M + M > N then M := N - M;
    tot := 0;
    for i := 1 to N do begin read(data[i]); inc(tot , data[i]); end;
    readln;
end;

function visit(p , q , sum : longint) : boolean;
var
    hashnum    : longint;
begin
    hashnum := (sum * 100 + p) * 100 + q;
    hashnum := hashnum mod LimitHash;
    if hashnum < 0 then inc(hashnum , LimitHash);
    while hash[hashnum].T = cases do
      begin
          if (hash[hashnum].p = p) and (hash[hashnum].q = q) and (hash[hashnum].sum = sum) then
            begin
                visit := true;
                exit;
            end;
          hashnum := hashnum + 1;
          if hashnum = LimitHash then hashnum := 0;
      end;
    visit := false;
    hash[hashnum].T := cases; hash[hashnum].sum := sum;
    hash[hashnum].p := p; hash[hashnum].q := q;
end;

procedure dfs(p , q , sum : longint);
var
    min , max , tmp ,
    last ,
    difference : longint;
begin
    last := M - q;
    if N - p + 1 < last then exit;
    min := subsum[p + last - 1] - subsum[p - 1];
    max := subsum[N] - subsum[N - last];
    tmp := abs(tot - answer * 2);
    if ((sum + min) * 2 >= tot + tmp) or ((sum + max) * 2 <= tot - tmp) then
      exit;
    if q = M
      then begin
               if abs(sum * 2 - tot) < abs(answer * 2 - tot) then
                 answer := sum;
               exit;
           end;
    if visit(p , q , sum) then exit;
    difference := sum * 2 - subsum[p - 1];
    if abs(difference + data[p]) <= abs(difference)
      then begin
               dfs(p + 1 , q + 1 , sum + data[p]);
               dfs(p + 1 , q , sum);
           end
      else begin
               dfs(p + 1 , q , sum);
               dfs(p + 1 , q + 1 , sum + data[p]);
           end;
end;

procedure work;
var
    i , j , tmp: longint;
begin
    for i := 1 to N do
      for j := i + 1 to N do
        if data[i] > data[j] then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;
    subsum[0] := 0;
    for i := 1 to N do subsum[i] := subsum[i - 1] + data[i];
    if subsum[M] * (tot - subsum[M]) < subsum[N - M] * (tot - subsum[N - M])
      then minimum := subsum[M] * (tot - subsum[M])
      else minimum := subsum[N - M] * (tot - subsum[N - M]);
    answer := maxlongint div 10;
    dfs(1 , 0 , 0);
end;

procedure out;
begin
    writeln(answer * (tot - answer) , ' ' , minimum);
end;

Begin
    fillchar(hash , sizeof(hash) , 0);
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while not eof do
        begin
            init;
            if N = 0 then break;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
