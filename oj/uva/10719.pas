Var
    i ,
    N , K      : longint;
    data       : array[1..20000] of extended;

Begin
    while not eof do
      begin
          readln(K); N := 0;
          while not eoln do begin inc(N); read(data[N]); end;
          readln;
          for i := 1 to N - 1 do data[i + 1] := data[i + 1] + data[i] * K;
          write('q(x):');
          for i := 1 to N - 1 do write(' ' , data[i] : 0 : 0);
          writeln;
          writeln('r = ' , data[N] : 0 : 0);
          if not eof then writeln else break;
      end;
End.