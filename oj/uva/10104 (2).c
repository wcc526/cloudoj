#include <stdio.h>

int gcd(int a, int b, int *x, int *y)
{
  int d, u, v;
  if (a < b)
    gcd(b, a, y, x);
  if (b == 0) {
    *x = 1, *y = 0;
    return a;
  } else {
    d = gcd(b, a % b, &u, &v);
    *x = v, *y = u - a / b * v;
    return d;
  }
}

int main()
{
  int a, b, d, x, y;

  while (scanf("%d %d", &a, &b) == 2) {
    d = gcd(a, b, &x, &y);
    printf("%d %d %d\n", x, y, d);
  }

  return 0;
}
