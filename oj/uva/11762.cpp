/*
 * uva_11762.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
double dp(int x)
{
	if(x==1) return 0.0;
	if(vis[x]) return f[x];
	vis[x]=1;
	double &ans=f[x];
	int g=0,p=0;
	ans=0;
	for(int i=0;i<prime_cnt&&primes[i]<=x;++i)
	{
		++p;
		if(x%primes[i]==0)
		{
			++g;
			ans+=dp(x/primes[i]);
		}
		ans=(ans+p)/g;
		return ans;
	}
}






