{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10552.in';
    OutFile    = 'p10552.out';
    Limit      = 250;

Type
    Tperson    = record
                     totChildren                            : longint;
                     name , birthday , deathdate ,
                     father , mother                        : string;
                     children                               : array[1..Limit] of string;
                 end;
    Tdata      = array[1..Limit] of Tperson;
    Tcommand   = array[0..4] of string;

Var
    data       : Tdata;
    N          : longint;
    first      : boolean;

procedure wipe_spaces(var s : string);
begin
    while (s <> '') and (s[1] = ' ') do delete(s , 1 , 1);
    while (s <> '') and (s[length(s)] = ' ') do delete(s , length(s) , 1);
end;

procedure split(s : string; var command : Tcommand);
var
    j          : longint;
begin
    wipe_spaces(s);
    command[0] := '';
    while (s <> '') and (s[1] <> ' ') do
      begin
          command[0] := command[0] + s[1];
          delete(s , 1 , 1);
      end;
    for j := 1 to 4 do command[j] := '';
    
    j := 1;
    while s <> '' do
      begin
          if s[1] <> ':'
            then command[j] := command[j] + s[1]
            else inc(j);
          delete(s , 1 , 1);
      end;
    for j := 1 to 4 do wipe_spaces(command[j]);
end;

function find_person(const s : string) : longint;
var
    i          : longint;
begin
    for i := 1 to N do
      if data[i].name = s then
        begin
            find_person := i;
            exit;
        end;
    inc(N);
    data[N].name := s;
    data[N].totChildren := 0;
    data[N].birthday := '';
    data[N].deathdate := '';
    data[N].father := '';
    data[N].mother := '';
    find_person := N;
end;

procedure ins(const s1 , s2 : string);
var
    p , i      : longint;
begin
    p := find_person(s1);
    for i := 1 to data[p].totChildren do
      if data[p].children[i] = s2 then exit;
    i := data[p].totChildren + 1;
    while (i > 1) and (data[p].children[i - 1] > s2) do
      begin
          data[p].children[i] := data[p].children[i - 1];
          dec(i);
      end;
    data[p].children[i] := s2;
    inc(data[p].totChildren);
end;

procedure print_info(p , indent : longint);
begin
    write('' : indent);
    write(data[p].name);
    if data[p].birthday <> '' then write(' ' , data[p].birthday , ' -');
    if data[p].deathdate <> '' then write(' ' , data[p].deathdate);
    writeln;
end;

procedure print_ancestor_sub(const s : string; indent : longint);
var
    p          : longint;
begin
    if s = '' then exit;
    p := find_person(s);
    print_info(p , indent);
    if data[p].mother < data[p].father
      then begin
               print_ancestor_sub(data[p].mother , indent + 2);
               print_ancestor_sub(data[p].father , indent + 2);
           end
      else begin
               print_ancestor_sub(data[p].father , indent + 2);
               print_ancestor_sub(data[p].mother , indent + 2);
           end;
end;

procedure print_ancestor(const s : string);
var
    p          : longint;
begin
    writeln('ANCESTORS of ' , s);
    p := find_person(s);
    if data[p].mother < data[p].father
      then begin
               print_ancestor_sub(data[p].mother , 2);
               print_ancestor_sub(data[p].father , 2);
           end
      else begin
               print_ancestor_sub(data[p].father , 2);
               print_ancestor_sub(data[p].mother , 2);
           end;
end;

procedure print_descendant_sub(const s : string; indent : longint);
var
    p , i      : longint;
begin
    if s = '' then exit;
    p := find_person(s);
    print_info(p , indent);
    for i := 1 to data[p].totChildren do
      print_descendant_sub(data[p].children[i] , indent + 2);
end;

procedure print_descendant(const s : string);
var
    p , i      : longint;
begin
    writeln('DESCENDANTS of ' , s);
    p := find_person(s);
    for i := 1 to data[p].totChildren do
      print_descendant_sub(data[p].children[i] , 2);
end;

procedure main;
var
    p          : longint;
    s          : string;
    command    : Tcommand;
begin
    fillchar(data , sizeof(data) , 0);
    N := 0;
    readln(s); split(s , command);
    while command[0] <> 'QUIT' do
      begin
          if command[0] = 'BIRTH'
            then begin
                     p := find_person(command[1]);
                     data[p].birthday := command[2];
                     data[p].mother := command[3];
                     data[p].father := command[4];
                     ins(data[p].mother , data[p].name);
                     ins(data[p].father , data[p].name);
                 end;
          if command[0] = 'DEATH'
            then begin
                     p := find_person(command[1]);
                     data[p].deathdate := command[2];
                 end;
          if command[0] = 'ANCESTORS'
            then begin
                     if not first then writeln;
                     first := false;
                     print_ancestor(command[1]);
                 end;
          if command[0] = 'DESCENDANTS'
            then begin
                     if not first then writeln;
                     first := false;
                     print_descendant(command[1]);
                 end;
          readln(s); split(s , command);
      end;
end;

Begin
    first := true;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      main;
//    Close(OUTPUT);
//    Close(INPUT);
End.
