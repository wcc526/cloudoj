{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10539.in';
    OutFile    = 'p10539.out';
    Limit      = 1000000;

Type
    Tprime     = array[1..Limit] of boolean;
    Tprimes    = array[1..Limit] of comp;

Var
    prime      : Tprime;
    primes     : Tprimes;
    tot , 
    Cases ,
    answer     : longint;
    a , b      : comp;

procedure Get_Prime;
var
    i , j      : longint;
begin
    tot := 0;
    fillchar(prime , sizeof(prime) , 1);
    prime[1] := false;
    for i := 1 to Limit do
      begin
          if prime[i] then
            begin
                inc(tot);
                primes[tot] := i;
                for j := 2 to Limit div i do
                  prime[i * j] := false;
            end;
      end;
end;

procedure init;
begin
    dec(Cases);
    read(A , B);
end;

procedure work;
var
    i          : longint;
    tmp        : comp;
begin
    answer := 0;
    for i := 1 to tot do
      if sqr(primes[i]) <= B then
        begin
            tmp := primes[i] * primes[i];
            while tmp <= B do
              begin
                  if tmp >= A then inc(answer);
                  tmp := tmp * primes[i];
              end;
        end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
    Get_Prime;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
