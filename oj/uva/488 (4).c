#include <stdio.h>

char *w[] = {
  "1", "22", "333", "4444", "55555", "666666",
  "7777777", "88888888", "999999999"
};

int main()
{
  int a, f, i, n;

  scanf("%d", &n);
  while (n--) {
    scanf("%d %d", &a, &f);
    while (f--) {
      for (i = 0; i < a; ++i)
        puts(w[i]);
      for (--i; i--;)
        puts(w[i]);
      if (f)
        putchar('\n');
    }
    if (n)
      putchar('\n');
  }

  return 0;
}
