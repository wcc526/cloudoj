{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10419.in';
    OutFile    = 'p10419.out';
    Limit      = 1000;
    LimitT     = 14;
    LimitP     = 100;

Type
    Tprime     = array[1..LimitP] of longint;
    Tdata      = array[1..LimitP , 0..Limit , 0..LimitT] of boolean;

Var
    prime      : Tprime;
    data       : Tdata;
    N , T ,
    P , cases  : longint;                                         
    add2       : boolean;

procedure pre_process;
var
    tmp , 
    i , j      : longint;
    ok         : boolean;
    s1 , s2    : string;
begin
    P := 0;
    fillchar(data , sizeof(data) , 0);
    for i := 3 to 300 do
      begin
          ok := true;
          for j := 2 to i - 1 do
            if i mod j = 0 then
              begin
                  ok := false;
                  break;
              end;
          if ok then begin inc(P); prime[P] := i; end;
      end;
    for i := 1 to P do
      for j := i + 1 to P do
        begin
            str(prime[i] , s1); s1 := s1 + '+';
            str(prime[j] , s2); s2 := s2 + '+';
            if s1 > s2 then
              begin
                  tmp := prime[i]; prime[i] := prime[j]; prime[j] := tmp;
              end;
        end;
end;

function init : boolean;
begin
    readln(N , T);
    inc(Cases);
    init := (N <> 0);
end;

procedure work;
var
    i , j , k  : longint;
begin
    data[P , prime[p] , 1] := true; data[P , prime[p] * 2 , 2] := true; data[P , 0 , 0] := true;
    data[P + 1 , 0 , 0] := true;
    for i := P - 1 downto 1 do
      for j := 0 to Limit do
        for k := 0 to LimitT do
          begin
              if data[i + 1 , j , k] then data[i , j , k] := true;
              if (k > 0) and (j - prime[i] >= 0) and data[i + 1 , j - prime[i] , k - 1] then data[i , j , k] := true;
              if (k > 1) and (j - prime[i] * 2 >= 0) and data[i + 1 , j - prime[i] * 2 , k - 2] then data[i , j , k] := true;
          end;
end;

function out : boolean;
type
    Tpath      = array[1..LimitT] of longint;
var
    path       : Tpath;
    tot , i    : longint;
    s          : string;
begin
    if odd(N) <> odd(T)
      then begin
               dec(N , 2); dec(T); add2 := true;
           end
      else add2 := false;
    out := false;
    if N < 0 then exit;
    tot := 0;
    if not data[1 , N , T] then exit;
    for i := 1 to P do
      begin
          if (N >= prime[i] * 2) and (T > 1) and (data[i + 1 , N - prime[i] * 2 , T - 2]) then
            begin inc(tot); path[tot] := prime[i]; inc(tot); path[tot] := prime[i]; dec(N , prime[i] * 2); dec(T , 2); end
          else if (N >= prime[i]) and (T > 0) and (data[i + 1 , N - prime[i] , T - 1]) then
                 begin inc(tot); path[tot] := prime[i]; dec(N , prime[i]); dec(T); end;
      end;
    if add2 then
      begin
          i := tot + 1;    
          while (i > 1) do
            begin
                str(path[i - 1] , s); s := s + '+';
                if s > '2+'
                  then begin path[i] := path[i - 1]; dec(i); end
                  else break;
            end;
          path[i] := 2;
          inc(tot);
      end;
    for i := 1 to tot - 1 do
      write(path[i] , '+');
    writeln(path[tot]);
    out := true;
end;

Begin
    pre_process;
    work;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            writeln('CASE ' , Cases , ':');
            if not out
              then writeln('No Solution.');
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
