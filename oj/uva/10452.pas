Const
    Letters    = ['A' , 'V' , 'O' , 'H' , 'I' , 'E' , '@'];

Var
    N , M , T ,
    start ,
    i , j , k  : longint;
    visited    : array[1..8 , 1..8] of boolean;
    data       : array[1..8 , 1..8] of char;

function dfs(x , y , count : longint) : boolean;
begin
    visited[x , y] := true;
    if data[x , y] = '@'
      then exit(true)
      else begin
               if (x <= N) and not visited[x + 1 , y] and (data[x + 1 , y] in Letters) and dfs(x + 1 , y , count + 1)
                 then begin write('forth'); if data[x , y] <> '#' then write(' '); exit(true); end;
               if (y > 1) and not visited[x , y - 1] and (data[x , y - 1] in Letters) and dfs(x , y - 1 , count + 1)
                 then begin write('right'); if data[x , y] <> '#' then write(' '); exit(true); end;
               if (y <= M) and not visited[x , y + 1] and (data[x , y + 1] in Letters) and dfs(x , y + 1 , count + 1)
                 then begin write('left'); if data[x , y] <> '#' then write(' '); exit(true); end;
               exit(false);
           end;
end;

Begin
//    assign(INPUT , 'p10452.in'); ReSet(INPUT);
    readln(T);
    for i := 1 to T do
      begin
          fillchar(data , sizeof(data) , 0);
          fillchar(visited , sizeof(visited) , 0);
          readln(N , M);
          for j := 1 to N do
            begin
                for k := 1 to M do
                  begin
                      read(data[j , k]);
                      if (j = 1) and (data[j , k] = '#') then start := k;
                  end;
                readln;
            end;
          dfs(1 , start , 1);
          writeln;
      end;
End.
