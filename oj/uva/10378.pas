Const
    minimum    = 1e-6;
    Limit      = 1000;

Type
    Tstr       = string[100];
    Tkey       = record
                     X , Y    : double;
                 end;
    Tanswer    = array[1..Limit] of Tkey;

Var
    s1 , s2    : Tstr;
    ch         : char;
    answer     : Tanswer;
    cases ,
    N , i , j  : longint;
    alpha , Len ,
    A , B      : double;

function zero(num : double) : boolean;
begin
    exit(abs(num) <= minimum);
end;

function Get_Angle(cosA , sinA : double) : double;
begin
    if sinA < 0
      then exit(2 * pi - Get_Angle(cosA , -sinA))
      else if zero(cosA)
             then exit(pi / 2)
             else if cosA < 0
                    then exit(pi - Get_Angle(-cosA , sinA))
                    else exit(arctan(sinA / cosA));
end;

function compare(const k1 , k2 : Tkey) : double;
begin
    if zero(k1.X - k2.X)
      then exit(-(k1.Y - k2.Y))
      else exit(-(k1.X - k2.X));
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := answer[tmp]; answer[tmp] := answer[start];
    while start < stop do
      begin
          while (start < stop) and (compare(answer[stop] , key) > 0) do dec(stop);
          answer[start] := answer[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(answer[start] , key) < 0) do inc(start);
          answer[stop] := answer[start];
          if start < stop then dec(stop);
      end;
    mid := start; answer[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

Begin
//    assign(INPUT , 'p10378.in'); ReSet(INPUT);
    cases := 0;
    while not eof do
      begin
          s1 := ''; s2 := '';
          read(ch); while ch in ['+' , '-'] do begin s1 := s1 + ch; read(ch); end;
          while not (ch in ['+' , '-']) do begin s1 := s1 + ch; read(ch); end;
          while ch <> ' ' do begin s2 := s2 + ch; read(ch); end;
          readln(N);
          val(s1 , A); delete(s2 , length(s2) , 1); val(s2 , B);
          Len := sqrt(sqr(A) + sqr(B));
          if not zero(Len) then Len := exp(ln(Len) / N);
          alpha := Get_Angle(A , B);
          alpha := alpha / N;
          for i := 1 to N do
            begin
                answer[i].X := cos(alpha + i * pi * 2 / N) * Len;
                answer[i].Y := sin(alpha + i * pi * 2 / N) * Len;
            end;
          qk_sort(1 , N);

          inc(Cases);
          writeln('Case ' , cases , ':');
          for i := 1 to N do
            begin
                str(answer[i].X : 0 : 3 , s1); if s1 = '-0.000' then s1 := '0.000';
                str(answer[i].Y : 0 : 3 , s2); if s2 = '-0.000' then s2 := '0.000';
                if not (s2[1] in ['+' , '-']) then s2 := '+' + s2;
                writeln(s1 , s2 , 'i');
            end;
          writeln;
      end;
End.
