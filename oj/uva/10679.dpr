{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10679.in';
    OutFile    = 'p10679.out';
    LimitLen   = 100100;

Type
    Tstr       = array[1..LimitLen] of char;
    Tnext      = array[1..LimitLen] of longint;
    Tappeared  = array[#32..#130 , #32..#130 , #32..#130] of longint;

Var
    cases , Q ,
    len1 , len2: longint;
    next       : Tnext;
    s , subs   : Tstr;
    appeared   : Tappeared;

procedure read_str(var s : Tstr; var Len : longint);
var
    ch         : char;
    i          : longint;
begin
    len := 0;
    while not eoln do
      begin
          read(ch); inc(Len);
          s[Len] := ch;
      end;
    for i := 1 to len div 2 do
      begin
          ch := s[i]; s[i] := s[len - i + 1]; s[len - i + 1] := ch;
      end;
    readln;
end;

procedure process;
var
    i          : longint;
begin
    for i := 1 to Len1 - 2 do
      appeared[s[i] , s[i + 1] , s[i + 2]] := cases;
end;

procedure Get_Next;
var
    i , j      : longint;
begin
    next[1] := 0;
    i := 2; j := 1;
    while i <= Len2 do
      if subs[i] = subs[j]
        then begin
                 next[i] := j;
                 inc(i); inc(j);
             end
        else if j = 1
               then begin
                        next[i] := 0; inc(i);
                    end
               else j := next[j - 1] + 1;
end;

function kmp : boolean;
var
    i , j      : longint;
begin
    kmp := false;
    for i := 1 to Len2 - 2 do
      if cases <> appeared[subs[i] , subs[i + 1] , subs[i + 2]] then
        exit;

    Get_Next;
    i := 1; j := 1;
    while (i <= Len1) and (j <= Len2) do
      if s[i] = subs[j]
        then begin inc(i); inc(j); end
        else if j <> 1
               then j := next[j - 1] + 1
               else inc(i);
    kmp := (j > Len2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      fillchar(appeared , sizeof(appeared) , 0);
      readln(cases);
      while Cases > 0 do
        begin
            read_str(s , Len1);
            process;
            readln(Q);
            while Q > 0 do
              begin
                  read_str(subs , Len2);
                  if KMP
                    then writeln('y')
                    else writeln('n');
                  dec(Q);
              end;
            dec(Cases);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
