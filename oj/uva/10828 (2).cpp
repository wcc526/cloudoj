/*
Author: Mabraygas @ BUPT
*/
#include"iostream"
#include"vector"
#include"string"
#include"cstdio"
#include"cstdlib"
#include"cmath"
#include"algorithm"
#include"queue"
#include"cstring"
#include"map"
#include"set"
#include"fstream"
#include"sstream"
#include"numeric"
#include"stack"
#include"iomanip"
#include"bitset"
#include"list"
#include"functional"
#include"utility"
#include"ctime"
#include"cctype"

using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef vector<ll> vl;
typedef vector<string> vs;
typedef pair<int,int> pii;
typedef vector<pii> vpi;
const double eps=1e-8;
const int inf=(1<<31)-1;
const int hinf=0x3f3f3f3f;
const ll mod=1000000007;

#define sz(a) sizeof(a)
#define mp make_pair
#define pb push_back
#define fi first
#define se second
#define ms(a,i) memset((a),(i),sz(a))
#define clr(x) memset(x,0,sz(x))
#define cdp(x) memset((x),-1,sizeof(x))
#define infi(x) memset(x,0x3f,sz(x))
#define foreach(e,x) for(__typeof(x.begin()) e=x.begin();e!=x.end();++e)

double A[110][110];
void guass_jordan(int n)
{
    int i,j,k,r;
    for(i=0;i<n;i++)
    {
        r=i;
        for(j=i+1;j<n;j++) if(fabs(A[j][i])>fabs(A[r][i])) r=j;
        if(fabs(A[r][i])<eps) continue;
        if(r!=i) for(j=0;j<=n;j++) swap(A[r][j],A[i][j]);
        for(k=0;k<n;k++) if(k!=i)
            for(j=n;j>=i;j--) A[k][j]-=A[k][i]/A[i][i]*A[i][j];
    }
}

int n;
vector<int> pre[110];
int d[110];
int isf[110];

int main()
{
	//std::ios::sync_with_stdio(false);
	int tt=0;
    while(scanf("%d",&n)!=EOF)
    {
        if(n==0) return 0;
        for(int i=0;i<=n;i++) {isf[i]=0; d[i]=0; pre[i].clear();}
        int a,b;
        while(scanf("%d%d",&a,&b))
        {
            if(a==0 && b==0) break;
            a--; b--;
            d[a]++;
            pre[b].pb(a);
        }
        clr(A);
        for(int i=0;i<n;i++)
        {
            A[i][i]=1;
            for(int j=0;j<pre[i].size();j++)
            {
                A[i][pre[i][j]]-=1.0/d[pre[i][j]];
            }
            if(i==0) A[i][n]=1;
        }
        guass_jordan(n);
        for(int i=n-1;i>=0;i--)
        {
            if(fabs(A[i][i])<eps && fabs(A[i][n])>eps) isf[i]=1;
            for(int j=i+1;j<n;j++)
            {
                if(fabs(A[i][j])>eps && isf[j]==1)
                    isf[i]=1;
            }
        }
        int que;
        scanf("%d",&que);
        printf("Case #%d:\n",++tt);
        for(int i=1;i<=que;i++)
        {
            int qq; scanf("%d",&qq); qq--;
            if(isf[qq]==1) {puts("infinity"); continue;}
            else
            {
                if(fabs(A[qq][qq])<eps) puts("0.000");
                else printf("%.3lf\n",A[qq][n]/A[qq][qq]);
            }
        }
    }
}
