Const
    InFile     = 'p10337.in';
    Limit      = 1000;

Type
    Tdata      = array[0..Limit , 0..9] of longint;

Var
    data , opt : Tdata;
    N , cases  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    readln(N); N := N div 100;
    for i := 9 downto 0 do
      for j := 1 to N do
        read(data[j , i]);
end;

procedure better(var a : longint; b : longint);
begin
    if b < a then a := b;
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(opt , sizeof(opt) , 1);
    opt[0 , 0] := 0;
    for i := 0 to N - 1 do
      for j := 0 to 9 do
        begin
            better(opt[i + 1 , j] , 30 - data[i + 1 , j] + opt[i , j]);
            if j <> 0 then
              better(opt[i + 1 , j - 1] , 20 - data[i + 1 , j] + opt[i , j]);
            if j <> 9 then
              better(opt[i + 1 , j + 1] , 60 - data[i + 1 , j] + opt[i , j]);
        end;
end;

procedure out;
begin
    writeln(opt[N , 0]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.