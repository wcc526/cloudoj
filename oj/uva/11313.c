#include <stdio.h>

int main()
{
  int c, m, n;

  scanf("%d", &c);
  while (c--) {
    scanf("%d %d", &n, &m);
    if ((n - 1) % (m - 1))
      puts("cannot do this");
    else
      printf("%d\n", (n - 1) / (m - 1));
  }

  return 0;
}
