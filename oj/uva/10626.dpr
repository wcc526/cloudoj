{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10626.in';
    OutFile    = 'p10626.out';

Var
    T , tot ,
    a , b , c ,
    answer     : longint;

procedure init;
begin
    dec(T);
    read(tot , a , b , c);
    inc(b , 2 * c);
end;

procedure work;
begin
    answer := 0;
    while tot > 0 do
      begin
          if b <> 0
            then if (a >= 3) and (b div 2 < tot)
                   then begin
                            dec(b); dec(a , 3);
                            inc(answer , 4);
                        end
                   else begin
                            if c > 0 then begin dec(answer); dec(c); end;
                            dec(b , 2); inc(a , 2);
                            inc(answer , 2);
                        end
            else begin
                     dec(a , 8);
                     inc(answer , 8);
            end;
          dec(tot);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
//    Close(OUTPUT);
End.
