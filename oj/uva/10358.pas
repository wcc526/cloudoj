Const
    InFile     = 'p10358.in';
    N          = 8;
    dirx       : array[1..5] of longint = (-1 , 1 , 0 , 0 , 0);
    diry       : array[1..5] of longint = (0 , 0 , -1 , 1 , 0);
    Base       = 64 * 64 * 64;

Type
    Tmap       = array[1..N , 1..N] of char;
    Tdata      = array[0..Base * 3] of smallint;
    Tvalid     = array[0..Base * 3] of boolean;
    Tgraph1    = array[0..Base - 1] of
                   record
                       tot    : longint;
                       data   : array[1..5] of longint;
                   end;
    Tgraph2    = array[Base..Base * 2 - 1] of
                   record
                       tot    : longint;
                       data   : array[1..5] of longint;
                   end;
    Tgraph3    = array[Base * 2..Base * 3 - 1] of
                   record
                       tot    : longint;
                       data   : array[1..5] of longint;
                   end;

Var
    map        : Tmap;
    data       : Tdata;
    valid      : Tvalid;
    graph1     : Tgraph1;
    graph2     : Tgraph2;
    graph3     : Tgraph3;
    answer ,
    cases      : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    for i := 1 to N do
      begin
          for j := 1 to N do
            read(map[i , j]);
          readln;
      end;
    readln;
    fillchar(data , sizeof(data) , 0);
    fillchar(valid , sizeof(valid) , 0);
    fillchar(graph1 , sizeof(graph1) , 0);
    fillchar(graph2 , sizeof(graph2) , 0);
    fillchar(graph3 , sizeof(graph3) , 0);
end;

procedure decode(status : longint; var x1 , y1 , x2 , y2 , x3 , y3 : longint);
var
    p1 , p2 , p3
               : longint;
begin
    p1 := status mod 64; p2 := (status div 64) mod 64; p3 := (status div 4096) mod 64;
    x1 := p1 mod 8 + 1; y1 := p1 div 8 + 1;
    x2 := p2 mod 8 + 1; y2 := p2 div 8 + 1;
    x3 := p3 mod 8 + 1; y3 := p3 div 8 + 1;
end;

procedure code(var status : longint; x1 , y1 , x2 , y2 , x3 , y3 , signal : longint);
var
    p1 , p2 , p3
               : longint;
begin
    p1 := x1 - 1 + (y1 - 1) * 8;
    p2 := x2 - 1 + (y2 - 1) * 8;
    p3 := x3 - 1 + (y3 - 1) * 8;
    status := ((p3 * 64) + p2) * 64 + p1 + signal * 64 * 64 * 64;
end;

procedure pre_process;
var
    i , p1 , p2 ,
    nx1 , ny1 ,
    nx2 , ny2 ,
    nstatus ,
    x1 , y1 , x2 , y2 ,
    x3 , y3    : longint;
begin
    for i := 0 to Base * 3 - 1 do
      begin
          decode(i , x1 , y1 , x2 , y2 , x3 , y3);
          if (map[x1 , y1] = '#') or (map[x2 , y2] = '#') or (map[x3 , y3] = '#') then
            begin valid[i] := false; continue; end;
          valid[i] := true;
      end;

    for i := 0 to Base * 3 - 1 do if valid[i] then
      begin
          decode(i , x1 , y1 , x2 , y2 , x3 , y3);
          if (x1 = x2) and (y1 = y2) or (x1 = x3) and (y1 = y3)
            then begin data[i] := -1; continue; end;
          if map[x1 , y1] = 'P'
            then begin data[i] := 1; continue; end;
          if i < Base
            then for p1 := 1 to 5 do
                   begin
                       nx1 := x1 + dirx[p1]; ny1 := y1 + diry[p1];
                       if (nx1 >= 1) and (ny1 >= 1) and (nx1 <= N) and (ny1 <= N) then
                         begin
                             code(nstatus , nx1 , ny1 , x2 , y2 , x3 , y3 , 1);
                             if not valid[nstatus] then continue;
                             inc(graph1[i].tot);
                             graph1[i].data[graph1[i].tot] := nstatus;
                         end;
                   end;
          if (i >= Base) and (i < Base * 2)
            then for p1 := 1 to 5 do
                   begin
                       nx1 := x2 + dirx[p1]; ny1 := y2 + diry[p1];
                       if (nx1 >= 1) and (ny1 >= 1) and (nx1 <= N) and (ny1 <= N) then
                         begin
                             code(nstatus , x1 , y1 , nx1 , ny1 , x3 , y3 , 2);
                             if not valid[nstatus] then continue;
                             inc(graph2[i].tot);
                             graph2[i].data[graph2[i].tot] := nstatus;
                         end;
                   end;
          if i >= Base * 2
            then for p1 := 1 to 5 do
                   begin
                       nx1 := x3 + dirx[p1]; ny1 := y3 + diry[p1];
                       if (nx1 >= 1) and (ny1 >= 1) and (nx1 <= N) and (ny1 <= N) then
                         begin
                             code(nstatus , x1 , y1 , x2 , y2 , nx1 , ny1 , 0);
                             if not valid[nstatus] then continue;
                             inc(graph3[i].tot);
                             graph3[i].data[graph3[i].tot] := nstatus;
                         end;
                   end;
      end;
end;

procedure work;
var
    res ,
    i , j , status ,
    x1 , y1 , x2 , y2 ,
    x3 , y3    : longint;
    changed    : boolean;
begin
    pre_process;
    repeat
      changed := false;
      for i := Base - 1 downto 0 do if valid[i] and (graph1[i].tot > 0) then
        begin
            res := -1;
            for j := 1 to graph1[i].tot do if valid[graph1[i].data[j]] then
              if res < data[graph1[i].data[j]] then
                res := data[graph1[i].data[j]];
            if data[i] <> res then
              begin
                  data[i] := res;
                  changed := true;
              end;
        end;

      for i := Base * 2 - 1 downto Base do if valid[i] and (graph2[i].tot > 0) then
        begin
            res := 1;
            for j := 1 to graph2[i].tot do if valid[graph2[i].data[j]] then
              if res > data[graph2[i].data[j]] then
                res := data[graph2[i].data[j]];
            if data[i] <> res then
              begin
                  data[i] := res;
                  changed := true;
              end;
        end;

      for i := Base * 3 - 1 downto Base * 2 do if valid[i] and (graph3[i].tot > 0) then
        begin
            res := 1;
            for j := 1 to graph3[i].tot do if valid[graph3[i].data[j]] then
              if res > data[graph3[i].data[j]] then
                res := data[graph3[i].data[j]];
            if data[i] <> res then
              begin
                  data[i] := res;
                  changed := true;
              end;
        end;
    until not changed;

    x2 := 0;
    for i := 1 to N do
      for j := 1 to N do
        begin
            if map[i , j] = 'M'
              then begin x1 := i; y1 := j; end;
            if map[i , j] = 'A'
              then if x2 = 0
                     then begin x2 := i; y2 := j; end
                     else begin x3 := i; y3 := j; end;
        end;
    code(status , x1 , y1 , x2 , y2 , x3 , y3 , 1);
    answer := data[status];
end;

procedure out;
begin
    case answer of
      -1       : writeln('You are eliminated.');
      0        : writeln('You are trapped in the Matrix.');
      1        : writeln('You can escape.');
    end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.