{$R+,Q+,S+}
Const
    InFile     = 'p10618.in';
    OutFile    = 'p10618.out';
    Limit      = 80;

Type
    Tdata      = array[1..Limit] of longint;
    Tpath      = array[1..Limit] of char;
    Topt       = array[0..Limit , 1..4 , 1..4 , 1..2] of longint;
                        {           1
                                  4 o 2
                                    3       }
Var
    data       : Tdata;
    opt ,
    s1 , s2 , s3
               : Topt;
    N          : longint;

function init : boolean;
var
    ch         : char;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    fillchar(s1 , sizeof(s1) , 0);
    fillchar(s2 , sizeof(s2) , 0);
    fillchar(s3 , sizeof(s3) , 0);
    init := false; N := 0;
    while not eoln do
      begin
          read(ch);
          if ch = '#' then exit;
          inc(N);
          case ch of
            'U'               : data[N] := 1;
            'R'               : data[N] := 2;
            'D'               : data[N] := 3;
            'L'               : data[N] := 4;
            '.'               : data[N] := 0;
          end;
      end;
    readln;
    init := true;
end;

procedure work;
var
    i , p1 , p2 ,
    p , np1 , np2 ,
    np , j , last ,
    add , tmp  : longint;
begin
    opt[0 , 4 , 2 , 1] := 0;
    opt[0 , 4 , 2 , 2] := 0;
    for i := 0 to N - 1 do
      for p1 := 1 to 4 do
       for p2 := 1 to 4 do
         for p := 1 to 2 do
           if opt[i , p1 , p2 , p] <> -1 then
             if data[i + 1] <> 0 then
               begin
                   j := data[i + 1];
                   for np := 1 to 2 do
                     begin
                         if np <> p
                           then add := 1
                           else begin
                                    if p = 1 then last := p1 else last := p2;
                                    if last = j
                                      then add := 3
                                      else if (last + 1) mod 4 + 1 = j
                                             then add := 7
                                             else add := 5;
                                end;
                         np1 := p1; np2 := p2;
                         if np = 1 then np1 := j else np2 := j;
                         if np1 = np2 then continue;
                         if (np1 = 2) or (np2 = 4) then
                           if (np1 = 1) or (np1 = 3) or (np2 = 1) or (np2 = 3)
                             then if (np1 = 1) or (np1 = 3)
                                    then if j = 1 then continue else
                                    else if j = 2 then continue else
                             else continue;
                         tmp := add + opt[i , p1 , p2 , p];
                         if (opt[i + 1 , np1 , np2 , np] = -1) or (opt[i + 1 , np1 , np2 , np] > tmp)
                           then begin
                                    opt[i + 1 , np1 , np2 , np] := tmp;
                                    s1[i + 1 , np1 , np2 , np] := p1;
                                    s2[i + 1 , np1 , np2 , np] := p2;
                                    s3[i + 1 , np1 , np2 , np] := p;
                                end;
                     end;
               end
             else
               for np := 1 to 2 do
                 if (opt[i + 1 , p1 , p2 , np] = -1) or (opt[i + 1 , p1 , p2 , np] > opt[i , p1 , p2 , p])
                   then begin
                            opt[i + 1 , p1 , p2 , np] := opt[i , p1 , p2 , p];
                            s1[i + 1 , p1 , p2 , np] := p1;
                            s2[i + 1 , p1 , p2 , np] := p2;
                            s3[i + 1 , p1 , p2 , np] := p;
                        end;
end;

procedure out;
var
    i , p1 , p2 , p ,
    t1 , t2 , t3
               : longint;
    path       : Tpath;
begin
    p1 := 0; p2 := 0; p := 0;
    for t1 := 1 to 4 do
      for t2 := 1 to 4 do
        for t3 := 1 to 2 do
          if opt[N , t1 , t2 , t3] <> -1 then
            if (p1 = 0) or (opt[N , t1 , t2 , t3] < opt[N , p1 , p2 , p]) then
              begin
                  p1 := t1; p2 := t2; p := t3;
              end;
    for i := N downto 1 do
      begin
          if data[i] = 0
            then path[i] := '.'
            else if p = 1 then path[i] := 'L' else path[i] := 'R';
          t1 := s1[i , p1 , p2 , p];
          t2 := s2[i , p1 , p2 , p];
          t3 := s3[i , p1 , p2 , p];
          p1 := t1; p2 := t2; p := t3;
      end;
    for i := 1 to N do write(path[i]);
    writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
