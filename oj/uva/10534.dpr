{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10534.in';
    OutFile    = 'p10534.out';
    Limit      = 10000;

Type
    Tdata      = array[0..Limit] of longint;

Var
    data ,
    negativedata , 
    positive ,
    negative   : Tdata;
    N , ans    : longint;

procedure init;
var
    i          : longint;
begin
    readln(N);
    for i := 1 to N do
      read(data[i]);
    readln;
end;

procedure DP(const data : Tdata; var opt : Tdata);
var
    best       : Tdata;
    tot , i ,
    st , ed ,
    mid        : longint;
begin
    tot := 0;
    fillchar(best , sizeof(best) , 0);
    best[0] := -1;
    for i := 1 to N do
      begin
          st := 0; ed := tot;
          opt[i] := 0;
          while st <= ed do
            begin
                mid := (st + ed) div 2;
                if best[mid] < data[i]
                  then begin
                           if opt[i] < mid then opt[i] := mid;
                           st := mid + 1;
                       end
                  else begin
                           ed := mid - 1;
                       end;
            end;
          inc(opt[i]);
          if opt[i] > tot
            then begin tot := opt[i]; best[opt[i]] := data[i]; end
            else if data[i] < best[opt[i]]
                   then best[opt[i]] := data[i];
      end;
end;

procedure work;
var
    i , tmp    : longint;
begin
    for i := 1 to N do negativedata[i] := data[N - i + 1];
    DP(data , positive);
    DP(negativedata , negative);
    
    ans := 0;
    for i := 1 to N do
      begin
          if positive[i] > negative[N - i + 1]
            then tmp := negative[N - i + 1]
            else tmp := positive[i];
          if tmp * 2 - 1 > ans then
            ans := tmp * 2 - 1;
      end;
end;

procedure out;
begin
    writeln(ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
