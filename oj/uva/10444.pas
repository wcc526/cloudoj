Const
    LimitN     = 200;
    LimitP     = 20;
    LimitLen   = 70;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num : lint);
                     function bigger(num : lint) : boolean;
                     procedure print;
                 end;
    Tdata      = array[0..LimitN , 1..LimitP] of lint;
    Tprocessed = array[0..LimitN , 1..LimitP] of boolean;

Var
    processed  : Tprocessed;
    data       : Tdata;
    one        : lint;
    T , N , P  : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0); len := 1;
end;

procedure lint.add(num : lint);
var
    i , jw , tmp
               : longint;
begin
    i := 1; jw := 0;
    while (i <= Len) or (i <= num.len) or (jw <> 0) do
      begin
          tmp := data[i] + num.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

function lint.bigger(num : lint) : boolean;
var
    i          : longint;
begin
    if num.len <> len
      then exit(len > num.len)
      else begin
               for i := len downto 1 do
                 if data[i] <> num.data[i]
                   then exit(data[i] > num.data[i]);
               exit(false);
           end;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
    writeln;
end;

procedure dfs(N , P : longint);
var
    i          : longint;
    tmp        : lint;
begin
    if processed[N , P]
      then exit
      else begin
               for i := 1 to N - 1 do
                 if (P > 3) or (N - i = 1) then
                   begin
                       dfs(i , P); tmp := data[i , P]; tmp.add(data[i , P]);
                       dfs(N - i , P - 1); tmp.add(data[N - i , P - 1]);
                       if not processed[N , P] or data[N , P].bigger(tmp)
                         then begin
                                  data[N , P] := tmp;
                                  processed[N , P] := true;
                              end;
                   end;
           end;
end;

procedure pre_process;
var
    i          : longint;
begin
    fillchar(processed , sizeof(processed) , 0);
    for i := 2 to LimitP do
      begin
          data[1 , i].init;
          data[1 , i].data[1] := 1;
          processed[1 , i] := true;
          data[0 , i].init;
      end;
    one.init; one.data[1] := 1;
end;

Begin
    pre_process;
    T := 0;
    readln(N , P);
    while N + P <> 0 do
      begin
          inc(T);
          write('Case ' , T , ': ');
          dfs(N , P);
          data[N , P].print;
          readln(N , P);
      end;
End.