#include <math.h>
#include <stdio.h>

int main(void)
{
  int i, n, v;

  while (scanf("%d", &n) == 1) {
    i = (int)ceil(0.5 * (sqrt(1 + 8 * (n - 1e-6)) - 1));
    v = (i * (i + 1) >> 1) - n;
    if (i & 1)
      printf("TERM %d IS %d/%d\n", n, v + 1, i - v);
    else
      printf("TERM %d IS %d/%d\n", n, i - v, v + 1);
  }

  return 0;
}
