Const
    InFile     = 'p10319.in';
    OutFile    = 'p10319.out';
    Limit      = 200;

Type
    Tdata      = array[1..Limit] of
                   record
                       x1 , y1 , x2 , y2     : longint;
                   end;
    Tmap       = array[1..Limit * 2 , 1..Limit * 2] of boolean;
    Tvisited   = array[1..Limit * 2] of boolean;
    Tpath      = array[1..Limit * 2] of longint;

Var
    data       : Tdata;
    map        : Tmap;
    color ,
    path       : Tpath;
    used ,
    visited    : Tvisited;
    answer     : boolean;
    tot , totColor ,
    N , totCase ,
    Cases      : longint;

procedure init;
var
    S , A ,
    i          : longint;
begin
    inc(Cases);
    readln(S , A , N);
    for i := 1 to N do
      with data[i] do
        readln(x1 , y1 , x2 , y2);
end;

procedure add_contradiction(i , j , p1 , p2 : longint);
begin
    map[i * 2 - p1 , j * 2 - (p2 xor 1)] := true;
    map[j * 2 - p2 , i * 2 - (p1 xor 1)] := true;
end;

procedure dfs_positive(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    for i := 1 to N do
      if map[p , i] and not visited[i] then
        dfs_positive(i);
    inc(tot); path[tot] := p;
end;

procedure dfs_negative(p : longint);
var
    i          : longint;
begin
    used[p] := true; color[p] := totColor;
    for i := 1 to N do
      if map[i , p] and visited[i] and not used[i] then
        dfs_negative(i);
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(map , sizeof(map) , 0);
    for i := 1 to N do
      for j := 1 to N do
        begin
            if (data[i].x1 <> data[i].x2) and (data[j].x1 <> data[j].x2) then
              if (data[i].x2 > data[i].x1) <> (data[j].x2 > data[j].x1) then
                begin
                    if data[i].y1 = data[j].y1 then
                      add_contradiction(i , j , 0 , 0);
                    if data[i].y1 = data[j].y2 then
                      add_contradiction(i , j , 0 , 1);
                    if data[i].y2 = data[j].y1 then
                      add_contradiction(i , j , 1 , 0);
                    if data[i].y2 = data[j].y2 then
                      add_contradiction(i , j , 1 , 1);
                end;

            if (data[i].y1 <> data[i].y2) and (data[j].y1 <> data[j].y2) then
              if (data[i].y2 > data[i].y1) <> (data[j].y2 > data[j].y1) then
                begin
                    if data[i].x1 = data[j].x1 then
                      add_contradiction(i , j , 1 , 1);
                    if data[i].x1 = data[j].x2 then
                      add_contradiction(i , j , 1 , 0);
                    if data[i].x2 = data[j].x1 then
                      add_contradiction(i , j , 0 , 1);
                    if data[i].x2 = data[j].x2 then
                      add_contradiction(i , j , 0 , 0);
                end;
        end;

    N := N * 2;
    answer := true;
    fillchar(visited , sizeof(visited) , 0);
    fillchar(used , sizeof(used) , 0);
    totColor := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            tot := 0;
            dfs_positive(i);
            for j := tot downto 1 do
              if not used[path[j]] then
                begin
                    inc(totColor);
                    dfs_negative(path[j]);
                end;
        end;
    for i := 1 to N div 2 do
      if color[i * 2] = color[i * 2 - 1] then
        begin
            answer := false;
            break;
        end;
end;

procedure out;
begin
    if answer then writeln('Yes') else writeln('No');
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      readln(totcase); cases := 0;
      while cases < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.