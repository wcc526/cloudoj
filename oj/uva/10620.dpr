{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10620.in';
    OutFile    = 'p10620.out';

Var
    S , x , y ,
    dx , dy    : longint;

function init : boolean;
begin
    readln(S , x , y , dx , dy);
    if S + x + y + dx + dy = 0
      then init := false
      else init := true;
end;

procedure workout;
var
    i , x0 , y0: longint;
begin
    x0 := x mod (2 * S); y0 := y mod (2 * S);
    for i := 1 to S * S do
      begin
          if not ((x0 <= S) and (y0 <= S) or (x0 >= S) and (y0 >= S)) and (x0 <> 0) and (y0 <> 0)
            then begin
                     writeln('After ' , i - 1 , ' jumps the flea lands at (' , x + (i - 1) * dx , ', ' , y + (i - 1) * dy , ').');
                     exit; 
                 end;
          inc(x0 , dx); inc(y0 , dy);
          x0 := x0 mod (2 * S); y0 := y0 mod (2 * S);
      end;
    writeln('The flea cannot escape from black squares.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        workout;
//    Close(OUTPUT);
//    Close(INPUT);
End.
