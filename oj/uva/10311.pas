{$R-,Q-,S-}
Const
    InFile     = 'p10311.in';
    OutFile    = 'p10311.out';
    Limit      = 100000000;

Type
    Tprime     = array[1..Limit div 3 + 1] of boolean;

Var
    prime      : Tprime;
    count      : longint;

procedure Get_Prime;
var
    i , p , j ,
    x , y , dj ,
    dx , dy ,
    now        : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    i := 1; p := 5; now := 2;
    while p <= Limit do
      begin
          if prime[i] then
            begin
                inc(count);
                x := p div 6; y := p - x * 6;
                dx := x * 2; dy := y * 2;
                if dy >= 6 then begin dec(dy , 6); inc(dx); end;
                dj := dx * 6 + dy;
                inc(x , dx); inc(y , dy); j := p + dj;
                if y >= 6 then begin dec(y , 6); inc(x); end;
                while j <= Limit do
                  begin
                      if y = 1 then prime[x * 2] := false;
                      if y = 5 then prime[x * 2 + 1] := false;
                      inc(x , dx); inc(y , dy); inc(j , dj);
                      if y >= 6 then begin dec(y , 6); inc(x); end;
                  end;
            end;
          inc(i); inc(p , now); now := 6 - now;
      end;
end;

Begin
    Get_Prime;
    writeln(count);
End.
