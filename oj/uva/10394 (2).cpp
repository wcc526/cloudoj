/**
 * UVa 10394 Twin Primes
 * Author: chchwy
 * Last Modified: 2011/07/21
 * Blog: http://chchwy.blogspot.com
 */
#include<cstdio>
#include<cstring>
#include<vector>
#include<bitset>
using namespace std;

enum { MAX=20000000+1 };

bitset<MAX> p;

void sieve() {

    p.flip();

    p[0] = p[1] = 0;

    for(int i=3; i*i<=MAX; i+=2) {  // WA: i<=MAX  (這樣下面i平方就溢位了)
        if( p[i] )
            for(unsigned int j=i*i; j<MAX; j+=i)
                p[j] = 0;
                /* if( j==169321 ) cout << "GOT!! i="<< i <<endl; overflow */
    }
}

int main() {

    sieve();

    vector<pair<int,int> > twin;

    twin.push_back(make_pair(0,0)); // skip twin[0]

    for(int i=5; i<MAX; i+=2) {
        if( p[i] && p[i-2] ) {
            twin.push_back(make_pair(i-2, i));
        }
    }

    int no;
    while( scanf("%d", &no)==1 ) {
        printf("(%d, %d)\n", twin[no].first, twin[no].second);
    }

    return 0;
}
