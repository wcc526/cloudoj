Const
    InFile     = 'p10486.in';
    OutFile    = 'p10486.out';
    Limit      = 40;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tdata      = array[1..Limit * Limit] of longint;

Var
    map        : Tmap;
    answer ,
    data ,
    father ,
    children   : Tdata;
    max ,
    N , M      : longint;

procedure init;
var
    i , j      : longint;
begin
    readln(N , M);
    for i := 1 to N do
      for j := 1 to M do
        read(map[i , j]);
end;

function Fmap(p : longint) : longint;
begin
    Fmap := map[(p - 1) div M + 1 , (p - 1) mod M + 1];
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (Fmap(data[stop]) > Fmap(key)) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (Fmap(data[start]) < Fmap(key)) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function Get_Root(p : longint) : longint;
begin
    if father[p] = p
      then exit(p)
      else begin
               father[p] := Get_Root(father[p]);
               exit(father[p]);
           end;
end;

procedure add(p : longint);
var
    i ,
    x , y , np ,
    fp1 , fp2 ,
    nx , ny    : longint;
begin
    father[p] := p; children[p] := 1;
    x := (p - 1) div M + 1; y := (p - 1) mod M + 1;
    for i := 1 to 4 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx > 0) and (nx <= N) and (ny > 0) and (ny <= M) then
            if father[(nx - 1) * M + ny] <> -1 then
              begin
                  np := (nx - 1) * M + ny;
                  fp1 := Get_Root(p); fp2 := Get_Root(np);
                  if fp1 = fp2 then continue;
                  father[fp2] := fp1;
                  inc(children[fp1] , children[fp2]);
              end;
      end;
    fp1 := Get_Root(p);
    if children[fp1] > max then max := children[fp1];
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(answer , sizeof(answer) , $7F);
    for i := 1 to N * M do data[i] := i;
    qk_sort(1 , N * M);
    for i := 1 to N * M do
      if (i = 1) or (Fmap(data[i - 1]) <> Fmap(data[i])) then
        begin
            fillchar(father , sizeof(father) , $FF);
            max := 0;
            for j := i to N * M do
              begin
                  add(data[j]);
                  if answer[max] > Fmap(data[j]) - Fmap(data[i]) then
                    answer[max] := Fmap(data[j]) - Fmap(data[i]);
              end;
        end;
    for i := N * M - 1 downto 1 do
      if answer[i + 1] < answer[i] then
        answer[i] := answer[i + 1];
end;

procedure out;
var
    tot , i , p
               : longint;
begin
    readln(tot);
    for i := 1 to tot do
      begin
          readln(p);
          writeln(answer[p]);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.