Var
    tmp ,
    A , B      : longint;

function dfs(A , B : longint) : boolean;
begin
    if B = 0
      then exit(false)
      else if A div B > 1
             then exit(true)
             else exit(not dfs(B , A mod B));
end;

Begin
    while not eof do
      begin
          readln(A , B);
          if (A = 0) and (B = 0) then break;
          if A < B then
            begin
                tmp := A; A := B; B := tmp;
            end;
          if dfs(A , B)
            then writeln('Stan wins')
            else writeln('Ollie wins');
      end;
End.