#include <math.h>
#include <stdio.h>

int main(void)
{
  double n, p;

  while (scanf("%lf %lf", &n, &p) == 2)
    printf("%.0lf\n", exp(log(p)/n));

  return 0;
}
