Var
    N , tN ,
    upper , i ,
    tmp , count ,
    cases      : longint;
    answer     : comp;

Begin
    cases := 0;
    readln(N);
    while N <> 0 do
      begin
          inc(cases);
          tN := N; upper := trunc(sqrt(N));
          answer := 0; count := 0;
          for i := 2 to upper do
            if tN mod i = 0 then
              begin
                  tmp := 1; inc(count);
                  while tN mod i = 0 do
                    begin
                        tmp := tmp * i;
                        tN := tN div i;
                    end;
                  answer := answer + tmp;
              end;
          if tN <> 1 then
            if answer = 0
              then answer := tN + 1.0
              else answer := answer + tN
          else
            if count = 1 then answer := answer + 1;
          if N = 1 then
            writeln('Case ' , cases , ': ' , 2)
          else
            writeln('Case ' , cases , ': ' , answer : 0 : 0);
          readln(N);
      end;
End.