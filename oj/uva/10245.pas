Const
    InFile     = 'p10245.in';
    Limit      = 10000;
    LimitTimes = 10000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    answer     : double;
    N          : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(N);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do
      readln(data[i].x , data[i].y);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].x > key.x) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].x < key.x) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure rotate(p : Tpoint; cosA , sinA : double; var np : Tpoint);
begin
    np.x := p.x * cosA - p.y * sinA;
    np.y := p.x * sinA + p.y * cosA;
end;

function min(a , b : double) : double;
begin
    if a < b then min := a else min := b;
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

procedure work;
var
    cosA , sinA
               : double;
    p1 , p2 ,
    i , j      : longint;
begin
    cosA := cos(12345.678); sinA := sin(12345.678);
    for i := 1 to N do
      rotate(data[i] , cosA , sinA , data[i]);

    qk_sort(1 , N);
    answer := 10000;
    if N < 2 then exit;
    for i := 1 to LimitTimes do
      begin
          repeat
            p1 := random(N) + 1; p2 := random(N) + 1;
          until p1 <> p2;
          answer := min(answer , dist(data[p1] , data[p2]));
      end;

    for i := 2 to N do
      begin
          j := i - 1;
          while (j > 0) and (data[i].x - data[j].x < answer) do
            begin
                answer := min(answer , dist(data[i] , data[j]));
                dec(j);
            end;
      end;
end;

procedure out;
begin
    if answer >= 10000 - minimum
      then writeln('INFINITY')
      else writeln(answer : 0 : 4);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.