#include<iostream>
#include<stdio.h>
#include<queue>
using namespace std;
typedef struct

{
	int q;int w;
	int e;int p;
}point;
queue<point> y;
int fx[6][3]={0,1,0,0,0,-1,0,-1,0,0,0,1,1,0,0,-1,0,0};
char s[35][35][35];
int map[35][35][35];
int a,b,c;
void dfs(point r);
int co;
int main()
{
	while(cin>>a>>b>>c&&a!=0)
	{
		co=0;
		for(int i=0;i<a;i++){
			gets(s[i][0]);
		    for(int j=0;j<b;j++)
				gets(s[i][j]);
		}
		point r;
		for(int i=0;i<a;i++)
			for(int j=0;j<b;j++)
				for(int k=0;k<c;k++){
					map[i][j][k]=0;
					if(s[i][j][k]=='S'){
						r.q=i;r.w=j;r.e=k;r.p=0;
					}
				}
		map[r.q][r.w][r.e]=1;
		dfs(r);
		if(co==0)cout<<"Trapped!"<<endl;
		else cout<<"Escaped in "<<co<<" minute(s)."<<endl;
	}
	return 0;
}
void dfs(point r)
{
	if(s[r.q][r.w][r.e]=='E') co=r.p;
	else{
		for(int i=0;i<6;i++){
			point temp=r;
			temp.q+=fx[i][0];temp.w+=fx[i][1];temp.e+=fx[i][2];temp.p++;
			if(temp.q<a&&temp.q>=0&&temp.w<b&&temp.w>=0&&temp.e<c&&temp.e>=0&&s[temp.q][temp.w][temp.e]!='#'&&map[temp.q][temp.w][temp.e]==0){
                map[temp.q][temp.w][temp.e]=1;
				y.push(temp);
			}
		}
		while(!y.empty()){
			point temp2;
			temp2=y.front();
			y.pop();
			dfs(temp2);
		}
	}
}