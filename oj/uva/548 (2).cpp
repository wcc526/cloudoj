#include <stdio.h>

#include <string.h>

#include <ctype.h>

#include <limits.h>

#define len 10005

int s1[len],s2[len],n,min,leaf;

int intchr(int s2[],int num,int n)

{

    int i;

    for(i=0;i<n;i++)

        if(num==s2[i])

            return i;

}

void search_tree(int n,int sum,int s1[],int s2[])

{

    if(n<=0)

        return ;

    sum+=s1[n-1];

    if(n==1)

    {

        if(min>sum)

        {

            min=sum;

            leaf=s1[n-1];

        }

        else if(min==sum) //if min==sum the get the minimun to the leaf value

            if(leaf>s1[n-1])

                leaf=s1[n-1];

            return ;

    }

    int p=intchr(s2,s1[n-1],n); //search the number of s1[n-1] in s2,and return the index

    search_tree(p,sum,s1,s2); //search left tree

    search_tree(n-p-1,sum,s1+p,s2+p+1); //search right tree

}

int main(void)

{

    char ch;

    int i,sum;

    for(n=sum=0;scanf("%d%c",&s2[n++],&ch)==2;)

    {

        if(ch=='\n')

        {

            for(i=0;i<n;i++)

                scanf("%d",&s1[i]);

            min=leaf=INT_MAX; //initialize the min and leaf

            search_tree(n,0,s1,s2); //here 0 is the sum value

            printf("%d\n",leaf);

            n=0; //don't remember to set the variable n=0;

        }

    }

    return 0;

}