Var
    count      : longint;
    N , A      : extended;

procedure process;
var
    s1 , s2 , s3
               : extended;
begin
    s1 := pi;
    s2 := sin(2 * pi / N) * N / 2;
    s3 := sqr(cos(pi / N)) * pi;
    writeln((s1 - s2) / s2 * A : 0 : 5 , ' ' , (s2 - s3) / s2 * A : 0 : 5);
end;

Begin
    count := 0;
    readln(N , A);
    while N >= 3 do
      begin
          inc(count);
          write('Case ' , count , ': ');
          process;
          readln(N , A);
      end;
End.