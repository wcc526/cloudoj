Const
    InFile     = 'p10715.in';
    OutFile    = 'p10715.out';
    LimitW     = 5000;
    Limit      = 100;

Type
    Topt       = array[1..Limit , 0..LimitW + 100] of longint;
    Tdata      = array[1..Limit] of longint;
    Tsource    = array[1..Limit] of double;

Var
    source     : Tsource;
    data       : Tdata;
    opt        : Topt;
    N , tot    : longint;

function init : boolean;
var
    i          : longint;
    sum        : extended;
begin
    fillchar(source , sizeof(source) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    sum := 0;
    readln(N);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do
      begin read(source[i]); sum := sum + source[i]; end;
    tot := 0;
    for i := 1 to N do
      begin
          data[i] := round(source[i] / sum * LimitW);
          inc(tot , data[i]);
      end;
end;

function dfs_find(p , sum : longint) : longint;
var
    tmp        : longint;
begin
    if p > N
      then exit(abs(sum * 2 - tot))
      else if opt[p , sum] <> -1
             then exit(opt[p , sum])
             else begin
                      opt[p , sum] := dfs_find(p + 1 , sum);
                      tmp := dfs_find(p + 1 , sum + data[p]);
                      if tmp < opt[p , sum] then opt[p , sum] := tmp;
                      exit(opt[p , sum]);
                  end;
end;

function dfs_print(p , sum : longint) : longint;
begin
    if p > N
      then exit
      else begin
               if dfs_find(p + 1 , sum) < dfs_find(p + 1 , sum + data[p])
                 then dfs_print(p + 1 , sum)
                 else begin
                          write(p , ' ');
                          dfs_print(p + 1 , sum + data[p]);
                      end;
            end;
end;

procedure work;
begin
    dfs_find(1 , 0);
    dfs_print(1 , 0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      work;
End.