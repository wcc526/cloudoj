#include <algorithm>
#include <cstdio>
#include <functional>
#include <queue>
#include <vector>

using namespace std;

int main()
{
  priority_queue<int, vector<int>, greater<int> > q;
  int a[5842], i = 0, n = 0;

  q.push(1);

  while (n < 5842) {
    while (i == q.top())
      q.pop();
    i = q.top();
    q.pop();
    if (i % 2 == 0 || i == 1) {
      if (i < 1073741824) {
        q.push(i << 1);
        goto l3;
      }
    } else if (i % 3 == 0) {
    l3:
      if (i < 715827882) {
        q.push(i * 3);
        goto l5;
      }
    } else if (i % 5 == 0) {
    l5:
      if (i < 429496729) {
        q.push(i * 5);
        goto l7;
      }
    } else {
    l7:
      if (i < 306783378)
        q.push(i * 7);
    }
    a[n++] = i;
  }

  while (scanf("%d", &n) == 1 && n)
    printf("The %d%s humble number is %d.\n", n,
           (n / 10) % 10 == 1 ? "th" :
           n % 10 == 1 ? "st" :
           n % 10 == 2 ? "nd" :
           n % 10 == 3 ? "rd" : "th",
           a[n - 1]);

  return 0;
}
