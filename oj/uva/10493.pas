Var
    N , M      : longint;
Begin
    readln(N , M);
    while N <> 0 do
      begin
          write(N , ' ' , M , ' ');
          if N = 1
            then if M = 1
                   then writeln('Multiple')
                   else writeln('Impossible')
            else if (M - 1) mod (N - 1) = 0
                   then writeln((M - 1) div (N - 1) + M)
                   else writeln('Impossible');
          readln(N , M);
      end;
End.