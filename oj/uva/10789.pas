Const
    InFile     = 'p10789.in';
    OutFile    = 'p10789.out';
    Limit      = 2000;
    N          = 62;

Type
    Tcount     = array[#1..#255] of longint;
    Torder     = array[1..N] of char;

Var
    count      : Tcount;
    order      : Torder;
    totCases ,
    Cases      : longint;

procedure init;
var
    ch         : char;
begin
    inc(Cases);
    fillchar(count , sizeof(count) , 0);
    while not eoln do
      begin
          read(ch);
          inc(count[ch]);
      end;
    readln;
end;

procedure work;
var
    i          : longint;
begin
    for i := 0 to 9 do order[i + 1] := chr(i + 48);
    for i := 1 to 26 do order[i + 10] := chr(i + ord('A') - 1);
    for i := 1 to 26 do order[i + 36] := chr(i + ord('a') - 1);
end;

function prime(x : longint) : boolean;
var
    i          : longint;
begin
    if x < 2 then exit(false);
    for i := 2 to x - 1 do
      if x mod i = 0 then
        exit(false);
    exit(true);
end;

procedure out;
var
    i , ok     : longint;
begin
    write('Case ' , cases , ': ');
    ok := 0;
    for i := 1 to N do
      if prime(count[order[i]]) then
        begin
            write(order[i]);
            ok := 1;
        end;
    if ok = 0 then write('empty');
    writeln;
end;

Begin
    readln(totCases);
    cases := 0;
    while cases < totCases do
      begin
          init;
          work;
          out;
      end;
End.