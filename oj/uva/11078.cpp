/*
 * uva_11078.cpp
 *
 *  Created on: 2012-12-11
 *      Author: Administrator
 */
#include <cstdio>
#include <algorithm>
using namespace std;
int A[100000],n;

/*
int main(){
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=0;i<n;++i) scanf("%d",&A[i]);
		int ans=A[0]-A[1];
		for(int i=0;i<n;++i)
			for(int j=i+1;j<n;++j)
				ans=max(ans,A[i]-A[j]);
		printf("%d\n",ans);
	}
	return 0;
}
*/
int main()
{
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=0;i<n;++i) scanf("%d",&A[i]);
		int ans=A[0]-A[1];
		int MaxAi=A[0];
		for(int j=1;j<n;++j){
			ans=max(ans,MaxAi-A[j]);
			MaxAi=max(A[j],MaxAi);
		}
		printf("%d\n",ans);
	}
	return 0;
}





