{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10676.in';
    OutFile    = 'p10676.out';
    Q          : array[1..4] of extended
               = (1 , 2 , 2.5 , 5);
    minimum    = 1e-10;
    

Type
    Tans       = record
                     space                   : extended;
                     Len , precise , 
                     Left , Right            : longint;
                 end;

Var
    ans        : Tans;
    cases , K  : longint;      
    min , max  : extended;

procedure init;
var
    i , N      : longint;
    num        : extended;
begin
    dec(cases);
    read(N , K);
    min := 0; max := 0;
    for i := 1 to N do
      begin
          read(num);
          if num < min then min := num;
          if num > max then max := num;
      end;
end;

function calc(num , space : extended) : longint;
begin
    if num / space > 100000
      then calc := 100000
      else if num < minimum
             then calc := 0
             else calc := trunc(num / space - minimum) + 1;
end;

procedure work;
var
    p1 , p2 ,
    i , j      : longint;
    num , space: extended;
    tmp        : Tans;
begin
    ans.Len := 1000000;
    num := 1e-7; i := 7;
    while num <= 2e6 do
      begin
          for j := 1 to 4 do
            begin
                space := num * Q[j];
                tmp.space := space;
                tmp.Left := -calc(-min , space);
                tmp.Right := calc(max , space);
                tmp.Len := tmp.Right - tmp.Left + 1;
                if j = 3 then tmp.precise := i + 1 else tmp.precise := i;

                p1 := abs(tmp.Len - K); p2 := abs(ans.Len - K);
                if (p1 < p2) or (p1 = p2) and (tmp.Len > ans.Len) then
                  ans := tmp;
            end;
          dec(i); num := num * 10;
      end;
end;

procedure out;
var
    i , p      : longint;
begin
    if ans.precise < 0 then p := 0 else p := ans.precise; 
    for i := ans.Left to ans.Right do
      begin
          write(ans.space * i : 0 : p);
          if i = ans.Right then writeln else write(' ');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
