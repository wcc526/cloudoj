Var
    data       : array[1..3 , 1..3] of char;
    a , b ,
    Cases ,
    i , j      : longint;
    last       : char;

function check_sub : boolean;
var
    ok         : boolean;
begin
    for i := 1 to 3 do
      if (data[i , 1] = data[i , 2]) and (data[i , 1] = data[i , 3]) and (data[i , 1] <> '.') then
        exit(false);
    for i := 1 to 3 do
      if (data[1 , i] = data[2 , i]) and (data[1 , i] = data[3 , i]) and (data[1 , i] <> '.') then
        exit(false);
    if (data[1 , 1] = data[2 , 2]) and (data[2 , 2] = data[3 , 3]) and (data[1 , 1] <> '.') then
      exit(false);
    if (data[1 , 3] = data[2 , 2]) and (data[2 , 2] = data[3 , 1]) and (data[2 , 2] <> '.') then
      exit(false);
    exit(true);
end;

procedure check;
var
    i , j      : longint;
begin
    for i := 1 to 3 do
      for j := 1 to 3 do
        if data[i , j] = last then
          begin
              data[i , j] := '.';
              if check_sub then
                begin
                    writeln('yes');
                    exit;
                end;
              data[i , j] := last;
          end;
    writeln('no');
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          for i := 1 to 3 do
            begin
                for j := 1 to 3 do
                  read(data[i , j]);
                readln;
            end;
          readln;
          a := 0; b := 0;
          for i := 1 to 3 do
            for j := 1 to 3 do
              begin
                  if data[i , j] = 'X' then inc(a);
                  if data[i , j] = 'O' then inc(b);
              end;
          if (a < b) or (a > b + 1) then
            begin writeln('no'); continue; end;
          if a = b then last := 'O' else last := 'X';
          if check_sub then
            begin writeln('yes'); continue; end;
          check;
      end;
End.