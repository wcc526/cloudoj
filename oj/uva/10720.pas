Const
    Limit      = 10000;

Type
    Tnums      = array[0..Limit] of longint;

Var
    nums       : Tnums;
    answer     : boolean;
    N          : longint;

function init : boolean;
var
    i , p      : longint;
begin
    answer := false;
    read(N); if N = 0 then exit(false);
    init := true;
    fillchar(nums , sizeof(nums) , 0);
    for i := 1 to N do
      begin
          read(p);
          if (p >= N) or (p < 0) then begin readln; exit; end;
          inc(nums[p]);
      end;
    answer := true;
end;

procedure work;
var
    i , j , last ,
    count      : longint;
begin
    if not answer then exit;
    answer := false;
    i := N - 1;
    while i > 0 do
      begin
          if nums[i] > 0 then
            begin
                dec(nums[i]); count := i; last := 0;
                for j := i downto 1 do
                  begin
                      if nums[j] - last > count
                        then begin
                                 dec(nums[j] , count); inc(nums[j - 1] , count);
                                 count := 0;
                                 break;
                             end
                        else begin
                                 last := nums[j] - last;
                                 dec(nums[j] , last); inc(nums[j - 1] , last);
                                 dec(count , last);
                             end;
                  end;
                if count > 0 then exit;
            end
          else
            dec(i);
      end;
    answer := true;
end;

Begin
    while init do
      begin
          work;
          if answer
            then writeln('Possible')
            else writeln('Not possible');
      end;
End.