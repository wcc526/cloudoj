#include <math.h>
#include <stdio.h>

typedef struct {
  double x, y;
} pt;

typedef struct {
  int u, v;
  double w;
} edge;

typedef struct stack {
  int lo;
  int hi;
} stack;

#define MIN_SIZE   16
#define STACK_MAX  13
#define STACK_USED (s < t)
#define PUSH(l, h) (t->lo = (l), t->hi = (h), ++t)
#define POP(l, h)  (--t, (l) = t->lo, (h) = t->hi)
#define SWAP(a, b) (e = (a), (a) = (b), (b) = e)

void quicksort(edge *v, int n)
{
  int l = 0, h = n - 1, m, a, b;
  stack s[STACK_MAX], *t = s;
  double p;
  edge e;

  if (n < 2)
    return;

  PUSH(l, h);

  while (STACK_USED) {
    POP(l, h);

    m = l + ((h - l) >> 1);
    if (v[m].w < v[l].w)
      SWAP(v[m], v[l]);
    if (v[h].w < v[m].w) {
      SWAP(v[h], v[m]);
      if (v[m].w < v[l].w)
        SWAP(v[m], v[l]);
    }
    p = v[m].w;
    a = l + 1;
    b = h - 1;

    do {
      do {
        for (; v[a].w < p; ++a);
        for (; v[b].w > p; --b);
        if (a < b) {
          SWAP(v[a], v[b]);
          ++a;
          --b;
        } else if (a == b) {
          ++a;
          --b;
          break;
        }
      } while (a < b);
      if (b - l > h - a) {
        if (b - l > MIN_SIZE)
          PUSH(l, b);
        l = a;
      } else {
        if (h - a > MIN_SIZE)
          PUSH(a, h);
        h = b;
      }
    } while (h - l > MIN_SIZE);
  }

  for (a = 1; a < n; ++a) {
    for (e = v[b = a]; b-- > 0 && v[b].w > e.w;)
      v[b + 1] = v[b];
    v[b + 1] = e;
  }
}

double dist(pt a, pt b)
{
  return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

int root(int *s, int i)
{
  return s[i] < 0 ? i : (s[i] = root(s, s[i]));
}

int main()
{
  int c, i, j, k, n, s[100], u, v;
  pt f[100];
  edge e[4950];
  double w;

  scanf("%d", &c);
  while (c--) {
    scanf("%d", &n);
    for (i = n; i--;)
      scanf("%lf %lf", &f[i].x, &f[i].y), s[i] = -1;
    for (k = 0, i = n; --i;)
      for (j = i; j--;)
        e[k].u = i, e[k].v = j, e[k++].w = dist(f[i], f[j]);
    quicksort(e, k);
    for (w = 0, i = 0, k = 1; k < n; ++i)
      if ((u = root(s, e[i].u)) ^ (v = root(s, e[i].v)))
        s[u] += s[v], s[v] = u, w += e[i].w, ++k;
    printf("%.2lf\n", w);
    if (c)
      putchar('\n');
  }

  return 0;
}
