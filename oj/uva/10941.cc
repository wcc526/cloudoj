#include <algorithm>
#include <iostream>
#include <iterator>
#include <queue>
#include <set>
#include <string>

using namespace std;

class state {
public:
  int d;
  string s;
  state(int d, string s) : d(d), s(s) {
  }
};

int main(void)
{
  int k, n;
  string t, x, y;
  set<string> s, w;
  queue<state> q;

  cin >> n;
  while (n--) {
    cin >> x;
    cin >> y;
    cin >> k;
    while (k--) {
      cin >> t;
      w.insert(t);
    }

    if (x == y) {
      cout << "0" << endl;
      w.clear();
      continue;
    }

    if (x.length() < y.length())
      t = y.substr(x.length());
    else
      t = x.substr(y.length());
    q.push(state(0, t));
    s.insert(t);

    while (q.empty() == false) {
      state &c = q.front();
      for (set<string>::iterator i = w.lower_bound(c.s);
           i != w.end() && i->substr(0, c.s.length()) == c.s; ++i) {
        if (c.s.length() < i->length())
          t = i->substr(c.s.length());
        else
          t = c.s.substr(i->length());
        if (t.length() == 0) {
          cout << c.d + 1 << endl;
          goto done;
        }
        if (s.find(t) == s.end()) {
          q.push(state(c.d + 1, t));
          s.insert(t);
        }
      }
      for (int i = c.s.length(); --i;) {
        set<string>::iterator m = w.find(c.s.substr(0, i));
        if (m != w.end()) {
          if (c.s.length() < m->length())
            t = m->substr(c.s.length());
          else
            t = c.s.substr(m->length());
          if (t.length() == 0) {
            cout << c.d + 1 << endl;
            goto done;
          }
          if (s.find(t) == s.end()) {
            q.push(state(c.d + 1, t));
            s.insert(t);
          }
        }
      }
      q.pop();
    }

    cout << "-1" << endl;

  done:
    while (q.size() > 0)
      q.pop();
    s.clear();
    w.clear();
  }

  return 0;
}
