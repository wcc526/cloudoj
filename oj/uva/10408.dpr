{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10408.in';
    OutFile    = 'p10408.out';

Var
    N , K      : longint;

procedure init;
begin
    readln(N , K);
end;

function dfs(a , b , c , d : longint) : boolean;
begin
    dfs := false;
    if (b + d > N) then exit;
    dfs := true;
    if dfs(a , b , a + c , b + d) then exit;
    dec(K);
    if K = 0 then
      begin
          writeln(a + c , '/' , b + d);
          exit;
      end;
    if dfs(a + c , b + d , c , d) then exit;
    dfs := false;
end;

procedure work;
begin
    if not dfs(0 , 1 , 1 , 1) then writeln('1/1');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
