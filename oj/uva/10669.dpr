{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10669.in';
    OutFile    = 'p10669.out';
    LimitLen   = 50;
    LimitSave  = 50;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure readnum;
                     procedure print;
                     function bigger(num : lint) : boolean;
                     procedure wipezero;
                     procedure minus(num1 , num2 : lint);
                     procedure multi(num : longint);
                     procedure div2;
                 end;
    Tanswer    = record
                     tot      : longint;
                     data     : array[1..LimitSave] of lint;
                 end;

Var
    N          : lint;
    answer     : Tanswer;

procedure lint.init;
begin
    len := 0;
    fillchar(data , sizeof(data) , 0);
end;

procedure lint.readnum;
var
    ch         : char;
    i , tmp    : longint;
begin
    init;
    while not eoln do
      begin
          inc(len);
          read(ch);
          data[len] := ord(ch) - ord('0');
      end;
    readln;
    for i := 1 to len div 2 do
      begin
          tmp := data[i]; data[i] := data[len - i + 1]; data[len - i + 1] := tmp;
      end;
end;

procedure lint.print;
var
    i          : longint;
begin
    if len = 0 then write(0);
    for i := len downto 1 do write(data[i]);
end;

function lint.bigger(num : lint) : boolean;
var
    i          : longint;
begin
    if len <> num.len
      then bigger := len > num.len
      else begin
               bigger := false;
               for i := len downto 1 do
                 if data[i] <> num.data[i] then
                   begin
                       bigger := data[i] > num.data[i];
                       exit;
                   end;
           end;
end;

procedure lint.wipezero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.minus(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    jw := 0;
    for i := 1 to num1.len do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0;
          if tmp < 0 then begin jw := 1; inc(tmp , 10); end;
          data[i] := tmp;
      end;
    len := num1.len;
    wipezero;
end;

procedure lint.multi(num : longint);
var
    i , jw ,
    tmp        : longint;
begin
    jw := 0; i := 1;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
    wipezero;
end;

procedure lint.div2;
var
    tmp , i ,
    last       : longint;
begin
    i := len; last := 0;
    while i > 0 do
      begin
          tmp := last * 10 + data[i];
          data[i] := tmp div 2;
          last := tmp mod 2;
          dec(i);
      end;
    wipezero;
end;

function init : boolean;
begin
    N.readnum;
    if (N.len = 1) and (N.data[1] = 0) then init := false else init := true; 
end;

procedure work;
var
    power , 
    one        : lint;
begin
    one.init;
    one.len := 1; one.data[1] := 1;
    fillchar(answer , sizeof(answer) , 0);
    N.minus(N , one);
    power := one;
    while (N.len > 1) or (N.data[1] <> 0) do
      begin
          if odd(N.data[1]) then
            begin
                inc(answer.tot);
                answer.data[answer.tot] := power;
            end;
          N.div2;
          power.multi(3);
      end;
end;

procedure out;
var
    i          : longint;
begin
    write('{ ');
    for i := 1 to answer.tot do
      begin
          answer.data[i].print;
          if i <> answer.tot then write(',');
          write(' '); 
      end;
    writeln('}');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
