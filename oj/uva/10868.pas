Const
    G          = 9.81;

Var
    max , now ,
    k , L , s , w
               : double;

Begin
    readln(k , L , s , w);
    while k + L + s + w > 0 do
      begin
          max := w * 50;
          if L > s
            then now := G * w * s
            else now := G * w * s - 0.5 * k * sqr(L - s);
          if now < 0
            then writeln('Stuck in the air.')
            else if now <= max
                   then writeln('James Bond survives.')
                   else writeln('Killed by the impact.');
          readln(k , L , s , w);
      end;
End.