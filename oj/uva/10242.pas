Const
    InFile     = 'p10242.in';
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : double;
                 end;

Var
    answer ,
    p1 , p2 ,
    p3 , p4    : Tpoint;

procedure init;
begin
    readln(p1.x , p1.y , p2.x , p2.y , p3.x , p3.y , p4.x , p4.y);
end;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function same_p(p1 , p2 : Tpoint) : boolean;
begin
    exit(zero(p1.x - p2.x) and zero(p1.y - p2.y));
end;

procedure swap(var p1 , p2 : Tpoint);
var
    tmp        : Tpoint;
begin
    tmp := p1; p1 := p2; p2 := tmp;
end;

procedure work;
begin
    if same_p(p1 , p3) or same_p(p1 , p4)
      then
      else swap(p1 , p2);
    if same_p(p1 , p3) or same_p(p2 , p3)
      then
      else swap(p3 , p4);
    answer.x := p2.x + p4.x - p1.x; answer.y := p2.y + p4.y - p1.y;
end;

procedure out;
begin
    writeln(answer.x + minimum : 0 : 3 , ' ' , answer.y + minimum : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            while eoln and not eof do readln;
            if eof then break;
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
