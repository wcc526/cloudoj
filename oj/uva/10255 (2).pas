{$R-,Q-,S-}
const
    dirx       : array[1..8] of longint = (-1 , -1 , -2 , -2 , 2 , 2 , 1 , 1);
    diry       : array[1..8] of longint = (2 , -2 , -1 , 1 , 1 , -1 , -2 , 2);
    N          = 50;

    type
    Tdata      =  array[-1..60 , -1..60] of longint;

Var
    visited    : array[-1..60 , -1..60] of boolean;
    data       : Tdata;
    answer     : array[1..50] of Tdata;
    M , len ,
    x0 , y0 ,
    count , tx , ty ,
    i , j      : longint;

procedure print;
var
    num ,
    i, j       : longint;
begin
    num := data[tx , ty];
   for i := 1 to M do
      begin
          for j := 1 to M do
            if data[i , j] > 0 then
              write((data[i , j] - num + 1 + M * M - 1) mod (M * M) + 1 : 5)
            else
              write(0 : 10);
          writeln;
      end;
end;

function check(x , y : longint) : boolean;
var
    i , nx , ny
               : longint;
begin
    inc(count); visited[x , y] := true; check := false;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx = x0) and (ny = y0) then check := true;
          if not visited[nx , ny] and (data[nx , ny] = 0) then
            if check(nx , ny) then
              check := true;
      end;
end;

function calc(x , y : longint) : longint;
var
    res ,
   nx , ny , i: longint;
begin
    res := 0;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if data[nx , ny] = 0 then
            inc(res);
      end;
    exit(res);
end;

function dfs(x , y , step : longint) : boolean;
var
    tot , tmp ,
    nx , ny    : longint;
    i          : longint;
    num, degree: array[1..8] of longint;
begin
    count := 0; dfs := false;
    fillchar(visited , sizeof(visited) , 0);
    if not check(x , y) or (count <> Len - step  + 1) then
      exit;
    data[x , y] := step;// data[y , 8 - x + 1] := -step;
//    data[8 - x + 1 , 8 - y + 1] := -step;
//    data[8 - y + 1 , x] := -step;
    tot := 0;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx = y0) and (ny = x0) and (step = Len) then begin answer[M] := data; exit(true); end;
      end;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if data[Nx , ny] = 0 then
            begin
                inc(tot);
                num[tot] := i;
                degree[tot] := calc(nx , ny);
            end;
      end;
    for i := 1 to tot do
      for j := i + 1 to tot do
        if degree[i] > degree[j] then
          begin
              tmp := num[i]; num[i] := num[j]; num[j] := tmp;
              tmp := degree[i]; degree[i] := degree[j]; degree[j] := tmp;
          end;
    for i := 1 to tot do
      begin
          nx := x + dirx[num[i]]; ny := y + diry[num[i]];
          if step < Len then
            if data[nx , ny] = 0 then
              begin
                  dfs(nx , ny , step + 1);
              end;
      end;
    data[x , y] := 0;// data[y , 8 - x + 1] := 0;
//    data[8 - x + 1 , 8 - y + 1] := 0;
//    data[8 - y + 1 , x] := 0;
end;

Begin

for M := 6 to 50 do if not odd(M) then begin

  x0 := 1; y0 := 1; Len := M * M;
    fillchar(data , sizeof(data) , $FF);
    for i := 1 to M do
      for j := 1 to M do
//        if (i <= 2) or (i >= 8 - 1) or (j <= 2) or (j >= 8 - 1) then
          data[i , j] := 0;
    dfs(x0 , y0 , 1);
end;
while not eof do
 begin
 readln(M , tx , ty);
if odd(M) or (M < 6) then
  writeln('No Circuit Tour.')
  else begin data := answer[M]; print; end;
end;
End.
