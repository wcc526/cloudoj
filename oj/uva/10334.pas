Const
    LimitLen   = 400;
    Limit      = 1000;

Type
    lint       = object
                     Len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    data       : Tdata;
    N          : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    Len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          data[i] := tmp mod 10;
          jw := tmp div 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := Len downto 1 do write(data[i]);
end;

Begin
    data[0].init; data[0].data[1] := 1;
    data[1].init; data[1].data[1] := 2;
    for N := 2 to Limit do data[N].add(data[N - 1] , data[N - 2]);
    while not eof do
      begin
          readln(N);
          data[N].print;
          writeln;
      end;
End.