Const
    InFile     = 'p10344.in';
    N          = 5;

Type
    Tvisited   = array[1..N] of boolean;
    Tdata      = array[1..N] of longint;

Var
    visited    : Tvisited;
    data       : Tdata;
    answer     : boolean;

procedure init;
var
    i          : longint;
begin
    for i := 1 to N do read(data[i]);
    readln;
end;

function dfs(last , step : longint) : boolean;
var
    i          : longint;
begin
    if step > N
      then dfs := last = 23
      else begin
               for i := 1 to N do
                 if not visited[i] then
                   begin
                       visited[i] := true;
                       if dfs(last + data[i] , step + 1) then exit(true);
                       if dfs(last - data[i] , step + 1) then exit(true);
                       if dfs(last * data[i] , step + 1) then exit(true);
                       visited[i] := false;
                   end;
               exit(false);
           end;
end;

procedure work;
var
    i          : longint;
begin
    answer := true;
    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to 5 do
      begin
          visited[i] := true;
          if dfs(data[i] , 2) then exit;
          visited[i] := false;
      end;
    answer := false;
end;

Begin
    while true do
      begin
          init;
          if abs(data[1]) + abs(data[2]) + abs(data[3]) + abs(data[4]) + abs(data[5]) = 0 then
            break;
          work;
          if answer
            then writeln('Possible')
            else writeln('Impossible');
      end;
End.