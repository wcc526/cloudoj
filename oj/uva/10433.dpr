{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10433.in';
    OutFile    = 'p10433.out';
    LimitLen   = 2000;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen + 10] of longint;
                 end;

Var
    N , sqrN   : lint;
    ok         : boolean;
    mod10 , div10
               : array[0..100000] of longint;
    i , j ,
    tmp        : longint;
    ch         : char;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      for i := 0 to 100000 do
        begin
            mod10[i] := i mod 10;
            div10[i] := i div 10;
        end;
      while not eof do
        begin
            N.len := 0;
            while not eoln do
              begin
                  read(ch);
                  inc(N.len); N.data[N.len] := ord(ch) - ord('0');
                  if N.len > LimitLen then break;
              end;
            readln;
            if N.len > LimitLen then
              begin
                  writeln('Not an Automorphic number.');
                  continue;
              end;
            for i := 1 to N.len div 2 do
              begin tmp := N.data[i]; N.data[i] := N.data[N.len - i + 1]; N.data[N.len - i + 1] := tmp; end;

            for i := 1 to N.len do SqrN.data[i] := 0;
            for i := 1 to N.len do
              begin
                  j := 1;
                  if N.data[i] <> 0 then
                    while (i + j - 1 <= N.len) do
                      begin
                          sqrN.data[i + j - 1] := N.data[i] * N.data[j] + sqrN.data[i + j - 1];
                          inc(j);
                      end;
                  inc(sqrN.data[i + 1] , div10[sqrN.data[i]]);
                  sqrN.data[i] := mod10[sqrN.data[i]];
                  if sqrN.data[i] <> N.data[i] then break;
              end;

            ok := false;
            for i := 1 to N.len do if N.data[i] <> 0 then begin ok := true; break; end;
            for i := 1 to N.len do
              if N.data[i] <> sqrN.data[i] then
                begin ok := false; break; end;
            if ok
              then writeln('Automorphic number of ' , N.len , '-digit.')
              else writeln('Not an Automorphic number.');
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
