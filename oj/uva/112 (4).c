#include <stdio.h>

int n;

int r(int s)
{
  int a, b, c;
  while (getchar() ^ '(');
  if (scanf("%d", &c) > 0)
    s += c, a = r(s), b = r(s), c = a < 0 && b < 0 ? s == n : a < b ? b : a;
  else
    c = -1;
  while (getchar() ^ ')');
  return c;
}

int main()
{
  while (scanf("%d", &n) > 0)
    puts(r(0) > 0 ? "yes" : "no");
  return 0;
}
