Const
    Limit      = 10000000;
    LimitSave  = 700000;

Type
    Tdata      = array[0..Limit] of boolean;
    Tprimes    = array[1..LimitSave] of longint;

Var
    data ,
    prime      : Tdata;
    primes     : Tprimes;
    P          : longint;

procedure GetPrimes;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    P := 0;
    for i := 2 to Limit do
      if prime[i] then
        begin
            for j := 2 to Limit div i do
              prime[j * i] := false;
            inc(P); primes[P] := i;
        end;
end;

procedure pre_process;
var
    i , j      : longint;
begin
    GetPrimes;
    fillchar(data , sizeof(data) , 0);
    for i := 0 to trunc(sqrt(Limit)) do
      for j := i to trunc(sqrt(Limit - i * i)) do
        data[i * i + j * j] := true;
end;

procedure main;
var
    s          : string[100];
    ans        : array[1..50] of longint;
    tot , i , j ,
    count ,
    N          : longint;
begin
    while not eof do
      begin
          readln(s);
          if s[length(s)] <> '!'
            then begin
                     val(s , N);
                     if data[N]
                       then writeln('He might be honest.')
                       else writeln('He is a liar.');
                 end
            else begin
                     delete(s , length(s) , 1);
                     val(s , N);
                     tot := 0;
                     i := 1;
                     while (i <= P) and (primes[i] <= N) do
                       begin
                           j := primes[i];
                           count := 0;
                           while j <= N do
                             begin
                                 count := count + N div j;
                                 if N div j >= primes[i]
                                   then j := j * primes[i]
                                   else break;
                             end;
                           if odd(count) then
                             if not data[primes[i]] then
                               if tot < 50 then
                                 begin
                                     inc(tot);
                                     ans[tot] := primes[i];
                                 end
                               else break;
                           inc(i);
                       end;
                     if tot = 0
                       then writeln('He might be honest.')
                       else begin
                                writeln('He is a liar.');
                                for i := 1 to tot do
                                  write(ans[i] , ' ');
                                writeln;
                            end;
                 end;
          writeln;
      end;
end;

Begin
    pre_process;
    main;
End.