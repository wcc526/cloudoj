#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int main()
{
    string line;
    while (getline(cin, line)) {
        int numRows;
        istringstream is(line);
        is >> numRows;
        if (numRows == 0) continue;
        vector<int> numXVec;
        int maxX = 0;
        for (int i=0; i<numRows; ++i) {
            getline(cin, line);
            int numX = 0;
            for (int j=0; j<line.size(); ++j) {
                if (line[j] == 'X') numX++;
            }
            numXVec.push_back(numX);
            if (maxX < numX) maxX = numX;
        }
        int diff = 0;
        for (int i=0; i<numRows; ++i) {
            diff += (maxX - numXVec[i]);
        }
        cout << diff << endl;
    }

    return 0;
}
