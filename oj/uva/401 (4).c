#include <stdio.h>

int main(void)
{
  int i, j, n, t;
  char s[21], m[] = {
    'A', 0, 0, 0, '3', 0, 0, 'H', 'I', 'L', 0, 'J',
    'M', 0, 'O', 0, 0, 0, '2', 'T', 'U', 'V', 'W', 'X',
    'Y', '5', '1', 'S', 'E', 0, 'Z', 0, 0, '8', 0
  };

  while (scanf("%s%n\n", s, &n) == 1) {
    t = 0;
    for (j = n - (i = n >> 1) - 1; i >= 0; --i, ++j)
      if (s[i] != s[j])
        goto l1;
    t |= 1;
  l1:
    for (j = n - (i = n >> 1) - 1; i >= 0; --i, ++j)
      if (s[i] != m[s[j] - (s[j] < 'A' ? 23 : 'A')])
        goto l2;
    t |= 2;
  l2:
    printf("%s -- is ", s);
    switch (t) {
    case 0:
      puts("not a palindrome.");
      break;
    case 1:
      puts("a regular palindrome.");
      break;
    case 2:
      puts("a mirrored string.");
      break;
    case 3:
      puts("a mirrored palindrome.");
      break;
    }
    putchar('\n');
  }

  return 0;
}
