{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10567.in';
    OutFile    = 'p10567.out';
    Limit      = 1000000;
    SegLen     = 1000;
    LimitSeg   = Limit div SegLen + 1;

Type
    Tdata      = array[1..Limit] of byte;
    Tseg       = array[0..LimitSeg , 1..52] of boolean;

Var
    data       : Tdata;
    seg        : Tseg;
    N , Q ,
    totSeg     : longint;

procedure init;
var
    ch         : char;
begin
    fillchar(seg , sizeof(seg) , 0);
    N := 0;
    while not eoln do
      begin
          read(ch);
          inc(N);
          if ch <= 'Z'
            then data[N] := ord(ch) - ord('A') + 1
            else data[N] := ord(ch) - ord('a') + 27;
          seg[(N - 1) div SegLen + 1 , data[N]] := true; 
      end;
    totSeg := (N - 1) div SegLen + 1;
    readln;
end;

procedure workout;
var
    i , first ,
    high , low : longint;
    num        : byte;
    ch         : char;
    find       : boolean;
begin
    readln(Q);
    while Q > 0 do
      begin
          dec(Q);
          first := -1;
          i := 1; find := true; high := 1; low := 1;
          while not eoln do
            begin
                read(ch);
                if ch <= 'Z'
                  then num := ord(ch) - ord('A') + 1
                  else num := ord(ch) - ord('a') + 27;
                while (i <= N) and (data[i] <> num) do
                  begin
                      inc(i);
                      inc(low);
                      if low > SegLen then break;
                  end;
                if i > N then begin find := false; break; end;
                if low > SegLen then
                  begin
                      inc(high);
                      while (high <= totSeg) and not Seg[high , num] do inc(high);
                      if high > totSeg then begin find := false; break; end;
                      low := 1; i := (high - 1) * SegLen + low;
                      while data[i] <> num do begin inc(i); inc(low); end;
                  end;
                inc(i); inc(low); if low > SegLen then begin low := 1; inc(high); end;
                if first = -1 then first := i;
            end;
          if find
            then writeln('Matched ' , first - 2 , ' ' , i - 2)
            else writeln('Not matched');
          readln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      init;
      workout;
//    Close(OUTPUT);
//    Close(INPUT);
End.
