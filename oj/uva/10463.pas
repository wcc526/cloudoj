Const
    InFile     = 'p10463.in';
    OutFile    = 'p10463.out';
    Limit      = 15;
    dirx       : array[1..8] of longint = (-1 , -1 , -3 , -3 , 1 , 1 , 3 , 3);
    diry       : array[1..8] of longint = (-3 , 3 , -1 , 1 , -3 , 3 , -1 , 1);

Type
    Tvisited   = array[1..Limit , 1..Limit] of boolean;

Var
    visited    : Tvisited;
    cases ,
    N , M ,
    x1 , y1 ,
    x2 , y2    : longint;

procedure init;
begin
    inc(cases);
    readln(N , M , x1 , y1 , x2 , y2);
end;

function prime(num : longint) : boolean;
var
    i          : longint;
begin
    if num <= 1
      then exit(false)
      else begin
               for i := 2 to trunc(sqrt(num)) do
                 if num mod i = 0 then
                   exit(false);
               exit(true);
           end;
end;

function bfs : longint;
type
    Tqueue     = array[1..Limit * Limit] of
                   record
                       x , y , step          : longint;
                   end;
var
    queue      : Tqueue;
    open , closed ,
    x , y , i  : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    queue[1].x := x1; queue[1].y := y1; queue[1].step := 0;
    open := 1; closed := 1; visited[x1 , y1] := true;
    while open <= closed do
      begin
          if (queue[open].x = x2) and (queue[open].y = y2) then
            exit(queue[open].step);
          for i := 1 to 8 do
            begin
                x := queue[open].x + dirx[i]; y := queue[open].y + diry[i];
                if not ((x <= 0) or (x > N) or (y <= 0) or (y > M)) then
                  if not visited[x , y] then
                    begin
                        inc(closed);
                        queue[closed].x := x; queue[closed].y := y;
                        queue[closed].step := queue[open].step + 1;
                        visited[x , y] := true;
                    end;
            end;
          inc(open);
      end;
    bfs := -1;
end;

function dfs(x , y , step : longint) : boolean;
var
    i , nx , ny: longint;
begin
    if (x = x2) and (y = y2) or (step = 0) then
      exit((x = x2) and (y = y2) and (step = 0));
    visited[x , y] := true;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if not ((nx <= 0) or (ny <= 0) or (nx > N) or (ny > M)) then
            if not visited[nx , ny] then
              if dfs(nx , ny , step - 1) then
                exit(true);
      end;
    visited[x , y] := false;
    exit(false);
end;

procedure workout;
var
    num , i    : longint;
begin
    inc(x1); inc(y1); inc(x2); inc(y2);
    if (x1 <= 0) or (x1 > N) or (x2 <= 0) or (x2 > N) or
       (y1 <= 0) or (y1 > M) or (y2 <= 0) or (y2 > M) then
      begin
          writeln('Destination is not reachable.');
          exit;
      end;
    num := bfs;
    if (num = -1) or prime(num) then
      begin
          if num = -1
            then writeln('Destination is not reachable.')
            else writeln('The knight takes ' , num , ' prime moves.');
          exit;
      end;
    fillchar(visited , sizeof(visited) , 0);
    for i := 2 to N * M do
      if prime(i) and (odd(i) = odd(num)) then
        if dfs(x1 , y1 , i) then
          begin
              writeln('The knight takes ' , i , ' prime moves.');
              exit;
          end;
    writeln('The knight takes ' , num , ' composite move(s).');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            init;
            write('CASE# ' , cases , ': ');
            workout;
        end;
End.
