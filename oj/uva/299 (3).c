#include <stdio.h>

int main(void)
{
  unsigned short c[50];
  unsigned int i, j, k, n, s, t;

  scanf("%d", &t);

  while (t--) {
    scanf("%d", &n);
    for (s = n; s--;)
      scanf("%hd", &c[s]);
    for (i = n; i--;)
      for (j = 0; j < i; ++j)
        if (c[j] < c[j + 1]) {
          k = c[j]; c[j] = c[j + 1]; c[j + 1] = k;
          ++s;
        }
    printf("Optimal train swapping takes %d swaps.\n", ++s);
  }

  return 0;
}
