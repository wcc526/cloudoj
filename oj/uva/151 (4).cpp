/**
 * UVa 151 Power Crisis
 * Author: chchwy
 * Last Modified: 2011.04.15
 * Blog: http://chchwy.blogspot.com
 */
#include<cstdio>
#include<deque>
#include<algorithm>

int end_with(int M, int total_region) {

    std::deque<int> que;

    for(int i=1; i<=total_region; ++i)
        que.push_back(i);

    int turn_off_region = que.front(); // pop 1
    que.pop_front();

    for(int i=0;i<total_region-1;++i){

        // skip M-1 element
        for(int j=0;j<M-1;++j){
            que.push_back(que.front());
            que.pop_front();
        }
        // turn off next M'th region
        turn_off_region = que.front();
        que.pop_front();
    }
    return turn_off_region;
}

int Min_M(int N) {

    for(int m=1;m<N;++m){
        if(end_with(m, N) == 13)
            return i;
    }
    return 0;
}

int main() {

    int N;
    while(scanf("%d", &N)==1) {
        if(N==0) break;
        printf("%d\n", Min_M(N));
    }
    return 0;
}

