/*
 * uva_10828.cpp
 *
 *  Created on: 2012-12-13
 *      Author: Administrator
 */
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <vector>
#include <vector>
using namespace std;

const double EPS=1e-8;
const int MXN=100+10;
typedef double Mt[MXN][MXN];
Mt A;
int n,d[MXN];
vector<int> prev[MXN];
int inf[MXN];
void Gauss_jordan(Mt A,int n)
{
	int i,j,k,r;
	for(i=0;i<n;++i)
	{
		r=i;
		for(j=i+1;j<n;++j)
			if(fabs(A[j][i]>fabs(A[r][i]))) r=j;
		if(fabs(A[r][j]<EPS)) continue;
		if(r!=i) for(j=0;j<=n;++j)
			swap(A[r][j],A[i][j]);
		for(k=0;k<n;++k)
			if(k!=i)
				for(j=n;j>=i;--j)
					A[k][j]-=A[k][i]/A[i][i]*A[i][j];
	}
}
int main()
{
	int kase=0;
	while(~scanf("%d",&n)&&n)
	{
		memset(d,0,sizeof(d));
		for(int i=0;i<n;++i)
			prev[i].clear();
		int a,b;
		while(~scanf("%d%d",&a,&b)&&a)
		{
			--a;--b;
			d[a]++;
			prev[b].push_back(a);
		}
		
		memset(A,0,sizeof(A));
		for(int i=0;i<n;++i)
		{
			A[i][i]=1;
			for(int j=0;j<prev[i].size();++j)
				A[i][prev[i][j]]-=1.0/d[prev[i][j]];
			if(i==0) A[i][n]=1;
		}
		
		Gauss_jordan(A,n);
		memset(inf,0,sizeof(inf));
		for(int i=n-1;i>=0;--i)
		{
			if(fabs(A[i][i])<EPS&&fabs(A[i][n])>EPS) inf[i]=1;
			for(int j=i+1;j<n;++j)
				if(fabs(A[i][j])>EPS&&inf[j]) inf[i]=1;
		}
		int q,u;
		scanf("%d",&q);
		printf("Case #%d:\n",++kase);
		while(--q)
		{
			scanf("%d",&u);
			--u;
			if(inf[u]) printf("infinity\n");
			else printf("%.3lf\n",fabs(A[u][u])<EPS?0.0:A[u][n]/A[u][u]);
		}
	}
	return 0;
}







