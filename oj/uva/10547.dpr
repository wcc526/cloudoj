{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10547.in';
    OutFile    = 'p10547.out';

Var
    K , N , base ,
    answer     : extended;
    t , cases  : longint;

function init : boolean;
begin
    inc(cases);
    readln(K , N , t);
    init := (K <> 0);
end;

procedure work;
var
    i          : longint;
begin
    base := 1; for i := 1 to t do base := base * 10;
    answer := 1;
    K := K - int(K / base) * base;
    while N <> 0 do
      begin
          if int(N / 2) * 2 <> N
            then answer := answer * K - int(answer * K / base) * base;
          K := K * K - int(K * K / base) * base;
          N := int(N / 2);
      end;
end;

procedure print(num : extended);
begin
    if num = 0
      then exit
      else begin
               print(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure out;
begin
    write('Case #' , cases , ': ');
    print(answer);
    if answer = 0 then write(0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
