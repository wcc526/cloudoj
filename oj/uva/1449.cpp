/*****************************************
* SDU_ACM 2012
* 
* By SDU_Phonism
*****************************************/
#include <map>
#include <set>
#include <list>
#include <ctime>
#include <cmath>
#include <stack>
#include <queue>
#include <bitset>
#include <vector>
#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define REP(i, n) for (int i = 0; i < n; i++)
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define MEM(a) memset(a, 0, sizeof(a))
#define MEME(a) memset(a, -1, sizeof(a))
#define MEMX(a) memset(a, 0x7f, sizeof(a));

typedef long long LL;
typedef unsigned long long ULL;
typedef unsigned int UINT;

template<class T> inline void checkMin(T &a, T b) {
    if (b < a) a = b;
}
template<class T> inline void checkMax(T &a, T b) {
    if (a < b) a = b;
}
template<class T> inline T Min(T a, T b) {
    return a < b ? a : b;
}
template<class T> inline T Max(T a, T b) {
    return a > b ? a : b;
}
template<class T> inline T Myabs(T a) {
    return a > 0 ? a : -a;
}

const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
const int inf = 0x3f3f3f3f;
const long long linf = 1LL << 60;
const double pi = acos(-1.0);
const double eps = 1e-9;
const int maxn = 200;
const int maxp = 100;
const int maxs = 256;
const int maxl = 1001000;

int n;
char pattern[200][100];

//this template is copied from UESTC_Izayoi
struct trie_AC {
    int next[maxp*maxn][maxs];
    int fail[maxp*maxn];
    int end[maxp*maxn], root, L;
    int newnode(int node) {
        for (int i = 0; i < 256; i++)
            next[node][i] = -1;
        end[node] = -1;
        return node;
    }

    void init() {
        L = 0;
        root = newnode(L++);
    }

    void insert(char s[], int id) {
        int len = strlen(s);
        int now = root;
        for (int i = 0; i < len; i++) {
            if (next[now][s[i]-'a'] == -1)
                next[now][s[i]-'a'] = newnode(L++);
            now = next[now][s[i]-'a'];
        }
        end[now] = id;
    }

    void build() {
        queue<int> Q;
        fail[root] = root;
        for (int i = 0; i < 26; i++)
            if (next[root][i] != -1) {
                fail[next[root][i]] = root;
                Q.push(next[root][i]);
            }
            else
                next[root][i] = root;
        while (!Q.empty()) {
            int now = Q.front();
            Q.pop();
            for (int i = 0; i < 256; i++)
                if (next[now][i] != -1) {
                    fail[next[now][i]] = next[fail[now]][i];
                    Q.push(next[now][i]);
                }
                else
                    next[now][i] = next[fail[now]][i];
        }
    }

    int res[200];
    void query(char buf[]) {
        int len = strlen(buf), now = 0;
        memset(res, 0, sizeof(res));
        for (int i = 0; i < len; i++) {
            now = next[now][buf[i]-'a'];
            for (int failnow = now; failnow != root; failnow = fail[failnow]) {
                if (end[failnow] != -1)
                    res[end[failnow]]++;
            }
        }
        set<int> adj;
        int ans = 0;
        for (int i = 0; i < n; i++) {
            if (res[i] > ans) {
                ans = res[i]; adj.clear(); 
                adj.insert(i);
            }
            if (res[i] == ans) adj.insert(i);
        }
        cout << ans << endl;
        set<int>::iterator it;
        for (it = adj.begin(); it != adj.end(); it++)
            cout << pattern[*it] << endl;
    }
}ac;

char buf[maxl];

int main() {
    while (scanf("%d", &n) != EOF) {
        if (n == 0) break;
        ac.init();
        for (int i = 0; i < n; i++) {
            scanf("%s", pattern[i]);
            ac.insert(pattern[i], i);
        }
        scanf("%s", buf);
        ac.build(); ac.query(buf);
    }
    return 0;
}