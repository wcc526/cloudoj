Var
    s          : string[26];
    calc       : array[0..26] of extended;
    answer     : array[1..26] of longint;
    visited    : array[1..26] of boolean;
    count ,
    N , i , j ,
    k          : longint;
    p , tmp    : extended;
Begin
    calc[0] := 1;
    for i := 1 to 26 do calc[i] := calc[i - 1] * i;

    readln(count);
    while Count > 0 do
      begin
          readln(s);
          N := length(s);
          readln(p); answer[1] := 1;
          for i := 2 to N do
            begin
                tmp := calc[N] / calc[i];
                k := trunc((p - 1) / tmp);
                p := p - k * tmp;
                for j := i - 1 downto k + 1 do
                  answer[j + 1] := answer[j];
                answer[k + 1] := i;
            end;
          for i := 1 to N do
            write(s[answer[i]]);
          writeln;
          dec(count);
      end;
End.