Const
    InFile     = 'p10381.in';
    OutFile    = 'p10381.out';
    Limit      = 40;
    Maximum    = 100000;
    dirx       : array[1..4] of longint = (-1 , 1 , 0 , 0);
    diry       : array[1..4] of longint = (0 , 0 , -1 , 1);

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit , 1..Limit] of char;
    Tshortest  = array[1..Limit , 1..Limit] of longint;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;

Var
    data       : Tdata;
    blocked ,
    shortest   : Tshortest;
    visited    : Tvisited;
    start , stop
               : Tpoint;
    cases ,
    N , M      : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    readln(N , M);
    for i := 1 to N do
      begin
          for j := 1 to M do
            begin
                read(data[i , j]);
                if data[i , j] = 'E' then
                  begin start.x := i; start.y := j; data[i , j] := '.'; end;
                if data[i , j] = 'X' then
                  begin stop.x := i; stop.y := j; data[i , j] := '.'; end;
            end;
          readln;
      end;
end;

function bfs(start : Tpoint) : longint;
type
    Tqueue     = array[1..Limit * Limit] of Tpoint;
    Tvisited   = array[1..Limit , 1..Limit] of longint;
var
    queue      : Tqueue;
    visited    : Tvisited;
    p , np     : Tpoint;
    open , closed ,
    i          : longint;
begin
    open := 1; closed := 1;
    queue[1] := start;
    fillchar(visited , sizeof(visited) , $FF);
    visited[start.x , start.y] := 0;
    while open <= closed do
      begin
          p := queue[open];
          if (p.x = stop.x) and (p.y = stop.y) then exit(visited[p.x , p.y]);
          for i := 1 to 4 do
            begin
                np.x := p.x + dirx[i]; np.y := p.y + diry[i];
                if (np.x <= N) and (np.y <= M) and (np.x >= 1) and (np.y >= 1) then
                  if (data[np.x , np.y] = '.') and (visited[np.x , np.y] = -1) then
                    begin
                        visited[np.x , np.y] := visited[p.x , p.y] + 1;
                        inc(closed); queue[closed] := np;
                    end;
            end;
          inc(open);
      end;
    exit(maximum);
end;

procedure work;
var
    minx , miny ,
    tmp , i , j: longint;
    np , p     : Tpoint;
begin
    for p.x := 1 to N do
      for p.y := 1 to M do
        if data[p.x , p.y] = '.' then
          begin
              blocked[p.x , p.y] := 1;
              if abs(p.x - stop.x) + abs(p.y - stop.y) <= 1 then continue;
              for i := 1 to 4 do
                begin
                    np.x := p.x + dirx[i]; np.y := p.y + diry[i];
                    if (np.x <= N) and (np.y <= M) and (np.x >= 1) and (np.y >= 1) then
                      if data[np.x , np.y] = '.' then
                        begin
                            data[np.x , np.y] := '#';
                            tmp := bfs(p);
                            if tmp > blocked[p.x , p.y] then blocked[p.x , p.y] := tmp;
                            data[np.x , np.y] := '.';
                        end;
                end;
          end;

    fillchar(visited , sizeof(visited) , 0);
    fillchar(shortest , sizeof(shortest) , 1);
    shortest[stop.x , stop.y] := 0;

    while true do
      begin
          minx := 0;
          for i := 1 to N do
            for j := 1 to M do
              if not visited[i , j] then
                if (minx = 0) or (shortest[minx , miny] > shortest[i , j]) then
                  begin minx := i; miny := j; end;

          if (minx = start.x) and (miny = start.y) then exit;
          visited[minx , miny] := true;

          for i := 1 to 4 do
            begin
                np.x := minx + dirx[i]; np.y := miny + diry[i];
                if (np.x <= N) and (np.y <= M) and (np.x >= 1) and (np.y >= 1) then
                  if data[np.x , np.y] = '.' then
                    begin
                        if blocked[np.x , np.y] > shortest[minx , miny] + 1
                          then tmp := blocked[np.x , np.y]
                          else tmp := shortest[minx , miny] + 1;
                        if tmp < shortest[np.x , np.y] then
                          shortest[np.x , np.y] := tmp;
                    end;
            end;
      end;
end;

procedure out;
begin
    writeln(shortest[start.x , start.y]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.