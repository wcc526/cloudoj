#include <stdio.h>

int main(void)
{
  int a, b, i, j, n;
  char c, u[14], v[14];

  u[0] = v[0] = '0';
  u[13] = v[13] = 0;
  scanf("%d", &n);

  while (n--) {
    scanf("%x %c %x", &a, &c, &b);
    for (i = a, j = 13; j; i >>= 1)
      u[--j] = (i & 1) + '0';
    for (i = b, j = 13; j; i >>= 1)
      v[--j] = (i & 1) + '0';
    printf("%s %c %s = %d\n", u, c, v, c == '+' ? a + b : a - b);
  }

  return 0;
}
