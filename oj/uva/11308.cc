#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

class recipe {
public:
  string name;
  int cost;
  recipe(string n, int c) : name(n), cost(c) {
  }
  bool operator<(const recipe &rhs) const {
    return cost < rhs.cost || cost == rhs.cost && name < rhs.name;
  }
};

int main()
{
  int b, c, k, m, n, t;
  string s, v;

  cin >> t;
  while (t--) {
    cin.ignore();
    map<string, int> ingred;
    getline(cin, s);
    for (string::iterator i = s.begin(); i != s.end(); ++i)
      cout << (char)toupper(*i);
    cout << endl;
    cin >> m >> n >> b;
    while (m--) {
      cin >> s >> c;
      ingred.insert(make_pair(s, c));
    }
    vector<recipe> r;
    while (n--) {
      cin.ignore();
      getline(cin, v);
      cin >> k;
      c = 0;
      while (k--) {
        cin >> s >> m;
        c += ingred[s] * m;
      }
      if (c < b)
        r.push_back(recipe(v, c));
    }
    sort(r.begin(), r.end());
    for (vector<recipe>::iterator i = r.begin(); i != r.end(); ++i)
      cout << i->name << endl;
    if (r.size() == 0)
      cout << "Too expensive!" << endl;
    cout << endl;
  }

  return 0;
}
