Const
    InFile     = 'p10467.in';
    OutFile    = 'p10467.out';
    Limit      = 200;

Type
    Tdata      = object
                     N , M    : longint;
                     data     : array[1..Limit , 1..Limit] of char;
                     procedure init;
                     procedure add(x , y : longint; ch : char);
                     procedure paste(newdata : Tdata; x , y : longint);
                     procedure print;
                 end;
    Tstr       = string[Limit];
    Tnext      = array[#0..#255] of char;

Var
    s          : Tstr;
    next       : Tnext;
    answer     : Tdata;
    rooty      : longint;

procedure Tdata.init;
begin
    fillchar(data , sizeof(data) , 32);
    N := 0; M := 0;
end;

procedure Tdata.add(x , y : longint; ch : char);
begin
    data[x , y] := ch;
    if x > N then N := x; if y > M then M := y;
end;

procedure Tdata.paste(newdata : Tdata; x , y : longint);
var
    i , j      : longint;
begin
    for i := 1 to newdata.N do
      for j := 1 to newdata.M do
        data[i + x - 1 , j + y - 1] := newdata.data[i , j];
    if N < newdata.N + x - 1 then N := newdata.N + x - 1;
    if M < newdata.M + y - 1 then M := newdata.M + y - 1;
end;

procedure Tdata.print;
var
    i , j , k  : longint;
begin
    for i := 1 to N do
      begin
          k := M;
          while data[i , k] = ' ' do dec(k);
          for j := 1 to k do
            write(data[i , j]);
          writeln;
      end;
end;

procedure init;
begin
    readln(s);
    next['E'] := 'T'; next['T'] := 'F'; next['F'] := 'E';
end;

function Get_Next(p : longint) : longint;
var
    left       : longint;
begin
    left := 0;
    repeat
      if s[p] = '(' then inc(left);
      if s[p] = ')' then dec(left);
      inc(p);
    until left = 0;
    exit(p - 1);
end;

procedure dfs(start , stop : longint; root : char; var data : Tdata; var rooty : longint);
var
    tmp        : Tdata;
    ty , i , min ,
    L , R      : longint;
begin
    data.init;
    if start > stop then exit;
    if (s[start] = '(') and (Get_Next(start) = stop) then
      begin
          if root <> 'F'
            then begin
                     dfs(start , stop , next[root] , tmp , ty);
                     data.paste(tmp , 3 , 1);
                     data.add(1 , ty , root);
                     data.add(2 , ty , '|');
                     rooty := ty;
                 end
            else begin
                     dfs(start + 1 , stop - 1 , next[root] , tmp , ty);
                     data.paste(tmp , 3 , 4);
                     data.add(1 , ty + 3 , root);
                     data.add(2 , ty + 3 , '|');
                     data.add(3 , 1 , '('); data.add(3 , data.M + 3 , ')');
                     for i := 1 to data.M do
                       if i <> ty + 3 then
                         data.add(2 , i , '=');
                     rooty := ty + 3;
                 end;
          exit;
      end;
    if start = stop then
      begin
          if root <> 'F'
            then begin
                     dfs(start , stop , next[root] , tmp , ty);
                     data.paste(tmp , 3 , 1);
                     data.add(1 , 1 , root);
                     data.add(2 , 1 , '|');
                     rooty := 1;
                 end
            else begin
                     data.add(1 , 1 , root); data.add(2 , 1 , '|');
                     data.add(3 , 1 , s[start]);
                     rooty := 1;
                 end;
          exit;
      end;
    i := start; min := 0;
    while i <= stop do
      if s[i] = '('
        then i := Get_Next(i) + 1
        else begin
                 if s[i] in ['+' , '-' , '*' , '/'] then
                   if min = 0
                     then min := i
                     else if (s[i] in ['*' , '/']) and (s[min] in ['*' , '/'])
                            then min := i
                            else if s[i] in ['+' , '-'] then
                                   min := i;
                 inc(i);
             end;
    if s[min] in ['*' , '/'] then
      begin
          if root <> 'T'
            then begin
                     dfs(start , stop , next[root] , tmp , ty);
                     data.paste(tmp , 3 , 1);
                     data.add(1 , ty , root);
                     data.add(2 , ty , '|');
                     rooty := ty;
                 end
            else begin
                     dfs(start , min - 1 , 'T' , tmp , ty);
                     L := ty;
                     data.paste(tmp , 3 , 1);
                     rooty := tmp.M + 3;
                     data.add(1 , rooty , root);
                     data.add(2 , rooty , '|');
                     data.add(3 , rooty , s[min]);
                     dfs(min + 1 , stop , 'F' , tmp , ty);
                     R := ty + rooty + 2;
                     data.paste(tmp , 3 , rooty + 3);
                     for i := L to R do
                      if i <> rooty then
                        data.add(2 , i , '=');
                 end;
          exit;
      end;
    if s[min] in ['+' , '-'] then
      begin
          dfs(start , min - 1 , 'E' , tmp , ty);
          L := ty;
          data.paste(tmp , 3 , 1);
          rooty := tmp.M + 3;
          data.add(1 , rooty , root);
          data.add(2 , rooty , '|');
          data.add(3 , rooty , s[min]);
          dfs(min + 1 , stop , 'T' , tmp , ty);
          R := ty + rooty + 2;
          data.paste(tmp , 3 , rooty + 3);
          for i := L to R do
            if i <> rooty then
              data.add(2 , i , '=');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            dfs(1 , length(s) , 'E' , answer , rooty);
            answer.print;
            if not eof then writeln else break;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
