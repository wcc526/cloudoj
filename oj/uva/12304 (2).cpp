#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#define ALL(x) x.begin(),x.end()
#define CLR(x) memset((x),0,sizeof(x))
#define Foreach(e,x) for(__typeof(x.begin()) e=x.begin();e!=x.end();++e)
#define MP make_pair
#define PB push_back
#define SZ(x) (int)((x).size())
using namespace std;
typedef double DB;
const int N = 111;
const DB eps = 1e-8;
const DB pi = acos(-1.0);
inline DB sqr(DB x) { return x*x; }
inline int sgn(DB x) { return fabs(x)<eps ? 0 : x>eps ? 1 : -1; }
struct point {
	DB x,y;
	point(DB _x=0,DB _y=0):x(_x),y(_y) {}
	void input(void) { scanf("%lf%lf",&x,&y); }
	void output(void) { printf("%f %f\n",x,y); }
	bool operator==(const point &p)const {
		return sgn(x-p.x)==0 && sgn(y-p.y)==0;
	}
	bool operator<(const point &p)const {
		//return sgn(y-p.y)<0 || (sgn(y-p.y)==0 && sgn(x-p.x)<0);
		return sgn(x-p.x)<0 || (sgn(x-p.x)==0 && sgn(y-p.y)<0);
	}
	point operator+(const point &p)const {
		return point(x+p.x,y+p.y);
	}
	point operator-(const point &p)const {
		return point(x-p.x,y-p.y);
	}
	point operator*(DB k) { return point(x*k,y*k); }
	point operator/(DB k) { return point(x/k,y/k); }
	DB operator*(const point &p)const {
		return x*p.x+y*p.y;
	}
	DB operator^(const point &p)const {
		return x*p.y-y*p.x;
	}
	DB norm(void) { return hypot(x,y); }
	DB norm2(void) { return sqr(x)+sqr(y); }
	DB dist(const point &p)const { return hypot(x-p.x,y-p.y); }
	DB dist2(const point &p)const { return sqr(x-p.x)+sqr(y-p.y); }
	DB rad(point a,point b) {
		point p=*this;
		return fabs(atan2(fabs((a-p)^(b-p)),(a-p)*(b-p)));
	}
	point trunc(DB r) {
		DB l=norm();
		if(!sgn(l))	return *this;
		r/=l;
		return point(x*r,y*r);
	}
	point rotleft(void) { return point(-y,x); }
	point rotright(void) { return point(y,-x); }
	point rev(void) { return point(-x,-y); }
	point unit(void) {
		DB l=norm();
		return point(x/l,y/l);
	}
	// rotate theta degrees in counter-clockwise round the point p
	point rotate(point p,DB theta) {
		point q = *this-p;
		DB C=cos(theta),S=sin(theta);
		return point(p.x+q.x*C-q.y*S,p.y+q.x*S+q.y*C);
	}
};
struct line {
	point a,b;
	line() {}
	line(point _a,point _b):a(_a),b(_b) {}
	void input(void) { a.input(); b.input(); }
	DB length(void) { return a.dist(b); }
	DB angle(void) { // 0<=angle<pi
		DB k=atan2(b.y-a.y,b.x-a.x);
		if(sgn(k)<0) k+=pi;
		if(sgn(k-pi)==0) k-=pi;
		return k;
	}
	DB dist_point_to_line(point p) { return fabs((p-a)^(b-a))/length(); }
	DB dist_point_to_seg(point p) {
		if(sgn((p-b)*(a-b))<0||sgn((p-a)*(b-a))<0)
			return min(p.dist(a),p.dist(b));
		return dist_point_to_line(p);
	}
	bool same_side(point p1,point p2) {
		return sgn(((a-b)^(p1-b))*((a-b)^(p2-b)))==1;
	}
	bool parallel(line v) {
		return sgn((b-a)^(v.b-v.a))==0;
	}
	point cross_point(line v) {
		DB a1=(v.b-v.a)^(a-v.a);
		DB a2=(v.b-v.a)^(b-v.a);
		return point((a.x*a2-b.x*a1)/(a2-a1),(a.y*a2-b.y*a1)/(a2-a1));
	}
	point line_prog(point p) {
		return a+(b-a)*((b-a)*(p-a)/b.dist2(a));
	}
	point symmetry_point(point p) {
		point q=line_prog(p);
		return point(2*q.x-p.x,2*q.y-p.y);
	}
	line perpendicular_bisector(void) {
		point aa=point((a.x+b.x)/2,(a.y+b.y)/2);
		point bb=point(aa.x-(a.y-b.y),aa.y+(a.x-b.x));
		return line(aa,bb);
	}
};
struct circle {
	point p;
	DB r;
	circle() {}
	circle(point _p,DB _r):p(_p),r(_r) {}
	circle(DB x,DB y,DB _r):p(point(x,y)),r(_r) {}
	// circumscribed circle of triangle
	circle(point a,point b,point c) {
		p=line((a+b)/2,(a+b)/2+(b-a).rotleft()).cross_point(line((c+b)/2,(c+b)/2+(b-c).rotleft()));
		r=p.dist(a);
	}
	// inscribed circle of triangle
	circle(point a,point b,point c,bool t) {
		line u,v;
		DB m=atan2(b.y-a.y,b.x-a.x),n=atan2(c.y-a.y,c.x-a.x);
		u.a=a;
		u.b=u.a+point(cos((n+m)/2),sin((n+m)/2));
		v.a=b;
		m=atan2(a.y-b.y,a.x-b.x),n=atan2(c.y-b.y,c.x-b.x);
		v.b=v.a+point(cos((n+m)/2),sin((n+m)/2));
		p=u.cross_point(v);
		r=line(a,b).dist_point_to_seg(p);
	}
	void input(void) { p.input(); scanf("%lf",&r); }
	void output(void) { printf("%f %f %f\n",p.x,p.y,r); }
	bool operator==(const circle &c)const { return p==c.p&&sgn(r-c.r)==0; }
	bool operator<(const circle &c)const {
		return p<c.p||(p==c.p&&sgn(r-c.r)<0);
	}
	DB area(void) { return pi*sqr(r); }
	DB circumference(void) { return 2*pi*r; }
	// 2: in the circle, 1: on the circle, 0: out of the circle
	int relation(point q) {
		DB dst=q.dist(p);
		if(sgn(dst-r)<0)	return 2;
		if(sgn(dst-r)==0)	return 1;
		return 0;
	}
	int relation_seg(line l) {
		DB dst=l.dist_point_to_seg(p);
		if(sgn(dst-r)<0)	return 2;
		if(sgn(dst-r)==0)	return 1;
		return 0;
	}
	int relation_line(line l) {
		DB dst=l.dist_point_to_line(p);
		if(sgn(dst-r)<0)	return 2;
		if(sgn(dst-r)==0)	return 1;
		return 0;
	}
	// 5: external circles
	// 4: externally tangent
	// 3: secant circles
	// 2: internally tangent
	// 1: internal circles
	int relation_circle(circle c) {
		DB d=p.dist(c.p);
		if(sgn(d-r-c.r)>0)	return 5;
		if(sgn(d-r-c.r)==0)	return 4;
		DB l=fabs(r-c.r);
		if(sgn(d-r-c.r)<0&&sgn(d-l)>0)	return 3;
		if(sgn(d-l)==0)	return 2;
		return 1;
	}
	// through point a and point b, with radius r
	int get_circle(point a,point b,DB r,circle &c1,circle &c2) {
		circle x(a,r),y(b,r);
		int t=x.point_cross_circle(y,c1.p,c2.p);
		if(!t)	return 0;
		c1.r=c2.r=r;
		return t;
	}
	// tangent to line l, through point q, with radius R
	int get_circle(line l,point q,DB R,circle &c1,circle &c2) {
		DB dis=l.dist_point_to_line(q);
		if(sgn(dis-R*2)>0)	return 0;
		if(sgn(dis)==0) {
			c1.p=q+(l.b-l.a).rotleft().trunc(R);
			c2.p=q+(l.b-l.a).rotright().trunc(R);
			c1.r=c2.r=R;
			return 2;
		}
		line u1(l.a+(l.b-l.a).rotleft().trunc(R),l.b+(l.b-l.a).rotleft().trunc(R));
		line u2(l.a+(l.b-l.a).rotright().trunc(R),l.b+(l.b-l.a).rotright().trunc(R));
		circle cc(q,R);
		point p1,p2;
		if(!cc.point_cross_line(u1,p1,p2)) cc.point_cross_line(u2,p1,p2);
		c1=circle(p1,R);
		c2=circle(p2,R);
		if(p1==p2)	return 1;
		return 2;
	}
	// tangent to line u and v, with radius R
	int get_circle(line u,line v,DB R,circle &c1,circle &c2,circle &c3,circle &c4) {
		if(u.parallel(v))	return 0;
		line u1(u.a+(u.b-u.a).rotleft().trunc(R),u.b+(u.b-u.a).rotleft().trunc(R));
		line u2(u.a+(u.b-u.a).rotright().trunc(R),u.b+(u.b-u.a).rotright().trunc(R));
		line v1(v.a+(v.b-v.a).rotleft().trunc(R),v.b+(v.b-v.a).rotleft().trunc(R));
		line v2(v.a+(v.b-v.a).rotright().trunc(R),v.b+(v.b-v.a).rotright().trunc(R));
		c1.r=c2.r=c3.r=c4.r=R;
		c1.p=u1.cross_point(v1);
		c2.p=u1.cross_point(v2);
		c3.p=u2.cross_point(v1);
		c4.p=u2.cross_point(v2);
		return 4;
	}
	// tangent to two disjoint circle u and v, with radius R
	int get_circle(circle u,circle v,DB R,circle &c1,circle &c2) {
		circle x(u.p,R+u.r),y(v.p,R+v.r);
		int t=x.point_cross_circle(y,c1.p,c2.p);
		if(!t)	return 0;
		c1.r=c2.r=R;
		return t;
	}
	// if need to know cross_point with segment, judge relation_seg
	int point_cross_line(line l,point &p1,point &p2) {
		if(!(*this).relation_line(l))	return 0;
		point a=l.line_prog(p);
		DB d=l.dist_point_to_line(p);
		d=sqrt(sqr(r)-sqr(d));
		if(!sgn(d)) { p1=p2=a; return 1; }
		p1=a-(l.b-l.a).trunc(d);
		p2=a+(l.b-l.a).trunc(d);
		return 2;
	}
	// return the number of cross_point
	int point_cross_circle(circle c,point &p1,point &p2) {
		int rel=relation_circle(c);
		if(rel==1||rel==5)	return 0;
		DB d=p.dist(c.p);
		DB l=(d+(sqr(r)-sqr(c.r))/d)/2;
		DB h=sqrt(sqr(r)-sqr(l));
		p1=p+(c.p-p).trunc(l)+(c.p-p).rotleft().trunc(h);
		p2=p+(c.p-p).trunc(l)+(c.p-p).rotright().trunc(h);
		if(rel==2||rel==4)	return 1;
		return 2;
	}
	// tangent line of the circle, through point q
	int tangent_line(point q,line &u,line &v) {
		int rel=relation(q);
		if(rel==2)	return 0;
		if(rel==1) { u=v=line(q,q+(q-p).rotleft()); return 1; }
		DB d=p.dist(q);
		DB l=sqr(r)/d;
		DB h=sqrt(sqr(r)-sqr(l));
		u=line(q,p+((q-p).trunc(l)+(q-p).rotleft().trunc(h)));
		v=line(q,p+((q-p).trunc(l)+(q-p).rotright().trunc(h)));
		return 2;
	}
	DB area_circle(circle c) {
		int rel=relation_circle(c);
		if(rel>=4)	return 0.0;
		if(rel<=2)	return min(area(),c.area());
		DB d=p.dist(c.p);
		DB hf=(r+c.r+d)/2.0;
		DB ss=2*sqrt(hf*(hf-r)*(hf-c.r)*(hf-d));
		DB a1=acos((sqr(r)+sqr(d)-sqr(c.r))/2.0/r/d);
		DB a2=acos((sqr(c.r)+sqr(d)-sqr(r))/2.0/c.r/d);
		a1*=sqr(r);
		a2*=sqr(c.r);
		return a1+a2-ss;
	}
	DB area_triangle(point a,point b) {
		if(sgn((p-a)^(p-b))==0)	return 0.0;
		point q[5];
		int len=0;
		q[len++]=a;
		line l(a,b);
		point p1,p2;
		if(point_cross_line(l,q[1],q[2])==2) {
			if(sgn((a-q[1])*(b-q[1]))<0)	q[len++]=q[1];
			if(sgn((a-q[2])*(b-q[2]))<0)	q[len++]=q[2];
		}
		q[len++]=b;
		if(len==4&&sgn((q[0]-q[1])*(q[2]-q[1]))>0)	swap(q[1],q[2]);
		DB res=0;
		for(int i=0;i<len-1;i++) {
			if(relation(q[i])==0||relation(q[i+1])==0) {
				DB arg=p.rad(q[i],q[i+1]);
				res+=sqr(r)*arg/2.0;
			}
			else	res+=fabs((q[i]-p)^(q[i+1]-p)/2.0);
		}
		return res;
	}
};
int main(void) {
	string opt;
	point A,B,C,p;
	line l[2];
	circle cc,c[4];
	int cnt,i;
	while(cin>>opt) {
		if(opt=="CircumscribedCircle") {
			A.input(); B.input(); C.input();
			cc=circle(A,B,C);
			printf("(%.6f,%.6f,%.6f)\n",cc.p.x,cc.p.y,cc.r);
		}
		else if(opt=="InscribedCircle") {
			A.input(); B.input(); C.input();
			cc=circle(A,B,C,0);
			printf("(%.6f,%.6f,%.6f)\n",cc.p.x,cc.p.y,cc.r);
		}
		else if(opt=="TangentLineThroughPoint") {
			cc.input();
			p.input();
			cnt=cc.tangent_line(p,l[0],l[1]);
			if(cnt==2) {
				DB ang1=l[0].angle()*180.0/pi;
				DB ang2=l[1].angle()*180.0/pi;
				if(sgn(ang1-ang2)>0) swap(ang1,ang2);
				printf("[%.6f,%.6f]\n",ang1,ang2);
			}
			else if(cnt==1) printf("[%.6f]\n",l[0].angle());
			else puts("[]");
		}
		else if(opt=="CircleThroughAPointAndTangentToALineWithRadius") {
			cc.p.input();
			l[0].input();
			scanf("%lf",&cc.r);
			cnt=cc.get_circle(l[0],cc.p,cc.r,c[0],c[1]);
			if(cnt==2) {
				if(c[1].p<c[0].p) swap(c[0],c[1]);
				printf("[(%.6f,%.6f),(%.6f,%.6f)]\n",c[0].p.x,c[0].p.y,c[1].p.x,c[1].p.y);
			}
			else if(cnt==1) printf("[(%.6f,%.6f)]\n",c[0].p.x,c[0].p.y);
			else puts("[]");
		}
		else if(opt=="CircleTangentToTwoLinesWithRadius") {
			for(i=0;i<2;i++) l[i].input();
			scanf("%lf",&cc.r);
			cnt=cc.get_circle(l[0],l[1],cc.r,c[0],c[1],c[2],c[3]);
			sort(c,c+4);
			printf("[");
			for(i=0;i<4;i++) {
				if(i) putchar(',');
				printf("(%.6f,%.6f)",c[i].p.x,c[i].p.y);
			}
			puts("]");
		}
		else if(opt=="CircleTangentToTwoDisjointCirclesWithRadius") {
			for(i=0;i<2;i++) c[i].input();
			scanf("%lf",&cc.r);
			cnt=cc.get_circle(c[0],c[1],cc.r,c[2],c[3]);
			if(cnt==2) {
				if(c[3].p<c[2].p) swap(c[2],c[3]);
				printf("[(%.6f,%.6f),(%.6f,%.6f)]\n",c[2].p.x,c[2].p.y,c[3].p.x,c[3].p.y);
			}
			else if(cnt==1) printf("[(%.6f,%.6f)]\n",c[2].p.x,c[3].p.y);
			else puts("[]");
		}
	}
	return 0;
}