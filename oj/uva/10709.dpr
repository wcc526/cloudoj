{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10709.in';
    OutFile    = 'p10709.out';
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    TLine      = record
                     A , B , C               : extended;
                 end;
    Tseg       = record
                     p1 , p2                 : Tpoint;
                     L                       : TLine;
                     segment                 : boolean;
                 end;

Var
    seg1 , seg2
               : Tseg;
    answer     : extended;

function init : boolean;
var
    s1 , s2    : string;
begin
    with seg1 do readln(p1.x , p1.y , p2.x , p2.y , s1);
    with seg2 do readln(p1.x , p1.y , p2.x , p2.y , s2);
    if pos('END' , s1) <> 0
     then init := false
     else begin
              init := true;
              seg1.segment := pos('LS' , s1) <> 0;
              seg2.segment := pos('LS' , s2) <> 0;
          end;
end;

procedure improve(num : extended);
begin
    if num < answer then answer := num;
end;

procedure GetLine(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p2.y * p1.x - p2.x * p1.y;
end;

function GetCrossing(L1 , L2 : TLine; var p : Tpoint) : boolean;
var
    tmp        : extended;
begin
    tmp := L1.A * L2.B - L2.A * L1.B;
    if abs(tmp) <= minimum
      then GetCrossing := false
      else begin
               GetCrossing := true;
               p.x := (L2.C * L1.B - L1.C * L2.B) / tmp;
               p.y := (L1.C * L2.A - L2.C * L1.A) / tmp;
           end;
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function onseg(seg : Tseg; p : Tpoint) : boolean;
begin
    if not seg.segment
      then onseg := true
      else onseg := abs(dist(seg.p1 , seg.p2) - dist(seg.p1 , p) - dist(seg.p2 , p)) <= minimum;
end;

procedure work_sub(p : Tpoint; seg : Tseg);
var
    L1         : TLine;
    crossp     : Tpoint;
begin
    improve(dist(p , seg.p1));
    improve(dist(p , seg.p2));
    L1.A := seg.L.B; L1.B := -seg.L.A;
    L1.C := -L1.A * p.x - L1.B * p.y;
    GetCrossing(L1 , seg.L , crossp);
    if onseg(seg , crossp) then
      improve(dist(crossp , p));
end;

procedure work;
var
    p          : Tpoint;
begin
    answer := 1e10;
    GetLine(seg1.p1 , seg1.p2 , seg1.L);
    GetLine(seg2.p1 , seg2.p2 , seg2.L);
    if GetCrossing(seg1.L , seg2.L , p) then
      if onseg(seg1 , p) and onseg(seg2 , p) then
        improve(0);
    work_sub(seg2.p1 , seg1);
    work_sub(seg2.p2 , seg1);
    work_sub(seg1.p1 , seg2);
    work_sub(seg1.p2 , seg2); 
end;

procedure out;
begin
    writeln(answer : 0 : 5);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
