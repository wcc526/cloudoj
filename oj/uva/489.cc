#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#define NUM_ALPHABET 26

int main()
{
    string line;
    while (getline(cin, line)) {
        if (line == "-1") break;
        cout << "Round " << line << endl;
        string word;
        getline(cin, word);
        string guess;
        getline(cin, guess);
        int freq[NUM_ALPHABET];
        for (int i=0; i<NUM_ALPHABET; ++i) freq[i] = 0;
        for (int i=0; i<word.size(); ++i) freq[word[i]-'a']++;
        int match = 0, incorrectGuess = 0;
        bool guessed[NUM_ALPHABET];
        for (int i=0; i<NUM_ALPHABET; ++i) guessed[i] = false;
        for (int i=0; i<guess.size(); ++i) {
            if (guessed[guess[i]-'a']) continue;
            if (freq[guess[i]-'a'] == 0) incorrectGuess++;
            else match += freq[guess[i]-'a'];
            guessed[guess[i]-'a'] = true;
            if (match == word.size() && incorrectGuess < 7) {
                cout << "You win." << endl;
                break;
            } else if (incorrectGuess >= 7 && match < word.size()) {
                cout << "You lose." << endl;
                break;
            }
        }
        if (match < word.size() && incorrectGuess < 7)
            cout << "You chickened out." << endl;
    }

    return 0;
}
