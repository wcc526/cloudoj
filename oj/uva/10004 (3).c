#include <stdio.h>
#include <string.h>

#define STACK_USED (s < t)
#define PUSH(x) *t++ = (x)
#define POP(x)  x = *--t
#define CLEAR t = c

int main()
{
  int e, i, n;
  short a, b;
  unsigned char c[200], g[200][200], s[200], *t;

  while (scanf("%d", &n) == 1 && n) {
    memset(g, 0, sizeof(g));
    memset(s, 0, sizeof(s));
    scanf("%d", &e);
    while (e--) {
      scanf("%hd %hd", &a, &b);
      g[a][b] = g[b][a] = 1;
    }
    s[0] = 1;
    CLEAR;
    PUSH(0);
    while (STACK_USED) {
      POP(e);
      for (i = n; i--;) {
        if (g[e][i]) {
          if (s[i]) {
            if (s[i] & s[e]) {
              puts("NOT BICOLORABLE.");
              goto d;
            }
          } else {
            s[i] = s[e] ^ 3;
            PUSH(i);
          }
        }
      }
    }
    puts("BICOLORABLE.");
  d:;
  }

  return 0;
}
