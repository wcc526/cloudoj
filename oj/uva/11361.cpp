#include<cstdio>
#include<cstring>
int dp[10][81][81][81],dig[10];
int dfs(int pos,int sum,int cnt,int k,bool fg)//fg=1 lim=9
{
    if(pos<0) return sum==0&&cnt==0;
    if(fg&&~dp[pos][sum][cnt][k]) return dp[pos][sum][cnt][k];
    int lim=fg?9:dig[pos],ans=0;
    for(int i=0;i<=lim;i++) ans+=dfs(pos-1,(sum*10+i)%k,(cnt+i)%k,k,fg||i<lim);
    return fg?dp[pos][sum][cnt][k]=ans:ans;
}
int cal(int x,int k)
{
    int i=0;
    for(;x;x/=10) dig[i++]=x%10;
    return dfs(i-1,0,0,k,0);
}
int main()
{
    memset(dp,-1,sizeof(dp));
    int t,a,b,k;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d%d%d",&a,&b,&k);
        if(k>=81) puts("0");
        else printf("%d\n",cal(b,k)-cal(a-1,k));
    }
    return 0;
}
