{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10586.in';
    OutFile    = 'p10586.out';
    Limit      = 10000;

Type
    Tdata      = array[0..Limit] of longint;

Var
    data       : Tdata;
    N , K      : longint;

function init : boolean;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    read(N , K);
    init := true;
    if N = -1
      then init := false
      else for i := 0 to N do read(data[i]);
end;

procedure work;
var
    i          : longint;
begin
    for i := N downto K do
      begin
          dec(data[i - K] , data[i]);
          data[i] := 0;
      end;
end;

procedure out;
var
    i , top    : longint;
begin
    write(data[0]);
    top := K - 1; while (top > 0) and (data[top] = 0) do dec(top);
    for i := 1 to top do write(' ' , data[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
