#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
  int a, c, i, j, m, n, r, s;
  char f[100][61];

  while (scanf("%d", &n) == 1) {
    for (m = 0, i = n; i--;) {
      scanf("%s%n", f[i], &s);
      if (m < s)
        m = s;
    }
    c = 62 / (m-- + 1);
    r = (n - 1) / c + 1;
    s = (n - 1) / r + 1;
    a = n + r - r * s;
    qsort(f, n, sizeof(*f), (int (*)(const void *, const void *))strcmp);
    puts("------------------------------------------------------------");
    for (i = 0; i < r; ++i) {
      for (j = 0; j < s; ++j)
        printf("%-*s%s%s",m, f[j * r + i],
               (c - j) ^ 1 ? "  " : "",
               (s - j) ^ 1 ? "" : "\n");
      if (--a == 0)
        --s;
    }
  }

  return 0;
}
