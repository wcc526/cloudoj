Const
    InFile     = 'p10201.in';
    Limit      = 200;

Type
    Tdata      = array[1..Limit] of
                   record
                       p , cost              : longint;
                   end;

Var
    data       : Tdata;
    noanswer   : boolean;
    Len , answer ,
    N , cases  : longint;

procedure init;
begin
    N := 0;
    readln(Len);
    while not eof and not eoln do
      begin
          inc(N);
          readln(data[N].p , data[N].cost);
          if (N > 1) and (data[N].p = data[N - 1].p) then dec(N);
      end;
    dec(Cases);
end;

function dfs(start , stop , filled : longint; var overflow , answer : longint) : boolean;
var
    i , min ,
    over1 , ans1 ,
    over2 , ans2 ,
    add        : longint;
begin
    min := 0;
    for i := 1 to N do
      if (data[i].p > start) and (data[i].p < stop) then
        if (min = 0) or (data[min].cost > data[i].cost) then
          min := i;
    if min = 0 then
      begin
          overflow := filled - (stop - start); answer := 0;
          if overflow < 0 then exit(false) else exit(true);
      end;
    if not dfs(start , data[min].p , filled , over1 , ans1) then exit(false);
    if stop - data[min].p > 200
      then add := 200 - over1
      else add := stop - data[min].p - over1;
    if add < 0 then add := 0;
    if not dfs(data[min].p , stop , over1 + add , over2 , ans2) then exit(false);
    overflow := over2;
    answer := ans1 + ans2 + add * data[min].cost;
    exit(true);
end;

procedure work;
var
    overflow   : longint;
begin
    noanswer := not dfs(-1 , Len + 100 , 101 , overflow , answer);
end;

procedure out;
begin
    if noanswer
      then writeln('Impossible')
      else writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.