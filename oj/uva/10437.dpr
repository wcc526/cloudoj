{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10437.in';
    OutFile    = 'p10437.out';
    Limit      = 500;
    LimitLen   = 50;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure readin(start , stop : longint);
                     procedure wipe_zero;
                     procedure add(num1 , num2 : lint);
                     procedure minus(num1 , num2 : lint);
                     procedure times(num1 , num2 : lint);
                     procedure divide(num1 , num2 : lint; var remainder : lint);
                     procedure shift(num : longint);
                     function bigger(num : lint) : boolean;
                     procedure print;
                 end;
    Tfraction  = object
                     signal   : longint;
                     A , B    : lint;
                     function init(start , stop : longint) : boolean;
                     procedure add_sub(num1 , num2 : Tfraction);
                     procedure minus_sub(num1 , num2 : Tfraction);
                     procedure add(num1 , num2 : Tfraction);
                     procedure minus(num1 , num2 : Tfraction);
                     procedure times(num1 , num2 : Tfraction);
                     function divide(num1 , num2 : Tfraction) : boolean;
                     procedure reduce;
                     function bigger(num : Tfraction) : boolean;
                     procedure print;
                 end;
    Tdata      = array[1..Limit] of char;

Var
    s          : Tdata;
    answer     : Tfraction;
    valid      : boolean;
    N          : longint;

procedure Get_GCD(const A , B : lint; var GCDNum : lint);
var
    tmp ,
    remainder  : lint;
begin
    if (A.len = 0) or (A.len = 1) and (A.data[1] = 0)
      then GCDNum := B
      else begin
               tmp.divide(B , A , remainder);
               Get_GCD(remainder , A , GCDNum);
           end;
end;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 0;
end;

procedure lint.readin(start , stop : longint);
var
    i          : longint;
begin
    lint.init;
    for i := stop downto start do
      begin
          inc(len); data[len] := ord(s[i]) - ord('0');
      end;
    wipe_zero;
end;

procedure lint.wipe_zero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          data[i] := tmp mod 10;
          jw := tmp div 10;
          inc(i);
      end;
    len := i - 1;
    wipe_zero;
end;

procedure lint.minus(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0;
          if tmp < 0 then begin inc(tmp , 10); jw := 1; end;
          data[i] := tmp;
          inc(i);
      end;
    len := num1.len;
    wipe_zero;
end;

procedure lint.times(num1 , num2 : lint);
var
    i , j , jw : longint;
begin
    init;
    for i := 1 to num1.len do
      for j := 1 to num2.len do
        inc(data[i + j - 1] , num1.data[i] * num2.data[j]);
    j := num1.len + num2.len - 1; i := 1; jw := 0;
    while (i <= j) or (jw <> 0) do
      begin
          inc(data[i] , jw);
          jw := data[i] div 10;
          data[i] := data[i] mod 10;
          inc(i);
      end;
    len := i - 1;
    wipe_zero;
end;

procedure lint.divide(num1 , num2 : lint; var remainder : lint);
var
    i , j      : longint;
    tmp , sum  : lint;
begin
    init;
    len := num1.len - num2.len + 1;
    if len <= 0 then len := 1;
    for i := len downto 1 do
      begin
          tmp := num2;
          tmp.shift(i - 1);
          sum.init; j := 0;
          while not sum.bigger(num1) do
            begin
                sum.add(sum , tmp);
                inc(j);
            end;
          dec(j);
          sum.minus(sum , tmp);
          num1.minus(num1 , sum);
          data[i] := j;
      end;
    wipe_zero;
    remainder := num1;
end;

procedure lint.shift(num : longint);
var
    i          : longint;
begin
    for i := len downto 1 do data[i + num] := data[i];
    for i := 1 to num do data[i] := 0;
    inc(len , num); 
end;

function lint.bigger(num : lint) : boolean;
var
    i          : longint;
begin
    if (len = 1) and (data[1] = 0) or (len = 0)
      then bigger := false
      else if len <> num.len
             then bigger := len > num.len
             else begin
                      bigger := false;
                      for i := len downto 1 do
                        if data[i] <> num.data[i] then
                          begin
                              bigger := data[i] > num.data[i];
                              exit;
                          end;
                  end;
end;

procedure lint.print;
var
    i          : longint;
begin
    if len = 0 then write(0);
    for i := len downto 1 do write(data[i]);
end;

function Tfraction.init(start , stop : longint) : boolean;
var
    i , p      : longint;
begin
    p := -1;
    init := false;
    for i := start to stop do
      if not ((s[i] >= '0') and (s[i] <= '9'))
        then if s[i] <> '|'
               then exit
               else if p = -1
                      then p := i
                      else exit;
    if p = -1
      then begin
               A.readin(start , stop);
               B.init; B.len := 1; B.data[1] := 1;
           end
      else begin
               if (p = start) or (p = stop) then exit;
               A.readin(start , p - 1);
               B.readin(p + 1 , stop);
               if (B.len = 0) or (B.len = 1) and (B.data[1] = 0) then exit;
           end;
    signal := 1;
    reduce;
    init := true;
end;

procedure Tfraction.add_sub(num1 , num2 : Tfraction);
var
    tmp1 , tmp2: lint;
begin
    B.times(num1.B , num2.B);
    tmp1.times(num1.A , num2.B);
    tmp2.times(num1.B , num2.A);
    A.add(tmp1 , tmp2);
    reduce;
end;

procedure Tfraction.minus_sub(num1 , num2 : Tfraction);
var
    tmp1 , tmp2: lint;
begin
    B.times(num1.B , num2.B);
    tmp1.times(num1.A , num2.B);
    tmp2.times(num1.B , num2.A);
    A.minus(tmp1 , tmp2);
    reduce;
end;

procedure Tfraction.add(num1 , num2 : Tfraction);
begin
    if num1.signal <> num2.signal
      then if num1.bigger(num2)
             then begin
                      signal := num1.signal;
                      minus_sub(num1 , num2)
                  end
             else begin
                      signal := num2.signal;
                      minus_sub(num2 , num1);
                  end
      else begin
               signal := num1.signal;
               add_sub(num1 , num2);
           end;
end;

procedure Tfraction.minus(num1 , num2 : Tfraction);
begin
    if num1.signal = num2.signal
      then if num1.bigger(num2)
             then begin
                      signal := num1.signal;
                      minus_sub(num1 , num2)
                  end
             else begin
                      signal := -num2.signal;
                      minus_sub(num2 , num1);
                  end
      else begin
               signal := num1.signal;
               add_sub(num1 , num2);
           end;
end;

procedure Tfraction.times(num1 , num2 : Tfraction);
begin
    signal := num1.signal * num2.signal;
    A.times(num1.A , num2.A);
    B.times(num1.B , num2.B);
    reduce;
end;

function Tfraction.divide(num1 , num2 : Tfraction) : boolean;
begin
    signal := num1.signal * num2.signal;
    A.times(num1.A , num2.B);
    B.times(num1.B , num2.A);
    if (B.len = 0) or (B.len = 1) and (B.data[1] = 0)
      then divide := false
      else divide := true;
    reduce;
end;

procedure Tfraction.reduce;
var
    remainder ,
    GCDnum     : lint;
begin
    Get_Gcd(A , B , GCDnum);
    A.divide(A , GCDnum , remainder);
    B.divide(B , GCDnum , remainder);
end;

function Tfraction.bigger(num : Tfraction) : boolean;
var
    x , y      : lint;
begin
    x.times(A , num.B);
    y.times(B , num.A);
    bigger := x.bigger(y)
end;

procedure Tfraction.print;
begin
    if (A.len = 0) or (A.len = 1) and (A.data[1] = 0)
      then begin write(0); exit; end;
    if signal = -1 then write('-');
    A.print;
    if (B.len = 1) and (B.data[1] = 1)
      then
      else begin
               write('|');
               B.print;
           end;
end;

procedure init;
var
    ch         : char;
begin
    fillchar(s , sizeof(s) , 0);
    N := 0;
    while not eoln do
      begin
          read(ch);
          if ch in ['0'..'9' , '|' , '(' , ')' , '+' , '-' , '*' , '/'] then
            begin
                inc(N);
                s[N] := ch;
            end;
      end;
    readln;
end;

function Get_Next(p : longint) : longint;
var
    Left       : longint;
begin
    Left := 1; inc(p);
    while Left > 0 do
      begin
          if s[p] = '('
            then inc(Left)
            else if s[p] = ')'
                   then dec(Left);
          inc(p);
      end;
    dec(p);
    Get_Next := p;
end;

function DFS_Calc(start , stop : longint; var answer : Tfraction) : boolean;
var
    p , min    : longint;
    num1 , num2: Tfraction;
begin
    DFS_Calc := false;
    answer.signal := 1;
    answer.A.init; answer.A.len := 1;
    answer.B.init; answer.B.len := 1; answer.B.data[1] := 1;
    while (s[start] = '(') and (Get_Next(start) = stop) do
      begin inc(start); dec(stop); end;
    p := start;
    min := 0;
    while p <= stop do
      if s[p] = '('
        then p := Get_Next(p) + 1
        else begin
                 if s[p] in ['+' , '-' , '*' , '/']
                   then if min = 0
                          then min := p
                          else if s[p] in ['+' , '-']
                                 then min := p
                                 else if s[min] in ['*' , '/']
                                        then min := p;
                 inc(p);
             end;
    if min = 0
      then begin
               DFS_Calc := answer.init(start , stop);
               exit;
           end;
    if not DFS_Calc(start , min - 1 , num1) then exit;
    if not DFS_Calc(min + 1 , stop , num2) then exit;
    case s[min] of
      '+'      : answer.add(num1 , num2);
      '-'      : answer.minus(num1 , num2);
      '*'      : answer.times(num1 , num2);
      '/'      : if not answer.divide(num1 , num2)
                   then exit;
    end;
    DFS_Calc := true;
end;

procedure work;
begin
    valid := DFS_Calc(1 , N , answer);
end;

procedure out;
begin
    if valid
      then answer.print
      else write('INVALID');
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
