Const
    InFile     = 'p10225.in';
    LimitSegment
               = 10000;

Type
    Tkey       = record
                     x , p    : longint;
                 end;
    Tnums      = object
                     tot      : longint;
                     data     : array[1..LimitSegment] of Tkey;
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                     function binary_find(K : longint) : longint;
                 end;

Var
    nums       : Tnums;
    P , B , N  : longint;

procedure init;
begin
    readln(P , B , N);
end;

procedure multi(a , b : extended; var c : longint);
begin
    a := a * b;
    a := a - int(a / P) * P;
    c := trunc(a);
end;

procedure Tnums.qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].p > key.p) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].p < key.p) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure Tnums.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function Tnums.binary_find(K : longint) : longint;
var
    start , stop ,
    mid        : longint;
begin
    start := 1; stop := tot;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if data[mid].p = K
            then exit(data[mid].x)
            else if data[mid].p < K
                   then start := mid + 1
                   else stop := mid - 1;
      end;
    exit(-1);
end;

procedure euclid_extend(A , B , C : extended; var x , y : extended);
var
    _mod , t ,
    _div       : extended;
begin
    if A = 0
      then begin x := 0; y := int(C / B); end
      else begin
               _div := int(B / A); _mod := B - _div * A;
               euclid_extend(_mod , A , C , y , t);
               x := t - _div * y;
           end;
end;

function divide(A , B : extended) : longint;
var
    x , y      : extended;
begin
    euclid_extend(B , P , A , x , y);
    x := x - int(x / P) * P;
    if x < 0 then x := x + P;
    divide := trunc(x);
end;

procedure work;
var
    ans ,
    j , tmp , K ,
    num , i    : longint;
begin
    if P <= LimitSegment
      then begin
               num := 1;
               for i := 0 to P - 2 do
                 if num = N
                   then begin writeln(i); exit; end
                   else multi(num , B , num);
           end
      else begin
               num := 1; nums.tot := 0;
               for i := 0 to LimitSegment - 1 do
                 begin
                     inc(nums.tot);
                     nums.data[nums.tot].x := i;
                     nums.data[nums.tot].p := num;
                     multi(num , B , num);
                 end;
               nums.qk_sort(1 , nums.tot);
               i := 1; j := 2;
               while j <= nums.tot do
                 if nums.data[i].p <> nums.data[j].p
                   then begin inc(i); nums.data[i] := nums.data[j]; inc(j); end
                   else begin
                            if nums.data[i].x > nums.data[j].x then nums.data[i] := nums.data[j];
                            inc(j);
                        end;
               nums.tot := i; tmp := num; num := 1;
               for i := 0 to P div LimitSegment do
                 begin
                     K := divide(N , num);
                     ans := nums.binary_Find(K);
                     if ans <> -1
                       then begin
                                writeln(ans + i * LimitSegment);
                                exit;
                            end;
                     multi(num , tmp , num);
                 end;
           end;
    writeln('no solution');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
        end;
//    Close(INPUT);
End.
