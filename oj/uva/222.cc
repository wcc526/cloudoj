#include <cstdio>
#include <cstring>
using namespace std;

double gasStationDist[51], gasStationPrice[51];
double fuelCapacity, milesPerGallon, costFillingOrigCity;
double destDist, minCost, curFuel;
int numStations;

bool refillHere(double curFuel, int station) {
    if (curFuel <= fuelCapacity/2.00) return true; // fuel at less than half capacity
    // fuel is more than half capacity
    double distCanBeTravelled = curFuel * milesPerGallon;
    if (station < numStations-1) {
        if (distCanBeTravelled < gasStationDist[station+1]-gasStationDist[station]) { //can't get to next station
            return true;
        }
    } else if (distCanBeTravelled < destDist-gasStationDist[station]) { // if can't get to dest with given fuel
        return true;
    }
    return false;
}

void backtrack(int station, double cost) {
    double newCost = cost;
    if (refillHere(curFuel, station)) {
        newCost += (fuelCapacity-curFuel)*gasStationPrice[station] + 200.0;
        curFuel = fuelCapacity;
    }
    // calculate fuel comsumption till next station/dest
    if (station < numStations-1) {
        curFuel -= (gasStationDist[station+1]-gasStationDist[station])/milesPerGallon;
        if (station+1 < numStations) backtrack(station+1, newCost);
    } else { // last station
        if (minCost > newCost) minCost = newCost;
    }
}

int main()
{
    int TC = 1;
    while (true) {
        scanf("%lf", &destDist);
        if (destDist < 0) break;
        scanf("%lf %lf %lf %d", &fuelCapacity, &milesPerGallon, &costFillingOrigCity, &numStations);
        costFillingOrigCity *= 100;
        for (int i=0; i<numStations; ++i) {
            scanf("%lf %lf", &gasStationDist[i], &gasStationPrice[i]);
        }
        minCost = 2000000000000.00;
        curFuel = fuelCapacity-gasStationDist[0]/milesPerGallon;
        backtrack(0, costFillingOrigCity);
        printf("Data Set #%d\n", TC++);
        printf("minimum cost = $%.2lf\n", minCost/100);
    }

    return 0;
}

