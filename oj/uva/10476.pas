Const
    InFile     = 'p10476.in';
    OutFile    = 'p10476.out';
    LimitSave  = 1000000;
    LimitLen   = 15;

Type
    Ttrigram   = string[3];
    Tkey       = record
                     trigram  : Ttrigram;
                     times    : longint;
                 end;
    Tdata      = object
                     tot      : longint;
                     data     : array[1..LimitSave] of Tkey;
                     procedure init;
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                     procedure process;
                 end;
    Tstr       = string[LimitLen];

Var
    spam , nonspam ,
    message    : Tdata;
    Cases ,
    S , N , C  : longint;

procedure read_message(var data : Tdata);
var
    s          : Tstr;
    trigram    : Ttrigram;
    tot        : longint;
    ch         : char;
begin
    repeat
      s := ''; if eoln then begin readln; continue; end;
      read(ch); s := s + ch; if eoln then begin readln; continue; end;
      read(ch); s := s + ch; if eoln then begin readln; continue; end;
      tot := 2;
      while not eoln do
        begin
            read(ch);
            inc(tot);
            if length(s) = LimitLen then delete(s , 1 , 1);
            s := s + ch;
            trigram := copy(s , length(s) - 2 , 3);
            inc(data.tot); data.data[data.tot].trigram := trigram;
            data.data[data.tot].times := 1;
        end;
      readln;
      if s = 'ENDMESSAGE' then dec(data.tot , 8);
    until s = 'ENDMESSAGE';
end;

function init : boolean;
var
    i          : longint;
begin
    readln(S , N , C);
    if S = 0 then exit(false);
    inc(Cases);
    init := true;
    spam.init; nonspam.init;
    for i := 1 to S do read_message(spam);
    for i := 1 to N do read_message(nonspam);
    spam.qk_sort(1 , spam.tot); spam.process;
    nonspam.qk_sort(1 , nonspam.tot); nonspam.process;
end;

procedure Tdata.init;
begin
    tot := 0;
end;

procedure Tdata.qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tkey;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].trigram > key.trigram) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].trigram < key.trigram) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure Tdata.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure Tdata.process;
var
    i , j      : longint;
begin
    i := 1; j := 2;
    while j <= tot do
      begin
          if data[i].trigram = data[j].trigram
            then inc(data[i].times , data[j].times)
            else begin inc(i); data[i] := data[j]; end;
          inc(j);
      end;
    tot := i;
end;

function similarity(const data1 , data2 : Tdata) : extended;
var
    res ,
    num1 , num2: extended;
    i , j      : longint;
begin
    num1 := 0; num2 := 0;
    for i := 1 to data1.tot do num1 := num1 + sqr(data1.data[i].times * 1.0);
    for i := 1 to data2.tot do num2 := num2 + sqr(data2.data[i].times * 1.0);
    num1 := sqrt(num1); num2 := sqrt(num2);

    res := 0;
    i := 1; j := 1;
    while (i <= data1.tot) and (j <= data2.tot) do
      if data1.data[i].trigram = data2.data[j].trigram
        then begin
                 res := res + 1.0 * data1.data[i].times * data2.data[j].times;
                 inc(i); inc(j);
             end
        else if data1.data[i].trigram < data2.data[j].trigram
               then inc(i)
               else inc(j);
    res := res / num1 / num2;
    similarity := res;
end;

procedure workout;
var
    p1 , p2    : extended;
    i          : longint;
begin
    for i := 1 to C do
      begin
          message.init;
          read_message(message);
          message.qk_sort(1 , message.tot);
          message.process;
          p1 := similarity(message , spam);
          p2 := similarity(message , nonspam);
          writeln(p1 : 0 : 5 , ' ' , p2 : 0 : 5);
          if p1 > p2 + 1e-10 then writeln('spam') else writeln('non-spam');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      Cases := 0;
      while init do
        begin
            writeln('Set ' , Cases , ':');
            workout;
        end;
//    Close(INPUT);
End.
