#include <stdio.h>

double v[1 << 21];

double f(int n, int k, int l)
{
  double& r = v[((n << 7) + l << 7) + k];
  if (r > -1) return r;
  if (n == 1) return 1;
  if (l == 0) return k + 1 == n ? 1 : 0;
  int x = k - 1 < 0 ? n - 1 : k - 1;
  double gl = (double)l / (n + l - 1) * f(n, x, l - 1);
  if (k == 0)
    return r = gl;
  double gs = (double)(n - 1) / (n + l - 1) * f(n - 1, x, l);
  return r = gs + gl;
}

int main()
{
  int k, n, m, t;

  for (n = 1 << 21; n--; v[n] = -1.0);
  for (scanf("%d", &t); t--; printf("%.3lf\n", 100.0 * f(n, k, m)))
    scanf("%d %d %d", &n, &m, &k);

  return 0;
}
