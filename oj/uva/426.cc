#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

struct transaction
{
  int number;
  double amount;
  char date[9];

  bool operator<(const transaction &rhs) const {
    return number < rhs.number;
  }
};

int main()
{
  int c;
  char buffer[32];

  gets(buffer);
  sscanf(buffer, "%d", &c);
  gets(buffer);
  while (c--) {
    vector<transaction> transactions;
    while (gets(buffer) && buffer[0]) {
      int i;
      transaction t;
      t.amount = 0;
      sscanf(buffer, "%s %d%n", t.date, &t.number, &i);
      sscanf(buffer + i, "%lf", &t.amount);
      transactions.push_back(t);
    }
    sort(transactions.begin(), transactions.end());
    int n = transactions.size();
    int r = (n - 1) / 3 + 1;
    int s = (n - 1) / r + 1;
    int a = n + r - r * s;
    for (int i = 0; i < r; ++i) {
      for (int j = 0; j < s; ++j) {
        int k = j * r + i;
        transaction &t = transactions[k];
        printf("%4d%c %9.2lf %s%s", t.number,
               k > 0 && t.number - 1 != transactions[k - 1].number ? '*' : ' ',
               t.amount, t.date, (s - j) ^ 1 ? "   " : "\n");
      }
      if (--a == 0)
        --s;
    }
    if (c)
      putchar('\n');
  }

  return 0;
}
