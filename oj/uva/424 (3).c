#include <stdio.h>
#include <string.h>

int main()
{
  char a[104], b[104];
  int i, j, n;

  memset(a, 0, sizeof(a));
  while (scanf("%100[0-9]%n", b, &n) == 1 && (b[0] > '0' || b[1] > 0)) {
    for (i = 103, j = n; j--; --i)
      if ((a[i] += b[j] - '0') > 9)
        a[i] -= 10, ++a[i - 1];
    while (a[i] > 9)
      a[i] -= 10, ++a[--i];
    scanf("%*[^0-9]");
  }

  for (i = 0; i < 104 && a[i] < 1; ++i);
  if (i == 104)
    putchar('0');
  else
    while (i < 104)
      putchar(a[i++] + '0');
  putchar('\n');

  return 0;
}
