#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

using namespace std;

template<typename T>
struct delete_pointer : public unary_function<T *, void> {
  void operator()(T *&v) {
    delete v;
  }
};

template<typename K, typename V> struct delete_second :
  public unary_function<pair<const K, V *>, void> {
  inline void operator()(pair<const K, V *> &item) {
    delete item.second;
  }
};

class node {
public:
  char operation;
  node(char o) : operation(o) { }
  virtual ~node(void) { }
  virtual void print(void)=0;
};

class expression : public node {
private:
  node *left;
  node *right;

public:
  expression(node *l, node *r, char o) : node(o), left(l), right(r) {
  }

  void print(void) {
    if (operation == '*' && left->operation == '+') {
      cout << '(';
      left->print();
      cout << ')';
    } else {
      left->print();
    }
    cout << operation;
    if (operation == right->operation ||
        (operation == '*' && right->operation == '+')) {
      cout << '(';
      right->print();
      cout << ')';
    } else {
      right->print();
    }
  }
};

class leaf : public node {
private:
  int value;

public:
  leaf(int v) : node('.'), value(v) {
  }

  void print(void) {
    cout << value;
  }
};

int main(void)
{
  int c = 0, n, t;
  string e, o, x, y, z;
  map<string, node *> v;
  vector<node *> b;
  node *l, *r, *s;

  cin >> t;

  while (t--) {
    v.clear();
    b.clear();
    cin >> n;
    cout << "Expression #" << ++c << ": ";
    while (n--) {
      cin >> x >> e >> y >> o >> z;
      if (y[0] < 'A')
        b.push_back(l = new leaf(atoi(y.c_str())));
      else
        l = v[y];
      if (z[0] < 'A')
        b.push_back(r = new leaf(atoi(z.c_str())));
      else
        r = v[z];
      s = new expression(l, r, o[0]);
      v.insert(make_pair(x, s));
    }
    s->print();
    cout << endl;
    for_each(v.begin(), v.end(), delete_second<string, node>());
    for_each(b.begin(), b.end(), delete_pointer<node>());
  }

  return 0;
}
