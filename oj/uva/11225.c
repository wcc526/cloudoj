#include <stdio.h>

int main()
{
  int c, n, o, t, g = 0;
  char s[9], v[11];

  scanf("%d", &c);

  while (c--) {
    scanf("%d", &n);
    o = t = 0;
    while (n--) {
      scanf("%s", v);
      if (v[0] == 'f' && v[2] == 'o') {
        t += 9, ++o;
      } else {
        scanf("%*s %s", s);
        if (v[0] == 'k')
          t += v[1] == 'i' ? 9 : 5;
        else if (v[0] == 'q')
          t += 7;
        else if (v[0] == 'j')
          t += 3;
        else if (v[0] == 'o')
          t += 9, ++o;
        else if (v[0] == 't' && v[3] == 'n' && v[6] == '-' && s[0] == 't')
          t += 9, ++o;
        else
          ++t;
      }
    }
    t >>= 1;
    t -= o == 3 ? 36 : o == 2 ? 41 : o == 1 ? 51 : 56;
    printf("Hand #%d\nGame %s by %d point(s).\n",
           ++g, t < 0 ? "lost" : "won", t < 0 ? -t : t);
    if (c)
      putchar('\n');
  }

  return 0;
}
