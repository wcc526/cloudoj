{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
const
  InFile   = 'p10418.in';
  OutFile   ='p10418.out';
  dire:array[0..3,1..2]of integer=
    ((-1,0),(0,1),(1,0),(0,-1));
type
  arr=array[1..101]of integer;
var
  n,m,kk,ans:integer;
  pos,goal:array[1..101,1..2]of integer;
  map,cost,dist:array[1..101]of arr;
  q:array[1..10000,1..2]of integer;
  match:array[1..101]of integer;
  visited:array[1..101]of boolean;
  cases        : longint;

procedure init;
var
  i,j,tt,p,x,y,c:integer;
begin
  readln(n,m,kk,tt);
  for i:=1 to kk*2+1 do read(pos[i,1],pos[i,2]);
  p:=0;
  for i:=1 to tt do begin
    read(x,y,c);
    for j:=1 to c do begin
      inc(p);goal[p,1]:=x;goal[p,2]:=y
    end
  end;
  for i:=1 to n do for j:=1 to m do read(map[i,j]);
end;

procedure getdist(var dist:arr;x,y:integer;b:boolean);
var
  i,head,tail,head1,tail1,x1,y1:integer;
begin
  fillchar(cost,sizeof(cost),$ff);
  head:=0;tail:=1;
  q[1,1]:=x;q[1,2]:=y;cost[x,y]:=0;
  while head<tail do begin
    head1:=head;
    while head<tail do begin
      inc(head);
      x:=q[head,1];y:=q[head,2];
      for i:=0 to 3 do begin
        x1:=x+dire[i,1];y1:=y+dire[i,2];
        if (x1>0)and(x1<=n)and(y1>0)and(y1<=m)and
          ((map[x1,y1]=map[x,y])or((map[x1,y1]>map[x,y])xor b)) then begin
            if cost[x1,y1]=-1 then begin
              inc(tail);
              q[tail,1]:=x1;q[tail,2]:=y1;
              cost[x1,y1]:=cost[x,y]
            end
          end
      end
    end;
    head:=head1;
    tail1:=tail;
    while head<tail1 do begin
      inc(head);
      x:=q[head,1];y:=q[head,2];
      for i:=0 to 3 do begin
        x1:=x+dire[i,1];y1:=y+dire[i,2];
        if (x1>0)and(x1<=n)and(y1>0)and(y1<=m)and(cost[x1,y1]=-1) then begin
          inc(tail);
          q[tail,1]:=x1;q[tail,2]:=y1;
          cost[x1,y1]:=cost[x,y]+1
        end
      end
    end;
    b:=not b
  end;
  for i:=1 to kk*2+1 do dist[i]:=cost[goal[i,1],goal[i,2]]
end;

procedure prepare;
var
  i,j,k:integer;
begin
  for i:=1 to kk do getdist(dist[i],pos[i,1],pos[i,2],false);
  for i:=kk+1 to kk+kk do getdist(dist[i],pos[i,1],pos[i,2],true)
end;

function find(k:integer):boolean;
var
  t,i:integer;
begin
  if visited[k] then begin find:=false;exit end;
  visited[k]:=true;
  for i:=1 to kk*2+1 do if (dist[k,i]<=ans)then begin
    t:=match[i];match[i]:=k;
    if (t=0)or find(t) then begin find:=true;exit end;
    match[i]:=t
  end;
  find:=false
end;

procedure work;
var
  i,j,maxmatch:integer;
  matched:array[1..100]of boolean;
begin
  fillchar(match,sizeof(match),0);
  maxmatch:=0;
  ans:=-1;
  fillchar(matched,sizeof(matched),0);
  while maxmatch+ans<kk*2 do begin
    inc(ans);
    for i:=1 to kk*2 do if not matched[i] then begin
      fillchar(visited,sizeof(visited),0);
      if find(i) then begin
        matched[i]:=true;
        inc(maxmatch)
      end
    end
  end;
  writeln(ans);
end;

begin
//  assign(input,InFile);reset(input);
//  assign(output,OutFile);rewrite(output);
    readln(Cases);
    while Cases > 0 do
      begin
          init;
          prepare;
          work;
          dec(Cases);
      end;
//  close(output);
//  close(input);
end.