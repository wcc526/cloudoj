{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10544.in';
    OutFile    = 'p10544.out';

Type
    Tmap       = array['A'..'Z' , 'A'..'Z'] of boolean;
    Tstr       = array[1..26] of char;
    Tnumber    = array['A'..'Z'] of longint;
    Tvisited   = array['A'..'Z'] of boolean;

Var
    map        : Tmap;
    s ,
    sorted     : Tstr;
    number     : Tnumber;
    visited    : Tvisited;
    answer ,
    N , M , Len ,
    Cases      : longint;

procedure init;
var
    i          : longint;
    c1 , c2    : char;
begin
    dec(Cases);
    readln(N , M);
    fillchar(map , sizeof(map) , 0);
    fillchar(s , sizeof(s) , 0);

    for i := 1 to M do
      begin
          readln(c1 , c2);
          map[c1 , c2] := true;
      end;
end;

procedure DP;
var
    i          : longint;
    upC , c    : char;
begin
    upC := chr(ord('A') + N - 1);
    fillchar(number , sizeof(number) , 0);
    for i := N downto 1 do
      begin
          for c := 'A' to upC do
            if map[sorted[i] , c] then
              inc(number[sorted[i]] , number[c]);
          if number[sorted[i]] = 0 then number[sorted[i]] := 1;
      end;

    answer := 1;
    for i := 1 to Len - 1 do
      begin
          for c := 'A' to s[i + 1] do
            if c <> s[i + 1] then
              if map[s[i] , c] then
                inc(answer , number[c]);
      end;
end;

procedure work;
var
    i , Q      : longint;
    c1 , c2 ,
    upc        : char;
    find       : boolean;
begin
    fillchar(visited , sizeof(visited) , 0);
    upC := chr(ord('A') + N - 1);
    for i := 1 to N do
      begin
          for c1 := 'A' to upC do
            if not visited[c1] then
              begin
                  find := true;
                  for c2 := 'A' to upC do
                    if not visited[c2] and map[c2 , c1] then
                      begin
                          find := false; break;
                      end;
                  if find
                    then begin
                             sorted[i] := c1;
                             visited[c1] := true;
                             break;
                         end;
              end;
      end;

    readln(Q);
    for i := 1 to Q do
      begin
          Len := 0;
          fillchar(s , sizeof(s) , 0);
          while not eoln do
            begin
                inc(Len);
                read(s[Len]);
            end;
          readln;
          DP;
          for Len := 1 to Len do write(s[Len]);
          write(': ');
          writeln(answer);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
