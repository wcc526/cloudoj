Const
    InFile     = 'p10371.in';
    dat        : array[1..64] of string =
('WET','0',
'UTC','0',
'GMT','0',
'BST','+1',
'IST','+1',
'WEST','+1',
'CET','+1',
'CEST','+2',
'EET','+2',
'EEST','+3',
'MSK','+3',
'MSD','+4',
'AST','-4',
'ADT','-3',
'NST','-3.5',
'NDT','-2.5',
'EST','-5',
'EDT','-4',
'CST','-6',
'CDT','-5',
'MST','-7',
'MDT','-6',
'PST','-8',
'PDT','-7',
'HST','-10',
'AKST','-9',
'AKDT','-8',
'AEST','+10',
'AEDT','+11',
'ACST','+9.5',
'ACDT','+10.5',
'AWST','+8');

Type
    Tstr       = string[100];

Var
    time , A , B ,
    cases      : longint;
    p1 , p2    : double;
    s , s1 , s2: Tstr;

procedure read_str(var s : Tstr);
var
    ch         : char;
begin
    s := '';
    repeat read(ch); until ch <> ' ';
    while ch <> ' ' do
      begin
          s := s + ch;
          if eoln then ch := ' ' else read(ch);
      end;
end;

Begin
    readln(cases);
    while Cases > 0 do
      begin
          dec(Cases);
          read_str(s);
          if s = 'midnight'
            then time := 0
            else if s = 'noon'
                   then time := 12 * 60
                   else begin
                            s1 := copy(s , 1 , pos(':' , s) - 1);
                            s2 := copy(s , pos(':' , s) + 1 , length(s) - pos(':' , s));
                            val(s1 , A); if A = 12 then A := 0; val(s2 , B);
                            time := A * 60 + B;
                            read_str(s);
                            if s = 'p.m.' then inc(time , 12 * 60);
                        end;
          read_str(s); A := 1;
          while dat[A] <> s do inc(A , 2);
          read_str(s); B := 1;
          while dat[B] <> s do inc(B , 2);
          val(dat[A + 1] , p1); val(dat[B + 1] , p2);
          A := round(p1 * 60); B := round(p2 * 60);
          time := (time - A + B + 24 * 60) mod (24 * 60);
          readln;
          if time = 0
            then writeln('midnight')
            else if time = 12 * 60
                   then writeln('noon')
                   else begin
                            A := time div 60 mod 12; if A = 0 then A := 12;
                            B := time mod 60;
                            write(A , ':' , B div 10 , B mod 10 , ' ');
                            if time > 12 * 60
                              then writeln('p.m.')
                              else writeln('a.m.');
                        end;
      end;
End.
