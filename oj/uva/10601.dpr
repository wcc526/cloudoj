{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10601.in';
    OutFile    = 'p10601.out';
    LimitLen   = 1500;
    Limit      = 12;
    LimitSave  = 100;
    next       : array[1..3 , 1..12] of word
{                   1   2   3   4   5   6   7   8   9  10  11  12 }
               = (( 2,  3,  4,  1, 10, 11, 12,  9,  5,  8,  7,  6) ,
                  (12, 11, 10,  9,  6,  7,  8,  5,  1,  2,  3,  4) ,
                  ( 5,  6,  7,  8,  2,  1,  4,  3, 10, 11, 12,  9));

Type
    lint       = record
                     len      : word;
                     data     : array[1..LimitLen] of word;
                 end;
    Tdata      = array[1..Limit] of word;
    Tpermu     = array[1..Limit] of word;
    Trotate    = array[1..LimitSave] of Tpermu;
    Tsection   = record
                     tot      : word;
                     data     : array[1..Limit] of word;
                 end;
    Tsections  = array[1..LimitSave] of Tsection;
    Tvisited   = array[1..Limit] of boolean;
    Tpower     = array[0..Limit] of lint;

Var
    data       : Tdata;
    rotate     : Trotate;
    visited    : Tvisited;
    sections   : Tsections;
    Signal ,
    tot ,
    Cases      : word;

    answer1    : extended;

    power      : Tpower;
    N ,
    answer2    : lint;
{    tmpans ,
    tmpN       : extended;}

procedure perform(p1 , p2 , p3 : word; var permu : Tpermu);
var
    i , j , p  : word;
begin
    fillchar(permu , sizeof(permu) , 0);
    for i := 1 to 12 do
      begin
          p := i;
          for j := 1 to p1 do p := next[1 , p];
          for j := 1 to p2 do p := next[2 , p];
          for j := 1 to p3 do p := next[3 , p];
          permu[i] := p;
      end;
end;

procedure get_section;
var
    i , j , p ,
    count      : word;
begin
    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to 12 do
      if not visited[i] then
        begin
            p := i; count := 0;
            while not visited[p] do
              begin
                  visited[p] := true; p := rotate[tot , p];
                  inc(count);
              end;
            inc(sections[tot].tot);
            sections[tot].data[sections[tot].tot] := count;
        end;
    for i := 1 to sections[tot].tot do
      for j := 1 to sections[tot].tot - 1 do
        if sections[tot].data[j] > sections[tot].data[j + 1] then
          begin
              p := sections[tot].data[j];
              sections[tot].data[j] := sections[tot].data[j + 1];
              sections[tot].data[j + 1] := p;
          end;
end;

procedure ins(permu : Tpermu);
var
    i , j      : word;
    same       : boolean;
begin
    for i := 1 to tot do
      begin
          same := true;
          for j := 1 to 12 do
            if permu[j] <> rotate[i , j] then
              begin
                  same := false;
                  break;
              end;
          if same then exit;
      end;
    inc(tot);
    rotate[tot] := permu;
    get_section;
end;

procedure pre_process;
var
    p1 , p2 , p3
               : word;
    permu      : Tpermu;
begin
    tot := 0;
    for p1 := 0 to 3 do
      for p2 := 0 to 3 do
        for p3 := 0 to 3 do
          begin
              perform(p1 , p2 , p3 , permu);
              ins(permu);
          end;
end;

procedure init;
var
    i , p      : word;
    c          : char;
begin
    dec(Cases);
    if Signal = 1
      then begin
               fillchar(data , sizeof(data) , 0);
               for i := 1 to 12 do
                 begin
                     read(p);
                     inc(data[p]);
                 end;
           end
      else begin
               fillchar(N , sizeof(N) , 0);
{               tmpN := 0;}
               while not eoln do
                 begin
                     inc(N.len);
                     read(c);
                     N.data[N.len] := ord(c) - ord('0');
{                     tmpN := tmpN * 10 + N.data[N.len];}
                 end;
               for i := 1 to N.len div 2 do
                 begin
                     p := N.data[i]; N.data[i] := N.data[N.len - i + 1];
                     N.data[N.len - i + 1] := p;
                 end;
               readln;
           end;
end;

function C(m , n : word) : extended;
var
    res        : extended;
    i          : word;
begin
    res := 1;
    for i := m downto m - n + 1 do res := res * i;
    for i := 1 to n do res := res / i;
    C := res;
end;

function express_Multi(start , last : word; section : Tsection; data : Tdata)
                      : extended;
var
    res        : extended;
    i          : word;
begin
    if section.data[start] = section.data[section.tot]
      then begin
               res := 1; express_Multi := 0;
               if last mod section.data[start] = 0
                 then last := last div section.data[start]
                 else exit;
               for i := 1 to 12 do
                 if data[i] <> 0 then
                   if data[i] mod section.data[start] <> 0
                     then exit
                     else begin
                              res := res * C(last , data[i] div section.data[start]);
                              dec(last , data[i] div section.data[start]);
                          end;
               express_Multi := res;
           end
      else begin
               res := 0;
               for i := 1 to 12 do
                 if data[i] > 0 then
                   begin
                       dec(data[i]);
                       res := res + express_Multi(start + 1 , last - 1 , section , data);
                       inc(data[i]);
                   end;
               express_Multi := res;
           end;
end;

procedure work1;
var
    res        : extended;
    i          : word;
begin
    res := 0;
    for i := 1 to tot do
      res := res + express_Multi(1 , 12 , sections[i] , data);
    answer1 := res / tot;
end;

procedure times(const num1 , num2 : lint; var num3 : lint);
var
    i , j ,
    jw , tmp   : word;
begin
    fillchar(num3 , sizeof(num3) , 0);
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := num1.data[i] * num2.data[j] + jw + num3.data[i + j - 1];
                jw := tmp div 10;
                num3.data[i + j - 1] := tmp mod 10;
                inc(j);
            end;
          dec(j);
          if i + j - 1 > num3.len then
            num3.len := i + j - 1;
      end;
    if num3.len = 0 then num3.len := 1;
    while (num3.len > 1) and (num3.data[num3.len] = 0) do dec(num3.len);
end;

procedure add(var num1 : lint; const num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          num1.data[i] := tmp mod 10;
          inc(i);
      end;
    dec(i);
    num1.len := i;
end;

procedure division(var num1 : lint; num2 : word);
var
    i , last   : longint;
begin
    last := 0;
    for i := num1.len downto 1 do
      begin
          last := last * 10 + num1.data[i];
          num1.data[i] := last div num2;
          last := last mod num2;
      end;
    while (num1.len > 1) and (num1.data[num1.len] = 0) do dec(num1.len);
end;

procedure Task2_pre_process;
var
    i          : word;
begin
    fillchar(power , sizeof(power) , 0);
    power[0].len := 1; power[0].data[1] := 1;
    for i := 1 to 12 do
      times(power[i - 1] , N , power[i]);
end;

procedure work2;
var
    i          : longint;
{    tmp        : extended;}
begin
    fillchar(answer2 , sizeof(answer2) , 0);
    answer2.len := 1;
    for i := 1 to tot do
      begin
          add(answer2 , power[sections[i].tot]);
{          tmp := 1;
          for j := 1 to sections[i].tot do tmp := tmp * tmpN;
          tmpans := tmpans + tmp;}
      end;
    division(answer2 , tot);
{    tmpans := tmpans / tot;}
end;

procedure work;
begin
    Task2_pre_process;
    if Signal = 1
      then work1
      else work2;
end;

procedure out;
var
    i          : word;
begin
    if Signal = 1
      then writeln(answer1 : 0 : 0)
      else begin
               for i := answer2.len downto 1 do
                 write(answer2.data[i]);
               writeln;
{               writeln(tmpans : 0 : 0);}
           end;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln({Signal , }Cases);
      Signal := 1;
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.