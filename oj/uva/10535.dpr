{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10535.in';
    OutFile    = 'p10535.out';
    Limit      = 500;
    zero       = 1e-5;

Type
    Tpoint     = record
                     x , y , angle           : extended;
                     signal                  : byte;
                 end;
    Tsegment   = record
                     p1 , p2                 : Tpoint;
                     deleted                 : boolean;
                 end;
    Tdata      = array[1..Limit] of Tsegment;
    Tsorted    = array[1..Limit * 2] of Tpoint;

Var
    data       : Tdata;
    sorted     : Tsorted;
    p          : Tpoint;
    N , answer : longint;

function init : boolean;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(sorted , sizeof(sorted) , 0);
    read(N);

    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 with data[i] do
                   read(p1.x , p1.y , p2.x , p2.y);
               read(p.x , p.y);
           end;
end;

function calc_angle(x , y : extended) : extended;
begin
    if y >= 0
      then begin
               if abs(x) <= zero
                 then calc_angle := pi / 2
                 else if x < 0
                        then calc_angle := pi + arctan(y / x)
                        else calc_angle := arctan(y / x);
           end
      else begin
               calc_angle := 2 * pi - calc_angle(x , -y);
           end;
end;

function bigger(p1 , p2 : Tpoint) : boolean;
begin
    if abs(p1.angle - p2.angle) <= zero
      then bigger := p1.signal < p2.signal
      else bigger := p1.angle > p2.angle;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := sorted[tmp]; sorted[tmp] := sorted[start];
    while start < stop do
      begin
          while (start < stop) and bigger(sorted[stop] , key) do dec(stop);
          sorted[start] := sorted[stop];
          if start < stop then inc(start);
          while (start < stop) and bigger(key , sorted[start]) do inc(start);
          sorted[stop] := sorted[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    sorted[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i , tot ,
    tmp , max  : longint;
    st , ed    : extended;
begin
    answer := 0;
    for i := 1 to N do
      begin
          data[i].p1.angle := calc_angle(data[i].p1.x - p.x , data[i].p1.y - p.y);
          data[i].p2.angle := calc_angle(data[i].p2.x - p.x , data[i].p2.y - p.y);
          if abs(abs(data[i].p1.angle - data[i].p2.angle) - pi) <= zero
            then begin
                     data[i].deleted := true;
                     inc(answer);
                 end
            else if (data[i].p1.angle < data[i].p2.angle) xor (abs(data[i].p1.angle - data[i].p2.angle) > pi)
                   then begin
                            data[i].p1.signal := 1; data[i].p2.signal := 0;
                        end
                   else begin
                            data[i].p1.signal := 0; data[i].p2.signal := 1;
                        end;
      end;

    tot := 0;
    for i := 1 to N do
      if data[i].deleted
        then
        else begin
                 sorted[tot + 1] := data[i].p1;
                 sorted[tot + 2] := data[i].p2;
                 inc(tot , 2) ;
             end;
    qk_sort(1 , tot);

    tmp := 0;
    for i := 1 to N do
      if not data[i].deleted then
        begin
            if data[i].p1.signal = 1
              then begin st := data[i].p1.angle; ed := data[i].p2.angle; end
              else begin st := data[i].p2.angle; ed := data[i].p1.angle; end;
            if st > ed then
              inc(tmp);
        end;
    max := tmp;
    for i := 1 to tot do
      begin
          if sorted[i].signal = 1
            then inc(tmp)
            else dec(tmp);
          if tmp > max then max := tmp;
      end;

    inc(answer , max);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
