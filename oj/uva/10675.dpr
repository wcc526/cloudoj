{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10675.in';
    OutFile    = 'p10675.out';
    Limit      = 11;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);
    minimum    = 1e-6;

Type
    Tdata      = array[0..Limit + 1 , 0..Limit + 1] of extended;
    Tvisited   = array[0..Limit + 1 , 0..Limit + 1] of boolean;
    Tmap       = array[1..Limit * Limit , 1..Limit * Limit] of longint;
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tkey       = record
                     height   : extended;
                     area     : longint;
                     data     : array[1..Limit * Limit] of Tpoint;
                 end;
    Tblocks    = array[1..Limit * Limit] of Tkey;
    Trate      = array[1..Limit * Limit] of extended;
    Tdegree    = array[1..Limit * Limit] of longint;

Var
    data       : Tdata;
    map        : Tmap;         
    visited    : Tvisited;
    blocks     : Tblocks;
    rate       : Trate;
    degree     : Tdegree;
    answer     : extended;
    tot ,
    N , M ,
    x0 , y0    : longint;

function init : boolean;
var
    i , j      : longint;
begin
    for i := 0 to Limit + 1 do
      for j := 0 to Limit + 1 do
        data[i , j] := -1;
    readln(N , M);
    init := false;
    if N = 0 then exit;
    init := true;
    readln(x0 , y0);
    for i := 1 to N do
      for j := 1 to M do
        read(data[i , j]);
end;

function zero(num : extended) : boolean;
begin
    zero := abs(num) <= minimum;
end;

procedure dfs(x , y : longint);
var
    i ,
    nx , ny    : longint;
begin
    visited[x , y] := true;
    inc(blocks[tot].area);
    blocks[tot].data[blocks[tot].area].x := x;
    blocks[tot].data[blocks[tot].area].y := y;
    for i := 1 to 4 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx <= N + 1) and (ny <= M + 1) and (nx >= 0) and (ny >= 0) then
            if not visited[nx , ny] and zero(data[nx , ny] - data[x , y]) then
              dfs(nx , ny);
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := blocks[tmp]; blocks[tmp] := blocks[start];
    while start < stop do
      begin
          while (start < stop) and (blocks[stop].height < key.height) do dec(stop);
          blocks[start] := blocks[stop];
          if start < stop then inc(start);
          while (start < stop) and (blocks[start].height > key.height) do inc(start);
          blocks[stop] := blocks[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    blocks[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure Build_Graph;
var
    p1 , p2 ,
    i , j      : longint;
    find       : boolean;        
begin
    fillchar(blocks , sizeof(blocks) , 0);
    fillchar(visited , sizeof(visited) , 0);
    tot := 0;
    for i := 0 to N + 1 do
      for j := 0 to M + 1 do
        if not visited[i , j] then
          begin
              inc(tot);
              dfs(i , j);
              blocks[tot].height := data[i , j];
          end;
    qk_sort(1 , tot);

    fillchar(map , sizeof(map) , 0);
    fillchar(degree , sizeof(degree) , 0);
    for i := 1 to tot do
      for j := i + 1 to tot do
        if (i <> j) and (blocks[i].height > blocks[j].height) then
          begin
              map[i , j] := 0;
              for p1 := 1 to blocks[i].area do
                for p2 := 1 to blocks[j].area do 
                  if abs(blocks[i].data[p1].x - blocks[j].data[p2].x) + abs(blocks[i].data[p1].y - blocks[j].data[p2].y) = 1 then
                    begin
                        inc(map[i , j]);
                        inc(degree[i]);
                    end;
          end;

    fillchar(rate , sizeof(rate) , 0);
    for i := 1 to tot do
      begin
          find := false;
          for j := 1 to blocks[i].area do
            if (blocks[i].data[j].x = x0) and (blocks[i].data[j].y = y0) then
              begin
                  rate[i] := 1;
                  find := true;
                  break;
              end;
          if find then break;
      end;
      
    for i := 1 to tot do
      if (degree[i] > 0) and (rate[i] > 0) then
        begin
            for j := i + 1 to tot do
              if map[i , j] > 0 then
                rate[j] := rate[j] + rate[i] * map[i , j] / degree[i];
            rate[i] := 0;
        end;
end;

procedure work;
var
    i , j      : longint;
    nextbulk ,
    nexttime ,
    tmptime    : extended;
begin
    answer := 0;
    while true do
      begin
          Build_Graph;
          if rate[tot] > 0 then break;
          nexttime := 1e6;
          for i := 1 to tot do
            if rate[i] > 0 then
              begin
                  nextbulk := (int(blocks[i].height + minimum) + 1 - blocks[i].height) * blocks[i].area;
                  tmptime := nextbulk / rate[i];
                  if tmptime < nexttime then nexttime := tmptime;
              end;
          answer := answer + nexttime;
          for i := 1 to tot do
            if rate[i] > 0 then
              begin
                  rate[i] := rate[i] / blocks[i].area * nexttime;
                  for j := 1 to blocks[i].area do
                    with blocks[i].data[j] do
                      data[x , y] := data[x , y] + rate[i];
              end;
      end;
end;

procedure out;
begin
    writeln(answer : 0 : 2);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
