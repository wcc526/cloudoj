#include <stdio.h>

int main()
{
  int c[64], i, n, r, t, x;
  char s;

  for (;;) {
    n = 0;
    do
      if (scanf("%d", &c[n++]) < 1)
        return 0;
    while (scanf("%c", &s) == 1 && s ^ '\n');
    s = 0;
    do {
      scanf("%d", &x);
      for (r = 0, t = 1, i = n; i--; t *= x)
        r += c[i] * t;
      printf("%s%d", s ? " " : "", r);
    } while (scanf("%c", &s) == 1 && s ^ '\n');
    putchar('\n');
  }

  return 0;
}
