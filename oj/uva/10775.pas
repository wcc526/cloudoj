Var
    N , p , i  : longint;
    data       : array[0..1000] of longint;

Begin
    readln(N);
    while N <> 0 do
      begin
          if not odd(N)
            then writeln('IMPOSSIBLE')
            else begin
                     p := N div 3;
                     for i := 1 to p do
                       if odd(i)
                         then data[i] := p - i div 2
                         else data[i] := p div 2 - i div 2 + 1;
                     data[0] := data[p];
                     for i := 1 to p do write(7 * p + i , ' ');
                     for i := 1 to p do write(i , ' ');
                     for i := 1 to p do write(5 * p + i , ' ');
                     writeln;
                     for i := 1 to p do write(2 * p + data[i] , ' ');
                     for i := 1 to p do write(4 * p + data[i] , ' ');
                     for i := 1 to p do write(6 * p + data[i] , ' ');
                     writeln;
                     for i := 1 to p do write(3 * p + data[i - 1] , ' ');
                     for i := 1 to p do write(8 * p + data[i - 1] , ' ');
                     for i := 1 to p do write(p + data[i - 1] , ' ');
                     writeln;
                 end;
          readln(N);
      end;
End.