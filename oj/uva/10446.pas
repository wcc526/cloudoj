Type
    Tdata      = array[1..60 , 1..60] of qword;
    Tvisited   = array[1..60 , 1..60] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    T ,
    N , back   : longint;

function dfs(N , Back : longint) : qword;
var
    i          : longint;
begin
    if (N <= 1) or (back <= 0) then exit(1);
    if visited[N , back]
      then dfs := (data[N , back])
      else begin
               visited[N , Back] := true;
               data[N , Back] := 1;
               for i := 1 to back do
                 data[N , back] := qword(data[N , back]) + dfs(N - i , back);
               dfs := (data[N , back]);
           end;
end;

Begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(data , sizeof(data) , 0);
    readln(N , Back);
    T := 0;
    while N <= 60 do
      begin
          inc(T);
          write('Case ' , T , ': ');
          writeln(dfs(N , back));
          readln(n , back);
      end;
End.
