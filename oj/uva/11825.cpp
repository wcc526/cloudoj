/*
 * uva_11825.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int P[1<<17], cover[1<<17], f[1<<17];
int main() {
	int kase = 0;
	int n;
	//freopen("in.txt", "r", stdin);
	while (~scanf("%d", &n)&&n) {
		for (int i = 0; i < n; ++i) {
			int m, x;
			scanf("%d", &m);
			P[i] = 1 << i;
			while (m--) {
				scanf("%d", &x);
				P[i] |= (1 << x);
			}
		}
		for (int S = 0; S < (1 << n); S++) {
			cover[S] = 0;
			for (int i = 0; i < n; ++i)
				if (S & (1 << i))
					cover[S] |= P[i];
		}
		f[0] = 0;
		int ALL = (1 << n) - 1;
		for (int S = 1; S < (1 << n); S++) {
			f[S] = 0;
			for (int S0 = S; S0; S0 = (S0 - 1) & S)
				if (cover[S0] == ALL)
					f[S] = max(f[S], f[S ^ S0] + 1);
		}
		printf("Case %d: %d\n", ++kase, f[ALL]);
	}
	return 0;
}

