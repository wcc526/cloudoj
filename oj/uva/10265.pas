Const
    Limit      = 14;

Type
    Tdata      = array[0..Limit] of longint;
    Tvisited   = array[0..Limit] of boolean;

Var
    data       : Tdata;
    v1 , v2 ,
    v3         : Tvisited;
    modulo ,
    N , M , K  : longint;

function dfs(step , last : longint) : boolean;
var
    i          : longint;
begin
    if last > N - step + 1 then exit(false);
    if last = 0 then
      begin
          for i := 1 to N do
            if data[i] <> -1
              then writeln(i , ' ' , data[i]);
          exit(true);
      end;
    if step > N then exit(false);
    for i := 1 to M do
      if not v1[i] and not v2[(step - i + Modulo * 10) mod Modulo] and not v3[(step + i) mod Modulo] then
        begin
            v1[i] := true;
            v2[(step - i + Modulo * 10) mod Modulo] := true;
            v3[(step + i) mod Modulo] := true;
            data[step] := i;
            if dfs(step + 1 , last - 1) then
              exit(true);
            v1[i] := false;
            v2[(step - i + Modulo * 10) mod Modulo] := false;
            v3[(step + i) mod Modulo] := false;
            data[step] := -1;
        end;
    if dfs(step + 1 , last) then exit(true);
    exit(false);
end;

procedure work;
begin
    fillchar(data , sizeof(data) , $FF);
    fillchar(v1 , sizeof(v1) , 0); fillchar(v2 , sizeof(v2) , 0);
    fillchar(v3 , sizeof(v3) , 0);
    modulo := N;
    while (N mod modulo <> 0) or (M mod modulo <> 0) do dec(modulo);
    if (K > modulo) or not dfs(1 , K)
      then writeln('0 0');
end;

Begin
    while not eof do
      begin
          readln(N , M , K);
          work;
      end;
End.