/*
 * uva_11300.cpp
 *
 *  Created on: 2012-12-8
 *      Author: Administrator
 */
#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

const int MXN=1000000+10;
typedef long long LL;
LL A[MXN],B[MXN],C[MXN],ToT,M;
int main()
{
	int N;
	ios::sync_with_stdio(false);
	//while(~scanf("%d",&N))
	while(cin>>N)
	{
		ToT=0;
		int i,j,k;
		for(i=1;i<=N;++i)
		{
		//	scanf("%d",&A[i]);
			cin >> A[i];
			ToT+=A[i];
		}
		M=ToT/N;
		C[0]=0;
		for(i=1;i<N;++i)
			C[i]=C[i-1]+A[i]-M;
		sort(C,C+N);
		LL x1=C[N/2];
		LL ans=0;
		for(i=0;i<N;++i)
			ans+=abs(x1-C[i]);
		printf("%lld\n",ans);
	}
	return 0;
}






