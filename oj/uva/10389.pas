Const
    InFile     = 'p10389.in';
    Limit      = 300;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tpoints    = object
                     tot      : longint;
                     data     : array[1..Limit] of Tpoint;
                     function find(p : Tpoint) : longint;
                 end;
    Tmap       = array[1..Limit , 1..Limit] of double;
    Tshortest  = array[1..Limit] of double;

Var
    points     : Tpoints;
    map        : Tmap;
    shortest   : Tshortest;
    cases ,
    S , T      : longint;

function Tpoints.find(p : Tpoint) : longint;
var
    i          : longint;
begin
    for i := 1 to tot do
      if (p.x = data[i].x) and (p.y = data[i].y) then
        exit(i);
    inc(tot);
    data[tot].x := p.x; data[tot].y := p.y;
    exit(tot);
end;

function read_point : longint;
var
    p          : Tpoint;
begin
    read(p.x , p.y);
    if p.x = -1 then exit(-1);
    exit(points.find(p));
end;

procedure init;
var
    i , j ,
    last , p   : longint;
begin
    points.tot := 0;
    dec(Cases);
    S := read_point; T := read_point;
    readln;
    for i := 1 to Limit do
      for j := 1 to Limit do
        map[i , j] := 10 * 1000 / 60;

    while not eoln do
      begin
          last := read_point; p := read_point;
          while p <> -1 do
            begin
                map[last , p] := 40 * 1000 / 60; map[p , last] := 40 * 1000 / 60;
                last := p;
                p := read_point;
            end;
          readln;
      end;
    for i := 1 to points.tot do
      for j := 1 to points.tot do
        map[i , j] := sqrt(sqr(points.data[i].x - points.data[j].x) + sqr(points.data[i].y - points.data[j].y)) / map[i , j];
    readln;
end;

procedure work;
var
    i , j      : longint;
    changed    : boolean;
begin
    for i := 1 to points.tot do shortest[i] := 1e10;
    shortest[S] := 0;
    repeat
      changed := false;
      for i := 1 to points.tot do
        for j := 1 to points.tot do
          if shortest[i] + map[i , j] < shortest[j] - 1e-6 then
            begin
                shortest[j] := shortest[i] + map[i , j];
                changed := true;
            end;
    until not changed;
end;

procedure out;
begin
    writeln(shortest[T] : 0 : 0);
    if cases > 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
