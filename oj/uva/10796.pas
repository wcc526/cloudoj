Const
    InFile     = 'p10796.in';
    OutFile    = 'p10796.out';
    Limit      = 25;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , 1 , 0 , -1);

Type
    Tdata      = array[1..Limit , 1..Limit] of char;
    Tpath      = array[1..Limit * Limit * 4] of
                   record
                       dir , x , y           : longint;
                   end;
    Tvisited   = array[1..Limit * Limit * 4 , 1..Limit , 1..Limit] of longint;
    Tqueue     = array[1..Limit * Limit * 4 * Limit * Limit] of
                   record
                       time                  : longint;
                       x , y                 : byte;
                   end;

Var
    data       : Tdata;
    path       : Tpath;
    queue      : Tqueue;
    visited    : Tvisited;
    answer ,
    cases ,
    x0 , y0 ,
    x1 , y1 ,
    N , M ,
    tot , T    : longint;
    circle     : boolean;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
    dec(T); inc(cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(path , sizeof(path) , 0);
    readln(N , M); tot := 1;
    read(x0 , y0 , x1 , y1 , path[1].x , path[1].y);
    inc(x0); inc(y0); inc(x1); inc(y1); inc(path[1].x); inc(path[1].y);
    read(ch); while ch = ' ' do read(ch);
    readln;
    case ch of
      'U'      : path[1].dir := 1;
      'R'      : path[1].dir := 2;
      'D'      : path[1].dir := 3;
      'L'      : path[1].dir := 4;
    end;
    for i := 1 to N do
      begin
          for j := 1 to M do read(data[i , j]);
          readln;
      end;
end;

procedure Get_Path;
var
    i , j ,
    nx , ny    : longint;
begin
    circle := false;
    while true do
      begin
          with path[tot] do
            begin
                dir := dir mod 4 + 1;
                i := 1;
                while i <= 4 do
                  begin
                      nx := x + dirx[dir]; ny := y + diry[dir];
                      if (nx <= 0) or (ny <= 0) or (nx > N) or (ny > M) then break;
                      if data[nx , ny] = '.' then break;
                      dir := (dir + 2) mod 4 + 1;
                      inc(i);
                  end;
                if i > 4 then exit;
                if (nx <= 0) or (ny <= 0) or (nx > N) or (ny > M) then break;
                if (tot > 1) and (dir = path[1].dir) and (x = path[1].x) and (y = path[1].y) then
                  begin circle := true; dec(tot); break; end;
            end;
          inc(tot);
          path[tot].dir := path[tot - 1].dir;
          path[tot].x := nx; path[tot].y := ny;
      end;
end;

procedure work;
var
    open , closed ,
    i , j , time ,
    nx , ny ,
    px , py ,
    x , y ,
    qx , qy    : longint;
begin
    if (x0 = x1) and (y0 = y1) then begin answer := 0; exit; end;
    if (x0 = path[1].x) and (y0 = path[1].y) then begin answer := -1; exit; end;
    Get_Path;

    open := 1; closed := 1;
    fillchar(visited , sizeof(visited) , $FF);
    queue[1].time := 1; queue[1].x := x0; queue[1].y := y0;
    visited[1 , x0 , y0] := 1;
    while open <= closed do
      begin
          x := queue[open].x; y := queue[open].y; time := queue[open].time;
          px := path[time].x; py := path[time].y;
          qx := path[time mod tot + 1].x; qy := path[time mod tot + 1].y;
          if (time = tot) and not circle then begin px := 0; py := 0; end;
          for i := -1 to 1 do
            for j := -1 to 1 do
              if abs(i) + abs(j) <= 1 then
                begin
                    nx := x + i; ny := y + j;
                    if (nx <= 0) or (ny <= 0) or (nx > N) or (ny > M) then continue;
                    if data[nx , ny] <> '.' then continue;
                    if (nx = qx) and (ny = qy) then continue;
                    if (nx = px) and (ny = py) and (x = qx) and (y = qy) then continue;
                    if visited[time mod tot + 1 , nx , ny] = -1 then
                      begin
                          visited[time mod tot + 1 , nx , ny] := visited[time , x , y] + 1;
                          inc(closed);
                          queue[closed].time := time mod tot + 1;
                          queue[closed].x := nx; queue[closed].y := ny;
                          if (nx = x1) and (ny = y1) then
                            begin
                                answer := visited[time , x , y];
                                exit;
                            end;
                      end;
                end;
          inc(open);
      end;
    answer := -1;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    assign(INPUT , 'input.txt'); ReSet(INPUT);
    assign(OUTPUT , 'output1.txt'); ReWrite(OUTPUT);
    readln(T); cases := 0;
    while T > 0 do
      begin
          init;
          work;
          writeln('Maze ' , cases , ': ' , answer);
      end;
    Close(OUTPUT);
End.
