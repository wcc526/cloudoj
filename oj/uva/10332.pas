Const
    InFile     = 'p10332.in';
    Limit      = 50;
    LimitNum   = 10000;

Type
    Tdata      = array[1..LimitNum] of longint;
    Tanswer    = array[1..Limit] of longint;

Var
    data       : Tdata;
    answer     : Tanswer;
    noanswer   : boolean;
    N , M      : longint;

procedure init;
var
    i , p      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(answer , sizeof(answer) , 0);
    readln(N); M := 0;
    for i := 1 to N * (N - 1) div 2 do
      begin
          read(p); if p > M then M := p;
          inc(data[p]);
      end;
    answer[N] := M; answer[1] := 0; dec(data[M]);
    readln;
end;

function check(step , p : longint) : boolean;
var
    i          : longint;
begin
    for i := 1 to step - 1 do
      begin
          if data[p - answer[i]] <= 0 then
            exit(false);
          if answer[N] - p = p - answer[i] then
            if data[p - answer[i]] <= 1 then
              exit(false);
      end;
    if data[answer[N] - p] <= 0 then
      exit(false);
    exit(true);
end;

procedure fill(step , p , sign : longint);
var
    i          : longint;
begin
    for i := 1 to step - 1 do
      inc(data[p - answer[i]] , sign);
    inc(data[answer[N] - p] , sign);
end;

function dfs(step , start : longint) : boolean;
begin
    if step = N then
      exit(true);
    while start < M do
      begin
          if data[start] > 0 then
            begin
                if check(step , start) then
                  begin
                      answer[step] := start;
                      fill(step , start , -1);
                      if dfs(step + 1 , start + 1) then
                        exit(true);
                      answer[step] := 0;
                      fill(step , start , 1);
                  end;
            end;
          inc(start);
      end;
    exit(false);
end;

procedure out;
var
    i          : longint;
begin
    if noanswer
      then writeln('Incorrect Balance.')
      else begin
               for i := 1 to N do
                 answer[i] := M - answer[i];
               for i := N downto 1 do
                 write(answer[i] , ' ');
               writeln;
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            noanswer := not dfs(2 , 1);
            out;
        end;
//    Close(INPUT);
End.
