Var
    N , K , answer ,
    last       : longint;
Begin
    while not eof do
      begin
          readln(N , K);
          answer := N; last := N;
          while last >= K do
            begin
                inc(answer , last div K);
                last := last div K + last mod K;
            end;
          writeln(answer);
      end;
End.