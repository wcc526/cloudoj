Var
    N , M , K  : longint;
    p1 , p2    : extended;
Begin
    while not eof do
      begin
          readln(M , N , K);
          p1 := N / (N + M);
          p2 := ((N - 1) * N + N * M) / (N + M) / (N + M - K - 1);
          if p1 > p2
           then writeln(p1 : 0 : 5)
           else writeln(p2 : 0 : 5);
      end;
End.