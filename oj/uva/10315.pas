Const
    InFile     = 'p10315.in';
    Base       = 10000000;

Type
    Tcard      = record
                     num , color             : longint;
                 end;
    Tdata      = array[1..5] of Tcard;

Var
    A , B      : Tdata;
    VA , VB    : longint;

procedure read_card(var card : Tcard);
var
    ch         : char;
begin
    repeat read(ch); until ch <> ' ';
    if ch in ['2'..'9']
      then card.num := ord(ch) - ord('0')
      else case ch of
               'T' : card.num := 10;
               'J' : card.num := 11;
               'Q' : card.num := 12;
               'K' : card.num := 13;
               'A' : card.num := 14;
           end;
    dec(card.num , 2);
    read(ch);
    card.color := ord(ch);
end;

procedure init;
var
    i          : longint;
begin
    for i := 1 to 5 do
      read_card(A[i]);
    for i := 1 to 5 do
      read_card(B[i]);
    readln;
end;

function rank(data : Tdata) : longint;
var
    res , i    : longint;
begin
    res := 0;
    for i := 1 to 5 do res := res * 13 + data[i].num;
    exit(res);
end;

function Flush(var data : Tdata) : boolean;
var
    i          : longint;
begin
    for i := 2 to 5 do
      if data[1].color <> data[i].color then
        exit(false);
    exit(true);
end;

function straight(var data : Tdata) : boolean;
var
    i          : longint;
begin
    for i := 2 to 5 do
      if data[i].num <> data[i - 1].num - 1 then
        exit(false);
    exit(true);
end;

function FourofKind(var data : Tdata; var res : longint) : boolean;
begin
    if (data[1].num = data[2].num) and (data[1].num = data[3].num) and (data[1].num = data[4].num)
      then begin res := data[1].num; exit(true); end;
    if (data[5].num = data[2].num) and (data[5].num = data[3].num) and (data[5].num = data[4].num)
      then begin res := data[5].num; exit(true); end;
    exit(false);
end;

function FullHouse(var data : Tdata; var res : longint) : boolean;
begin
    if (data[1].num = data[2].num) and (data[1].num = data[3].num) and (data[4].num = data[5].num)
      then begin res := data[1].num; exit(true); end;
    if (data[1].num = data[2].num) and (data[3].num = data[4].num) and (data[4].num = data[5].num)
      then begin res := data[3].num; exit(true); end;
    exit(false);
end;

function ThreeofKind(var data : Tdata; var res : longint) : boolean;
begin
    if (data[1].num = data[2].num) and (data[2].num = data[3].num)
      then begin res := data[1].num; exit(true); end;
    if (data[2].num = data[3].num) and (data[3].num = data[4].num)
      then begin res := data[2].num; exit(true); end;
    if (data[3].num = data[4].num) and (data[4].num = data[5].num)
      then begin res := data[3].num; exit(true); end;
    exit(false);
end;

function TwoPairs(var data : Tdata) : boolean;
var
    i , count ,
    j          : longint;
    tmp        : Tcard;
begin
    count := 0;
    for i := 2 to 5 do
      if data[i].num = data[i - 1].num then
        inc(count);
    if count <> 2 then exit(false);
    j := 1;
    while (j <> 1) and (data[j - 1].num = data[j].num) or (j <> 5) and (data[j].num = data[j + 1].num) do
      inc(j);
    tmp := data[j];
    for i := j + 1 to 5 do
      data[i - 1] := data[i];
    data[5] := tmp;
    exit(true);
end;

function Pair(var data : Tdata) : boolean;
var
    i , j      : longint;
    tmp        : Tcard;
begin
    j := 2;
    while (j <= 5) and (data[j - 1].num <> data[j].num) do inc(j);
    if j = 6 then exit(false);
    tmp := data[j - 1];
    for i := j - 2 downto 1 do data[i + 1] := data[i];
    data[1] := tmp;
    tmp := data[j];
    for i := j - 1 downto 2 do data[i + 1] := data[i];
    data[2] := tmp;
    exit(true);
end;

procedure Value(data : Tdata; var res : longint);
var
    i , j      : longint;
    tmp        : Tcard;
begin
    for i := 1 to 5 do
      for j := i + 1 to 5 do
        if data[i].num < data[j].num then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;
    if Flush(data) and Straight(data)
      then begin
               res := 9 * Base + rank(data);
               exit;
           end;
    if FourofKind(data , res)
      then begin
               res := res + 8 * Base;
               exit;
           end;
    if FullHouse(data , res)
      then begin
               res := res + 7 * Base;
               exit;
           end;
    if Flush(data)
      then begin
               res := 6 * Base + rank(data);
               exit;
           end;
    if Straight(data)
      then begin
               res := 5 * Base + rank(data);
               exit;
           end;
    if ThreeofKind(data , res)
      then begin
               res := res + 4 * Base;
               exit;
           end;
    if TwoPairs(data)
      then begin
               res := 3 * Base + rank(data);
               exit;
           end;
    if Pair(data)
      then begin
               res := 2 * Base + rank(data);
               exit;
           end;
    res := Base + rank(data);
end;

procedure work;
begin
    Value(A , VA);
    Value(B , VB);
end;

procedure out;
begin
    if VA = VB
      then writeln('Tie.')
      else if VA > VB
             then writeln('Black wins.')
             else writeln('White wins.');
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
    Close(INPUT);
End.