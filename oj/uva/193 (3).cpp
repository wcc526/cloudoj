/**
 * UVa 193 Graph Coloring
 * Author: chchwy (a) gmail.com
 * Last Modified: 2010.04.13
 */
#include<iostream>
enum{MAX_NODE=100};

void setAdjacentMatrix(bool adj[MAX_NODE][], int numEdge){

    memset(adj, 0, MAX_NODE*MAX_NODE);
    for(int i=0;i<numEdge;++i){
        int x,y;
        scanf("%d%d",&x,&y);
        adj[x-1][y-1] = true;
    }
}

int main(){

    int numCase;
    scanf("%d",&numCase);

    while(numCase--){
        int numNode, numEdge;
        scanf("%d%d", &numNode, &numEdge);

        bool adj[MAX_NODE][MAX_NODE];
        setAdjcentMatrix(adj, numEdge);



    }
    return 0;
}
