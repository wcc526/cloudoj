Const
    InFile     = 'p10744.in';
    Limit      = 20000;

Type
    Tpoint     = record
                     x , y , C               : real;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    TLine      = record
                     A , B , C               : real;
                 end;

Var
    data       : Tdata;
    answer     : extended;
    N , Q      : longint;

procedure init;
var
    i          : longint;
begin
    readln(N , Q);
    for i := 1 to N do
      read(data[i].x , data[i].y);
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p1.x * p2.y - p1.y * p2.x;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].C > key.C) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].C < key.C) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure find_mid;
var
    midp , start , stop ,
    mid        : longint;
begin
    midp := (N + 1) div 2;
    start := 1; stop := N;
    while start <= stop do
      begin
          qk_pass(start , stop , mid);
          if mid = midp
            then exit
            else if mid < midp
                   then start := mid + 1
                   else stop := mid - 1;
      end;
end;

procedure workout;
var
    A , B      : Tpoint;
    L          : TLine;
    i , j      : longint;
begin
    for i := 1 to Q do
      begin
          readln(A.x , A.y , B.x , B.y);
          Get_Line(A , B , L);
          for j := 1 to N do
            with data[j] do
              C := -(L.A * x + L.B * y);
          find_mid;
          L.C := data[(N + 1) div 2].C;
          answer := 0;
          for j := 1 to N do
            answer := answer + abs(L.A * data[j].x + L.B * data[j].y + L.C) / sqrt(sqr(L.A) + sqr(L.B));
          writeln('Case ' , i , ': ' , answer : 0 : 0);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    init;
    workout;
End.