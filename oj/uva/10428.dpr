{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10428.in';
    OutFile    = 'p10428.out';
    Limit      = 5;
    LimitTimes = 50;
    minimum    = 1e-10;
    min2       = 1e-4;

Type
    Tdata      = array[0..Limit] of extended;
    Tans       = array[1..Limit] of extended;

Var
    data       : Tdata;
    ans        : Tans;
    cases , 
    N , tot    : longint;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    inc(cases);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := N downto 0 do
                 read(data[i]);
           end;
end;

procedure ins(p : extended);
var
    i          : longint;
begin
    for i := 1 to tot do
      if abs(p - ans[i]) <= min2 then
        exit;
    i := tot + 1;
    while (i > 1) and (ans[i - 1] > p) do
      begin ans[i] := ans[i - 1]; dec(i); end;
    ans[i] := p; inc(tot);
end;

procedure work;
var
    derivative , 
    p , f      : extended;
    power      : Tdata;
    i , j      : longint;
begin
    tot := 0;
    while tot < N do
      begin
          p := random(61) - 31;
          p := p + random(1000) / 1000;
          for j := 1 to LimitTimes do
            begin
                power[0] := 1;
                for i := 1 to N do power[i] := power[i - 1] * p;
                derivative := 0;
                for i := N - 1 downto 0 do
                  derivative := derivative + power[i] * (i + 1) * data[i + 1];
                if abs(derivative) <= minimum
                  then break
                  else begin
                           f := 0;
                           for i := 0 to N do f := f + power[i] * data[i];
                           p := p - f / derivative;
                           if abs(p) > 1000 then break;
                       end;
            end;
          power[0] := 1;
          for i := 1 to N do power[i] := power[i - 1] * p;
          f := 0;
          for i := 0 to N do f := f + power[i] * data[i];
          if (abs(f) <= minimum) and (abs(p) <= 25) then
            ins(p);
      end;
end;

procedure out;
var
    i          : longint;
begin
    write('Equation ' , cases , ':');
    for i := 1 to N do write(' ' , ans[i] : 0 : 4);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
