{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10582.in';
    OutFile    = 'p10582.out';
    Limit      = 64;
    dirx       : array[1..4] of integer = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of integer = (0 , -1 , 0 , 1);

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    answer ,
    cases , 
    N , M      : longint;

procedure init;
var
    i , j      : longint;
    ch1 , ch2  : char;
begin
    dec(cases);
    readln(N , M);
    for i := 1 to N do
      begin
          readln;
          readln;
          for j := 1 to M do
            begin
                read(ch1); read(ch1);
                read(ch1); read(ch2);
                if ch1 <> '*'
                  then data[i , j] := 0
                  else if ch2 <> '*'
                         then data[i , j] := 2
                         else data[i , j] := 1;
            end;
          readln;
          readln;
      end;
    readln;
end;

procedure dfs(x , y , dir : longint);
var
    nx , ny ,
    ndir       : longint;
begin
    if (x = N) and (y = M)
      then begin
               inc(answer);
               exit;
           end;
    visited[x , y] := true;
      if data[x , y] = 1 then
        begin
            ndir := dir;
            nx := x + dirx[ndir]; ny := y + diry[ndir];
            if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) and (data[nx , ny] <> 0) and not visited[nx , ny]
              then dfs(nx , ny , ndir);
        end;
      if data[x , y] = 2 then
        begin
            ndir := dir mod 4 + 1;
            nx := x + dirx[ndir]; ny := y + diry[ndir];
            if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) and (data[nx , ny] <> 0) and not visited[nx , ny]
              then dfs(nx , ny , ndir);
            ndir := (dir + 2) mod 4 + 1;
            nx := x + dirx[ndir]; ny := y + diry[ndir];
            if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) and (data[nx , ny] <> 0) and not visited[nx , ny]
              then dfs(nx , ny , ndir);
        end;
    visited[x , y] := false;
end;

procedure work;
begin
    fillchar(visited , sizeof(visited) , 0);
    answer := 0;
    dfs(1 , 1 , 3);
    dfs(1 , 1 , 4);
end;

procedure out;
begin
    writeln('Number of solutions: ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
