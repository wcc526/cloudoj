Const
    InFile     = 'p10462.in';
    OutFile    = 'p10462.out';
    Limit      = 100;
    LimitM     = 200;

Type
    Tedge      = record
                     p1 , p2 , cost          : longint;
                     deleted                 : boolean;
                 end;
    Tdata      = array[1..LimitM] of Tedge;
    Tfather    = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data , tree: Tdata;
    father , color ,
    cost       : Tfather;
    visited    : Tvisited;
    N , M , T ,
    tot , cases: longint;

procedure init;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    readln(N , M);
    for i := 1 to M do
      with data[i] do
        read(p1 , p2 , cost);
end;

function Get_Color(p : longint) : longint;
begin
    if color[p] = p
      then exit(p)
      else begin
               color[p] := Get_Color(color[p]);
               exit(color[p]);
           end;
end;

procedure dfs(p : longint);
var
    i , tmp    : longint;
begin
    visited[p] := true;
    for i := 1 to T do
      if (tree[i].p1 = p) or (tree[i].p2 = p) then
        begin
            if tree[i].p2 = p then
              begin tmp := tree[i].p1; tree[i].p1 := tree[i].p2; tree[i].p2 := tmp; end;
            if not visited[tree[i].p2] then
              begin
                  father[tree[i].p2] := p;
                  cost[tree[i].p2] := tree[i].cost;
                  dfs(tree[i].p2);
              end;
        end;
end;

function Get_Min(p1 , p2 : longint) : longint;
var
    tmp1 ,
    tmp2       : array[1..Limit] of longint;
    res , i ,
    n1 , n2    : longint;
begin
    n1 := 0;
    while p1 <> 0 do
      begin
          inc(n1); tmp1[n1] := p1; p1 := father[p1];
      end;
    n2 := 0;
    while p2 <> 0 do
      begin
          inc(n2); tmp2[n2] := p2; p2 := father[p2];
      end;
    while (n1 > 0) and (n2 > 0) and (tmp1[n1] = tmp2[n2]) do
      begin dec(n1); dec(n2); end;
    res := -maxlongint;
    for i := 1 to n1 do
      if cost[tmp1[i]] > res then res := cost[tmp1[i]];
    for i := 1 to n2 do
      if cost[tmp2[i]] > res then res := cost[tmp2[i]];
    exit(res);
end;

procedure workout;
var
    i , j , ans ,
    sum , t2   : longint;
    tmp        : Tedge;
begin
    for i := 1 to M do
      for j := i + 1 to M do
        if data[i].cost > data[j].cost then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;

    T := 0; for i := 1 to N do color[i] := i;
    sum := 0;
    for j := 1 to M do
      if Get_Color(data[j].p1) <> Get_Color(data[j].p2) then
        begin
            color[color[data[j].p1]] := color[data[j].p2];
            inc(T); tree[T] := data[j];
            data[j].deleted := true;
            inc(sum , data[j].cost);
        end;

    if T <> N - 1 then begin writeln('No way'); exit; end;
    ans := maxlongint;
    fillchar(visited , sizeof(visited) , 0);
    dfs(1);

    for i := 1 to M do
      if not data[i].deleted then
        begin
            t2 := Get_Min(data[i].p1 , data[i].p2);
            if ans > sum - t2 + data[i].cost then
              ans := sum - t2 + data[i].cost;
        end;
    if ans = maxlongint
      then writeln('No second way')
      else writeln(ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(tot);
      for cases := 1 to tot do
        begin
            init;
            write('Case #' , cases , ' : ');
            workout;
        end;
//    Close(INPUT);
End.