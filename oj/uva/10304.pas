{$I-}
Const
    Limit      = 300;
    Maxlongint = 1000000000;

Type
    Tdata      = array[1..Limit] of longint;
    Topt       = array[0..Limit + 1 , 0..Limit + 1] of longint;
    Tsum       = array[0..Limit] of longint;

Var
    data , queue
               : Tdata;
    C ,
    opt        : Topt;
    subsum     : Tsum;
    N , answer : longint;

procedure init;
var
    i          : longint;
begin
    read(N);
    for i := 1 to N do read(data[i]);
    readln;
end;

procedure work;
var
    i , j , k  : longint;
begin
    subsum[0] := 0;
    for i := 1 to N do subsum[i] := subsum[i - 1] + data[i];
    fillchar(opt , sizeof(opt) , 0);
    for i := 1 to N do C[i , i] := i;
    for i := N - 1 downto 1 do
      for j := i + 1 to N do
        begin
            opt[i , j] := maxlongint;
            for k := C[i , j - 1] to C[i + 1 , j] do
              if opt[i , k - 1] + opt[k + 1 , j] + subsum[j] - subsum[i - 1] - data[k] < opt[i , j]
                then begin
                         opt[i , j] := opt[i , k - 1] + opt[k + 1 , j] + subsum[j] - subsum[i - 1] - data[k];
                         C[i , j] := k;
                     end;
        end;
end;

Begin
    while not eof do
      begin
          init;
          if N = 0 then break;
          work;
          writeln(opt[1 , N]);
      end;
End.