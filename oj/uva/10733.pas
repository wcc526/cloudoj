Var
    N          : extended;

Begin
    readln(N);
    while N <> 0 do
      begin
          writeln((N * N * N * N * N * N + 12 * N * N * N + 3 * N * N * N * N + 8 * N * N) / 24 : 0 : 0);
          readln(N);
      end;
End.