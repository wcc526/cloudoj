#include <algorithm>
#include <cstdio>
#include <functional>
#include <set>
#include <vector>

using namespace std;

class e {
public:
  int x, h, t;
  e(int x, int h, int t) : x(x), h(h), t(t) { }
  bool operator<(const e &r) const {
    return x < r.x;
  }
};

int main()
{
  vector<e> q;
  multiset<int, greater<int> > p;
  int h = 0, x = -11000, l, r, y;

  while (scanf("%d %d %d", &l, &y, &r) == 3)
    q.push_back(e(l, y, 1)), q.push_back(e(r, y, 0));
  sort(q.begin(), q.end());

  for (vector<e>::iterator i = q.begin(); i != q.end();) {
    int c = i->x;
    do {
      if (i->t)
        p.insert(i->h);
      else
        p.erase(p.lower_bound(i->h));
    } while ((++i)->x == c);
    int m = p.empty() ? 0 : *p.begin();
    if (h ^ m)
      printf("%d %d%c", x = c, h = m, i == q.end() ? '\n' : ' ');
  }

  return 0;
}
