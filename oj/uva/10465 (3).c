#include <stdio.h>
#include <string.h>

int main()
{
  short i, j, m, n, t, v[10001];

  while (scanf("%hd %hd %hd", &m, &n, &t) == 3) {
    memset(v, 0, sizeof(v));
    v[n] = 1;
    for (j = 0, i = m; i <= t; v[i] = j += 1, i += m);
    for (i = 0; ++i + n <= t;)
      if (v[i] && v[i + n] < (j = v[i] + 1))
        v[i + n] = j;
    if (v[t]) {
      printf("%hd\n", v[t]);
    } else {
      for (i = t; --i && v[i] == 0;);
      printf("%hd %hd\n", v[i], t - i);
    }
  }

  return 0;
}
