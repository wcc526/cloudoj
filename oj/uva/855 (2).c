#include <stdio.h>

#define C(a,b) (a > b)
#define S(a,b) n = a, a = b, b = n

int kth_element(short *z, int n, int k)
{
  int a, b, u = 0, v = n - 1, m;

  for (;;) {
    m = u + ((v - u) >> 1);
    if (C(z[u], z[m])) {
      if (C(z[m], z[v]))
        S(z[m], z[v]);
      else if (C(z[v], z[u]))
        S(z[u], z[v]);
    } else {
      if (C(z[u], z[v]))
        S(z[u], z[v]);
      else if (C(z[v], z[m]))
        S(z[m], z[v]);
    }

    m = z[v], a = u, b = v - 1;
    do {
      for (; C(z[a], m); ++a);
      for (; C(m, z[b]); --b);
      if (a < b) {
        S(z[a], z[b]);
        ++a, --b;
      } else if (a == b) {
        ++a, --b;
        break;
      }
    } while (a <= b);

    z[v] = z[a], z[a] = m;

    if (k < a)
      v = a - 1;
    else if (k > a)
      u = a + 1;
    else
      return z[a];
  }
}

int main()
{
  int f, i, t;
  short a[50000], s[50000];

  scanf("%d", &t);
  while (t--) {
    scanf("%*d %*d %d", &f);
    for (i = f; i--;)
      scanf("%hd %hd", &s[i], &a[i]);
    printf("(Street: %hd, Avenue: %hd)\n",
           kth_element(s, f, f >> 1), kth_element(a, f, f >> 1));
  }

  return 0;
}
