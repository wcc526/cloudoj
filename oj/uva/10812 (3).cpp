#include <iostream>
#include <cstdio>

using namespace std;

int main(void)

{


    int tests;
    cin >> tests;

    int count = 1;

    while(count <= tests)
    {
        int sum, difference;
        cin >> sum;
        cin >> difference;

        if(sum < difference)
        {
            cout << "impossible" << endl;
            break;
        }

        int num1 = (sum + difference) / 2;

        int num2 = sum - num1;

        count++;

        if(num1 - num2 == difference)
        {
            if(num1 > num2)


            {
                cout << num1 << " " << num2 << endl;
            }

            else
                cout << num2 << " " << num1 << endl;

        }

        else
        {
            cout << "impossible" << endl;
        }

    }

    return 0;

}
