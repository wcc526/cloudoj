#include <iostream>
#include <cstring>
#include <queue>
#include <vector>
#include <cstdio>
#define INF 0x3f3f3f3f3f3f3f3fLL
#define maxn 60
using namespace std;
typedef pair <long long int,int> pii;
long long int d[maxn],p;
int first[maxn];
vector<int> st,v,next,w,path;
int n,m,e,start,end;

void init()
{
	e = 0;
	st.clear();
	v.clear();
	next.clear();
	w.clear();
	path.clear();
	memset(first,-1,sizeof(first));
}
void add_edge(int a,int b)
{
	//st[e] = a;
	st.push_back(a);
	//v[e] = b;
	v.push_back(b);
	//next[e] = first[a];
	next.push_back(first[a]);
	w.push_back(0);
	first[a] = e;
	e++;
}
void Read_Graph()
{
	int i,a,b;
	char ch1,ch2;
	e = 0;
	getchar();
	for(i = 1;i <= m;i++)
	{
		scanf("%c %c",&ch1,&ch2);
		getchar();
		a = ch1 - 'A';
		b = ch2 - 'A';
		add_edge(a,b);
		add_edge(b,a);
	}
	scanf("%lld %c %c",&p,&ch1,&ch2);
	start = ch1 - 'A';
	end = ch2 - 'A';
}

void dijkstra()
{
	priority_queue < pii,vector<pii>,greater<pii> > q;

	int i;
	for(i = 0;i < maxn;i++)
	{
		d[i] = INF;
	}
	d[end] = p;
	q.push(make_pair(p,end));
	while(!q.empty())
	{
		while(!q.empty() && q.top().first > d[q.top().second])   q.pop();
		int u = q.top().second;
		q.pop();
		if(u == start)  break;
		for(i = first[u];i != -1;i = next[i])
		{
			if(u < 26)
			{
				int k = d[u]/19;
				if( d[u]%19 != 0 ) ++k;
				if( d[u] + k < d[v[i] ] ){
					d[v[i] ] = d[u] + k;
					w[i] = w[i ^ 1] = d[v[i]] - d[u];
					q.push(make_pair(d[v[i]],v[i]));
				}
			}
			else
			{
				if(d[v[i]] > d[u] + 1)
				{
					d[v[i]] = d[u] + 1;
					w[i] = w[i ^ 1] = 1;
					q.push(make_pair(d[v[i]],v[i]));
				}
			}
		}
	}
}

void solve()
{
	int i,temp,now = start;
	path.push_back(now);
	while(now != end)
	{
		temp = 1 << 6;
		for(i = 0;i < e;i++)
		{
			if(d[now] == d[v[i]] + w[i] && st[i] == now && w[i] != 0 && v[i] < temp)
			{
				temp = v[i];
			}
		}
		now = temp;
		path.push_back(now);
	}
	printf("%lld\n",d[start]);
	printf("%c",path[0] + 'A');
	for(i = 1;i < path.size();i++)
		printf("-%c",path[i] + 'A');
	printf("\n");
}

int main()
{
	int ncase = 0;
	while(scanf("%d",&m))
	{
		if(m == -1) break;
		init();
		Read_Graph();
		dijkstra();
		printf("Case %d:\n",++ncase);
		solve();
	}
	return 0;
}