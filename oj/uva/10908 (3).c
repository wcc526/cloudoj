#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  int c, i, j, m, n, r, q, t;
  char v[100][101], s;

  scanf("%d", &t);

  while (t--) {
    scanf("%d %d %d", &m, &n, &q);
    printf("%d %d %d\n", m, n, q);
    for (i = 0; i < m; ++i)
      scanf("%100s", v[i]);
    while (q--) {
      scanf("%d %d", &r, &c);
      s = v[r][c];
      for (i = 1; c + i < n && r + i < m && c - i >= 0 && r - i >= 0; ++i) {
        for (j = c - i; j <= c + i; ++j)
          if (v[r - i][j] != s || v[r + i][j] != s)
            goto d;
        for (j = r - i + 1; j <= r + i - 1; ++j)
          if (v[j][c - i] != s || v[j][c + i] != s)
            goto d;
      }
    d:
      printf("%d\n", ((i - 1) << 1) + 1);
    }
  }

  return 0;
}
