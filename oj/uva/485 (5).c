#include <stdio.h>
#include <string.h>

typedef struct {
  unsigned int v[7];
} big;

void big_add(big *s1, big *s2, big *d)
{
  int i;
  for (i = 7; i--;)
    if ((d->v[i] += s1->v[i] + s2->v[i]) > 999999999)
      d->v[i] -= 1000000000, ++d->v[i - 1];
}

void big_print(big *b)
{
  int i;
  for (i = 0; b->v[i] == 0; ++i);
  printf("%u", b->v[i]);
  while (++i < 7)
    printf("%09u", b->v[i]);
}

int main()
{
  big a[205], b[205], *t, *u = a, *v = b;
  int i, n;

  memset(a, 0, sizeof(a));
  a[0].v[6] = 1;
  puts("1");

  for (n = 1; n++ < 205;) {
    memset(v, 0, sizeof(a));
    v[0].v[6] = 1;
    for (i = 1; i < n; ++i)
      big_add(&u[i - 1], &u[i], &v[i]);
    putchar('1');
    for (i = 1; i < n; ++i)
      putchar(' '), big_print(&v[i]);
    putchar('\n');
    t = u, u = v, v = t;
  }

  return 0;
}
