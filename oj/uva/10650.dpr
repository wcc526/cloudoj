{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10650.in';
    OutFile    = 'p10650.out';
    Limit      = 32000;

Type
    Tdata      = array[1..Limit] of boolean;
    Tprime     = array[1..Limit] of longint;
    Tanswer    = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..5] of longint;
                   end;

Var
    data       : Tdata;
    prime      : Tprime;
    answer     : Tanswer;
    sum , 
    tot , A , B: longint;

function init : boolean;
begin
    readln(A , B);
    init := (A + B <> 0);
end;

procedure pre_process;
var
    i , j      : longint;
begin
    tot := 0;
    fillchar(data , sizeof(data) , 1);
    fillchar(prime , sizeof(prime) , 0);
    data[1] := false;
    for i := 2 to Limit do
      if data[i] then
        begin
            inc(tot);
            prime[tot] := i;
            for j := 2 to Limit div i do
              data[j * i] := false;
        end;

    i := 1; sum := 0;
    while i <= tot do
      begin
          if i >= tot - 1 then break;
          if prime[i + 1] * 2 = prime[i] + prime[i + 2] then
            begin
                inc(sum); answer[sum].tot := 3;
                answer[sum].data[1] := prime[i]; answer[sum].data[2] := prime[i + 1];
                answer[sum].data[3] := prime[i + 2];
                i := i + 3;
                while (i <= tot) and (prime[i - 1] * 2 = prime[i] + prime[i - 2]) do
                  begin
                      inc(answer[sum].tot);
                      answer[sum].data[answer[sum].tot] := prime[i];
                      inc(i);
                  end;
                dec(i);
            end
          else
            inc(i);
      end;
end;

procedure out;
var
    i , j      : longint;
begin
    if A > B then begin i := A; A := B; B := i; end;
    for i := 1 to sum do
      if answer[i].data[1] >= A then
        if answer[i].data[answer[i].tot] <= B then
          begin
              write(answer[i].data[1]);
              for j := 2 to answer[i].tot do
                write(' ' , answer[i].data[j]);
              writeln;
          end;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      while init do out;
    Close(OUTPUT);
    Close(INPUT);
End.
