{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10435.in';
    OutFile    = 'p10435.out';

Type
    Tdata      = class
                     key      : longint;
                     next     : Tdata;
                 end;
    Tarray     = array[1..10] of longint;

Var
    data       : Tdata;
    p          : ^Tarray;
    a          : Tarray;

Begin
    GetMem(p , 12);
    @a := p;
    p^[1] := 1;
    p^[2] := 2;
    p^[3] := 3;
    p^[4] := 4;
    p^[5] := 5;
End.
