Const
    InFile     = 'p10202.in';
    Limit      = 10;

Type
    Tdata      = object
                     data     : array[1..Limit * Limit] of longint;
                     used     : array[1..Limit * Limit] of boolean;
                     procedure sort;
                     function get_min : longint;
                     function seek(num : longint) : boolean;
                 end;
    Tanswer    = array[1..Limit] of longint;

Var
    data       : Tdata;
    answer     : Tanswer;
    noanswer   : boolean;
    N          : longint;

procedure Tdata.sort;
var
    i , j , tmp: longint;
begin
    for i := 1 to N * (N - 1) div 2 do
      for j := i + 1 to N * (N - 1) div 2 do
        if data[i] > data[j] then
          begin
              tmp := data[i];
              data[i] := data[j];
              data[j] := tmp;
          end;
end;

function Tdata.Get_Min : longint;
var
    i          : longint;
begin
    for i := 1 to N * (N - 1) div 2 do
      if not used[i] then
        begin
            used[i] := true;
            exit(data[i]);
        end;
end;

function Tdata.seek(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 1 to N * (N - 1) div 2 do
      if not used[i] and (data[i] = num)
        then begin
                 used[i] := true;
                 exit(true);
             end;
    exit(false);
end;

procedure init;
var
    i          : longint;
begin
    read(N);
    for i := 1 to N * (N - 1) div 2 do
      read(data.data[i]);
    readln;
end;

function check : boolean;
var
    i , j      : longint;
begin
    for i := 4 to N do
      begin
          answer[i] := data.Get_Min - answer[1];
          for j := 2 to i - 1 do
            if not data.seek(answer[j] + answer[i]) then
              exit(false);
      end;
    exit(true);
end;

procedure work;
var
    p1 , p2 ,
    p3 , i     : longint;
begin
    data.sort;
    noanswer := false;
    for i := 3 to N do
      begin
          fillchar(data.used , sizeof(data.used) , 0);
          p1 := data.Get_Min;
          p2 := data.Get_Min;
          p3 := data.data[i]; data.used[i] := true;
          if not odd(p1 + p2 + p3)
            then begin
                     answer[1] := (p1 + p2 - p3) div 2;
                     answer[2] := (p1 + p3 - p2) div 2;
                     answer[3] := (p2 + p3 - p1) div 2;
                     if check then
                       exit;
                 end;
      end;
    noanswer := true;
end;

procedure out;
var
    i          : longint;
begin
    if noanswer
      then write('Impossible')
      else for i := 1 to N do write(answer[i] , ' ');
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.