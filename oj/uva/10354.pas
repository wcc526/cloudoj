Const
    InFile     = 'p10354.in';
    OutFile    = 'p10354.out';
    Maximum    = 1000000;
    Limit      = 100;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tblocked   = array[1..Limit] of boolean;

Var
    shortest ,
    map        : Tmap;
    blocked    : Tblocked;
    N , R ,
    BH , OFF ,
    YH , M     : longint;

procedure init;
var
    i , j ,
    p1, p2 , cost
               : longint;
begin
    for i := 1 to Limit do
      for j := 1 to Limit do
        map[i , j] := maximum;
    for i := 1 to Limit do map[i , i] := 0;
    readln(N , R , BH , OFF , YH , M);
    for i := 1 to R do
      begin
          readln(p1 , p2 , cost);
          map[p1 , p2] := cost; map[p2 , p1] := cost;
      end;
end;

procedure workout;
var
    i , j , k  : longint;
begin
    shortest := map;
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          if shortest[i , j] > shortest[i , k] + shortest[k , j] then
            shortest[i , j] := shortest[i , k] + shortest[k , j];

    fillchar(blocked , sizeof(blocked) , 0);
    for i := 1 to N do
      if shortest[BH , i] + shortest[OFF , i] = shortest[BH , OFF] then
        blocked[i] := true;

    if blocked[YH] or blocked[M] then
      begin writeln('MISSION IMPOSSIBLE.'); exit; end;

    for k := 1 to N do if not blocked[k] then
      for i := 1 to N do if not blocked[i] then
        for j := 1 to N do if not blocked[j] then
          if map[i , j] > map[i , k] + map[k , j] then
            map[i , j] := map[i , k] + map[k , j];
    if map[YH , M] >= maximum
      then writeln('MISSION IMPOSSIBLE.')
      else writeln(map[YH , M]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(INPUT);
End.