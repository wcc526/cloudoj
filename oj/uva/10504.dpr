{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10504.in';
    OutFile    = 'p10504.out';
    Limit      = 100;
    LimitQ     = 1000;

Type
    Tdata      = array[1..Limit , 1..Limit] of char;
    Tquery     = array[1..LimitQ] of
                   record
                       c      : char;
                       ans    : longint;
                   end;

Var
    data       : Tdata;
    query      : Tquery;
    N , Q      : longint;

function init : boolean;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(query , sizeof(query) , 0);
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               readln(Q);
               for i := 1 to N do
                 begin
                     for j := 1 to N do
                       read(data[i , j]);
                     readln;
                 end;
               for i := 1 to Q do
                 readln(query[i].c);
           end;
end;

function process(c : char) : longint;
var
    x1 , y1 ,
    x2 , y2 ,
    x3 , y3 ,
    x4 , y4 ,
    res        : longint;
begin
    res := 0;
    for x1 := 1 to N do
      for y1 := 1 to N do
        if data[x1 , y1] = c then
          for x2 := x1 + 1 to N do
            for y2 := y1 to N do
              if data[x2 , y2] = c then
                begin
                    if odd(x1 + x2 + y1 - y2) then continue;
                    if odd(y1 + y2 + x2 - x1) then continue;
                    x3 := (x1 + x2 + y1 - y2) div 2; y3 := (y1 + y2 + x2 - x1) div 2;
                    x4 := (x1 + x2 + y2 - y1) div 2; y4 := (y1 + y2 + x1 - x2) div 2;
                    if (x3 >= 1) and (x3 <= N) and (y3 >= 1) and (y3 <= N) then
                      if (x4 >= 1) and (x4 <= N) and (y4 >= 1) and (y4 <= N) then
                        if (data[x3 , y3] = c) and (data[x4 , y4] = c) then
                          inc(res);
                end;
    process := res;
end;

procedure work;
var
    i          : longint;
begin
    for i := 1 to Q do
      query[i].ans := process(query[i].c);
end;

procedure out;
var
    i          : longint;
begin
    for i := 1 to Q do
      writeln(query[i].c , ' ' , query[i].ans);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
