{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10533.in';
    OutFile    = 'p10533.out';
    Limit      = 1000000;

Type
    Tprime     = array[1..Limit] of boolean;
    Tsum       = array[0..Limit] of longint;

Var
    prime      : Tprime;
    digitsum ,
    sum        : Tsum;
    N , p1 , p2: longint;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , true);
    fillchar(digitsum , sizeof(digitsum) , 0);
    fillchar(sum , sizeof(sum) , 0);
    prime[1] := false;
    for i := 1 to Limit do
      begin
          digitsum[i] := digitsum[i div 10] + i mod 10;
          sum[i] := sum[i - 1];
          if prime[i] and prime[digitsum[i]] then inc(sum[i]);
          if prime[i] then
            for j := 2 to Limit div i do
              prime[i * j] := false;
      end;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N > 0 do
        begin
            dec(N);
            read(p1 , p2);
            writeln(sum[p2] - sum[p1 - 1]);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
