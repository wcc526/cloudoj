Const
    InFile     = 'p10362.in';
    Limit      = 20;
    LimitLen   = 50;
    Base       = 1440;

Type
    Tstr       = string[LimitLen];
    Tnames     = object
                     tot      : longint;
                     data     : array[1..Limit * Limit] of Tstr;
                     function find(s : Tstr) : longint;
                 end;
    Tdata      = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..Limit] of
                                  record
                                      runtime , arrivetime ,
                                      station               : longint;
                                  end;
                   end;
    Topt       = array[0..Base - 1 , 1..Limit * Limit] of longint;
    Tindex     = array[0..Base - 1 , 1..Limit * Limit] of
                   record
                       tot    : longint;
                       data   : array[1..Limit] of smallint;
                   end;
    Tnext      = array[0..Base - 1] of longint;
    Tstack     = array[1..Base] of
                   record
                       t1 , t2               : longint;
                   end;

Var
    names      : Tnames;
    data       : Tdata;
    opt        : Topt;
    index      : Tindex;
    next       : Tnext;
    stack      : Tstack;
    tot ,
    N , cases ,
    start , stop
               : longint;

function Tnames.find(s : Tstr) : longint;
var
    i          : longint;
begin
    for i := 1 to tot do
      if s = data[i]
        then exit(i);
    inc(tot); data[tot] := s;
    exit(tot);
end;

function read_time : longint;
var
    res        : longint;
    ch         : char;
begin
    ch := ' ';
    repeat if eoln then readln else read(ch); until ch <> ' ';
    res := 0;
    while ch <> ':' do
      begin
          res := res * 10 + ord(ch) - 48;
          read(ch);
      end;
    read(ch); res := res * 60 + (ord(ch) - 48) * 10;
    read(ch); res := res + ord(ch) - 48;
    exit(res);
end;

function read_name : Tstr;
var
    res        : Tstr;
    ch         : char;
begin
    ch := ' ';
    repeat if eoln then readln else read(ch); until ch <> ' ';
    res := '';
    while ch <> ' ' do
      begin
          res := res + ch;
          if eoln then ch := ' ' else read(ch);
      end;
    exit(res);
end;

procedure init;
var
    i , j      : longint;
begin
    fillchar(names , sizeof(names) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , 1);
    fillchar(index , sizeof(index) , 0);
    dec(Cases);
    readln(N);
    for i := 0 to Base - 1 do
      Next[i] := (i + 1) mod Base;
    for i := 1 to N do
      with data[i] do
        begin
            read(tot);
            for j := 1 to tot do
              begin
                  if j = 1
                    then data[j].arrivetime := read_time
                    else begin
                             data[j].runtime := read_time;
                             data[j].arrivetime := (data[j - 1].arrivetime + data[j].runtime) mod Base;
                             inc(data[j].runtime , data[j - 1].runtime);
                         end;
                  data[j].station := names.find(read_name);
              end;
            readln;
        end;
    start := names.find(read_name);
    stop := names.find(read_name);
    readln;
end;

procedure work;
var
    i , j , k , p ,
    t , tmp , newsta ,
    last , cost: longint;
    changed    : boolean;
begin
    for i := 1 to N do
      for j := 1 to data[i].tot do
        with index[data[i].data[j].arrivetime , data[i].data[j].station] do
          if (tot = 0) or (data[tot] <> i) then
            begin
                inc(tot);
                data[tot] := i;
            end;
    for i := 0 to Base - 1 do
      opt[i , stop] := 0;

    repeat
      changed := false;
      for i := Base - 1 downto 0 do
        for j := 1 to names.tot do
          begin
              if opt[next[i] , j] + 1 < opt[i , j] then
                begin
                    opt[i , j] := opt[next[i] , j] + 1;
                    changed := true;
                end;
              last := -1;
              for k := 1 to index[i , j].tot do
                begin
                    t := index[i , j].data[k];
                    last := -1;
                    for p := 1 to data[t].tot do
                      if (data[t].data[p].station = j) and (data[t].data[p].arrivetime = i)
                        then last := data[t].data[p].runtime
                        else if last <> -1 then
                               begin
                                   tmp := data[t].data[p].arrivetime;
                                   newsta := data[t].data[p].station;
                                   cost := opt[tmp , newsta] + data[t].data[p].runtime - last;
                                   if cost < opt[i , j] then
                                     begin
                                         opt[i , j] := cost;
                                         changed := true;
                                     end;
                               end;
                end;
          end;
    until not changed;
end;

procedure print_time(time : longint);
begin
    write(time div 60 div 10);
    write(time div 60 mod 10);
    write(':');
    write(time mod 60 div 10);
    write(time mod 60 mod 10);
end;

procedure out;
var
    i , last   : longint;
begin
    last := opt[0 , start];
    tot := 0;
    for i := Base - 1 downto 0 do
      begin
          inc(last);
          if opt[i , start] < last
            then begin
                     last := opt[i , start];
                     inc(tot);
                     stack[tot].t1 := i; stack[tot].t2 := opt[i , start];
                 end;
      end;
    for i := tot downto 1 do
      with stack[i] do
        begin
            print_time(t1);
            write(' ');
            writeln(t2 div 60 , ':' , t2 mod 60 div 10 , t2 mod 60 mod 10);
        end;
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.