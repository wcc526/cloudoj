Const
    LimitSave  = 100000;

Type
    Tprimes    = array[1..LimitSave] of longint;
    Tdata      = array[1..LimitSave] of double;

Var
    primes     : Tprimes;
    data       : Tdata;
    tot , i ,
    answer , N ,
    count      : longint;

function check(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to trunc(sqrt(num)) do
      if num mod i = 0 then
        exit(false);
    exit(true);
end;

procedure Get_Primes;
var
    i , tot    : longint;
begin
    tot := 2; primes[1] := 0; primes[2] := 2;
    i := 3;
    while tot < LimitSave do
      begin
          if check(i) then begin inc(tot); primes[tot] := i; end;
          inc(i);
      end;
end;

function calc(start : longint; x : double) : longint;
var
    i , j , k  : longint;
begin
    data[start] := x; j := start;
    for i := start - 1 downto 1 do
      begin
          while primes[j] - primes[i] > N do dec(j);
          if j = LimitSave
            then data[i] := x
            else begin
                     data[i] := 1;
                     for k := i + 1 to j do data[i] := data[i] * data[k];
                     data[i] := 1 - data[i];
                 end;
      end;
    if round(data[1]) = 0
      then exit(-1)
      else for j := 1 to start do
             if round(data[j]) = 0
               then exit(primes[j]);
end;

procedure work;
var
    i , tmp    : longint;
begin
    answer := 0; count := 0;
    for i := 1 to 11 do
      begin
          tmp := calc(LimitSave - random(40) , 0);
          if tmp = answer
            then inc(count)
            else begin
                     dec(count);
                     if count < 0 then begin count := 0; answer := tmp; end;
                 end;
      end;
end;

procedure out;
begin
    if answer = -1
      then writeln('B')
      else writeln('A ' , answer);
end;

Begin
    Get_Primes;
    readln(tot);
    for i := 1 to tot do
      begin
          readln(N);
          work;
          out;
      end;
End.