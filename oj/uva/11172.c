#include <stdio.h>

int main(void)
{
  int n, x, y;

  scanf("%d", &n);

  while (n--) {
    scanf("%d %d", &x, &y);
    puts(x < y ? "<" : x > y ? ">" : "=");
  }

  return 0;
}
