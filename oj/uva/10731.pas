Const
    InFile     = 'p10731.in';
    OutFile    = 'p10731.out';
    Limit      = 26;

Type
    Tstr       = string[Limit];
    Tanswer    = record
                     tot      : longint;
                     data     : array[1..Limit] of Tstr;
                 end;
    Tmap       = array['A'..'Z' , 'A'..'Z'] of boolean;
    Tappeared  = array['A'..'Z'] of boolean;

Var
    answer     : Tanswer;
    map        : Tmap;
    order      : Tstr;
    visited , used ,
    appeared   : Tappeared;
    first      : boolean;

procedure read_ch(var ch : char);
begin
    repeat read(ch); until ch <> ' ';
end;

function init : boolean;
var
    data       : array[1..5] of char;
    ch         : char;
    N , i , j  : longint;
begin
    fillchar(answer , sizeof(answer) , 0);
    fillchar(map , sizeof(map) , 0);
    fillchar(appeared , sizeof(appeared) , 0);
    readln(N);
    if N = 0
      then exit(false)
      else begin
               for j := 1 to N do
                 begin
                     for i := 1 to 5 do read_ch(data[i]);
                     read_ch(ch);
                     readln;
                     for i := 1 to 5 do
                       if ch <> data[i] then
                         map[ch , data[i]] := true;
                     for i := 1 to 5 do appeared[data[i]] := true;
                 end;
               exit(true);
           end;
end;

procedure dfs_p(root : char; var order : Tstr);
var
    i          : char;
begin
    used[root] := true;
    for i := 'A' to 'Z' do
      if not used[i] and map[root , i] then
        dfs_p(i , order);
    order := order + root;
end;

procedure dfs_n(root : char; var order : Tstr);
var
    i          : char;
begin
    visited[root] := true;
    for i := 'A' to 'Z' do
      if not visited[i] and used[i] and map[i , root] then
        dfs_n(i , order);
    order := order + root;
end;

procedure sort(var s : Tstr);
var
    i , j      : longint;
    tmp        : char;
begin
    for i := 1 to length(s) do
      for j := i + 1 to length(s) do
        if s[i] > s[j] then
          begin tmp := s[i]; s[i] := s[j]; s[j] := tmp; end;
end;

procedure work;
var
    i          : char;
    tmp        : Tstr;
    j , k      : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(used , sizeof(used) , 0);
    for i := 'A' to 'Z' do
      if appeared[i] and not used[i] then
        begin
            order := '';
            dfs_p(i , order);
            for j := length(order) downto 1 do
              if not visited[order[j]] then
                begin
                    inc(answer.tot);
                    dfs_n(order[j] , answer.data[answer.tot]);
                end;
        end;
    for j := 1 to answer.tot do
      sort(answer.data[j]);
    for k := 1 to answer.tot do
      for j := k + 1 to answer.tot do
        if answer.data[k] > answer.data[j] then
          begin
              tmp := answer.data[k]; answer.data[k] := answer.data[j];
              answer.data[j] := tmp;
          end;
end;

procedure out;
var
    i , j      : longint;
begin
    for i := 1 to answer.tot do
      begin
          write(answer.data[i , 1]);
          for j := 2 to length(answer.data[i]) do
            write(' ' , answer.data[i , j]);
          writeln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    first := true;
    while init do
      begin
          if first then first := false else writeln;
          work;
          out;
      end;
End.