{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}

Const
    minimum    = 1e-10;
    
Var
    p , q , 
    a , b , c ,
    st , ed , mid ,
    tmp        : extended;
Begin
    while not eof do
      begin
          readln(a , b , c);
          if a < b then ed := a else ed := b;
          st := 0;
          while st < ed - minimum do
            begin
                mid := (st + ed) / 2;
                p := sqrt(a * a - mid * mid); q := sqrt(b * b - mid * mid);
                tmp := p * q / (p + q);
                if tmp > c
                  then st := mid
                  else ed := mid;
            end;
          writeln(mid : 0 : 3);
      end;
End.
