#include<iostream>
#include<cstdio>
using namespace std;
#include <vector>
#include <queue>
using namespace std;
#define arraysize 1001
#define inf 0x7fffffff   
struct ss {
        int u, v;
        double temp, dis;
};
ss edge[10100];
double per[1001][1001],dis[1001][1001];
int pre[1001],path[1001][1001];
int main()
{
    int n,m,s,t,i,j,k;
    while(cin>>n>>m>>s>>t)
    {
     for(i=1;i<=n;i++)
     for(j=1;j<=n;j++)
     {
      per[i][j]=dis[i][j]=inf;
      path[i][j]=j;
     }
     for(i=1;i<=m;i++)
     {
      int a,b;
     scanf("%d %d %lf %lf",&edge[i].u,&edge[i].v,&edge[i].temp,&edge[i].dis);
     a=edge[i].u;
     b=edge[i].v;
      if(edge[i].temp<per[a][b])
      per[b][a]=per[a][b]=edge[i].temp;
     }
     for(k=1;k<=n;k++)
       for(i=1;i<=n;i++)
        for(j=1;j<=n;j++)
        {
           per[i][j]=min(per[i][j],max(per[i][k],per[k][j]));
        }
        double lim=per[s][t];
        int u,v;
        for(i=1;i<=m;i++)
        {
          u=edge[i].u;
          v=edge[i].v;
          if(dis[u][v]>edge[i].dis&&edge[i].temp<=lim)
          dis[u][v]=dis[v][u]=edge[i].dis;
        }
        for(k=1;k<=n;k++)
         for(i=1;i<=n;i++)
          for(j=1;j<=n;j++)
        {
          double f=dis[i][k]+dis[k][j];
           if(f<dis[i][j])
             dis[i][j]=f,path[i][j]=path[i][k];
        }
        k=s;
        printf("%d",s);                
        while(k!=t)
        {
          printf(" %d",path[k][t]);
          k=path[k][t];
          if(k==t) printf("\n");
        }  
        printf ("%.1f %.1f\n",dis[s][t],per[s][t]);
     }
     return 0;
    }
          
