#include <stdio.h>

int main()
{
  int c = 0, i, n, t;
  double r;

  scanf("%d", &t);
  while (t--) {
    scanf("%d", &n);
    for (r = 0.0, i = n >> 1; i++ < n;)
      r += 1.0 / i;
    r = 1 - r;
    printf("Case #%d: %.8lf %.8lf\n", ++c, r, 1 / r);
  }

  return 0;
}
