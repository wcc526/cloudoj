{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10561.in';
    OutFile    = 'p10561.out';
    Limit      = 200;

Type
    TSG        = array[-6..Limit] of longint;
    Tdata      = array[1..Limit] of
                   record
                       len , st              : longint;
                   end;
    Tpath      = record
                     tot      : longint;
                     data     : array[1..Limit] of longint;
                 end;

Var
    SG         : TSG;
    data       : Tdata;
    path , p2  : Tpath;
    N , Cases  : longint;

procedure pre_process;
var
    visited    : array[0..Limit] of boolean;
    i , j      : longint;
begin
    fillchar(SG , sizeof(SG) , 0);
    for i := 1 to Limit do
      begin
          fillchar(visited , sizeof(visited) , 0);
          for j := 1 to i do
            visited[SG[j - 3] xor SG[i - j - 2]] := true;
          while visited[SG[i]] do inc(SG[i]);
      end;
end;

procedure init;
var
    ch         : char;
    i , last   : longint;
begin
    dec(cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(path , sizeof(path) , 0);
    fillchar(p2 , sizeof(p2) , 0);
    N := 1; i := 1; last := -100;
    while not eoln do
      begin
          read(ch);
          if ch = 'X'
            then begin
                     if data[N].len <> 0
                       then inc(N);
                     if last = i - 2 then
                       begin
                           inc(path.tot); path.data[path.tot] := i - 1;
                       end;
                     if last = i - 1 then
                       begin
                           if (i > 2) and ((p2.tot = 0) or (p2.data[p2.tot] <> i - 2))
                             then begin inc(p2.tot); p2.data[p2.tot] := i - 2; end;
                           if not eoln then begin inc(p2.tot); p2.data[p2.tot] := i + 1; end;
                       end;
                     last := i;
                 end
            else begin
                     inc(data[N].len);
                     if data[N].len = 1 then data[N].st := i;
                 end;
          inc(i);
      end;
    if data[1].st <> 1 then begin inc(data[1].st , 2); dec(data[1].len , 2); end;
    if data[N].st + data[N].len - 1 <> i - 1 then dec(data[N].len , 2);
    if N <> 1 then begin dec(data[1].len , 2); inc(data[N].st , 2); dec(data[N].len , 2); end;
    for i := 2 to N - 1 do begin inc(data[i].st , 2); dec(data[i].len , 4); end;
    readln;
end;

procedure workout;
var
    SGValue , i ,
    j          : longint;
    first      : boolean;
begin
    SGValue := 0;
    for i := 1 to N do
      SGValue := SGValue xor SG[data[i].len];

    if p2.tot <> 0 then
      begin
          writeln('WINNING');
          write(p2.data[1]);
          for i := 2 to p2.tot do
            write(' ' , p2.data[i]);
          writeln;
          exit;
      end;

    if path.tot <> 0 then
      begin
          writeln('WINNING');
          write(path.data[1]);
          for i := 2 to path.tot do
            write(' ' , path.data[i]);
          writeln;
          exit;
      end;
    if SGValue = 0
      then writeln('LOSING')
      else writeln('WINNING');
    first := true;
    for i := 1 to N do
      for j := 1 to data[i].len do
        if SG[j - 3] xor SG[data[i].len - j - 2] xor SG[data[i].len] = SGValue
          then begin
                   if not first then write(' ');
                   first := false;
                   write(j + data[i].st - 1);
               end;
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
