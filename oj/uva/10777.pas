Var
    i , T ,
    j , N      : longint;
    p1 , p2 ,
    aH , bH ,
    a , b      : extended;
Begin
    readln(T);
    for i := 1 to T do
      begin
          write('Case ' , i , ': ');
          readln(N);
          aH := 0; bH := 0; a := 0; b := 0;
          for j := 1 to N do
            begin
                readln(p1 , p2);
                if p1 < 0
                  then begin
                           bH := bH - p1 * p2; b := b + p2;
                       end
                  else begin
                           aH := aH + p1 * p2; a := a + p2;
                       end;
            end;
          if abs(aH) < 1e-6
            then writeln('God! Save me')
            else writeln((aH + bH) / (1 - b) : 0 : 2);
      end;
End.