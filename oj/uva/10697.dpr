{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10697.in';
    OutFile    = 'p10697.out';
    minimum    = 1e-8;

Var
    x1 , y1 , x2 ,
    y2 , x3 , y3 ,
    A1 , B1 , C1 ,
    A2 , B2 , C2 ,
    tmp , x , y
               : extended;
    s1 , s2    : string;
    cases      : longint;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while cases > 0 do
        begin
            dec(cases);
            read(x1 , y1 , x2 , y2 , x3 , y3);
            A1 := 2 * (x2 - x1); B1 := 2 * (y2 - y1); C1 := x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2;
            A2 := 2 * (x3 - x1); B2 := 2 * (y3 - y1); C2 := x1 * x1 - x3 * x3 + y1 * y1 - y3 * y3;
            tmp := A1 * B2 - A2 * B1;
            if abs(tmp) <= minimum
              then if (abs(A1 * C2 - A2 * C1) <= minimum) and (abs(B1 * C2 - C1 * B2) <= minimum)
                     then writeln('There is an infinity of possible locations.')
                     else writeln('There is no possible location.')
              else begin
                       x := (C2 * B1 - C1 * B2) / tmp;
                       y := (C1 * A2 - C2 * A1) / tmp;
                       if x > 0 then x := x + minimum else x := x - minimum;
                       if y > 0 then y := y + minimum else y := y - minimum;
                       str(x : 0 : 1 , s1);
                       str(y : 0 : 1 , s2);
                       if s1 = '-0.0' then s1 := '0.0';
                       if s2 = '-0.0' then s2 := '0.0';
                       writeln('The equidistant location is (' , s1 , ', ' , s2 , ').');
                   end;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
