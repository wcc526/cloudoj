#include <set>
#include <map>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <cctype>
#include <cstdio>
#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int f91(int num)
{
    if(num >= 101)
        return num-10;
    else
        return f91(f91(num + 11));
}

int main()
{
    int input;
    while(cin >> input)

    {
        if(input <= 0)
            break;

        cout << "f91(" << input << ") = " << f91(input) << endl;
    }

	return 0;
}

