#include <stdio.h>
#include <string.h>

int main()
{
  unsigned char u[28], v[28], c[100], m[28], *a = u, *b = v, *t;
  int g[26][26], i, j, k, n = 0;

  memset(c, 255, sizeof(c));
  memset(g, 0, sizeof(g));

  scanf("%s", a);
  while (scanf("%s", b) == 1 && *b ^ '#') {
    for (i = 0; a[i] && b[i]; ++i) {
      if (a[i] ^ b[i]) {
        if (c[a[i]] == 255)
          c[c[n] = a[i]] = n, ++n;
        if (c[b[i]] == 255)
          c[c[n] = b[i]] = n, ++n;
        g[c[a[i]]][c[b[i]]] = 1;
        break;
      }
    }
    t = a, a = b, b = t;
  }

  for (k = n; k--;)
    for (i = n; i--;)
      for (j = n; j--;)
        g[i][j] |= g[i][k] & g[k][j];

  for (i = n; i--;)
    for (m[i] = 0, j = n; j--;)
      if (g[j][i])
        ++m[i];

  for (i = 1; i < n; ++i, m[j + 1] = k, c[j + 1] = c[99])
    for (k = m[i], c[99] = c[i], j = i; j-- && m[j] > k;)
      m[j + 1] = m[j], c[j + 1] = c[j];

  for (i = 0; i < n; ++i)
    putchar(c[i]);
  putchar('\n');

  return 0;
}
