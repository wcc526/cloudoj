{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
     g         = 9.80665;
     
Var
     W , D , h ,
     SG , HH   : extended;
     signal ,
     i         : longint;
     ch , ch2  : char;

Begin
    while not eof do
      begin
          signal := 0;
          W := 0; D := 0; h := 0; SG := 0;
          for i := 1 to 3 do
            begin
                read(ch);
                case ch of
                  'W'         : readln(ch2 , W);
                  'D'         : readln(ch2 , D);
                  'h'         : begin readln(ch2 , h); signal := 1; end;
                  'S'         : begin readln(ch2 , ch2 , SG); signal := 2; end;
                end;
            end;
          W := W / 1000; h := h / 100; D := D / 1000;
          HH := 4 * W / pi / D / D / 1000;
          if signal = 1
            then writeln('SG=' , HH / (HH - h) : 0 : 2)
            else writeln('h=' , (SG - 1) / SG * HH * 100 : 0 : 2);
      end;
End.
