Const
    InFile     = 'p10224.in';
    Limit      = 12;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tcircle    = record
                     O        : Tpoint;
                     R        : double;
                 end;
    Tdata      = array[1..Limit] of Tcircle;
    Tpoints    = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..Limit * 4] of
                                  record
                                      p                     : Tpoint;
                                      angle , shortest      : double;
                                      t1 , t2               : longint;
                                  end;
                   end;
    Tedges     = array[1..Limit * Limit * 4] of
                   record
                       i , j , p , q         : longint;
                   end;

Var
    data       : Tdata;
    points     : Tpoints;
    edges      : Tedges;
    E ,
    N , cases  : longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    fillchar(data , sizeof(data) , 0);
    readln(N , data[1].O.x , data[1].O.y , data[2].O.x , data[2].O.y);
    inc(N , 2);
    for i := 3 to N do
      with data[i] do
        begin
            read(O.x , O.y , R);
            R := R / 2;
        end;
end;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

procedure Rotate(p1 , p2 : Tpoint; var p : Tpoint; sinA , cosA , k : double);
begin
    p2.x := (p2.x - p1.x) * k; p2.y := (p2.y - p1.y) * k;
    p.x := p2.x * cosA - p2.y * sinA + p1.x;
    p.y := p2.x * sinA + p2.y * cosA + p1.y;
end;

function cross(k : longint; p1 , p2 : Tpoint) : boolean;
var
    p , h , area ,
    a , b , c  : double;
begin
    a := dist(data[k].O , p1); b := dist(data[k].O , p2); c := dist(p1 , p2);
    p := (a + b + c) / 2;
    area := sqrt(abs(p * (p - a) * (p - b) * (p - c)));
    h := area * 2 / c;
    if h >= data[k].R then exit(false);
    if (a * a + c * c < b * b) or (b * b + c * c < a * a) then exit(false);
    exit(true);
end;

function atan(sinA , cosA : double) : double;
begin
    if sinA < 0
      then atan := pi * 2 - atan(-sinA , cosA)
      else if zero(cosA)
             then atan := pi / 2
             else if cosA < 0
                    then atan := pi - atan(sinA , -cosA)
                    else atan := arctan(sinA / cosA);
end;

function calc_angle(k : longint; p : Tpoint) : double;
begin
    if zero(data[k].R)
      then exit(0)
      else exit(atan((p.y - data[k].O.y) / data[k].R , (p.x - data[k].O.x) / data[k].R));
end;

procedure INSERT_Tangent(i , j : longint; sinA , cosA , sinB , cosB : double);
var
    d , k1 , k2: double;
    p1 , p2    : Tpoint;
    k          : longint;
begin
    d := dist(data[i].O , data[j].O);
    k1 := data[i].R / d; k2 := data[j].R / d;
    Rotate(data[i].O , data[j].O , p1 , sinA , cosA , k1);
    Rotate(data[j].O , data[i].O , p2 , sinB , cosB , k2);
    for k := 1 to N do
      if (i <> k) and (j <> k) then
        if cross(k , p1 , p2) then
          exit;
    inc(points[i].tot); inc(points[j].tot);
    points[i].data[points[i].tot].p := p1; points[j].data[points[j].tot].p := p2;
    points[i].data[points[i].tot].angle := calc_angle(i , p1);
    points[j].data[points[j].tot].angle := calc_angle(j , p2);
    points[i].data[points[i].tot].shortest := 1e10;
    points[j].data[points[j].tot].shortest := 1e10;
    inc(E);
    Edges[E].i := i; Edges[E].j := j;
    Edges[E].p := points[i].tot; Edges[E].q := points[j].tot;
    inc(E);
    Edges[E].i := j; Edges[E].j := i;
    Edges[E].p := points[j].tot; Edges[E].q := points[i].tot;
end;

procedure Get_Tangent_EX(i , j : longint);
var
    d , Len    : double;
begin
    d := dist(data[i].O , data[j].O);
    Len := sqrt(sqr(d) - sqr(data[i].R - data[j].R));
    INSERT_Tangent(i , j , Len / d , (data[i].R - data[j].R) / d , -Len / d , (data[j].R - data[i].R) / d);
    INSERT_Tangent(i , j , -Len / d , (data[i].R - data[j].R) / d , Len / d , (data[j].R - data[i].R) / d);
end;

procedure Get_Tangent_IN(i , j : longint);
var
    d , Len    : double;
begin
    d := dist(data[i].O , data[j].O);
    Len := sqrt(sqr(d) - sqr(data[i].R + data[j].R));
    INSERT_Tangent(i , j , Len / d , (data[i].R + data[j].R) / d , Len / d , (data[j].R + data[i].R) / d);
    INSERT_Tangent(i , j , -Len / d , (data[i].R + data[j].R) / d , -Len / d , (data[j].R + data[i].R) / d);
end;

function calc_arc(i , j , k : longint) : double;
var
    angle      : double;
begin
    angle := abs(points[i].data[j].angle - points[i].data[k].angle);
    if angle > pi then angle := 2 * pi - angle;
    calc_arc := data[i].R * angle;
end;

procedure work;
var
    i , j , k  : longint;
    tmp        : double;
    changed    : boolean;
begin
    E := 0;
    fillchar(points , sizeof(points) , 0);
    for i := 1 to N do
      for j := i + 1 to N do
        begin
            Get_Tangent_EX(i , j);
            Get_Tangent_IN(i , j);
        end;

    points[1].data[1].shortest := 0;
    repeat
      changed := false;
      for i := 1 to N do
        with points[i] do
          for j := 1 to tot do
            for k := 1 to tot do if j <> k then
              begin
                  tmp := calc_arc(i , j , k) + data[j].shortest;
                  if data[k].shortest > tmp + minimum then
                    begin
                        data[k].shortest := tmp;
                        data[k].t1 := i;
                        data[k].t2 := j;
                        changed := true;
                    end;
              end;

      for k := 1 to E do
        with edges[k] do
          begin
              tmp := points[i].data[p].shortest + dist(points[i].data[p].p , points[j].data[q].p);
              if points[j].data[q].shortest > tmp + minimum then
                begin
                    points[j].data[q].shortest := tmp;
                    points[j].data[q].t1 := i;
                    points[j].data[q].t2 := p;
                    changed := true;
                end;
          end;
    until not changed;
end;

procedure out;
var
    answer     : double;
    ni , nj ,
    i , j      : longint;
begin
    answer := points[2].data[1].shortest;
    answer := answer / 200 * 60 * 60;
    writeln(answer : 0 : 2);
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
