Const
    Limit      = 300;

Type
    Tdata      = array[0..Limit , 0..Limit] of comp;

Var
    data       : Tdata;
    p1 , p2    : comp;
    N , L1 , L2: longint;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    for j := 0 to Limit do data[0 , j] := 1;
    for i := 1 to Limit do
      for j := 1 to Limit do
        begin
            data[i , j] := data[i , j - 1];
            if i >= j then
              data[i , j] := data[i , j] + data[i - j , j];
        end;
end;

procedure init;
begin
    read(N);
    if eoln
      then begin L1 := 0; L2 := 300; end
      else begin
               read(L1);
               if eoln
                 then begin L2 := L1; L1 := 0; end
                 else begin
                          read(L2);
                      end;
           end;
    readln;
end;

Begin
    pre_process;
    while not eof do
      begin
          init;
          if L1 = 0
            then p1 := 0
            else if L1 - 1 > Limit
                   then p1 := data[N , Limit]
                   else p1 := data[N , L1 - 1];
          if L2 > Limit
            then p2 := data[N , Limit]
            else p2 := data[N , L2];
          writeln(p2 - p1 : 0 : 0);
      end;
End.