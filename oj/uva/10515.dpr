{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10515.in';
    OutFile    = 'p10515.out';
    LimitLen   = 100;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                 end;
    Tdata      = array[0..1000] of longint;

Var
    n          : lint;
    _div2 , mod2 ,
    mod10      : Tdata;
    m ,
    answer     : longint;

function init : boolean;
var
    c          : char;
    i , tmp    : longint;
begin
      read(c); m := 0;
      while c = ' ' do read(c);
      while c <> ' ' do
        begin
            m := ord(c) - ord('0');
            read(c);
        end;
      while c = ' ' do read(c);
      fillchar(n , sizeof(n) , 0);
      while c <> ' ' do
        begin
            inc(n.len);
            n.data[n.len] := ord(c) - ord('0');
            if eoln then break;
            read(c);
        end;
      readln;
      for i := 1 to n.len div 2 do
        begin
            tmp := n.data[i]; n.data[i] := n.data[n.len - i + 1];
            n.data[n.len - i + 1] := tmp;
        end;
      while (n.len > 1) and (n.data[n.len] = 0) do dec(n.len);
      if (m = 0) and (n.len = 1) and (n.data[1] = 0)
        then init := false
        else init := true;
end;

procedure div2(var num : lint);
var
    last , i   : longint;
begin
    last := 0;
    for i := num.len downto 1 do
      begin
          last := last * 10 + num.data[i];
          num.data[i] := _div2[last];
          last := mod2[last];
      end;
    while (num.len > 1) and (num.data[num.len] = 0) do dec(num.len);
end;

procedure work;
begin
    answer := 1;
    while (n.len > 1) or (n.data[1] <> 0) do
      begin
          if odd(n.data[1]) then answer := mod10[answer * m];
          m := mod10[m * m];
          div2(n);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

procedure pre_process;
var
    i          : longint;
begin
    for i := 0 to 1000 do
      begin
          _div2[i] := i div 2;
          mod2[i] := i mod 2;
          mod10[i] := i mod 10;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
