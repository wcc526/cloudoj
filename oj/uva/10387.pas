Var
    a , b ,
    s , m , n  : longint;
    tg , Len   : double;
Begin
    readln(a , b , s , m , n);
    while a + b + s + m + n > 0 do
      begin
          tg := b * n / (a * m);
          Len := a * m * sqrt(1 + sqr(tg));
          writeln(arctan(tg) * 180 / pi : 0 : 2 , ' ' , Len / s : 0 : 2);
          readln(a , b , s , m , n);
      end;
End.