Var
    R , H ,
    d1 , A1 ,
    d2 , A2 ,
    x1 , y1 ,
    x2 , y2 ,
    deltaA     : extended;
Begin
    while not eof do
      begin
          readln(r , h , d1 , A1 , d2 , A2);
          h := sqrt(sqr(R) + sqr(H));
          deltaA := abs(A1 - A2) / 180 * pi;
          deltaA := deltaA - int(deltaA / pi / 2) * pi * 2;
          if deltaA < 0 then deltaA := deltaA + pi * 2;
          if 2 * pi - deltaA < deltaA then deltaA := 2 * pi - deltaA;
          deltaA := deltaA * r / h;
          x1 := d1; y1 := 0;
          x2 := d2 * cos(deltaA); y2 := d2 * sin(deltaA);
          writeln(sqrt(sqr(x1 - x2) + sqr(y1 - y2)) : 0 : 2);
      end;
End.