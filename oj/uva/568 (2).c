#include <stdio.h>

int main()
{
  short i, j, k = 0, p, m[10][10], v[10001];

  for (i = 0; i < 10; ++i)
    for (j = i; j < 10; ++j)
      m[i][j] = m[j][i] = i * j % 10;

  for (v[0] = p = i = 1; (j = i) < 10001; ++i) {
    while ((j & 1) == 0)
      j >>= 1, ++k;
    while (j % 5 == 0)
      j /= 5, --k;
    p = m[p][j % 10];
    if (k > 9)
      k -= 4;
    v[i] = m[p][(1 << k) % 10];
  }

  while (scanf("%hd", &i) == 1)
    printf("%5hd -> %hd\n", i, v[i]);

  return 0;
}
