{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
Var
    T , ans ,
    x1 , y1 ,
    x2 , y2 ,
    i , j      : longint;

Begin
    readln(T);
    while T > 0 do
      begin
          readln(x1 , x2 , y1 , y2);
          ans := 0;
          for i := x1 to x2 do
            for j := y1 to y2 do
              if ((i * j mod 6 = 0) and (i > 1) and (j > 1)) or (i * j mod 3 = 0) and (i > 3) and (j > 3) then
                inc(ans);
          writeln(ans);
          dec(T);
      end;
End.