Const
    Limit      = 1000000;

Type
    Tprime     = array[1..Limit] of boolean;
    Tdata      = array[1..Limit] of longint;

Var
    prime      : Tprime;
    data       : Tdata;
    answer , i , j ,
    tot , T , N: longint;

procedure process;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    tot := 0;
    for i := 2 to Limit do
      if prime[i] then
        begin
            inc(tot); data[tot] := i;
            for j := 2 to Limit div i do
              prime[i * j] := false;
        end;
end;

Begin
    process;
    readln(N); T := 0;
    while N <> 0 do
      begin
          inc(T);
          answer := 0; j := tot;
          for i := 1 to tot do
            begin
                while (i < j) and (data[i] + data[j] > N) do dec(j);
                if i = j then break;
                inc(answer , j - i);
            end;
          writeln('Case ' , T , ': ' , answer);
          readln(N);
      end;
End.