{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10416.in';
    OutFile    = 'p10416.out';
    Limit      = 1000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tnum       = array[1..Limit] of longint;
    TLine      = record
                     A , B , C               : extended;
                 end;
    Tans       = object
                     tot      : longint;
                     data     : array[1..Limit * 2] of TLine;
                     procedure init;
                     procedure ins(key : TLine);
                 end;

Var
    data       : Tdata;
    prev , next: Tnum;
    ans        : Tans;
    N , cases  : longint;

function init : boolean;
var
    i          : longint;
begin
    dec(Cases);
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do
                 read(data[i].x , data[i].y);
               fillchar(prev , sizeof(prev) , 0);
               fillchar(next , sizeof(next) , 0);
               for i := 1 to N do
                 begin
                     prev[i] := (i + N - 2) mod N + 1;
                     next[i] := i mod N + 1;
                 end;
           end;
end;

function zero(num : extended) : boolean;
begin
    zero := abs(num) <= minimum;
end;

function same_line(L1 , L2 : TLine) : boolean;
begin
    same_Line := zero(L1.A * L2.B - L1.B * L2.A) and zero(L1.A * L2.C - L1.C * L2.A)
             and zero(L1.B * L2.C - L1.C * L2.B);
end;

procedure Tans.init;
begin
    fillchar(data , sizeof(data) , 0);
    tot := 0;
end;

procedure Tans.ins(key : TLine);
var
    i          : longint;
begin
    for i := 1 to tot do
      if same_line(key , data[i]) then
        exit;
    inc(tot);
    data[tot] := key;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p1.x * P2.y - p2.x * p1.y;
end;

procedure Get_Vertical_Line(p1 , p2 : Tpoint; var L : TLine);
var
    L1         : TLine;
    p          : Tpoint;
begin
    Get_Line(p1 , p2 , L1);
    L.A := -L1.B; L.B := L1.A;
    p.x := (p1.x + p2.x) / 2; p.y := (p1.y + p2.y) / 2;
    L.C := -(p.x * L.A + p.y * L.B);
end;

procedure Get_Bisector(p1 , p2 , p3 : Tpoint; var L : TLine);
var
    a , b      : Tpoint;
    d1 , d2    : extended;
begin
    a.x := p1.x - p2.x; a.y := p1.y - p2.y;
    b.x := p3.x - p2.x; b.y := p3.y - p2.y;
    d1 := sqrt(sqr(a.x) + sqr(a.y)); d2 := sqrt(sqr(b.x) + sqr(b.y));
    a.x := a.x / d1; b.x := b.x / d2; a.y := a.y / d1; b.y := b.y / d2;
    a.x := a.x + p2.x; a.y := a.y + p2.y;
    b.x := b.x + p2.x; b.y := b.y + p2.y;
    Get_Vertical_Line(a , b , L);
end;

function symmetry(p1 , p2 : Tpoint; L : TLine) : boolean;
var
    L1         : TLine;
    p          : Tpoint;
begin
    Get_Line(p1 , p2 , L1);
    p.x := (p1.x + p2.x) / 2; p.y := (p1.y + p2.y) / 2;
    symmetry := zero(L1.B * L.B + L1.A * L.A) and zero(L.A * p.x + L.B * p.y + L.C);
end;

procedure work;
var
    L          : TLine;
    p1 , p2 ,
    i , j      : longint;
    ok         : boolean;                   
begin
    ans.init;
    for i := 1 to N do
      begin
          Get_Vertical_Line(data[i] , data[next[i]] , L);
          p1 := i; p2 := next[i]; ok := true;
          for j := 1 to (N + 1) div 2 do
            if symmetry(data[p1] , data[p2] , L)
              then begin p1 := prev[p1]; p2 := next[p2]; end
              else begin ok := false; break; end;
          if ok then ans.ins(L);
      end;

    for i := 1 to N do
      begin
          Get_Bisector(data[prev[i]] , data[i] , data[next[i]] , L);
          p1 := prev[i]; p2 := next[i]; ok := true;
          for j := 1 to N div 2 do
            if symmetry(data[p1] , data[p2] , L)
              then begin p1 := prev[p1]; p2 := next[p2]; end
              else begin ok := false; break; end;
          if ok then ans.ins(L);
      end;
end;

procedure out;
begin
    if ans.tot = 0
      then writeln('No')
      else writeln('Yes');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.