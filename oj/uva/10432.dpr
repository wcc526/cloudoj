{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Var
    R , N      : extended;
    
Begin
    while not eof do
      begin
          readln(R , N);
          writeln(sin(2 * pi / N) * R * R / 2 * N : 0 : 3);
      end;
End.
