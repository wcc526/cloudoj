#include <stdio.h>

double f(char **s)
{
  char *c;
  double p, x, y;
  while (**s == ' ') ++*s;
  if (**s == '(') {
    ++*s;
    p = f(s);
    x = f(s);
    y = f(s);
    while (**s == ' ') ++*s;
    ++*s;
    return p * (x + y) + (1 - p) * (x - y);
  } else {
    sscanf(*s, "%lf", &p);
    while (**s != ' ' && **s != ')') ++*s;
    return p;
  }
}

int main()
{
  double v;
  char a[65536], *s;

  while (gets(a)) {
    if (a[0] == '(' && a[1] == ')')
      break;
    s = a;
    v = f(&s);
    printf("%.2lf\n", v);
  }

  return 0;
}
