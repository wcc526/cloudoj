{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10651.in';
    OutFile    = 'p10651.out';
    N          = 12;

Type
    Tdata      = array[0..1 shl N] of longint;

Var
    data       : Tdata;
    ch         : char;
    T , M ,
    answer , i : longint;

procedure pre_process;
var
    i , j , tmp: longint;
    changed    : boolean;
begin
    fillchar(data , sizeof(data) , 0);
    changed := true;
    while changed do
      begin
          changed := false;
          for i := 1 shl N - 1 downto 0 do
            for j := 1 to N - 2 do
              if (1 shl (j - 1) and i <> 0) and (1 shl j and i <> 0) and (1 shl (j + 1) and i = 0) then
                begin
                    tmp := i - 1 shl (j - 1) - 1 shl j + 1 shl (j + 1);
                    if data[tmp] + 1 > data[i] then begin changed := true; data[i] := data[tmp] + 1; end;
                end;
          for i := 0 to 1 shl N - 1 do
            for j := 3 to N do
              if (1 shl (j - 3) and i = 0) and (1 shl (j - 2) and i <> 0) and (1 shl (j - 1) and i <> 0) then
                begin
                    tmp := i + 1 shl (j - 3) - 1 shl (j - 2) - 1 shl (j - 1);
                    if data[tmp] + 1 > data[i] then begin changed := true; data[i] := data[tmp] + 1; end;
                end;
      end;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            M := 0; answer := 0;
            for i := 1 to 12 do
              begin
                  M := M * 2;
                  read(ch);
                  if ch = 'o' then begin inc(M); inc(answer); end;
              end;
            readln;
            answer := answer - data[M];
            writeln(answer);
            dec(T);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
