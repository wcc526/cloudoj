{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10583.in';
    OutFile    = 'p10583.out';
    Limit      = 50000;

Type
    Tfather    = array[1..Limit] of longint;

Var
    father     : Tfather;
    cases , 
    N , M , ans: longint;

function Get_Father(p : longint) : longint;
var
    tmp        : longint;
begin
    if father[p] = 0
      then Get_Father := p
      else begin
               tmp := Get_Father(father[p]);
               father[p] := tmp;
               Get_Father := tmp;
           end;
end;

procedure main;
var
    i , 
    p1 , p2    : longint;
begin
    read(N , M); cases := 0;
    while N <> 0 do
      begin
          ans := N;
          for i := 1 to N do father[i] := 0;
          for i := 1 to M do
            begin
                read(p1 , p2);
                p1 := Get_Father(p1);
                p2 := Get_Father(p2);
                if p1 <> p2 then
                  begin
                      dec(ans);
                      father[p1] := p2;
                  end;
            end;
          inc(Cases);
          writeln('Case ' , cases , ': ' , ans);
          read(N , M);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      main;
//    Close(OUTPUT);
//    Close(INPUT);
End.
