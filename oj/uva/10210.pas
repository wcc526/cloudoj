Var
    x1 , y1 , x2 , y2 ,
    alpha , beta ,
    d          : double;

Begin
    while not eof do
      begin
          readln(x1 , y1 , x2 , y2 , alpha , beta);
          alpha := alpha / 180 * pi; beta := beta / 180 * pi;
          d := sqrt(sqr(x1 - x2) + sqr(y1 - y2));
          writeln(d / sin(alpha) * cos(alpha) + d / sin(beta) * cos(beta) : 0 : 3);
      end;
End.