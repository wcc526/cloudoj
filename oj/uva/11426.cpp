#include <stdio.h>
#include <string.h>
typedef long long LL;
const int maxn = 4000001;

LL f[maxn], s[maxn];

int phi[maxn] = {0};
void phi_table()
{
    int i, j;
    phi[1] = 1;
    for(i = 2; i < maxn; ++ i)
    {
        if(!phi[i]) for(j = i; j < maxn; j += i)
            {
                if(!phi[j]) phi[j] = j;
                phi[j] -= phi[j] / i;
            }
    }
}
int main()
{
    //freopen("data.in", "r", stdin);
    phi_table();

    //Ԥ����f
    memset(f, 0, sizeof (f));
    for(int i = 1; i <= maxn; i ++)
        for(int n = i * 2; n <= maxn; n += i)
            f[n] += i * phi[n / i];
    //Ԥ����s
    s[2] = f[2];
    for(int n = 3; n <= maxn; n ++)
        s[n] = s[n - 1] + f[n];

    int n;
    while(scanf("%d", &n) == 1 && n)
        printf("%lld\n", s[n]);
    return 0;
}