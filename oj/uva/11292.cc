#include <algorithm>
#include <cstdio>

using namespace std;

int main()
{
  int c, i, j, n, m, d[20001], h[20001];

  while (scanf("%d %d", &n, &m) == 2 && (n || m)) {
    for (i = n; i--;)
      scanf("%d", &d[i]);
    for (i = m; i--;)
      scanf("%d", &h[i]);
    sort(d, d + n);
    sort(h, h + m);
    for (c = i = j = 0; i < n && j < m;) {
      if (d[i] > h[j])
        ++j;
      else
        c += h[j], ++i, ++j;
    }
    if (i == n)
      printf("%d\n", c);
    else
      puts("Loowater is doomed!");
  }

  return 0;
}
