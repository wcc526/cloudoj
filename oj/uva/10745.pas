Const
    InFile     = 'p10745.in';
    OutFile    = 'p10745.out';
    Limit      = 15000;
    LimitLen   = 10;

Type
    Tstr       = string[LimitLen];
    Tkey       = record
                     s , u    : Tstr;
                     ok       : boolean;
                 end;
    Tdata      = array[1..Limit] of Tkey;
    Tfather    = array[1..Limit] of longint;
    Tpoint     = ^Tnode;
    Tnode      = record
                     ch       : char;
                     signal   : longint;
                     child    : array['a'..'z'] of Tpoint;
                 end;

Var
    data       : Tdata;
    father     : Tfather;
    root       : Tpoint;
    N          : longint;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
//    assign(INPUT , InFile); ReSet(INPUT);
      N := 0;
      while not eof do
        begin
            inc(N); readln(data[N].s);
            data[N].u := data[N].s;
            for i := 1 to length(data[N].u) do
              for j := i + 1 to length(data[N].u) do
                if data[N].u[i] > data[N].u[j] then
                  begin
                      ch := data[N].u[i]; data[N].u[i] := data[N].u[j];
                      data[N].u[j] := ch;
                  end;
            data[N].ok := true;
        end;
//    Close(INPUT);
end;

procedure qk_pass_by_s(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tkey;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].s > key.s) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].s < key.s) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort_by_s(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_by_s(start , stop , mid);
          qk_sort_by_s(start , mid - 1);
          qk_sort_by_s(mid + 1 , stop);
      end;
end;

procedure qk_pass_by_u(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tkey;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].u > key.u) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].u < key.u) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort_by_u(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_by_u(start , stop , mid);
          qk_sort_by_u(start , mid - 1);
          qk_sort_by_u(mid + 1 , stop);
      end;
end;

procedure mark(s : Tstr);
var
    p          : Tpoint;
    i          : longint;
begin
    p := root;
    for i := 1 to length(s) do
      begin
          p := p^.child[s[i]];
          if p = NIL then exit;
      end;
    if p^.signal <> 0 then
      data[p^.signal].ok := false;
end;

procedure new_point(var p : Tpoint; ch : char);
begin
    new(p);
    p^.signal := 0; p^.ch := ch;
    for ch := 'a' to 'z' do p^.child[ch] := NIL;
end;

procedure add(s : Tstr; num : longint);
var
    p          : Tpoint;
    i          : longint;
begin
    p := root;
    for i := 1 to length(s) do
      begin
          if p^.child[s[i]] = NIL then new_point(p^.child[s[i]] , s[i]);
          p := p^.child[s[i]];
      end;
    p^.signal := num;
end;

procedure work;
var
    i , j , k ,
    tmp        : longint;
    s , ns     : Tstr;
begin
    qk_sort_by_u(1 , N);
    father[1] := 1;
    for i := 2 to N do
      if data[i].u = data[i - 1].u
        then father[i] := father[i - 1]
        else father[i] := i;
    new_point(root , #0);
    for i := 1 to N do
      if father[i] = i then
        add(data[i].u , i)
      else data[i].ok := false;

    for i := 1 to N do if father[i] = i then
      begin
          s := data[i].u;
          for j := 0 to 1 shl length(s) - 2 do
            begin
                ns := ''; tmp := j;
                for k := 1 to length(s) do
                  begin
                      if odd(tmp) then ns := ns + s[k];
                      tmp := tmp div 2;
                  end;
                mark(ns);
            end;
      end;
    qk_sort_by_s(1 , N);
end;

procedure out;
var
    i          : longint;
begin
    for i := 1 to N do
      if data[i].ok and ((i = N) or (data[i].u <> data[i + 1].u)) then
        writeln(data[i].s);
end;

Begin
    init;
    work;
    out;
End.