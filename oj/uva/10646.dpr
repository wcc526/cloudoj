{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
Const
    InFile     = 'p10646.in';
    OutFile    = 'p10646.out';

Type
    Tdata      = array[1..52] of longint;
    Tcolor     = array[1..52 , 1..2] of char;

Var
    color1 ,
    color2     : Tcolor;
    data1 ,
    data2      : Tdata;
    T , nowCase ,
    tot1       : longint;
    ac1 , ac2  : char;

procedure init;
var
    c1 , c2 ,
    c          : char;
    i          : longint;
begin
    dec(T);
    for i := 27 downto 1 do
      begin
          read(c); while c = ' ' do read(c);
          c1 := c; read(c2);
          color1[i , 1] := c1;
          color1[i , 2] := c2;
          if c1 in['2'..'9']
            then data1[i] := ord(c) - ord('0')
            else data1[i] := 10;
      end;
    for i := 1 to 25 do
      begin
          read(c); while c = ' ' do read(c);
          c1 := c; read(c2);
          color2[i , 1] := c1;
          color2[i , 2] := c2;
          if c1 in['2'..'9']
            then data2[i] := ord(c) - ord('0')
            else data2[i] := 10;
      end;
    readln;
    inc(nowCase);
end;

procedure work;
var
    Y , i      : longint;
begin
    tot1 := 1; Y := 0;
    for i := 1 to 3 do
      begin
          inc(Y , data1[tot1]);
          inc(tot1 , 11 - data1[tot1]);
      end;
    if 25 - tot1 + 1 >= Y
      then begin ac1 := color1[tot1 + Y - 1 , 1]; ac2 := color2[tot1 + Y - 1 , 2]; end
      else begin ac1 := color2[tot1 + Y - 1 - 27 , 1]; ac2 := color2[tot1 + Y - 1 - 27 , 2]; end;
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , ac1 , ac2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T); nowCase := 0;
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
