Const
    InFile     = 'p10309.in';
    N          = 10;

Type
    Tdata      = array[1..N] of longint;
    Tcount     = array[0..1 shl N - 1] of longint;

Var
    data       : Tdata;
    count      : Tcount;
    answer     : longint;
    name       : string;

function init : boolean;
var
    i , j      : longint;
    ch         : char;
begin
    fillchar(count , sizeof(count) , 0);
    for i := 1 to 1 shl N - 1 do count[i] := count[i div 2] + i mod 2;
    readln(name);
    if name = 'end' then exit(false);
    init := true;
    fillchar(data , sizeof(data) , 0);
    for i := 1 to N do
      begin
          for j := 1 to N do
            begin
                read(ch);
                data[i] := data[i] * 2;
                if ch = 'O' then inc(data[i]);
            end;
          readln;
      end;
end;

function calc(option : longint) : longint;
begin
    calc := option xor (option div 2) xor (option * 2 mod (1 shl N));
end;

procedure check(data : Tdata; option : longint);
var
    i , tmp    : longint;
begin
    tmp := count[option];
    data[1] := data[1] xor calc(option);
    data[2] := data[2] xor option;
    for i := 2 to N do
      begin
          option := data[i - 1];
          inc(tmp , count[option]);
          data[i - 1] := data[i - 1] xor option;
          data[i] := data[i] xor calc(option);
          if i <> N then data[i + 1] := data[i + 1] xor option;
      end;
    if data[N] = 0
      then if tmp < answer
             then answer := tmp;
end;

procedure work;
var
    i          : longint;
begin
    answer := maxlongint;
    for i := 0 to 1 shl N - 1 do
      check(data , i);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            write(name + ' ');
            if answer = maxlongint
              then writeln(-1)
              else writeln(answer);
        end;
//    Close(INPUT);
End.