#include <math.h>
#include <stdio.h>

#define D(a,b) sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))

typedef struct {
  short x, y;
} pt;

double b, c, d[256];
unsigned short k, m;

void S(int i, int n)
{
  int j;
  m ^= 1 << i;
  for (j = i; ++j < n;) {
    if (m & 1 << j)
      continue;
    c += d[(i << 4) + j];
    if (b > c) {
      m ^= 1 << j;
      for (k = i; m & 1 << ++k;);
      S(k, n);
      if (m == (1 << n) - 1)
        b = c;
      m ^= 1 << j;
    }
    c -= d[(i << 4) + j];
  }
  m ^= 1 << i;
}

int main(void)
{
  int i, j, n, r = 0;
  pt p[16];

  while (scanf("%d", &n) == 1 && n) {
    for (i = n <<= 1; i--;)
      scanf("%*s %hd %hd", &p[i].x, &p[i].y);
    for (i = n; i--;)
      for (j = i; j--;)
        d[(j << 4) + i] = D(p[i], p[j]);
    b = 16000.0;
    c = 0.0;
    m = 0;
    S(0, n);
    printf("Case %d: %.2lf\n", ++r, b);
  }

  return 0;
}
