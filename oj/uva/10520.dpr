{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10520.in';
    OutFile    = 'p10520.out';
    Limit      = 20;

Type
    Tdata      = array[1..Limit , 1..Limit] of extended;

Var
    data       : Tdata;
    N          : longint;

function init : boolean;
begin
    if eof
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               read(N);
               readln(data[N , 1]);
           end;
end;

procedure work;
var
    i , j , k  : longint;
    max        : extended;
begin
    for i := N downto 1 do
      for j := 1 to N do
        if (i <> N) or (j <> 1) then
          if i >= j
            then begin
                     max := 0;
                     for k := i + 1 to N do
                       if data[k , 1] + data[k , j] > max then
                         max := data[k , 1] + data[k , j];
                     data[i , j] := data[i , j] + max;
                     max := 0;
                     for k := 1 to j - 1 do
                       if data[i , k] + data[N , k] > max then
                         max := data[i , k] + data[N , k];
                     data[i , j] := data[i , j] + max;
                 end
            else begin
                     max := 0;
                     for k := i to j - 1 do
                       if data[i , k] + data[k + 1 , j] > max then
                         max := data[i , k] + data[k + 1 , j];
                     data[i , j] := data[i , j] + max;
                 end;
end;

procedure out;
begin
    writeln(data[1 , N] : 0 : 0);
end;
Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
