Const
    InFile     = 'p10457.in';
    OutFile    = 'p10457.out';
    Limit      = 2000;
    LimitN     = 200;
    LimitK     = 50;

Type
    Tedge      = record
                     p1 , p2 , v             : longint;
                 end;
    Tdata      = array[1..Limit] of Tedge;
    Tindex     = array[1..Limit] of longint;
    Tvisited   = array[1..LimitN] of boolean;
    Tvelocity  = array[1..Limit] of longint;
    Tquery     = array[1..LimitK] of
                   record
                       st , ed , answer      : longint;
                   end;

Var
    data       : Tdata;
    index      : Tindex;
    visited    : Tvisited;
    query      : Tquery;
    velocity   : Tvelocity;
    N , M , K ,
    start , stop
               : longint;

procedure init;
var
    p1 , p2 , v ,
    i          : longint;
begin
    readln(N , M);
    for i := 1 to M do
      begin
          readln(p1 , p2 , v);
          data[i].p1 := p1; data[i].p2 := p2; data[i].v := v;
          data[i + M].p1 := p2; data[i + M].p2 := p1; data[i + M].v := v;
          velocity[i] := v;
      end;
    readln(start , stop);
    readln(K);
    for i := 1 to K do
      with query[i] do
        readln(st , ed);
    M := M * 2;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tedge;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].p1 > key.p1) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].p1 < key.p1) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function _dfs(st , ed , p1 , p2 : longint) : boolean;
var
    i          : longint;
begin
    if st = ed
      then exit(true)
      else begin
               visited[st] := true;
               i := index[st];
               while (i <> 0) and (i <= M) and (data[i].p1 = st) do
                 begin
                     if not visited[data[i].p2] and (data[i].v >= p1) and (data[i].v <= p2) then
                       if _dfs(data[i].p2 , ed , p1 , p2) then exit(true);
                     inc(i);
                 end;
               exit(false);
           end;
end;

function dfs(st , ed , p1 , p2 : longint) : boolean;
begin
    fillchar(visited , sizeof(visited) , 0);
    dfs := _dfs(st , ed , p1 , p2);
end;

procedure work;
var
    j , tmp ,
    i , p1 , p2: longint;
    quit       : boolean;
begin
    qk_sort(1 , M);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[data[i].p1] = 0 then
        index[data[i].p1] := i;
    for i := 1 to M div 2 do
      for j := i + 1 to M div 2 do
        if velocity[i] > velocity[j] then
          begin
              tmp := velocity[i]; velocity[i] := velocity[j]; velocity[j] := tmp;
          end;

    velocity[M + 1] := maxlongint;
    for i := 1 to K do
      with query[i] do
        begin
            p1 := 1; p2 := 1; answer := maxlongint; quit := false;
            repeat
              while not dfs(st , ed , velocity[p1] , velocity[p2]) do
                begin
                    inc(p2);
                    if p2 > M div 2 then begin quit := true; break; end;
                end;
              if quit then break;
              while dfs(st , ed , velocity[p1] , velocity[p2]) do
                inc(p1);
              dec(p1);
              tmp := velocity[p2] - velocity[p1] + start + stop;
              inc(p2);
              if tmp < answer then answer := tmp;
            until p2 > M div 2;
        end;
end;

procedure out;
var
    i          : longint;
begin
    for i := 1 to K do
      writeln(query[i].answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.