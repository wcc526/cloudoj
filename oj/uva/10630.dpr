{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10630.in';
    OutFile    = 'p10630.out';
    LimitN     = 60;
    LimitM     = 60;
    LimitK     = 9;

Type
    Tmap       = array[1..LimitM , 1..LimitM] of longint; { 1 - normal; 2 - on tree; 3 - down; 4 - up }
    Tdata      = array[-1..LimitN + 1] of Tmap;
    Tmatch     = array[1..LimitM] of longint;
    Tvisited   = array[1..LimitM] of boolean;
    Tused      = array[1..LimitN , 1..LimitM] of boolean;
    Tpath      = record
                     tot      : longint;
                     data     : array[1..LimitN * LimitM * LimitK] of
                                  record
                                      x , y                 : longint;
                                  end;
                 end;

Var
    data ,
    T1 , T2    : Tdata;
    path       : Tpath;
    match      : Tmatch;
    visited    : Tvisited;
    used       : Tused;
    N , M , K ,
    totCase ,
    nowCase    : longint;

procedure init;
var
    i , j ,
    t , p      : longint;
begin
    read(N , M , K);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to N - 1 do
      for j := 1 to M do
        for t := 1 to K do
          begin
              read(p);
              data[i , j , p] := 1;
          end;
end;

function dfs_extend(const map : Tmap; var match : Tmatch; side , root : longint) : boolean;
var
    i          : longint;
begin
    if side = 1
      then begin
               dfs_extend := false;
               for i := 1 to M do
                 if not visited[i] and (map[root , i] = 1) then
                   if dfs_extend(map , match , 2 , i)
                     then begin
                              dfs_extend := true;
                              match[i] := root;
                              exit;
                          end;
           end
      else begin
               visited[root] := true;
               if match[root] = 0
                 then dfs_extend := true
                 else dfs_extend := dfs_extend(map , match , 1 , match[root]);
           end;
end;

procedure Get_Match(const map : Tmap; var match : Tmatch);
var
    i          : longint;
begin
    fillchar(match , sizeof(match) , 0);
    for i := 1 to M do
      begin
          fillchar(visited , sizeof(visited) , 0);
          dfs_extend(map , match , 1 , i);
      end;
end;

procedure Get_Tree;
var
    i , j      : longint;
begin
    T1 := data; T2 := data;
    for i := 1 to N - 1 do
      begin
          Get_Match(data[i] , match);
          for j := 1 to M do
            begin
                if odd(j)
                  then T1[i , match[j] , j] := 2
                  else T2[i , match[j] , j] := 2;
                data[i , match[j] , j] := 0;
            end;
      end;
end;

procedure Make_Direction;
var
    i , j , p  : longint;
begin
    for i := 1 to N - 1 do
      for j := 1 to M do
        for p := 1 to M do
          if (T1[i , j , p] = 2) or (T2[i , j , p] = 2)
            then if T1[i , j , p] = 2
                   then T2[i , j , p] := 4
                   else T1[i , j , p] := 4;
    for i := 1 to N - 1 do
      for p := 1 to K - 1 do
        begin
            Get_Match(T1[i] , match);
            for j := 1 to M do
              if odd(p)
                then begin T1[i , match[j] , j] := 3; T2[i , match[j] , j] := 3; end
                else begin T1[i , match[j] , j] := 4; T2[i , match[j] , j] := 4; end;
        end;
    for i := 1 to M do
      if odd(i)
        then begin T1[0 , 1 , i] := 2; T2[0 , 1 , i] := 4; T1[N , i , 1] := 3; T2[N , i , 1] := 4; end
        else begin T1[0 , 1 , i] := 4; T2[0 , 1 , i] := 2; T1[N , i , 1] := 4; T2[N , i , 1] := 3; end;
    T1[N , 1 , 1] := 2; T2[N , 2 , 1] := 2;
end;

procedure go(var T : Tdata; x , y , signal : longint; var nx , ny : longint);
var
    lx , ly ,
    i          : longint;
begin
    lx := -1; ly := -1;
    if (x >= 1) and (x <= N)
      then if odd(y + signal)
             then begin lx := x; ly := (y + M - 2) mod M + 1; end
             else if not used[x , y]
                    then begin used[x , y] := true; nx := x; ny := (y + M - 2) mod M + 1; exit; end;
    for i := 1 to M do
      if T[x , y , i] = 4
        then begin T[x , y , i] := 0; nx := x + 1; ny := i; exit; end;
    for i := 1 to M do
      if T[x - 1 , i , y] = 2
        then begin lx := x - 1; ly := i; end
        else if T[x - 1 , i , y] = 3
               then begin T[x - 1 , i , y] := 0; nx := x - 1; ny := i; exit; end;
    nx := lx; ny := ly;
end;

procedure print_path(var T : Tdata; signal : longint);
var
    x , y ,
    nx , ny ,
    i          : longint;
begin
    fillchar(path , sizeof(path) , 0);
    x := 0; y := 1;

    fillchar(used , sizeof(used) , 0);
    for i := 1 to M * (N * K - K + N + 2) do
      begin
          inc(path.tot);
          path.data[path.tot].x := x; path.data[path.tot].y := y;
          if i <> M * (N * K - K + N + 2) then go(T , x , y , signal , nx , ny);
          x := nx; y := ny;
      end;
    for i := M * (N * K - K + N + 2) downto 1 do
      begin
          write(path.data[i].x);
          if (path.data[i].x >= 1) and (path.data[i].x <= N)
            then writeln(' ' , path.data[i].y)
            else writeln(' ' , 0);
      end;
end;

procedure workout;
begin
    Get_Tree;
    Make_Direction;

    Writeln('Case ' , nowCase , ': ' , 'Yes');
    print_path(T1 , 1);
    print_path(T2 , 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase);
      for nowCase := 1 to totCase do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
