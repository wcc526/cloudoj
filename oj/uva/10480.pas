Const
    InFile     = 'p10480.in';
    OutFile    = 'p10480.out';
    Limit      = 50;
    S          = 1;
    T          = 2;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;

Var
    backupF ,
    C , F      : Tmap;
    N          : longint;

function init : boolean;
var
    i , p1 , p2 ,
    cost , M   : longint;
begin
    fillchar(C , sizeof(C) , 0);
    fillchar(F , sizeof(F) , 0);
    readln(N , M);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to M do
      begin
          readln(p1 , p2 , cost);
          inc(C[p1 , p2] , cost);
          inc(C[p2 , p1] , cost);
      end;
end;

function extend : boolean;
type
    Tshortest  = array[1..Limit] of
                   record
                       father , cost         : longint;
                   end;
    Tqueue     = array[1..Limit * 10] of longint;
    Tinqueue   = array[1..Limit] of boolean;
var
    shortest   : Tshortest;
    queue      : Tqueue;
    inqueue    : Tinqueue;
    open , closed ,
    i , tmp    : longint;
begin
    open := 1; closed := 1;
    fillchar(shortest , sizeof(shortest) , 0); shortest[S].cost := maxlongint;
    fillchar(inqueue , sizeof(inqueue) , 0); inqueue[S] := true;
    queue[1] := S;
    while open <= closed do
      begin
          inqueue[queue[open]] := false;
          for i := 1 to N do
            if C[queue[open] , i] > F[queue[open] , i] then
              begin
                  if shortest[queue[open]].cost > C[queue[open] , i] - F[queue[open] , i]
                    then tmp := C[queue[open] , i] - F[queue[open] , i]
                    else tmp := shortest[queue[open]].cost;
                  if tmp > shortest[i].cost
                    then begin
                             shortest[i].cost := tmp; shortest[i].father := queue[open];
                             if not inqueue[i] then
                               begin inqueue[i] := true; inc(closed); queue[closed] := i; end;
                         end;
              end;
          inc(open);
      end;
    if shortest[T].cost = 0 then exit(false);
    extend := true;
    i := T;
    while i <> S do
      begin
          tmp := shortest[i].father;
          inc(F[tmp , i] , shortest[T].cost);
          dec(F[i , tmp] , shortest[T].cost);
          i := tmp;
      end;
end;

procedure del_positive(last : longint; p : longint);
var
    i , tmp    : longint;
begin
    if p <> T then
      for i := 1 to N do
        if F[p , i] > 0 then
          begin
              if F[p , i] > last then tmp := last else tmp := F[p , i];
              dec(F[p , i] , tmp); inc(F[i , p] , tmp);
              del_positive(tmp , i);
              dec(last , tmp); if last = 0 then exit;
          end;
end;

procedure del_negative(last : longint; p : longint);
var
    i , tmp    : longint;
begin
    if p <> T then
      for i := 1 to N do
        if F[i , p] > 0 then
          begin
              if F[i , p] > last then tmp := last else tmp := F[i , p];
              dec(F[i , p] , tmp); inc(F[p , i] , tmp);
              del_negative(tmp , i);
              dec(last , tmp); if last = 0 then exit;
          end;
end;

procedure work;
var
    tmp ,
    i , j      : longint;
begin
    while extend do;
    for i := 1 to N do
      for j := 1 to N do
        if (C[i , j] > 0) and (F[i , j] = C[i , j]) then
          begin
              backupF := F;
              del_positive(F[i , j] , j);
              del_negative(F[i , j] , i);
              tmp := C[i , j];
              F[i , j] := 0; F[j , i] := 0; C[i , j] := 0; C[j , i] := 0;
              if extend
                then begin
                         F := backupF;
                         C[i , j] := tmp; C[j , i] := tmp;
                     end
                else writeln(i , ' ' , j);
          end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            writeln;
        end;
//    Close(INPUT);
End.