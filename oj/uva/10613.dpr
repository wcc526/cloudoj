{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10613.in';
    OutFile    = 'p10613.out';
    Limit      = 1000;
    minimum    = 1e-6;

Type
    Tkey       = record
                     num , signal            : longint;
                 end;
    Tdata      = array[1..Limit * 2] of Tkey;
    Tpoint     = record
                     x , y                   : extended;
                 end;
    Tcircle    = record
                     O                       : Tpoint;
                     R                       : extended;
                 end;
    Tcircles   = array[1..Limit] of Tcircle;

Var
    datax ,
    datay      : Tdata;
    circles    : Tcircles;
    size , N ,
    Cases ,
    answer     : longint;

procedure init;
var
    i          : longint;
begin
    fillchar(datax , sizeof(datax) , 0);
    fillchar(datay , sizeof(datay) , 0);
    fillchar(circles , sizeof(circles) , 0);
    dec(Cases);
    read(size , N);
    for i := 1 to N do
      with circles[i] do
        read(O.x , O.y , R);
end;

function floor(p : extended) : longint;
var
    res        : longint; 
begin
    res := trunc(p);
    while abs(res + 1 - p) <= minimum do inc(res);
    floor := res;
end;

function ceiling(p : extended) : longint;
var
    res        : longint;
begin
    res := trunc(p) + 1;
    while abs(res - 1 - p) <= minimum do dec(res);
    ceiling := res;
end;

procedure qk_pass(var data : Tdata; start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].num > key.num) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].num < key.num) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(var data : Tdata; start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(data , start , stop , mid);
          qk_sort(data , start , mid - 1);
          qk_sort(data , mid + 1 , stop);
      end;
end;

procedure process(y : longint);
var
    tot , i ,
    covercount : longint;
    len        : extended;
begin
    fillchar(datax , sizeof(datax) , 0); tot := 0;
    for i := 1 to N do
      begin
          if (circles[i].O.y <= y + 1) and (circles[i].O.y >= y)
            then begin
                     inc(tot);
                     datax[tot * 2 - 1].num := floor(circles[i].O.x - circles[i].R);
                     datax[tot * 2  ].num := ceiling(circles[i].O.x + circles[i].R);
                 end
            else if circles[i].O.y > y + 1
                   then begin
                            len := sqr(circles[i].R) - sqr(circles[i].O.y - (y + 1));
                            if len <= sqr(minimum) then continue;
                            len := sqrt(len);
                            inc(tot);
                            datax[tot * 2 - 1].num := floor(circles[i].O.x - len);
                            datax[tot * 2  ].num := ceiling(circles[i].O.x + len);
                        end
                   else begin
                            len := sqr(circles[i].R) - sqr(circles[i].O.y - y);
                            if len <= sqr(minimum) then continue;
                            len := sqrt(len);
                            inc(tot);
                            datax[tot * 2 - 1].num := floor(circles[i].O.x - len);
                            datax[tot * 2  ].num := ceiling(circles[i].O.x + len);
                        end;
          if datax[tot * 2 - 1].num < 0 then datax[tot * 2 - 1].num := 0;
          if datax[tot * 2 ].num > size then datax[tot * 2    ].num := size;
          datax[tot * 2 - 1].signal := 1; datax[tot * 2   ].signal := 2;
      end;
    qk_sort(datax , 1 , tot * 2);
    covercount := 0;
    for i := 1 to tot * 2 - 1 do
      begin
          if datax[i].signal = 1
            then inc(covercount)
            else dec(covercount);
          if covercount > 0 then
            inc(answer , datax[i + 1].num - datax[i].num);
      end;
end;

procedure work;
var
    i , p ,
    covered    : longint;
begin
    for i := 1 to N do
      begin
          datay[i * 2 - 1].num := floor(circles[i].O.y - circles[i].R); datay[i * 2 - 1].signal := 1;
          datay[i * 2  ].num := ceiling(circles[i].O.y + circles[i].R); datay[i * 2    ].signal := 2;
          if datay[i * 2 - 1].num < 0 then datay[i * 2 - 1].num := 0;
          if datay[i * 2 - 1].num > size then datay[i * 2 - 1].num := size;
      end;
    qk_sort(datay , 1 , N * 2);

    p := 1; covered := 0; answer := 0;
    for i := 0 to size - 1 do
      begin
          while (p <= N * 2) and (datay[p].num = i) do
            begin
                if datay[p].signal = 1
                  then inc(covered)
                  else dec(covered);
                inc(p);
            end;
          if covered > 0 then
            process(i);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
