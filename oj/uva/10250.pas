var
    midx , midy ,
    x1 , y1 ,
    x2 , y2 ,
    x3 , y3 ,
    x4 , y4    : extended;

Begin
    while not eof do
      begin
          readln(x1 , y1 , x2 , y2);
          if (abs(x1 - x2) <= 1e-6) and (abs(y1 - y2) <= 1e-6) then
            begin
                writeln('Impossible.');
                continue;
            end;
          midx := (x1 + x2) / 2; midy := (y1 + y2) / 2;
          x1 := x1 - midx; y1 := y1 - midy;
          x3 := x1 * cos(pi / 2) - y1 * sin(pi / 2) + midx;
          y3 := x1 * sin(pi / 2) + y1 * cos(pi / 2) + midy;
          x4 := x1 * cos(pi / 2) + y1 * sin(pi / 2) + midx;
          y4 := - x1 * sin(pi / 2) + y1 * cos(pi / 2) + midy;
          writeln(x3 : 0 : 10 , ' ' , y3 : 0 : 10 , ' ' , x4 : 0 : 10 , ' ' , y4 : 0 : 10);
      end;
End.