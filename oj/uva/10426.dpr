{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10426.in';
    OutFile    = 'p10426.out';
    Limit      = 16;
    maximum    = 10000000;
    dirx       : array[1..8] of longint = (-1 , -2 , -2 , -1 , 1 , 2 , 2 , 1);
    diry       : array[1..8] of longint = (-2 , -1 , 1 , 2 , 2 , 1 , -1 , -2);

Type
    Tvisited   = array[1..Limit , 1..Limit] of boolean;
    Tqueue     = array[1..Limit * Limit] of
                   record
                       x , y , shortest      : longint;
                   end;
    Tdata      = array[1..5] of
                   record
                       x ,  y                : longint;
                   end;
    Topt       = array[1..4 , 0..1] of longint;
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tshortest  = array[1..4 , 0..1] of Tmap;

Var
    visited    : Tvisited;
    queue      : Tqueue;
    data       : Tdata;
    opt        : Topt;
    shortest   : Tshortest;
    N , M ,
    answer     : longint;
    s          : string;

procedure init;
var
    i          : longint;
begin
    readln(s);
    readln(N , M);
    for i := 1 to 5 do read(data[i].x , data[i].y);
    readln;
end;

procedure bfs(x0 , y0 : longint; var shortest : Tmap);
var
    open , closed ,
    i , nx , ny: longint;
begin
    for nx := 1 to N do
      for ny := 1 to M do
        shortest[nx , ny] := maximum;
    visited[x0 , y0] := true;
    open := 1; closed := 1;
    queue[open].x := x0; queue[open].y := y0; queue[open].shortest := 0;
    while open <= closed do
      begin
          shortest[queue[open].x , queue[open].y] := queue[open].shortest;
          for i := 1 to 8 do
            begin
                nx := queue[open].x + dirx[i];
                ny := queue[open].y + diry[i];
                if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) then
                  if not visited[nx , ny] then
                    begin
                        inc(closed);
                        queue[closed].x := nx; queue[closed].y := ny;
                        queue[closed].shortest := queue[open].shortest + 1;
                        visited[nx , ny] := true;
                    end;
            end;
          inc(open);
      end;
end;

procedure work;
var
    i , j , k ,
    sum        : longint;
begin
    answer := maximum;
    for k := 1 to 4 do
      begin
          fillchar(visited , sizeof(visited) , 0);
          visited[data[5].x , data[5].y] := true;
          bfs(data[k].x , data[k].y , shortest[k , 0]);
          fillchar(visited , sizeof(visited) , 0);
          bfs(data[k].x , data[k].y , shortest[k , 1]);
      end;
    for i := 1 to N do
      for j := 1 to M do
        begin
            for k := 1 to 4 do
              begin
                  opt[k , 0] := shortest[k , 0 , i , j];
                  opt[k , 1] := shortest[k , 1 , i , j];
              end;
            sum := opt[1 , 0] + opt[2 , 0] + opt[3 , 0] + opt[4 , 0];
            for k := 1 to 4 do
              if sum - opt[k , 0] + opt[k , 1] < answer then
                answer := sum - opt[k , 0] + opt[k , 1];
        end;
end;

procedure out;
begin
    writeln(s);
    if answer = maximum
      then writeln('Meeting is impossible.')
      else writeln('Minimum time required is ' , answer , ' minutes.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
