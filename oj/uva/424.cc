#include <iostream>
#include <string>

using namespace std;

int main()
{
    char num[1000][1000];
    for (int i=0; i<1000; ++i) {
        for (int j=0; j<1000; ++j) {
            num[i][j] = 0;
        }
    }

    string line;
    int lineNum = 0;
    while (getline(cin, line)) {
        if (line == "0") break;
        for (int i=0; i<line.size(); ++i) {
            num[lineNum][999-i] = line[line.size()-i-1];
        }
        lineNum++;
    }
    int carry = 0;
    for (int i=999; i>=0; --i) {
        int sum = carry;
        for (int j=0; j<lineNum; ++j) {
            if (num[j][i] >= '0' && num[j][i] <= '9') {
                sum += (num[j][i] - '0');
            }
        }
        num[lineNum][i] = (sum % 10) + '0';
        carry = sum / 10;
    }
    int i = 0;
    for (i=0; i<1000; ++i) {
        if (num[lineNum][i] != '0') break;
    }
    for ( ; i<1000; ++i) {
        cout << num[lineNum][i];
    }
    cout << endl;

    return 0;
}
