#include <math.h>
#include <stdio.h>

double a[7][7] = {
  { 1, 1, 1, 1, 1, 1, 1 },
  { 1, 2, 2*2, 2*2*2, 2*2*2*2, 2*2*2*2*2, 2*2*2*2*2*2 },
  { 1, 3, 3*3, 3*3*3, 3*3*3*3, 3*3*3*3*3, 3*3*3*3*3*3 },
  { 1, 4, 4*4, 4*4*4, 4*4*4*4, 4*4*4*4*4, 4*4*4*4*4*4 },
  { 1, 5, 5*5, 5*5*5, 5*5*5*5, 5*5*5*5*5, 5*5*5*5*5*5 },
  { 1, 6, 6*6, 6*6*6, 6*6*6*6, 6*6*6*6*6, 6*6*6*6*6*6 },
  { 1, 7, 7*7, 7*7*7, 7*7*7*7, 7*7*7*7*7, 7*7*7*7*7*7 },
};

int main()
{
  int i, j, k, t;
  long double d[7][7], x[1500], y[7], r, s;

  for (k = 0; k < 7; ++k) {
    d[k][k] = a[k][k];
    for (i = k + 1; i < 7; ++i)
      d[i][k] = a[i][k] / d[k][k], d[k][i] = a[k][i];
    for (i = k + 1; i < 7; ++i)
      for (j = k + 1; j < 7; ++j)
        a[i][j] -= d[i][k] * d[k][j];
  }

  scanf("%d", &t);
  while (t--) {
    for (i = 0; i < 1500; ++i)
      scanf("%Lf", x + i);
    for (i = 0; i < 7; y[i] = x[i] - s, ++i)
      for (s = 0, j = 0; j < i; ++j)
        s += d[i][j] * y[j];
    for (i = 7; i--; x[i] = (y[i] - s) / d[i][i])
      for (s = 0, j = 7; --j > i;)
        s += d[i][j] * x[j];
    for (i = 7; i--;)
      if (x[i] < 0 - 1e-5 || x[i] > 1000 + 1e-5)
        goto smart;
    for (i = 7; i < 1500; ++i) {
      for (r = 0, j = 7; --j;)
        r = (i + 1) * (r + x[j]);
      if (fabs(r + x[0] - x[i]) > 1e-0)
        goto smart;
    }
    printf("%.0Lf %.0Lf %.0Lf %.0Lf %.0Lf %.0Lf %.0Lf\n",
           x[0], x[1], x[2], x[3], x[4], x[5], x[6]);
    continue;
    smart:
      puts("This is a smart sequence!");
  }

  return 0;
}
