Const
    InFile     = 'p10329.in';
    OutFile    = 'p10329.out';
    LimitSave  = 1000;
    Limit      = 5000;
    LimitLen   = 100;

Type
    Tprimes    = array[1..LimitSave] of longint;
    Tsub_count = array[1..Limit] of longint;
    lint       = object
                     Len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     function times(num : longint) : boolean;
                     procedure print;
                 end;

Var
    primes ,
    count      : Tprimes;
    sub_count  : Tsub_count;
    P          : longint;
    answer     : lint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    Len := 1;
end;

function lint.times(num : longint) : boolean;
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          if i > LimitLen then exit(false);
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
    exit(true);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

function prime_chk(p : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to round(sqrt(p)) do
      if p mod i = 0 then
        exit(false);
    exit(true);
end;

procedure pre_process;
var
    i          : longint;
begin
    P := 0;
    for i := 2 to Limit do
      if prime_chk(i) then
        begin
            inc(P);
            primes[P] := i;
        end;
end;

procedure add(num , sign : longint);
var
    i          : longint;
begin
    i := 1;
    while num > 1 do
      if num mod primes[i] = 0
        then begin
                 inc(count[i] , sign);
                 num := num div primes[i];
             end
        else inc(i);
end;

procedure work;
var
    N , M ,
    A , B ,
    i , j      : longint;
begin
    fillchar(count , sizeof(count) , 0);
    fillchar(sub_count , sizeof(sub_count) , 0);
    readln(N , M);
    for i := 1 to N do
      begin
          readln(A , B);
          if B * 2 > A then B := A - B;
          for j := A - B + 1 to A do inc(sub_count[j] , 1);
          for j := 2 to B do inc(sub_count[j] , -1);
      end;
    for i := 1 to M do
      begin
          readln(A , B);
          if B * 2 > A then B := A - B;
          for j := A - B + 1 to A do inc(sub_count[j] , -1);
          for j := 2 to B do inc(sub_count[j] , 1);
      end;
    for i := 2 to Limit do
      if sub_count[i] <> 0 then
        add(i , sub_count[i]);
    for i := 1 to P do
      if count[i] < 0 then
        begin
            writeln(0);
            exit;
        end;
    answer.init; answer.data[1] := 1;
    for i := 1 to P do
      for j := 1 to count[i] do
        if not answer.times(primes[i]) then
          begin
              writeln(-1);
              exit;
          end;
    answer.print;
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        work;
//    Close(INPUT);
End.