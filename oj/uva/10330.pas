Const
    InFile     = 'p10330.in';
    OutFile    = 'p10330.out';
    Limit      = 210;
    maximum    = 1000000;

Type
    TC         = array[1..Limit , 1..Limit] of longint;

Var
    C          : TC;
    answer , M ,
    N , S , T  : longint;

procedure init;
var
    i , bound ,
    A , B , p ,
    p1 , p2    : longint;
begin
    fillchar(C , sizeof(C) , 0);
    readln(N);
    for i := 1 to N do
      read(C[i * 2 - 1 , i * 2]);
    readln(M);
    for i := 1 to M do
      begin
          readln(p1 , p2 , bound);
          p1 := p1 * 2; p2 := p2 * 2 - 1;
          inc(C[p1 , p2] , bound);
      end;
    S := N * 2 + 1; T := N * 2 + 2;
    readln(A , B);
    for i := 1 to A do
      begin read(p); C[S , p * 2 - 1] := maximum; end;
    for i := 1 to B do
      begin read(p); C[p * 2 , T] := maximum; end;
    readln;
end;

function extend : boolean;
type
    Tshortest  = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
var
    shortest ,
    father     : Tshortest;
    visited    : Tvisited;
    tmp ,
    i , max , j: longint;
begin
    fillchar(shortest , sizeof(shortest) , 0);
    fillchar(visited , sizeof(visited) , 0);
    shortest[S] := maximum;
    for i := 1 to T do
      begin
          max := 0;
          for j := 1 to T do
            if not visited[j] then
              if (max = 0) or (shortest[j] > shortest[max]) then
                max := j;

          if (max = 0) or (shortest[max] = 0) then exit(false);
          if max = T then break;
          visited[max] := true;

          for j := 1 to T do
            if not visited[j] then
              begin
                  if C[max , j] < shortest[max]
                    then tmp := C[max , j]
                    else tmp := shortest[max];
                  if shortest[j] < tmp then
                    begin
                        shortest[j] := tmp;
                        father[j] := max;
                    end;
              end;
      end;

    extend := true;
    inc(answer , shortest[T]);
    i := T;
    while i <> S do
      begin
          j := father[i];
          dec(C[j , i] , shortest[T]);
          inc(C[i , j] , shortest[T]);
          i := j;
      end;
end;

procedure work;
begin
    answer := 0;
    while extend do;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            writeln(answer);
        end;
//    Close(INPUT);
End.