{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10632.in';
    OutFile    = 'p10632.out';
    LimitLen   = 5000;
    Limit      = 40;

Type
    Tanswer    = record
                     tot , x0 , y0           : longint;
                     data                    : array[1..LimitLen] of char;
                 end;
    Tdata      = array[1..Limit , 1..Limit] of longint;

Var
    answer     : Tanswer;
    data ,
    backup     : Tdata;
    N          : longint;

function init : boolean;
var
    i , j      : longint;
    c          : char;
begin
    readln(N);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     for j := 1 to i do
                       begin
                           read(c);
                           case c of
                             'B'             : data[i , j] := 0;
                             'R'             : data[i , j] := 1;
                             'G'             : data[i , j] := 2;
                           end;
                       end;
                     readln;
                 end;
           end;
end;

procedure Go(var x , y : longint; direction : char);
begin
    inc(answer.tot);
    answer.data[answer.tot] := direction;
    case direction of
      '1'      : inc(x);
      '3'      : begin inc(x); inc(y); end;
      '7'      : begin dec(x); dec(y); end;
      '9'      : dec(x);
    end;
    data[x , y] := (data[x , y] + 1) mod 3;
end;

function Get_Answer : boolean;
var
    i , j , dir ,
    x , y      : longint;
begin
    i := N - 1; j := 1; dir := 1;
    while i >= 1 do
      begin
          while (j <= i) and (j >= 1) do
            begin
                x := i + 1;
                if dir = 1 then y := j else y := j + 1;
                while data[x , y] <> 0 do
                  if dir = 1
                    then begin go(i , j , '1'); go(i , j , '9'); end
                    else begin go(i , j , '3'); go(i , j , '7'); end;
                inc(y , dir);
                if (j + dir <= i) and (j + dir >= 1)
                  then begin
                           if data[x , y] <> 0
                             then if dir = 1
                                    then begin go(i , j , '3'); go(i , j , '9'); end
                                    else begin go(i , j , '1'); go(i , j , '7'); end
                             else if dir = 1
                                    then begin go(i , j , '9'); go(i , j , '3'); end
                                    else begin go(i , j , '7'); go(i , j , '1'); end;
                       end
                  else begin
                           while data[x , y] <> 0 do
                             if dir = 1
                               then begin go(i , j , '3'); go(i , j , '7'); end
                               else begin go(i , j , '1'); go(i , j , '9'); end;
                           if i > 1
                             then if dir = 1
                                    then go(i , j , '7')
                                    else go(i , j , '9')
                             else dec(i);
                           break;
                       end;
            end;
          dir := -dir;
      end;

    Get_Answer := true;
    if data[1 , 1] = 0 then exit;
    if data[1 , 1] = 1
      then begin dec(answer.tot); exit; end
      else Get_Answer := false;
end;

procedure work;
begin
    backup := data;

    data := backup;
    fillchar(answer , sizeof(answer) , 0);
    answer.x0 := N - 1; answer.y0 := 1;
    if Get_Answer then exit;

    data := backup;
    fillchar(answer , sizeof(answer) , 0);
    answer.x0 := N; answer.y0 := 1;
    answer.tot := 1; answer.data[1] := '9';
    data[N - 1 , 1] := (data[N - 1 , 1] + 1) mod 3;
    if Get_Answer then exit;
end;

procedure out;
var
    i          : longint;
begin
    writeln(answer.x0 , ' ' , answer.y0);
    for i := 1 to answer.tot do
      write(answer.data[i]);
    writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
