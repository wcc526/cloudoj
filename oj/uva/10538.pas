{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10538.in';
    OutFile    = 'p10538.out';

Type
    Tdata      = array[1..5 , 1..5] of longint;
    Tvisited   = array[1..25] of boolean;
    Tsum       = array[1..5] of longint;

Var
    data       : Tdata;
    visited    : Tvisited;
    sum        : Tsum;
    answer     : longint;

procedure check;
var
    i          : longint;
begin
    writeln('ok');
    i := 65 - (data[1 , 5] + data[2 , 5] + data[3 , 5] + data[4 , 5]);
    if (i <= 25) and (i >= 1) and not visited[i] then
      begin
          if (data[1 , 2] = 7) and (data[1 , 3] = 13) then
            writeln;
          if data[5 , 1] + data[5 , 2] + data[5 , 3] + data[5 , 4] + i <> 65 then exit;
          if data[1 , 1] + data[2 , 2] + data[3 , 3] + data[4 , 4] + i <> 65 then exit;
          if data[1 , 2] + data[2 , 3] + data[3 , 4] + data[4 , 5] + data[5 , 1] <> 65 then exit;
          if data[1 , 3] + data[2 , 4] + data[3 , 5] + data[4 , 1] + data[5 , 2] <> 65 then exit;
          if data[1 , 4] + data[2 , 5] + data[3 , 1] + data[4 , 2] + data[5 , 3] <> 65 then exit;
          if data[1 , 5] + data[2 , 1] + data[3 , 2] + data[4 , 3] + data[5 , 4] <> 65 then exit;

          if data[1 , 5] + data[2 , 4] + data[3 , 3] + data[4 , 2] + data[5 , 1] <> 65 then exit;
          if data[1 , 4] + data[2 , 3] + data[3 , 2] + data[4 , 1] + i <> 65 then exit;
          if data[1 , 3] + data[2 , 2] + data[3 , 1] + data[4 , 5] + data[5 , 4] <> 65 then exit;
          if data[1 , 2] + data[2 , 1] + data[3 , 5] + data[4 , 4] + data[5 , 3] <> 65 then exit;
          if data[1 , 1] + data[2 , 5] + data[3 , 4] + data[4 , 3] + data[5 , 2] <> 65 then exit;
          inc(answer);
          writeln(answer);
      end;
end;

procedure dfs(x , y , last : longint);
var
    i          : longint;
begin
    if x = 5
      then check
      else for i := 1 to 25 do
             if (last > i) then
               if  (sum[y] > i) then
                 if  (not visited[i]) then
               begin
                   if (x = 1) and (y = 2) then
                     writeln(i);
                   data[x , y] := i;
                   dec(sum[y] , i);
                   visited[i] := true;
                   if x = 4 then
                     begin
                         if (sum[y] > 25) or (visited[sum[y]]) then
                           begin
                               data[x , y] := 0; visited[i] := true;
                               continue;
                           end;
                         visited[sum[y]] := true;
                         data[x + 1 , y] := sum[y];
                     end;
                   if y = 4
                     then begin
                              data[x , y + 1] := last - i;
                              if (last - i <= 25) and (not visited[last - i]) then
                                begin
                                    visited[last - i] := true;
                                    dfs(x + 1 , 1 , 65);
                                    visited[last - i] := false;
                                end;
                              data[x , y + 1] := 0;
                          end
                     else begin
                              dfs(x , y + 1 , last - i);
                          end;
                   if x = 4 then
                     begin
                         visited[sum[y]] := false;
                         data[x + 1 , y] := 0;
                     end;
                   inc(sum[y] , i);
                   data[x , y] := 0;
                   visited[i] := false;
               end;
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := 1 to 5 do sum[i] := 65;
    fillchar(visited , sizeof(visited) , 0);
    dfs(1 , 1 , 65);
end;

procedure out;
begin
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      writeln(answer);
    Close(OUTPUT);
end;

Begin
    work;
    out;
End.
