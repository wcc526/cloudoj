#include <stdio.h>

int min(int x, int y)
{
  return x < y ? x : y;
}

int max(int x, int y)
{
  return x > y ? x : y;
}

int main(void)
{
  int c, i, j, n;
  short p[170], s[1001];

  for (i = 1001; i--;)
    s[i] = 1;
  for (i = 2; i <= 32; ++i)
    if (s[i])
      for (j = i * i; j < 1001; j += i)
        s[j] = 0;

  for (c = n = 0, i = 1; i < 1001; ++i)
    if (s[i])
      s[i] = c = n, p[n++] = i;
    else
      s[i] = c;

  while (scanf("%d %d", &n, &c) == 2) {
    printf("%d %d:", n, c);
    i = max((s[n] >> 1) - c + 1, 0);
    n = min((s[n] >> 1) + c + (s[n] & 1), s[n] + 1);
    for (; i < n; ++i)
      printf(" %d", p[i]);
    putchar('\n');
    putchar('\n');
  }

  return 0;
}
