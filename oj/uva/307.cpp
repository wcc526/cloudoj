#include <cstdio>
#include <cstring>
#include <algorithm> 
using namespace std;
#define N 1010
int a[N]; 
bool vis[N]; 
int n;
int res; 
bool cmp(int a, int b){
     return a > b;
} 

bool dfs(int len, int cur, int cnt, int sum){
      if (len * cnt == res)return true; 
      for (int i = cur; i < n; i++){
           if (vis[i] || i  && !vis[i - 1] && a[i] == a[i - 1]) continue; 
           if (sum + a[i] == len){
                   vis[i] = true;
                   if (dfs(len, 0, cnt + 1, 0))return true; 
                   vis[i] = false; 
                   return false;
           }
           else if (sum + a[i] < len){
                   vis[i] = true;
                   if (dfs(len, i + 1, cnt, sum + a[i]))return true;
                   vis[i] = false;
                   if (sum == 0)return false;
           } 
      }
      return false;
} 
       
int main(){
    int ca = 1; 
   // FILE* fp = fopen("in.txt", "r"); 
    while (scanf( "%d", &n) != EOF && n){
          res = 0;
          for (int i = 0; i < n; i++){
               scanf( "%d", a + i);
               res += a[i];
          }
          sort(a, a + n, cmp); 
          int ans; 
          for (int i = a[0]; i <= res; i++)if (res % i == 0){
                  for (int j = 0; j < n; j++)vis[j] = false;
                  if (dfs(i, 0, 0, 0)){
                             ans = i;
                             break;
                  } 
          }
          printf("%d\n", ans);
    }
   //getchar();
    return 0;
} 
           
     
