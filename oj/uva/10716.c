#include <stdio.h>

int main()
{
  int c, f, n, o;
  char r[101], t, *u, *v, *w;

  scanf("%d\n", &c);
  while (c--) {
    scanf("%s%n\n", r, &n);
    for (u = r, v = u + --n, f = o = 0; u < v;) {
      for (w = v; u < w && *u != *w; --w);
      if (u == w)
        if (f)
          goto i;
        else
          f = 1, t = u[0], u[0] = u[1], u[1] = t, ++o;
      else
        for (f = 0, ++u, --v; w <= v; w[0] = w[1], ++w, ++o);
    }
    printf("%d\n", o);
    continue;
  i:
    puts("Impossible");
  }

  return 0;
}
