{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10401.in';
    OutFile    = 'p10401.out';
    Limit      = 15;

Type
    Tdata      = array[1..Limit] of longint;
    Topt       = array[1..Limit , 1..Limit] of int64;

Var
    data       : Tdata;
    opt        : Topt;
    N          : longint;
    answer     : int64;

procedure init;
var
    ch         : char;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , 0);
    N := 0;
    while not eoln do
      begin
          read(ch);
          if ch = ' ' then break;
          inc(N);
          case ch of
            '1'..'9'          : data[N] := ord(ch) - ord('0');
            'A'..'F'          : data[N] := ord(ch) - ord('A') + 10;
          else
            data[N] := 0;
          end;
      end;
    readln;
end;

procedure work;
var
    i , j , k  : longint;
begin
    for i := 1 to N do
      if (data[1] = 0) or (data[1] = i) then
        opt[1 , i] := 1;
    for i := 2 to N do
      for j := 1 to N do
        if (data[i] = 0) or (data[i] = j) then
          for k := 1 to N do
            if abs(j - k) > 1 then
              opt[i , j] := opt[i , j] + opt[i - 1 , k];
    answer := 0;
    for i := 1 to N do
      answer := answer + opt[N , i];
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            if N = 0 then break;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
