Const
    InFile     = 'p10440.in';
    OutFile    = 'p10440.out';
    Limit      = 1440;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    ans1 , ans2 ,
    tot , Cases ,
    N , M , T  : longint;

procedure init;
var
    i          : longint;
begin
    inc(Cases);
    readln(M , T , N);
    for i := 1 to N do read(data[i]);
end;

procedure check(start : longint);
var
    i , k      : longint;
begin
    i := 1;
    while i <= N do
      begin
          k := 0;
          while (k < M) and (i + k <= N) and (data[i + k] <= start) do inc(k);
          dec(k);
          inc(i , k + 1); start := start + T * 2;
      end;
    if start < ans1 then ans1 := start;
end;

procedure work;
var
    i          : longint;
begin
    ans1 := maxlongint;
    for i := 0 to T * 2 - 1 do
      check(i);
    ans2 := (N + M - 1) div M;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    readln(tot); Cases := 0;
    while cases < tot do
      begin
          init;
          work;
          writeln(ans1 - T , ' ' , ans2);
      end;
End.