//Izayoi bless me
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<stdlib.h>
#include<cctype>
#include<cmath>
#include<limits.h>
#include<iomanip>
#include<cstring>
#include<fstream>
#include<string>
#include<queue>
#include<stack>
#include<set>
#include<map>
#include<vector>
using namespace std;
const double pi = 4.0 * atan(1.0);
typedef signed long long LL;
#define clr(x) memset(x,0,sizeof(x))
#define clro(x) memset(x,-1,sizeof(x))
typedef pair<int,int> pii;
const int inf = 99999999;
const LL INF = 0x3f3f3f3f3f3f3f3fLL;
#define sf scanf
#define pf printf
const int maxn = 555;
const int maxm = 3333;
struct Edge{
	int v,nex,len;
}edges[maxm];
int tot;
int head[maxn],dis[maxn],in[maxn];
bool vis[maxn];
int n,m;
void init(){
	tot = 0;
	clro(head);
}
void addedge( int u, int v, int c){
	Edge e;
	e.v = v, e.len = c;
	e.nex = head[u];
	edges[tot] = e;
	head[u] = tot++;
}
void input(){
	int a,b,c;
	for( int i=0; i<m; ++i){
		sf("%d%d%d",&a,&b,&c);
		addedge(a,b,c);
	}
}
bool spfa(int z){
	clr(in); clr(dis); clr(vis);
	queue<int>que;
	for( int i=1; i<=n; ++i){ 
		que.push(i);
	}
	int x;
	while( !que.empty() ){
		x = que.front(); que.pop();
		vis[x] = false;
		for( int i=head[x]; ~i; i = edges[i].nex){
			Edge &e = edges[i];
			if( dis[e.v] > dis[x] + e.len-z ){
				dis[e.v] = dis[x] + e.len-z;
				if( !vis[e.v] ){
					vis[e.v] = true;
					if( ++in[e.v] > n ) return true;
					que.push(e.v);
				}
			}
		}
	}
	return false;
}
void doit(){
	if( spfa(0) ){
		puts("No Solution");
		return;
	}
	if( !spfa(10001) ){
		puts("Infinite");
		return;
	}
	int l=1,r=10000,mid;
	while( l < r ){
		mid = (l+r)>>1;
		if( spfa(mid) ) r = mid;
		else l = mid+1;
	}
	--l;
	if( l == 0 ){
		puts("No Solution");
		return;
	}
	pf("%d\n",l);
	
}
int main(){
	while( sf("%d%d",&n,&m) == 2 ){
		init();
		input();
		doit();
	}
	return 0;
}