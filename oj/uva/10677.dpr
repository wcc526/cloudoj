{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10677.in';
    OutFile    = 'p10677.out';

Var
    cases , 
    B1 , B2 ,
    r1 , r2 ,
    answer     : longint;

procedure init;
begin
    dec(Cases);
    readln(B1 , B2 , r1 , r2);
end;

procedure work;
var
    power , tmp ,
    i , num    : longint;
begin
    answer := -1;
    for i := r2 - 1 downto r1 + 1 do
      begin
          tmp := i; power := 1; num := 0;
          while tmp > 0 do
            begin
                inc(num , tmp mod b1 * power);
                tmp := tmp div b1;
                if tmp <> 0 then
                  power := power * b2;
            end;
          if num mod i = 0 then
            begin
                answer := i; break;
            end;
      end;
end;

procedure out;
begin
    if answer <> -1
      then writeln(answer)
      else writeln('Non-existent.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
