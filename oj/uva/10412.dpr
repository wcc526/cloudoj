{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10412.in';
    OutFile    = 'p10412.out';
    Limit      = 1000;
    LimitH     = 20;

Type
    Tdata      = array[1..Limit] of
                   record
                       h      : longint;
                       data   : array[1..LimitH] of longint;
                   end;

Var
    data       : Tdata;
    answer , 
    cases , N ,  
    M , LEN    : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    fillchar(data , sizeof(data) , 0);
    readln(N , M , LEN);
    LEN := sqr(LEN);
    for i := 1 to N do
      with data[i] do
        begin
            read(h);
            for j := 1 to h do
              read(data[j]);
            readln;
        end;
end;

procedure work;
var
    i , j , k ,
    p , st , ed ,
    p1 , p2 ,
    min        : longint;
    ok         : boolean;
begin
    answer := 0;
    for i := 2 to N do
      begin
          min := M;
          for j := 1 to data[i].h do
            for k := 1 to data[i - 1].h do
              if sqr(M - data[i].data[j] - data[i - 1].data[k]) + sqr(j - k) <= LEN then
                begin
                    p1 := data[i - 1].data[k]; p2 := M - data[i].data[j];
                    ok := true;
                    if j < k
                      then begin st := j + 1; ed := k - 1; end
                      else begin st := k + 1; ed := j - 1; end;
                    for p := st to ed do
                      begin
                          if (p <= data[i - 1].h) and (data[i - 1].data[p] * abs(j - k) >= abs((p - k) * p2 + (j - p) * p1)) then
                            begin ok := false; break; end;
                          if (p <= data[i].h) and ((M - data[i].data[p]) * abs(j - k) <= abs((p - k) * p2 + (j - p) * p1)) then
                            begin ok := false; break; end;
                      end;
                    if ok then
                      if data[i].data[j] + data[i - 1].data[k] < min then
                        min := data[i].data[j] + data[i - 1].data[k];
                end;
          inc(answer , min);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0  do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
