Type
    lint       = object
                     len      : longint;
                     data     : array[1..100] of int64;
                     procedure init;
                     procedure times(num : int64);
                     procedure add(num : lint);
                     procedure divide(num : int64);
                     procedure print;
                 end;
Var
    cases      : longint;
    N          : int64;
    res , tmp  : lint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.times(num : int64);
var
    i          : longint;
    jw , tmp   : int64;
begin
    i := 1; jw := 0;
    while (i <= Len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.add(num : lint);
var
    i          : longint;
    jw , tmp   : int64;
begin
    i := 1; jw := 0;
    while (i <= Len) or (i <= num.len) or (jw <> 0) do
      begin
          tmp := data[i] + num.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.divide(num : int64);
var
    i          : longint;
    last       : int64;
begin
    last := 0;
    for i := len downto 1 do
      begin
          last := last * 10 + data[i];
          data[i] := last div num;
          last := last mod num;
      end;
    while (len > 0) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          readln(N);
          if N = 0 then writeln(1);
          if N = 1 then writeln(1);
          if N = 2 then writeln(2);
          if N = 3 then writeln(4);
          if N >= 4 then
            begin
                tmp.init; tmp.data[1] := 1; res.init; res.data[1] := 1;
                res.times(N); res.times(N - 1); res.times(N - 2); res.times(N - 3);
                res.divide(24);
                tmp.times(N); tmp.times(N - 1); tmp.divide(2);
                res.add(tmp);
                tmp.init; tmp.data[1] := 1;
                res.add(tmp);
                res.print;
                writeln;
            end;
      end;
End.