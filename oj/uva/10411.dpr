{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10411.in';
    OutFile    = 'p10411.out';
    Limit      = 130;
    put        : array[1..19 , 1..4] of longint
               = ((4 , 0 , 0 , 0) , (1 , 1 , 1 , 1) , (2 , 2 , 0 , 0) ,
                  (1 , 2 , 1 , 0) , (1 , 2 , 1 , 0) , (2 , 2 , 0 , 0) ,
                  (2 , 2 , 0 , 0) , (1 , 2 , 1 , 0) , (1 , 2 , 1 , 0) ,
                  (1 , 3 , 0 , 0) , (3 , 1 , 0 , 0) , (2 , 1 , 1 , 0) ,
                  (1 , 1 , 2 , 0) , (2 , 1 , 1 , 0) , (1 , 1 , 2 , 0) ,
                  (3 , 1 , 0 , 0) , (1 , 3 , 0 , 0) , (3 , 1 , 0 , 0) ,
                  (1 , 3 , 0 , 0));
    shape      : array[1..19 , 1..3] of longint
               = ((0 , 0 , 0) , (0 , 0 , 0) , (0 , 0 , 0) , (-1 , -1 , 0) ,
                  (0 , 1 , 0) , (-1 , 0 , 0) , (1 , 0 , 0) , (0 , 0 , 0) ,
                  (-1 , 0 , 0) , (-1 , 0 , 0) , (1 , 0 , 0) , (1 , 1 , 0) ,
                  (0 , -1 , 0) , (0 , 0 , 0) , (0 , 0 , 0) , (2 , 0 , 0) ,
                  (-2 , 0 , 0) , (0 , 0 , 0) , (0 , 0 , 0));

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    N , cases  : longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    read(N);
    for i := 1 to N do read(data[i]);
end;

procedure getshape_start(start , stop : longint; var shapenum : longint);
var
    i          : integer;
begin
    shapenum := 0;
    for i := start to stop - 1 do
      inc(shapenum , abs(data[i] - data[i + 1]));
end;

procedure getshape(start , stop , where , block : longint; var shapenum : longint);
var
    i          : integer;
begin
    for i := 1 to 5 do
      if (i < 5) and (put[block , i] <> 0) then
        if where + i - 1 <> start then
          begin
              dec(shapenum , abs(data[where + i - 1] - data[where + i - 2]));
              if i = 1 then
                inc(shapenum , abs(data[where + i - 1] + put[block , i] - data[where + i - 2]))
              else
                inc(shapenum , abs(data[where + i - 1] + put[block , i] - data[where + i - 2] - put[block , i - 1]));
          end
        else
      else
        begin
            if i + where - 2 < stop then
              begin
                  dec(shapenum , abs(data[where + i - 2] - data[where + i - 1]));
                  inc(shapenum , abs(data[where + i - 2] + put[block , i - 1] - data[where + i - 1]));
              end;
            break;
        end;
end;

function canput(start , stop , where , block : integer) : boolean;
var
    i          : integer;
begin
    canput := false;
    for i := 2 to 4 do
      if (put[block , i] <> 0) and
         ((i + where - 1 > stop) or (data[i + where - 1] <> data[where] + shape[block , i - 1])) then
        exit;
    canput := true;
end;

procedure proc_put(where , block : integer);
var
    i          : integer;
begin
    for i := 1 to 4 do
      if put[block , i] > 0 then
        inc(data[where + i - 1] , put[block , i]);
end;

procedure fill(start , stop : integer);
var
    maxwhere ,
    maxblock ,
    minshape ,
    nowshape ,
    shapenum ,
    i , j      : longint;
begin
    getshape_start(start , stop , shapenum);
    while shapenum > 0 do
      begin
          maxwhere := 0;
          for i := start to stop do
            for j := 1 to 19 do
              if canput(start , stop , i , j) then
                begin
                    nowshape := shapenum;
                    getshape(start , stop , i , j , nowshape);
                    if (maxwhere = 0) or (minshape > nowshape) then
                      begin
                          maxwhere := i;
                          maxblock := j;
                          minshape := nowshape;
                      end;
                end;
          proc_put(maxwhere , maxblock);
          writeln(maxblock , ' ' , maxwhere);
          shapenum := minshape;
      end;
end;

procedure getmod4(modnum , col : longint);
begin
    if modnum <> 0 then
      begin
          while (data[col] - data[col + 1] < -2) or (data[col] - data[col + 1] > 1) do
            if data[col] < data[col + 1] then
              begin
                  writeln(1 , ' ' , col);
                  inc(data[col] , 4);
              end
            else
              begin
                  writeln(1 , ' ' , col + 1);
                  inc(data[col + 1] , 4);
              end;
          case data[col] - data[col + 1] of
            1   : case modnum of
                    1 : begin
                            writeln(10 , ' ' , col);
                            inc(data[col] , 1); inc(data[col + 1] , 3);
                            getmod4(2 , col);
                        end;
                    2 : begin writeln(6 , ' ' , col); inc(data[col] , 2); inc(data[col + 1] , 2); end;
                    3 : begin writeln(10 , ' ' , col); inc(data[col] , 1); inc(data[col + 1] , 3); end;
                  end;
            0   : case modnum of
                    1 : begin writeln(18 , ' ' , col); inc(data[col] , 3); inc(data[col + 1] , 1); end;
                    2 : begin writeln(3 , ' ' , col); inc(data[col] , 2); inc(data[col + 1] , 2); end;
                    3 : begin writeln(19 , ' ' , col); inc(data[col] , 1); inc(data[col + 1] , 3); end;
                  end;
            -1  : case modnum of
                    1 : begin writeln(11 , ' ' , col); inc(data[col] , 3); inc(data[col + 1] , 1); end;
                    2 : begin writeln(7 , ' ' , col); inc(data[col] , 2); inc(data[col + 1] , 2); end;
                    3 : begin writeln(7 , ' ' , col); inc(data[col] , 2); inc(data[col + 1] , 2); getmod4(1 , col); end;
                  end;
            -2  : begin
                      writeln(16 , ' ' , col);
                      inc(data[col] , 3); inc(data[col + 1] , 1);
                      getmod4(modnum - 1 , col);
                  end;
          end;
      end;
end;

procedure workout;
var
    i , sum , j ,
    start , max: integer;
begin
    max := 0;
    for i := 1 to (N - 1) div 4 do
      begin
          start := (i - 1) * 4 + 1;
          sum := data[start] + data[start + 1] + data[start + 2] + data[start + 3];
          getmod4(sum mod 4 , i * 4);
          fill(start , i * 4);
          if data[start] > max then
            max := data[start];
      end;
    for i := 1 to (N - 1) div 4 do
      begin
          for j := 1 to max - data[(i - 1) * 4 + 1] do
            writeln(2 , ' ' , (i - 1) * 4 + 1);
          data[(i - 1) * 4 + 1] := max;
          data[(i - 1) * 4 + 2] := max;
          data[(i - 1) * 4 + 3] := max;
          data[(i - 1) * 4 + 4] := max;
      end;
    fill(1 , N);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout;
            writeln(-1 , ' ' , -1);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
