Const
    InFile     = 'p10266.in';
    Limit      = 100;

Type
    Tans       = array[1..Limit , 1..Limit] of longint;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;
    Tpoint     = record
                     x , y , height          : longint;
                 end;
    Tconnect   = array[1..Limit * Limit] of Tpoint;
    Tdata      = array[1..Limit * Limit] of
                   record
                       tot    : longint;
                       data   : ^Tconnect;
                   end;

Var
    ans        : Tans;
    data       : Tdata;
    visited    : Tvisited;
    cases ,
    x0 , y0 ,
    N , M , K  : longint;

procedure init;
var
    connect    : Tconnect;
    i          : longint;
begin
    dec(Cases);
    readln(N , M , x0 , y0);
    K := 0;
    while not eoln do
      begin
          inc(K); data[K].tot := 0;
          while not eoln do
            begin
                inc(data[K].tot);
                with connect[data[K].tot] do
                  read(x , y , height);
            end;
          readln;
          GetMem(data[K].data , data[K].tot * sizeof(Tpoint));
          for i := 1 to data[K].tot do
            data[K].data[i] := connect[i];
      end;
end;

function dfs(x , y : longint) : boolean;
var
    x1 , y1 ,
    i , p , j  : longint;
begin
    for i := 1 to K do
      begin
          j := 1;
          while (j <= data[i].tot) and ((x <> data[i].data[j].x) or (y <> data[i].data[j].y)) do
            inc(j);
          if j <= data[i].tot then
            for p := 1 to data[i].tot do
              begin
                  x1 := data[i].data[p].x; y1 := data[i].data[p].y;
                  if not visited[x1 , y1]
                    then begin
                             ans[x1 , y1] := ans[x , y] - data[i].data[j].height + data[i].data[p].height;
                             visited[x1 , y1] := true;
                             if not dfs(x1 , y1) then exit(false);
                         end
                    else if ans[x1 , y1] <> ans[x , y] - data[i].data[j].height + data[i].data[p].height
                           then exit(false);
              end;
      end;
    exit(true);
end;

procedure workout;
var
    num , tmp ,
    i , j      : longint;
    find ,
    changed    : boolean;
begin
    fillchar(ans , sizeof(ans) , 0);
    fillchar(visited , sizeof(visited) , 0);
    ans[x0 , y0] := 0; visited[x0 , y0] := true;
    repeat
      changed := false;
      for i := 1 to K do
        begin
            find := false;
            for j := 1 to data[i].tot do
              if visited[data[i].data[j].x , data[i].data[j].y] then
                begin
                    tmp := ans[data[i].data[j].x , data[i].data[j].y] - data[i].data[j].height;
                    if find
                      then if num <> tmp
                              then begin
                                       writeln('conflicting measurements');
                                       exit;
                                   end
                              else
                      else begin num := tmp; find := true; end;
                end;
            if find then
              for j := 1 to data[i].tot do
                if not visited[data[i].data[j].x , data[i].data[j].y] then
                  begin
                      changed := true;
                      ans[data[i].data[j].x , data[i].data[j].y] := num + data[i].data[j].height;
                      visited[data[i].data[j].x , data[i].data[j].y] := true;
                  end;
        end;

    until not changed;

    for i := 1 to N do
      for j := 1 to M do
        if not visited[i , j] then
          begin
              writeln('the lack of measurements');
              exit;
          end;
    for i := 1 to N do
      for j := 1 to M do
        begin
            write(ans[i , j]);
            if j <> M then write(' ') else writeln;
        end;
    for i := 1 to K do
      dispose(data[i].data);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout;
            if cases <> 0 then writeln;
        end;
//    Close(INPUT);
End.