/**
 * UVa 455 Periodic Strings
 * Author: chchwy
 * Last Modified: 2010.04.22
 */
#include<iostream>

int findPeriod(char* buf){

    int len = strlen(buf);
    if (len==1) return 1; //special case

    int i=0,j=1;
    while(buf[j]!=NULL) { //reach the end of string
        if(buf[i]==buf[j])
            ++i, ++j;
        else
            ++j;
    }
    return ( len%(j-i)==0 )? j-i:len;
}

int main(){
    #ifndef ONLINE_JUDGE
    freopen("455.in","r",stdin);
    #endif

    int caseNum;
    scanf("%d ", &caseNum);

    while( caseNum-- ){
        char buf[256];
        scanf("%s", buf);

        printf("%d\n", findPeriod(buf) );
        if( caseNum > 0 ) putchar('\n');
    }
    return 0;
}
