{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10637.in';
    OutFile    = 'p10637.out';
    Limit      = 100;

Type
    Tgcd       = array[1..Limit , 1..Limit] of longint;
    Tpath      = array[1..Limit] of longint;

Var
    gcd        : Tgcd;
    path       : Tpath;
    N , M ,
    nowCase ,
    cases      : longint;

procedure Get_Gcd;
var
    i , j      : longint;
begin
    for i := 1 to Limit do
      for j := 1 to Limit do
        begin
            gcd[i , j] := i;
            while (i mod gcd[i , j] <> 0) or (j mod gcd[i , j] <> 0) do dec(gcd[i , j]);
        end;
end;

procedure dfs_print(step , last , start : longint);
var
    i , j      : longint;
    ok         : boolean;
begin
    if (M - step + 1) * start > last then exit;
    if step > M
      then begin
               if last <> 0 then exit; 
               for i := 1 to M - 1 do write(path[i] , ' ');
               writeln(path[M]);
           end
      else begin
               for i := start to last div (M - step + 1) do
                 begin
                     ok := true;
                     for j := 1 to step - 1 do
                       if gcd[path[j] , i] <> 1 then
                         begin
                             ok := false;
                             break;
                         end;
                     if ok then
                       begin
                           path[step] := i;
                           dfs_print(step + 1 , last - i , i);
                       end;
                 end;
           end;
end;

Begin
    Get_Gcd;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      nowCase := 1;
      while nowCase <= cases do
        begin
            readln(N , M);
            writeln('Case ' , nowCase , ':');
            dfs_print(1 , N , 1);
            inc(nowCase)
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
