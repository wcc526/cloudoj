#include <cmath>
#include <cstdio>
#include <map>

using namespace std;

int s[100];

int find(int i)
{
  if (s[i] == i)
    return i;
  return s[i] = find(s[i]);
}

int main()
{
  int c, i, j, n, t, u, v;
  double d, p[100][2];
  map<double, pair<int, int> > e;

  for (scanf("%d", &t); t--;) {
    for (e.clear(), scanf("%d", &n), i = n; i--; s[i] = i)
      for (scanf("%lf%lf", p[i], p[i] + 1), j = i; ++j < n;)
        e.insert(make_pair(sqrt((p[i][0] - p[j][0]) * (p[i][0] - p[j][0]) +
                                (p[i][1] - p[j][1]) * (p[i][1] - p[j][1])),
                           make_pair(i, j)));
    map<double, pair<int, int> >::iterator k = e.begin();
    for (c = n - 1, d = 0; c; ++k)
      if ((u = find(k->second.first)) ^ (v = find(k->second.second)))
        --c, s[v] = u, d += k->first;
    printf("%.2lf\n", d);
  }

  return 0;
}
