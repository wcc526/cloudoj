Const
    InFile     = 'p10717.in';

Var
    data       : array[1..100] of longint;
    num , p ,
    N , T , i , j ,
    L , p1 , p2 ,
    p3 , p4 ,
    res1 , res2 ,
    min , max  : longint;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then gcd := b
      else gcd := gcd(b mod a , a);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    readln(N , T);
    while N + T <> 0 do
      begin
          for i := 1 to N do read(data[i]);
          for i := 1 to T do
            begin
                min := maxlongint; max := 0;
                readln(L);
                for p1 := 1 to N do
                  for p2 := p1 + 1 to N do
                    for p3 := p2 + 1 to N do
                      for p4 := p3 + 1 to N do
                        begin
                            num := data[p1] div gcd(data[p1] , data[p2]) * data[p2];
                            num := num div gcd(num , data[p3]) * data[p3];
                            num := num div gcd(num , data[p4]) * data[p4];
                            res1 := L div num * num;
                            res2 := (L + num - 1) div num * num;
                            if res1 > max then max := res1;
                            if res2 < min then min := res2;
                        end;
                writeln(max , ' ' , min);
            end;
          readln(N , T);
      end;
End.