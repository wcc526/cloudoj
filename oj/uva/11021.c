#include <stdio.h>

int main()
{
  int c = 0, i, k, m, n, t;
  long double p[1000], x, y;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %d %d", &n, &k, &m);
    for (i = 0; i < n; ++i)
      scanf("%Lf", p + i);
    if (m == 0) {
      y = k == 0 ? 1 : 0;
    } else {
      for (x = p[0]; --m; x = p[0] + y)
        for (y = 0, i = n; --i;)
          y = (p[i] + y) * x;
      for (y = 1; k; k >>= 1, x *= x)
        if (k & 1)
          y *= x;
    }
    printf("Case #%d: %.7Lf\n", ++c, y);
  }

  return 0;
}
