
Var
    N          : longint;

function reverse(num : longint) : longint;
var
    res        : longint;
begin
    res := 0;
    while num <> 0 do
      begin
          res := res * 10 + num mod 10;
          num := num div 10;
      end;
    exit(res);
end;

function check(num : longint) : boolean;
var
    i          : longint;
begin
    if num < 2
      then exit(false)
      else for i := 2 to trunc(sqrt(num)) do
             if num mod i = 0
               then exit(false);
    exit(true);
end;

Begin
    while not eof do
      begin
          readln(N);
          if check(N)
            then if (reverse(N) <> N) and check(reverse(N))
                   then writeln(N , ' is emirp.')
                   else writeln(N , ' is prime.')
            else writeln(N , ' is not prime.');
      end;
End.