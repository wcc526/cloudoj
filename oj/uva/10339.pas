Var
    K , time ,
    x1 , x2    : extended;
    H , T      : longint;

Begin
    while not eof do
      begin
          readln(x1 , x2);
          k := 43200.0 * 86400 / abs(x1 - x2);
          time := int((1 - x1 / 86400) * K / 60 + 5e-1 + 1e-7);
          time := time - int(time / 720) * 720;
          H := trunc(time / 60); T := trunc(time) - H * 60;
          if H = 0 then H := 12;
          writeln(x1 : 0 : 0 , ' ' , x2 : 0 : 0 , ' ' , H div 10 , H mod 10 , ':' , T div 10 , T mod 10);
      end;
End.