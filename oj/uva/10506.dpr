{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10506.in';
    OutFile    = 'p10506.out';
    Limit      = 65536;
    LimitN     = 10;      

Type
    Tdata      = array[0..Limit , 0..LimitN] of boolean;
    Tdegree    = array[0..Limit] of longint;
    Tpath      = array[1..Limit * 2] of
                   record
                       p , next , choose     : longint;
                   end;

Var
     data      : Tdata;
     degree    : Tdegree;
     path      : Tpath;
     N , M , tot , 
     sumP      : longint;

function init : boolean;
begin
    if eof
      then init := false
      else begin
               readln(M , N);
               init := true;
               fillchar(path , sizeof(path) , 0);
               fillchar(data , sizeof(data) , 1);
               fillchar(degree , sizeof(degree) , 0);
               tot := 0;
           end;
end;

procedure work;
var
    p , tp , np ,
    i ,
    tmptot     : longint;
begin
    tot := 2; path[1].p := 0; path[1].next := 2; path[2].p := 0; path[2].next := 0;
    fillchar(degree , sizeof(degree) , 0);
    sumP := 1;
    for p := 1 to M - 1 do sumP := sumP * N;
    for p := 0 to sumP - 1 do degree[p] := N;
    p := 1;
    while p <> 0 do
      begin
          tp := path[p].p;
          while degree[tp] <> 0 do
            begin
                np := tp; tmptot := tot;
                repeat
                  dec(degree[np]);
                  for i := 0 to N - 1 do
                    if data[np , i] then
                      begin
                          data[np , i] := false;
                          np := (np * N + i) mod sumP;
                          inc(tot); path[tot].p := np; path[tot].next := tot + 1;
                          path[tot].choose := i;
                          break;
                      end;
                until np = tp;
                path[tot].next := path[p].next;
                path[p].next := tmptot + 1;
            end;
          p := path[p].next;
      end;
end;

procedure out;
var
    i , p      : longint;
begin
    p := path[1].next;
    for i := 1 to sumP * N do
      begin
          write(path[p].choose);
          p := path[p].next;
      end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
