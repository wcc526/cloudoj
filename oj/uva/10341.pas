Var
    p , q , r , s ,
    t , u , mid ,
    start , stop ,
    tmp        : double;
    i          : longint;

function calc(x : double) : double;
begin
    calc := p * exp(-x) + q * sin(x) + r * cos(x) + s * sin(x) / cos(x) + t * x * x + u;
end;

function zero(num : double) : boolean;
begin
    exit(abs(num) <= 1e-8);
end;

Begin
    while not eof do
      begin
          readln(p , q , r , s , t , u);
          if zero(calc(0)) then
            begin writeln(0.0000 : 0 : 4); continue; end;
          if zero(calc(1)) then
            begin writeln(1.0000 : 0 : 4); continue; end;
          start := 0; stop := 1;
          if calc(0) * calc(1) > 0 then
            begin writeln('No solution'); continue; end;
          for i := 1 to 100 do
            begin
                mid := (start + stop) / 2;
                tmp := calc(mid);
                if tmp < 0
                  then stop := mid
                  else start := mid;
            end;
          writeln(mid : 0 : 4);
      end;
End.