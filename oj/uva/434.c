#include <stdio.h>

int main()
{
  int c, i, j, m, n, t;
  short f[8], r[8], x[9], y[9];

  scanf("%d", &c);
  while (c--) {
    scanf("%d", &n);
    for (i = 9; i--;)
      x[i] = y[i] = 0;
    for (i = n; i--;)
      scanf("%hd", &f[i]), ++x[f[i]];
    for (i = n; i--;)
      scanf("%hd", &r[i]), ++y[r[i]];
    for (t = 0, i = 9; i--;)
      t += i * (x[i] > y[i] ? x[i] : y[i]);
    for (m = 0, i = n; i--;)
      for (j = n; j--;)
        m += f[i] < r[j] ? f[i] : r[j];
    printf("Matty needs at least %hd blocks, and can add at most %hd extra blocks.\n", t, m - t);
  }

  return 0;
}
