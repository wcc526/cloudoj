Const
    InFile     = 'p10316.in';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of
                   record
                       a , b ,
                       x , y , z             : double;
                   end;
    Tbest      = array[1..Limit] of double;

Var
    data       : Tdata;
    best       : Tbest;
    N , min    : longint;

procedure init;
var
    i          : longint;
    a , b      : extended;
begin
    readln(N);
    for i := 1 to N do
      begin
          readln(a , b);
          data[i].a := a; data[i].b := b;
          a := a / 180 * pi; b := b / 180 * pi;
          data[i].x := cos(a) * cos(b);
          data[i].y := cos(a) * sin(b);
          data[i].z := sin(a);
      end;
end;

procedure work;
var
    i , j      : longint;
    tmp        : double;
begin
    fillchar(best , sizeof(best) , 0);
    for i := 1 to N do
      for j := 1 to N do
        begin
            tmp := sqr(data[i].x - data[j].x) + sqr(data[i].y - data[j].y) + sqr(data[i].z - data[j].z);
            if best[i] < tmp then best[i] := tmp;
        end;
    min := N;
    for i := N - 1 downto 1 do
      if best[i] < best[min] then
        min := i;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            writeln(data[min].a : 0 : 2 , ' ' , data[min].b : 0 : 2);
        end;
//    Close(INPUT);
End.