Const
    Minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tcircle    = record
                     O        : Tpoint;
                     R        : double;
                 end;

Var
    tmp ,
    L , W , R  : double;
    cross1 , cross2 ,
    midp , p1 ,
    p2         : Tpoint;
    C1 , C2    : Tcircle;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    exit(sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)));
end;

procedure Rotate(var p : Tpoint; sinA , cosA : double);
var
    x , y      : double;
begin
    y := sinA * p.x + cosA * p.y;
    x := cosA * p.x - sinA * p.y;
    p.x := x; p.y := y;
end;

function Get_Crossing(C1 , C2 : Tcircle; var cross1 , cross2 : Tpoint) : boolean;
var
    vector     : Tpoint;
    sinA , cosA ,
    L          : double;
begin
    Get_Crossing := true;
    L := dist(C1.O , C2.O);
    cross1.x := (L * L + sqr(C1.R) - sqr(C2.R)) / 2 / L;
    cross1.y := sqrt(sqr(C1.R) - sqr(cross1.x));
    cross2.x := cross1.x; cross2.y := -cross1.y;
    sinA := (C2.O.y - C1.O.y) / L;
    cosA := (C2.O.x - C1.O.x) / L;
    Rotate(cross1 , sinA , cosA);
    Rotate(cross2 , sinA , cosA);
    cross1.x := cross1.x + C1.O.x; cross1.y := cross1.y + C1.O.y;
    cross2.x := cross2.x + C1.O.x; cross2.y := cross2.y + C1.O.y;
end;

function tri_area(p1 , p2 , p3 : Tpoint) : double;
begin
    p2.x := p2.x - p1.x; p3.x := p3.x - p1.x;
    p2.y := p2.y - p1.y; p3.y := p3.y - p1.y;
    tri_area := abs(p2.x * p3.y - p2.y * p3.x) / 2;
end;

function Get_Area(C : Tcircle; p1 , p2 , p3 : Tpoint) : double;
var
    tmp , t2 , t3 ,
    L , alpha  : double;
begin
    L := dist(p2 , p3) / 2;
    alpha := arctan(L / sqrt(sqr(C.R) - sqr(L)));
    tmp := Tri_area(p1 , p2 , p3);
    t2 := pi * sqr(C.R) / pi * alpha;
    t3 := Tri_area(C.O , p2 , p3);
    Get_Area := tmp + t2 - t3;
end;

Begin
    while not eof do
      begin
          readln(L , W , R);
          if zero(L) or zero(W) then
            begin writeln(pi * R * R : 0 : 10); continue; end;
          if (R <= L - minimum) and (R <= W - minimum) then
            begin writeln(pi * R * R / 4 * 3 : 0 : 10); continue; end;
          if R <= L - minimum then
            begin writeln(pi * R * R / 4 * 3 + pi * sqr(R - W) / 4 : 0 : 10); continue; end;
          if R <= W - minimum then
            begin writeln(pi * R * R / 4 * 3 + pi * sqr(R - L) / 4 : 0 : 10); continue; end;
          C1.O.x := W; C1.O.y := 0; C1.R := R - W;
          C2.O.x := 0; C2.O.y := L; C2.R := R - L;
          if (L + W < R) and Get_Crossing(C1 , C2 , cross1 , cross2)
            then begin
                     if (cross1.x <= W - minimum) or (cross1.y <= L - minimum) then
                       cross1 := cross2;
                     midp.x := W; midp.y := L;
                     p1.x := W; p1.y := C1.R;
                     p2.x := C2.R; p2.y := L;
                     tmp := Get_Area(C1 , midp , p1 , cross1) + Get_Area(C2 , midp , cross2 , p2);
                     writeln(pi * R * R / 4 * 3 + pi * sqr(C1.R) / 4 + pi * sqr(C2.R) / 4 - tmp : 0 : 10);
                 end
            else writeln(pi * R * R / 4 * 3 + pi * sqr(C1.R) / 4 + pi * sqr(C2.R) / 4 : 0 : 10);
      end;
End.