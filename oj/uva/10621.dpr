{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10621.in';
    OutFile    = 'p10621.out';
    Limit      = 30;
    LimitNum   = Limit * Limit;
    maximum    = 100000;
    dirx       : array[1..5] of longint = (-1 , 1 , 0 , 0 , 0);
    diry       : array[1..5] of longint = (0 , 0 , -1 , 1 , 0);

Type
    Tmap       = array[1..Limit , 1..Limit] of char;
    Tshortest  = array[1..LimitNum * LimitNum] of
                   record
                       num , father          : longint;
                   end;
    Theap      = object
                     tot                     : longint;
                     index , data            : array[1..LimitNum * LimitNum] of longint;
                     procedure init;
                     procedure swap(p1 , p2 : longint);
                     procedure insert(num : longint);
                     procedure del_top;
                     procedure shift(p : longint);
                     procedure down(p : longint);
                 end;
    Tpath      = array[1..LimitNum * LimitNum] of char;

Var
    map        : Tmap;
    shortest   : Tshortest;
    heap       : Theap;
    path1 ,
    path2      : Tpath;
    Base ,
    hnum , snum ,
    N , source ,
    target     : longint;
    first      : boolean;

procedure Theap.init;
begin
    tot := 0;
end;

procedure Theap.swap(p1 , p2 : longint);
var
    tmp        : longint;
begin
    tmp := data[p1]; data[p1] := data[p2]; data[p2] := tmp;
    index[data[p1]] := p1; index[data[p2]] := p2;
end;

procedure Theap.insert(num : longint);
begin
    inc(tot);
    data[tot] := num; index[num] := tot;
    shift(tot);
end;

procedure Theap.del_top;
begin
    swap(1 , tot);
    dec(tot);
    if tot > 0 then down(1);
end;

procedure Theap.shift(p : longint);
begin
    while p <> 1 do
      if shortest[data[p]].num > shortest[data[p div 2]].num
        then begin
                 swap(p , p div 2);
                 p := p div 2;
             end
        else break;
end;

procedure Theap.down(p : longint);
var
    nextp      : longint;
begin
    while p * 2 <= tot do
      begin
          nextp := p;
          if shortest[data[p * 2]].num > shortest[data[nextp]].num then nextp := p * 2;
          if p * 2 < tot then
            if shortest[data[p * 2 + 1]].num > shortest[data[nextp]].num then
              nextp := p * 2 + 1;
          if nextp = p then break;
          swap(p , nextp);
          p := nextp;
      end;
end;

function init : boolean;
var
    i , j ,
    p , q , 
    x1 , y1 ,
    x2 , y2    : longint;
begin
    readln(N);
    x1 := 0; y1 := 0; x2 := 0; y2 := 0;
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(shortest , sizeof(shortest) , $FF);
               for i := 1 to N do
                 begin
                     for j := 1 to N do
                       begin
                           read(map[i , j]);
                           if map[i , j] = 'S'
                             then map[i , j] := 'h'
                             else if map[i , j] = 'h'
                                    then map[i , j] := 'S';
                           case map[i , j] of
                             'H'             : x1 := (i - 1) * N + j;
                             'h'             : x2 := (i - 1) * N + j;
                             'S'             : y1 := (i - 1) * N + j;
                             's'             : y2 := (i - 1) * N + j;
                           end;
                       end;
                     readln;
                 end;
               hnum := x2; snum := y2;
               Base := N * N;
               source := (x1 - 1) * Base + y1;
               target := (x2 - 1) * Base + y2;
               i := (x2 - 1) mod N; j := (x2 - 1) div N;
               p := (y2 - 1) mod N; q := (y2 - 1) div N;
               shortest[target].num := sqr(i - p) + sqr(j - q);
           end;
end;

procedure work;
var
    i , j ,
    p1 , p2 ,
    x1 , y1 ,
    x2 , y2 ,
    nx1 , ny1 ,
    nx2 , ny2 ,
    newstatus ,
    newstep ,
    open       : longint;
    b          : boolean;
begin
    heap.init; heap.insert(target);
    while heap.tot > 0 do
      begin
          if heap.data[1] = source then break;
          open := heap.data[1];
          heap.del_top;
          if open = source then break;
          p1 := (open - 1) div Base + 1;
          p2 := (open - 1) mod Base + 1;
          x1 := (p1 - 1) div N + 1; y1 := (p1 - 1) mod N + 1;
          x2 := (p2 - 1) div N + 1; y2 := (p2 - 1) mod N + 1;
          for i := 1 to 5 do
            if (i <> 5) or (p1 = hnum) then
              begin
                  nx1 := x1 + dirx[i]; ny1 := y1 + diry[i];
                  if (nx1 <= N) and (ny1 <= N) and (nx1 >= 1) and (ny1 >= 1) and ((map[nx1 , ny1] = '.') or (map[nx1 , ny1] = 'H') or (i = 5)) then
                  for j := 1 to 5 do
                    if (j <> 5) or (p2 = snum) then
                      begin
                          nx2 := x2 + dirx[j]; ny2 := y2 + diry[j];
                          if (nx2 <= N) and (ny2 <= N) and (nx2 >= 1) and (ny2 >= 1) and ((map[nx2 , ny2] = '.') or (map[nx2 , ny2] = 'S') or (j = 5)) then
                            begin
                                newstatus := ((nx1 - 1) * N + ny1 - 1) * Base + (nx2 - 1) * N + ny2;
                                newstep := sqr(nx1 - nx2) + sqr(ny1 - ny2);
                                if newstep > shortest[open].num then
                                  newstep := shortest[open].num;
                                if newstep > shortest[newstatus].num
                                  then begin
                                           b := shortest[newstatus].num = -1;
                                           shortest[newstatus].num := newstep;
                                           shortest[newstatus].father := open;
                                           if b then heap.insert(newstatus) else heap.shift(heap.index[newstatus]); 
                                       end;
                            end;
                      end;
              end;
      end;
end;

function go(p1 , p2 : longint) : char;
begin
    if p1 = p2
      then go := ' '
      else begin
               if (p1 - 1) mod N = (p2 - 1) mod N
                 then if p1 < p2
                        then go := 'S'
                        else go := 'N'
                 else if p1 < p2
                        then go := 'E'
                        else go := 'W';
           end;
end;

procedure out;
var
    i , 
    p , tot    : longint;
begin
    p := source;
    if not first then writeln;
    first := false;
    writeln(sqrt(shortest[p].num) : 0 : 2);
    tot := 0;
    while p <> target do
      begin
          inc(tot);
          path1[tot] := go((p - 1) div Base + 1 , (shortest[p].father - 1) div Base + 1);
          path2[tot] := go((p - 1) mod Base + 1 , (shortest[p].father - 1) mod Base + 1);
          p := shortest[p].father;
      end;
    for i := 1 to tot do if path1[i] <> ' ' then write(path1[i]);
    writeln;
    for i := 1 to tot do if path2[i] <> ' ' then write(path2[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      first := true;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
