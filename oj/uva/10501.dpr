{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
{$I+}
Const
    InFile     = 'p10501.in';
    OutFile    = 'p10501.out';
    Limit      = 22;
    dirx       : array[1..4] of integer = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of integer = (0 , -1 , 0 , 1);

Type
    Tdata      = array[0..Limit , 0..Limit] of char;
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tpath      = record
                     tot      : longint;
                     data     : array[1..Limit * Limit div 2] of
                                  record
                                      p , q                 : Tpoint;
                                  end;
                 end;
    Tshortest  = array[0..Limit , 0..Limit] of longint;
    Tvisited   = array[0..Limit , 0..Limit] of boolean;
    Tqueue     = array[1..Limit * Limit] of Tpoint;

Var
    data       : Tdata;
    path       : Tpath;
    shortest   : Tshortest;
    queue      : Tqueue;
    visited    : Tvisited;
    N , M ,
    open , closed
               : longint;
    answer     : boolean;

function init : boolean;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    if eof
      then init := false
      else begin
               init := true;
               readln(M , N);
               for i := 1 to N do
                 begin
                     for j := 1 to M do read(data[i , j]);
                     readln;
                 end;
           end;
end;

procedure bfs(x , y : longint);
type
    Tdirections
               = array[1..4] of Tpoint;               
var
    directions : Tdirections;
    find       : boolean;
    i          : longint;
begin
    fillchar(shortest , sizeof(shortest) , 0);
    fillchar(visited , sizeof(visited) , 0);
    visited[x , y] := true;
    open := 1; closed := 1;
    queue[1].x := x; queue[1].y := y;
    while open <= closed do
      begin
          for i := 1 to 4 do directions[i] := queue[open];
          find := true;
          if shortest[queue[open].x , queue[open].y] < 3 then
            while find do
              begin
                  find := false;
                  for i := 1 to 4 do
                    if directions[i].x <> -1 then
                      begin
                          inc(directions[i].x , dirx[i]); inc(directions[i].y , diry[i]);
                          if (directions[i].x <= N) and (directions[i].x >= 0) and (directions[i].y <= M) and (directions[i].y >= 0) then
                            begin
                                if not visited[directions[i].x , directions[i].y] then
                                  begin
                                      visited[directions[i].x , directions[i].y] := true;
                                      shortest[directions[i].x , directions[i].y] := shortest[queue[open].x , queue[open].y] + 1;
                                      if data[directions[i].x , directions[i].y] = #0 then
                                        begin
                                            inc(closed);
                                            queue[closed] := directions[i];
                                        end;
                                  end;
                                if data[directions[i].x , directions[i].y] <> #0
                                  then directions[i].x := -1
                                  else find := true;
                            end
                          else
                            directions[i].x := -1;
                      end;
              end;
          inc(open);
      end;
end;

function dfs : boolean;
var
    i , j , p1 ,
    p2         : longint;
    c          : char;
begin
    if path.tot * 2 = N * M then
      begin
          dfs := true;
          exit;
      end;
    for i := 1 to N do
      for j := 1 to M do
        if data[i , j] <> #0 then
          begin
              bfs(i , j);
              for p1 := 1 to N do
                for p2 := 1 to M do
                  if ((p1 <> i) or (p2 <> j)) and (data[p1 , p2] = data[i , j]) and visited[p1 , p2] then
                    begin
                        inc(path.tot);
                        path.data[path.tot].p.x := i; path.data[path.tot].p.y := j;
                        path.data[path.tot].q.x := p1; path.data[path.tot].q.y := p2;
                        c := data[i , j]; data[i , j] := #0; data[p1 , p2] := #0;
                        if dfs then
                          begin
                              dfs := true;
                              exit;
                          end;
                        data[i , j] := c; data[p1 , p2] := c;
                        fillchar(path.data[path.tot] , sizeof(path.data[path.tot]) , 0);
                        dec(path.tot);
                    end;
          end;
    dfs := false;
end;

procedure work;
begin
    fillchar(path , sizeof(path) , 0);
    answer := dfs;
end;

procedure out;
var
    i          : longint;
begin
    if not answer
      then writeln('No solution')
      else for i := 1 to path.tot do
             writeln('(' , path.data[i].p.y , ',' , path.data[i].p.x , '),(' ,
                           path.data[i].q.y , ',' , path.data[i].q.x , ')');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
