Var
    answer ,
    x , y , z ,
    Vx , Vy , Vz ,
    x1 , y1 , z1 ,
    x2 , y2 , z2 ,
    time , K   : extended;
    T , i      : longint;

Begin
    read(T);
    for i := 1 to T do
      begin
          read(time , x , y , z , Vx , Vy , Vz , x1 , y1 , z1 , x2 , y2 , z2);
          Vx := Vx - x; Vy := Vy - y; Vz := Vz - z;
          x2 := x2 - x1; y2 := y2 - y1; z2 := z2 - z1;
          x := x - x1; y := y - y1; z := z - z1;
          Vx := Vx - x2; Vy := Vy - y2; Vz := Vz - z2;
          if abs(Vx * Vx + Vy * Vy + Vz * Vz) <= 1e-6
            then K := 0
            else K := -(Vx * x + Vy * y + Vz * z) / (Vx * Vx + Vy * Vy + Vz * Vz);
          if K < 0 then K := 0;
          answer := sqr(x + Vx * K) + sqr(y + Vy * K) + sqr(z + Vz * K);
          writeln('Case ' , i , ': ' , sqrt(answer) : 0 : 4);
      end;
End.
