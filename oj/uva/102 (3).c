#include <stdio.h>

int main(void)
{
  int b[3][3], c[3][3], m, x;
  char *o;

  while (scanf("%d %d %d %d %d %d %d %d %d",
               &b[0][0], &b[0][1], &b[0][2],
               &b[1][0], &b[1][1], &b[1][2],
               &b[2][0], &b[2][1], &b[2][2]) == 9) {
    c[0][0] = b[1][0] + b[2][0];
    c[0][1] = b[1][1] + b[2][1];
    c[0][2] = b[1][2] + b[2][2];
    c[1][0] = b[0][0] + b[2][0];
    c[1][1] = b[0][1] + b[2][1];
    c[1][2] = b[0][2] + b[2][2];
    c[2][0] = b[0][0] + b[1][0];
    c[2][1] = b[0][1] + b[1][1];
    c[2][2] = b[0][2] + b[1][2];

    m = c[0][0] + c[1][2] + c[2][1], o = "BCG";
    if (m > (x = c[0][0] + c[1][1] + c[2][2])) m = x, o = "BGC";
    if (m > (x = c[0][2] + c[1][0] + c[2][1])) m = x, o = "CBG";
    if (m > (x = c[0][2] + c[1][1] + c[2][0])) m = x, o = "CGB";
    if (m > (x = c[0][1] + c[1][0] + c[2][2])) m = x, o = "GBC";
    if (m > (x = c[0][1] + c[1][2] + c[2][0])) m = x, o = "GCB";

    printf("%s %d\n", o, m);
  }

  return 0;
}
