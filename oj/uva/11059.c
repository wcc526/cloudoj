#include <stdio.h>

int main()
{
  int c = 0, i, j, n, v[18];
  long long m, p;

  while (scanf("%d", &n) == 1) {
    for (i = n; i--;)
      scanf("%d", v + i);
    for (m = i = 0; i < n; ++i) {
      for (p = 1, j = i; j < n; ++j) {
        p *= v[j];
        if (m < p)
          m = p;
      }
    }
    printf("Case #%d: The maximum product is %lld.\n\n", ++c, m);
  }

  return 0;
}
