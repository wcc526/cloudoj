#include <algorithm>
#include <cstdio>

int main()
{
  int b, c = 0, i, j, k, n, v[13];
  while (scanf("%d", &k) == 1 && k) {
    if (c++) putchar('\n');
    for (i = k; i--; scanf("%d", v + i));
    std::sort(v, v + k);
    for (n = 1 << k; --n;) {
      for (b = 0, j = n; j; ++b, j &= j - 1);
      if (b == 6)
        for (j = k; j--;)
          if (n & (1 << j))
            printf("%d%c", v[k - j - 1], --b ? ' ' : '\n');
    }
  }
}
