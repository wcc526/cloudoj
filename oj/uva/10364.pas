Const
    LimitHash  = 999997;

Var
    cases , j , tmp ,
    N , sum , i: longint;
    data       : array[1..20] of longint;
    hash       : array[0..LimitHash] of
                   record
                       a , b , c , d         : longint;
                   end;

function visited(a , b , c , d : longint) : boolean;
var
    p          : longint;
begin
    p := (a + b * 5013 + c * 101 + d * 1003) mod LimitHash;
    while (hash[p].a <> -1) and ((hash[p].a <> a) or (hash[p].b <> b) or (hash[p].c <> c) or (hash[p].d <> d)) do
      begin
          inc(p);
          if p = LimitHash then p := 0;
      end;
    if hash[p].a <> -1 then exit(true);
    visited := false;
    hash[p].a := a; hash[p].b := b; hash[p].c := c; hash[p].d := d;
end;

procedure swap(var a , b : longint);
var
    tmp        : longint;
begin
    tmp := a; a := b; b := tmp;
end;

procedure sort(var a , b , c , d : longint);
begin
    if a > b then swap(a , b);
    if a > c then swap(a , c);
    if a > d then swap(a , d);
    if b > c then swap(b , c);
    if b > d then swap(b , d);
    if c > d then swap(c , d);
end;

function dfs(a , b , c , d , step : longint) : boolean;
begin
    if (a > sum) or (b > sum) or (c > sum) or (d > sum) then exit(false);
    sort(a , b , c , d);
    if (step <= 10) and visited(a , b , c , d) then exit(false);
    if step > N then exit(true);
    if (sum <> d) and (sum - d < data[N]) then
      exit(false);
    if dfs(a + data[step] , b , c , d , step + 1) then exit(true);
    if dfs(a , b + data[step] , c , d , step + 1) then exit(true);
    if dfs(a , b , c + data[step] , d , step + 1) then exit(true);
    if dfs(a , b , c , d + data[step] , step + 1) then exit(true);
    exit(false);
end;

Begin
//    assign(INPUT , 'p10364.in'); ReSet(INPUT);
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          fillchar(hash , sizeof(hash) , $FF);
          read(N); for i := 1 to N do read(data[i]);
          for i := 1 to N do
            for j := i + 1 to N do
              if data[i] < data[j] then
                begin
                    tmp := data[i]; data[i] := data[j];
                    data[j] := tmp;
                end;
          sum := 0;
          for i := 1 to N do inc(sum , data[i]);
          if sum mod 4 <> 0
            then writeln('no')
            else begin
                     sum := sum div 4;
                     if dfs(0 , 0 , 0 , 0 , 1)
                       then writeln('yes')
                       else writeln('no');
                 end;
      end;
End.