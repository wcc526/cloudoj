#include <stdio.h>

int main(void)
{
  int i, n;
  double m, t, v, x, y;

  while (scanf("%d %lf", &n, &t) == 2 && n && t) {
    v = (double)(1 << n);
    for (i = 1; i <= n; ++i) {
      x = (double)(1 << (n - i));
      m = x / v;
      if (m < t) {
        m = t;
        x = 0.0;
      } else {
        x = (m - t) * x;
      }
      y = m < 1.0 ? v * (1 - m * m) * 0.5 : 0.0;
      v = (x + y) / (1 - t);
    }
    printf("%.3lf\n", v);
  }

  return 0;
}
