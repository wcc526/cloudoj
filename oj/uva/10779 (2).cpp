#include<iostream>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<map>
#include<vector>

#define MAXN 50
#define MAXM 55
#define MOD 10000
#define inf 1000000000
#define eps 1e-9
#define LL long long 

using namespace std;

int t, n, m, x, val, st, ed;
int cnt[15][30], es, head[MAXN], pre[MAXN], low[MAXN];
struct Edge {
	int u, v, cap, flow, next; 
} edge[MAXN * MAXN];
bool vis[MAXN];
int Q[MAXN], maxflow;

void add(int u, int v, int w) {
	edge[es].u = u, edge[es].v = v, edge[es].cap = w, edge[es].flow = 0, edge[es].next = head[u], head[u] = es++;
	edge[es].u = v, edge[es].v = u, edge[es].cap = 0, edge[es].flow = 0, edge[es].next = head[v], head[v] = es++;
}

void Edmonds_Karp() {
	int front, rear, u, v, cap, flow;
	maxflow = 0;
	do {
		memset(vis, false, sizeof(vis)); vis[st] = true;
		memset(low, 0, sizeof(low)); low[st] = inf;
		front = rear = 0;
		Q[rear++] = st;
		while (front != rear) {
			u = Q[front++];
			front %= MAXN;
			for (int i = head[u]; i != -1; i = edge[i].next) {
				v = edge[i].v, cap = edge[i].cap, flow = edge[i].flow;
				if (!vis[v] && cap > flow) {
					vis[v] = true;
					pre[v] = i;
					Q[rear++] = v;
					rear %= MAXN;
					low[v] = cap - flow;
					if (low[v] > low[u])
						low[v] = low[u];
				}
			}
		}
		if (low[ed] > 0) {
			u = ed;
			do {
				edge[pre[u]].flow += low[ed];
				edge[pre[u] ^ 1].flow -= low[ed];
				u = edge[pre[u]].u;
			} while (u != st);
			maxflow += low[ed];
		}
	} while (low[ed] > 0);
}

int main() {
	//freopen("in.txt","r",stdin);
	//freopen("out.txt", "w", stdout);
	scanf("%d", &t);
	for (int Case = 1; Case <= t; Case++) {
		scanf("%d %d", &n, &m);
		memset(cnt, 0, sizeof(cnt));
		for (int i = 1; i <= n; i++) {
			scanf("%d", &x);
			for (int j = 1; j <= x; j++) {
				scanf("%d", &val);
				cnt[i][val]++;
			}
		}
		st = 0, ed = n + m + 1;
		es = 0; memset(head, -1, sizeof(head));
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (i == 1) 
					add(st, j, cnt[i][j]);
				else {
					if (cnt[i][j]) 
						add(m + i, j, cnt[i][j] - 1);
					else 
						add(j, m + i, 1);
				}
			}
		}
		for (int i = 1; i <= m; i++) 
			add(i, ed, 1);
		Edmonds_Karp();
		printf("Case #%d: %d\n", Case, maxflow);
	}
    return 0;
}