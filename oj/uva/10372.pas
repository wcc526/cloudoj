Const
    InFile     = 'p10372.in';
    Limit      = 100;
    G          = 9.8;

Type
    Tdata      = array[1..Limit] of
                   record
                       Len , H , subsum      : double;
                   end;

Var
    data       : Tdata;
    X1 , angle ,
    V          : double;
    N          : longint;

procedure init;
var
    i          : longint;
begin
    readln(N);
    for i := 1 to N do
      begin
          read(data[i].H , data[i].Len);
          if i = 1
            then data[i].subsum := data[i].Len
            else data[i].subsum := data[i - 1].subsum + data[i].Len;
      end;
    readln;
    X1 := data[N].subsum;
end;

function check(A , B : double) : boolean;
var
    i          : longint;
  function calc(x : double) : double; begin exit(x * x * A + x * B); end;

begin
    for i := 1 to N do
      begin
          if calc(data[i].subsum) < data[i].H then exit(false);
          if calc(data[i].subsum - data[i].Len) < data[i].H then exit(false);
      end;
    exit(true);
end;

procedure work;
var
    start , stop ,
    mid , best ,
    A , B , T  : double;
    i          : longint;
begin
    start := 0; stop := 1e7;
    for i := 1 to 200 do
      begin
          mid := (start + stop) / 2;
          if check(-mid , mid * X1)
            then begin best := mid; stop := mid; end
            else start := mid;
      end;
    A := -best; B := best * X1;
    angle := arctan(B);
    V := sqrt(G * X1 / sin(angle * 2));
end;

procedure out;
begin
    writeln(angle / pi * 180 : 0 : 2 , ' ' , V : 0 : 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.