{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10588.in';
    OutFile    = 'p10588.out';
    Limit      = 1010;
    LimitSave  = 1000100;

Type
    Tqueue     = object
                     tot                     : longint;
                     qdata                   : array[1..Limit] of longint;
                     visited                 : array[1..Limit] of boolean;
                     procedure init;
                     procedure ins(p : longint);
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                     procedure process;
                 end;
    Tkey       = record
                     number , index , next   : longint;
                 end;
    Tdata      = array[1..LimitSave] of Tkey;
    Tdoctor    = object
                     head , tail             : longint;
                     procedure ins(p , index , doctornum : longint);
                     procedure cuthead;
                 end;
    Tdoctors   = array[1..Limit] of Tdoctor;
    Trec       = record
                     t , k , st , no         : longint;
                 end;
    Tpatient   = array[1..Limit] of Trec;
    Tsave      = record
                     tot                     : longint;
                     data                    : array[1..LimitSave] of longint;
                 end;

Var
    queue      : Tqueue;
    data       : Tdata;
    doctors    : Tdoctors;
    patient    : Tpatient;
    save       : Tsave;
    cases ,
    time ,  
    N , M ,
    sum        : longint;

procedure Tqueue.init;
begin
    fillchar(qdata , sizeof(data) , 0);
    fillchar(visited , sizeof(visited) , 0);
    tot := 0;
end;

procedure Tqueue.ins(p : longint);
begin
    if not visited[p] then
      begin
          visited[p] := true;
          inc(tot); qdata[tot] := p;
      end;
end;

procedure Tqueue.qk_pass(start , stop : longint; var mid : longint);
var
    tmp , key  : longint;        
begin
    tmp := random(stop - start + 1) + start;
    key := qdata[tmp]; qdata[tmp] := qdata[start];
    while start < stop do
      begin
          while (start < stop) and (patient[data[doctors[qdata[stop]].head].number].no > patient[data[doctors[key].head].number].no) do dec(stop);
          qdata[start] := qdata[stop];
          if start < stop then inc(start);
          while (start < stop) and (patient[data[doctors[qdata[start]].head].number].no < patient[data[doctors[key].head].number].no) do inc(start);
          qdata[stop] := qdata[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    qdata[start] := key;
end;

procedure Tqueue.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure Tqueue.process;
var
    i          : longint;
begin
    i := 1;
    while i <= tot do
      if doctors[qdata[i]].head = 0
        then begin
                 visited[qdata[i]] := false;
                 qdata[i] := qdata[tot];
                 dec(tot);
             end
        else inc(i);
    qk_sort(1 , tot);
end;

procedure Tdoctor.ins(p , index , doctornum : longint);
begin
    inc(sum);
    data[sum].number := p;
    data[sum].index := index;
    data[sum].next := 0;
    if head = 0
      then begin
               head := sum; tail := sum;
               queue.ins(doctornum);
           end
      else begin
               data[tail].next := sum;
               tail := sum;
           end;
end;

procedure Tdoctor.cuthead;
begin
    head := data[head].next;
    if head = 0 then tail := 0;
end;

procedure init;
var
    i , j      : longint;
begin
    dec(cases);
    fillchar(queue , sizeof(queue) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(doctors , sizeof(doctors) , 0);
    fillchar(patient , sizeof(patient) , 0);
    fillchar(save , sizeof(save) , 0);
    read(N , M);
    for i := 1 to N do
      begin
          read(patient[i].t , patient[i].k);
          patient[i].st := save.tot + 1; patient[i].no := i;
          for j := 1 to patient[i].k do
            begin
                inc(save.tot);
                read(save.data[save.tot]);
            end;
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Trec;
begin
    tmp := random(stop - start + 1) + start;
    key := patient[tmp]; patient[tmp] := patient[start];
    while start < stop do
      begin
          while (start < stop) and ((patient[stop].t > key.t) or (patient[stop].t = key.t) and (patient[stop].no > key.no)) do dec(stop);
          patient[start] := patient[stop];
          if start < stop then inc(start);
          while (start < stop) and ((patient[start].t < key.t) or (patient[start].t = key.t) and (patient[start].no < key.no)) do inc(start);
          patient[stop] := patient[start];
          if start < stop then dec(stop); 
      end;
    mid := start;
    patient[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop
      then begin
               qk_pass(start , stop , mid);
               qk_sort(start , mid - 1);
               qk_sort(mid + 1 , stop);
           end;
end;

procedure work;
var
    newdoc ,
    p , i ,
    num ,
    index ,
    target     : longint;
begin
    qk_sort(1 , N);
    time := -1; p := 1;
    while (queue.tot > 0) or (p <= N) do
      begin
          if queue.tot = 0 then time := patient[p].t else inc(time);
          while (p <= N) and (patient[p].t = time) do
            begin
                if patient[p].k > 0 then
                  begin
                      newdoc := save.data[patient[p].st];
                      doctors[newdoc].ins(p , 1 , newdoc);
                  end;
                inc(p);
            end;
          queue.process;
          
          target := queue.tot; i := 1;
          while (i <= target) or (p <= N) and (patient[p].t = time + 1) do
            if (i <= target) and (not ((p <= N) and (patient[p].t = time + 1)) or
               (patient[data[doctors[queue.qdata[i]].head].number].no < patient[p].no)) then
              begin
                  newdoc := queue.qdata[i];
                  num := data[doctors[newdoc].head].number;
                  index := data[doctors[newdoc].head].index;
                  doctors[newdoc].cuthead;
                  if index < patient[num].k then
                    begin
                        newdoc := save.data[patient[num].st + index];
                        doctors[newdoc].ins(num , index + 1 , newdoc);
                    end;
                  inc(i);
              end
            else
              begin
                  if patient[p].k > 0 then
                    begin
                        newdoc := save.data[patient[p].st];
                        doctors[newdoc].ins(p , 1 , newdoc);
                    end;
                  inc(p);
              end;
      end;
end;

procedure out;
begin
    writeln(time);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
