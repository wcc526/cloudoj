#include <math.h>
#include <stdio.h>

int main()
{
  int t;
  double ax, ay, bx, by, dx, dy, ex, ey;

  scanf("%d", &t);
  while (t--) {
    scanf("%lf %lf %lf %lf", &ax, &ay, &bx, &by);
    dx = ax - bx, dy = ay - by;
    ex = ax + bx, ey = ay + by;
    printf("%.3lf\n", sqrt(dx * dx + dy * dy) + sqrt(ex * ex + ey * ey));
  }

  return 0;
}
