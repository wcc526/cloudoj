{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10546.in';
    OutFile    = 'p10546.out';
    Limit      = 300;

Type
    Tmap       = array[1..Limit * 2 + 2 , 1..Limit * 2 + 1] of longint;
    Tdata      = array[0..Limit + 1] of longint;
    Tlongest   = array[0..Limit + 1] of longint;
    Tvisited   = array[1..Limit * 2 + 2] of boolean;

Var
    C          : Tmap;
    data       : Tdata;
    longest    : Tlongest;
    visited    : Tvisited;
    answer ,
    Cases , N ,
    st , ed    : longint;

function init : boolean;
var
    i          : longint;
    ch         : char;
begin
    fillchar(C , sizeof(C) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(longest , sizeof(longest) , 0);
    inc(Cases);
    readln(N);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     read(ch);
                     while ch = ' ' do read(ch);
                     while ch <> ' ' do read(ch);
                     readln(data[i]);
                 end;
           end;
end;

function extend(p : longint) : boolean;
var
    i          : longint;
begin
    visited[p] := true;
    if p = ed
      then extend := true
      else begin
               for i := st to ed do
                 if not visited[i] and (C[p , i] = 1) and extend(i) then
                   begin
                       extend := true;
                       dec(C[p , i]); inc(C[i , p]);
                       exit;
                   end;
               extend := false;
           end;
end;

procedure work;
var
    i , j      : longint;
begin
    st := 1; ed := N * 2 + 2;
    data[N + 1] := maxlongint; data[0] := -1;
    longest[N + 1] := 1;
    for i := N downto 0 do
      begin
          longest[i] := 0;
          for j := i + 1 to N + 1 do
            if data[j] > data[i] then
              if longest[j] > longest[i] then
                longest[i] := longest[j];

          for j := i + 1 to N + 1 do
            if (longest[i] = longest[j]) and (data[j] > data[i]) then
              C[i * 2 + 1 , j * 2] := 1;
          inc(longest[i]);
      end;
    for i := 1 to N do
      C[i * 2 , i * 2 + 1] := 1;

    answer := 0;
    fillchar(visited , sizeof(visited) , 0);
    while extend(st) do
      begin
          fillchar(visited , sizeof(visited) , 0);
          inc(answer , longest[0] - 2);
      end;
end;

procedure out;
begin
    writeln('Case #' , cases , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
