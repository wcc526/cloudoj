{$R-,Q-,S-}
Const
    InFile     = 'p10829.in';
    OutFile    = 'p10829.out';
    Limit      = 50100;
    LimitLog   = 20;

Type
    Tnums      = array[0..Limit] of longint;
    TSA        = object
                     Len                     : longint;
                     source , SA , Rank ,
                     Height , Log2           : Tnums;
                     rmq                     : array[1..LimitLog] of Tnums;
                     procedure build;
                     function lcp(a , b : longint) : longint;
                 end;

Var
    data       : Tnums;
    SA00 , SA11: TSA;
    Len , G , answer ,
    T , cases  : longint;

function inc(var a : longint; b : longint) : longint; //inline;
begin inc := a; a += b; end;

function _inc(var a : longint; b : longint) : longint; //inline;
begin a += b; _inc := a; end;

function compare_sub(a1 , a2 , b1 , b2 : longint) : longint; //inline;
begin
    if a1 <> b1 then exit(a1 - b1) else exit(a2 - b2);
end;

function compare(a1 , a2 , a3 , b1 , b2 , b3 : longint) : longint; //inline;
begin
    if a1 <> b1 then exit(a1 - b1) else exit(compare_sub(a2 , a3 , b2 , b3));
end;

procedure radixSort(var source , target , key : Tnums; K , Len , d : longint);
var
    count      : Tnums;
    i          : longint;
begin
    for i := 0 to K do count[i] := 0;
    for i := 1 to Len do count[key[source[i] + d]] += 1;
    for i := 1 to K do count[i] += count[i - 1];
    for i := Len downto 1 do
      target[inc(count[key[source[i] + d]] , -1)] := source[i];
end;

procedure dfs_buildSA(var source , SA , Rank : Tnums; K , Len : longint);
var
    s , s23 , SA23 ,
    SA1 , tmp ,
    r          : ^Tnums;
    size ,
    i , j , tot: longint;
  function better(i , j : longint) : boolean; //inline;
  begin
      if i mod 3 = 2
        then exit(compare_sub(source[i] , r^[i + 1] , source[j] , r^[j + 1]) > 0)
        else exit(compare(source[i] , source[i + 1] , r^[i + 2] , source[j] , source[j + 1] , r^[j + 2]) > 0);
  end;
begin
    Rank[Len + 1] := 0; Rank[Len + 2] := 0; Rank[Len + 3] := 0;
    source[Len + 1] := 0; source[Len + 2] := 0; source[Len + 3] := 0;
    if Len = 1 then begin SA[1] := 1; Rank[1] := 1; exit; end;

    size := sizeof(longint) * (Len + 5);
    GetMem(s , size); GetMem(s23 , size); GetMem(SA23 , size);
    GetMem(SA1 , size); GetMem(tmp , size); GetMem(r , size);

    tot := 0;
    i := 2; while i <= Len + 1 do s^[_inc(tot , 1)] := inc(i , 3);
    i := 3; while i <= Len do s^[_inc(tot , 1)] := inc(i , 3);

    radixSort(s^ , r^ , source , K , tot , 2);
    radixSort(r^ , tmp^ , source , K , tot , 1);
    radixSort(tmp^ , r^ , source , K , tot , 0);

    for i := 1 to tot do tmp^[s^[i]] := i; s23^[tmp^[r^[1]]] := 1;
    for i := 2 to tot do
      s23^[tmp^[r^[i]]] := s23^[tmp^[r^[i - 1]]] +
        ord(compare(source[r^[i]] , source[r^[i] + 1] , source[r^[i] + 2] ,
          source[r^[i - 1]] , source[r^[i - 1] + 1] , source[r^[i - 1] + 2]) <> 0);

    dfs_buildSA(s23^ , SA23^ , r^ , s23^[tmp^[r^[tot]]] , tot);
    for i := 1 to tot do SA23^[i] := s^[SA23^[i]];
    for i := 1 to tot do r^[SA23^[i]] := i;

    j := 0;
    for i := 1 to tot do if SA23^[i] mod 3 = 2 then tmp^[_inc(j , 1)] := SA23^[i] - 1;
    radixSort(tmp^ , SA1^ , source , K , j , 0);

    i := tot; tot := Len;
    while (i >= 1) or (j >= 1) do
      if (i <> 0) and ((j = 0) or better(SA23^[i] , SA1^[j]))
        then SA[inc(tot , -1)] := SA23^[inc(i , -1)]
        else SA[inc(tot , -1)] := SA1^[inc(j , -1)];
    for i := 1 to Len do Rank[SA[i]] := i;

    dispose(s); dispose(s23); dispose(SA23);
    dispose(SA1); dispose(tmp); dispose(r);
end;

procedure TSA.build;
var
    i , k , t  : longint;
begin
    dfs_buildSA(source , SA , Rank , 255 , Len);
    k := 0;
    for i := 1 to Len do
      if Rank[i] = 1
        then begin height[Rank[i]] := 0; k := 0; end
        else begin
                 if k > 0 then k -= 1; t := SA[Rank[i] - 1];
                 while source[t + k] = source[i + k] do k += 1;
                 height[Rank[i]] := k;
             end;
    k := 1; t := 1; for i := 1 to Len do rmq[1 , i] := height[i];
    while k < Len do
      begin
          for i := 1 to Len - k do
            if rmq[t , i] < rmq[t , i + k]
              then rmq[t + 1 , i] := rmq[t , i]
              else rmq[t + 1 , i] := rmq[t , i + k];
          for i := Len - k + 1 to Len do rmq[t + 1 , i] := 0;
          k *= 2; t += 1;
      end;
    Log2[1] := 1;
    for i := 2 to Len do Log2[i] := Log2[i div 2] + 1;
end;

function TSA.lcp(a , b : longint) : longint;
var
    k          : longint;
begin
    if a = b then exit(Len - a + 1);
    if a > b then begin k := a; a := b; b := k; end;
    k := Log2[b - a]; a += 1; b -= 1 shl (k - 1) - 1;
    if rmq[k , a] < rmq[k , b] then exit(rmq[k , a]) else exit(rmq[k , b]);
end;

procedure init;
var
    ch         : char;
begin
    Len := 0;
    read(G); repeat read(ch); until ch <> ' ';
    while ch <> ' ' do
      begin
          data[_inc(Len , 1)] := ord(ch);
          if eoln or eof then ch := ' ' else read(ch);
      end;
    readln;
end;

function lcp00(i , j : longint) : longint; //inline;
begin
    exit(SA00.lcp(SA00.Rank[i] , SA00.Rank[j]));
end;

function lcp11(i , j : longint) : longint; //inline;
begin
    exit(SA11.lcp(SA11.Rank[Len - i + 1] , SA11.Rank[Len - j + 1]));
end;

procedure dfs(start , stop : longint);
var
    mid , i , j ,
    k1 , k2    : longint;
begin
    if stop - start < G + 1 then exit;
    mid := (start + stop) div 2;
    for i := start to mid - G do
      begin
          if i = start then k1 := 0 else k1 := lcp11(i - 1 , mid);
          k2 := lcp00(i , mid + 1);
          if k1 > i - start then k1 := i - start;
          if k2 > stop - mid then k2 := stop - mid;
          k1 := mid - k1 + 1; k2 := i + k2 - 1;
          if k1 < i + G + 1 then k1 := i + G + 1;
          if k2 > mid - G then k2 := mid - G;
          if k2 + G - k1 + 2 > 0 then inc(answer , k2 + G - k1 + 2);
      end;
    for i := mid + 1 + G to stop do
      begin
          if i = stop then k1 := 0 else k1 := lcp00(i + 1 , mid + 1);
          k2 := lcp11(mid , i);
          if k1 > stop - i then k1 := stop - i;
          if k2 > mid - start + 1 then k2 := mid - start + 1;
          k1 := mid + k1; k2 := i - k2 + 1;
          if k1 > i - G - 1 then k1 := i - G - 1;
          if k2 < mid + 1 + G then k2 := mid + 1 + G;
          if k1 + G - k2 + 2 > 0 then inc(answer , k1 + G - k2 + 2);
      end;
    for i := mid - G + 2 to mid do if (i > start) and (i + G <= stop) then
      for j := start to i - 1 do if i - j + i + G - 1 <= stop then
        if lcp00(j , i + G) + j >= i then
          answer += 1;
    dfs(start , mid); dfs(mid + 1 , stop);
end;

procedure work;
var
    i          : longint;
begin
    SA00.Len := Len; SA11.Len := Len;
    for i := 1 to Len do
      begin
          SA00.source[i] := data[i]; SA11.source[i] := data[Len - i + 1];
      end;
    SA00.build; SA11.build;
    answer := 0;
    dfs(1 , Len);
end;

procedure out;
begin
    writeln('Case ' , _inc(cases , 1) , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T); cases := 0;
      while cases < T do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.