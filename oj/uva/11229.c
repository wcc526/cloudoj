#include <stdio.h>
#include <string.h>

float max(short, short);
float min(short, short);

float p[9], q[9];
float x[262144];
short t[9], w[9] = { 0x124, 0x92, 0x49, 0x1C0, 0x38, 0x7, 0x111, 0x54 };

float base(short m, short s)
{
  int i;
  if (x[i = ((m << 9) | s)] > -0.5)
    return x[i];
  for (i = 8; i--;) {
    if ((m & w[i]) == w[i]) {
      if ((s & w[i]) == w[i])
        return 1;
      else if ((s & w[i]) == 0)
        return 0;
    }
  }
  if (m == 511)
    return 0;
  return -1;
}

#define d(f, o, v, p, z)                      \
  float f(short m, short s)                   \
  {                                           \
    float r, u;                               \
    short i, j;                               \
    if ((r = base(m, s)) > -0.5)              \
      return r;                               \
    for (r = v, j = 9, i = 1; j--; i <<= 1)   \
      if ((m & i) == 0)                       \
        if (r z (u = o(m | i, s | i) * p[j] + \
                 o(m | i, s) * (1 - p[j])))   \
          r = u;                              \
    x[(m << 9) | s] = r;                      \
    return r;                                 \
  }

d(max, min, -1, p, <)
d(min, max, 1, q, >)

int main()
{
  short c = 0, j, n;
  int i;

  scanf("%hd", &n);
  while (n--) {
    for (t[i = 0] = 1; i++ < 9; t[i] = t[i - 1] * 3);
    memset(x, 0xFF, sizeof(x));
    for (i = 9; i--;)
      scanf("%hd", &j), p[i] = 0.01 * j;
    for (i = 9; i--;)
      scanf("%hd", &j), q[i] = 0.01 * (100 - j);
    printf("Case #%d: %.2f\n", ++c, 100.0 * max(0, 0));
  }

  return 0;
}
