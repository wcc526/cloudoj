#include <stdio.h>
#include <string.h>

typedef struct {
  unsigned int v[117];
} big;

void big_add(big *s1, big *s2, big *d)
{
  int i;
  for (i = 117; i--;)
    if ((d->v[i] += s1->v[i] + s2->v[i]) > 999999999)
      d->v[i] -= 1000000000, ++d->v[i - 1];
}

void big_print(big *b)
{
  int i;
  for (i = 0; i < 117 && b->v[i] == 0; ++i);
  printf("%u", b->v[i]);
  while (++i < 117)
    printf("%09u", b->v[i]);
}

int main()
{
  big f[5001];
  int m = 2, n;

  memset(f, 0, sizeof(f));
  f[1].v[116] = 1;

  while (scanf("%d", &n) == 1) {
    for (; m <= n; ++m)
      big_add(&f[m - 2], &f[m - 1], &f[m]);
    printf("The Fibonacci number for %d is ", n);
    big_print(&f[n]);
    putchar('\n');
  }

  return 0;
}
