Const
    InFile     = 'p10367.in';
    OutFile    = 'p10367.out';

Type
    Tequa      = record
                     A , B , C               : longint;
                 end;
    Tfraction  = object
                     A , B                   : longint;
                     procedure reduce;
                     procedure print;
                 end;

Var
    E1 , E2    : Tequa;
    cases      : longint;
    X , Y      : Tfraction;

procedure read_Equa(var E : Tequa);
var
    direct ,
    startdirect ,
    num        : longint;
    appeared   : boolean;
    ch , mark  : char;
begin
    fillchar(E , sizeof(E) , 0);
    startdirect := 1;
    while not eoln do
      begin
          direct := startdirect;
          repeat
            if eoln then ch := ' ' else read(ch);
          until ch <> ' ';
          if eoln then break;
          while ch in ['+' , '-' , '='] do
            begin
                if ch = '=' then begin startdirect := -startdirect; direct := -direct; end;
                if ch = '-' then direct := -direct;
                read(ch);
            end;
          while ch = ' ' do read(ch);
          num := 0; appeared := false; mark := #0;
          while ch <> ' ' do
            begin
                if ch = '-' then direct := -direct;
                if ch in ['0'..'9'] then
                  begin appeared := true; num := num * 10 + ord(ch) - ord('0'); end;
                if ch in ['x' , 'y'] then
                  mark := ch;
                if eoln then ch := ' ' else read(ch);
            end;
          if not appeared then num := 1;
          case mark of
            #0 : inc(E.C , direct * num);
            'x': inc(E.A , direct * num);
            'y': inc(E.B , direct * num);
          end;
      end;
    readln;
end;

procedure init;
begin
    dec(Cases);
    read_Equa(E1);
    read_Equa(E2);
    readln;
end;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then exit(b)
      else exit(gcd(b mod a , a));
end;

procedure work;
var
    tmp        : longint;
begin
    X.A := 0; X.B := 0; Y.A := 0; Y.B := 0;
    if abs(E1.A) + abs(E1.B) = 0 then if E1.C <> 0 then exit;
    if abs(E2.A) + abs(E2.B) = 0 then if E2.C <> 0 then exit;
    if comp(E1.A) * comp(E2.B) = comp(E1.B) * comp(E2.A) then
      begin
          if comp(E1.A) * comp(E2.C) <> comp(E1.C) * comp(E2.A) then exit;
          if comp(E1.B) * comp(E2.C) <> comp(E1.C) * comp(E2.B) then exit;
      end;
    if abs(E1.A) + abs(E2.A) = 0 then
      begin
          if E1.B <> 0
            then begin Y.A := -E1.C; Y.B := E1.B; end
            else begin Y.A := -E2.C; Y.B := E2.B; end;
          exit;
      end;
    if abs(E1.B) + abs(E2.B) = 0 then
      begin
          if E1.A <> 0
            then begin X.A := -E1.C; X.B := E1.A; end
            else begin X.A := -E2.C; X.B := E2.A; end;
          exit;
      end;
    if comp(E1.A) * comp(E2.B) = comp(E1.B) * comp(E2.A) then exit;
    tmp := E1.A * E2.B - E1.B * E2.A;
    X.A := -(E1.C * E2.B - E1.B * E2.C); X.B := tmp;
    Y.A := -(E1.A * E2.C - E1.C * E2.A); Y.B := tmp;
end;

procedure Tfraction.reduce;
var
    gcdnum     : longint;
begin
    if B < 0 then begin A := -A; B := -B; end;
    gcdnum := gcd(abs(A) , abs(B));
    if gcdnum <> 0 then
      begin A := A div gcdnum; B := B div gcdnum; end;
end;

procedure Tfraction.print;
begin
    if B = 0
      then writeln('don''t know')
      else if B = 1
             then writeln(A)
             else writeln(A , '/' , B);
end;

procedure out;
begin
    X.reduce; Y.reduce;
    X.print; Y.print;
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.