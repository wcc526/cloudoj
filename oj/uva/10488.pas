Var
    swim , walk ,
    D , L , tmp ,
    xs , ys , t , tot ,
    xt , yt    : extended;
Begin
    while not eof do
      begin
          readln(D , L , xs , ys , xt , yt);
          if ys > yt then
            begin tmp := ys; ys := yt; yt := tmp; end;
          t := int(ys / D);
          ys := ys - t * D; yt := yt - t * D;
          while ys < 0 do begin ys := ys + D; yt := yt + D; end;
          while ys > D do begin ys := ys - D; yt := yt - D; end;
          tot := int(yt / D);
          swim := tot * L * 2;
          walk := sqrt(sqr(yt - ys - swim) + sqr(xt - xs));
          writeln('The gopher has to swim ' , swim : 0 : 2 , ' meters and walk ' , walk : 0 : 2 , ' meters.');
      end;
End.