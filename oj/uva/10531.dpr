{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10531.in';
    OutFile    = 'p10531.out';
    LimitN     = 7;
    LimitM     = 5;

Type
    Tdata      = array[0..LimitN , 0..1 shl LimitM - 1 , 0..4 , 1..3] of extended;
    Tnums      = array[1..LimitN , 1..LimitM] of extended;
    Topt       = array[0..LimitN , 0..1 shl LimitM - 1 , 0..4 , 1..3] of Tnums;
    Tmap       = array[0..LimitM * LimitM , 0..LimitM * LimitM] of boolean;
    Tnum       = array[1..LimitN] of longint;

Var
    data       : Tdata;
    answer ,
    nums       : Tnums;
    opt        : Topt;
    map        : Tmap;
    N , M , T  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(T);
    readln(M , N);
    for j := 1 to M do
      for i := 1 to N do
        read(nums[i , j]);
    for j := 1 to M do
      nums[N + 1 , j] := 1;
    nums[N + 1 , M] := 0;
end;

procedure Get_New_Status(k , p1 , p2 , j : longint; var np1 , np2 : longint);
var
    num1 , num2
               : Tnum;
    a , b      : array[1..3] of longint;
    i , ta , tb: longint;
begin
    fillchar(num1 , sizeof(num1) , 0);
    fillchar(num2 , sizeof(num2) , 0);
    for i := 1 to M do
      begin
          num1[i] := ord(k and (1 shl (i - 1)) <> 0);
          num2[i] := ord(j and (1 shl (i - 1)) <> 0);
      end;

    ta := 0; a[1] := 0; a[2] := 0; a[3] := 0;
    tb := 0; b[1] := 0; b[2] := 0; b[3] := 0;
    for i := M downto 1 do
      begin
          if (num1[i] = 0) and ((i = M) or (num1[i + 1] <> 0)) then
            begin inc(ta); a[ta] := i; end;
          if (num2[i] = 0) and ((i = M) or (num2[i + 1] <> 0)) then
            begin inc(tb); b[tb] := i + M; end;
      end;
    fillchar(map , sizeof(map) , 0);
    for i := 1 to M do
      if (num1[i] = 0) and (num2[i] = 0) then
        begin map[i , i + M] := true; map[i + M , i] := true; end;
    for i := 2 to M do
      begin
          if (num1[i] = 0) and (num1[i - 1] = 0) then begin map[i , i - 1] := true; map[i - 1 , i] := true; end;
          if (num2[i] = 0) and (num2[i - 1] = 0) then begin map[i + M , i + M - 1] := true; map[i + M - 1 , i + M] := true; end;
      end;
    case p1 of
      0        : ;
      1        : begin map[a[1] , a[2]] := true; map[a[2] , a[1]] := true; end;
      2        : begin map[a[2] , a[3]] := true; map[a[3] , a[2]] := true; end;
      3        : begin map[a[1] , a[3]] := true; map[a[3] , a[1]] := true; end;
      4        : begin
                     map[a[1] , a[2]] := true; map[a[2] , a[1]] := true;
                     map[a[2] , a[3]] := true; map[a[3] , a[2]] := true;
                 end;
    end;

    for k := 1 to M * 2 do
      for i := 1 to M * 2 do
        for j := 1 to M * 2 do
          if map[i , k] and map[k , j] then
            map[i , j] := true;
    np2 := 1;
    while (np2 <= tb) and not map[a[p2] , b[np2]] do inc(np2);
    if np2 > tb then begin np2 := 0; exit; end;
    np1 := 0;
    if map[b[1] , b[2]] then np1 := 1;
    if map[b[2] , b[3]] then np1 := 2;
    if map[b[1] , b[3]] then np1 := 3;
    if map[b[1] , b[2]] and map[b[2] , b[3]] then np1 := 4;
end;

function calc_poss(i , j : longint) : extended;
var
    res        : extended;
    k          : longint;
begin
    res := 1;
    for k := 1 to M do
      begin
          if odd(j)
            then res := res * nums[i , k]
            else res := res * (1 - nums[i , k]);
          j := j div 2;
      end;
    calc_poss := res;
end;

procedure work;
var
    i , j , k ,
    x , y , p ,  
    p1 , p2 ,
    np1 , np2  : longint;
    tmp        : extended;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , 0);
    data[0 , 1 shl M - 2 , 0 , 1] := 1;
    for i := 1 to N + 1 do
      begin
          for k := 0 to 1 shl M - 2 do
            for p1 := 0 to 4 do
              for p2 := 1 to 3 do
                if data[i - 1 , k , p1 , p2] > 1e-20 then
                  for j := 0 to 1 shl M - 2 do
                    begin
                        Get_New_Status(k , p1 , p2 , j , np1 , np2);
                        if np2 <> 0 then
                          data[i , j , np1 , np2] := data[i , j , np1 , np2] + data[i - 1 , k , p1 , p2] * calc_poss(i , j);
                    end;
      end;

    for i := 1 to N + 1 do
      begin
          for k := 0 to 1 shl M - 2 do
            for p1 := 0 to 4 do
              for p2 := 1 to 3 do
                if data[i - 1 , k , p1 , p2] > 1e-20 then
                  for j := 0 to 1 shl M - 2 do
                    begin
                        Get_New_Status(k , p1 , p2 , j , np1 , np2);
                        if np2 <> 0 then
                          begin
                              tmp := calc_poss(i , j);
                              if tmp <= 1e-20 then continue;
                              tmp := data[i - 1 , k , p1 , p2] * tmp / data[i , j , np1 , np2];
                              for x := 1 to i - 1 do
                                for y := 1 to M do
                                  opt[i , j , np1 , np2 , x , y] := opt[i , j , np1 , np2 , x , y] + opt[i - 1 , k , p1 , p2 , x , y] * tmp;
                              p := j;
                              for y := 1 to M do
                                begin
                                    if odd(p)
                                      then opt[i , j , np1 , np2 , i , y] := 1
                                      else opt[i , j , np1 , np2 , i , y] := 0;
                                    p := p div 2;
                                end;
                          end;
                    end;
      end;

    answer := opt[N + 1 , 1 shl (M - 1) - 1 , 0 , 1];
end;

procedure out;
var
    i , j      : longint;
begin
    for j := 1 to M do
      begin
          for i := 1 to N do
            begin
                write(answer[i , j] : 0 : 6);
                if i <> N then write(' ');
            end;
          writeln;
      end;
    if T <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
