#include <stdio.h>
#include <string.h>

short v[100000];

int c(int n)
{
  int i;
  if (n < 100000) {
    if (!v[n]) {
      if (n & 1)
        i = 3 * n + 1;
      else
        i = n >> 1;
      v[n] = 1 + c(i);
    }
    return v[n];
  } else {
    i = 1;
    while (n > 1) {
      if (n & 1)
        n = 3 * n + 1;
      else
        n >>= 1;
      ++i;
    }
    return i;
  }
}

int main(void)
{
  int i, j, m, n, t;

  memset(v, 0, sizeof v);
  v[1] = 1;

  while (scanf("%d %d", &i, &j) == 2) {
    printf("%d %d ", i, j);
    m = 0;
    if (i > j) {
      t = i; i = j; j = t;
    }
    for (n = i; n <= j; ++n) {
      t = c(n);
      if (m < t)
        m = t;
    }
    printf("%d\n", m);
  }

  return 0;
}
