#include<cstring>
#include<stdio.h>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<map>
using namespace std;
#define N 20005
#define M 100000
int s[M+1],a[N];
int ld[N],lx[N],rd[N],rx[N],n;
int lowbit(int x)
{
	return x&(-x);
}
void add(int x)
{
	while(x<=M)
	{
		s[x]++;
		x+=lowbit(x);
	}
}
int sum(int x)
{
	int m=0;
	while(x>0)
	{
		m+=s[x];
		x-=lowbit(x);
	}
	return m;
}
int main()
{
	int i,j,k,t;
	cin>>t;
	while(t--)
	{
		cin>>n;
		memset(s,0,sizeof(s));
		for(i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			lx[i]=sum(a[i]);
			ld[i]=i-lx[i]-1;
			add(a[i]);
		}
		memset(s,0,sizeof(s));
		for(i=n;i>=1;i--)
		{
			rx[i]=sum(a[i]);
			rd[i]=n-i-rx[i];
			add(a[i]);
		}
		long long sum=0;
		for(i=1;i<=n;i++)
		{
	//		cout<<lx[i]<<' '<<ld[i]<<' '<<rx[i]<<' '<<rd[i]<<endl;
			sum=sum+rx[i]*ld[i]+rd[i]*lx[i];
		}
		cout<<sum<<endl;
	}
	return 0;
}