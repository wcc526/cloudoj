{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10574.in';
    OutFile    = 'p10574.out';
    Limit      = 5000;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tstarting  = array[1..Limit] of longint;

Var
    data       : Tdata;
    starting   : Tstarting;
    answer , 
    N , totCase ,
    nowCase    : longint;

procedure init;
var
    i          : longint;
begin
    inc(nowCase);
    read(N);
    for i := 1 to N do
      read(data[i].x , data[i].y);
end;

function bigger(p1 , p2 : Tpoint) : boolean;
begin
    if p1.x <> p2.x
      then bigger := p1.x > p2.x
      else bigger := p1.y > p2.y;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and bigger(data[stop] , key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and bigger(key , data[start]) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop
      then begin
               qk_pass(start , stop , mid);
               qk_sort(start , mid - 1);
               qk_sort(mid + 1 , stop);
           end;
end;

procedure work;
var
    tot , i ,
    j , p1 , p2 ,
    sum        : longint;
begin
    qk_sort(1 , N);
    tot := 1; starting[1] := 1;
    for i := 2 to N do
      if data[i].x <> data[starting[tot]].x then
        begin
            inc(tot);
            starting[tot] := i;
        end;

    answer := 0;
    for i := 1 to tot do
      for j := i + 1 to tot do
        begin
            sum := 0;
            p1 := starting[i]; p2 := starting[j];
            while (p1 <= N) and (data[p1].x = data[starting[i]].x) and (p2 <= N) and (data[p2].x = data[starting[j]].x) do
              begin
                  if data[p1].y = data[p2].y
                    then begin
                             inc(p1); inc(p2); inc(sum);
                         end
                    else if data[p1].y < data[p2].y
                           then inc(p1)
                           else inc(p2);
              end;
            inc(answer , (sum - 1) * sum div 2);
        end;
end;

procedure out;
begin
    writeln('Case ' , nowcase , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
