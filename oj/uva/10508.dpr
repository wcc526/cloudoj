{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10508.in';
    OutFile    = 'p10508.out';
    Limit      = 1000;

Type
    Tword      = array[1..Limit] of char;
    Tdata      = array[1..Limit] of Tword;
    Tdiff      = array[1..Limit] of longint;

Var
    first      : Tword;
    data       : Tdata;
    diff       : Tdiff;
    N , M      : longint;

function init : boolean;
var
    i , j      : longint;
begin
    if eof
      then init := false
      else begin
               init := true;
               readln(N , M);
               for j := 1 to M do read(first[j]);
               readln;
               for i := 1 to M do
                 begin
                     for j := 1 to M do read(data[i , j]);
                     readln;
                 end;
           end;
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(diff , sizeof(diff) , 0);
    for i := 1 to M do
      for j := 1 to M do
        if first[j] <> data[i , j] then
          inc(diff[i]);
end;

procedure print(w : Tword);
var
    i          : longint;
begin
    for i := 1 to M do
      write(w[i]);
    writeln;
end;

procedure out;
var
    i , j      : longint;
begin
    print(first);
    for i := 1 to M do
      for j := 1 to M do
        if diff[j] = i then
          begin
              print(data[j]);
              break;
          end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
