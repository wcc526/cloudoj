{$I-}
Const
    LimitSave  = 260000;
    Limit      = 22000;
    minimum    = 1e-16;

Type
    Tprimes    = array[1..Limit] of longint;
    spint      = object
                     num                     : extended;
                     power                   : longint;
                     procedure add(num1 , num2 : spint);
                     procedure print;
                 end;
    Tfib       = array[1..LimitSave] of spint;

Var
    fib        : Tfib;
    primes     : Tprimes;
    P , N      : longint;

procedure spint.add(num1 , num2 : spint);
var
    tmp        : spint;
begin
    if num1.power < num2.power
      then begin tmp := num1; num1 := num2; num2 := tmp; end;
    while (num2.power < num1.power) and (num2.num > minimum) do
      begin
          inc(num2.power); num2.num := num2.num / 10;
      end;
    num := num1.num + num2.num; power := num1.power;
    while num >= 10 do
      begin
          num := num / 10 ; inc(power);
      end;
end;

procedure spint.print;
var
    i          : longint;
    tmp        : extended;
begin
    if power <= 8
      then begin
               tmp := num;
               for i := 1 to power do tmp := tmp * 10;
               writeln(trunc(tmp + 1e-8));
           end
      else begin
               tmp := num * 100000000;
               writeln(trunc(tmp + 1e-8));
           end;
end;

function check(num : longint) : boolean;
var
    i          : longint;
begin
    if not odd(num)
      then exit(num = 4)
      else for i := 3 to trunc(sqrt(num)) do
             if num mod i = 0 then exit(false);
    exit(true);
end;

procedure pre_process;
var
    i , j      : longint;
begin
    P := 0;
    for i := 2 to LimitSave do
      if check(i) then
        begin
            inc(P); primes[P] := i;
            if P >= Limit then break;
        end;
    fib[1].num := 1; fib[1].power := 0; fib[2].num := 1; fib[2].power := 0;
    for i := 3 to primes[P] do
      fib[i].add(fib[i - 1] , fib[i - 2]);
end;

Begin
    pre_process;
    while not eof do
      begin
          readln(N);
          if N = 0 then break;
          fib[primes[N]].print;
      end;
End.