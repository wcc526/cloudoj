Const
    InFile     = 'p10743.in';
    OutFile    = 'p10743.out';
    Size       = 3;
    Modulo     = 10000;

Type
    Tmatrix    = array[1..Size , 1..Size] of longint;

Const
    A          : Tmatrix
               = ((0 , 0 , 4) ,
                  (1 , 0 , -7) ,
                  (0 , 1 , 5));

Var
    N , T ,
    cases      : longint;

procedure times(A , B : Tmatrix; var C : Tmatrix);
var
    i , j , k  : longint;
begin
    fillchar(C , sizeof(C) , 0);
    for i := 1 to Size do
      for j := 1 to Size do
        for k := 1 to Size do
          C[i , j] := (C[i , j] + A[i , k] * B[k , j]) mod Modulo;
end;

procedure process;
var
    M          : longint;
    s          : string[200];
    answer ,
    power      : Tmatrix;
begin
    M := N;
    if M <= 4 then
      begin
          case M of
            1  : writeln(1);
            2  : writeln(2);
            3  : writeln(6);
            4  : writeln(19);
          end;
          exit;
      end;
    dec(M , 4);
    fillchar(answer , sizeof(answer) , 0);
    answer[1 , 1] := 2; answer[1 , 2] := 6; answer[1 , 3] := 19;
    power := A;
    while M > 0 do
      begin
          if odd(M) then
            times(answer , power , answer);
          M := M div 2;
          times(power , power , power);
      end;
    str(answer[1 , 3] , s);
    if N >= 10 then while length(s) < 4 do s := '0' + s;
    writeln(s);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      for cases := 1 to T do
        begin
            readln(N);
            write('Case ' , cases , ': ');
            process;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.