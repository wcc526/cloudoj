#include <stdio.h>
#include <string.h>

int abs(int x)
{
  return x < 0 ? -x : x;
}

int main()
{
  int i, j, n;
  char l[7], r[7], d[5], s[12], *p;

  scanf("%d", &n);
  while (n--) {
    memset(s, -128, sizeof(s));
    for (j = 3; j--;) {
      scanf("%s %s %s", l, r, d);
      switch (d[0]) {
      case 'e':
        for (p = l; *p; ++p)
          s[*p - 'A'] = 0;
        for (p = r; *p; ++p)
          s[*p - 'A'] = 0;
        break;
      case 'u':
        for (p = l; *p; ++p) {
          if (s[*p - 'A']) {
            if (s[*p - 'A'] > -128)
              s[*p - 'A']++;
            else
              s[*p - 'A'] = 1;
          }
        }
        for (p = r; *p; ++p) {
          if (s[*p - 'A']) {
            if (s[*p - 'A'] > -128)
              s[*p - 'A']--;
            else
              s[*p - 'A'] = -1;
          }
        }
        break;
      case 'd':
        for (p = l; *p; ++p) {
          if (s[*p - 'A']) {
            if (s[*p - 'A'] > -128)
              s[*p - 'A']--;
            else
              s[*p - 'A'] = -1;
          }
        }
        for (p = r; *p; ++p) {
          if (s[*p - 'A']) {
            if (s[*p - 'A'] > -128)
              s[*p - 'A']++;
            else
              s[*p - 'A'] = 1;
          }
        }
        break;
      }
    }
    for (j = 0, i = 12; i--;)
      if (s[i] > -128 && abs(s[j]) < abs(s[i]))
        j = i;
    putchar(j + 'A');
    puts(s[j] < 0 ?
         " is the counterfeit coin and it is light." :
         " is the counterfeit coin and it is heavy.");
  }

  return 0;
}
