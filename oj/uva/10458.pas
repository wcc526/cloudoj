Const
    InFile     = 'p10458.in';
    OutFile    = 'p10458.out';
    LimitK     = 7;
    LimitLen   = 100;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num : lint);
                     procedure minus(num : lint);
                     procedure times(num : comp);
                     procedure divide(num : comp);
                     procedure wipe_zero;
                     procedure print;
                 end;
    Tdata      = array[1..LimitK] of comp;
    Tcount     = array[0..1 shl LimitK - 1] of longint;

Var
    data       : Tdata;
    count      : Tcount;
    answer     : lint;
    N          : comp;
    K          : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.add(num : lint);
var
    tmp ,
    i , jw     : longint;
begin
    i := 1; jw := 0;
    while (i <= num.len) or (i <= len) or (jw <> 0) do
      begin
          tmp := jw + num.data[i] + data[i];
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    dec(i);
    len := i;
end;

procedure lint.minus(num : lint);
var
    tmp ,
    i , jw     : longint;
begin
    i := 1; jw := 0;
    while (i <= num.len) or (jw <> 0) do
      begin
          tmp := data[i] - num.data[i] - jw;
          jw := 0;
          if tmp < 0 then begin jw := 1; inc(tmp , 10); end;
          data[i] := tmp;
          inc(i);
      end;
    wipe_zero;
end;

procedure lint.times(num : comp);
var
    tmp , jw   : comp;
    i          : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := int(tmp / 10);
          data[i] := round(tmp - jw * 10);
          inc(i);
      end;
    len := i - 1;
    wipe_zero;
end;

procedure lint.divide(num : comp);
var
    last       : comp;
    i          : longint;
begin
    last := 0;
    for i := len downto 1 do
      begin
          last := last * 10 + data[i];
          data[i] := trunc(last / num);
          last := last - int(last / num) * num;
      end;
    wipe_zero;
end;

procedure lint.wipe_zero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

procedure init;
var
    i          : longint;
    p1 , p2    : comp;
begin
    readln(K , N);
    for i := 1 to K do
      begin
          read(p1 , p2); N := N - p1;
          data[i] := p2 - p1;
      end;
    readln;
end;

procedure Get_Num(status : longint; var res : lint);
var
    tN         : comp;
    i          : longint;
begin
    tN := N;
    for i := 1 to K do
      if (1 shl (i - 1)) and status <> 0 then
        tN := tN - data[i] - 1;
    res.init;
    if tN < 0 then exit;
    res.data[1] := 1;
    tN := tN + K - 1;
    for i := 0 to K - 2 do
      res.times(tN - i);
    for i := 2 to K - 1 do
      res.divide(i);
end;

procedure work;
var
    i          : longint;
    tmp        : lint;
begin
    count[0] := 0;
    answer.init;
    if N < 0 then exit;
    for i := 1 to K do if data[i] < 0 then exit;
    for i := 1 to 1 shl K - 1 do count[i] := count[i div 2] + i mod 2;
    for i := 0 to 1 shl K - 1 do
      if not odd(count[i]) then
        begin
            Get_Num(i , tmp);
            answer.add(tmp);
        end;
    for i := 0 to 1 shl K - 1 do
      if odd(count[i]) then
        begin
            Get_Num(i , tmp);
            answer.minus(tmp);
        end;
end;

procedure out;
begin
    answer.print;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.