{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10573.in';
    OutFile    = 'p10573.out';

Var
    cases ,
    p1 , p2    : longint;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            dec(Cases);
            read(p1);
            if eoln
              then writeln(p1 * p1 / 16 * pi * 2 : 0 : 4)
              else begin
                       read(p2);
                       writeln(2 * pi * p1 * p2 : 0 : 4);
                   end;
            readln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
