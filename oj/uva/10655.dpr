{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10655.in';
    OutFile    = 'p10655.out';
    LimitSave  = 1000;

Type
    Thistory   = array[0..LimitSave] of extended;

Var
    history    : Thistory;
    p , q ,
    answer     : extended;
    N          : longint;

function init : boolean;
begin
    read(p , q);
    if ((p + q) = 0) and eoln
      then init := false
      else begin
               init := true;
               read(N);
           end;
end;

procedure work;
var
    a , b , c  : extended;
    i , j , k  : longint;
begin
    if N = 0
      then answer := 2
      else begin
               a := 2; b := p; history[0] := 2; history[1] := p;
               for i := 2 to N do
                 begin
                     c := b * p - q * a;
                     history[i] := c;
                     k := -1;
                     for j := 1 to i - 1 do
                       if (history[j - 1] = b) and (history[j] = c) then
                         begin
                             k := j;
                             break;
                         end;
                     if k <> -1 then
                       begin
                           j := i - k;
                           b := history[(N - i) mod j + k];
                           break;
                       end;
                     a := b; b := c;
                 end;
               answer := b;
           end;
end;

procedure print(num : extended);
begin
    if num <> 0 then
      begin
          print(int(num / 10));
          write(num - int(num / 10) * 10 : 0 : 0);
      end;
end;

procedure out;
begin
    if answer < 0
      then begin write('-'); print(-answer); end
      else print(answer);
    if answer = 0 then write(0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
