#include <stdio.h>

int main(void)
{
  int n;
  char m[256];

  for (n = 256; n--;)
    m[n] = n - 7;
  m['\n'] = '\n';

  while ((n = fgetc(stdin)) != EOF)
    putc(m[n], stdout);

  return 0;
}
