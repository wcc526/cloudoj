Const
    InFile     = 'p10227.in';
    Limit      = 100;

Type
    Tdata      = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    cases , answer ,
    N , M      : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(visited , sizeof(visited) , 0);
    readln(N , M);
    while not eoln and not eof do
      begin
          readln(i , j);
          data[i , j] := true;
      end;
end;

function check(i , j : longint) : boolean;
var
    k          : longint;
begin
    for k := 1 to M do
      if data[i , k] <> data[j , k] then
        exit(false);
    exit(true);
end;

procedure work;
var
    i , j      : longint;
begin
    answer := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            inc(answer);
            for j := i + 1 to N do
              if not visited[j] then
                if check(i , j) then
                  visited[j] := true;
        end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            writeln(answer);
            if cases <> 0 then writeln;
        end;
//    Close(INPUT);
End.