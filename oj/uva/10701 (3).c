#include <stdio.h>

#define N 'z' + 1

char l[N], r[N], a[53], b[53];

int build(int u, int v, int x)
{
  int i;
  if (u == v) return 0;
  for (i = v; b[--i] ^ a[x];);
  l[a[x]] = build(u, i, x + 1);
  r[a[x]] = build(i + 1, v, x + i - u + 1);
  return a[x];
}

void print(int i)
{
  if (i == 0) return;
  print(l[i]);
  print(r[i]);
  putchar(i);
}

int main()
{
  int n, t;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %s %s", &n, a, b);
    print(build(0, n, 0));
    putchar('\n');
  }

  return 0;
}
