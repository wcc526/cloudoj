#include <iostream>
#include <string>

using namespace std;

int main()
{
    string line;
    while (getline(cin, line)) {
        int numRepeat = 0;
        char ch;
        for (int i=0; i<line.size(); ++i) {
            if (line[i] >= '0' && line[i] <= '9') {
                numRepeat += (line[i] - '0');
                continue;
            } else if (line[i] == 'b') {
                ch = ' ';
            } else if (line[i] == '!') {
                cout << endl;
            } else {
                ch = line[i];
            }

            for (int j=0; j<numRepeat; ++j) {
                cout << ch;
            }
            numRepeat = 0;
        }
        cout << endl;
    }

    return 0;
}
