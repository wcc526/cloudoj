#include <stdio.h>

int main(void)
{
  int i, n;
  char p[66], s[66], m[256], *c;

  gets(p);
  sscanf(p, "%d", &n);
  gets(p);

  while (n--) {
    gets(p);
    gets(s);
    for (i = 256; i--;)
      m[i] = i;
    for (i = 0; p[i]; ++i)
      m[(unsigned)p[i]] = s[i];
    puts(s);
    puts(p);
    while (*p && gets(p) && *p) {
      for (c = p; *c;)
        putc(m[(unsigned)*c++], stdout);
      putc('\n', stdout);
    }
    if (n)
      putc('\n', stdout);
  }

  return 0;
}
