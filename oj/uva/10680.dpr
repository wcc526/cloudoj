{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10680.in';
    OutFile    = 'p10680.out';
    Limit      = 1000000;
    Base       = 10000;

Type
    Tprime     = array[1..Limit] of boolean;
    Tdata      = array[1..Limit] of longint;

Var
    prime      : Tprime;
    data ,
    times2 ,  
    answer     : Tdata;
    tmp , i ,
    N          : longint;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    fillchar(data , sizeof(data) , $FF);
    fillchar(answer , sizeof(answer) , 0);
    prime[1] := false;
    for i := 2 to Limit do
      if prime[i] then
        begin
            for j := 2 to Limit div i do prime[i * j] := false;
            j := 1;
            while Limit div i >= j do
              begin
                  j := j * i;
                  data[j] := i;
              end;
        end;

    answer[1] := 1; times2[1] := 0;
    for i := 2 to Limit do
      if data[i] = -1
        then begin
                 answer[i] := answer[i - 1];
                 times2[i] := times2[i - 1];
             end
        else begin
                 answer[i] := answer[i - 1];
                 times2[i] := times2[i - 1];
                 if data[i] = 2
                  then times2[i] := times2[i - 1] + 1
                  else if data[i] = 5
                         then times2[i] := times2[i - 1] - 1
                         else answer[i] := (data[i] mod Base) * answer[i] mod Base;
             end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      readln(N);
      while N <> 0 do
        begin
            tmp := answer[N] mod 10;
            for i := 1 to times2[N] do
              tmp := tmp * 2 mod 10;
            writeln(tmp);
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
