Const
    InFile     = 'p10479.in';
    OutFile    = 'p10479.out';
    Limit      = 64;

Type
    Tpower     = array[0..Limit] of extended;

Var
    power      : Tpower;
    N          : extended;
    step       : longint;

procedure pre_process;
var
    i          : longint;
begin
    power[0] := 1; power[1] := 1;
    for i := 2 to Limit do power[i] := power[i - 1] * 2;
end;

function calc(root , step : longint; count : extended) : longint;
var
    i          : longint;
begin
    if step = 0
      then exit(root)
      else begin
               for i := 1 to root do
                 if power[step - 1] < count
                   then count := count - power[step - 1]
                   else exit(calc(0 , step - 1 , count));
               exit(calc(root + 1 , step - 1 , count));
           end;
end;

Begin
    pre_process;
    readln(N);
    while N <> 0 do
      begin
          step := 0;
          while N > power[step] do
            begin
                N := N - power[step];
                inc(step);
            end;
          writeln(calc(0 , step , N));
          readln(N);
      end;
End.