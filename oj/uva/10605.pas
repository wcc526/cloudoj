{ $A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{ $MINSTACKSIZE $00004000}
{ $MAXSTACKSIZE $00100000}
{ $IMAGEBASE $00400000}
{ $APPTYPE GUI}
{ $R+,Q+,S+}
{ $R-,Q-,S-}
Const
    InFile     = 'p10605.in';
    OutFile    = 'p10605.out';
    Limit      = 11;
    LimitHash  = 399997;
    Base       = 1 shl Limit;
    dirx       : array[1..4] of longint = (-1 , 1 , 0 , 0);
    diry       : array[1..4] of longint = (0 , 0 , -1 , 1);

Type
    Ttree      = array[#64..'J' , #64..'J'] of longint;
    Tdata      = array[0..Limit + 1 , 0..Limit + 1] of char;
    Tmap       = array[1..Limit] of integer;
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tqueue     = array[1..Limit * Limit] of Tpoint;
    Thash      = array[0..LimitHash] of
                   record
                       used   : longint;
                       map    : Tmap;
                   end;

Var
    tree       : Ttree;
    data       : Tdata;
    queue      : Tqueue;
//    hash       : Thash;
    N , M , T ,
    tot , sum ,
    hashusage ,
    bound      : longint;

procedure init;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , '#');
    fillchar(queue , sizeof(queue) , 0);
//    fillchar(hash , sizeof(hash) , $FF);
    tot := 0; sum := 0; dec(T);
    readln(N , M);
    for i := 1 to N do
      begin
          for j := 1 to M do
            begin
                read(data[i , j]);
                if data[i , j] = '*' then
                  begin
                      inc(sum);
                      data[i , j] := chr(sum + ord('A') - 1);
                  end;
            end;
          readln;
      end;
end;

function same_map(const map1 , map2 : Tmap) : boolean;
var
    i          : longint;
begin
    same_map := false;
    for i := 1 to N do
      if map1[i] <> map2[i] then
        exit;
    same_map := true;
end;

{function add_on : boolean;
var
    tmpmap     : Tmap;
    i , j , p  : longint;
begin
    fillchar(tmpmap , sizeof(tmpmap) , 0);
    p := 0;
    for i := 1 to N do
      for j := 1 to M do
        begin
            tmpmap[i] := tmpmap[i] * 2;
            if (data[i , j] = '#') or (data[i , j] = '^') then inc(tmpmap[i]);
            p := (p * base + tmpmap[i]) mod LimitHash;
        end;
    add_on := false;
    while hash[p].used = bound do
      begin
          if same_map(hash[p].map , tmpmap) then exit;
          inc(p); if p = LimitHash then p := 0;
      end;
    hash[p].used := bound;
    hash[p].map := tmpmap;
//    for i := 1 to N do
//      begin
//          for j := 1 to M do
//            write(data[i , j]);
//          writeln;
//      end;
//    writeln;
    inc(hashusage);
    add_on := true;
end;          }

procedure flood_fill(x0 , y0 : longint);
type
    Tvisited   = array[1..Limit , 1..Limit] of longint;
var
    q          : Tqueue;
    visited    : Tvisited;
    open , closed ,
    i ,
    nx , ny    : longint;
begin
    open := 1; closed := 1;
    fillchar(visited , sizeof(visited) , $FF);
    visited[x0 , y0] := 0;
    q[1].x := x0; q[1].y := y0;
    while open <= closed do
      begin
          for i := 1 to 4 do
            begin
                nx := q[open].x + dirx[i]; ny := q[open].y + diry[i];
                if data[nx , ny] = '#'
                  then begin
                           if visited[q[open].x , q[open].y] + 1 < tree[data[x0 , y0] , #64]
                             then begin
                                      tree[data[x0 , y0] , #64] := visited[q[open].x , q[open].y] + 1;
                                      tree[#64 , data[x0 , y0]] := visited[q[open].x , q[open].y] + 1;
                                  end;
                       end
                  else if (data[nx , ny] <> '^') and (visited[nx , ny] = -1)
                         then begin
                                  visited[nx , ny] := visited[q[open].x , q[open].y] + 1;
                                  if data[nx , ny] <> '.' then
                                    begin
                                        tree[data[x0 , y0] , data[nx , ny]] := visited[nx , ny];
                                        tree[data[nx , ny] , data[x0 , y0]] := visited[nx , ny];
                                    end;
                                  inc(closed);
                                  q[closed].x := nx; q[closed].y := ny;
                              end;
            end;
          inc(open);
      end;
end;

function estimate : longint;
type
    Tappeared  = array[#64..'J'] of boolean;
    Tmin       = array[#64..'J'] of longint;

var
    appeared ,
    v          : Tappeared;
    min        : Tmin;
    i , j , res: longint;
    c1 , c2    : char;
begin
    fillchar(appeared , sizeof(appeared) , 0);
    fillchar(tree , sizeof(tree) , 1);
    for i := 1 to N do
      for j := 1 to M do
        if (data[i , j] <> '^') and (data[i , j] <> '.') then
          if data[i , j] = '#'
            then appeared[#64] := true
            else appeared[data[i , j]] := true;
    for i := 1 to N do
      for j := 1 to M do
        if (data[i , j] <> '#') and (data[i , j] <> '.') and (data[i , j] <> '^') then
          flood_fill(i , j);
    fillchar(v , sizeof(v) , 0);
    for c1 := 'A' to 'J' do
      if appeared[c1] then
        min[c1] := tree[#64 , c1];
    v[#64] := true; res := 0;
    for i := 1 to 10 do
      begin
          c2 := #64;
          for c1 := 'A' to 'J' do
            if appeared[c1] and not v[c1] then
              if (c2 = #64) or (min[c2] > min[c1]) then
                c2 := c1;
          if c2 = #64 then break;
          inc(res , min[c2]);
          v[c2] := true;
          for c1 := 'A' to 'J' do
            if appeared[c1] and not v[c1] then
              if tree[c1 , c2] < min[c1] then
                min[c1] := tree[c1 , c2];
      end;
    estimate := res;
end;

function dfs(p , produced : longint) : boolean;
var
    i , j , newp
               : longint;
    c          : char;
begin
    dfs := false;
    if p > tot then exit;
    if produced + estimate > bound then exit;
{    if (produced <= 4) then
      if not add_on then
        exit;}
    data[queue[p].x , queue[p].y] := '^';
    for i := 1 to N do
      for j := 1 to M do
        if data[i , j] = '^' then
          if ord(data[i - 1 , j] = '^') +
             ord(data[i + 1 , j] = '^') +
             ord(data[i , j - 1] = '^') +
             ord(data[i , j + 1] = '^') > 2 then
               begin
                   data[queue[p].x , queue[p].y] := '#';
                   exit;
               end;
    dfs := true;
    if sum = 0 then exit;
    for newp := p to tot do
      for i := 1 to 4 do
        begin
            inc(queue[newp].x , dirx[i]); inc(queue[newp].y , diry[i]);
            if (data[queue[newp].x , queue[newp].y] <> '#') and (data[queue[newp].x , queue[newp].y] <> '^') then
              if data[queue[newp].x , queue[newp].y] <> '.'
                then begin
                         c := data[queue[newp].x , queue[newp].y];
                         data[queue[newp].x , queue[newp].y] := '#';
                         dec(sum);
                         if dfs(newp , produced + 1) then exit;
                         inc(sum);
                         data[queue[newp].x , queue[newp].y] := c;
                     end
                else begin
                         data[queue[newp].x , queue[newp].y] := '#';
                         if dfs(newp , produced + 1) then exit;
                         data[queue[newp].x , queue[newp].y] := '.';
                     end;
            dec(queue[newp].x , dirx[i]); dec(queue[newp].y , diry[i]);
        end;
    dfs := false;
    data[queue[p].x , queue[p].y] := '#';
end;

procedure work;
var
    i , j , k ,
    nx , ny    : longint;
begin
    for i := 1 to N do
      for j := 1 to M do
        if data[i , j] = '#' then
          for k := 1 to 4 do
            begin
                nx := i + dirx[k]; ny := j + diry[k];
                if data[nx , ny] <> '#' then
                  begin
                      inc(tot);
                      queue[tot].x := i; queue[tot].y := j;
                      break;
                  end;
            end;

    bound := 0;
    hashusage := 0;
    while not dfs(1 , 0) do
      begin
          inc(bound);
          writeln(bound);
{          writeln('Hash used : ' , hashusage , '/' , LimitHash , '   ' , hashusage / LimitHash : 0 : 2);
          writeln(bound);}
          hashusage := 0;
     end;
end;

procedure out;
var
    f          : text;
begin
    writeln(bound);
{    assign(f , ''); reset(f);
      readln(f);
    close(f);}
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
