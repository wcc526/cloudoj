Const
    InFile     = 'p10724.in';
    OutFile    = 'p10724.out';
    Limit      = 50;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tmap       = array[1..Limit , 1..Limit] of double;

Var
    data       : Tdata;
    map        : Tmap;
    answer     : double;
    bestu , bestv ,
    N          : longint;

function init : boolean;
var
    p1 , p2 , M ,
    i , j      : longint;
begin
    readln(N , M);
    if N + M = 0 then exit(false);
    init := true;
    fillchar(data , sizeof(data) , 0);
    fillchar(map , sizeof(map) , 0);
    answer := 0;
    for i := 1 to N do
      for j := 1 to N do
        if i <> j then
          map[i , j] := 1e20;
    for i := 1 to N do read(data[i].x , data[i].y);
    for i := 1 to M do
      begin
          read(p1 , p2);
          map[p1 , p2] := sqrt(sqr(data[p1].x - data[p2].x) + sqr(data[p1].y - data[p2].y));
          map[p2 , p1] := map[p1 , p2];
      end;
end;

procedure work;
var
    i , j , k ,
    p1 , p2    : longint;
    t1 , t2 ,
    adddist ,
    sumdist    : double;
begin
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          if map[i , k] + map[k , j] < map[i , j]
            then map[i , j] := map[i , k] + map[k , j];
    for i := 1 to N do
      for j := i + 1 to N do
        begin
            adddist := sqrt(sqr(data[i].x - data[j].x) + sqr(data[i].y - data[j].y));
            sumdist := 0;
            for p1 := 1 to N do
              for p2 := 1 to N do
                begin
                    t1 := map[p1 , i] + adddist + map[j , p2];
                    t2 := map[p1 , j] + adddist + map[i , p2];
                    if t1 > t2 then t1 := t2;
                    if t1 < map[p1 , p2] then
                      sumdist := sumdist + map[p1 , p2] - t1;
                end;
            if sumdist > answer
              then begin answer := sumdist; bestu := i; bestv := j; end;
        end;
end;

procedure out;
begin
    if answer <= 1
      then writeln('No road required')
      else writeln(bestu , ' ' , bestv);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            out;
        end;
End.