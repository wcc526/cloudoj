{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10606.in';
    OutFile    = 'p10606.out';
    LimitLen   = 200;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure read_num;
                     procedure wipezero;
                     function bigger(num : lint) : boolean;
                     procedure minus(num1 , num2 : lint);
                     procedure multi(num1 : lint; num2 : longint);
                     procedure Get_Sqrt(num : lint);
                     procedure multiple(num1 , num2 : lint);
                     procedure shift(num : longint);
                     procedure print;
                 end;

Var
    N , answer : lint;

procedure lint.init;
begin
    len := 0;
    fillchar(data , sizeof(data) , 0);
end;

procedure lint.read_num;
var
    i , tmp    : longint;
    ch         : char;
begin
    init;
    while not eoln do
      begin
          inc(len); read(ch);
          data[len] := ord(ch) - ord('0');
      end;
    for i := 1 to len div 2 do
      begin
          tmp := data[i]; data[i] := data[len - i + 1];
          data[len - i + 1] := tmp;
      end;
    readln;
    wipezero;
end;

procedure lint.wipezero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

function lint.bigger(num : lint) : boolean;
var
    i          : longint;
begin
    if num.len <> len
      then bigger := len > num.len
      else begin
               bigger := false;
               for i := len downto 1 do
                 if data[i] <> num.data[i] then
                   begin
                       bigger := data[i] > num.data[i];
                       exit;
                   end;
           end;
end;

procedure lint.minus(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while i <= num1.len do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0;
          if tmp < 0 then begin inc(tmp , 10); jw := 1; end;
          data[i] := tmp;
          inc(i);
      end;
    len := num1.len;
    wipezero;
end;

procedure lint.multi(num1 : lint; num2 : longint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] * num2 + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
    wipezero;
end;

procedure lint.multiple(num1 , num2 : lint);
var
    i , jw , j ,
    tmp        : longint;
begin
    init;
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := data[i + j - 1] + num1.data[i] * num2.data[j] + jw;
                data[i + j - 1] := tmp mod 10;
                jw := tmp div 10;
                inc(j);
            end;
          if i + j - 2 > len then len := i + j - 2;
      end;
    wipezero;
end;

procedure lint.shift(num : longint);
var
    i          : longint;
begin
    for i := len downto 1 do data[i + num] := data[i];
    for i := num downto 1 do data[i] := 0;
    inc(len , num);
end;

procedure lint.Get_Sqrt(num : lint);
var
    i , j      : longint;
    tmp , t2   : lint;
begin
    init;
    len := (num.len + 1) div 2;
    tmp.init; tmp.len := len;
    for i := len downto 1 do
      begin
          j := 1; tmp.multi(self , 2); if tmp.len < i then tmp.len := i;
          while (j <= 9) do
            begin
                tmp.data[i] := j;
                t2 := tmp; t2.shift(i - 1);
                t2.multi(t2 , j);
                if t2.bigger(N) then break;
                inc(j);
            end;
          dec(j); tmp.data[i] := j;
          t2 := tmp; t2.shift(i - 1);
          t2.multi(t2 , j);
          N.minus(N , t2);
          data[i] := j;
      end;
    wipezero;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do
      write(data[i]);
    if len = 0 then write(0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while true do
        begin
            N.read_num;
            if (N.len = 1) and (N.data[1] = 0) then break;
            answer.Get_Sqrt(N);
            answer.multiple(answer , answer);
            answer.print;
            writeln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
