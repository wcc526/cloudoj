{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10517.in';
    OutFile    = 'p10517.out';
    R          : extended = 6378;

Var
    n , m ,
    answer     : extended;
    T , nowCase: longint;

procedure init;
begin
    readln(N , M);
    inc(nowCase);
end;

procedure work;
var
     cosB , sinB ,
     tgA , beta
               : extended;
begin
    beta := n / R;
    cosB := cos(beta); sinB := sin(beta);
    if (abs(M) <= 1e-5) or (abs(sinB) <= 1e-5)
      then answer := -1
      else begin
               tgA := (cosB - n / m) / sinB;
               answer := arctan(tgA);
               if (answer < 0) or (answer > 70 / 180 * pi) then
                 answer := -1;
           end;
end;

procedure out;
var
    tmp ,
    a , b , c  : longint;
begin
    write('Case ' , nowCase , ': ');
    if (answer < 0) or (n >= 1001.0)
      then writeln('Invalid or impossible.')
      else begin
               answer := answer / pi * 180;
               tmp := round(answer * 3600);
               c := tmp mod 60; tmp := tmp div 60;
               b := tmp mod 60; tmp := tmp div 60;
               a := tmp;
               writeln(a , ' deg ' , b , ' min ' , c , ' sec');
           end;            
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T); nowCase := 0;
      while nowCase < T do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
