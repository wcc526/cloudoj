Const
    InFile     = 'p10256.in';
    Limit      = 500;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..2 , 1..Limit] of Tpoint;
    Ttot       = array[1..2] of longint;

Var
    data       : Tdata;
    tot        : Ttot;

function init : boolean;
var
    i , j      : longint;
begin
    read(tot[1] , tot[2]);
    if tot[1] + tot[2] = 0 then exit(false);
    init := true;
    for i := 1 to 2 do
      for j := 1 to tot[i] do
        with data[i , j] do
          read(x , y);
end;

function cross(p1 , p2 : Tpoint) : longint;
begin
    exit(p1.x * p2.y - p1.y * p2.x);
end;

function cover(p1 , p2 , p : Tpoint) : boolean;
var
    tmp        : longint;
begin
    tmp := cross(p1 , p2);
    if tmp > 0 then exit(false);
    if tmp < 0
      then exit((cross(p1 , p) <= 0) and (cross(p , p2) <= 0));
    if p1.x * p2.x + p1.y * p2.y > 0
      then exit((cross(p1 , p) <= 0) and (cross(p , p2) <= 0) and (p1.x * p.x + p1.y * p.y > 0))
      else exit((cross(p1 , p) <= 0) and (cross(p , p2) <= 0));
end;

function check_sub(Q , p : longint; vector : Tpoint) : boolean;
var
    tmp        : Tpoint;
    i          : longint;
begin
    for i := 1 to tot[Q] do
      begin
          tmp.x := data[Q , i].x - data[Q , p].x;
          tmp.y := data[Q , i].y - data[Q , p].y;
          if cross(vector , tmp) > 0 then exit(false);
      end;
    for i := 1 to tot[3 - Q] do
      begin
          tmp.x := data[3 - Q , i].x - data[Q , p].x;
          tmp.y := data[3 - Q , i].y - data[Q , p].y;
          if cross(vector , tmp) <= 0 then exit(false);
      end;
    exit(true);
end;

function check(Q , p : longint) : boolean;
var
    tmp ,
    start1 , stop1 ,
    start2 , stop2
               : Tpoint;
    i , j      : longint;
    b1 , b2    : boolean;
begin
    start1.x := -maxlongint;
    for i := 1 to tot[Q] do
      begin
          if start1.x <> -maxlongint
            then if (cross(start1 , stop1) = 0) and (start1.x * stop1.x + start1.y * stop1.y < 0) then
              begin
                  j := 1;
                  while j <= tot[Q] do
                    begin
                        tmp.x := data[Q , j].x - data[Q , p].x;
                        tmp.y := data[Q , j].y - data[Q , p].y;
                        if not cover(start1 , stop1 , tmp) then
                          break;
                        inc(j);
                    end;
                  b1 := (j > tot[Q]);

                  j := 2;
                  while j <= tot[Q] do
                    begin
                        tmp.x := data[Q , j].x - data[Q , p].x;
                        tmp.y := data[Q , j].y - data[Q , p].y;
                        if not cover(stop1 , start1 , tmp) then
                          break;
                        inc(j);
                    end;
                  b2 := (j > tot[Q]);

                  if not b1 and not b2 then exit(false);
                  if not b1 then
                    begin
                        tmp := start1; start1 := stop1; stop1 := tmp;
                        b1 := true; b2 := false;
                    end;
                  break;
              end;
          tmp.x := data[Q , i].x - data[Q , p].x;
          tmp.y := data[Q , i].y - data[Q , p].y;
          if sqr(tmp.x) + sqr(tmp.y) = 0 then continue;
          if start1.x = -maxlongint
            then begin start1 := tmp; stop1 := tmp; continue; end;
          if cover(start1 , stop1 , tmp) then continue;
          if cover(start1 , tmp , stop1) then begin stop1 := tmp; continue; end;
          if cover(tmp , stop1 , start1) then begin start1 := tmp; continue; end;
          exit(false);
      end;

    start2.x := -maxlongint;
    for i := 1 to tot[3 - Q] do
      begin
          tmp.x := data[3 - Q , i].x - data[Q , p].x;
          tmp.y := data[3 - Q , i].y - data[Q , p].y;
          if sqr(tmp.x) + sqr(tmp.y) = 0 then exit(false);
          if start2.x = -maxlongint
            then begin start2 := tmp; stop2 := tmp; continue; end;
          if cover(start2 , stop2 , tmp) then continue;
          if cover(start2 , tmp , stop2) then begin stop2 := tmp; continue; end;
          if cover(tmp , stop2 , start2) then begin start2 := tmp; continue; end;
          exit(false);
      end;
    if start1.x = -maxlongint then exit(false);
    if (cross(start2 , stop2) = 0) and (start2.x * stop2.x + start2.y * stop2.y < 0) then exit(false);
    if cover(start2 , stop2 , start1) then exit(false);
    if cover(start2 , stop2 , stop1) then exit(false);

    if not cover(start1 , stop1 , start2) and not cover(start1 , stop1 , stop2) then exit(true);
    if (cross(start1 , stop1) = 0) and (start1.x * stop1.x + start1.y * stop1.y < 0) and b2 then
      if not cover(stop1 , start1 , start2) and not cover(stop1 , start1 , stop2)
        then exit(true);
    exit(false);
end;

procedure workout;
var
    p          : Tpoint;
    i , j , k  : longint;
begin
    for i := 1 to 2 do
      begin
          j := 1;
          while j <= tot[i] do
            begin
                for k := 1 to j - 1 do
                  if (data[i , j].x = data[i , k].x) and (data[i , j].y = data[i , k].y) then
                    begin
                        data[i , j] := data[i , tot[i]];
                        dec(j); dec(tot[i]);
                        break;
                    end;
                inc(j);
            end;
      end;

    for i := 1 to 2 do
      for j := 1 to tot[i] do
        if check(i , j) then
          begin
              writeln('Yes');
              exit;
          end;
    if (tot[1] = 1) and (tot[2] = 1) then
      if (data[1 , 1].x <> data[2 , 1].x) or (data[1 , 1].y <> data[2 , 1].y)
//    if (tot[1] <= 1) or (tot[2] <= 1)
        then begin writeln('Yes'); exit; end;
    writeln('No');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        workout;
//    Close(INPUT);
End.
