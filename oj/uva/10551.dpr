{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10551.in';
    OutFile    = 'p10551.out';
    LimitLen   = 1200;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                 end;

Var
    b          : longint;
    m , answer : comp;
    p          : lint;

function init : boolean;
var
    c          : char;
begin
    read(b);
    if b = 0
      then init := false
      else begin
               init := true;
               read(c);
               while c = ' ' do read(c);
               fillchar(p , sizeof(p) , 0);
               while c <> ' ' do
                 begin
                     inc(p.len);
                     p.data[p.len] := ord(c) - ord('0');
                     read(c);
                 end;
               read(c);
               while c = ' ' do read(c);
               m := 0;
               while c <> ' ' do
                 begin
                     m := m * b + ord(c) - ord('0');
                     if eoln then break else read(c);
                 end;
               readln;
           end;
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := 1 to p.len do
      begin
          answer := answer * b + p.data[i];
          answer := answer - int(answer / m) * m; 
      end;
end;

procedure print_num(number : longint);
begin
    if number = 0
      then exit
      else begin
               print_num(number div b);
               write(number mod b);
           end;
end;

procedure out;
begin
    print_num(trunc(answer));
    if answer = 0 then write(0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
