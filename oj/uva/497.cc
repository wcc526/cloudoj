#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

void lis(vector<int> &a, vector<int> &b)
{
	vector<int> p(a.size());
	vector<int>::size_type u, v;

  b.clear();

	if (a.size() < 1)
    return;

	b.push_back(0);

	for (vector<int>::size_type i = 1; i < a.size(); i++) {
		if (a[b.back()] < a[i]) {
			p[i] = b.back();
			b.push_back(i);
			continue;
		}

		for (u = 0, v = b.size() - 1; u < v;) {
      vector<int>::size_type c = (u + v) >> 1;
			if (a[b[c]] < a[i])
        u = c + 1;
      else
        v = c;
		}

		if (u <= v && a[i] < a[b[u]]) {
			if (u > 0)
        p[i] = b[u - 1];
			b[u] = i;
		}
	}

	for (u = b.size(), v = b.back(); u--; v = p[v])
    b[u] = v;
}

int main()
{
  vector<int> a, b;
  int i, n;
  char s[32];

  cin.getline(s, 32);
  sscanf(s, "%d", &n);
  cin.getline(s, 32);
  while (n--) {
    while (cin.getline(s, 32) && *s)
      sscanf(s, "%d", &i), a.push_back(i);
    lis(a, b);
    printf("Max hits: %u\n", b.size());
    for (vector<int>::size_type j = 0; j < b.size(); ++j)
      printf("%d\n", a[b[j]]);
    if (n) {
      putchar('\n');
      a.clear();
    }
  }

  return 0;
}
