Var
    i ,
    N , M ,
    res        : extended;

Begin
    while not eof do
      begin
          readln(N , M);
          res := 0;
          i := N - M + 1;
          while i <= N do
            begin res := res + ln(i); i := i + 1; end;
          i := 1;
          while i <= M do
            begin res := res - ln(i); i := i + 1; end;
          res := res / ln(10);
          writeln(trunc(res + 1e-6) + 1);
      end;
End.