Var
    i , j , tot ,
    amin , amax ,
    bmin , bmax ,
    mmin , mmax ,
    T , k      : longint;
    answer     : extended;
Begin
    readln(T);
    for k := 1 to T do
      begin
          answer := 0;
          readln(amin , amax , bmin , bmax , mmin , mmax);
          for i := bmin to bmax do
            for j := mmin to mmax do
              if i * 2 mod j = 0
                then answer := answer + 1;
          writeln('Case ' , k , ': ' , answer * (amax - amin + 1) : 0 : 0);
      end;
End.