Var
    cases ,
    low , high , K ,
    N , x      : longint;
    p , q      : extended;
Begin
    readln(low , high , K);
    while low <> 0 do
      begin
          inc(Cases);
          N := high - low;
          x := k - 1;
          if (k - 1) mod 2 <> 0
            then x := x * 2;
          p := N div x; q := N mod x;
          writeln('Case ' , cases , ': ' , x * (p - 1) * p / 2 + (q + 1) * p : 0 : 0);
          readln(low , high , K);
      end;
End.