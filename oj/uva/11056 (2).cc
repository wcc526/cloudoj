#include <algorithm>
#include <cstdio>

using namespace std;

bool strcmpi(const char *l, const char *r)
{
  while ((*l | 0x20) == (*r | 0x20))
    ++l, ++r;
  return (*l | 0x20) < (*r | 0x20);
}

struct driver
{
  char name[21];
  int time;
  bool operator<(const driver &r) const {
    if (time < r.time)
      return true;
    else if (time > r.time)
      return false;
    return strcmpi(name, r.name);
  }
};

int main()
{
  driver d[100];
  int i, n, r, m, s;

  while (scanf("%d", &n) == 1) {
    for (i = n; i--;) {
      scanf("%s %*s %d %*s %d %*s %d %*s", d[i].name, &m, &s, &d[i].time);
      d[i].time += (m * 60 + s) * 1000;
    }
    sort(d, d + n);
    for (r = i = 0; i < n; ++i) {
      if (!(i & 1))
        printf("Row %d\n", ++r);
      puts(d[i].name);
    }
    putchar('\n');
  }

  return 0;
}
