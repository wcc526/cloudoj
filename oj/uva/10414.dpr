{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10414.in';
    OutFile    = 'p10414.out';
    Limit      = 20;
    LimitSave  = 10000;
    Corner     = 20;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tanswer    = record
                     tot      : longint;
                     data     : array[1..LimitSave] of char;
                 end;
    Tvisited   = array[-Corner..Corner , -Corner..Corner] of boolean;
    Tqueue     = array[1..Corner * Corner * 5] of
                   record
                       p      : Tpoint;
                       father : longint;
                   end;

Var
    target , 
    data       : Tdata;
    answer     : Tanswer;
    p          : Tpoint;
    queue      : Tqueue;
    visited    : Tvisited;
    cases , N  : longint;

procedure init;
var
    i , j      : longint;
    tmp        : Tpoint;
begin
    dec(Cases);
    answer.tot := 0;
    read(N);
    for i := 1 to N do read(data[i].x , data[i].y);
    for i := 1 to N do read(target[i].x , target[i].y);
    for i := 1 to N do
      for j := i + 1 to N do
        if (target[i].x > target[j].x) or (target[i].x = target[j].x) and (target[i].y > target[j].y) then
          begin
              tmp := target[i]; target[i] := target[j]; target[j] := tmp;
          end;
    p.x := 0; p.y := 0;
end;

procedure ins(ch : char);
begin
    inc(answer.tot);
    answer.data[answer.tot] := ch;
end;

procedure Goto_XY(targetX , targetY : longint);
begin
    if odd(p.x) then begin ins('D'); inc(p.x); end;
    if odd(p.y) then begin ins('L'); inc(p.y); end;
    while p.x <> targetX do
      if p.x < targetX
        then begin ins('D'); inc(p.x); end
        else begin ins('U'); dec(p.x); end;
    while p.y <> targetY do
      if p.y < targetY
        then begin ins('L'); inc(p.y); end
        else begin ins('R'); dec(p.y); end;
end;

procedure bfs(startx , starty : longint);
var
    open , closed ,
    i          : longint;
    p , np     : Tpoint;
begin
    open := 1; closed := 1;
    visited[startx , starty] := true;
    queue[open].p.x := startx; queue[open].p.y := starty;
    queue[open].father := 0;
    while open <= closed do
      begin
          p := queue[open].p;
          for i := 1 to 4 do
            begin
                np.x := p.x + dirx[i]; np.y := p.y + diry[i];
                if (abs(np.x) <= corner) and (abs(np.y) <= corner) then
                  if not visited[np.x , np.y] then
                    begin
                        visited[np.x , np.y] := true;
                        inc(closed);
                        queue[closed].father := open;
                        queue[closed].p := np;
                        if (np.x = 0) and (np.y = 0) then
                          break;
                    end;
            end;
          if (queue[closed].p.x = 0) and (queue[closed].p.y = 0) then
            break;
          inc(open);
      end;
    while queue[closed].father <> 0 do
      begin
          p := queue[closed].p; np := queue[queue[closed].father].p;
          closed := queue[closed].father;
          if p.x = np.x
            then if p.y < np.y then ins('L')
                               else ins('R')
            else if p.x < np.x then ins('D')
                               else ins('U'); 
      end;
end;

procedure work;
var
    i , j , tmp: longint;
begin
    for i := 1 to N do
      begin
          Goto_XY(data[i].x + 1 , data[i].y);
          tmp := data[i].x;
          for j := tmp downto (target[i].x - target[1].x - Corner) + 2 do
            begin
                ins('U');
                dec(p.x); dec(data[i].x);
            end;
          ins('L'); ins('U'); inc(p.y); dec(p.x);
          
          tmp := data[i].y;
          for j := tmp downto (target[i].y - target[1].y - Corner) + 1 do
            begin
                ins('R');
                dec(p.y); dec(data[i].y);
            end;

          ins('D'); ins('R'); inc(p.x); dec(p.y);
          ins('U'); dec(p.x); dec(data[i].x);
      end;

    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to N do visited[data[i].x - p.x , data[i].y - p.y] := true;
    bfs(-target[1].x - Corner - p.x , -target[1].y - Corner - p.y);
end;

procedure out;
var
    i          : longint;
begin
    writeln(answer.tot);
    for i := 1 to answer.tot do
      case answer.data[i] of
        'R'    : write('R');
        'L'    : write('L');
        'U'    : write('D');
        'D'    : write('U');
      end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
