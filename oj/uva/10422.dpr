{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10422.in';
    OutFile    = 'p10422.out';
    N          = 5;
    dirx       : array[1..8] of longint = (-1 , -2 , -2 , -1 , 1 , 2 , 2 , 1);
    diry       : array[1..8] of longint = (-2 , -1 , 1 , 2 , 2 , 1 , -1 , -2);

Type
    Tdata      = array[1..N , 1..N] of longint;

Var
    data       : Tdata;
    answer , 
    Cases      : longint;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
    dec(Cases);
    for i := 1 to N do
      begin
          for j := 1 to N do
            begin
                read(ch);
                case ch of
                  '0'         : data[i , j] := 0;
                  '1'         : data[i , j] := 1;
                  ' '         : data[i , j] := 2;
                end;
            end;
          readln;
      end;
end;

procedure dfs(step : longint);
var
    i , j ,
    x , y ,
    nx , ny    : longint;
begin
    if step >= answer then exit;
    if (data[1 , 1] = 1) and (data[1 , 2] = 1) and (data[1 , 3] = 1) and (data[1 , 4] = 1) and (data[1 , 5] = 1) then
      if (data[2 , 2] = 1) and (data[2 , 3] = 1) and (data[2 , 4] = 1) and (data[2 , 5] = 1) then
        if (data[3 , 4] = 1) and (data[3 , 5] = 1) and (data[4 , 5] = 1) and (data[3 , 3] = 2) then
          begin
              answer := step;
              exit;
          end;
    if step + 1 >= answer then exit;
    x := 0; y := 0;
    for i := 1 to N do
      for j := 1 to N do
        if data[i , j] = 2 then
          begin
              x := i; y := j;
          end;
    for i := 1 to 8 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx <= N) and (nx >= 1) and (ny <= N) and (ny >= 1) then
            begin
                data[x , y] := data[nx , ny]; data[nx , ny] := 2;
                dfs(step + 1);
                data[nx , ny] := data[x , y]; data[x , y] := 2;
            end;
      end;
end;

procedure work;
begin
    answer := 11;
    dfs(0);
end;

procedure out;
begin
    if answer = 11
      then writeln('Unsolvable in less than 11 move(s).')
      else writeln('Solvable in ' , answer , ' move(s).');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
