Const
    A          = 7.02250950343038135;
    B          = 7.51913089063159980;

Var
    R          : extended;

Begin
    while not eof do
      begin
          readln(R);
          writeln(R * A : 0 : 5 , ' ' , R * B : 0 : 5);
      end;
End.