Const
    InFile     = 'p10445.in';
    OutFile    = 'p10445.out';
    Limit      = 20;
    minimum    = 1e-8;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    N          : longint;
    sum ,
    min , max  : double;

function init : boolean;
var
    i          : longint;
begin
    readln(N);
    if N = 0
      then exit(false)
      else begin
               for i := 1 to N do
                 read(data[i].x , data[i].y);
               exit(true);
           end;
end;

function angle(sinA , cosA : double) : double;
begin
    if sinA < -minimum
      then exit(pi * 2 - angle(-sinA , cosA))
      else if cosA < -minimum
             then exit(pi - angle(sinA , -cosA))
             else if abs(cosA) <= minimum
                    then exit(pi / 2)
                    else exit(arctan(sinA / cosA));
end;

function get_angle(p1 , p2 , p3 : Tpoint) : double;
var
    sinA , cosA ,
    a , b      : double;
begin
    p1.x := p1.x - p2.x; p1.y := p1.y - p2.y;
    p3.x := p3.x - p2.x; p3.y := p3.y - p2.y;
    a := sqrt(sqr(p1.x) + sqr(p1.y));
    b := sqrt(sqr(p3.x) + sqr(p3.y));
    cosA := (p1.x * p3.x + p1.y * p3.y) / a / b;
    sinA := (p1.x * p3.y - p1.y * p3.x) / a / b;
    exit(angle(sinA , cosA) / pi * 180);
end;

procedure work;
var
    i          : longint;
    angle , tmp: double;
begin
    min := 1000; max := -1; sum := 0;
    for i := 1 to N do
      begin
          angle := get_angle(data[i] , data[i mod N + 1] , data[(i + 1) mod N + 1]);
          sum := sum + 180 - angle;
          if angle > max then max := angle;
          if angle < min then min := angle;
      end;
    if sum < 0 then
      begin tmp := min; min := max; max := tmp; min := 360 - min; max := 360 - max; end;
end;

procedure out;
begin
    writeln(min : 0 : 6 , ' ' , max : 0 : 6);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          out;
      end;
End.
