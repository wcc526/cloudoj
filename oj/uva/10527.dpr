{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10527.in';
    OutFile    = 'p10527.out';
    LimitLen   = 10000;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     function modulo(num : longint) : longint;
                     procedure division(num : longint);
                     procedure clearzero;
                     procedure print;
                 end;

Var
    N , ans    : lint;

function init : boolean;
var
    c          : char;
    i , tmp    : longint;
begin
    fillchar(N , sizeof(N) , 0);
    fillchar(ans , sizeof(ans) , 0);
    read(c);
    if c = '-'
      then init := false
      else begin
               init := true;
               while c in ['0'..'9'] do
                 begin
                     inc(N.len); N.data[N.len] := ord(c) - ord('0');
                     if not eoln
                       then read(c)
                       else break;
                 end;
               for i := 1 to N.len div 2 do
                 begin
                     tmp := N.data[i]; N.data[i] := N.data[N.len - i + 1];
                     N.data[N.len - i + 1] := tmp;
                 end;
               readln;
           end;
end;

function lint.modulo(num : longint) : longint;
var
    i , last   : longint;
begin
    last := 0;
    for i := len downto 1 do
      last := (last * 10 + data[i]) mod num;
    modulo := last;
end;


procedure lint.division(num : longint);
var
    i , last   : longint;
begin
    last := 0;
    for i := len downto 1 do
      begin
          inc(last , data[i]);
          data[i] := last div num;
          last := last mod num * 10;
      end;
    clearzero;
end;

procedure lint.clearzero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

procedure work;
var
    i          : longint;
begin
    if (N.len = 1) and (N.data[1] = 0)
      then inc(ans.len)
      else for i := 9 downto 2 do
             while N.modulo(i) = 0 do
               begin
                   inc(ans.len); ans.data[ans.len] := i;
                   N.division(i);
               end;
    while ans.len <= 1 do
      begin
          inc(ans.len);
          ans.data[ans.len] := 1;
      end;
end;

procedure out;
begin
    if (N.len = 1) and (N.data[1] <= 1)
      then ans.print
      else write('There is no such number.');
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
