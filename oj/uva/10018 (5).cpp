/**
 * UVa 10018 Reverse and Add
 * Author: chchwy (a) gmail.com
 * Last modified: 2010.07.27
 */
#include<cstdio>

unsigned long reverse(unsigned long n){
    unsigned long rev_n = 0; //WA : int rev_n = 0;
    while(n!=0){
        rev_n = (rev_n*10) + n%10;
        n /= 10;
    }
    return rev_n;
}
inline int is_palindrome(unsigned long n){
    return (n==reverse(n)) ;
}
int reverse_and_add(unsigned long num, unsigned long &result){
    int counter = 0;
    do{
        num += reverse(num);
        counter++;
    } while( !is_palindrome(num) );
    result = num;
    return counter;
}
int main(){
    #ifndef ONLINE_JUDGE
    freopen("10018.in","r", stdin);
    #endif

    int numOfCase;
    scanf("%d", &numOfCase);

    while(numOfCase--){

        unsigned long num;
        scanf("%lu", &num);

        int iteration;        // the number of iterations
        unsigned long result; // the final palindrome, like 9339

        iteration = reverse_and_add(num, result);

        printf("%d %lu\n",iteration,result);
    }
    return 0;
}
