Const
    InFile     = 'p10355.in';
    Limit      = 10;

Type
    Tpoint     = record
                     x , y , z               : double;
                 end;
    Tcircle    = record
                     O                       : Tpoint;
                     R                       : double;
                 end;
    Tdata      = array[1..Limit] of Tcircle;

Var
    data       : Tdata;
    name       : string;
    startp , v : Tpoint;
    N          : longint;
    answer     : double;

procedure init;
var
    i          : longint;
begin
    readln(name);
    readln(startp.x , startp.y , startp.z , v.x , v.y , v.z);
    v.x := v.x - startp.x; v.y := v.y - startp.y; v.z := v.z - startp.z;
    readln(N);
    for i := 1 to N do
      with data[i] do
        readln(O.x , O.y , O.z , R);
end;

function calc(Cir : Tcircle) : double;
var
    A , B , C ,
    delta ,
    x1 , x2    : double;
begin
    A := sqr(v.x) + sqr(v.y) + sqr(v.z);
    B := (startp.x - Cir.O.x) * v.x * 2 + (startp.y - Cir.O.y) * v.y * 2 + (startp.z - Cir.O.z) * v.z * 2;
    C := sqr(startp.x - Cir.O.x) + sqr(startp.y - Cir.O.y) + sqr(startp.z - Cir.O.z) - sqr(Cir.R);
    delta := B * B - 4 * A * C;
    if delta <= 0 then exit(0);
    delta := sqrt(delta);
    x1 := (-B - delta) / 2 / A; x2 := (-B + delta) / 2 / A;
    if x1 < 0 then x1 := 0; if x2 > 1 then x2 := 1;
    if x1 >= x2
      then exit(0)
      else exit(x2 - x1);
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := 1 to N do
      answer := answer + calc(data[i]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            writeln(name);
            writeln(answer * 100 : 0 : 2);
        end;
//    Close(INPUT);
End.