/**
 * UVa 109 SCUD Busters
 * Author: chchwy
 * Last Modified: 2011.10.07
 * Tag: Computational Geometry, Convex Hull
 */
#include<cstdio>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

struct point { int x, y; };

// 兩點距離
int dist (point a, point b) {
    return (b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y);
}

// 外積 (p1,p2) (p1,p3)
int cross (point p1, point p2, point p3) {
    return (p2.x-p1.x)*(p3.y-p1.y) - (p2.y-p1.y)*(p3.x-p1.x);
}

bool min_y (point a, point b) {
    if( a.y == b.y )
        return (a.x < b.x);
    return (a.y < b.y);
}

// Graham's Scan 要將所有的點依照角度排序
point base;
bool counter_clockwise(point a, point b) {

    int c = cross(base, a, b);
    if( c==0 )
        return ( dist(base, a) < dist(base, b));
    return (c > 0);
}

class kingdom {
public:
    vector<point> house;
    bool is_destroyed;

    kingdom();
    void   convex_hull();
    void   destroy(point);
    bool   is_inside(point);
    double area();
};

kingdom::kingdom() { is_destroyed = false; }

// 計算凸包: Graham's Scan Algorithm
void kingdom::convex_hull() {

    swap(house[0], *min_element(house.begin(), house.end(), min_y));

    base = house[0];
    sort(house.begin(), house.end(), counter_clockwise);

    house.push_back(house[0]);

    int CH = 1;
    for(int i=2; i<house.size(); ++i) {

        while( cross(house[CH-1], house[CH], house[i])<=0 ) {
            if( CH==1 ) {
                house[CH] = house[i];
                i++;
            } else {
                CH--;
            }
        }
        CH++;
        house[CH] = house[i];
    }

    house.resize(CH+1);
}

void kingdom::destroy (point missile) {

    if (is_destroyed)
        return;

    if (is_inside(missile))
        is_destroyed = true;
}

// 計算被摧毀的王國面積 (如果沒被摧毀就回傳0)
double kingdom::area() {

    if(!is_destroyed)
        return 0;

    double area = 0;
    for(int i=1; i<house.size(); ++i)
        area += (house[i-1].x * house[i].y) - (house[i].x * house[i-1].y);
    return area/2;
}

// 判斷飛彈落點是否擊中王國
bool kingdom::is_inside (point missile) {

    for (int i=1;i<house.size(); ++i) {
        if ( cross(house[i-1], house[i], missile) < 0)
            return false;
    }
    return true;
}

kingdom build_kindom ( int house_count ) {

    kingdom k;

    int x,y;
    for (int i=0; i<house_count; ++i) {
        cin >> x >> y;
        k.house.push_back(point{x,y});
    }

    return k;
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("109.in","r",stdin);
#endif

    // 1. build the world
    vector<kingdom> king;

    int house_count;
    while (cin>>house_count) {

        if (house_count==-1)
            break;

        king.push_back(build_kindom( house_count ));
    }

    // 2. do convex hull
    for (int i=0; i<king.size(); ++i)
        king[i].convex_hull();

    // 3. missiles attack!
    double total_area = 0;

    point missile;
    while ( cin>>missile.x>>missile.y ) {
        for (int i=0; i<king.size(); ++i)
            king[i].destroy( missile );
    }

    // 4. calculate the sum of destroyed area
    for(int i=0;i<king.size();++i)
        total_area +=  king[i].area();

    printf("%.2lf\n", total_area );

    return 0;
}
