#include <stdio.h>

int main()
{
  double b[20][20][20], t;
  int p[20][20][20], r[20], i, j, k, n, s;

  while (scanf("%d", &n) > 0) {
    for (i = 0; i < n; ++i)
      for (j = 0; j < n; ++j) {
        if (i ^ j)
          scanf("%lf", b[i][j]);
        else
          b[i][i][0] = 1;
        for (k = 1; k < n; ++k)
          b[i][j][k] = 0;
        p[i][j][0] = i;
      }
    for (s = 1; s < n; ++s)
      for (k = 0; k < n; ++k)
        for (i = 0; i < n; ++i)
          for (j = 0; j < n; ++j)
            if (b[i][j][s] < (t = b[i][k][s - 1] * b[k][j][0]))
              b[i][j][s] = t, p[i][j][s] = k;
    for (s = 1; s < n; ++s)
      for (i = 0; i < n; ++i)
        if (b[i][i][s] > 1.01)
          goto p;
    goto n;
  p:
    r[s] = p[i][i][s];
    for (j = s; j--;)
      r[j] = p[i][r[j + 1]][j];
    for (j = 0; j <= s; ++j)
      printf("%d ", r[j] + 1);
    printf("%d\n", i + 1);
    continue;
  n:
    puts("no arbitrage sequence exists");
    continue;
  }

  return 0;
}
