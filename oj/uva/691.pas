Const
    InFile     = 'p691.in';
    OutFile    = 'p691.out';
    LimitSave  = 500;
    minimum1   = 0.5e-4;
    minimum2   = 1e-10;
    minimum3   : extended = 1;

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    TLine      = record
                     A , B , C               : extended;
                 end;
    Tdata      = array[1..LimitSave] of Tpoint;
    Tregion    = object
                     tot                     : longint;
                     data                    : Tdata;
                     function area : extended;
                     function rnd_getpoint : Tpoint;
                     procedure incise(L : TLine; signal : longint);
                     function inregion(p : Tpoint) : boolean;
                 end;

Var
    points     : Tdata;
    region     : Tregion;
    pA , pB , pC ,
    answer     : Tpoint;
    N          : longint;

function init : boolean;
var
    i          : longint;
begin
    if eof then begin init := false; exit; end;
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               if N < 80 then minimum3 := 10;
               for i := 1 to N do with points[i] do read(x , y);
           end;
end;

function zero1(num : extended) : boolean;
begin
    zero1 := abs(num) <= minimum1;
end;

function zero2(num : extended) : boolean;
begin
    zero2 := abs(num) <= minimum2;
end;

function zero3(num : extended) : boolean;
begin
    zero3 := abs(num) <= minimum3;
end;

function tri_area(p1 , p2 , p3 : Tpoint) : extended;
begin
    tri_area := abs((p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x)) / 2;
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function Tregion.area : extended;
var
    i          : longint;
    res        : extended;
begin
    res := 0;
    for i := 2 to tot - 1 do
      res := res + tri_area(data[1] , data[i] , data[i + 1]);
    area := res;
end;

function Tregion.rnd_getpoint : Tpoint;
var
    p1 , p2 , p3 ,
    p , v1 , v2: Tpoint;
    tot_area ,
    pro1 , pro2: extended;
    i          : longint;
begin
    pro1 := random; tot_area := area;
    if tot = 2 then begin p1 := data[1]; p2 := data[2]; p3 := data[3]; end;
    for i := 2 to tot - 1 do
      begin
          pro1 := pro1 - tri_area(data[1] , data[i] , data[i + 1]) / tot_area;
          if pro1 <= minimum1 then
            begin
                p1 := data[1]; p2 := data[i]; p3 := data[i + 1];
                break;
            end;
      end;
    pro1 := random; pro2 := random;
    if pro1 + pro2 > 1 then pro2 := 1 - pro2;
    v1.x := p2.x - p1.x; v1.y := p2.y - p1.y;
    v2.x := p3.x - p1.x; v2.y := p3.y - p1.y;
    p.x := v1.x * pro1 + v2.x * pro2 + p1.x;
    p.y := v1.y * pro1 + v2.y * pro2 + p1.y;
    rnd_getpoint := p;
end;

function calc(L : TLine; p : Tpoint) : extended;
begin
    calc := L.A * p.x + L.B * p.y + L.C;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p1.x * p2.y - p2.x * p1.y;
end;

function Get_Crossing_Sub(L1 , L2 : TLine; var crossp : Tpoint) : boolean;
var
    tmp        : extended;
begin
    tmp := L1.A * L2.B - L2.A * L1.B;
    Get_Crossing_Sub := false;
    if zero2(tmp) then exit;
    crossp.x :=  (L2.C * L1.B - L1.C * L2.B) / tmp;
    crossp.y := -(L2.C * L1.A - L1.C * L2.A) / tmp;
    Get_Crossing_Sub := true;
end;

function Get_Crossing(L1 : TLine; p1 , p2 : Tpoint; var crossp : Tpoint) : boolean;
var
    tmp        : extended;
    L2         : TLine;
begin
    Get_Line(p1 , p2 , L2);
    Get_Crossing := false;
    if not Get_Crossing_Sub(L1 , L2 , crossp) then exit;
    Get_Crossing := zero1(dist(p1 , crossp) + dist(crossp , p2) - dist(p1 , p2));
end;

function samepoint(p1 , p2 : Tpoint) : boolean;
begin
    samepoint := zero2(p1.x - p2.x) and zero2(p1.y - p2.y);
end;

procedure Tregion.incise(L : TLine; signal : longint);
var
    tmpdata    : Tdata;
    p          : Tpoint;
    tmptot , i : longint;
begin
    tmptot := 0;
    for i := 1 to tot do
      begin
          if calc(L , data[i]) * signal >= -minimum1 then
            begin inc(tmptot); tmpdata[tmptot] := data[i]; end;
          if Get_Crossing(L , data[i] , data[i mod tot + 1] , p) then
            if calc(L , p) * signal >= -minimum1 then
              begin inc(tmptot); tmpdata[tmptot] := p; end;
      end;
    tot := 1; data[1] := tmpdata[1];
    for i := 2 to tmptot do
      if not samepoint(tmpdata[i] , data[tot]) then
        begin inc(tot); data[tot] := tmpdata[i]; end;
    while (tot > 1) and samepoint(data[tot] , data[1]) do dec(tot);
end;

function pointonline(p : Tpoint) : boolean;
var
    L          : TLine;
    i          : longint;
begin
    pointonline := true;
    Get_Line(p , pA , L);
    for i := 1 to N do if zero1(calc(L , points[i])) then exit;
    Get_Line(p , pB , L);
    for i := 1 to N do if zero1(calc(L , points[i])) then exit;
    Get_Line(p , pC , L);
    for i := 1 to N do if zero1(calc(L , points[i])) then exit;
    pointonline := false;
end;

function in_triangle(p , p1 , p2 , p3 : Tpoint) : boolean;
begin
    in_triangle := zero1(tri_area(p1 , p2 , p3) - tri_area(p , p1 , p2)
                      - tri_area(p , p2 , p3) - tri_area(p , p3 , p1));
end;

function in_triangle3(p , p1 , p2 , p3 : Tpoint) : boolean;
begin
    in_triangle3 := zero3(tri_area(p1 , p2 , p3) - tri_area(p , p1 , p2)
                      - tri_area(p , p2 , p3) - tri_area(p , p3 , p1));
end;

procedure count(p : Tpoint; var AB , BC , CA : longint; canbreak : boolean);
var
    i          : longint;
begin
    AB := 0; BC := 0; CA := 0;
    for i := N downto 1 do
      begin
          if canbreak then
            begin
                if abs(AB - BC) > i then break;
                if abs(AB - CA) > i then break;
                if abs(CA - BC) > i then break;
            end;
          if in_triangle(points[i] , p , pA , pB) then inc(AB);
          if in_triangle(points[i] , p , pB , pC) then inc(BC);
          if in_triangle(points[i] , p , pC , pA) then inc(CA);
      end;
end;

function Tregion.inregion(p : Tpoint) : boolean;
var
    i          : longint;
begin
    inregion := true;
    if tot = 2
      then inregion := zero3(dist(data[1] , data[2]) - dist(p , data[1]) - dist(p , data[2]))
      else begin
               for i := 2 to tot - 1 do
                 if in_triangle3(p , data[1] , data[i] , data[i + 1]) then
                   exit;
               inregion := false;
           end;
end;

function process(p1 , p2 , q1 , q2 : Tpoint) : boolean;
var
    L1 , L2    : TLine;
    p          : Tpoint;
    AB , BC , CA
               : longint;
begin
    Get_Line(p1 , q1 , L1); Get_Line(p2 , q2 , L2);
    process := false;
    if not Get_Crossing_Sub(L1 , L2 , p) then exit;
    if not region.inregion(p) then exit;
    count(p , AB , BC , CA , true);
    if (AB = BC) and (BC = CA) then
      begin { writeln('found!');} answer := p; process := true; end;
end;

procedure work;
var
    p , p1 , p2: Tpoint;
    L          : TLine;
    found      : boolean;
    AB , BC , CA ,
    notfound ,
    excur1 , excur2 ,
    i , j      : longint;
begin
    fillchar(region , sizeof(region) , 0);
    pA.x := 0; pA.y := 0; pB.x := 1000; pB.y := 0; pC.x := 0; pC.y := 1000;
    region.tot := 3;
    region.data[1] := pA; region.data[2] := pB; region.data[3] := pC;
    notfound := 0;
    while (notfound < 10) and (region.area > minimum3) do
      begin
          repeat
            p := region.rnd_getpoint;
          until not pointonline(p);
          found := false;
          Count(p , AB , BC , CA , false);
          if (AB = BC) and (BC = CA) then begin {writeln('found!'); } answer := p; exit; end;
          if (BC > AB) and (BC > CA) then
            begin
                found := true;
                if CA >= AB then
                  begin
                      Get_Line(p , pB , L);
                      if calc(L , pC) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
                if CA <= AB then
                  begin
                      Get_Line(p , pC , L);
                      if calc(L , pB) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
            end;
          if (AB > BC) and (AB > CA) then
            begin
                found := true;
                if BC >= CA then
                  begin
                      Get_Line(p , pA , L);
                      if calc(L , pB) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
                if BC <= CA then
                  begin
                      Get_Line(p , pB , L);
                      if calc(L , pA) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
            end;
          if (CA > AB) and (CA > BC) then
            begin
                found := true;
                if AB >= BC then
                  begin
                      Get_Line(p , pC , L);
                      if calc(L , pA) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
                if AB <= BC then
                  begin
                      Get_Line(p , pA , L);
                      if calc(L , pC) > 0 then region.incise(L , 1) else region.incise(L , -1);
                  end;
            end;
          if found then notfound := 0 else inc(notfound);
      end;

    for i := 1 to N do
      for excur1 := -1 to 1 do
        for j := 1 to N do
          for excur2 := -1 to 1 do
            begin
                p1.x := points[i].x + excur1 * 0.01; p1.y := points[i].y;
                p2.x := points[j].x + excur2 * 0.01; p2.y := points[j].y;
                if process(p1 , p2 , pA , pB) then exit;
                if process(p1 , p2 , pA , pC) then exit;
                if process(p1 , p2 , pB , pC) then exit;
            end;
end;

procedure out;
begin
    writeln(answer.x : 0 : 7 , ' ' , answer.y : 0 : 7);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
