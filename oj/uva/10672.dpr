{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10672.in';
    OutFile    = 'p10672.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    father ,
    index ,
    data ,
    children ,
    count      : Tdata;
    tot , answer , 
    N , root   : longint;

function init : boolean;
var
    v , q , i  : longint;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(father , sizeof(father) , 0);
               fillchar(index , sizeof(index) , 0);
               fillchar(count , sizeof(count) , 0);
               fillchar(data , sizeof(data) , 0);
               tot := 0;
               for i := 1 to N do
                 begin
                     read(v);
                     read(count[v]);
                     index[v] := tot + 1;
                     read(q); children[v] := q;
                     while q > 0 do
                       begin
                           inc(tot);
                           read(data[tot]);
                           father[data[tot]] := v;
                           dec(q);
                       end;
                     
                 end;
               root := 1;
               while father[root] <> 0 do inc(root);
           end;
end;

function dfs(root : longint) : longint;
var
    i , tmp ,  
    negative ,
    positive   : longint;
begin
    negative := 0; positive := 0;
    for i := 0 to children[root] - 1 do
      begin
          tmp := dfs(data[i + index[root]]);
          if tmp < 0
            then dec(negative , tmp)
            else inc(positive , tmp);
      end;
    inc(answer , positive + negative);
    dfs := positive - negative + count[root] - 1; 
end;

procedure work;
begin
    answer := 0;
    dfs(root);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
