#include <algorithm>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

bool ncmp(const string &x, const string &y)
{
  string u = x + y, v = y + x;
  return u < v || (u == v && x < y);
}

int main(void)
{
  int i, n;
  string v[50];

  while (scanf("%d", &n) == 1 && n) {
    for (i = n; i--; )
      cin >> v[i];
    sort(v, v + n, ncmp);
    for (i = n; i--; )
      cout << v[i];
    cout << endl;
  }

  return 0;
}
