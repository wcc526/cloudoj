Const
    InFile     = 'p10497.in';
    Limit      = 800;
    LimitLen   = 3000;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure times(num : longint);
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    data       : Tdata;
    N          : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0); len := 1;
end;

procedure lint.times(num : longint);
var
    tmp ,
    i , jw     : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    dec(i); len := i;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;


procedure pre_process;
var
    i          : longint;
begin
    data[0].init; data[0].data[1] := 1;
    data[1].init; data[2].init; data[2].data[1] := 1;
    for i := 3 to Limit do
      begin
          data[i].add(data[i - 2] , data[i - 1]);
          data[i].times(i - 1);
      end;
end;

Begin
    pre_process;
    readln(N);
    while N <> -1 do
      begin
          data[N].print;
          writeln;
          readln(N);
      end;
End.