{$I-}
Const
    InFile     = 'p10397.in';
    Limit      = 750;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tmap       = array[1..Limit , 1..Limit] of extended;
    Tbest      = array[1..Limit] of extended;
    Tvisited   = array[1..Limit] of boolean;

Var
    map        : Tmap;
    data       : Tdata;
    best       : Tbest;
    visited    : Tvisited;
    answer     : extended;
    N          : longint;

procedure init;
var
    i , j , M ,
    p1 , p2    : longint;
begin
    read(N);
    for i := 1 to N do
      read(data[i].x , data[i].y);
    read(M);
    for i := 1 to N do
      for j := 1 to N do
        map[i , j] := sqrt(sqr(data[i].x - data[j].x) + sqr(data[i].y - data[j].y));
    for i := 1 to M do
      begin
          read(p1 , p2);
          map[p1 , p2] := 0;
          map[p2 , p1] := 0;
      end;
    readln;
end;

procedure work;
var
    j , min ,
    i          : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    answer := 0;
    visited[1] := true;
    for i := 1 to N do best[i] := map[1 , i];
    for i := 2 to N do
      begin
          min := 0;
          for j := 2 to N do
            if not visited[j] then
              if (min = 0) or (best[min] > best[j]) then
                min := j;

          visited[min] := true;
          answer := answer + best[min];

          for j := 2 to N do
            if not visited[j] then
              if map[min , j] < best[j] then
                best[j] := map[min , j];
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            if N = 0 then break;
            work;
            writeln(answer : 0 : 2);
        end;
//    Close(INPUT);
End.