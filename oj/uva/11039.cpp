#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;
int A[500000+10];
bool cmp(int a,int b)
{
	return abs(a)<abs(b);
}
int main()
{
	int T;
	//freopen("in.txt","r",stdin);
	scanf("%d",&T);
	while(T--)
	{
		int n;
		scanf("%d",&n);
		int i,j,k;
		for(i=0;i<n;++i)
			scanf("%d",&A[i]);
		sort(A,A+n,cmp);
		int ans=0;
		int flag=-1;
		int last=0;
		for(i=0;i<n;++i)
		{
			if(flag==-1)
			{
				last=A[i];
				++ans;
				if(A[i]>0) flag=0;
				else flag=1;
			}
			else if(flag==0&&A[i]<0)
			{
				last=A[i];
				flag=1-flag;
				++ans;
			}
			else if(flag==1&&A[i]>0)
			{
				++ans;
				last=A[i];
				flag=1-flag;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
