Const
    InFile     = 'p10470.in';
    OutFile    = 'p10470.out';
    Limit      = 15;

Type
    Tpower     = array[0..Limit] of comp;

Var
    power      : Tpower;
    tN ,
    N , tmp    : comp;
    Cases , i , j ,
    B , L      : longint;
    find       : boolean;

Begin
    Cases := 0;
    while not eof do
      begin
          inc(Cases);
          writeln('CASE# ' , cases , ':');
          readln(B , L , N);
          power[0] := 1;
          for i := 1 to Limit do power[i] := power[i - 1] * B;

          find := false; tN := N;
          for i := 1 to Limit do
            if (N - (power[i] - 1) / (B - 1) * L >= 0) and (N - (power[i] - 1) / (B - 1) * L <= power[i] - 1) then
              begin
                  N := N - (power[i] - 1) / (B - 1) * L;
                  find := true;
                  for j := i - 1 downto 0 do
                    begin
                        tmp := int(N / power[j]) + L;
                        if tmp >= 0 then write('+');
                        write(tmp : 0 : 0 , '*' , B , '^' , j);
                        N := N - (tmp - L) * power[j];
                    end;
                  writeln(' = ' , tN : 0 : 0);
                  break;
              end;
          if not find then
            writeln('NOT REPRESENTABLE');
      end;
End.
