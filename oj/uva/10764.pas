Var
    N , bound  : extended;
    B , A , step
               : longint;

procedure print_char(p : extended);
begin
    if p < 0 then write('''');
    write(abs(p) : 0 : 0);
end;

procedure dfs_print(N , bound : extended; step : longint);
var
    p          : extended;
    i          : longint;
begin
    if step = 1
      then print_char(N)
      else begin
               p := (bound - int(bound / B)) / A;
               for i := -A to A do
                if abs(N - p * i) <= int(bound / B) then
                  begin
                      print_char(i);
                      dfs_print(N - p * i , int(bound / B) , step - 1);
                      exit;
                  end;
           end;
end;

Begin
    readln(N , B , A);
    while N + B + A <> 0 do
      begin
          bound := A; step := 1;
          while abs(N) > bound do
            begin bound := bound * B + A; inc(step); end;
          dfs_print(N , bound , step);
          writeln;
          readln(N , B , A);
      end;
End.
