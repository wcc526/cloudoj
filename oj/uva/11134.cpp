#include<cstdio>
#include<cstring>
const int N=5555,M=3333333;
int vis[N],mat[N];
int ev[M],nxt[M],head[N],e;
void init()
{
    memset(head,-1,sizeof(head));
    e=0;
}
void add(int u,int v)
{
    ev[e]=v,nxt[e]=head[u];head[u]=e++;
}
bool dfs(int u)
{
    vis[u]=1;
    for(int i=head[u];~i;i=nxt[i])
    {
        int v=ev[i];
        if(vis[mat[v]]) continue;
        if(!mat[v]||dfs(mat[v]))
        {
            mat[v]=u;
            return 1;
        }
    }
    return 0;
}
int hg(int n)
{
    int ans=0;
    memset(mat,0,sizeof(mat));
    for(int i=1;i<=n;i++)
    {
        memset(vis,0,sizeof(vis));
        if(dfs(i)) ans++;
    }
    return ans;
}
int xl[N],xr[N],yl[N],yr[N];
int x[N],y[N];
int main()
{
    int n;
    while(scanf("%d",&n),n)
    {
        for(int i=1;i<=n;i++) scanf("%d%d%d%d",&xl[i],&yl[i],&xr[i],&yr[i]);
        int fg=1;

        init();
        for(int i=1;i<=n;i++) for(int j=xl[i];j<=xr[i];j++) add(i,j);
        if(hg(n)!=n) fg=0;
        for(int i=1;i<=n;i++) x[mat[i]]=i;

        init();
        for(int i=1;i<=n;i++) for(int j=yl[i];j<=yr[i];j++) add(i,j);
        if(hg(n)!=n) fg=0;
        for(int i=1;i<=n;i++) y[mat[i]]=i;

        if(fg) for(int i=1;i<=n;i++) printf("%d %d\n",x[i],y[i]);
        else puts("IMPOSSIBLE");
    }
    return 0;
}
