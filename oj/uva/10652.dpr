{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10652.in';
    OutFile    = 'p10652.out';
    Limit      = 3000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tpoly      = array[1..Limit] of Tpoint;

Var
    poly ,
    convex     : Tpoly;
    N , T , M  : longint;
    area1 ,
    area2      : extended;

procedure init;
var
    i          : longint;
    x , y ,
    x1 , y1 ,
    x2 , y2 , 
    w , h ,
    alpha      : extended;
begin
    fillchar(poly , sizeof(poly) , 0);
    fillchar(convex , sizeof(convex) , 0);
    dec(T); read(M);
    area1 := 0; area2 := 0;
    for i := 1 to M do
      begin
          read(x , y , w , h , alpha);
          alpha := alpha / 180 * pi; 
          area1 := area1 + w * h;
          x1 := sin(alpha) * h / 2; y1 := cos(alpha) * h / 2;
          x2 := cos(alpha) * w / 2; y2 := -sin(alpha) * w / 2;
          poly[i * 4 - 3].x := x + x1 + x2; poly[i * 4 - 3].y := y + y1 + y2;
          poly[i * 4 - 2].x := x + x1 - x2; poly[i * 4 - 2].y := y + y1 - y2;
          poly[i * 4 - 1].x := x - x1 + x2; poly[i * 4 - 1].y := y - y1 + y2;
          poly[i * 4 - 0].x := x - x1 - x2; poly[i * 4 - 0].y := y - y1 - y2;
      end;
    N := M * 4;
end;

function zero(num : extended) : boolean;
begin
    zero := abs(num) <= minimum;
end;

procedure find_min;
var
    i , min    : longint;
    tmp        : Tpoint;
begin
    min := 1;
    for i := 2 to N do
      if zero(poly[i].y - poly[min].y) and (poly[i].x < poly[min].x) or (poly[i].y < poly[min].y) then
        min := i;
    tmp := poly[1]; poly[1] := poly[min]; poly[min] := tmp;
end;

function anticlockwise(p1 , p2 , p3 : Tpoint) : boolean;   // check : p1p2 --anticlockwise--> p1p3
var
    tmp        : extended;
begin
    p2.x := p2.x - p1.x; p2.y := p2.y - p1.y;
    p3.x := p3.x - p1.x; p3.y := p3.y - p1.y;
    tmp := p2.x * p3.y - p2.y * p3.x;
    anticlockwise := tmp >= 0;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := poly[tmp]; poly[tmp] := poly[start];
    while start < stop do
      begin
          while (start < stop) and anticlockwise(poly[1] , key , poly[stop]) do dec(stop);
          poly[start] := poly[stop];
          if start < stop then inc(start);
          while (start < stop) and anticlockwise(poly[1] , poly[start] , key) do inc(start);
          poly[stop] := poly[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    poly[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i          : longint;
    x1 , y1 ,
    x2 , y2    : extended;
begin
    find_min;
    qk_sort(2 , N);
    convex[1] := poly[1]; convex[2] := poly[2]; M := 2;
    for i := 3 to N do
      begin
          while (M > 1) and not anticlockwise(convex[M - 1] , convex[M] , poly[i]) do dec(M);
          inc(M);
          convex[M] := poly[i];
      end;
    area2 := 0;
    for i := 3 to M do
      begin
          x1 := convex[i - 1].x - convex[1].x; y1 := convex[i - 1].y - convex[1].y;
          x2 := convex[i].x - convex[1].x; y2 := convex[i].y - convex[1].y;
          area2 := area2 + (x1 * y2 - x2 * y1);
      end;
    area2 := abs(area2 / 2);
end;

procedure out;
begin
    writeln(area1 / area2 * 100 : 0 : 1 , ' %');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
