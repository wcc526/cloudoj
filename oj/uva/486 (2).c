#include <stdio.h>

int main()
{
  char c, s[10];
  int m, n, r, t;

  while (scanf("%s", s) == 1) {
    m = n = r = t = 0;
    do {
      if (*s < 'o') {
        if (*s < 'h') {
          if (*s == 'e') {
            if (s[5] < 'n') {
              if (s[5] == 0) /* eight */
                r += 8;
              else /* eighteen */
                r += 18;
            } else {
              if (s[5] == 'n') /* eleven */
                r += 11;
              else /* eighty */
                r += 80;
            }
          } else {
            if (s[1] == 'i') {
              if (s[4] == 0) /* five */
                r += 5;
              else if (s[4] == 'e') /* fifteen */
                r += 15;
              else /* fifty */
                r += 50;
            } else {
              if (s[4] == 0) /* four */
                r += 4;
              else if (s[4] == 't') /* fourteen */
                r += 14;
              else /* forty */
                r += 40;
            }
          }
        } else {
          if (*s < 'n') {
            if (*s == 'h') /* hundred */
              r *= 100;
            else /* million */
              m = r, r = 0;
          } else {
            if (s[1] == 'i') {
              if (s[4] == 0) /* nine */
                r += 9;
              else if (s[5] == 'e') /* nineteen */
                r += 19;
              else /* ninety */
                r += 90;
            } else { /* negative */
              n = 1;
            }
          }
        }
      } else {
        if (*s < 't') {
          if (*s == 's') {
            if (s[1] == 'e') {
              if (s[5] == 0) /* seven */
                r += 7;
              else if (s[6] == 'e') /* seventeen */
                r += 17;
              else /* seventy */
                r += 70;
            } else {
              if (s[3] == 0) /* six */
                r += 6;
              else if (s[4] == 'e') /* sixteen */
                r += 16;
              else /* sixty */
                r += 60;
            }
          } else { /* one */
            r += 1;
          }
        } else {
          if (*s == 't') {
            if (s[1] ^ 'h') {
              if (s[2] == 'e') {
                if (s[3] == 'l') /* twelve */
                  r += 12;
                else /* twenty */
                  r += 20;
              } else {
                if (s[2] == 'o') /* two */
                  r += 2;
                else /* ten */
                  r += 10;
              }
            } else {
              if (s[2] < 'o') {
                if (s[5] == 'e') /* thirteen */
                  r += 13;
                else /* thirty */
                  r += 30;
              } else {
                if (s[2] == 'o') /* thousand */
                  t = r, r = 0;
                else /* three */
                  r += 3;
              }
            }
          } /* zero */
        }
      }
      if (scanf("%c", &c) < 1 || c == '\n')
        break;
    } while (scanf("%s", s) == 1);
    r += m * 1000000 + t * 1000;
    printf("%d\n", n ? -r : r);
  }

  return 0;
}
