{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10549.in';
    OutFile    = 'p10549.out';
    Limit      = 200;

Type
    Tkey       = record
                     x , y , w , sum         : longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;
    Topt       = array[1..Limit , 0..Limit , -Limit..Limit] of
                   record
                       ok                    : boolean;
                       strategy              : byte;
                   end;
    Tsort      = array[1..Limit] of longint;

Var
    data       : Tdata;
    opt        : Topt;
    sort       : Tsort;
    first      : boolean;
    N          : longint;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               N := N * 2;
               for i := 1 to N do
                 begin
                     read(data[i].x , data[i].y , data[i].w);
                     data[i].w := data[i].w * 2;
                     data[i].sum := data[i].x + data[i].y;
                 end;
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].sum > key.sum) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].sum < key.sum) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    qk_sort(1 , N);
    fillchar(opt , sizeof(opt) , 0);
    opt[1 , 0 , 1].ok := true;
    for i := 2 to N do
      for k := -i + 1 to i do
        begin
            opt[i , i - 1 , k].ok := false;
            for j := i - 2 downto 0 do
              if opt[i - 1 , j , 1 - k].ok then
                if (j = 0) or (data[j].x <= data[i].x - data[i].w) and (data[j].y <= data[i].y - data[i].w) then
                  begin
                      opt[i , i - 1 , k].strategy := j;
                      opt[i , i - 1 , k].ok := true;
                      break;
                  end;
            for j := i - 2 downto 0 do
              if (data[j + 1].x <= data[j + 2].x - data[j + 2].w) and (data[j + 1].y <= data[j + 2].y - data[j + 2].w)
                then if j = 0
                       then opt[i , j , k].ok := (k = i)
                       else opt[i , j , k].ok := opt[j + 1 , j , k - (i - j - 1)].ok
                else break;
        end;
end;

procedure out;
var
    p1 , p2 , k ,
    i , tmp ,
    sign       : longint;
begin
    p1 := N; p2 := N - 1;
    while not opt[p1 , p2 , 0].ok do dec(p2);
    k := 0;
    sign := 1;
    while true do
      begin
          for i := p2 + 1 to p1 do
            begin
                sort[i] := sign;
                dec(k);
            end;
          if p2 = 0 then break;
          p1 := opt[p2 + 1 , p2 , k + 1].strategy;
          tmp := p1; p1 := p2; p2 := tmp;
          k := -k; sign := 1 - sign;
      end;
    if not first then writeln;
    for i := N downto 1 do
      if sort[i] = 0 then
        writeln(data[i].x , ' ' , data[i].y , ' ' , data[i].w div 2);
    writeln('-');
    for i := N downto 1 do
      if sort[i] = 1 then
        writeln(data[i].x , ' ' , data[i].y , ' ' , data[i].w div 2);
    first := false;
end;

Begin
    first := true;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
