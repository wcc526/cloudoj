Var
    i , j , tmp ,
    H , N , M  : longint;
    A , B      : array[1..100] of longint;

function binary_search(start , stop , key : longint) : boolean;
var
    mid        : longint;
begin
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if key = B[mid]
            then exit(true)
            else if key < B[mid]
                   then start := mid + 1
                   else stop := mid - 1;
      end;
    exit(false);
end;

procedure process;
var
    p1 , p2 , p3
               : longint;
begin
    for p1 := 1 to N do
      for p2 := 1 to M do
        for p3 := p1 + 1 to N do
          if binary_search(p2 + 1 , M , H - A[p1] - B[p2] - A[p3]) then
            begin
                writeln(A[p1] , ' ' , B[p2] , ' ' , A[p3] , ' ' , H - A[p1] - B[p2] - A[p3]);
                exit;
            end;
    writeln('no solution');
end;

Begin
    while not eof do
      begin
          readln(H);
          N := 0; M := 0;
          while not eoln do begin inc(N); read(A[N]); end; readln;
          while not eoln do begin inc(M); read(B[M]); end; readln;
          for i := 1 to N do
            for j := i + 1 to N do
              if A[i] < A[j] then
                begin
                    tmp := A[i]; A[i] := A[j]; A[j] := tmp;
                end;
          for i := 1 to M do
            for j := i + 1 to M do
              if B[i] < B[j] then
                begin
                    tmp := B[i]; B[i] := B[j]; B[j] := tmp;
                end;
          process;
      end;
End.