Var
    N          : double;

procedure print;
var
    A , B , C  : double;
begin
    A := sqr(2 * N * sin(pi / 12)) + (pi * N * N / 3 - N * N);
    B := (pi * N * N / 4 - N * N / 2) * 4 - A * 2;
    C := N * N - A - B;
    writeln(A : 0 : 3 , ' ' , B : 0 : 3 , ' ' , C : 0 : 3);
end;

Begin
    while not eof do
      begin
          readln(N);
          print;
      end;
End.
