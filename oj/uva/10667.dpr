{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10667.in';
    OutFile    = 'p10667.out';
    Limit      = 110;

Type
    Tdata      = array[0..Limit , 0..Limit] of longint;

Var
    data       : Tdata;
    answer , 
    S , P      : longint;

procedure init;
var
    i , N ,
    x1 , y1 ,
    x2 , y2 ,
    x , y      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    dec(P);
    read(S , N);
    for i := 1 to N do
      begin
          read(x1 , y1 , x2 , y2);
          for x := x1 to x2 do
            for y := y1 to y2 do
              data[x , y] := 1;
      end;
end;

procedure work;
var
    x1 , x2 ,
    y1 , y2 , 
    i , j      : longint;
begin
    for i := 1 to S do
      for j := 1 to S do
        data[i , j] := data[i , j - 1] + data[i - 1 , j] - data[i - 1 , j - 1] + data[i , j];
    answer := 0;
    for x1 := 0 to S do
      for x2 := x1 + 1 to S do
        for y1 := 0 to S do
          for y2 := S downto y1 + 1 do
            if data[x2 , y2] + data[x1 , y1] = data[x1 , y2] + data[x2 , y1] then
              begin
                  if (x2 - x1) * (y2 - y1) > answer
                    then answer := (x2 - x1) * (y2 - y1);
                  break;
              end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(P);
      while P > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
