#include <stdio.h>

int main()
{
  int a[26], f[26], i, j, k, t;

  for (i = 26; i--;)
    f[a[i] = i] = 0;

  while ((i = getc(stdin)) > 0)
    if ((i |= 0x20) >= 'a' && i <= 'z')
      ++f[i - 'a'];

  for (i = 1; i < 26; ++i, f[++j] = k, a[j] = t)
    for (k = f[j = i], t = a[i]; j-- && f[j] < k;)
      f[j + 1] = f[j], a[j + 1] = a[j];

  for (i = 0; i < 26 && f[i]; ++i)
    printf("%c %d\n", a[i] + 'A', f[i]);

  return 0;
}
