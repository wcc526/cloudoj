#include <algorithm>
#include <cstdio>
#include <functional>

#define N 10000

using namespace std;

struct modcmp : public binary_function<int, int, bool> {
  int m;
  modcmp(int m) : m(m) {}
  bool operator()(int x, int y) {
    int mx = x % m, my = y % m;
    if (mx < my)
      return true;
    if (my < mx)
      return false;
    if (x & y & 1)
      return y < x;
    if (~(x | y) & 1)
      return x < y;
    if (x & 1)
      return true;
    else
      return false;
  }
};

int main()
{
  int i, n, m, v[N];

  while (scanf("%d %d", &n, &m) == 2 && (n || m)) {
    printf("%d %d\n", n, m);
    for (i = 0; i < n; ++i)
      scanf("%d", v + i);
    sort(v, v + n, modcmp(m));
    for (i = 0; i < n; ++i)
      printf("%d\n", v[i]);
  }
  puts("0 0");

  return 0;
}
