Var
    data       : array[1..1000] of longint;
    sum , count ,
    i , j , C ,
    N          : longint;
Begin
    readln(C);
    for i := 1 to C do
      begin
          read(N);
          for j := 1 to N do read(data[j]);
          sum := 0;
          for j := 1 to N do inc(sum, data[j]);
          count := 0;
          for j := 1 to N do
            if data[j] * N > sum then
              inc(count);
          writeln(count / N * 100 : 0 : 3 , '%');
      end;
End.