Var
    i , T ,
    N , V , Q  : longint;

Begin
    readln(T);
    for i := 1 to T do
      begin
          readln(N , V , Q);
          write('League ' , i , ': ');
          if N = Q
            then writeln(0)
            else if Q = 1
                   then writeln(V * (N - Q) + 2)
                   else writeln(V * (N - Q) + 1);
      end;
End.