{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10579.in';
    OutFile    = 'p10579.out';
    LimitSave  = 5000;
    LimitLen   = 1010;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(const num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[1..LimitSave] of lint;

Var
    data       : Tdata;
    N          : longint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 0;
end;

procedure lint.add(const num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

procedure pre_process;
var
    i          : longint;
begin
    data[1].init; data[1].len := 1; data[1].data[1] := 1;
    data[2].init; data[2].len := 1; data[2].data[1] := 1;
    i := 2;
    while data[i].len <= 1000 do
      begin
          inc(i);
          data[i].add(data[i - 1] , data[i - 2]);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      while not eof do
        begin
            readln(N);
            data[N].print;
            writeln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
