{$I-}
Const
    InFile     = 'p10459.in';
    OutFile    = 'p10459.out';
    Limit      = 11000;

Type
    Tedge      = record
                     p1 , p2 , cost          : longint;
                 end;
    Tdata      = array[1..Limit * 2] of Tedge;
    Tindex     = array[1..Limit] of longint;
    Topt       = array[1..Limit , 1..2] of
                   record
                       child , bestlen       : longint;
                   end;
    Tbestfa    = array[1..Limit] of longint;
    Tkey       = record
                     best , number           : longint;
                 end;
    Tanswer    = array[1..Limit] of Tkey;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    father ,
    index      : Tindex;
    opt        : Topt;
    costfa ,
    bestfa     : Tbestfa;
    answer     : Tanswer;
    visited    : Tvisited;
    bestans ,
    N , M      : longint;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tedge;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].p1 > key.p1) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].p1 < key.p1) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure init;
var
    p1 , p2 , cost ,
    i , j , k  : longint;
begin
    M := 0; N := 1;
    while not eoln and not eof do
      begin
          readln(p1 , p2 , cost);
          inc(N);
          inc(M); data[M].p1 := p1; data[M].p2 := p2; data[M].cost := cost;
          inc(M); data[M].p1 := p2; data[M].p2 := p1; data[M].cost := cost;
      end;
    readln;
    qk_sort(1 , M);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[data[i].p1] = 0
        then index[data[i].p1] := i;
end;

procedure dfs_opt(root : longint);
var
    i , j , tmp: longint;
begin
    visited[root] := true;
    opt[root , 1].child := 0; opt[root , 1].bestlen := 0;
    i := index[root];
    while (i <> 0) and (i <= M) and (data[i].p1 = root) do
      begin
          if not visited[data[i].p2] then
            begin
                father[data[i].p2] := root;
                costfa[data[i].p2] := data[i].cost;
                dfs_opt(data[i].p2);
                tmp := opt[data[i].p2 , 1].bestlen + data[i].cost;
                if tmp > opt[root , 1].bestlen
                  then j := 1
                  else if tmp > opt[root , 2].bestlen
                         then j := 2
                         else j := 3;
                if j <> 3 then
                  begin
                      if j = 1 then opt[root , 2] := opt[root , 1];
                      opt[root , j].bestlen := tmp; opt[root , j].child := data[i].p2;
                  end;
            end;
          inc(i);
      end;
end;

procedure dfs_father(root : longint);
var
    i , tmp    : longint;
begin
    visited[root] := true;
    if father[root] <> 0 then
      begin
          bestfa[root] := bestfa[father[root]] + costfa[root];
          if opt[father[root] , 1].child = root
            then tmp := opt[father[root] , 2].bestlen
            else tmp := opt[father[root] , 1].bestlen;
          if tmp + 1 > bestfa[root] then bestfa[root] := tmp + costfa[root];
      end;

    i := index[root];
    while (i <> 0) and (i <= M) and (data[i].p1 = root) do
      begin
          if not visited[data[i].p2] then
            dfs_father(data[i].p2);
          inc(i);
      end;
end;

procedure work;
var
    i          : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(father , sizeof(father) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    dfs_opt(1);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(bestfa , sizeof(bestfa) , 0);
    dfs_father(1);

    bestans := 0;
    for i := 1 to N do
      begin
          answer[i].number := i;
          if bestfa[i] > opt[i , 1].bestlen
            then answer[i].best := bestfa[i]
            else answer[i].best := opt[i , 1].bestlen;
          if answer[i].best > bestans then bestans := answer[i].best;
      end;
end;

procedure out;
begin
    writeln(bestans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            if N <= 0 then break;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
