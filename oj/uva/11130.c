#include <stdio.h>
#include <math.h>

int g(void)
{
  int r = 0;
  char c;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  while ((c = getc(stdin)) >= '0')
    r = r * 10 + c - '0';
  return r;
}

int main(void)
{
  int a, b, v, A, s;
  double x, y, d;
  const double PI = 3.1415926535897932384626433832795028841971693993751;

  for (;;) {
    a = g(); b = g(); v = g(); A = g(); s = g();
    if (a || b || v || A || s) {
      d = 0.5 * v * s;
      x = cos(A * PI / 180) * d / a + 0.5;
      y = sin(A * PI / 180) * d / b + 0.5;
      printf("%d %d\n", (int)x, (int)y);
    } else {
      break;
    }
  }

  return 0;
}
