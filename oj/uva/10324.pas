Const
    Limit      = 1000000;

Var
    subsum     : array[0..Limit] of longint;
    K , tot , cases ,
    i , j , tmp: longint;
    ch         : char;

Begin
    cases := 0;
    while true do
      begin
          i := 0; subsum[0] := 0;
          while not eoln do
            begin
                read(ch); inc(i);
                subsum[i] := subsum[i - 1] + ord(ch) - ord('0');
            end;
          if i = 0 then break;
          inc(Cases);
          writeln('Case ' , Cases , ':');
          readln(tot);
          for k := 1 to tot do
            begin
                readln(i , j);
                if i > j then begin tmp := i; i := j; j := tmp; end;
                inc(j);
                if (subsum[i] = subsum[j]) or (subsum[j] - subsum[i] = j - i) then
                  writeln('Yes')
                else
                  writeln('No');
            end;
      end;
End.