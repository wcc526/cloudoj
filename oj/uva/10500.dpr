{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10500.in';
    OutFile    = 'p10500.out';
    Limit      = 200;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , 1 , 0 , -1);

Type
    Tmap       = array[1..Limit , 1..Limit] of char;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;

Var
    map        : Tmap;
    visited ,
    marked     : Tvisited;
    N , M ,
    x , y ,
    tot        : longint;

procedure read_char(var c : char);
begin
    read(c);
    while not (c in ['0' , 'X']) do read(c);
end;

function init : boolean;
var
    i , j      : longint;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(marked , sizeof(marked) , 0);
    fillchar(visited , sizeof(visited) , 0);
    readln(N , M , x , y);
    if N * M = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     for j := 1 to M do
                       read_char(map[i , j]);
                     readln;
                 end;
           end;
end;

procedure mark;
var
    i , nx , ny: longint;
begin
    for i := 1 to 4 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) then
            marked[nx , ny] := true;
      end;
    marked[x , y] := true;
end;

procedure work;
var
    nx , ny , i: longint;
    changed    : boolean;
begin
    changed := true;
    tot := 0;
    while changed do
      begin
          inc(tot);
          visited[x , y] := true;
          mark;
          changed := false;
          for i := 1 to 4 do
            begin
                nx := x + dirx[i]; ny := y + diry[i];
                if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) then
                  if not visited[nx , ny] and (map[nx , ny] = '0') then
                    begin
                        x := nx; y := ny;
                        changed := true;
                        break;
                    end;
            end;
      end;
end;

procedure print_Line;
var
    j          : longint;
begin
    write('|');
    for j := 1 to M do
      write('---|');
    writeln;
end;

procedure out;
var
    i , j      : longint;
begin
    print_Line;
    for i := 1 to N do
      begin
          write('|');
          for j := 1 to M do
            begin
                write(' ');
                if marked[i , j]
                  then write(map[i , j])
                  else write('?'); 
                write(' |');
            end;
          writeln;
          print_Line;
      end;
    writeln;
    writeln('NUMBER OF MOVEMENTS: ' , tot - 1);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
