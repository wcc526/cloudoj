/**
 * UVa 105 UVa 105 The Skyline Problem
 * Author: chchwy
 * Last Modified: 2008.11.27
 * Tag: Brute Force
 */
#include<iostream>
using namespace std;

int main()
{
#ifndef ONLINE_JUDGE
    freopen("105.in","r",stdin);
#endif;

    int map[10000+1];
    int rightMax=0; // the right most position of buildings

    memset(map,0,sizeof(map));

    //read and brute force
	int left, height, right; 
    while (scanf("%d %d %d", &left, &height, &right)==3) {

        for (int i=left; i<right; ++i) {
            if (height > map[i])
                map[i] = height;
        }
        if (right>rightMax)
            rightMax = right;
    }

    //output
    int prev = 0;
    for (int i=0; i<rightMax; ++i) {
        if (prev==map[i])
            continue;
        printf("%d %d ",i,map[i]);
        prev = map[i];
    }
    printf("%d %d\n",rightMax, 0); 
    return 0;
}
