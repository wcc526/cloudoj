Var
    N , i , j  : longint;
    find       : boolean;
    x , y ,
    x1 , y1 ,
    x2 , y2    : extended;
Begin
    while not eof do
      begin
          readln(N , x1 , y1 , x2 , y2);
          find := false;
          for i := 1 to N do
            begin
                readln(x , y);
                if (sqr(x - x1) + sqr(y - y1)) * 4 <= sqr(x - x2) + sqr(y - y2) then
                  begin
                      writeln('The gopher can escape through the hole at (' , x : 0 : 3 , ',' , y : 0 : 3 , ').');
                      find := true;
                      for j := i + 1 to N do
                        readln;
                      break;
                  end;
            end;
          if not find then
            writeln('The gopher cannot escape.');
      end;
End.