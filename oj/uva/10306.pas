Const
    InFile     = 'p10306.in';
    Limit      = 40;
    LimitC     = 300;
    Maximum    = 1000000;

Type
    Topt       = array[1..Limit , 0..LimitC , 0..LimitC] of longint;
    Tdata      = array[1..Limit] of
                   record
                       x , y  : longint;
                   end;

Var
    opt        : Topt;
    data       : Tdata;
    answer , S ,
    N , Cases  : longint;

procedure init;
var
    i          : longint;
begin
    read(N , S);
    for i := 1 to N do read(data[i].x , data[i].y);
end;

function dfs(step , x , y : longint) : longint;
var
    p1 , p2    : longint;
begin
    if step > N then exit(maximum);
    if sqr(x) + sqr(y) > sqr(S) then exit(maximum);
    if sqr(x) + sqr(y) = sqr(S) then
      exit(0);
    if opt[step , x , y] <> -1 then exit(opt[step , x , y]);
    p1 := dfs(step + 1 , x , y);
    p2 := dfs(step , x + data[step].x , y + data[step].y) + 1;
    if p1 < p2
      then opt[step , x , y] := p1
      else opt[step , x , y] := p2;
    exit(opt[step , x , y]);
end;

procedure work;
begin
    fillchar(opt , sizeof(opt) , $FF);
    answer := dfs(1 , 0 , 0);
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          init;
          work;
          if answer >= maximum
            then writeln('not possible')
            else writeln(answer);
      end;
End.