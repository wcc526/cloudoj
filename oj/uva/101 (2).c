#include <stdio.h>
#include <string.h>

typedef struct _node {
  struct _node *n;
  int v;
} node;

node *b[24];
node v[24];
int t[24];

node **p(int i)
{
  node *c = b[t[i]];
  if (c->v == i)
    return &b[t[i]];
  while (c->n->v != i)
    c = c->n;
  return &v[c->v].n;
}

node *f(int v)
{
  node *c = b[t[v]];
  while (c->v != v)
    c = c->n;
  return c;
}

void r(int v)
{
  node *c, *n = f(v);
  while (n->n) {
    c = n;
    n = n->n;
    c->n = 0;
    b[n->v] = n;
    t[n->v] = n->v;
  }
}

void m(int a, int b)
{
  node *n = f(b);
  while (n->n)
    n = n->n;
  n->n = f(a);
  *p(a) = 0;
  while (n->n) {
    n = n->n;
    t[n->v] = t[b];
  }
}

void w(node *n)
{
  if (n) {
    printf(" %d", n->v);
    while (n->n) {
      n = n->n;
      printf(" %d", n->v);
    }
  }
}

int main(void)
{
  int x, y, n;
  char z[5], o[5];

  for (n = 24; n--;) {
    b[n] = v + n;
    v[n].n = 0;
    t[n] = v[n].v = n;
  }

  scanf("%d", &n);

  while (scanf("%s", o) == 1) {
    if (strcmp(o, "quit") == 0)
      break;
    scanf("%d %s %d", &x, z, &y);
    if (x == y || t[x] == t[y])
      continue;
    if (strcmp(z, "onto") == 0)
      r(y);
    if (strcmp(o, "move") == 0)
      r(x);
    m(x, y);
  }

  for (x = 0; x < n; ++x) {
    printf("%d:", x);
    w(b[x]);
    putc('\n', stdout);
  }

  return 0;
}
