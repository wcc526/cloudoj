#include <stdio.h>

int main()
{
  int a, b, i, j, n, p, s, u, v, x;

  do {
    a = b = i = j = n = p = u = v = 0;
    s = 3;
    while (scanf("%d\n", &x) == 1 && x) {
      if (p > x) {
        if (s & 1) {
          if (s & 4) u += a, a = 0, ++i;
          else a = 0;
        }
        ++b, s = 6;
      } else if (p == x) {
        if (s & 1) ++a;
        if (s & 2) ++b;
      } else if (p) {
        if (s & 2) {
          if (s & 4) v += b, b = 0, ++j;
          else b = 0;
        }
        ++a, s = 5;
      }
      p = x, ++n;
    }
    if ((s & 4) && (s & 1)) u += a, ++i;
    if ((s & 4) && (s & 2)) v += b, ++j;
    if (n) {
      if (i == 0) i = 1;
      if (j == 0) j = 1;
      printf("Nr values = %d:  %.6lf %.6lf\n", n, (double)u / i, (double)v / j);
    }
  } while (n);

  return 0;
}
