#include <math.h>
#include <stdio.h>

#define EPS 1e-8
#define C(a, b, c, d) ((b.x - a.x)*(d.y - c.y) - (d.x - c.x)*(b.y - a.y))

typedef struct {
  double x, y;
} pt;

typedef struct {
  char s;
  pt t, b, r;
} rt;

int d(pt a, pt b, pt p)
{
  double r = C(a, b, a, p);
  return r < EPS ? 1 : r > EPS ? -1 : 0;
}

int it(rt *t, pt p)
{
  int d1 = d(t->t, t->b, p);
  int d2 = d(t->b, t->r, p);
  int d3 = d(t->r, t->t, p);
  return d1 != 1 && d2 != 1 && d3 != 1;
}

int main(void)
{
  rt r[10];
  pt p;
  int c = 0, f, i, n = 0;
  char t[2];

  while (scanf("%1s", t) == 1 && *t != '*') {
    r[n].s = *t;
    scanf("%lf %lf %lf", &r[n].t.x, &r[n].t.y, &r[n].b.x);
    if (*t != 'c') {
      scanf("%lf", &r[n].b.y);
      if (*t == 't') {
        scanf("%lf %lf", &r[n].r.x, &r[n].r.y);
        if (d(r[n].t, r[n].b, r[n].r) == 1) {
          p = r[n].b;
          r[n].b = r[n].r;
          r[n].r = p;
        }
      }
    }
    ++n;
  }

  while (scanf("%lf %lf", &p.x, &p.y) == 2 &&
         !(p.x == 9999.9 && p.y == 9999.9)) {
    for (++c, f = i = 0; i < n; ++i) {
      switch (r[i].s) {
      case 'r':
        if (p.x > r[i].t.x && p.x < r[i].b.x &&
            p.y > r[i].b.y && p.y < r[i].t.y)
          printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
        break;
      case 'c':
        if (sqrt((p.x - r[i].t.x) * (p.x - r[i].t.x) +
                 (p.y - r[i].t.y) * (p.y - r[i].t.y)) < r[i].b.x)
          printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
        break;
      case 't':
        if (it(r + i, p))
          printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
        break;
      }
    }
    if (f == 0)
      printf("Point %d is not contained in any figure\n", c);
  }

  return 0;
}
