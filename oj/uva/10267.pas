Const
    InFile     = 'p10267.in';
    Limit      = 250;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tmap       = array[1..Limit , 1..Limit] of char;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;

Var
    map        : Tmap;
    visited    : Tvisited;
    name       : string;
    N , M      : longint;

procedure dfs(x , y : longint; color : char);
var
    nx , ny , i: longint;
begin
    visited[x , y] := true;
    for i := 1 to 4 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx <= N) and (ny <= M) and (nx >= 1) and (ny >= 1) then
            if not visited[nx , ny] and (map[nx , ny] = map[x , y]) then
              dfs(nx , ny , color);
      end;
    map[x , y] := color;
end;

procedure swap(var a , b : longint);
var
    tmp        : longint;
begin
    if a > b then
      begin
          tmp := a; a := b; b := tmp;
      end;
end;

procedure main;
var
    ch , color : char;
    i , j ,
    x1 , y1 ,
    x2 , y2 ,
    x , y      : longint;
begin
    N := 0; M := 0;
    while true do
      begin
          repeat read(ch); until ch <> ' ';
          if ch = 'C'
            then fillchar(map , sizeof(map) , 'O');
          if ch = 'I'
            then begin
                     read(N , M);
                     fillchar(map , sizeof(map) , 'O');
                 end;
          if ch = 'X' then break;
          if ch = 'L'
            then begin
                     read(X , Y);
                     repeat read(color); until color <> ' ';
                     map[x , y] := color;
                 end;
          if ch = 'V'
            then begin
                     read(X , Y1 , Y2);
                     swap(Y1 , Y2);
                     repeat read(color); until color <> ' ';
                     for i := Y1 to Y2 do map[X , i] := color;
                 end;
          if ch = 'H'
            then begin
                     read(X1 , X2 , Y);
                     swap(X1 , X2);
                     repeat read(color); until color <> ' ';
                     for i := X1 to X2 do map[i , Y] := color;
                 end;
          if ch = 'K'
            then begin
                     read(X1 , Y1 , X2 , Y2);
                     swap(X1 , X2); swap(Y1 , Y2);
                     repeat read(color); until color <> ' ';
                     for i := X1 to X2 do
                       for j := Y1 to Y2 do
                         map[i , j] := color;
                 end;
          if ch = 'F'
            then begin
                     read(X , Y);
                     repeat read(color); until color <> ' ';
                     fillchar(visited , sizeof(visited) , 0);
                     dfs(X , Y , Color);
                 end;
          if ch = 'S'
            then begin
                     read(name); while name[1] = ' ' do delete(name , 1 , 1);
                     writeln(name);
                     for i := 1 to M do
                       begin
                           for j := 1 to N do write(map[j , i]);
                           writeln;
                       end;
                 end;
          readln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      main;
//    Close(INPUT);
End.