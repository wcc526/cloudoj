/*
 * uva_11549.cpp
 *
 *  Created on: 2012-12-11
 *      Author: Administrator
 */
#include <set>
#include <sstream>
#include <cstdio>
#include <iostream>
using namespace std;
int buf[10];
int next(int n,int k)
{
	if(!k) return 0;
	long long k2=(long long)k*k;
	int L=0;
	while(k2>0) {buf[L++]=k2%10;k2/=10;}
	if(n>L) n=L;
	int ans=0;
	for(int i=0;i<n;++i)
		ans=ans*10+buf[--L];
	return ans;
}
#if 0
int next(int n,int k)
{
	stringstream ss;
	ss<<(long long )k*k;
	string s=ss.str();
	if(s.length()>n) s=s.substr(0,n);
	int ans;
	stringstream ss2(s);
	ss2>> ans;
	return ans;
}
#endif

int main()
{
	//freopen("in.txt","r",stdin);
	int T;
	cin >> T;
	while(T--)
	{
		int n,k;
		cin>> n >> k;
		int ans=k;
		int k1=k,k2=k;
		do{
			k1=next(n,k1);
			k2=next(n,k2);if(k2>ans) ans=k2;
			k2=next(n,k2);if(k2>ans) ans=k2;
		}while(k1!=k2);
		cout << ans << endl;
	}
	return 0;
}
#if 0
int main()
{
	int T;
	//freopen("in.txt","r",stdin);
	cin >> T;
	while(T--)
	{
		int n,k;
		cin >> n >> k;
		set<int> s;
		int ans=k;
		while(!s.count(k))
		{
			s.insert(k);
			if(k>ans) ans=k;
			k=next(n,k);
		}
		cout << ans << endl;
	}
	return 0;
}
#endif





