Var
    i , F , N  : longint;
    sum ,
    A , B , C  : extended;
Begin
    readln(N);
    while N > 0 do
      begin
          dec(N);
          readln(F);
          sum := 0;
          for i := 1 to F do
            begin
                readln(A , B , C);
                sum := sum + A * C;
            end;
          writeln(sum : 0 : 0);
      end;
End.