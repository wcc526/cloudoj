Const
    InFile     = 'p10496.in';
    OutFile    = 'p10496.out';
    Limit      = 10;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    Cases , answer ,
    R , W , N  : longint;
    startp     : Tpoint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    readln(R , W , startp.x , startp.y , N);
    for i := 1 to N do
      readln(data[i].x , data[i].y);
end;

procedure dfs(step , last : longint; lastp : Tpoint);
var
    i          : longint;
begin
    if step > N
      then begin
               inc(last , abs(lastp.x - startp.x) + abs(lastp.y - startp.y));
               if last < answer then answer := last;
               exit;
           end;
    for i := 1 to N do
      if not visited[i] then
        begin
            visited[i] := true;
            dfs(step + 1 , last + abs(data[i].x - lastp.x) + abs(data[i].y - lastp.y) , data[i]);
            visited[i] := false;
        end;
end;

procedure work;
begin
    fillchar(visited , sizeof(visited) , 0);
    answer := maxlongint;
    dfs(1 , 0 , startp);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            writeln('The shortest path has length ' , answer);
        end;
//    Close(INPUT);
End.