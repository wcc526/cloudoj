#include <ctype.h>
#include <stdio.h>

int d[26][2], e, r, c, m;

void compute()
{
  int t, lr, lc;
  while (isspace(t = getchar()));
  if (t == '(') {
    compute();
    lr = r, lc = c;
    compute();
    if (lc ^ r)
      e = 1;
    m += lr * r * c, r = lr;
    while (isspace(t = getchar()));
  } else if (isalpha(t)) {
    r = d[t - 'A'][0], c = d[t - 'A'][1];
  } else {
    m = -1;
  }
}

int main()
{
  int i, n;
  short t;

  scanf("%d", &n);
  for (i = n; i--;)
    scanf("%1s", (char *)&t), scanf("%d %d", &d[t - 'A'][0], &d[t - 'A'][1]);

  for (;;) {
    e = m = 0;
    compute();
    if (m < 0)
      break;
    if (e)
      puts("error");
    else
      printf("%d\n", m);
  }

  return 0;
}
