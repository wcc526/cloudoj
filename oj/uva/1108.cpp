#include <cstdio>
#include <cstring>

#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int Max = 200000;
const int MaxE = 1000000;

struct edge{
    edge *s;
    int to;
};

/*每条独立的边都会被作为一个连通分量*/
/*ph记录连通分量，ft记录每个点属于的连通分量数*/
edge E[MaxE],*cp,*hd[Max],*ph[Max];
int sn,cnt,pCnt,pre[Max],low[Max],sk[Max],cId[Max],ft[Max];
void addEdge(int x,int y,edge *h[]){ cp->to = y,cp->s = h[x],h[x] = cp++;  }
void addComp(int x){++pCnt;sk[sn] = -1;
    do{ --sn; if (sk[sn] != sk[sn+1]){ int y = sk[sn];
            if (cId[y] != pCnt){ addEdge(cId[y] = pCnt,y,ph); ++ft[y];  }  }
    }while (sk[sn] != x); }
void dfs(int x,int f){ bool inS = false;
    pre[x] = low[x] = cnt++; sk[sn++] = x;
    for (edge *p = hd[x];p;p = p->s){ int y = p->to;
        if (pre[y] == -1){ dfs(y,sk[sn++] = x);
            if (low[y] == pre[x]) inS = true,addComp(x);
            else if (low[y] > pre[x]) --sn;
            low[x] = min(low[x],low[y]);
        }else low[x] = min(low[x],pre[y]);  }
    if (low[x] < pre[x]) return ; if (inS) --sn; else addComp(x);  }
int main(){
    int ct = 0,M;
    while (scanf("%d",&M) != EOF && M){
        memset(hd + 1,0,sizeof(hd[0]) * (M + 1));
        cp = E;
        int N = 0;
        for (int i = 0;i < M;i++){
            int x,y;
            scanf("%d%d",&x,&y);
            addEdge(x,y,hd),addEdge(y,x,hd);
            N = max(N,max(x,y));
        }
        if (M == 1){
            printf("Case %d: %d %d\n",++ct,2,1);
            continue;
        }
        memset(pre + 1,-1,sizeof(pre[0]) * N);
        memset(cId + 1,-1,sizeof(cId[0]) * N);
        memset(ft + 1,0,sizeof(ft[0]) * N);
        memset(ph + 1,0,sizeof(ph[0]) * N);
        sn = cnt = pCnt = 0;
        dfs(1,-1);
//        for (int i = 1;i <= pCnt;i++){for (edge *p = ph[i];p;p = p->s)printf("%d ",p->to);putchar('\n');}
//        for (int i = 1;i <= N;i++)printf("%d ",ft[i]);puts("");
        if (pCnt == 1){
            printf("Case %d: %d %lld\n",++ct,2,(LL)N * (N - 1) / 2);
            continue;
        }
        LL Res = 1;
        int nd = 0;
        for (int i = 1;i <= pCnt;i++){
            int c = 0,nc = 0;
            for (edge *p = ph[i];p;p = p->s){
                if (ft[p->to] > 1)
                    ++c;
                ++nc;
            }
            if (c == 1){
                ++nd;
                Res *= nc - 1;
                //printf("%d : %d\n",i,nc);
            }
        }
        printf("Case %d: %d %lld\n",++ct,nd,Res);
    }
    return 0;
}
