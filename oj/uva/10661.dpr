{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10661.in';
    OutFile    = 'p10661.out';
    Limit      = 20;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[1..Limit] of boolean;
    Tdegree    = array[1..Limit] of longint;
    Tcolor     = array[1..Limit] of longint;

Var
    visited    : Tvisited;
    degree     : Tdegree;
    color      : Tcolor;
    map        : Tmap;
    M , cases ,
    answer     : longint;

procedure init;
var
    i , P ,
    p1 , p2    : longint;
    ch1 , ch2  : char;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(degree , sizeof(degree) , 0);
    fillchar(color , sizeof(color) , 0);
    fillchar(map , sizeof(map) , 0);
    read(M , P);
    dec(cases);
    for i := 1 to P do
      begin
          repeat read(ch1); until ch1 in ['A'..'Z'];
          repeat read(ch2); until ch2 in ['A'..'Z'];
          p1 := ord(ch1) - ord('A') + 1;
          p2 := ord(ch2) - ord('A') + 1;
          map[p1 , p2] := true; map[p2 , p1] := true;
          inc(degree[p1]); inc(degree[p2]);
      end;
end;

procedure dfs(step , tot : longint);
var
    j , 
    i , max    : longint;
    ok         : boolean;
begin
    if tot >= answer then exit;
    if step > M then begin answer := tot; exit; end;
    
    max := 0;
    for i := 1 to M do
      if not visited[i] then
        if (max = 0) or (degree[max] < degree[i]) then
          max := i;
    visited[max] := true;
    for i := 1 to M do
      if not visited[i] and map[max , i] then
        dec(degree[i]);
    for j := 1 to tot do
      begin
          ok := true;
          for i := 1 to M do
            if visited[i] and map[max , i] and (color[i] = j) then
              begin ok := false; break; end;
          if ok then
            begin
                color[max] := j;
                dfs(step + 1 , tot);
                color[max] := 0;
            end;
      end;
    color[max] := tot + 1;
    dfs(step + 1 , tot + 1);
    color[max] := 0;
    for i := 1 to M do
      if not visited[i] and map[max , i] then
        inc(degree[i]);
    visited[max] := false;
end;

procedure work;
begin
    answer := M;
    dfs(1 , 0);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
