#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(void)
{
  int a, b, c, d, e, i;
  char s[6];
  map<string, int> m;

  for (i = s[1] = a = 0; a < 26; ++a) {
    s[0] = a + 'a';
    m.insert(make_pair(string(s), ++i));
  }

  for (s[2] = a = 0; a < 26; ++a) {
    s[0] = a + 'a';
    for (b = a; ++b < 26;) {
      s[1] = b + 'a';
      m.insert(make_pair(string(s), ++i));
    }
  }

  for (s[3] = a = 0; a < 26; ++a) {
    s[0] = a + 'a';
    for (b = a; ++b < 26;) {
      s[1] = b + 'a';
      for (c = b; ++c < 26;) {
        s[2] = c + 'a';
        m.insert(make_pair(string(s), ++i));
      }
    }
  }

  for (s[4] = a = 0; a < 26; ++a) {
    s[0] = a + 'a';
    for (b = a; ++b < 26;) {
      s[1] = b + 'a';
      for (c = b; ++c < 26;) {
        s[2] = c + 'a';
        for (d = c; ++d < 26;) {
          s[3] = d + 'a';
          m.insert(make_pair(string(s), ++i));
        }
      }
    }
  }

  for (s[5] = a = 0; a < 26; ++a) {
    s[0] = a + 'a';
    for (b = a; ++b < 26;) {
      s[1] = b + 'a';
      for (c = b; ++c < 26;) {
        s[2] = c + 'a';
        for (d = c; ++d < 26;) {
          s[3] = d + 'a';
          for (e = d; ++e < 26;) {
            s[4] = e + 'a';
            m.insert(make_pair(string(s), ++i));
          }
        }
      }
    }
  }

  string v;
  while (!getline(cin, v).eof())
    cout << m[v] << endl;

  return 0;
}
