{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10568.in';
    OutFile    = 'p10568.out';
    Limit      = 30;
    LimitAns   = 30000;

Type
    TC         = array[0..Limit , 0..Limit] of extended;
    Tgroup     = array[1..Limit] of
                   record
                       top , tot             : longint;
                       data                  : array[1..Limit] of longint;
                   end;
    Tstr       = string[30];
    Tdata      = array[1..LimitAns] of Tstr;

Var
    group      : Tgroup;
    data       : Tdata;
    tot        : longint;
    C          : TC;
    N , K , G  : longint;

procedure dfs_print(p : longint);
var
    i , j      : longint;
begin
    if p > N
      then begin
               inc(tot); data[tot] := '';
               for i := 1 to G do
                 begin
                     if i <> 1 then data[tot] := data[tot] + ' ';
                     for j := 1 to group[i].tot do
                       data[tot] := data[tot] + chr(group[i].data[j] + ord('A') - 1);
                 end;
           end
      else begin
               for i := 1 to G do
                 begin
                    if group[i].tot < group[i].top then
                      begin
                          inc(group[i].tot);
                          group[i].data[group[i].tot] := p;
                          dfs_print(p + 1);
                          group[i].data[group[i].tot] := 0;
                          dec(group[i].tot);
                      end;
                    if group[i].tot = 0 then break;
                 end;
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tstr;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure print(num : extended);
begin
    if num = 0
      then exit
      else begin
               print(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure main;
var
    ch , ch2   : char;
    i , j ,  
    last       : longint;
    answer     : extended;
begin
    fillchar(C , sizeof(C) , 0);
    C[1 , 1] := 1; C[1 , 0] := 1; C[0 , 0] := 1;
    for i := 2 to Limit do
      begin
          C[i , 0] := 1;
          for j := 1 to i do C[i , j] := C[i - 1 , j - 1] + C[i - 1 , j];
      end;
    while true do
      begin
          read(ch);
          if ch = 'E' then break;
          read(ch2); while ch2 <> ' ' do read(ch2);
          readln(N , K);
          case ch of
            'C' , 'G'         : begin
                                    answer := C[N , N mod K];
                                    last := N - N mod K;
                                    for i := 1 to N div K do
                                      begin
                                          answer := answer * C[last - 1 , K - 1];
                                          dec(last , K);
                                      end;
                                    print(answer);
                                    writeln;
                                end;
            'E'               : break;
          end;
          if ch = 'G' then
            begin
                G := (N + K - 1) div K;
                fillchar(group , sizeof(group) , 0);
                for i := 1 to G do group[i].top := K;
                tot := 0;
                if N mod K = 0
                  then dfs_print(1)
                  else for i := 1 to G do
                         begin
                             group[i].top := N mod K;
                             dfs_print(1);
                             group[i].top := K;
                         end;
                qk_sort(1 , tot);
                for i := 1 to tot do
                  writeln(data[i]);
            end;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      main;
//    Close(OUTPUT);
//    Close(INPUT);
End.
