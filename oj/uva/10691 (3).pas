Const
    InFile     = 'p10691.in';
    OutFile    = 'p10691.out';
    Limit      = 200;
    minimum    = 1e-8;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tinterval  = record
                     a , b    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tintervals = array[1..Limit] of Tinterval;

Var
    data       : Tdata;
    inter      : Tintervals;
    cases ,
    answer ,
    N          : longint;
    D          : extended;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    fillchar(data , sizeof(data) , 0);
    readln(N , D);
    i := 1;
    while i <= N do
      begin
          read(data[i].x , data[i].y);
          if sqr(data[i].x) + sqr(data[i].y) <= D * D + minimum
            then dec(N)
            else inc(i);
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tinterval;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := inter[tmp]; inter[tmp] := inter[start];
    while start < stop do
      begin
          while (start < stop) and (inter[stop].a > key.a) do dec(stop);
          inter[start] := inter[stop];
          if start < stop then inc(start);
          while (start < stop) and (inter[start].a < key.a) do inc(start);
          inter[stop] := inter[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    inter[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function atg(y , x : extended) : extended;
begin
    if y < 0
      then atg := 2 * pi - atg(-y , x)
      else if abs(x) <= minimum
             then atg := pi / 2
             else if x > 0
                    then atg := arctan(y / x)
                    else atg := pi - arctan(-y / x);
end;

function cover(inter1 , inter2 : Tinterval) : boolean;
begin
    if inter2.b < inter2.a then inter2.b := inter2.b + pi * 2;
    if inter1.b < inter1.a then inter1.b := inter1.b + pi * 2;
    cover := (inter1.a <= inter2.a + minimum) and (inter1.b >= inter2.b - minimum)
         or (inter1.a <= inter2.a + pi * 2 + minimum) and (inter1.b >= inter2.b + pi * 2 - minimum)
         or (inter1.a <= inter2.a - pi * 2 + minimum) and (inter1.b >= inter2.b - pi * 2 - minimum);
end;

function cover_point(a , b , p : extended) : boolean;
begin
    if b + minimum > a
      then cover_point := (a - minimum <= p) and (p <= b + minimum)
      else cover_point := cover_point(a , 2 * pi , p) or cover_point(0 , b , p);
end;

procedure check(p : longint);
var
    sum ,
    i , last   : longint;
begin
    sum := 1;
    last := p; i := p mod N + 1;
    while i <> p do
      begin
          if not cover_point(inter[i].a , inter[i].b , inter[last].b) then
            begin
                inc(sum);
                last := i;
            end;
          i := i mod N + 1;
      end;
    if sum < answer then answer := sum;
end;

procedure work;
var
    i , j      : longint;
    tmp , alpha: extended;
begin
    for i := 1 to N do
      begin
          tmp := atg(data[i].y , data[i].x);
          alpha := arctan(D / sqrt(sqr(data[i].x) + sqr(data[i].y) - sqr(D)));
          if tmp - alpha <= -minimum
            then inter[i].a := tmp + pi * 2 - alpha
            else inter[i].a := tmp - alpha;
          if tmp + alpha >= 2 * pi - minimum
            then inter[i].b := tmp - 2 * pi + alpha
            else inter[i].b := tmp + alpha;
      end;

    i := 1;
    while i <= N do
      begin
          for j := 1 to N do
            if (j <> i) and cover(inter[i] , inter[j]) then
              begin
                  inter[i] := inter[N];
                  dec(i); dec(N);
                  break;
              end;
          inc(i);
      end;
    qk_sort(1 , N);

    answer := maxlongint;
    for i := 1 to N do
      check(i);
    if N = 0 then answer := 1;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
