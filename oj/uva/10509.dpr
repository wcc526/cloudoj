{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Var
    num , dx   : extended;
    i          : longint;

Begin
    readln(num);
    while num > 0.5 do
      begin
          i := 1;
          while i * i * i <= num + 1e-5 do
            inc(i);
          dec(i);
          dx := (num - i * i * i) / 3 / i / i;
          writeln(i + dx : 0 : 4);
          readln(num);
      end;
End.
