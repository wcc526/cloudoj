{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10436.in';
    OutFile    = 'p10436.out';
    Limit      = 20;
    LimitLen   = 100;

Type
    Tstr       = string[LimitLen];
    Tnames     = object
                     tot      : longint;
                     data     : array[1..Limit] of Tstr;
                     procedure init;
                     procedure insert(s : Tstr);
                     function find(s : Tstr) : longint;
                 end;
    Tdata      = array[1..Limit] of longint;
    Tmap       = array[1..Limit , 1..Limit] of longint;

Var
    names      : Tnames;
    map , source
               : Tmap;
    data       : Tdata;
    N , Cases ,
    totCases   : longint;

procedure Tnames.init;
begin
    tot := 0;
end;

procedure Tnames.insert(s : Tstr);
begin
    inc(tot); data[tot] := s;
end;

function Tnames.find(s : Tstr) : longint;
var
    i          : longint;
begin
    find := -1;
    for i := 1 to tot do
      if data[i] = s then
        begin find := i; break; end;
end;

procedure init;
var
    i , M , k ,
    p1 , p2    : longint;
    s , s1 , s2: Tstr;
    ch         : char;
begin
    inc(Cases);
    names.init;
    fillchar(map , sizeof(map) , $0F);
    readln(N);
    for i := 1 to N do
      begin
          s := '';
          repeat read(ch); until ch <> ' ';
          while ch <> ' ' do begin s := s + ch; read(ch); end;
          names.insert(s);
          readln(data[i]);
      end;
    readln(M);
    for i := 1 to M do
      begin
          s1 := '';
          repeat read(ch); until ch <> ' ';
          while ch <> ' ' do begin s1 := s1 + ch; read(ch); end;
          s2 := '';
          repeat read(ch); until ch <> ' ';
          while ch <> ' ' do begin s2 := s2 + ch; read(ch); end;
          p1 := names.find(s1); p2 := names.find(s2);
          readln(k);
          map[p1 , p2] := k * 2; map[p2 , p1] := k * 2;
      end;
    for i := 1 to N do
      for k := 1 to N do
        if i <> k
          then map[i , k] := map[i , k] + data[i] + data[k]
          else map[i , k] := data[i];
end;

procedure work;
var
    k , i , j  : longint;
begin
    source := map;
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          if map[i , k] + map[k , j] - data[k] < map[i , j] then
            map[i , j] := map[i , k] + map[k , j] - data[k];
end;

procedure dfs_print(p1 , p2 : longint);
var
    k          : longint;
begin
    if p1 = p2
      then writeln(names.data[p1])
      else begin
               for k := 1 to N do
                 if (k <> p1) and (map[p1 , k] + map[k , p2] - data[k] = map[p1 , p2]) and (map[p1 , k] = source[p1 , k])
                   then begin
                            write(names.data[p1] , ' ');
                            dfs_print(k , p2);
                            break;
                        end;
           end;
end;

procedure out;
var
    p1 , p2 , i ,
    Q , num    : longint;
    ch         : char;
    s1 , s2    : Tstr;
begin
    readln(Q);
    if Cases <> 1 then writeln;
    writeln('Map #' , Cases);
    for i := 1 to Q do
      begin
          writeln('Query #' , i);

          s1 := '';
          repeat read(ch); until ch <> ' ';
          while ch <> ' ' do begin s1 := s1 + ch; read(ch); end;
          s2 := '';
          repeat read(ch); until ch <> ' ';
          while ch <> ' ' do begin s2 := s2 + ch; read(ch); end;
          readln(num);
          p1 := names.find(s1); p2 := names.find(s2);

          dfs_print(p1 , p2);
          writeln('Each passenger has to pay : ' , 1.1 * map[p1 , p2] / num : 0 : 2 , ' taka');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCases); Cases := 0;
      while Cases < totCases do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
