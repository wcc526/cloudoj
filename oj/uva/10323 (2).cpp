#include <set>
#include <map>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <cctype>
#include <cstdio>
#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int factorial(int num)
{
    if(num == 0)
        return 1;
    unsigned long long prod = 1;

    int i;
    for(i = 1; i <= num; i++)
    {
        prod *= i;
    }
    return prod;

}

int main()
{

    int input;
    while(scanf("%d",&input)!=EOF)
    {
        if(input < 0)
        {
            if(input/2 * 2 == input)
            {
                cout << "Underflow!"<<endl;
            }
            else
                cout << "Overflow!" << endl;
        }

        else if(input < 8)
        {
            cout << "Underflow!"<< endl;

        }
        else if(input >13)
        {
            cout << "Overflow!" << endl;

        }

        else
            cout << factorial(input) << endl;


        }





    return 0;
}

