Const
    InFile     = 'p10454.in';
    OutFile    = 'p10454.out';
    Limit      = 130;

Type
    Tcount     = array[1..Limit , 1..Limit] of comp;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;
    Tstr       = string[Limit];

Var
    count      : Tcount;
    visited    : Tvisited;
    s          : Tstr;
    N          : longint;

procedure init;
begin
    readln(s);
    N := length(s);
    fillchar(count , sizeof(count) , 0);
    fillchar(visited , sizeof(visited) , 0);
end;

function find_next(i : longint) : longint;
var
    left       : longint;
begin
    left := 1; inc(i);
    while left <> 0 do
      begin
          if s[i] = '(' then inc(left);
          if s[i] = ')' then dec(left);
          inc(i);
      end;
    exit(i);
end;

function dfs(start , stop : longint) : comp;
var
    i          : longint;
    find       : boolean;
    min        : char;
begin
    if visited[start , stop] then exit(count[start , stop]);
    visited[start , stop] := true; count[start , stop] := 0;
    if (s[start] = '(') and (find_next(start) = stop + 1) then
      begin count[start , stop] := dfs(start + 1 , stop - 1); exit(count[start , stop]); end;

    i := start; min := '*'; find := false;
    while i <= stop do
      begin
          while s[i] = '(' do i := find_next(i);
          if i > stop then break;
          if (s[i] = '+') or (s[i] = '*') then find := true;
          if s[i] = '+' then
            if min = '*' then begin min := '+'; break; end;
          inc(i);
      end;
    if not find then begin count[start , stop] := 1; exit(1); end;
    i := start;
    while i <= stop do
      begin
          while s[i] = '(' do i := find_next(i);
          if i > stop then break;
          if s[i] = min then
            count[start , stop] := count[start , stop] + dfs(start , i - 1) * dfs(i + 1 , stop);
          inc(i);
      end;
    exit(count[start , stop]);
end;

procedure work;
begin
    dfs(1 , N);
end;

procedure print(num : comp);
begin
    if num <> 0 then
      begin
          print(int(num / 10));
          write(num - int(num / 10) * 10 : 0 : 0);
      end;
end;

procedure out;
begin
    print(count[1 , N]);
    if count[1 , N] = 0 then write(0);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while not eof do
      begin
          init;
          work;
          out;
      end;
End.