{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}

Var
    totCase ,
    nowCase ,
    P , Q ,
    X , Y      : longint;
    answer     : boolean;

procedure check(num1 , num2 : longint);
var
    t1 , t2    : extended;
begin
    if num1 < num2 then exit;
    if (num1 > 32767) or (num1 < -32768) then exit;
    if (num2 > 32767) or (num2 < -32768) then exit;
    t1 := num1; t2 := num2;
    if abs((t1 + t2) * t2 - P) > 1e-4 then exit;
    if abs((t1 - t2) * t1 - Q) > 1e-4 then exit;
    if not answer or (num1 < X) then
      begin answer := true; X := num1; Y := num2; end;
end;

procedure work_sub_4(sinA , cosA : extended);
var
    num1 , num2 ,
    i , j      : longint;
begin
    num1 := trunc(cosA * sqrt(P * 1.0 + Q));
    num2 := trunc(sinA * sqrt(P * 1.0 + Q));
    for i := -1 to 1 do
      for j := -1 to 1 do
        check(num1 + i , num2 + j);
end;

procedure work_sub_3(cosa : extended);
begin
    work_sub_4(sqrt(abs(1 - sqr(cosa))) , cosA);
    work_sub_4(-sqrt(abs(1 - sqr(cosa))) , cosA);
end;

procedure work_sub_2(cos2a : extended);
begin
    work_sub_3(sqrt(abs(cos2a + 1) / 2));
    work_sub_3(-sqrt(abs(cos2a + 1) / 2));
end;

procedure work_sub_1(cos4a : extended);
begin
    work_sub_2(sqrt(abs(cos4a + 1) / 2));
    work_sub_2(-sqrt(abs(cos4a + 1) / 2));
end;

procedure work;
var
    sin4a      : extended;
begin
    answer := false;
    if P + Q <= 0
      then if (P = 0) and (Q = 0)
             then begin answer := true; X := 0; Y := 0; exit; end
             else exit;
    sin4a := - 4.0 * P * P / sqr(P * 1.0 + Q) + 4.0 * P / (P * 1.0 + Q);
    if abs(sin4a) <= 1 then
      begin
          work_sub_1(sqrt(abs(1 - sqr(sin4a))));
          work_sub_1(-sqrt(abs(1 - sqr(sin4a))));
      end;
end;
    
Begin
    read(totCase); nowCase := 1;
    while nowCase <= totCase do
      begin
          read(P , Q);
          writeln('Case ' , nowCase , ':');
          work;
          if answer
            then writeln(X , ' ' , Y)
            else writeln('Impossible.');
          inc(nowCase);
      end;
End.
