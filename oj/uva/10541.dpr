{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10541.in';
    OutFile    = 'p10541.out';
    Limit      = 200;
    LimitLen   = 70;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[1..Limit] of longint;
    Topt       = array[1..Limit + 1 , 1..Limit + 1] of lint;

Var
    data       : Tdata;
    opt        : Topt;
    answer     : lint;
    N , K ,
    Cases      : longint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    read(N , K);
    for i := 1 to K do read(data[i]);
end;

procedure work;
var
    i , j      : longint;       
begin
    for i := 1 to N + 1 do
      begin
          for j := 1 to K + 1 do opt[i , j].init;
          opt[i , K + 1].data[1] := 1;
      end;
    for i := N downto 1 do
      for j := 1 to K do
        if i + data[j] = N + 1
          then opt[i , j].add(opt[i + data[j] , j + 1] , opt[i + 1 , j])
          else if i + data[j] + 1 <= N + 1
                 then opt[i , j].add(opt[i + data[j] + 1 , j + 1] , opt[i + 1 , j])
                 else opt[i , j] := opt[i + 1 , j];
    answer := opt[1 , 1];
end;

procedure out;
begin
    answer.print;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
