#include <stdio.h>
#include <string.h>

int main()
{
  short i, j, n = 0;
  char t[101], c[100][25];

  memset(t, 0xFF, sizeof(t));
  for (i = 2; i < 100; ++i)
    if (t[i] < 0)
      for (t[i] = -n++, j = i * i; j < 101; j += i)
        t[j] = i;

  memset(c[0], 0, sizeof(c[0]));
  for (i = 1; i < 100; ++i) {
    memcpy(c[i], c[i - 1], sizeof(c[0]));
    for (j = i + 1; t[j] > 0; j /= t[j])
      ++c[i][-t[t[j]]];
    ++c[i][-t[j]];
  }

  while (scanf("%hd", &i) == 1 && i) {
    printf("%3hd! = ", i--);
    for (j = 0; j < 15 && c[i][j]; ++j)
      printf("%2hd%c", c[i][j], j < 14 && c[i][j + 1] ? ' ' : '\n');
    if (c[i][15]) {
      printf("       ");
      for (j = 15; j < 25 && c[i][j]; ++j)
        printf("%2hd%c", c[i][j], j < 24 && c[i][j + 1] ? ' ' : '\n');
    }
  }

  return 0;
}
