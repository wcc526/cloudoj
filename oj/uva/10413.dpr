{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10413.in';
    OutFile    = 'p10413.out';
    Limit      = 15;

Type
    Tdata      = array[1..Limit] of
                   record
                       start , go , life     : longint;
                   end;

Var
    data       : Tdata;
    N , answer ,
    max , Cases: longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    max := 0;
    readln(N);
    i := 1;
    while i <= N do
      begin
          with data[i] do
            read(start , go , life);
          if data[i].start > max then
            max := data[i].start;
          if data[i].life = 0 then
            dec(N)
          else
            inc(i);
      end;
end;

procedure solve(a , b , c : longint; var x , y , gcdnum : longint; var cando : boolean);
var
    tmp ,
    tmpnum     : longint;
begin
    if a = 0 then
      begin
          gcdnum := b;
          if c mod gcdnum = 0 then
            begin
                x := 0;
                y := c div gcdnum;
                cando := true;
            end
          else
            cando := false;
      end
    else
      begin
          tmp := b div a;
          solve(b - tmp * a , a , c , x , y , gcdnum , cando);
          if cando then
            begin
                tmpnum := x;
                x := y - tmp * x;
                y := tmpnum;
            end;
      end;
end;

function check(M : longint) : boolean;
var
    i , j ,
    x , k ,
    gcdnum ,
    t1 , t2    : longint;
    cando      : boolean;
begin
    check := false;
    for i := 1 to N do
      if data[i].start > M then
        exit;
    for i := 1 to N do
      for j := i + 1 to N do
        begin
            t2 := data[i].go - data[j].go;
            solve(t2 , -M , data[j].start - data[i].start , x , k , gcdnum , cando);
            gcdnum := abs(gcdnum);
            if not cando then
               continue;
            t1 := M div gcdnum;
            t2 := t2 div gcdnum;
            if x < 0 then
              while (x < 0) do
                begin
                    x := x + t1;
                    k := k + t2;
                end
            else
              while (x - t1 >= 0) do
                begin
                    x := x - t1;
                    k := k - t2;
                end;
            if (x <= data[i].life) and (x <= data[j].life) then
              exit;
        end;
    check := true;
end;

procedure work;
var
    i          : longint;
begin
    answer := -1;
    for i := max to 1000000 do
      if check(i) then
         begin
             answer := i;
             break;
         end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
