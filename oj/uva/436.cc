#include <iostream>
#include <map>
#include <string>

using namespace std;

int main()
{
  int c = 0, i, j, k, n;
  double t, v[30][30];
  string a, b;
  map<string, int> m;

  for (;;) {
    m.clear();
    cin >> n;
    if (n == 0)
      break;
    for (i = n; i--;)
      cin >> a, m[a] = i;
    for (i = n; i--; v[i][i] = 1.0)
      for (j = n; j--; v[i][j] = 0.0);
    cin >> i;
    while (i--)
      cin >> a >> t >> b, v[m[a]][m[b]] = t;
    for (k = n; k--;) {
      for (i = n; i--;) {
        for (j = n; j--;)
          if (v[i][j] < v[i][k] * v[k][j])
            v[i][j] = v[i][k] * v[k][j];
        if (v[i][i] > 1.0) {
          cout << "Case " << ++c << ": Yes" << endl;
          goto l;
        }
      }
    }
    cout << "Case " << ++c << ": No" << endl;
  l:;
  }

  return 0;
}
