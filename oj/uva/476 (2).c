#include <stdio.h>

typedef struct {
  double x, y;
} pt;

typedef struct {
  pt t, b;
} rt;

int main(void)
{
  rt r[10];
  pt p;
  int c = 0, f, i, n = 0;
  char t[2];

  while (scanf("%1s", t) == 1 && *t == 'r')
    scanf("%lf %lf %lf %lf", &r[n].t.x, &r[n].t.y, &r[n].b.x, &r[n].b.y), ++n;

  while (scanf("%lf %lf", &p.x, &p.y) == 2 &&
         !(p.x == 9999.9 && p.y == 9999.9)) {
    for (++c, f = i = 0; i < n; ++i)
      if (p.x > r[i].t.x && p.x < r[i].b.x &&
          p.y > r[i].b.y && p.y < r[i].t.y)
        printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
    if (f == 0)
      printf("Point %d is not contained in any figure\n", c);
  }

  return 0;
}
