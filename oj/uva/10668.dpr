{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10668.in';
    OutFile    = 'p10668.out';
    minimum    = 1e-10;

Var
    answer , 
    L , n , C  : extended;

function init : boolean;
begin
    readln(L , n , C);
    init := (L >= -minimum) and (n >= -minimum) and (c >= -minimum);
end;

function atan(a , b : extended) : extended;
begin
    if abs(b) <= minimum
      then atan := pi / 2
      else atan := arctan(a / b);
end;

procedure work;
var
    start , stop ,
    mid , R , dist , 
    angle ,
    Len , newL : extended;
    i          : longint;
begin
    if abs(L) <= minimum then begin answer := 0; exit; end;
    newL := L * (1 + n * C); 
    start := 0; stop := 1e20;
    for i := 1 to 2000 do
      begin
          mid := (start + stop) / 2;
          R := (sqr(L / 2) + sqr(mid)) / 2 / mid;
          dist := sqrt(sqr(mid) + sqr(L / 2));
          angle := atan((dist / 2) , sqrt(sqr(R) - sqr(dist / 2))) * 4;
          Len := angle * R;          
          if Len > newL
            then stop := mid
            else start := mid;
      end;
    answer := start;
end;

procedure out;
begin
    writeln(answer : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
