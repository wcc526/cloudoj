Const
    InFile     = 'p10263.in';
    Limit      = 100000;
    minimum    = 1e-8;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    TLine      = record
                     A , B , C: extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    M , bestp  : Tpoint;
    N          : longint;

procedure init;
var
    i          : longint;
begin
    readln(M.x , M.y);
    readln(N);
    for i := 1 to N + 1 do
      readln(data[i].x , data[i].y);
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    exit(sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)));
end;

function zero(p : extended) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function samep(p1 , p2 : Tpoint) : boolean;
begin
    exit(zero(p1.x - p2.x) and zero(p1.y - p2.y));
end;

procedure better(x , y : extended);
var
    p          : Tpoint;
begin
    p.x := x; p.y := y;
    if dist(M , p) < dist(M , bestp) then bestp := p;
end;

procedure work;
var
    L1 , L2    : TLine;
    p          : Tpoint;
    i          : longint;
    tmp        : extended;
begin
    bestp := data[N + 1];
    for i := 1 to N do
      begin
          better(data[i].x , data[i].y);
          if samep(data[i] , data[i + 1]) then continue;
          L1.A := data[i + 1].y - data[i].y;
          L1.B := data[i].x - data[i + 1].x;
          L1.C := data[i + 1].x * data[i].y - data[i].x * data[i + 1].y;
          L2.B := -L1.A; L2.A := L1.B;
          L2.C := -(L2.A * M.x + L2.B * M.y);
          tmp := L1.A * L2.B - L1.B * L2.A;
          p.x := -(L1.C * L2.B - L1.B * L2.C) / tmp;
          p.y := -(L1.A * L2.C - L1.C * L2.A) / tmp;
          if zero(dist(p , data[i]) + dist(p , data[i + 1]) - dist(data[i] , data[i + 1])) then
            better(p.x , p.y);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            writeln(bestp.x + minimum : 0 : 4);
            writeln(bestp.y + minimum : 0 : 4);
        end;
//    Close(INPUT);
End.