Const
    Limit      = 10000;

Type
    Tsum       = array[-1..Limit] of longint;

Var
    sum        : Tsum;
    a , b      : longint;

function chk(num , p : extended) : boolean;
var
    i , power ,
    res        : extended;
begin
    i := num - 1; res := 1; power := p;
    while i <> 0 do
      begin
          if int(i / 2) * 2 <> i then res := res * power;
          power := power * power;
          res := res - int(res / num) * num;
          power := power - int(power / num) * num;
          i := int(i / 2);
      end;
    exit(res = 1);
end;

function chk_prime(num : extended) : boolean;
begin
    exit(chk(num , 2) and chk(num , 3) and chk(num , 5) and chk(num , 7));
end;

procedure pre_process;
var
    i          : longint;
begin
    sum[-1] := 0;
    for i := 0 to Limit do
      if chk_prime(i * i + i + 41)
        then sum[i] := sum[i - 1] + 1
        else sum[i] := sum[i - 1];
end;

Begin
    pre_process;
    while not eof do
      begin
          readln(a , b);
          writeln((sum[b] - sum[a - 1]) / (b - a + 1) * 100 : 0 : 2);
      end;
End.