#include <stdio.h>

int n, v[180], x[180], y[180], z[180], g[180][180];

int f(int i)
{
  int j;
  if (v[i] < 0) {
    for (v[i] = 0, j = n; j--;)
      if (g[i][j] && v[i] < f(j))
        v[i] = v[j];
    v[i] += z[i];
  }
  return v[i];
}

int main()
{
  int c = 0, i, j, m;

  while (scanf("%d", &n) == 1 && n) {
    for (i = n; i--;) {
      j = i, scanf("%d %d %d", &x[j], &y[j], &z[j]);
      j += n, x[j] = x[i], y[j] = z[i], z[j] = y[i];
      j += n, x[j] = y[i], y[j] = x[i], z[j] = z[i];
      j += n, x[j] = y[i], y[j] = z[i], z[j] = x[i];
      j += n, x[j] = z[i], y[j] = x[i], z[j] = y[i];
      j += n, x[j] = z[i], y[j] = y[i], z[j] = x[i];
    }
    n *= 6;
    for (i = n; i--; v[i] = -1)
      for (j = n; j--;)
        g[i][j] = x[i] < x[j] && y[i] < y[j] ? 1 : 0;
    for (m = 0, i = n; i--;)
      if (m < f(i))
        m = v[i];
    printf("Case %d: maximum height = %d\n", ++c, m);
  }

  return 0;
}
