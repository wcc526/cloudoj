Type
    Tstr       = string[100];

Var
    base , code ,
    T          : longint;
    answer     : boolean;
    s          : Tstr;

function get_num(base : longint; s : Tstr) : longint;
var
    gather     : set of char;
    i , res    : longint;
begin
    if s = '' then exit(-1);
    gather := [];
    if base <= 10
      then for i := 0 to base - 1 do
             gather := gather + [chr(ord('0') + i)]
      else begin
               gather := ['0'..'9'];
               for i := 0 to base - 11 do
                 gather := gather + [chr(ord('a') + i)];
           end;
    for i := 1 to length(s) do
      if not (s[i] in gather) then
        exit(-1);
    res := 0;
    for i := 1 to length(s) do
      begin
          res := res * base;
          if s[i] <= '9'
            then inc(res , ord(s[i]) - ord('0'))
            else inc(res , ord(s[i]) - ord('a') + 10);
          if res > 17 then res := 17;
      end;
    exit(res);
end;

function Value(s : Tstr) : longint;
var
    p          : longint;
begin
    if s = '' then exit(-1);
    if s[length(s)] <> '#'
      then begin
               exit(Get_Num(10 , s));
           end
      else begin
               delete(s , length(s) , 1);
               p := length(s);
               while (p >= 1) and (s[p] <> '#') do dec(p);
               if p = 0 then exit(-1);
               base := Value(copy(s , 1 , p - 1));
               if (base < 2) or (base > 16) then exit(-1);
               s := copy(s , p + 1 , length(s) - p);
               exit(Get_Num(base , s));
           end;
end;

Begin
    readln(T);
    while T > 0 do
      begin
          dec(T);
          readln(s);
          if Value(s) <> -1
            then writeln('yes')
            else writeln('no');
      end;
End.