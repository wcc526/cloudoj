#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  unsigned long long bd, bn, d, n, p, q, t;
  char s[20], a[20], b[20];
  int i, l;

  while (scanf(" 0.%[0-9]...", s) == 1) {
    bn = bd = 0x7FFFFFFFFFFFFFFFull;
    for (l = i = strlen(s); i--;) {
      strcpy(a, s), a[i] = 0;
      strcpy(b, s + i);
      n = strtoull(a, NULL, 10) * (pow(10, l - i) - 1) + strtoull(b, NULL, 10);
      d = pow(10, i) * (pow(10, l - i) - 1);
      for (p = d, q = n; q;)
        t = p % q, p = q, q = t;
      n /= p, d /= p;
      if (bd > d)
        bd = d, bn = n;
    }
    printf("%lld/%lld\n", bn, bd);
  }

  return 0;
}
