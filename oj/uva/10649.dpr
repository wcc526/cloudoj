{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10649.in';
    OutFile    = 'p10649.out';

Var
    r , L      : extended;

function init : boolean;
begin
    readln(r , L);
    r := r / 2 * sqrt(2);
    init := (r <> 0) or (L <> 0);
end;

procedure workout;
var
    sinA , cosA
               : extended;
begin
    if (L < 0) or (L > r)
      then writeln('INCORRECT INFORMATION !!!')
      else begin
               sinA := L / r; cosA := sqrt(1 - sqr(sinA));
               writeln((sinA + cosA) * r - L : 0 : 6); 
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do workout;
//    Close(OUTPUT);
//    Close(INPUT);
End.
