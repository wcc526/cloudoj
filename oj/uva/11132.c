#include <stdio.h>
#include <string.h>

typedef unsigned int uint;

uint getValue(char *buffer, uint numChars)
{
  uint i, value = 0;
  for (i = 0; i < numChars; i++) {
    if (buffer[i] != 'H' && buffer[i] != 'T')
      return 0xFFFFFFFF;
    value <<= 1;
    if (buffer[i] == 'T')
      value |= 1;
  }
  return value;
}

int main()
{
  char buffer[1024];
  char *current;
  uint i;
  uint numDie, numSides, numChars;
  uint value, temp;

  for (;;) {
    fgets(buffer, sizeof(buffer), stdin);
    sscanf(buffer, "%u%*c%u", &numDie, &numSides);
    if (numDie == 0 && numSides == 0)
      break;

    if (numDie == 0 || numSides == 0) {
      printf("-1\n");
      continue;
    }

    fgets(buffer, sizeof(buffer), stdin);

    numChars = 1;
    while (1 << numChars < numSides)
      numChars++;

    current = buffer;
    value = 0;

    for (i = 0; i < numDie; i++) {
      do {
	temp = getValue(current, numChars);
	if (current > buffer + strlen(buffer))
	  goto error;
	current += numChars;
      } while (temp >= numSides);

      value += temp + 1;
    }

    printf("%u\n", value);
    continue;

  error:
    printf("-1\n");
  }

  return 0;
}
