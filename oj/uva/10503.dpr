{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10503.in';
    OutFile    = 'p10503.out';
    Limit      = 14;
    LimitSave  = 10;

Type
    Tedge      = record
                     p1 , p2  : longint;
                 end;
    Tdata      = array[1..Limit] of Tedge;
    Tused      = array[1..Limit] of boolean;
    Tdegree    = array[0..LimitSave] of longint;
    Tvisited   = array[0..LimitSave] of boolean;

Var
    data       : Tdata;
    degree     : Tdegree;
    visited    : Tvisited;
    used       : Tused;
    st , ed    : Tedge;
    N , M ,
    head , tail: longint;
    answer     : boolean;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               fillchar(degree , sizeof(degree) , 0);
               fillchar(visited , sizeof(visited) , 0);
               read(M);
               read(st.p1 , st.p2 , ed.p1 , ed.p2);
               for i := 1 to M do
                 read(data[i].p1 , data[i].p2);
           end;
end;

procedure dfs(root : longint);
var
    i          : longint;
begin
    if visited[root] then exit;
    visited[root] := true;
    for i := 1 to M do
      if used[i] then
        begin
            if data[i].p1 = root then dfs(data[i].p2);
            if data[i].p2 = root then dfs(data[i].p1);
        end;
end;

function check : boolean;
var
    i          : longint;
begin
    fillchar(degree , sizeof(degree) , 0);
    inc(degree[head]); inc(degree[tail]);
    for i := 1 to M do
      if used[i] then
        begin
            inc(degree[data[i].p1]); inc(degree[data[i].p2]);
        end;
    check := false;
    for i := 0 to LimitSave do
      if odd(degree[i]) then
        exit;
    fillchar(visited , sizeof(visited) , 0);
    for i := 0 to LimitSave do
      if degree[i] <> 0 then
        begin
            dfs(i);
            break;
        end;
    for i := 0 to LimitSave do
      if (degree[i] <> 0) and not visited[i] then
        exit;
    check := true;
end;

function dfs_work(step , count : longint) : boolean;
begin
    dfs_work := false;
    if count > N then exit;
    if M - step + 1 + count < N then exit;
    if step > M
      then dfs_work := check
      else begin
               dfs_work := true;
               used[step] := true;
               if dfs_work(step + 1 , count + 1) then exit;
               used[step] := false;
               if dfs_work(step + 1 , count) then exit;
               dfs_work := false;
           end;
end;

procedure work;
begin
    fillchar(used , sizeof(used) , 0);
    answer := true;
    head := st.p2; tail := ed.p1; if dfs_work(1 , 0) then exit;
    answer := false;
end;

procedure out;
begin
    if answer
      then writeln('YES')
      else writeln('NO');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
