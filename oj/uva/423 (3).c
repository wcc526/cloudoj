#include <stdio.h>

int main()
{
  int i, j, k, n, g[100][100];

  scanf("%d", &n);

  for (i = 0; i < n; g[i][i] = 0, ++i)
    for (j = 0; j < i; ++j)
      if (scanf("%d", &g[i][j]) == 1)
        g[j][i] = g[i][j];
      else
        g[j][i] = g[i][j] = 1000000000, scanf("%*s");

  for (k = n; k--;)
    for (i = n; i--;)
      for (j = n; j--;)
        if (g[i][j] > g[i][k] + g[k][j])
          g[i][j] = g[i][k] + g[k][j];

  for (k = 0, i = n; i--;)
    if (k < g[0][i])
      k = g[0][i];

  printf("%d\n", k);

  return 0;
}
