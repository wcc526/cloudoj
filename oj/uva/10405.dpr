{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10405.in';
    OutFile    = 'p10405.out';
    Limit      = 1000;

Type
    Topt       = array[0..Limit , 0..Limit] of longint;
    Tdata      = array[1..Limit] of char;

Var
    opt        : Topt;
    A , B      : Tdata;
    N , M      : longint;

procedure init;
begin
    N := 0;
    while not eoln do begin inc(N); read(A[N]); end;
    readln;
    M := 0;
    while not eoln do begin inc(M); read(B[M]); end;
    readln; 
end;

procedure work;
var
    i , j      : longint;
begin
    opt[0 , 0] := 0;
    for i := 1 to N do opt[i , 0] := 0;
    for i := 1 to M do opt[0 , i] := 0;
    for i := 1 to N do
      for j := 1 to M do
        if A[i] = B[j]
          then opt[i , j] := opt[i - 1 , j - 1] + 1
          else if opt[i - 1 , j] < opt[i , j - 1]
                 then opt[i , j] := opt[i , j - 1]
                 else opt[i , j] := opt[i - 1 , j];
end;

procedure out;
begin
    writeln(opt[N , M]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
