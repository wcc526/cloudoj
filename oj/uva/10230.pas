Const
    InFile     = 'p10230.in';
    Limit      = 1024;

Type
    Tdata      = array[0..Limit + 1 , 0..Limit + 1] of byte;
    Tstack     = array[1..3] of
                   record
                       x , y  : longint;
                   end;
    Tvisited   = array[1..15] of boolean;

Var
    data       : Tdata;
    stack      : Tstack;
    visited    : Tvisited;
    s          : string[255];
    top ,
    N , X , Y  : longint;

procedure init;
begin
    readln(N , Y , X);
    N := 1 shl N; fillchar(data , sizeof(data) , 0);
end;

procedure Get_Area(x1 , y1 , x2 , y2 , direct : longint; var nx1 , ny1 , nx2 , ny2 : longint);
var
    midx , midy: longint;
begin
    midx := (x1 + x2) div 2; midy := (y1 + y2) div 2;
    if direct = 1 then begin nx1 := x1; ny1 := y1; nx2 := midx; ny2 := midy; end;
    if direct = 2 then begin nx1 := x1; ny1 := midy; nx2 := midx; ny2 := y2; end;
    if direct = 3 then begin nx1 := midx; ny1 := midy; nx2 := x2; ny2 := y2; end;
    if direct = 4 then begin nx1 := midx; ny1 := y1; nx2 := x2; ny2 := midy; end;
end;

procedure add(x , y : longint);
var
    color ,
    nx , ny ,
    dx , dy    : longint;
begin
    inc(top); stack[top].x := x; stack[top].y := y;
    for dx := -1 to 1 do
      for dy := -1 to 1 do
        if (dx <> 0) or (dy <> 0) then
          begin
              nx := x + dx; ny := y + dy;
              if data[nx , ny] <> 0 then
                visited[data[nx , ny]] := true;
          end;
    if top = 3 then
      begin
          color := 1;
          while visited[color] do inc(color);
          data[stack[1].x , stack[1].y] := color;
          data[stack[2].x , stack[2].y] := color;
          data[stack[3].x , stack[3].y] := color;
          fillchar(visited , sizeof(visited) , 0);
          top := 0;
      end;
end;

procedure fill(x1 , y1 , x2 , y2 , direct : longint);
var
    nx1 , ny1 ,
    nx2 , ny2 ,
    i          : longint;
begin
    if x2 - x1 = 2
      then begin
               if direct <> 1 then add(x1 + 1 , y1 + 1);
               if direct <> 2 then add(x1 + 1 , y1 + 2);
               if direct <> 3 then add(x1 + 2 , y1 + 2);
               if direct <> 4 then add(x1 + 2 , y1 + 1);
               exit;
           end;
    for i := 1 to 4 do
      if i <> direct then
        begin
            Get_Area(x1 , y1 , x2 , y2 , i , nx1 , ny1 , nx2 , ny2);
            fill(nx1 , ny1 , nx2 , ny2 , (i + 1) mod 4 + 1);
        end;
    nx1 := (x2 - x1) div 4 + x1; ny1 := (y2 - y1) div 4 + y1;
    nx2 := x2 - (x2 - x1) div 4; ny2 := y2 - (y2 - y1) div 4;
    fill(nx1 , ny1 , nx2 , ny2 , direct);
end;

procedure work;
var
    i ,
    nx1 , ny1 ,
    nx2 , ny2 ,
    x1 , y1 ,
    x2 , y2    : longint;
begin
    x1 := 0; y1 := 0; x2 := N; y2 := N;
    top := 0;
    while x2 > x1 + 1 do
      begin
          for i := 1 to 4 do
            begin
                Get_Area(x1 , y1 , x2 , y2 , i , nx1 , ny1 , nx2 , ny2);
                if (X > nx1) and (Y > ny1) and (X <= nx2) and (Y <= ny2) then
                  begin
                      Fill(x1 , y1 , x2 , y2 , i);
                      break;
                  end;
            end;
          x1 := nx1; y1 := ny1; x2 := nx2; y2 := ny2;
      end;
end;

procedure print(ch : char);
begin
    if length(s) = 255
      then begin write(s); s := ''; end;
    s := s + ch;
end;

procedure out;
var
    i , j      : longint;
begin
    for i := 1 to N do
      begin
          s := '';
          for j := 1 to N do
            if data[i , j] = 0
              then print('*')
              else print(chr(data[i , j] + 96));
          writeln(s);
      end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.