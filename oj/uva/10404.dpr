{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10404.in';
    OutFile    = 'p10404.out';
    Limit      = 1000000;
    LimitN     = 10;

Type
    Tdata      = array[1..LimitN] of longint;
    Topt       = array[0..Limit] of boolean;

Var
    data       : Tdata;
    opt        : Topt;
    N , M      : longint;

procedure init;
var
    i          : longint;
begin
    read(M , N);
    for i := 1 to N do read(data[i]);
    readln;
end;

procedure work;
var
    i , j      : longint;
begin
    opt[0] := false;
    for i := 1 to M do
      begin
          opt[i] := false;
          for j := 1 to N do
            if i >= data[j] then
              if not opt[i - data[j]] then
                begin
                    opt[i] := true;
                    break;
                end;
      end;
end;

procedure out;
begin
    if opt[M]
      then writeln('Stan wins')
      else writeln('Ollie wins');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eoln do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
