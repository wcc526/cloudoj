{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10570.in';
    OutFile    = 'p10570.out';
    Limit      = 500;

Type
    Tdata      = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    N , answer : longint;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    for i := 1 to N do read(data[i]);
    init := (N > 0);
end;

procedure shift;
var
    i , tmp    : longint;
begin
    tmp := data[1];
    for i := 1 to N - 1 do
      data[i] := data[i + 1];
    data[N] := tmp;
end;

procedure process;
var
    i , p , tmp: longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    tmp := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            p := data[i];
            visited[i] := true;
            while p <> i do
              begin
                  inc(tmp);
                  visited[p] := true;
                  p := data[p];
              end;
        end;
    if tmp < answer then answer := tmp;
end;

procedure work;
var
    i , tmp    : longint;
begin
    answer := N;
    for i := 1 to N do
      begin
          shift;
          process;
      end;
    for i := 1 to N div 2 do
      begin
          tmp := data[i]; data[i] := data[N - i + 1];
          data[N - i + 1] := tmp;
      end;
    for i := 1 to N do
      begin
          shift;
          process;
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
