Var
    i , top , N ,
    count      : longint;
Begin
    readln(N);
    while N <> 0 do
      begin
          write(N , ' : ');
          count := 0; top := N;
          i := 2;
          while i * i <= top do
            begin
                if N mod i = 0 then
                  begin
                      inc(count);
                      while (N mod i = 0) do N := N div i;
                  end;
                inc(i);
            end;
          if N <> 1 then inc(count);
          writeln(count);
          readln(N);
      end;
End.