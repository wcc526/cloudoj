{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10540.in';
    OutFile    = 'p10540.out';
    Limit      = 100;

Type
    Tdata      = array[3..Limit , 1..Limit , 1..2] of extended;

Var
    data       : Tdata;
    N , i ,
    Cases      : longint;
    R          : extended;

function common_area(angle : extended) : extended;
var
    a          : extended;
begin
    a := pi - angle;
    common_area := a  - sin(a); 
end;

procedure pre_process;
var
    i , j , k  : longint;
begin
    for i := 3 to Limit do
      for j := (i + 1) div 2 downto 1 do
        begin
            data[i , j , 1] := common_area((j - 1) * 2 * pi / i) * i;
            data[i , j , 2] := data[i , j , 1]; 
            for k := j + 1 to (i + 1) div 2 do
              data[i , j , 2] := data[i , j , 2] - data[i , k , 2] * (k - j + 1);
        end;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      readln(R , N);
      while N <> 0 do
        begin
            inc(cases);
            writeln('Set ' , cases , ':');
            writeln((N + 1) div 2);
            for i := (N + 1) div 2 downto 1 do
              writeln(data[N , i , 1] * R * R : 0 : 4 , ' ' , data[N , i , 2] * R * R : 0 : 4);
            readln(R , N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
