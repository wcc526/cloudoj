#include <stdio.h>

short a[1000][1000];
char s[1001];

void g(short i, short j)
{
  if (i > j) {
    return;
  } else if (i == j) {
    putchar(s[i]);
  } else if (s[i] == s[j]) {
    putchar(s[i]);
    g(i + 1, j - 1);
    putchar(s[i]);
  } else if (a[i + 1][j] < a[i][j - 1] ||
             (a[i + 1][j] == a[i][j - 1] && s[i] < s[j])) {
    putchar(s[i]);
    g(i + 1, j);
    putchar(s[i]);
  } else {
    putchar(s[j]);
    g(i, j - 1);
    putchar(s[j]);
  }
}

int main()
{
  int i, j, k, n;

  for (i = 1000; --i;)
    a[i][i] = a[i][i - 1] = 0;
  a[0][0] = 0;

  while (scanf("%s%n\n", s, &n) == 1) {
    for (k = 1; k < n; ++k)
      for (i = 0, j = k; j < n; ++i, ++j)
        if (s[i] == s[j])
          a[i][j] = a[i + 1][j - 1];
        else
          a[i][j] = 1 + (a[i + 1][j] < a[i][j - 1] ? a[i + 1][j] : a[i][j - 1]);
    printf("%hd ", a[0][n - 1]);
    g(0, n - 1);
    putchar('\n');
  }

  return 0;
}
