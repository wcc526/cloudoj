{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10647.in';
    OutFile    = 'p10647.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of extended;

Var
    data       : Tdata;
    N          : longint;
    bestP ,
    best       : extended;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               for i := 1 to N do read(data[i]);
               init := true;
           end;
end;

procedure work;
var
    i , times1 ,
    times2     : longint;
    tot1 , tot2 ,
    tmpP , tmp : extended;
begin
    tot1 := 0; times1 := 0;
    tot2 := 0; times2 := 0;
    for i := N - 1 downto 2 do
      begin inc(times2); tot2 := tot2 + times2 * sqr(data[i + 1] - data[i]); end;

    best := 1e30;
    for i := 1 to N - 1 do
      begin
          tmpP := ((times1 + 1) * data[i] + (times2 + 1) * data[i + 1]) / N;
          tmp := tot1 + tot2 + sqr(data[i] - tmpP) * (times1 + 1) + sqr(data[i + 1] - tmpP) * (times2 + 1);
          if best > tmp then
            begin
                best := tmp;
                bestP := tmpP;
            end;
          inc(times1); tot1 := tot1 + times1 * sqr(data[i] - data[i + 1]);
          if i <> N - 1 then
            tot2 := tot2 - times2 * sqr(data[i + 1] - data[i + 2]);
          dec(times2);
      end;
end;

procedure out;
begin
    writeln(BestP : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
