Const
    InFile     = 'p10349.in';
    Limit      = 40;
    LimitM     = 10;
    Maximum    = 100000;

Type
    Topt       = array[0..Limit , 0..1 shl LimitM - 1] of longint;
    Tdata      = array[0..Limit] of longint;
    Tcount     = array[0..1 shl LimitM - 1] of longint;

Var
    data       : Tdata;
    opt        : Topt;
    count      : Tcount;
    cases ,
    N , M      : longint;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
    dec(Cases);
    readln(N , M);
    count[0] := 0;
    for i := 1 to 1 shl LimitM - 1 do
      count[i] := count[i div 2] + i mod 2;
    fillchar(data , sizeof(data) , 0);
    for i := 1 to N do
      begin
          for j := 1 to M do
            begin
                data[i] := data[i] * 2;
                read(ch);
                if ch = 'o' then inc(data[i]);
            end;
          readln;
      end;
end;

procedure workout;
var
    upper , tmp ,
    num , newstat ,
    i , j , k  : longint;
begin
    for i := 0 to N do
      for j := 0 to 1 shl M - 1 do
        opt[i , j] := maximum;
    upper := 1 shl M - 1;
    opt[0 , upper] := 0;
    for i := 0 to N - 1 do
      for j := 0 to 1 shl M - 1 do
        if opt[i , j] < maximum then
          for k := 0 to upper do
            begin
                tmp := k or (k div 2);
                num := count[k] + opt[i , j] + count[upper - j];
                newstat := tmp or (upper - j) or data[i + 1];
                if opt[i + 1 , newstat] > num
                  then opt[i + 1 , newstat] := num;
            end;
    writeln(opt[N , upper]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while cases > 0 do
        begin
            init;
            workout;
        end;
//    Close(INPUT);
End.