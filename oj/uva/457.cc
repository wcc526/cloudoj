#include <iostream>
#include <sstream>

using namespace std;

void printDish(int* density) {
    for (int i=0; i<40; ++i) {
        if (density[i] == 0) cout << ' ';
        else if (density[i] == 1) cout << '.';
        else if (density[i] == 2) cout << 'x';
        else cout << 'W';
    }
    cout << endl;
}

int main()
{
    string line;
    int numCases;
    getline(cin, line);
    istringstream is(line);
    is >> numCases;
    getline(cin, line);
    int* density = new int[40];
    for (int i=0; i<numCases; ++i) {
        for (int j=0; j<40; ++j) density[j] = 0;
        density[19] = 1;
        getline(cin, line);
        istringstream is(line);
        int* DNA = new int[10];
        for (int j=0; j<10; ++j) {
            is >> DNA[j];
        }
        for (int l=0; l<50; ++l) {
            printDish(density);
            int* newDensity = new int[40];
            for (int k=0; k<40; ++k) {
                int sum = 0;
                if (k-1 >= 0) sum += density[k-1];
                sum += density[k];
                if (k+1 < 40) sum += density[k+1];
                newDensity[k] = DNA[sum];
            }
            delete [] density;
            density = newDensity;
        }
        delete [] DNA;
        if (i < (numCases-1)) cout << endl;
        getline(cin, line);
    }
    delete [] density;

    return 0;
}
