Const
    InFile     = 'p10487.in';
    OutFile    = 'p10487.out';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of extended;

Var
    data       : Tdata;
    cases ,
    N          : longint;

function init : boolean;
var
    i          : longint;
begin
    read(N);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do
      read(data[i]);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : extended;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function Find_Closest(p : extended; start , stop : longint) : extended;
var
    mid        : longint;
    res        : extended;
begin
    res := 1e15;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if abs(res - p) > abs(data[mid] - p) then res := data[mid];
          if data[mid] > p
            then stop := mid - 1
            else start := mid + 1;
      end;
    exit(res);
end;

procedure work;
var
    i , j      : longint;
begin
    qk_sort(1 , N);
    i := 1; j := 2;
    while j <= N do
      if data[i] <> data[j]
        then begin inc(i); data[i] := data[j]; inc(j); end
        else inc(j);
    N := i;
end;

procedure out;
var
    i , M , j  : longint;
    p ,
    tmp , ans  : extended;
begin
    inc(Cases);
    writeln('Case ' , cases , ':');
    read(M);
    for i := 1 to M do
      begin
          ans := 1e15;
          read(p);
          for j := 1 to N do
            begin
                tmp := Find_Closest(p - data[j] , j + 1 , N);
                if abs(p - data[j] - tmp) < abs(p - ans) then
                  ans := data[j] + tmp;
            end;
          if N = 1 then ans := 0;
          writeln('Closest sum to ' , p : 0 : 0 , ' is ' , ans : 0 : 0 , '.');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.
