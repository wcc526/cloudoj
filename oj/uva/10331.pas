Const
    InFile     = 'p10331.in';
    Limit      = 100;
    LimitE     = 10000;
    Maximum    = 10000000;

Type
    Tedge      = record
                     count ,
                     p1 , p2 , cost          : longint;
                     ok                      : boolean;
                 end;
    Tedges     = array[1..LimitE] of Tedge;
    Tdata      = array[1..Limit , 1..Limit] of longint;

Var
    edges      : Tedges;
    data       : Tdata;
    max ,
    N , M      : longint;

procedure init;
var
    i , j      : longint;
begin
    readln(N , M);
    for i := 1 to N do
      for j := 1 to N do
        data[i , j] := maximum;
    for i := 1 to N do data[i , i] := 0;
    fillchar(edges , sizeof(edges) , 0);
    for i := 1 to M do
      with edges[i] do
        begin
            read(p1 , p2 , cost);
            if data[p1 , p2] > cost then data[p1 , p2] := cost;
            if data[p2 , p1] > cost then data[p2 , p1] := cost;
        end;
    readln;
end;

procedure work;
var
    i , j , k  : longint;
begin
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          if data[i , j] > data[i , k] + data[k , j] then
            data[i , j] := data[i , k] + data[k , j];
    for i := 1 to N do
      for j := i + 1 to N do
        for k := 1 to M do
          with edges[k] do
            if (data[i , p1] + data[p2 , j] + cost = data[i , j]) or
               (data[i , p2] + data[p1 , j] + cost = data[i , j]) then
              inc(count);
    max := 0;
    for k := 1 to M do
      if edges[k].count > max then
        max := edges[k].count;
    for k := 1 to M do
      edges[k].ok := (max = edges[k].count);
end;

procedure out;
var
    k          : longint;
begin
    for k := 1 to M do
      if edges[k].ok then
        write(k , ' ');
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.