Var
    T , N      : longint;
Begin
    readln(T);
    while T > 0 do
      begin
          readln(N);
          if N = 1
            then writeln(0.0 : 0 : 3)
            else writeln(N * N + (sqrt(2) - 1) * sqr(N - 2) : 0 : 3);
          dec(T);
      end;
End.