{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10505.in';
    OutFile    = 'p10505.out';
    Limit      = 200;
    Maximum    = 10000000;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tcolor     = array[1..Limit] of longint;

Var
    map        : Tmap;
    color      : Tcolor;
    N , T ,
    ans , 
    count1 ,
    count2     : longint;

procedure init;
var
    tot , i , p: longint;
begin
    dec(T); read(N);
    fillchar(map , sizeof(map) , 0);
    for i := 1 to N do
      begin
          read(tot);
          while tot > 0 do
            begin
                read(p);
                if (p <= N) and (p >= 1) then
                  begin
                      map[p , i] := true; map[i , p] := true;
                  end;
                dec(tot);
            end;
      end;
end;

procedure dfs(root , c : longint);
var
    i          : longint;
begin
    if color[root] = 0
      then color[root] := c
      else begin
               if  color[root] = c
                 then color[root] := c
                 else begin color[root] := -1; count1 := -Maximum; end;
               exit;
           end;
    if c = 1 then inc(count1) else inc(count2);
    for i := 1 to N do
      if map[root , i] then
        dfs(i , 3 - c);
end;

procedure work;
var
    i          : longint;
begin
    fillchar(color , sizeof(color) , 0);
    ans := 0;
    for i := 1 to N do
      if color[i] = 0 then
        begin
            count1 := 0; count2 := 0;            
            dfs(i , 1);
            if count1 >= 0
              then if count1 > count2
                     then inc(ans , count1)
                     else inc(ans , count2);
        end;
end;

procedure out;
begin
    writeln(ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
