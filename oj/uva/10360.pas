Const
    InFile     = 'p10360.in';
    N          = 1025;

Type
    Tdata      = array[0..N , 0..N] of longint;

Var
    data       : Tdata;
    D , cases ,
    bestx , besty ,
    bestnum    : longint;

procedure init;
var
    i , x , y , M ,
    num        : longint;
begin
    fillchar(data , sizeof(data) , 0);
    dec(Cases);
    read(D , M);
    for i := 1 to M do
      begin
          read(x , y , num);
          inc(data[x + 1 , y + 1] , num);
      end;
end;

procedure work;
var
    tmp ,
    i , j      : longint;
begin
    bestnum := 0; bestx := 0; besty := 0;
    for i := 1 to N do
      for j := 1 to N do
        inc(data[i , j] , data[i - 1 , j] + data[i , j - 1] - data[i - 1 , j - 1]);
    for i := 0 to N - D - D - 1 do
      for j := 0 to N - D - D - 1 do
        begin
            tmp := data[i , j] + data[i + D * 2 + 1 , j + D * 2 + 1]
                  - data[i , j + D * 2 + 1] - data[i + D * 2 + 1 , j];
            if tmp > bestnum then
              begin
                  bestnum := tmp;
                  bestx := i; besty := j;
              end;
        end;
end;

procedure out;
begin
    writeln(bestx + D , ' ' , besty + D , ' ' , bestnum);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.