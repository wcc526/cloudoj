Const
    InFile     = 'p10728.in';
    Limit      = 100;

Type
    Tstr       = string[Limit];
    Tdata      = array[1..Limit] of Tstr;

Var
    A , B      : Tdata;
    tA , tB , T: longint;
    noanswer   : boolean;

procedure read_str(var s : Tstr);
var
    ch         : char;
begin
    s := '';
    repeat if eoln then exit else read(ch); until ch <> ' ';
    while ch <> ' ' do
      begin
          s := s + ch; if eoln then ch := ' ' else read(ch);
      end;
end;

procedure read_line(var tot : longint; var data : Tdata);
var
    s          : Tstr;
begin
    tot := 0;
    while not eof do
      begin
          read_str(s);
          if s = '' then break else begin inc(tot); data[tot] := s; end;
      end;
    readln;
end;

procedure init;
begin
    dec(T);
    read_line(tA , A);
    read_line(tB , B);
end;

procedure work;
var
    i , j      : longint;
    tmp        : Tstr;
    change     : boolean;
begin
    noanswer := true;
    if tA <> tB then exit;
    repeat
      change := false;
      for i := 1 to tA do
        if (A[i , 1] <> '<') or (B[i , 1] <> '<') then
          if (A[i , 1] <> '<') and (B[i , 1] <> '<')
            then if A[i] <> B[i]
                   then exit
                   else
            else if A[i , 1] = '<'
                   then begin
                            change := true;
                            tmp := A[i];
                            for j := 1 to tA do
                              if A[j] = tmp then A[j] := B[i];
                        end
                   else begin
                            change := true;
                            tmp := B[i];
                            for j := 1 to tA do
                              if B[j] = tmp then B[j] := A[i];
                        end;
    until not change;
    noanswer := false;
end;

procedure out;
var
    i          : longint;
begin
    if noanswer
      then writeln('-')
      else for i := 1 to tA do
             begin
                 if A[i , 1] = '<'
                   then write('a')
                   else write(A[i]);
                 if i = tA then writeln else write(' ');
             end;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.