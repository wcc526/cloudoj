#include <stdio.h>

int main()
{
  unsigned short a[100], m;
  int b, i, j, k, n, p, t;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %d", &p, &n);
    for (i = n; i--;)
      for (a[i] = 0, j = p; j--;)
        scanf("%hd", &m), a[i] = (a[i] << 1) + m;
    for (b = 16, m = 1 << p; m--;) {
      for (i = n; i--;)
        for (j = i; j--;)
          if (((a[i] ^ a[j]) & m) == 0)
            goto n;
      for (j = i = p, k = m; i--; k >>= 1)
        if ((k & 1) == 0)
          --j;
      if (b > j)
        b = j;
    n:;
    }
    printf("%d\n", b ? b : 1);
  }

  return 0;
}
