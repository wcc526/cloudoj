Const
    Limit      = 50000;

Var
    i , N      : longint;
    answer     : array[1..Limit] of qword;

Begin
    answer[1] := 1;
    for i := 2 to Limit do
      answer[i] := answer[i - 1] + qword(i) * i * i;
    while not eof do
      begin
          readln(N);
          writeln(answer[N]);
      end;
End.