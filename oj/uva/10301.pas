Const
    InFile     = 'p10301.in';
    OutFile    = 'p10301.out';
    Limit      = 100;

Type
    Tcircle    = record
                     x , y , R               : extended;
                 end;
    Tdata      = array[1..Limit] of Tcircle;
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    map        : Tmap;
    answer ,
    count , N  : longint;

function init : boolean;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(map , sizeof(map) , 0);
    readln(N);
    if N = -1 then exit(false);
    init := true;
    for i := 1 to N do
      with data[i] do
        read(x , y , R);
end;

function touch(c1 , c2 : Tcircle) : boolean;
var
    dist       : extended;
begin
    dist := sqrt(sqr(c1.x - c2.x) + sqr(c1.y - c2.y));
    touch := (dist > abs(c1.R - c2.R)) and (dist < c1.R + c2.R);
end;

procedure dfs(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    inc(count);
    for i := 1 to N do
      if not visited[i] and map[p , i] then
        dfs(i);
end;

procedure work;
var
    i , j      : longint;
begin
    for i := 1 to N do
      for j := 1 to N do
        if i <> j then
          map[i , j] := touch(data[i] , data[j]);
    answer := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            count := 0;
            dfs(i);
            if count > answer then answer := count;
        end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            write('The largest component contains ' , answer);
            if answer = 1
              then writeln(' ring.')
              else writeln(' rings.');
        end;
//    Close(INPUT);
End.
