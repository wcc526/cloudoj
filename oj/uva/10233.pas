Var
    N , M      : longint;
    x1 , y1 ,
    x2 , y2    : double;

procedure Get(Num : longint; var x , y : double);
var
    line , col : longint;
begin
    line := 1;
    while num >= line * 2 - 1 do
      begin
          dec(num , line * 2 - 1);
          inc(line);
      end;
    col := num - line + 1;
    x := col * 0.5;
    y := sqrt(3) / 2 * line;
    if odd(col + line)
      then y := y + sqrt(3) / 2 / 3 * 2
      else y := y + sqrt(3) / 2 / 3;
end;

Begin
    while not eof do
      begin
          readln(N , M);
          Get(N , x1 , y1);
          Get(M , x2 , y2);
          writeln(sqrt(sqr(x1 - x2) + sqr(y1 - y2)) : 0 : 3);
      end;
End.