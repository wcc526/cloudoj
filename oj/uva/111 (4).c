#include <stdio.h>

int main()
{
  short p[20], s[20], b[20], i, j, n, m;

  scanf("%hd", &n);
  for (i = 0; i < n; ++i)
    scanf("%hd", &j), p[i] = j - 1;

  for (;;) {
    for (i = 0; i < n && scanf("%hd", &j) == 1; ++i)
      s[j - 1] = p[i];
    if (i < n)
      break;
    for (i = 0; i < n; ++i)
      b[i] = 1;
    for (i = 1; i < n; ++i)
      for (j = 0; j < i; ++j)
        if (s[i] > s[j] && b[i] < b[j] + 1)
          b[i] = b[j] + 1;
    for (m = i = 0; i < n; ++i)
      if (m < b[i])
        m = b[i];
    printf("%d\n", m);
  }

  return 0;
}
