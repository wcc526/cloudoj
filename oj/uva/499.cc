#include <iostream>
#include <string>

using namespace std;

int main()
{
    string line;
    while (getline(cin, line)) {
        int numWords = 0;
        bool startWord = false;
        for (int i=0; i<line.size(); ++i) {
            if (line[i] >= 'A' && line[i] <= 'z') {
                startWord = true;
            } else {
                if (startWord) {
                    numWords++;
                    startWord = false;
                }
            }
        }
        cout << numWords << endl;
    }

    return 0;
}
