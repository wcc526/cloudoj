{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $0010000000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10521.in';
    OutFile    = 'p10521.out';
    LimitLen   = 2000;

Type
    lint       = object
                     sign ,
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure read_num;
                     procedure init;
                     procedure clearzero;
                     procedure addzero(num1 : lint; num2 : longint);
                     procedure add(num1 , num2 : lint);
                     procedure minus(num1 , num2 : lint);
                     procedure divide(num1 , num2 : lint; var remainder : lint);
                     function bigger(const num2 : lint) : boolean;
                     procedure print;  
                 end;

Var
    A , B , one: lint;

procedure lint.read_num;
var
    c          : char;
    i , tmp    : longint;
begin
    init;
    len := 0;
    while eoln do readln;
    read(c);
    while not (c in ['+' , '-' , '0'..'9']) do read(c);
    while c in ['+' , '-' , '0'..'9'] do
      begin
          if c in ['+' , '-']
            then if c = '-'
                   then sign := -sign
                   else
            else begin
                     inc(len);
                     data[len] := ord(c) - ord('0');
                 end;
          if eoln then begin readln; break; end;
          read(c); 
      end;
    for i := 1 to len div 2 do
      begin
          tmp := data[i]; data[i] := data[len - i + 1];
          data[len - i + 1] := tmp;
      end;
    clearzero;
end;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 1; sign := 1;
end;

procedure lint.clearzero;
begin
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.addzero(num1 : lint; num2 : longint);
var
    i          : longint;
begin
    init;
    for i := 1 to num1.len do
      data[i + num2] := num1.data[i];
    len := num1.len + num2;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    init;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0)  do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.minus(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while i <= num1.len do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0;
          if tmp < 0 then begin jw := 1; inc(tmp , 10); end;
          data[i] := tmp;
          inc(i);
      end;
    len := num1.len;
    clearzero;
end;

procedure lint.divide(num1 , num2 : lint; var remainder : lint);
var
    i          : longint;
    tmp        : lint;
begin
    init;
    len := num1.len - num2.len + 1;
    if len <= 0 then len := 1;
    for i := len downto 1 do
      begin
          tmp.addzero(num2 , i - 1);
          while not tmp.bigger(num1) do
            begin
                inc(data[i]);
                num1.minus(num1 , tmp);
            end;
      end;
    clearzero;
    remainder := num1;
end;

function lint.bigger(const num2 : lint) : boolean;
var
    i          : longint;
begin
    if len <> num2.len
      then bigger := len > num2.len
      else begin
               for i := len downto 1 do
                 if data[i] <> num2.data[i] then
                   begin
                       bigger := data[i] > num2.data[i];
                       exit;
                   end;
               bigger := false;
           end;
end;

procedure lint.print;
var
    i          : longint;
begin
    if sign = -1 then write('-');
    for i := len downto 1 do
      write(data[i]);
end;

procedure init;
begin
    A.read_num;
    B.read_num;
end;

procedure dfs(A , B : lint);
var
    tmp ,
    remainder  : lint;
begin
    tmp.divide(A , B , remainder);
    if A.sign * B.sign = -1 then
      if (remainder.len = 1) and (remainder.data[1] = 0)
        then tmp.sign := -1
        else begin
                 tmp.add(tmp , one);
                 tmp.sign := -1;
                 remainder.minus(B , remainder);
             end;
    tmp.print;
    if (remainder.len = 1) and (remainder.data[1] = 0) then exit;
    write('+1/{');
    B.sign := 1; remainder.sign := 1;
    dfs(B , remainder);
    write('}');
end;

procedure workout;
begin
    A.print; write('/');
    B.print; write(' = ');
    dfs(A , B);
    writeln;
end;

Begin
    one.init;
    one.data[1] := 1;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eoln do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
