Const
    InFile     = 'p10765.in';
    OutFile    = 'p10765.out';
    Limit      = 10000;
    LimitEdge  = 300000;

Type
    Tedge      = record
                     p1 , p2  : longint;
                 end;
    Tdata      = array[1..LimitEdge] of Tedge;
    Tkey       = record
                     p , num  : longint;
                 end;
    Tanswer    = array[1..Limit] of Tkey;
    Tindex     = array[1..Limit] of longint;
    Tnums      = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    index      : Tindex;
    answer     : Tanswer;
    order , low: Tnums;
    visited    : Tvisited;
    tot ,
    N , M0 , M : longint;

function init : boolean;
var
    p1 , p2    : longint;
begin
    read(N , M0);
    if (N + M0 = 0) then exit(false);
    init := true;
    M := 0;
    readln(p1 , p2);
    while p1 <> -1 do
      begin
          inc(p1); inc(p2);
          inc(M); data[M].p1 := p1; data[M].p2 := p2;
          inc(M); data[M].p2 := p1; data[M].p1 := p2;
          readln(p1 , p2);
      end;
end;

procedure qk_pass_edge(start , stop : longint; var mid : longint);
var
    key        : Tedge;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].p1 > key.p1) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].p1 < key.p1) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort_edge(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_edge(start , stop , mid);
          qk_sort_edge(start , mid - 1);
          qk_sort_edge(mid + 1 , stop);
      end;
end;

procedure Build_Graph;
var
    i          : longint;
begin
    qk_sort_edge(1 , M);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[data[i].p1] = 0 then
        index[data[i].p1] := i;
end;

procedure qk_pass_answer(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := answer[tmp]; answer[tmp] := answer[start];
    while start < stop do
      begin
          while (start < stop) and ((answer[stop].num < key.num) or (answer[stop].num = key.num) and (answer[stop].p > key.p)) do dec(stop);
          answer[start] := answer[stop];
          if start < stop then inc(start);
          while (start < stop) and ((answer[start].num > key.num) or (answer[start].num = key.num) and (answer[start].p < key.p)) do inc(start);
          answer[stop] := answer[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    answer[start] := key;
end;

procedure qk_sort_answer(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_answer(start , stop , mid);
          qk_sort_answer(start , mid - 1);
          qk_sort_answer(mid + 1 , stop);
      end;
end;

procedure dfs(p : longint);
var
    i          : longint;
begin
    inc(tot); order[p] := tot;
    low[p] := order[p]; visited[p] := true;
    i := index[p];
    while (i <> 0) and (i <= M) and (data[i].p1 = p) do
      begin
          if visited[data[i].p2]
            then if order[data[i].p2] < low[p]
                   then low[p] := order[data[i].p2]
                   else
            else begin
                     dfs(data[i].p2);
                     if low[data[i].p2] >= order[p]
                       then inc(answer[p].num)
                       else if low[p] > low[data[i].p2] then
                              low[p] := low[data[i].p2];
                 end;
          inc(i);
      end;
end;

procedure work;
var
    i          : longint;
begin
    Build_Graph;
    fillchar(answer , sizeof(answer) , 0);
    for i := 1 to N do answer[i].p := i;

    fillchar(low , sizeof(low) , 0); fillchar(order , sizeof(order) , 0);
    fillchar(visited , sizeof(visited) , 0);
    for i := 2 to N do answer[i].num := 1;
    tot := 0;
    dfs(1);
    qk_sort_answer(1 , N);
end;

procedure out;
var
    i          : longint;
begin
    for i := 1 to M0 do
      writeln(answer[i].p - 1, ' ' , answer[i].num);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.