#include <stdio.h>
#include <iostream>
#include <math.h>
using namespace std; 

int main(int argc, char **argv)
{
	double l, w, max, min;
	
	while(cin >> l >> w){ 
		max = (w + l - sqrt( (double) ( w * w - w * l + l * l )) ) / 6;
		min = ((w>l) ? l:w)/2;
	
		max+=0.0000001;
		min+=0.0000001;
		
		printf( "%.3f %.3f %.3f\n", max, 0.0, min );
	}
	
	return 0;
}
