Var
    a , b , c ,
    p , num    : double;
Begin
    while not eof do
      begin
          readln(a , b , c);
          p := (a + b + c) / 2;
          num := p * (p - a) * (p - b) * (p - c);
          if num >= 1e-6
            then writeln(sqrt(num) * 4 / 3 : 0 : 3)
            else writeln(-1.000 : 0 : 3);
      end;
End.