{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10553.in';
    OutFile    = 'p10553.out';
    angles     : array[0..31] of string
               = ('N' , 'NbE' , 'NNE' , 'NEbN' , 'NE' , 'NEbE' , 'ENE' , 'EbN' ,
                  'E' , 'EbS' , 'ESE' , 'SEbE' , 'SE' , 'SEbS' , 'SSE' , 'SbE' ,
                  'S' , 'SbW' , 'SSW' , 'SWbS' , 'SW' , 'SWbW' , 'WSW' , 'WbS' ,
                  'W' , 'WbN' , 'WNW' , 'NWbW' , 'NW' , 'NWbN' , 'NNW' , 'NbW');
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of
                   record
                       angle , step          : extended;
                   end;

Var
    data       : Tdata;
    deviate ,
    answer     : extended;
    N          : longint;

function init : boolean;
var
    i , j      : longint;
    s          : string;
    ch         : char;
begin
    readln(N);
    if N = 0
      then init := false
      else begin
               init := true;
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do
                 begin
                     s := '';
                     read(ch);
                     while ch <> ' ' do
                       begin
                           s := s + ch;
                           read(ch);
                       end;
                     j := 0;
                     while angles[j] <> s do inc(j);
                     data[i].angle := j * 2 * pi / 32;
                     readln(data[i].step);
                 end;
               readln(deviate);
               deviate := deviate / 180 * pi;
           end;
end;

procedure work;
var
    a , b , c ,
    x0 , y0 , h ,
    x , y ,
    nx , ny    : extended;
    i          : longint;
begin
    x0 := 0; y0 := 0;
    for i := 1 to N do
      begin
          x0 := x0 + cos(data[i].angle + deviate) * data[i].step;
          y0 := y0 + sin(data[i].angle + deviate) * data[i].step;
      end;

    answer := 1e10;
    x := 0; y := 0;
    for i := 1 to N do
      begin
          nx := x + cos(data[i].angle) * data[i].step;
          ny := y + sin(data[i].angle) * data[i].step;
          a := sqrt(sqr(x0 - x) + sqr(y0 - y));
          b := sqrt(sqr(x0 - nx) + sqr(y0 - ny));
          c := sqrt(sqr(x - nx) + sqr(y - ny));
          if answer > a then answer := a;
          if answer > b then answer := b;
          if (b * b < a * a + c * c) and (a * a < b * b + c * c)
            then begin
                     h := sqrt((a + b + c) * (a + b - c) * (a - b + c) * (b + c - a)) / (2 * c);
                     if h < answer then answer := h;
                 end;
      end;
end;

procedure out;
begin
    writeln(answer : 0 : 2);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
