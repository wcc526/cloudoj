{$R-,Q-,S-}
Const
    Limit      = 50;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Topt       = array[0..Limit * Limit] of longint;
    Tstack     = array[1..Limit] of
                   record
                       x , y , Len           : longint;
                   end;

Var
    map        : Tmap;
    opt        : Topt;
    stack ,
    best       : Tstack;
    i ,
    N , bestans: longint;

procedure init;
var
    i , j      : longint;
begin
    readln(N); opt[0] := 0;
    for i := 1 to N * N do
      begin
          opt[i] := maxlongint;
          for j := 1 to trunc(sqrt(i)) do
            if opt[i - j * j] + 1 < opt[i] then
              opt[i] := opt[i - j * j] + 1;
      end;
    fillchar(map , sizeof(map) , 0);
end;

function calc(a , b : longint) : longint;
var
    tmp        : longint;
begin
    if (a = 0) or (b = 0) then exit(0);
    if a < b then begin tmp := a; a := b; b := tmp; end;
    calc := a div b;
end;

procedure GetXY(var startx , starty , least : longint);
var
    last , count ,
    sum ,
    i , j      : longint;
begin
    least := 0;
    for i := 1 to N do
      for j := 1 to N do
        if not map[i , j] then
          if ((i = 1) or map[i - 1 , j]) and ((i = N) or map[i + 1 , j]) or
            ((j = 1) or map[i , j - 1]) and ((j = N) or map[i , j + 1]) then
            inc(least);
    for i := 1 to N do
      for j := 1 to N do
        if not map[i , j] then
          begin
              startx := i; starty := j;
              exit;
          end;
end;

procedure GetTarget(startx , starty : longint; var target : longint);
var
    i          : longint;
    ok         : boolean;
begin
    target := 0;
    while true do
      begin
          ok := true;
          if (target = N - 1) or (startx + target > N) or (starty + target > N) then break;
          for i := 0 to target do
            if map[startx + target , starty + i] or map[startx + i , starty + target] then
              begin
                  ok := false;
                  break;
              end;
          if not ok then break;
          inc(target);
      end;
end;

procedure dfs(nowans , lastarea , startx , starty , least : longint);
var
    delta ,
    i , j , target
               : longint;
begin
    if least < 0 then least := 0;
    if nowans + opt[lastarea] >= bestans then exit;
    if nowans + least >= bestans then exit;
    if lastarea = 0 then
      begin bestans := nowans; best := stack; exit; end;
    while map[startx , starty] do
      if starty < N
        then inc(starty)
        else begin inc(startx); starty := 1; end;
    GetTarget(startx , starty , target);
    for i := 0 to target - 1 do
      for j := 0 to target - 1 do
        map[startx + i , starty + j] := true;
    for i := target downto 1 do
      begin
          delta := 0;
          for j := 0 to i - 1 do
            begin
                if (startx + i <= N) and not map[startx + i , starty + j] and ((startx + i + 1 > N) or map[startx + i + 1 , starty + j]) then inc(delta);
                if (starty + i <= N) and not map[startx + j , starty + i] and ((starty + i + 1 > N) or map[startx + j , starty + i + 1]) then inc(delta);
            end;
          if i = 1 then dec(delta);
          stack[nowans + 1].x := startx;
          stack[nowans + 1].y := starty;
          stack[nowans + 1].Len := i;
          dfs(nowans + 1 , lastarea - i * i , startx , starty , least + delta);
          for j := 0 to i - 1 do
            begin
                map[startx + i - 1 , starty + j] := false;
                map[startx + j , starty + i - 1] := false;
            end;
      end;
end;

Begin
    init;
    bestans := 16;
    dfs(0 , N * N , 1 , 1 , 0);
    writeln(bestans);
    for i := 1 to bestans do
      with best[i] do
        writeln(x , ' ' , y , ' ', Len);
End.
