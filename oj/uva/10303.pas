Const
    LimitLen   = 4000;

Type
    lint       = object
                     Len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure multi(num : longint);
                     procedure divide(num : longint);
                     procedure print;
                 end;

Var
    answer     : lint;
    N , i      : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    Len := 1;
end;

procedure lint.multi(num : longint);
var
    tmp ,
    i , jw     : longint;
begin
    jw := 0; i := 1;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.divide(num : longint);
var
    last , i   : longint;
begin
    last := 0;
    for i := len downto 1 do
      begin
          last := last * 10 + data[i];
          data[i] := last div num;
          last := last mod num;
      end;
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do
      write(data[i]);
end;

Begin
    while not eof do
      begin
          readln(N);
          answer.init;
          answer.data[1] := 1;
          for i := N * 2 downto N + 2 do answer.multi(i);
          for i := 2 to N do answer.divide(i);
          answer.print;
          writeln;
      end;
End.