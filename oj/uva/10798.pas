Const
    InFile     = 'p10798.in';
    OutFile    = 'p10798.out';
    Limit      = 25;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);
    Base       = 11 * 11 * 11;

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;
    Tgraph     = array[1..Limit * Limit div 4 , 1..4] of longint;
    Tcost      = array[1..Limit * Limit div 4] of longint;
    Tvisited   = array[1..Limit * Limit div 4 , 0..14641] of boolean;

Var
    number ,
    data       : Tdata;
    change ,
    graph      : Tgraph;
    cost       : Tcost;
    visited    : Tvisited;
    answer ,
    N , S , T  : longint;

function init : boolean;
var
    i , j      : longint;
    ch         : char;
begin
    readln(N);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     for j := 1 to N do
                       begin
                           read(ch);
                           if ch = 'R'
                             then data[i , j] := 1
                             else data[i , j] := 0;
                       end;
                     readln;
                 end;
           end;
end;

function value(status : longint) : longint;
var
    i , res    : longint;
begin
    res := 0;
    for i := 1 to 4 do
      begin
          if status mod 11 > res then
            res := status mod 11;
          status := status div 11;
      end;
    value := res;
end;

function Fchange(status , signal : longint) : longint;
begin
    case signal of
      0        : Fchange := status;
      1        : Fchange := status mod Base * 11 + status div Base;
      2        : Fchange := status mod 11 * Base + status div 11;
    end;
end;

procedure dfs(p , status : longint);
var
    i          : longint;
begin
    if visited[p , status] then exit;
    visited[p , status] := true;
    if value(status) >= answer then exit;
    if p = T then
      begin
          answer := value(status);
          exit;
      end;
    for i := 1 to 4 do
      if graph[p , i] <> 0 then
        dfs(graph[p , i] , Fchange(status , change[p , i]) + cost[graph[p , i]]);
end;

procedure work;
var
    i , j , k ,
    x , y ,
    count      : longint;
begin
    count := 0;
    fillchar(cost , sizeof(cost) , 0);
    for i := 1 to N div 2 do
      for j := 1 to N div 2 + 1 do
        begin
            inc(count);
            number[i , j] := count;
            cost[number[i , j]] := data[i , j];
        end;
    for i := 1 to N div 2 + 1 do
      for j := N div 2 + 2 to N do
        begin
            number[i , j] := number[N - j + 1 , i];
            cost[number[i , j]] := cost[number[i , j]] * 11 + data[i , j];
        end;
    for i := N div 2 + 2 to N do
      for j := N div 2 + 1 to N do
        begin
            number[i , j] := number[N - i + 1 , N - j + 1];
            cost[number[i , j]] := cost[number[i , j]] * 11 + data[i , j];
        end;
    for i := N div 2 + 1 to N do
      for j := 1 to N div 2 do
        begin
            number[i , j] := number[j , N - i + 1];
            cost[number[i , j]] := cost[number[i , j]] * 11 + data[i , j];
        end;

    fillchar(graph , sizeof(graph) , 0);
    fillchar(change , sizeof(change) , 0);
    S := count + 1; T := count + 2;
    for i := 1 to N div 2 do
      for j := 1 to N div 2 + 1 do
        for k := 1 to 4 do
          begin
              x := i + dirx[k]; y := j + diry[k];
              if (x <> y) or (x <> N div 2 + 1)
                then if (x >= 1) and (y >= 1)
                       then graph[number[i , j] , k] := number[x , y]
                       else graph[number[i , j] , k] := T
                else graph[S , k] := number[i , j];
              if x > N div 2 then change[number[i , j] , k] := 1;
              if y > N div 2 + 1 then change[number[i , j] , k] := 2;
          end;

    answer := N div 2;
    fillchar(visited , sizeof(visited) , 0);
    dfs(S , 0);
end;

procedure out;
begin
    writeln('At most ' , answer , ' rose(s) trampled.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
