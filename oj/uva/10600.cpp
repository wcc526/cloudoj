#include <iostream>
#include <cstdio>
#include <cstring>
#define EXIST 1
#define NOT_EXIST 0
#define USE 2
using namespace std;
const int MaxN = 110;
const int Inf = 0x7fffffff;
int g[MaxN][MaxN];
int N,M;
char use[MaxN][MaxN];
int ans1,ans2,F[MaxN][MaxN];
int prime()
{
    int dis[MaxN];
    bool vis[MaxN];
    int pre[MaxN];
    memset(pre,0xff,sizeof(pre));
    memset(vis,false,sizeof(vis));
    for(int i = 0 ; i < MaxN; ++i)
        dis[i] = Inf;
    dis[1] = 0;
    for(int i = 1; i <= N; ++i)
    {
        int pos;
        int tmp = Inf;
        for(int j = 1; j <= N; ++j)
        {
            if(!vis[j] && dis[j] < tmp)
            {
                tmp = dis[j];
                pos = j;
            }
        }
        if(pre[pos] != -1)
        {
            use[pre[pos]][pos] = use[pos][pre[pos]] = USE;
            for(int j = 1; j <= N; ++j)
            {
                if(vis[j])
                    F[j][pos] = max(F[j][pre[pos]],g[pre[pos]][pos]);
            }
        }

        vis[pos] = true;
        ans1 += dis[pos];
        for(int j = 1; j <= N; ++j)
        {
            if(!vis[j] && g[pos][j]!= 0 && g[pos][j] < dis[j])
            {
                dis[j] = g[pos][j];
                pre[j] = pos;
            }
        }
    }
    return ans1;
}
int Second_MST()
{
    ans2 = Inf;
    for(int i = 1; i <= N; ++i)
    {
        for(int j = 1; j <= N; ++j)
        {
            if(use[i][j] == EXIST)
            {
                if(ans1 + g[i][j] - F[i][j] < ans2)
                    ans2 = ans1 + g[i][j] - F[i][j];
            }
        }
    }
    return ans2;
}
int main()
{
    int T;
    scanf("%d",&T);
    while(T--)
    {
        memset(g,0,sizeof(g));
        memset(use,0,sizeof(use));
        memset(F,0,sizeof(F));
        ans1 = ans2 = 0;
        scanf("%d%d",&N,&M);
        for(int i = 0 ; i < M; ++i)
        {
            int A,B,C;
            scanf("%d%d%d",&A,&B,&C);
            g[A][B] = g[B][A] = C;
            use[A][B] = use[B][A] = EXIST;
        }
        printf("%d ",prime());
        printf("%d\n",Second_MST());
    }
    return 0;
}
