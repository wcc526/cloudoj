Const
    InFile     = 'p10788.in';
    OutFile    = 'p10788.out';
    Limit      = 200;

Type
    Tvisited   = array[1..Limit , 1..Limit] of longint;

Var
    visited    : Tvisited;
    totCase ,
    cases      : longint;
    s          : string[255];

procedure init;
begin
    inc(cases);
    readln(s);
    fillchar(visited , sizeof(visited) , $FF);
end;

function dfs(start , stop : longint) : longint;
var
    i          : longint;
begin
    if start > stop
      then exit(1)
      else if visited[start , stop] <> -1
             then exit(visited[start , stop])
             else begin
                      visited[start , stop] := 0;
                      for i := start + 1 to stop do
                        begin
                            if s[start] = s[i] then
                              inc(visited[start , stop] , dfs(start + 1 , i - 1) * dfs(i + 1 , stop));
                            if visited[start , stop] > 1 then
                              begin
                                  visited[start , stop] := 2;
                                  break;
                              end;
                        end;
                      exit(visited[start , stop]);

                  end;
end;

procedure out;
var
    tmp        : longint;
begin
    write('Case ' , Cases , ': ');
    tmp := dfs(1 , length(s));
    case tmp of
      0        : writeln('Invalid');
      1        : writeln('Valid, Unique');
      2        : writeln('Valid, Multiple');
    end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase);
      cases := 0;
      while cases < totCase do
        begin
            init;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.