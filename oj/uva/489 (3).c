#include <stdio.h>

int main(void)
{
  int c, l, r, w;

  while (scanf("%d", &r) == 1 && r >= 0) {
    printf("Round %d\n", r);
    r = l = w = 0;
    while ((c = getchar()) > '\n');
    while ((c = getchar()) > '\n')
      w |= 1 << (c - 'a');
    while ((c = getchar()) > '\n')
      l |= 1 << (c - 'a');
    for (c = 26; c--;)
      if (((l | w) ^ w) & (1 << c))
        ++r;
    puts((w | l) ^ l ? r < 7 ? "You chickened out." : "You lose." : "You win.");
  }

  return 0;
}
