#include <stdio.h>
#include <string.h>

int main()
{
  int i, j, k, l, n;
  char v[100][101];

  for (n = 0; gets(v[n]); ++n);

  for (j = i = 0; i < n; ++i) {
    l = strlen(v[i]);
    if (j > l) {
      for (k = l; k < j; ++k)
        v[i][k] = ' ';
      v[i][k] = 0;
    } else {
      j = l;
    }
  }

  for (i = 0; v[n - 1][i]; ++i) {
    for (j = n; j-- && v[j][i];)
      putchar(v[j][i]);
    putchar('\n');
  }

  return 0;
}
