#include <math.h>
#include <stdio.h>

#define PI 3.1415926535897932384626433832795
#define D 92900000.0
#define P 3950.0
#define S 432000.0
#define A ((2.0 * PI) / (60.0 * 60.0 * 24.0))

int main()
{
  const double h = sqrt(S * S + D * D);
  const double a = acos(P / h);
  const double b = atan(S / D);
  const double s = PI * S * S;
  const double r = 1.0 / s;
  double f, t, v;

  while (scanf("%lf", &t) == 1) {
    t = a - t * A;
    v = (h * cos(t) - P) / sin(t + b);
    if (v > S) {
      t = v / S - 1;
      if (t > 1.0)
        f = 1.0;
      else
        t = acos(t), f = ((PI - t) * S + (v - S) * sin(t)) * S * r;
    } else {
      t = acos(1 - v / S);
      f = 1.0 - ((PI - t) * S + (S - v) * sin(t)) * S * r;
      if (f < 0.0)
        f = 0.0;
    }
    printf("%.3lf\n", f);
  }

  return 0;
}
