Const
    InFile     = 'p10321.in';
    Limit      = 100;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    TLine      = record
                     A , B , C: extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tanswer    = object
                     tot      : longint;
                     data     : array[1..Limit * Limit] of Tpoint;
                     procedure add(p : Tpoint);
                 end;

Var
    A , B      : Tdata;
    answer     : Tanswer;
    tA , tB    : longint;

function init : boolean;
var
    i          : longint;
begin
    read(tA); if tA < 3 then exit(false);
    for i := 1 to tA do with A[i] do read(x , y);
    read(tB); if tB < 3 then exit(false);
    for i := 1 to tB do with B[i] do read(x , y);
    init := true;
end;

function zero(p : extended) : boolean;
begin
    exit(abs(p) <= minimum);
end;

procedure Tanswer.add(p : Tpoint);
var
    i          : longint;
begin
    for i := 1 to tot do
      if zero(data[i].x - p.x) and zero(data[i].y - p.y) then
        exit;
    i := tot + 1;
    while (i > 1) and (zero(data[i - 1].x - p.x) and (data[i - 1].y > p.y) or (data[i - 1].x > p.x)) do
      begin
          data[i] := data[i - 1];
          dec(i);
      end;
    inc(tot); data[i] := p;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p2.y - p1.y; L.B := p1.x - p2.x;
    L.C := -p1.x * p2.y + p2.x * p1.y;
end;

function Get_Crossing(L1 , L2 : TLine; var p : Tpoint) : boolean;
var
    tmp        : extended;
begin
    tmp := L1.A * L2.B - L1.B * L2.A;
    if zero(tmp) then exit(false);
    Get_Crossing := true;
    p.x := - (L1.C * L2.B - L1.B * L2.C) / tmp;
    p.y := - (L1.A * L2.C - L1.C * L2.A) / tmp;
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    exit(sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)));
end;

function onLine(p1 , p2 , p : Tpoint) : boolean;
begin
    exit(zero(dist(p1 , p2) - dist(p1 , p) - dist(p , p2)));
end;

function tri_area(p1 , p2 , p3 : Tpoint) : extended;
begin
    p2.x := p2.x - p1.x; p3.x := p3.x - p1.x;
    p2.y := p2.y - p1.y; p3.y := p3.y - p1.y;
    tri_area := abs(p2.x * p3.y - p2.y * p3.x) / 2;
end;

function onPoly(const data : Tdata; N : longint; p : Tpoint) : boolean;
var
    area       : extended;
    i          : longint;
begin
    area := 0;
    for i := 2 to N - 1 do
      area := area + tri_Area(data[1] , data[i] , data[i + 1]);
    for i := 1 to N do
      area := area - tri_Area(p , data[i] , data[i mod N + 1]);
    exit(zero(area));
end;

procedure work;
var
    i , j      : longint;
    p          : Tpoint;
    L1 , L2    : TLine;
begin
    answer.tot := 0;
    for i := 1 to tA do
      for j := 1 to tB do
        begin
            Get_Line(A[i] , A[i mod tA + 1] , L1);
            Get_Line(B[j] , B[j mod tB + 1] , L2);
            if Get_Crossing(L1 , L2 , p) then
              if onLine(A[i] , A[i mod tA + 1] , p) then
                if onLine(B[j] , B[j mod tB + 1] , p) then
                  answer.add(p);
        end;
    for i := 1 to tA do
      if onPoly(B , tB , A[i]) then
        answer.add(A[i]);
    for i := 1 to tB do
      if onPoly(A , tA , B[i]) then
        answer.add(B[i]);
end;

procedure out;
var
    i          : longint;
begin
    writeln(answer.tot);
    for i := 1 to answer.tot do
      writeln(answer.data[i].x + minimum * 2 : 0 : 2 , ' ' , answer.data[i].y + minimum * 2 : 0 : 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.
