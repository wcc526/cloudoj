Const
    InFile     = 'p10205.in';
    suit       : array[0..3] of string
               = ('Clubs' , 'Diamonds' , 'Hearts' , 'Spades');
    value      : array[0..12] of string
               = ('2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' , '10' ,
                  'Jack' , 'Queen' , 'King' , 'Ace');
    Limit      = 100;

Type
    Tpermu     = array[1..Limit , 1..52] of longint;
    Tdata      = array[0..1 , 1..52] of longint;

Var
    permu      : Tpermu;
    data       : Tdata;
    N , cases  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    readln(N);
    for i := 1 to N do
      for j := 1 to 52 do
        read(permu[i , j]);
    readln;
end;

procedure work;
var
    i , j ,
    k , now    : longint;
begin
    now := 0;
    for i := 1 to 52 do data[now , i] := i - 1;
    while not eof and not eoln do
      begin
          readln(k);
          now := 1 - now;
          for i := 1 to 52 do
            data[now , i] := data[1 - now , permu[k , i]];
      end;
    for i := 1 to 52 do
      begin
          j := data[now , i];
          writeln(value[j mod 13] , ' of ' , suit[j div 13]);
      end;
    if cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
        end;
//    Close(INPUT);
End.