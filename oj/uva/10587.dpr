{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10587.in';
    OutFile    = 'p10587.out';
    Limit      = 10002;

Type
    Tdata      = object
                     tot      : longint;
                     data     : array[1..Limit * 2] of longint;
                     procedure init;
                     procedure ins(num : longint);
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                     procedure arrange;
                     function index(num : longint) : longint;
                 end;
    Tseg       = array[1..Limit] of
                   record
                       L , R  : longint;
                   end;
    Tnext      = array[1..Limit * 2] of longint;

Var
    data , path: Tdata;
    seg        : Tseg;
    next       : Tnext;
    N , cases ,
    answer     : longint;

procedure Tdata.init;
begin
    fillchar(data , sizeof(data) , 0);
    tot := 0;
end;

procedure Tdata.ins(num : longint);
begin
    inc(tot);
    data[tot] := num;
end;

procedure Tdata.qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure Tdata.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure Tdata.arrange;
var
    i , p      : longint;
begin
    qk_sort(1 , tot);
    p := 1;
    for i := 2 to tot do
      if data[p] <> data[i] then
        begin
            inc(p);
            data[p] := data[i];
        end;
    for i := p + 1 to tot do data[i] := 0;
    tot := p;
end;

function Tdata.index(num : longint) : longint;
var
    mid , 
    st , ed    : longint;
begin
    index := 0;
    st := 1; ed := tot;
    while st <= ed do
      begin
          mid := (st + ed) div 2;
          if data[mid] = num
            then begin index := mid; exit; end
            else if data[mid] < num
                   then st := mid + 1
                   else ed := mid - 1; 
      end;
end;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    read(N);
    for i := 1 to N do
      begin
          read(seg[i].L , seg[i].R);
          inc(seg[i].R);
      end;
end;

procedure work;
var
    j , 
    i , st , ed: longint;
    find       : boolean;
begin
    data.init;
    for i := 1 to N do begin data.ins(seg[i].L); data.ins(seg[i].R); end;
    data.arrange;

    fillchar(next , sizeof(next) , 0);
    path.init; answer := 0;
    for i := N downto 1 do
      begin
          path.tot := 0;
          st := data.index(seg[i].L); ed := data.index(seg[i].R);
          find := false;
          while st < ed do
            begin
                path.ins(st);
                if next[st] = 0
                  then begin find := true; inc(st); end
                  else st := next[st];
            end;
          for j := 1 to path.tot do
            next[path.data[j]] := st;
          if find then inc(answer);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
