Var
    a , b , c ,
    a1 , b1 , c1 ,
    tmp
               : double;
    count      : longint;

procedure swap(var a , b : double);
var
    tmp        : double;
begin
    tmp := a; a := b; b := tmp;
end;

Begin
    count := 0;
    while not eof do
      begin
          inc(count);
          readln(a , b , c , a1 , b1 , c1);
          writeln('Set #' , count);
          if b < b1 then begin swap(a , b); swap(a1 , b1); end;
          if c < c1 then begin swap(a , c); swap(a1 , c1); end;
          if a >= a1
            then writeln(0.000000 : 0 : 6)
            else begin
                     a1 := a1 / 2; b1 := b1 / 2; c1 := c1 / 2;
                     a := a - a1;
                     tmp := 1 - sqr(a / a1);
                     b1 := b1 * sqrt(tmp); c1 := c1 * sqrt(tmp);
                     writeln(b1 * c1 * pi : 0 : 6);
                 end;
      end;
End.