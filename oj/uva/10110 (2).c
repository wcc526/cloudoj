#include <math.h>
#include <stdio.h>

unsigned int g()
{
  unsigned int r;
  short c;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  while ((c = getc(stdin)) >= '0')
    r = r * 10 + c - '0';
  return r;
}

int main()
{
  unsigned int i, n;

  while ((n = g()))
    i = sqrt(n), puts(i * i == n ? "yes" : "no");

  return 0;
}
