Const
    InFile     = 'p10453.in';
    OutFile    = 'p10453.out';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of char;
    Topt       = array[1..Limit , 1..Limit] of longint;

Var
    answer ,
    data       : Tdata;
    opt        : Topt;
    tot , N    : longint;

procedure init;
begin
    N := 0;
    while not eoln do
      begin inc(N); read(data[N]); end;
    readln;
end;

function dfs(start , stop : longint) : longint;
var
    t1 , t2    : longint;
begin
    if start > stop
      then exit(0)
      else if opt[start , stop] = -1
             then begin
                      if data[start] = data[stop]
                        then opt[start , stop] := dfs(start + 1 , stop - 1)
                        else begin
                                 t1 := dfs(start + 1 , stop) + 1;
                                 t2 := dfs(start , stop - 1) + 1;
                                 if t1 < t2 then opt[start , stop] := t1 else opt[start , stop] := t2;
                             end;
                      exit(opt[start , stop]);
                  end
             else exit(opt[start , stop]);
end;

procedure dfs_print(start , stop : longint);
begin
    if start <= stop
      then if data[start] = data[stop]
             then begin inc(tot); answer[tot] := data[start]; dfs_print(start + 1 , stop - 1); end
             else if dfs(start + 1 , stop) < dfs(start , stop - 1)
                    then begin inc(tot); answer[tot] := data[start]; dfs_print(start + 1 , stop); end
                    else begin inc(tot); answer[tot] := data[stop]; dfs_print(start , stop - 1); end;
end;

procedure work;
begin
    fillchar(opt , sizeof(opt) , $FF);
    dfs(1 , N);
    tot := 0;
    dfs_print(1 , N);
end;

procedure out;
var
    i          : longint;
begin
    write(opt[1 , N] , ' ');
    for i := 1 to tot do write(answer[i]);
    if odd(opt[1 , N] + N)
      then for i := tot - 1 downto 1 do write(answer[i])
      else for i := tot downto 1 do write(answer[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while not eof do
      begin
          init;
          if N = 0 then break;
          work;
          out;
      end;
End.