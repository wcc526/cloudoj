#include <stdio.h>

int pal(char t[6])
{
  if (t[0] == '0')
    if (t[1] == '0')
      if (t[3] == '0')
        return 1;
      else
        return t[3] == t[4];
    else
      return t[1] == t[4];
  else
    return t[0] == t[4] && t[1] == t[3];
}

int main()
{
  int c;
  char t[6];

  scanf("%d", &c);
  while (c--) {
    scanf("%s", t);
    do {
      if (t[4] < '9')
        ++t[4];
      else
        ++t[3], t[4] = '0';
      if (t[3] == '6') {
        ++t[1], t[3] = '0';
        if (t[1] > '9')
          ++t[0], t[1] = '0';
        else if (t[1] == '4' && t[0] == '2')
          t[0] = t[1] = '0';
      }
    } while (!pal(t));
    puts(t);
  }

  return 0;
}
