#include <stdio.h>
#include <string.h>

int main(void)
{
  int i, j, n;
  unsigned short p[4798];
  unsigned char s[46431];

  memset(s, 0xFF, sizeof(s));
  for (i = 2; i < 216; ++i)
    if (s[i])
      for (j = i * i; j < sizeof(s); j += i)
        s[j] = 0;
  for (n = 0, i = 1; i < sizeof(s); ++i)
    if (s[i])
      p[n++] = i;

  while (scanf("%d", &n) == 1 && n) {
    printf("%d = ", n);
    if (n < 0) {
      printf("-1 x ");
      n = -n;
    }
    for (i = j = 1; i < 4798 && p[i] < n; ++i) {
      while (n % p[i] == 0) {
        n /= p[i];
        printf("%s%d", j ? "" : " x ", p[i]);
        j = 0;
      }
    }
    if (n > 1)
      printf("%s%d", j ? "" : " x ", n);
    putchar('\n');
  }

  return 0;
}
