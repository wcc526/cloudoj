Var
    count ,
    N          : longint;

procedure print(ch : char; times : longint);
var
    i          : longint;
begin
    for i := 1 to times do write(ch);
end;

procedure workout;
var
    i , j , k  : longint;
begin
    i := 1;
    while N - (i + 1) * (i + 2) div 2 > i + 1 do inc(i);
    for j := 1 to i do
      for k := 1 to j do
        begin
            print(' ' , 2 * (i + 1 - j));
            if K <> 1
              then write('#..')
              else write('#**');
            print('.' , 4 * (j - 1));
            if K <> 1
              then write('..#')
              else write('**#');
            writeln;
        end;
    for k := 1 to N - i * (i + 1) div 2 do
      begin
          if K <> 1
            then write('#..')
            else write('#**');
          print('.' , 4 * (i - 1) + 2);
          write('..#');
          writeln;
      end;
end;

Begin
    count := 0;
    while not eof do
      begin
          inc(count);
          readln(N);
          writeln('Tower #' , count);
          workout;
      end;
End.