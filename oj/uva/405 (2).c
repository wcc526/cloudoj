#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define G(x)                                    \
  scanf("%s", t);                               \
  if ((s = vg(v, t)) < 0)                       \
    vi(v, t, s = ++w);                          \
  x = s;

typedef struct {
  char n[11];
  int i;
} I;

typedef struct {
  int i, c, a, p, o;
} R;

typedef struct {
  int i, n;
  R *r;
} M;

typedef struct {
  int n, s;
  I **v;
} V;

void vc(V *v)
{
  v->n = 0;
  v->s = 8;
  v->v = malloc(v->s * sizeof(*v));
}

void vd(V *v)
{
  int i;
  for (i = v->n; i--;)
    free(v->v[i]);
  free(v->v);
}

void vi(V *v, const char *n, int d)
{
  int i;
  I *r = malloc(sizeof(*r));
  strcpy(r->n, n);
  r->i = d;
  if (v->n == v->s) {
    v->s += 8;
    v->v = realloc(v->v, v->s * sizeof(*v));
  }
  for (i = v->n++; i-- && strcmp(v->v[i]->n, n) > 0; v->v[i + 1] = v->v[i]);
  v->v[i + 1] = r;
}

int vg(V *v, const char *n)
{
  int l = 0, h = v->n - 1, m, i;
  while (l <= h) {
    m = l + ((h - l) >> 1);
    i = strcmp(v->v[m]->n, n);
    if (i > 0)
      h = m - 1;
    else if (i < 0)
      l = m + 1;
    else
      return v->v[m]->i;
  }
  return -1;
}

const char *vr(V *v, int n)
{
  int i;
  for (i = v->n; i--;)
    if (v->v[i]->i == n)
      return v->v[i]->n;
  return "";
}

int main(void)
{
  int a, c = 0, i, j, k, mn, n, s, w;
  M m[10];
  V v[1];
  R d;
  char t[11];

  while (scanf("%d", &mn) == 1) {
    printf("Scenario # %d\n", ++c);
    vc(v);
    vi(v, "*", (w = 0));
    for (i = mn; i--;) {
      scanf("%s %d", t, &m[i].n);
      if ((s = vg(v, t)) < 0)
        vi(v, t, s = ++w);
      m[i].i = s;
      m[i].r = malloc(m[i].n * sizeof(*m[i].r));
      for (j = m[i].n; j--;) {
        G(m[i].r[j].i);
        G(m[i].r[j].c);
        G(m[i].r[j].a);
        G(m[i].r[j].p);
        G(m[i].r[j].o);
      }
    }

    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
      G(d.i);G(d.c);G(d.a);G(d.p);G(d.o);
      s = 0;
      a = d.i;
    S:
      for (j = mn; j--;) {
        if (m[j].i == a) {
          if (s & (1 << j)) {
            printf("%d -- circular routing detected by %s\n", i + 1, vr(v, a));
            goto D;
          }
          s |= 1 << j;
          for (k = m[j].n; k--;) {
            if ((!d.c || !m[j].r[k].c || m[j].r[k].c == d.c) &&
                (!d.a || !m[j].r[k].a || m[j].r[k].a == d.a) &&
                (!d.p || !m[j].r[k].p || m[j].r[k].p == d.p) &&
                (!d.o || !m[j].r[k].o || m[j].r[k].o == d.o)) {
              a = m[j].r[k].i;
              if (a == m[j].i) {
                printf("%d -- delivered to %s\n", i + 1, vr(v, a));
                goto D;
              }
              goto S;
            }
          }
          printf("%d -- unable to route at %s\n", i + 1, vr(v, a));
          goto D;
        }
      }
    D:;
    }

    for (i = mn; i--;)
      free(m[i].r);
    vd(v);
    putchar('\n');
  }

  return 0;
}
