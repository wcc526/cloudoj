/**
 * UVa 516 Prime Land
 * Author: chchwy
 * Last Modified: 2011.07.20
 */
#include<iostream>
#include<sstream>
#include<vector>
#include<cstdio>
#include<cmath>
#include<cstring>
using namespace std;

enum { MAX=32767+1 };

vector<int> prime;

// 用埃式篩法建質數表
int sieve() {
    char map[MAX];
    memset(map, 1, sizeof(map));

    map[0]=map[1]=0;

    for(int i=2;i<MAX;++i) {
        if( map[i] )
            for(int j=i*i; j<MAX; j+=i )
                map[j] = 0;
    }

    for(int i=0;i<MAX;++i)
        if( map[i]==1 )
            prime.push_back(i);
}

// 把質因數分解表示法轉成整數值
int to_int_value( string & line )
{
    int result = 1;

    istringstream sin(line);

    int base, exp;
    while( sin>>base>>exp ) {
        for(int i=0; i<exp; ++i)
            result = result*base;
    }
    return result;
}

bool is_prime( int n )
{
    for(int i=0; prime[i]*prime[i]<=n; ++i)
        if( n%prime[i]==0 )
            return false;
    return true;
}

// 把整數轉回質因數表示法
void to_prime_base_string(int num)
{
    if( is_prime(num) ) { // special case
        printf("%d 1\n", num);
        return;
    }

    bool first = true;

    for(int i=prime.size()-1; i>=0; --i) {

        int factor_exp = 0;

        while(num%prime[i] == 0) {
            num = num / prime[i];
            factor_exp++;
        }

        if( factor_exp==0 ) continue;

        if( !first )
            printf(" ");
        first = false;

        printf("%d %d", prime[i], factor_exp);
    }
    printf("\n");
}

int main()
{
#ifndef ONLINE_JUDGE
    freopen("516.in","r",stdin);
#endif
    sieve();

    string line;
    while(getline(cin,line)) {
        if(line=="0") break;

        int num = to_int_value(line);
        num = num-1;
        to_prime_base_string(num);
    }
    return 0;
}
