#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<algorithm>

using namespace std;

int n,m,a[5][70000],pr[70000];
long long ans,sum[70000],dp[5][70000];
char ch[333];

int main()
{
	int i,j,k,l,ne[50009];
	for(i=1;i<=50000;i++) pr[i] = 1;
	for(i=2;i<=300;i++)
		for(j=i;j*i<=50000;j++) pr[i*j] = 0;
	
	k = 50001;
	for(i=50000;i>0;i--)
	{
		ne[i] = k;
		if(pr[i] == 0) k = i;
	}
	
	ch['S'] = 1; ch['H'] = 2; ch['C'] = 3; ch['D'] = 4;
	
	while(scanf("%d %d %d",&n,&m,&k) != EOF)
	{
		if(n == 0) break;
		for(i=1;i<=4;i++) for(j=1;j<=m;j++) a[i][j] = 1;
		while(k--)
		{
			scanf("%s",ch);
			l = 0;
			for(i=0;ch[i]<'A';i++) l = l*10+ch[i]-48;
			j = ch[ch[i]];
			a[j][l] = 0;
		}
		
		for(i=0;i<=4;i++) for(j=0;j<=m;j++) dp[i][j] = 0;
		dp[0][0] = 1;
		for(i=1;i<=4;i++)
		{
			sum[0] = dp[i-1][0];
			for(j=1;j<=m;j++) sum[j] = sum[j-1] + dp[i-1][j];
			for(j=1;j<=m;j++) dp[i][j] = sum[j-1];
			for(j=1;j<=m;j++) if(pr[j] == 1)
			{
				for(k=m;k>=j;k--) dp[i][k] -= dp[i-1][k-j];
			}
			
			for(j=4;j<=m;j=ne[j]) if(a[i][j] == 0)
			{
				//printf("%d %d\n",i,j);
				for(k=m;k>=j;k--) dp[i][k] -= dp[i-1][k-j];
			}
		}
		for(i=n;i<=m;i++) printf("%lld\n",dp[4][i]);
		printf("\n");
	}
	return 0;
}
