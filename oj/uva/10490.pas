Var
    N          : longint;

function prime(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to trunc(sqrt(num)) do
      if num mod i = 0 then
        exit(false);
    exit(true);
end;

Begin
    readln(N);
    while N <> 0 do
      begin
          if prime(N)
            then if prime(1 shl (N - 1) - 1 + 1 shl (N - 1))
                   then writeln('Perfect: ' , qword(1) shl (N - 1) * (qword(1) shl N - 1) , '!')
                   else writeln('Given number is prime. But, NO perfect number is available.')
            else writeln('Given number is NOT prime! NO perfect number is available.');
          readln(N);
      end;
End.