#include <stdio.h>

typedef struct t {
  signed c : 8;
  unsigned n : 24;
} t;

int main(void)
{
  t s[52];
  int c, i, m;

l:
  for (c = 52; c-- > 26;)
    s[c].c = c + ('a' - 26), s[c].n = 0;
  while (c--)
    s[c].c = c + 'A', s[c].n = 0;
  while ((c = getchar()) > 0) {
    if (c >= 'a' && c <= 'z') {
      ++s[c - ('a' - 26)].n;
    } else if (c >= 'A' && c <= 'Z') {
      ++s[c - 'A'].n;
    } else if (c == '\n') {
      for (m = 0, i = 52; i--;)
        if (m < s[i].n)
          m = s[i].n;
      for (i = 0; i < 52; ++i)
        if (m == s[i].n)
          putchar(s[i].c);
      printf(" %d\n", m);
      goto l;
    }
  }

  return 0;
}
