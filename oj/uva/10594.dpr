{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10594.in';
    OutFile    = 'p10594.out';
    Limit      = 200;

Type
    Tmap       = array[1..Limit , 1..Limit , 1..2] of
                   record
                       C , W                 : comp;
                   end;
    Tshortest  = array[1..Limit] of
                   record
                       visited               : boolean;
                       time                  : comp;
                       father , code         : longint;
                   end;

Var
    map        : Tmap;
    shortest   : Tshortest;
    N          : longint;
    D , answer : comp;

function read_num(var num : longint) : boolean;
var
    ch         : char;
begin
    while not eof do
      begin
          read(ch);
          if ch in ['0'..'9'] then break;
      end;
    read_num := false;
    if eof then exit;
    read_num := true;
    num := 0;
    while ch in ['0'..'9'] do
      begin
          num := num * 10 + ord(ch) - ord('0');
          if eof then break else read(ch);
      end;
end;

function init : boolean;
var
    i , M ,
    p1 , p2    : longint;
    W , K      : comp;
begin
    fillchar(map , sizeof(map) , 0);
    if not read_num(N) then begin init := false; exit; end;
    init := true;
    read(M);
    for i := 1 to M do
      begin
          read(p1 , p2 , W);
          map[p1 , p2 , 1].C := 1; map[p1 , p2 , 1].W := W;
          map[p2 , p1 , 1].C := 1; map[p2 , p1 , 1].W := W;
          map[p1 , p2 , 2].W := -W; map[p2 , p1 , 2].W := -W;
      end;
    readln(D , K);
    for p1 := 1 to N do
      for p2 := 1 to N do
        map[p1 , p2 , 1].C := map[p1 , p2 , 1].C * K;
end;

function extend : boolean;
var
    i , j , p  : longint;
    max        : comp;
    changed    : boolean;
begin
    fillchar(shortest , sizeof(shortest) , 0);
    shortest[1].visited := true; shortest[1].time := 0;
    changed := true;
    while changed do
      begin
          changed := false;
          for i := 1 to N do
            if shortest[i].visited then
              for j := 1 to N do
                for p := 1 to 2 do
                  if map[i , j , p].C > 0 then
                    if not shortest[j].visited or (shortest[j].time > shortest[i].time + map[i , j , p].W) then
                      begin
                          shortest[j].visited := true;
                          shortest[j].time := shortest[i].time + map[i , j , p].W;
                          shortest[j].father := i; shortest[j].code := p;
                          changed := true;
                      end;
      end;
    if not shortest[N].visited then begin extend := false; exit; end;
    extend := true;
    p := N; max := D;
    while p <> 1 do
      begin
          j := shortest[p].code; i := shortest[p].father;
          if map[i , p , j].C < max then max := map[i , p , j].C;
          p := i;
      end;
    p := N;
    while p <> 1 do
      begin
          j := shortest[p].code; i := shortest[p].father;
          map[i , p , j].C := map[i , p , j].C - max;
          map[p , i , 3 - j].C := map[p , i , 3 - j].C + max;
          p := i;
      end;
    answer := answer + max * shortest[N].time;
    D := D - max;
end;

procedure work;
begin
    answer := 0;
    while (D > 0) and extend do;
end;

procedure print(num : comp);
begin
    if num = 0
      then exit
      else begin
               print(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure out;
begin
    if D > 0
      then writeln('Impossible.')
      else begin
               print(answer);
               if answer = 0 then write(0);
               writeln;
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
