Const
    InFile     = 'p10340.in';
    Limit      = 1000000;

Type
    Tdata      = array[1..Limit] of char;

Var
    A , B      : Tdata;
    LA , LB    : longint;

procedure init;
var
    ch         : char;
begin
    LA := 0; LB := 0;
    repeat read(ch); until ch <> ' ';
    while ch <> ' ' do begin inc(LA); A[LA] := ch; read(ch); end;
    repeat read(ch); until ch <> ' ';
    while ch <> ' ' do begin inc(LB); B[LB] := ch; if not eoln then read(ch) else ch := ' '; end;
    readln;
end;

procedure out;
var
    i , p      : longint;
begin
    p := 0;
    for i := 1 to LA do
      begin
          inc(p);
          while (p <= LB) and (B[p] <> A[i]) do inc(p);
          if p > LB then begin writeln('No'); exit; end;
      end;
    writeln('Yes');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            out;
        end;
//    Close(INPUT);
End.