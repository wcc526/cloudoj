Const
    LimitLen   = 1000;
    Limit      = 250;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    data       : Tdata;
    N , i , p  : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

Begin
    data[0].init; data[0].data[1] := 1;
    data[1].init; data[1].data[1] := 1;
    for i := 2 to Limit do
      begin
          data[i].add(data[i - 1] , data[i - 2]);
          data[i].add(data[i] , data[i - 2]);
      end;

    while not eof do
      begin
          readln(p);
          data[p].print;
          writeln;
      end;
End.