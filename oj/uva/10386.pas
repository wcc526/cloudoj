Const
    K1         = 9.29381004616310700;
    K2         = 10.73008793884865900;

Var
    i          : longint;
    N          : double;

Begin
    readln(i);
    while i > 0 do
      begin
          dec(i);
          readln(N);
          writeln(K1 * N : 0 : 12 , ' ' , K2 * N : 0 : 12);
      end;
End.
