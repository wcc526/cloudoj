Var
    dim , i , res
               : longint;
    answer ,
    dist , D ,
    CL         : extended;
Begin
    while not eof do
      begin
          readln(dim , dist , d);
          res := 0;
          for i := 1 to dim + 1 do res := res + i div 2;
          if abs(D - dist / 2) <= 1e-6
            then CL := pi * D
            else CL := arctan(dist / 2 / sqrt(sqr(D) - sqr(dist) / 4)) * 2 * D;
          answer := CL * (res div 2) + (res - res div 2) * dist;
          writeln(res , ' ' , int(answer + 0.5 + 1e-6) : 0 : 0);
      end;
End.