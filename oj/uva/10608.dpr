{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10608.in';
    OutFile    = 'p10608.out';
    Limit      = 30000;
    LimitM     = 500000;

Type
    Tedge      = record
                     p1 , p2  : longint;
                 end;
    Tdata      = array[1..LimitM * 2] of Tedge;
    Tvisited   = array[1..Limit] of boolean;
    Tindex     = array[1..Limit] of longint;

Var
    data , tmp : Tdata;
    visited    : Tvisited;
    index ,
    tot        : Tindex;
    N , M , T ,
    answer     : longint;

procedure init;
var
    i          : longint;
begin
    dec(T); 
    fillchar(data , sizeof(data) , 0);
    fillchar(visited , sizeof(visited) , 0);
    readln(N , M);
    for i := 1 to M do
      begin
          readln(data[i].p1 , data[i].p2);
          data[i + M].p1 := data[i].p2;
          data[i + M].p2 := data[i].p1;
      end;
end;

procedure sort;
var
    i , p      : longint;
begin
    fillchar(tot , sizeof(tot) , 0);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M * 2 do
      inc(tot[data[i].p1]);
    index[1] := 1;
    for i := 2 to N do
      index[i] := index[i - 1] + tot[i - 1];
    fillchar(tot , sizeof(tot) , 0);
    tmp := data;
    for i := 1 to M * 2 do
      begin
          p := tmp[i].p1;
          data[tot[p] + index[p]] := tmp[i];
          inc(tot[p]);
      end;
end;

procedure dfs(root : longint; var count : longint);
var
    i          : longint;
begin
    inc(count);
    visited[root] := true;
    i := index[root];
    while (i <= M * 2) and (i <> 0) and (data[i].p1 = root) do
      begin
          if not visited[data[i].p2] then
            dfs(data[i].p2 , count);
          inc(i);
      end;
end;

procedure work;
var
    count , i  : longint;
begin
    sort;
    
    answer := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            count := 0;
            dfs(i , count);
            if count > answer then answer := count;
        end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
