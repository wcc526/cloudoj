Const
    InFile     = 'p10342.in';
    Limit      = 100;
    Maximum    = 1000000;

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;
    Tshortest  = array[1..Limit] of
                   record
                       count                 : longint;
                       data                  : array[1..2] of longint;
                   end;

Var
    data ,
    answer     : Tdata;
    shortest   : Tshortest;
    cases ,
    N , M      : longint;

procedure init;
var
    p1 , p2 , cost ,
    i          : longint;
begin
    fillchar(data , sizeof(data) , $FF);
    fillchar(answer , sizeof(answer) , $FF);
    readln(N , M);
    for i := 1 to M do
      begin
          read(p1 , p2 , cost); inc(p1); inc(p2);
          data[p1 , p2] := cost; data[p2 , p1] := cost;
      end;
end;

procedure Dijkstra(startp : longint);
var
    i , min , tmp ,
    minnum     : longint;
begin
    fillchar(shortest , sizeof(shortest) , 0);
    for i := 1 to N do
      begin
          shortest[i].data[1] := maximum;
          shortest[i].data[2] := maximum;
      end;
    shortest[startp].data[1] := 0;
    while true do
      begin
          minnum := maximum;
          for i := 1 to N do
            if shortest[i].count < 2 then
              if shortest[i].data[shortest[i].count + 1] < minnum then
                begin
                    min := i;
                    minnum := shortest[i].data[shortest[i].count + 1];
                end;

          if minnum = maximum then exit;
          inc(shortest[min].count);
          if shortest[min].count = 2 then
            answer[startp , min] := minnum;
          for i := 1 to N do
            if data[min , i] <> -1 then
              begin
                  tmp := data[min , i] + minnum;
                  if (tmp = shortest[i].data[1]) or (tmp = shortest[i].data[2]) then continue;
                  if (shortest[i].count = 0) and (tmp < shortest[i].data[1]) then
                    begin
                        shortest[i].data[2] := shortest[i].data[1]; shortest[i].data[1] := tmp;
                    end
                  else
                    if (shortest[i].count < 2) and (tmp < shortest[i].data[2]) then
                      shortest[i].data[2] := tmp;
              end;
      end;
end;

procedure work;
var
    i          : longint;
begin
    for i := 1 to N do
      Dijkstra(i);
end;

procedure out;
var
    i , Q ,
    p1 , p2    : longint;
begin
    readln(Q);
    inc(Cases);
    writeln('Set #' , cases);
    for i := 1 to Q do
      begin
          readln(p1 , p2); inc(p1); inc(p2);
          if answer[p1 , p2] = -1
            then writeln('?')
            else writeln(answer[p1 , p2]);
      end;
    readln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.