Const
    Limit      = 9;
    LimitSave  = 362880;

Type
    Tdata      = array[1..Limit] of char;
    Tstr       = string[Limit];

Var
    data       : Tdata;
    s          : Tstr;
    i , j ,
    N          : longint;

procedure init;
begin
    N := 0;
    while not eoln do
      begin
          inc(N); read(data[N]);
      end;
    readln;
end;

procedure print;
var
    i          : longint;
begin
    s := '';
    for i := 1 to N do s := s + data[i];
    writeln(s);
end;

procedure workout(step : longint);
var
    i , j      : longint;
    tmp        : char;
begin
    if step = 1 then begin print; exit; end;
    workout(step - 1);
    for i := 1 to step - 1 do
      begin
          if odd(step) then j := 1 else j := i;
          tmp := data[j]; data[j] := data[step]; data[step] := tmp;
          workout(step - 1);
      end;
end;

Begin
    while not eof do
      begin
          init;
          workout(N);
          writeln;
      end;
End.
