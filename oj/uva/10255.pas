Const
    InFile     = 'p10255.in';
    dirx       : array[1..8] of longint = (-1 , -1 , -2 , -2 , 2 , 2 , 1 , 1);
    diry       : array[1..8] of longint = (2 , -2 , -1 , 1 , 1 , -1 , -2 , 2);
    Limit      = 50;
    ans6       : array[1..6 , 1..6] of longint
               = ((25, 32, 11,  2, 19, 34) ,
                  (10,  1, 26, 33, 12,  3) ,
                  (31, 24,  9, 18, 35, 20) ,
                  ( 8, 17, 36, 27,  4, 13) ,
                  (23, 30, 15,  6, 21, 28) ,
                  (16,  7, 22, 29, 14,  5));
    ans8       : array[1..8 , 1..8] of longint
               = ((    1,  22,   3,  18,  43,  38,  13,  16) ,
                  (    4,  19,  64,  39,  14,  17,  46,  37) ,
                  (   23,   2,  21,  42,  63,  44,  15,  12) ,
                  (   20,   5,  54,  49,  40,  47,  36,  45) ,
                  (   53,  24,  41,  62,  55,  50,  11,  32) ,
                  (    6,  27,  56,  51,  48,  33,  60,  35) ,
                  (   25,  52,  29,   8,  61,  58,  31,  10) ,
                  (   28,   7,  26,  57,  30,   9,  34,  59));

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tpath      = object
                     tot      : longint;
                     data     : array[1..Limit * Limit] of Tpoint;
                     procedure add(p : Tpoint);
                     procedure print(x0 , y0 : longint);
                 end;
    Tanswer    = array[1..Limit] of Tpath;
    Tdata      = array[-1..Limit + 2 , -1..Limit + 2] of longint;

Var
    answer     : Tanswer;
    path1 , path2 ,
    path3 , path4
               : Tpath;
    N , x0 , y0: longint;

procedure Tpath.add(p : Tpoint);
begin
    inc(tot); data[tot] := p;
end;

procedure Tpath.print(x0 , y0 : longint);
var
    ans        : Tdata;
    i , j , count
               : longint;
begin
    if tot = 0
      then begin writeln('No Circuit Tour.'); exit; end;
    i := 1;
    while (data[i].x <> x0) or (data[i].y <> y0) do inc(i);
    count := 0;
    for j := i to tot do
      begin
          inc(count);
          ans[data[j].x , data[j].y] := count;
      end;
    for j := 1 to i - 1 do
      begin
          inc(count);
          ans[data[j].x , data[j].y] := count;
      end;
    for i := 1 to trunc(sqrt(tot)) do
      begin
          for j := 1 to trunc(sqrt(tot)) do
            write(ans[i , j] : 5);
          writeln;
      end;
end;

function connect(p1 , p2 : Tpoint) : boolean;
var
    a , b      : longint;
begin
    a := abs(p1.x - p2.x); b := abs(p1.y - p2.y);
    exit((a = 1) and (b = 2) or (a = 2) and (b = 1));
end;

function merge_path(path1 , path2 : Tpath; var path3 : Tpath) : boolean;
var
    i , j , k  : longint;
begin
    path3.tot := 0;
    for i := 1 to path1.tot do
      for j := 1 to path2.tot do
        begin
            if connect(path1.data[i] , path2.data[j]) then
              if connect(path1.data[i mod path1.tot + 1] , path2.data[j mod path2.tot + 1]) then
                begin
                    for k := 1 to i do path3.add(path1.data[k]);
                    for k := j downto 1 do path3.add(path2.data[k]);
                    for k := path2.tot downto j + 1 do path3.add(path2.data[k]);
                    for k := i + 1 to path1.tot do path3.add(path1.data[k]);
                    exit(true);
                end;
            if connect(path1.data[i] , path2.data[j mod path2.tot + 1]) then
              if connect(path1.data[i mod path1.tot + 1] , path2.data[j]) then
                begin
                    for k := 1 to i do path3.add(path1.data[k]);
                    for k := j + 1 to path2.tot do path3.add(path2.data[k]);
                    for k := 1 to j do path3.add(path2.data[k]);
                    for k := i + 1 to path1.tot do path3.add(path1.data[k]);
                    exit(true);
                end;
        end;
    exit(false);
end;

procedure Get_Path1(N : longint);
var
    Len ,
    i , j      : longint;
    data       : Tdata;
    p2 , p3 , p4
               : Tpoint;

  function dfs(x , y , step : longint) : boolean;
  var
      nx , ny ,
      i        : longint;
  begin
      data[x , y] := step; data[y , N - x + 1] := -step;
      data[N - x + 1 , N - y + 1] := -step;
      data[N - y + 1 , x] := -step;
      inc(path1.tot);
      path1.data[path1.tot].x := x; path1.data[path1.tot].y := y;
      for i := 1 to 8 do
        begin
            nx := x + dirx[i]; ny := y + diry[i];
            if (nx = 1) and (ny = 1) and (step = Len) then
              exit(true);
            if step < Len then
              if data[nx , ny] = 0 then
                if dfs(nx , ny , step + 1) then
                  exit(true);
        end;
      data[x , y] := 0; data[y , N - x + 1] := 0;
      data[N - x + 1 , N - y + 1] := 0;
      data[N - y + 1 , x] := 0;
      dec(path1.tot);
      exit(false);
  end;

begin
    fillchar(data , sizeof(data) , $FF);
    for i := 1 to N do
      for j := 1 to N do
        if (i <= 2) or (i >= N - 1) or (j <= 2) or (j >= N - 1) then
          data[i , j] := 0;
    path1.tot := 0; path2.tot := 0; path3.tot := 0; path4.tot := 0;
    Len := (N * N - sqr(N - 4)) div 4;
    dfs(1 , 1 , 1);
    for i := 1 to path1.tot do
      with path1.data[i] do
        begin
            p2.x := y; p2.y := N - x + 1;
            p3.x := N - x + 1; p3.y := N - y + 1;
            p4.x := N - y + 1; p4.y := x;
            path2.add(p2); path3.add(p3); path4.add(p4);
        end;
end;

procedure Get_Path2_sub(data : Tdata; code : longint; var path : Tpath);
var
    x , y ,
    nx , ny ,
    i          : longint;
    found      : boolean;
begin
    path.tot := 0;
    x := 1; y := code;
    while true do
      begin
          data[x , y] := 0; found := false;
          inc(path.tot); path.data[path.tot].x := x; path.data[path.tot].y := y;
          for i := 1 to 8 do
            begin
                nx := x + dirx[i]; ny := y + diry[i];
                if data[nx , ny] = code then
                  begin
                      found := true;
                      x := nx; y := ny;
                      break;
                  end;
            end;
          if not found then break;
      end;
end;

procedure Get_Path2(N : longint);
var
    data       : Tdata;
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    for i := 1 to N div 4 do
      begin
          data[1 , i * 4 - 3] := 1; data[2 , i * 4 - 3] := 3;
          data[1 , i * 4 - 2] := 2; data[2 , i * 4 - 2] := 4;
          data[1 , i * 4 - 1] := 3; data[2 , i * 4 - 1] := 1;
          data[1 , i * 4 - 0] := 4; data[2 , i * 4 - 0] := 2;
      end;
    for i := 1 to N do
      begin
          data[N , i] := data[1 , N - i + 1];
          data[N - 1 , i] := data[2 , N - i + 1];
      end;
    for i := 3 to N - 2 do
      case i mod 4 of
        3      : begin data[i , 1] := 2; data[i , 2] := 1; data[i , N - 1] := 4; data[i , N] := 3; end;
        0      : begin data[i , 1] := 4; data[i , 2] := 3; data[i , N - 1] := 2; data[i , N] := 1; end;
        1      : begin data[i , 1] := 1; data[i , 2] := 2; data[i , N - 1] := 3; data[i , N] := 4; end;
        2      : begin data[i , 1] := 3; data[i , 2] := 4; data[i , N - 1] := 1; data[i , N] := 2; end;
      end;
    Get_Path2_sub(data , 1 , path1);
    Get_Path2_sub(data , 2 , path2);
    Get_Path2_sub(data , 3 , path3);
    Get_Path2_sub(data , 4 , path4);
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(answer , sizeof(answer) , 0);
    answer[6].tot := 36;
    for i := 1 to 6 do
      for j := 1 to 6 do
       with answer[6].data[ans6[i , j]] do
         begin x := i; y := j; end;
    answer[8].tot := 64;
    for i := 1 to 8 do
      for j := 1 to 8 do
        with answer[8].data[ans8[i , j]] do
          begin x := i; y := j; end;

    i := 10;
    while i <= Limit do
      begin
          answer[i] := answer[i - 4];
          for j := 1 to answer[i].tot do
            begin
                inc(answer[i].data[j].x , 2);
                inc(answer[i].data[j].y , 2);
            end;
          Get_Path1(i);
          merge_path(answer[i] , path1 , answer[i]);
          merge_path(answer[i] , path2 , answer[i]);
          merge_path(answer[i] , path3 , answer[i]);
          merge_path(answer[i] , path4 , answer[i]);
          inc(i , 4);
      end;

    i := 12;
    while i <= Limit do
      begin
          answer[i] := answer[i - 4];
          for j := 1 to answer[i].tot do
            begin
                inc(answer[i].data[j].x , 2);
                inc(answer[i].data[j].y , 2);
            end;
          Get_Path2(i);
          merge_path(answer[i] , path1 , answer[i]);
          merge_path(answer[i] , path2 , answer[i]);
          merge_path(answer[i] , path3 , answer[i]);
          merge_path(answer[i] , path4 , answer[i]);
          inc(i , 4);
      end;
end;

Begin
    work;
    while not eof do
      begin
          readln(N , x0 , y0);
          answer[N].print(x0 , y0);
          writeln;
      end;
End.
