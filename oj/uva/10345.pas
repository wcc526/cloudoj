Const
    r1         = sqrt(2) / 2;
    r2         = sqrt(5) / 2;
    r3         = sqrt(17) * 5 / 16;
    r4         = sqrt(2);
    r5         = sqrt(10) / 2;
    r6         = 1.68854296820284770;

Var
    i , tot    : longint;
    x          : extended;

Begin
    readln(tot);
    for i := 1 to tot do
      begin
          readln(x);
          writeln(r1 * x : 0 : 11 , ' ' , r2 * x : 0 : 11 , ' ' ,
                  r3 * x : 0 : 11 , ' ' , r4 * x : 0 : 11 , ' ' ,
                  r5 * x : 0 : 11 , ' ' , r6 * x : 0 : 11);
      end;
End.