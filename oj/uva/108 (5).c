#include <stdio.h>

int main()
{
  short a[100][100], p[100][100];
  int i, j, k, n, c, m = -128;

  scanf("%d", &n);
  for (i = 0; i < n; ++i)
    for (j = 0; j < n; ++j)
      scanf("%hd", &a[i][j]), p[i][j] = a[i][j] + (i ? p[i - 1][j] : 0);
  for (i = 0; i < n; ++i) {
    for (j = i; j < n; ++j) {
      for (c = k = 0; k < n; ++k) {
        c += p[j][k] - (i ? p[i - 1][k] : 0);
        if (m < c)
          m = c;
        if (c < 0)
          c = 0;
      }
    }
  }

  printf("%d\n", m);

  return 0;
}
