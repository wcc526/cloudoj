{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10625.in';
    OutFile    = 'p10625.out';
    LimitLen   = 30;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen] of smallint;
                 end;
    Ttrans     = array[#33..#126 , #33..#126] of longint;
    Tdata      = array[0..1 , #33..#126] of lint;
    Tappeared  = array[#33..#126] of boolean;

Var
    trans      : Ttrans;
    data       : Tdata;
    appeared   : Tappeared;
    one        : lint;
    T , R , Q ,
    now        : longint;

procedure init;
var
    x , c      : char;
begin
    dec(T);
    readln(R);
    fillchar(trans , sizeof(trans) , 0);
    fillchar(appeared , sizeof(appeared) , 0);
    while R > 0 do
      begin
          read(x);
          appeared[x] := true;
          read(c , c);
          while not eoln do
            begin
                read(c);
                inc(trans[x , c]);
            end;
          readln;
          dec(R);
      end;
end;

procedure add(var num1 : lint; const num2 : lint);
var
    i , jw ,
    tmp        : longint;                       
begin
    i := 1; jw := 0;
    while ((i <= num1.len) or (i <= num2.len) or (jw <> 0)) and (i <= LimitLen) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          num1.data[i] := tmp mod 10;
          inc(i);
      end;
    if i > LimitLen
      then begin
               num1.len := LimitLen;
               num1.data[LimitLen] := 1;
           end
      else begin
               num1.len := i - 1;
               while (num1.len > 1) and (num1.data[num1.len] = 0) do dec(num1.len); 
           end;
end;

procedure times(const num1 : lint; num2 : longint; var num3 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    if num2 = 0
      then num3.len := 1
      else begin
               i := 1; jw := 0;
               while ((i <= num1.len) or (jw <> 0)) and (i <= LimitLen) do
                 begin
                     tmp := num1.data[i] * num2 + jw;
                     jw := tmp div 10;
                     num3.data[i] := tmp mod 10;
                     inc(i);
                 end;
               if i > LimitLen
                 then begin
                          num3.len := LimitLen;
                          num3.data[LimitLen] := 1;
                      end
                 else begin
                          num3.len := i - 1;
                          while (num3.len > 1) and (num3.data[num3.len] = 0) do dec(num3.len);
                      end;
           end;
end;

procedure run(tot : longint);
var
    i          : longint;
    tmp        : lint;
    c1 , c2    : char;   
begin
    now := 0;
    for i := 1 to tot do
      begin
          now := 1 - now;
          fillchar(data[now] , sizeof(data[now]) , 0);
          for c1 := #33 to #126 do
            if (data[1 - now , c1].len > 1) or (data[1 - now , c1].data[1] <> 0) then
              if appeared[c1]
                then for c2 := #33 to #126 do
                       if trans[c1 , c2] <> 0 then
                         begin
                             times(data[1 - now , c1] , trans[c1 , c2] , tmp);
                             add(data[now , c2] , tmp);
                         end
                       else
                else add(data[now , c1] , data[1 - now , c1]);
      end;
end;

procedure work;
var
    i , tot    : longint;
    c          : char;
begin
    fillchar(one , sizeof(one) , 0);
    one.len := 1; one.data[1] := 1;

    readln(Q);
    while Q > 0 do
      begin
          fillchar(data , sizeof(data) , 0);
          read(c);
          while c <> ' ' do
            begin
                add(data[0 , c] , one);
                read(c);
            end;
          read(c); readln(tot);
          run(tot);
          dec(Q);

          for i := data[now , c].len downto 1 do
            write(data[now , c].data[i]);
          if data[now , c].len = 0 then write('0');
          writeln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
