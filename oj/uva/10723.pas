Const
    Limit      = 31;

Type
    Topt       = array[1..Limit , 1..Limit] of longint;
    Tcount     = array[1..Limit , 1..Limit] of extended;
    Tstr       = string[Limit];

Var
    count      : Tcount;
    opt        : Topt;
    s1 , s2    : Tstr;
    T , i      : longint;

procedure dfs(p1 , p2 : longint);
begin
    if (p1 > length(s1)) or (p2 > length(s2))
      then begin
               count[p1 , p2] := 1;
               opt[p1 , p2] := length(s1) + length(s2) - p1 - p2 + 2;
           end
      else if opt[p1 , p2] = -1 then
             if s1[p1] = s2[p2]
               then begin
                        dfs(p1 + 1 , p2 + 1);
                        count[p1 , p2] := count[p1 + 1 , p2 + 1];
                        opt[p1 , p2] := opt[p1 + 1 , p2 + 1] + 1;
                    end
               else begin
                        dfs(p1 , p2 + 1); dfs(p1 + 1 , p2);
                        if opt[p1 , p2 + 1] = opt[p1 + 1 , p2]
                          then begin
                                   opt[p1 , p2] := opt[p1 + 1 , p2] + 1;
                                   count[p1 , p2] := count[p1 , p2 + 1] + count[p1 + 1 , p2];
                               end
                          else if opt[p1 , p2 + 1] < opt[p1 + 1 , p2]
                                 then begin
                                          opt[p1 , p2] := opt[p1 , p2 + 1] + 1;
                                          count[p1 , p2] := count[p1 , p2 + 1];
                                      end
                                 else begin
                                          opt[p1 , p2] := opt[p1 + 1 , p2] + 1;
                                          count[p1 , p2] := count[p1 + 1 , p2];
                                      end;
                    end;
end;

Begin
    readln(T);
    for i := 1 to T do
      begin
          fillchar(opt , sizeof(opt) , $FF);
          readln(s1); readln(s2);
          while (length(s1) > 0) and (s1[length(s1)] = ' ') do delete(s1 , length(s1) , 1);
          while (length(s2) > 0) and (s2[length(s2)] = ' ') do delete(s2 , length(s2) , 1);
          dfs(1 , 1);
          writeln('Case #' , i , ': ' , opt[1 , 1] , ' ' , count[1 , 1] : 0 : 0);
      end;
End.