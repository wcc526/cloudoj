#include <cstdio>
#include <cstring>
#include <algorithm>

#define MAXN 500005
#define MAXE 30

using namespace std;

const long long INF = (1LL<<61);
long long n,m;
long long s[MAXN];
long long p[MAXN][MAXE],q[MAXN][MAXE];

long long findmax(long long x,long long y){
    long long i,j,k,ret;
    i = x;
    j = y;
    for (k=0;(1<<k)<=j-i+1;k++); k-=(k>0);
    if (s[p[i][k]]>=s[p[j-(1<<k)+1][k]]) ret = p[i][k];
        else ret = p[j-(1<<k)+1][k];
    return ret;
}

long long findmin(long long x,long long y){
    //printf("%d %d\n",x,y);
    long long i,j,k,ret;
    i = x;
    j = y;
    for (k=0;(1<<k)<=j-i+1;k++); k-=(k>0);
    if (s[q[i][k]]<=s[q[j-(1<<k)+1][k]]) ret = q[i][k];
        else ret = q[j-(1<<k)+1][k];
    return ret;
}

int main(){
   // freopen("b.in","r",stdin);
    long long tt,i,j,k,m1,m2,o;
    long long tmp,ans,tans;
    tt = 0;
    while (scanf("%lld%lld",&n,&m)!=EOF){
        printf("Case %lld:\n",++tt);
        p[0][0] = q[0][0] = 0LL;
        s[0] = 0LL;
        for (i=1;i<=n;i++){
            scanf("%lld",&tmp);
            s[i] = s[i-1] + tmp;
            p[i][0] = q[i][0] = i;
        }
        //for (i=0;i<=n;i++) printf("s%d = %lld\n",i,s[i]);
        for (i=1;(1<<i)<=n+1;i++)
            for (j=0;j+(1<<i)-1<=n;j++){
                if (s[p[j][i-1]]>=s[p[j+(1<<(i-1))][i-1]]) p[j][i] = p[j][i-1];
                    else p[j][i] = p[j+(1<<(i-1))][i-1];
                if (s[q[j][i-1]]<=s[q[j+(1<<(i-1))][i-1]]) q[j][i] = q[j][i-1];
                    else q[j][i] = q[j+(1<<(i-1))][i-1];
            }
        while (m--){
            scanf("%lld%lld",&i,&j);
            ans = -INF;
            k = findmax(i,j);
            while (k<j){
                o = findmin(i-1,k-1);
                tans = s[k] - s[o];
                if (tans>ans){
                    ans = tans;
                    m1 = o + 1;
                    m2 = k;
                }
                k = findmax(k+1,j);
            }
            o = findmin(i-1,k-1);
            tans = s[k] - s[o];
            if (tans>ans){
                ans = tans;
                m1 = o + 1;
                m2 = k;
            }
            printf("%lld %lld\n",m1,m2);
        }
    }
    return 0;
}
