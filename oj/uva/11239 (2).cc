#include <algorithm>
#include <ctype.h>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

using namespace std;

map<string, int> projects;

struct ecmp : public binary_function<string, string, bool>
{
  bool operator()(string left, string right)
  {
    return projects[left] > projects[right];
  }
};

int main()
{
  map<string, string> students;
  set<string> blacklist;
  vector<string> sorted;
  string p, s;

  for (;;) {
    projects.clear();
    students.clear();
    blacklist.clear();
    sorted.clear();
    for (;;) {
      getline(cin, s);
      if (isdigit(s[0]))
        break;
      if (isupper(s[0])) {
        p = s;
        projects[p] = 0;
      } else {
        if (blacklist.find(s) != blacklist.end())
          continue;
        if (students.find(s) != students.end()) {
          if (students[s] != p)
            --projects[students[s]], blacklist.insert(s);
        } else {
          students[s] = p;
          ++projects[p];
        }
      }
    }
    if (s[0] == '0')
      break;
    for (map<string, int>::iterator i = projects.begin(); i != projects.end(); ++i)
      sorted.push_back(i->first);
    stable_sort(sorted.begin(), sorted.end(), ecmp());
    for (vector<string>::iterator i = sorted.begin(); i != sorted.end(); ++i)
      cout << *i << " " << projects[*i] << endl;
  }

  return 0;
}
