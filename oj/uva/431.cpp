/**
 * UVa 431 Trial of the Millennium
 * Author: chchwy
 * Last Modified: 2011.08.04
 * Tag: Dynamic Programming, 0/1 Knapsack
 */
#include<iostream>
#include<sstream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<cstdio>
using namespace std;

enum {MAX_HOUR=240+2, MAX_EVIDENCE=100+1};

struct Evidence
{
    int score;
    int time_cost;
    char description[128];

    Evidence( string& );
};

Evidence::Evidence(string & line)
{
    stringstream sin(line);
    sin>> score >> time_cost;
    sin.getline(description, sizeof(description));
}

int  c[MAX_HOUR];
bool p[MAX_EVIDENCE][MAX_HOUR];

void solve_knapsack( int allowed_time, vector<Evidence>& ev )
{
    memset(c, 0, sizeof(c));
    memset(p, 0, sizeof(p));

    for(int i=ev.size()-1; i>=0 ; --i) {
        for(int j=allowed_time; j>=ev[i].time_cost; --j) {

            if( c[j-ev[i].time_cost]+ev[i].score > c[j]) {

                p[i][j] = true;
                c[j] = c[j-ev[i].time_cost] + ev[i].score;
            }
        }
    }
}

bool cmp_time( const Evidence& a, const Evidence& b)
{
    return (a.time_cost < b.time_cost);
}

void pretty_print( vector<Evidence>& ev )
{
    //sort( ev.begin(), ev.end(), cmp_time );

    int total_score = 0;
    for(int i=0; i<ev.size(); ++i)
        total_score  = total_score + ev[i].score;

    int total_time = 0;
    for(int i=0; i<ev.size(); ++i)
        total_time = total_time + ev[i].time_cost;

    printf("Score  Time  Description\n");
    printf("-----  ----  -----------\n");

    for(int i=0; i<ev.size(); ++i)
        printf("%3d    %3d  %s\n", ev[i].score, ev[i].time_cost, ev[i].description);

    printf("\n");
    printf("Total score: %d points\n", total_score);
    printf(" Total time: %d hours\n", total_time);
}

void backtrace_result(int allowed_time, vector<Evidence>& ev )
{
    vector<Evidence> selected_ev;

    int j = allowed_time;
    for(int i=0; i<ev.size(); ++i) {
        if( p[i][j] ) {
            selected_ev.push_back(ev[i]);
            j = j - ev[i].time_cost;
        }
    }
    pretty_print( selected_ev );
}

bool is_enough_time( int allowed_time, vector<Evidence>& ev )
{
    for(int i=0; i<ev.size(); ++i)
        if( ev[i].time_cost <= allowed_time ) //holy sxxt bug at "<" to  "<="
            return true;
    return false;
}

int main()
{
#ifndef ONLINE_JUDGE
    freopen("431.in","r",stdin);
    freopen("431.out","w",stdout);
#endif

    int num_case;
    cin>>num_case;

    while(num_case--) {

        int allowed_time;
        cin>>allowed_time;
        cin.ignore(10, '\n');

        vector<Evidence> ev;

        string line;
        while(getline(cin, line)) {
            if( line.size()==0 )
                break;
            ev.push_back(Evidence(line));
        }

        if( is_enough_time(allowed_time, ev) ) {

            solve_knapsack(allowed_time, ev);
            backtrace_result(allowed_time, ev);

        } else {
            puts("There is not enough time to present any evidence. Drop the charges.");
        }
        if( num_case!=0 ) putchar('\n');
    }
    return 0;
}
