Const
    InFile     = 'p10373.in';
    Limit      = 200;
    LimitChoose= 20;
    LimitNum   = 999;
    Maximum    = 1000000;

Type
    Topt       = array[0..LimitChoose , 0..LimitChoose * LimitNum] of longint;
    Tdata      = array[1..Limit] of longint;

Var
    opt        : Topt;
    data , cost: Tdata;
    N , cases  : longint;

procedure init;
var
    i          : longint;
begin
    read(N); dec(Cases);
    for i := 1 to N do read(data[i] , cost[i]);
end;

procedure work;
var
    now ,
    i , j , k  : longint;
begin
    fillchar(opt , sizeof(opt) , 1);
    opt[0 , 0] := 0; now := 0;
    for i := 1 to N do
      begin
          for j := LimitChoose - 1 downto 0 do
            if j < i then
              for k := 0 to 999 * j do
                if opt[j + 1 , k + data[i]] > opt[j , k] + cost[i] then
                  opt[j + 1 , k + data[i]] := opt[j , k] + cost[i];
      end;
end;

procedure out;
var
    C , i ,
    min , max , Choose ,
    best , j   : longint;
begin
    read(C);
    for i := 1 to C do
      begin
          read(Choose , min , max);
          best := maximum;
          for j := min * choose to max * choose do
            if best > opt[choose , j] then
              best := opt[choose , j];
          if best >= maximum
            then writeln('impossible')
            else writeln(best);
      end;
    if cases > 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.