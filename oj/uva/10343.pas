Const
    InFile     = 'p10343.in';
    Valid      = ['a'..'z' , 'A'..'Z' , '0'..'9' , '+' , '/' , '=' , '#'];

procedure Get_Num(var num : longint);
var
    ch         : char;
begin
    repeat
      read(ch);
    until ch in Valid;
    case ch of
      'A'..'Z' : num := ord(ch) - ord('A');
      'a'..'z' : num := ord(ch) - ord('a') + 26;
      '0'..'9' : num := ord(ch) - ord('0') + 52;
      '+'      : num := 62;
      '/'      : num := 63;
      '='      : num := -1;
      '#'      : num := -2;
    end;
end;

function decode_sub : boolean;
var
    data       : array[1..24] of longint;
    i , j ,
    num        : longint;
begin
    for i := 1 to 4 do
      begin
          get_num(num);
          if num = -2 then exit(false);
          for j := 0 to 5 do
            if num = -1
              then data[(i - 1) * 6 + j + 1] := -1
              else if num and (1 shl (5 - j)) = 0
                     then data[(i - 1) * 6 + j + 1] := 0
                     else data[(i - 1) * 6 + j + 1] := 1;
      end;
    decode_sub := true;
    for i := 1 to 3 do
      begin
          num := 0;
          for j := 0 to 7 do
            if data[(i - 1) * 8 + j + 1] = -1
              then exit
              else num := num * 2 + data[(i - 1) * 8 + j + 1];
          write(chr(num));
      end;
end;

function decode : boolean;
var
    count      : longint;
begin
    count := 0;
    while decode_sub do
      inc(count);
    decode := count <> 0;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while decode do
        write('#');
//    Close(INPUT);
End.