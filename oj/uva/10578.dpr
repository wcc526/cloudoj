{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10578.in';
    OutFile    = 'p10578.out';
    LimitSave  = 15625;
    Limit      = 24;

Type
    Topt       = array[0..LimitSave] of longint;
    Tline      = array[1..6] of longint;
    Tpath      = record
                     tot      : longint;
                     data     : array[1..Limit] of longint;
                 end;

Var
    opt        : Topt;
    line       : Tline;
    path       : Tpath;

procedure code(line : Tline; var num : longint);
var
    i          : longint;
begin
    num := 0;
    for i := 1 to 6 do
      num := num * 5 + line[i];
end;

procedure decode(num : longint; var line : TLine);
var
    i          : longint;
begin
    for i := 6 downto 1 do
      begin
          line[i] := num mod 5;
          num := num div 5;
      end;
end;

procedure init;
var
    ch         : char;
begin
    fillchar(line , sizeof(line) , 0);
    fillchar(path , sizeof(path) , 0);
    while not eoln do
      begin
          read(ch);
          inc(path.tot); path.data[path.tot] := ord(ch) - ord('0');
          inc(line[path.data[path.tot]]);
          write(ch);
      end;
    readln;
end;

procedure pre_process;
var
    tmp , 
    i , j , sum: longint;
begin
    fillchar(opt , sizeof(opt) , $FF);
    for i := LimitSave - 1 downto 0 do
      begin
          decode(i , line); sum := 0;
          for j := 1 to 6 do
            inc(sum , line[j] * j);
          if sum > 31 then continue;
          opt[i] := 0;
          for j := 1 to 6 do
            if line[j] < 4 then
              begin
                  inc(line[j]); code(line , tmp);
                  if opt[tmp] = 0 then opt[i] := 1;
                  dec(line[j]);
              end;
      end;
end;

procedure workout;
var
    tmp        : longint;
begin
    write(' ');
    code(line , tmp);
    if (opt[tmp] = 1) xor odd(path.tot)
      then write('A')
      else write('B');
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
