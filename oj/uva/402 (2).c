#include <stdio.h>

int main(void)
{
  int c, i, j, k, n, q[50], s = 0, x;

  while (scanf("%d %d", &n, &x) == 2) {
    printf("Selection #%d\n", ++s);
    for (i = n; i--;)
      q[i] = i + 1;
    for (i = 20; x < n && i--;) {
      scanf("%d", &c);
      for (j = --c; j < n && x < n; j += c, --n)
        for (k = j; ++k < n;)
          q[k - 1] = q[k];
    }
    while (i-- > 0)
      scanf("%*d");
    for (j = 0; j < n; ++j)
      printf("%d%c", q[j], n - j ^ 1 ? ' ' : '\n');
    putchar('\n');
  }

  return 0;
}
