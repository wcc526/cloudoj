Const
    Limit      = 100;
    LimitLen   = 80;

Type
    lint       = object
                     Len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    data       : Tdata;
    answer     : lint;
    N , K ,
    i , j      : longint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    Len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

Begin
    while not eof do
      begin
          readln(N , K);
          fillchar(data , sizeof(data) , 0);
          data[0].init; data[0].data[1] := 1;
          for i := 1 to K - 1 do
            data[i].add(data[i - 1] , data[i - 1]);
          for i := K to N do
            begin
                data[i].init;
                for j := i - K to i - 1 do
                  data[i].add(data[i] , data[j]);
            end;
          answer := data[0];
          for i := K + 1 to N do
            begin
                answer.add(answer , answer);
                answer.add(answer , data[i - K - 1]);
            end;
          answer.print;
          writeln;
      end;
End.