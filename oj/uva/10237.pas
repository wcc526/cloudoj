Const
    InFile     = 'p10237.in';
    OutFile    = 'p10237.out';
    Limit      = 32;

Type
    Tdata      = object
                     tot      : longint;
                     data     : array[1..Limit] of longint;
                     count    : array[1..Limit , 0..Limit , 0..Limit] of extended;
                     visited  : array[1..Limit , 0..Limit , 0..Limit] of boolean;
                     procedure ins(num : longint);
                     function dfs(step , num , delta : longint) : extended;
                 end;

Var
    data       : array[0..1] of Tdata;
    ans        : array[1..Limit , 0..Limit * Limit] of extended;
    answer     : extended;
    N , K      : longint;

procedure Tdata.ins(num : longint);
var
    i          : longint;
begin
    inc(tot); i := tot;
    while (i > 1) and (data[i - 1] > num) do
      begin data[i] := data[i - 1]; dec(i); end;
    data[i] := num;
end;

function Tdata.dfs(step , num , delta : longint) : extended;
var
    res        : extended;
begin
    if num = 0 then exit(1);
    if (num > tot - step + 1) or (num > data[tot] - delta) then exit(0);
    if visited[step , num , delta] then exit(count[step , num , delta]);
    visited[step , num , delta] := true;
    res := dfs(step + 1 , num , delta) + dfs(step + 1 , num - 1 , delta + 1) * (data[step] - delta);
    count[step , num , delta] := res;
    exit(res);
end;

procedure work;
var
    i , tot    : longint;
begin
    fillchar(data , sizeof(data) , 0);
    tot := N * 2 - 1;
    for i := 1 to N - 1 do
      begin
          data[i mod 2].ins(i);
          data[(tot - i + 1) mod 2].ins(i);
      end;
    data[N mod 2].ins(N);
    for K := 0 to N * N do
      begin
          answer := 0;
          for i := 0 to K do
            answer := answer + data[0].dfs(1 , i , 0) * data[1].dfs(1 , K - i , 0);
          ans[N , K] := answer;
      end;
end;

procedure print_sub(num : extended);
begin
    if num > 0
      then begin
               print_sub(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure print(num : extended);
begin
    if num = 0
      then write(0)
      else print_sub(num);
end;

Begin
    for N := 1 to 30 do
      work;
    while true do
      begin
          readln(N , K);
          if N + K = 0 then break;
          print(ans[N , K]);
          writeln;
      end;
End.