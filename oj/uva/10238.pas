Const
    Limit      = 50;
    LimitSave  = 2500;
    Base       = 100000000;
    SegLen     = 8;
    LimitLen   = 11;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[1..Limit , 0..LimitSave] of lint;
    Tvisited   = array[1..Limit , 0..LimitSave] of boolean;
    Tpower     = array[1..Limit , 0..Limit] of lint;

Var
    data       : Tdata;
    power      : Tpower;
    visited    : Tvisited;
    F , N , S  : longint;

procedure init;
begin
    readln(F , N , S);
    fillchar(visited , sizeof(visited) , 0);
end;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div Base;
          data[i] := tmp mod Base;
          inc(i);
      end;
    Len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
    s          : string[20];
begin
    write(data[Len]);
    for i := Len - 1 downto 1 do
      begin
          str(data[i] , s);
          while length(s) < SegLen do s := '0' + s;
          write(s);
      end;
end;

function dfs(N , S : longint) : lint;
var
    i          : longint;
    res        : lint;
begin
    res.init;
    if (N * F < S) or (S < N) then exit(res);
    if N <= 1 then begin res.data[1] := 1; exit(res); end;
    if visited[N , S] then exit(data[N , S]);

    visited[N , S] := true;
    for i := 1 to F do
      res.add(res , dfs(N - 1 , S - i));
    data[N , S] := res;
    exit(res);
end;

procedure pre_process;
var
    i , j , k  : longint;
begin
    for i := 1 to Limit do
      begin
          power[i , 0].init; power[i , 0].data[1] := 1;
          for j := 1 to Limit do
            begin
                power[i , j].init;
                for k := 1 to i do
                  power[i , j].add(power[i , j] , power[i , j - 1]);
            end;
      end;
end;

Begin
    pre_process;
    while not eof do
      begin
          init;
          dfs(N , S).print;
          write('/');
          power[F , N].print;
          writeln;
      end;
End.