{$I-}
Type
    Tpoint     = record
                     x , y    : extended;
                 end;

Var
    N , i      : longint;
    p , np     : Tpoint;
    T , alpha ,
    r1 , t1    : extended;
Begin
    while not eof do
      begin
          read(N , T);
          p.x := 0; p.y := 0;
          for i := 1 to N do
            begin
                read(r1 , t1);
                alpha := T / t1 * pi * 2;
                np.x := cos(alpha) * r1; np.y := sin(alpha) * r1;
                p.x := p.x + np.x; p.y := p.y + np.y;
                write(sqrt(sqr(p.x) + sqr(p.y)) : 0 : 4 , ' ');
            end;
          if N <> 0 then
            writeln;
      end;
End.