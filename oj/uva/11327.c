#include <stdio.h>

#define N 200001

long long p[N];
int t[N];

int main()
{
  int i, j, m, n, o;
  long long k;

  for (i = N; i--; t[i] = 1);
  for (i = 2; i * i < N; ++i)
    if (t[i] == 1)
      for (j = i * i; j < N; j += i)
        t[j] = i;
  for (p[1] = i = 2; i < N; ++i)
    if (t[i] == 1)
      p[i] = i - 1;
    else if (i / t[i] % t[i])
      p[i] = p[i / t[i]] * (t[i] - 1);
    else
      p[i] = p[i / t[i]] * t[i];
  for (i = 1; ++i < N; p[i] += p[i - 1]);

  while (scanf("%lld", &k) == 1 && k) {
    if (k > 2) {
      for (i = 1; p[i] < k; ++i);
      for (k -= p[i - 1], j = 1; k; ++j, m == 1 && --k)
        for (m = i, n = j; n; o = m % n, m = n, n = o);
    } else if (k == 2) {
      i = 1, j = 2;
    } else {
      i = j = 1;
    }
    printf("%d/%d\n", j - 1, i);
  }

  return 0;
}
