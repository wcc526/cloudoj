#include <stdio.h>

int main()
{
  char s[101];
  int g, i, j, n;

  while (scanf("%d", &g) == 1 && g) {
    scanf("%*[^A-Za-z0-9]s");
    scanf("%s%n", s, &n);
    n /= g;
    for (i = 0; i < g; ++i)
      for (j = n; j--;)
        putchar(s[i * n + j]);
    putchar('\n');
  }

  return 0;
}
