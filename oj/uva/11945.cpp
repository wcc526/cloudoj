#include<stdio.h>
int main()
{
	int n,i,cas=0;
	scanf("%d",&n);
	while(n--)
	{
		double a[12],sum=0,num;
		int ave,b[100],j;
		for(i=0;i<12;i++)
		{
			scanf("%lf",&a[i]);
			sum+=a[i];
		}
		sum/=12;
		num=sum-int(sum);
		ave=int(sum);
		j=0;
		while(ave)
		{
			b[j++]=ave%1000;
			ave/=1000;
		}
		printf("%d $",++cas);
		for(i=j-1;i>0;i--)
			printf("%d,",b[i]);
		printf("%.2lf\n",num+b[0]);
	}
	return 0;
}
