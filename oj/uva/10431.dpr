{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10431.in';
    OutFile    = 'p10431.out';
    Limit      = 100000;
    pp         = 0.2316419;
    b1         = 0.319381530;
    b2         = -0.356563782;
    b3         = 1.781477937;
    b4         = -1.821255978;
    b5         = 1.330274429;
    minimum    = 1e-6;


Var
    N , Cases , Q , 
    i          : longint;
    t , t2 ,
    t3 , t4 , t5 , 
    p , z , fz , 
    Pz , Qz ,  
    Mean , SD  : double;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while not eof do
        begin
            inc(Cases);
            readln(N);
            Mean := 0; SD := 0;
            for i := 1 to N do
              begin
                  read(p);
                  Mean := Mean + p;
                  SD := SD + p * p;
              end;
            Mean := Mean / N;
            if N = 1
              then SD := 0
              else SD := sqrt(abs(SD - Mean * Mean * N) / (N - 1));
            writeln('Data Set #' , Cases);
            writeln('Mean = ' , Mean : 0 : 4);
            writeln('Standard Deviation = ' , SD : 0 : 4);
            readln(Q);
            for i := 1 to Q do
              begin
                  read(p);
                  if abs(SD) > minimum
                    then begin
                             z := (p - Mean) / SD;
                             fz := 1 / sqrt(2 * pi) * exp(-z * z / 2);
                             t := 1 / (1 + pp * abs(z));
                             t2 := t * t;
                             t3 := t2 * t;
                             t4 := t3 * t;
                             t5 := t4 * t;
                             Qz := fz * (b1 * t + b2 * t2 + b3 * t3 + b4 * t4 + b5 * t5);
                         end
                    else begin
                             if p <= Mean
                               then Qz := 1
                               else Qz := 0;
                         end;
                  Pz := 1 - Qz;
                  writeln('P(z) = ' , Pz : 0 : 4 , ', Q(z) = ' , Qz : 0 : 4 , ', T = ' , Qz * N : 0 : 4);
              end;
            readln;
            if not Eof then writeln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
