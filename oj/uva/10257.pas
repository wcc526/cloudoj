{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p1110.in';
    OutFile    = 'p1110.out';

Var
    S , P , K ,
    J ,
    x , y , z  : integer;

function init : boolean;
begin
    if eof then
      init := false
    else
      begin
          init := true;
          readln(S , P , K , J);
      end;
end;

function check(p1 , p2 , p3 , p4 : integer) : boolean;
begin
    if p1 + p2 <> p3 then
      check := false
    else
      if (p4 - p1 + p2) mod 3 <> 0 then
        check := false
      else
        begin
            y := (p4 - p1 + p2) div 3;
            x := p1 + y;
            z := y - p2;
            if (x < 0) or (y < 0) or (z < 0) then
              check := false
            else
              check := true;
        end;
end;

procedure work;
var
    a , b , c  : integer;
begin
    for a := -1 to 0 do
      for b := -1 to 0 do
        for c := -1 to 0 do
          if (c = -1) or (a <> -1) or (b <> -1) then
            if check(S - a , P - b , K - c , 12 + J) then
              exit;
end;

procedure out;
begin
    writeln(x , ' ' , y , ' ' , z);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
