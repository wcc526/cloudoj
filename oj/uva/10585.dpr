{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10585.in';
    OutFile    = 'p10585.out';
    Limit      = 10000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     visited  : boolean;
                     x , y    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    N , cases  : longint;
    answer     : boolean;

procedure init;
var
    i          : longint;
begin
    dec(cases);
    read(N);
    for i := 1 to N do
      begin
          read(data[i].x , data[i].y);
          data[i].visited := false;
      end;
end;

procedure work;
var
    i , j      : longint;
    px , py    : extended;
    find       : boolean;
begin
    answer := false;
    px := 0; py := 0;
    for i := 1 to N do
      begin px := (px + data[i].x); py := (py + data[i].y); end;
    px := px / N; py := py / N;

    answer := false;
    for i := 1 to N do
      if not data[i].visited then
        begin
            data[i].visited := true;
            if (abs(px - data[i].x) <= minimum) and (abs(py - data[i].y) <= minimum) then
              continue;
            find := false;
            for j := i + 1 to N do
              if not data[j].visited then
                if (abs(px * 2 - data[i].x - data[j].x) <= minimum) and (abs(py * 2 - data[i].y - data[j].y) <= minimum) then
                  begin
                      data[j].visited := true;
                      find := true;
                      break;
                  end;
            if not find then exit;
        end;
    answer := true;
end;

procedure out;
begin
    if answer
      then writeln('yes')
      else writeln('no');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
