#include <stdio.h>
#include <string.h>

int main()
{
  unsigned short v[50001];
  int a, b, c = 0, i, n, m, r;

  while (scanf("%d %d", &n, &m) == 2 && (n || m)) {
    r = n;
    for (i = n; i--;)
      v[i] = i;
    while (m--) {
      scanf("%d %d", &a, &b);
      while (a != v[a]) a = v[a];
      while (b != v[b]) b = v[b];
      if (a != b)
        --r, v[b] = a;
    }
    printf("Case %d: %d\n", ++c, r);
  }

  return 0;
}
