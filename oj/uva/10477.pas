Const
    InFile     = 'p10477.in';
    OutFile    = 'p10477.out';
    Limit      = 21;
    dirx       : array[1..3 , 1..8] of longint
               = ((-1 , -2 , -2 , -1 , 1 , 2 , 2 , 1) ,
                  (-1 , -3 , -3 , -1 , 1 , 3 , 3 , 1) ,
                  (0 , -1 , 0 , 1 , 0 , -1 , 0 , 1));
    diry       : array[1..3 , 1..8] of longint
               = ((-2 , -1 , 1 , 2 , 2 , 1 , -1 , -2) ,
                  (-3 , -1 , 1 , 3 , 3 , 1 , -1 , -3) ,
                  (-1 , 0 , 1 , 0 , -1 , 0 , 1 , 0));

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tvisited   = array[1..Limit , 1..Limit , 1..3] of longint;
    Tqueue     = array[1..Limit * Limit * 3] of
                   record
                       p      : Tpoint;
                       time   : longint;
                   end;

Var
     visited   : Tvisited;
     queue     : Tqueue;
     start , stop
               : Tpoint;
     cases ,
     p1 , p2 , answer ,
     N , S     : longint;

function bfs : longint;
var
    i , time , ntime ,
    open , closed
               : longint;
    p , np     : Tpoint;
begin
    fillchar(visited , sizeof(visited) , $FF);
    open := 1; closed := 0;
    for i := 1 to 3 do
      begin
          inc(closed); queue[closed].p := start; queue[closed].time := i;
          visited[start.x , start.y , i] := 0;
      end;
    while open <= closed do
      begin
          p := queue[open].p; time := queue[open].time; ntime := time mod 3 + 1;
          if (p.x = stop.x) and (p.y = stop.y) then exit(visited[p.x , p.y , time]);
          if (time = 3) and (abs(p.x - stop.x) = 1) and (abs(p.y - stop.y) = 1) then
            if visited[stop.x , stop.y , ntime] = -1 then
              begin
                  visited[stop.x , stop.y , ntime] := visited[p.x , p.y , time] + 1;
                  inc(closed);
                  queue[closed].p := stop; queue[closed].time := ntime;
              end;
          for i := 1 to 8 do
            begin
                np.x := p.x + dirx[time , i]; np.y := p.y + diry[time , i];
                if not ((np.x <= 0) or (np.y <= 0) or (np.x > N) or (np.y > N)) then
                  if visited[np.x , np.y , ntime] = -1 then
                    begin
                        visited[np.x , np.y , ntime] := visited[p.x , p.y , time] + 1;
                        inc(closed);
                        queue[closed].p := np; queue[closed].time := ntime;
                    end;
            end;
          inc(open);
      end;
    exit(-1);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      readln(N , S);
      while N <> 0 do
        begin
            inc(Cases);
            writeln('Set ' , cases , ':');
            while S > 0 do
              begin
                  readln(p1 , p2);
                  start.x := (p1 - 1) div N + 1; start.y := (p1 - 1) mod N + 1;
                  stop.x := (p2 - 1) div N + 1; stop.y := (p2 - 1) mod N + 1;
                  answer := bfs;
                  if answer = -1
                    then writeln('?')
                    else writeln(answer);
                  dec(S);
              end;
            readln(N , S);
        end;
//    Close(INPUT);
End.
