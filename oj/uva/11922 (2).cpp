#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
#define maxn 200000
int N,M;
struct node
{
    node* ch[2];
    int s,v;
    bool flip;
    int cmp(int x)
    {
        int ss;
        if(ch[0]==NULL)
        ss=0;
        else
        ss=ch[0]->s;
        if(x==ss+1)
            return -1;
        else if(x<=ss)
            return 0;
        else
            return 1;
    }
    void pushdown()
    {
        if(flip)
        {
            swap(ch[0],ch[1]);
            if(ch[0]!=NULL)
            ch[0]->flip=!ch[0]->flip;
            if(ch[1]!=NULL)
            ch[1]->flip=!ch[1]->flip;
            flip=0;
        }
    }
    void pushup()
    {
        s=1;
        if(ch[0]!=NULL)
        s+=ch[0]->s;
        if(ch[1]!=NULL)
        s+=ch[1]->s;
    }
};
node* tar=NULL;
void removetree(node* &x)
{
    if(x->ch[0]!=NULL)
        removetree(x->ch[0]);
    if(x->ch[1]!=NULL)
        removetree(x->ch[1]);
    delete x;
    x=NULL;
}
void rotate(node* &o,int d)
{
    if(o!=NULL)
    o->pushdown();
    node* k=o->ch[d^1];
    if(k!=NULL)
    k->pushdown();
    o->ch[d^1]=k->ch[d];
    k->ch[d]=o;
    o->pushup();
    k->pushup();
    o=k;
}
void splay(node* &o,int k)
{
    o->pushdown();
    int d=o->cmp(k);
    int s1=o->ch[0]==NULL?0:o->ch[0]->s;
    if(d==1)
        k-=s1+1;
    if(d!=-1)
    {
        node *p=o->ch[d];
        p->pushdown();
        int d2=p->cmp(k);
        int s2=p->ch[0]==NULL?0:p->ch[0]->s;
        int k2;
        if(d2==0)
            k2=k;
        else
            k2=k-s2-1;
        if(d2!=-1)
        {
            splay(p->ch[d2],k2);
            if(d==d2)
                rotate(o,d^1);
            else
                rotate(o->ch[d],d);
        }
        rotate(o,d^1);
    }
}
node* merge(node* &left,node* &right)
{
    if(left==NULL)
        return right;
    else if(right==NULL)
        return left;
    splay(left,left->s);
    left->ch[1]=right;
    left->pushup();
    return left;
}
void dfs(node* &o)
{
    o->pushdown();
    if(o->ch[0]!=NULL)
        dfs(o->ch[0]);
    printf("%d\n",o->v);
    if(o->ch[1]!=NULL)
        dfs(o->ch[1]);
}
int main()
{
        scanf("%d%d",&N,&M);
        if(tar!=NULL)
            removetree(tar);
        tar=new node();
        tar->s=N;
        tar->v=1;
        tar->ch[0]=NULL;
        tar->flip=0;
        tar->ch[1]=NULL;
        node* qian=tar;
        node* next;
        for(int i=2;i<=N;i++)
        {
            next=new node();
            qian->ch[1]=next;
            next->s=N+1-i;
            next->ch[0]=NULL;
            next->flip=0;
            next->v=i;
            next->ch[1]=NULL;
            qian=next;
        }
        for(int i=0;i<M;i++)
        {
            int a,b;
            scanf("%d%d",&a,&b);
            splay(tar,a);
            node* zuo=tar->ch[0];
            tar->ch[0]=NULL;
            tar->pushup();
            splay(tar,b-a+1);
            node* you=tar->ch[1];
            tar->ch[1]=NULL;
            tar->pushup();
            tar->flip=!tar->flip;
            zuo=merge(zuo,you);
            tar=merge(zuo,tar);
        }
        dfs(tar);
}
