#include <stdio.h>

int main(void)
{
  int c, t = 0;

  while ((c = getc(stdin)) != EOF)
    if (c == '"')
      if ((t ^= 1)) {
        putc('`', stdout);
        putc('`', stdout);
      } else {
        putc('\'', stdout);
        putc('\'', stdout);
      }
    else
      putc(c, stdout);

  return 0;
}
