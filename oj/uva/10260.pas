Var
    ch ,
    now , last : char;
Begin
    while not eof do
      begin
          last := #0;
          while not eoln do
            begin
                read(ch);
                now := #0;
                case ch of
                  'B' , 'F' , 'P' , 'V'      : now := '1';
                  'C' , 'G' , 'J' , 'K' ,
                  'Q' , 'S' , 'X' , 'Z'      : now := '2';
                  'D' , 'T'                  : now := '3';
                  'L'                        : now := '4';
                  'M' , 'N'                  : now := '5';
                  'R'                        : now := '6';
                end;
                if (now <> last) and (now <> #0) then
                  write(now);
                last := now;
            end;
          readln;
          writeln;
      end;
End.