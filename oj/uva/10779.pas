Const
    InFile     = 'p10779.in';
    OutFile    = 'p10779.out';
    LimitN     = 10;
    LimitM     = 25;
    LimitP     = 50;

Type
    Tdata      = array[0..LimitN - 1 , 1..LimitM] of longint;
    Tmap       = array[1..LimitP , 1..LimitP] of longint;
    Tvisited   = array[1..LimitP] of boolean;

Var
    data       : Tdata;
    C          : Tmap;
    visited    : Tvisited;
    cases , totCase ,
    N , M , S ,
    T , tot ,
    answer     : longint;

procedure init;
var
    i , j , sum ,
    p          : longint;
begin
    inc(cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(C , sizeof(C) , 0);
    read(N , M);
    dec(N);
    for i := 0 to N do
      begin
          read(sum);
          for j := 1 to sum do
            begin
                read(p);
                inc(data[i , p]);
            end;
      end;
end;

procedure Build_Graph;
var
    i , j      : longint;
begin
    tot := N + M + 2;
    S := 1; T := tot;
    for i := 1 to N do
      for j := 1 to M do
        if data[i , j] <> 0
          then C[i + 1 , j + N + 1] := data[i , j] - 1
          else C[j + N + 1 , i + 1] := 1;
    for i := 1 to M do
      begin
          C[S , i + N + 1] := data[0 , i];
          C[i + N + 1 , T] := 1;
      end;
end;

function dfs(p : longint) : boolean;
var
    i          : longint;
begin
    visited[p] := true;
    if p = T
      then dfs := true
      else begin
               dfs := false;
               for i := 1 to tot do
                 if not visited[i] and (C[p , i] > 0) and dfs(i) then
                   begin
                       dfs := true;
                       dec(C[p , i]); inc(C[i , p]);
                       exit;
                   end;
           end;
end;

procedure work;
begin
    Build_Graph;

    answer := -1;
    repeat
      fillchar(visited , sizeof(visited) , 0);
      inc(answer);
    until not dfs(S);
end;

procedure out;
begin
    writeln('Case #' , cases , ': ' , answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(totCase); cases := 0;
      while cases < totCase do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.