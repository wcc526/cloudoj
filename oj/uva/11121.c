#include <stdio.h>

void c(int v)
{
  int q;
  if (v) {
    q = -(v >> 1);
    c(q);
    putc('0' + (v & 1), stdout);
  }
}

int s(void)
{
  int r = 0;
  char c, n;
  c = getc(stdin);
  if (c == '-') {
    n = 1;
    c = getc(stdin);
  } else {
    n = 0;
  }
  r = c - '0';
  while ((c = getc(stdin)) >= '0') {
    r *= 10;
    r += c - '0';
  }
  if (n)
    r = -r;
  return r;
}

int main(void)
{
  int n;
  int i = 0;
  int v;

  n = s() + 1;

  while (--n) {
    v = s();
    printf("Case #%d: ", ++i);
    if (v)
      c(v);
    else
      putc('0', stdout);
    putc('\n', stdout);
  }

  return 0;
}
