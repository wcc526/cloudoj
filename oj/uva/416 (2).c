#include <stdio.h>
#include <string.h>

int main()
{
  char s[10] = { 0x7E, 0x30, 0x6D, 0x79, 0x33, 0x5B, 0x5F, 0x70, 0x7F, 0x7B },
    d[11][11], v[8], c;
    int i, j, n;

  while (scanf("%d", &n) == 1 && n) {
    memset(d, 0, sizeof(d));
    memset(d[n], 0xFF, sizeof(d[10]));
    for (i = n; i--;) {
      scanf("%s", v);
      for (c = j = 0; j < 7; ++j)
        c = (c << 1) | (v[j] == 'Y' ? 1 : 0);
      for (j = 10; j-- > i;)
        if (d[i + 1][j + 1] && (c & ~s[j]) == 0 && (c & ~d[i + 1][j + 1]) == 0)
          d[i][j] = ~(c ^ s[j]) & d[i + 1][j + 1];
    }
    for (i = 10; i-- && !d[0][i];);
    puts(i < 0 ? "MISMATCH" : "MATCH");
  }

  return 0;
}
