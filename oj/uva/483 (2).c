#include <stdio.h>

int main()
{
  int c, n = 0;
  char s[1024];

  while ((c = getc(stdin)) > 0) {
    if (isspace(c)) {
      while (n)
        putc(s[--n], stdout);
      putc(c, stdout);
    } else {
      s[n++] = c;
    }
  }
  while (n)
    putc(s[--n], stdout);

  return 0;
}
