Const
    Limit      = 15;

Type
    Tdata      = array[0..1 shl Limit] of longint;

Var
    data ,
    sum        : Tdata;
    N , answer : longint;

procedure init;
var
    i          : longint;
begin
    readln(N);
    for i := 0 to 1 shl N - 1 do
      readln(data[i]);
end;

procedure work;
var
    i , j , tmp: longint;
begin
    fillchar(sum , sizeof(sum) , 0);
    for i := 0 to 1 shl N - 1 do
      for j := 0 to N - 1 do
        inc(sum[i] , data[i xor (1 shl j)]);
    answer := 0;
    for i := 0 to 1 shl N - 1 do
      for j := 0 to N - 1 do
        begin
            tmp := sum[i] + sum[i xor (1 shl j)];
            if tmp > answer then answer := tmp;
        end;
end;

Begin
    while not eof do
      begin
          init;
          work;
          writeln(answer);
      end;
End.