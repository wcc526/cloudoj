{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10603.in';
    OutFile    = 'p10603.out';
    Limit      = 200;
    Maximum    = 1000000000;

Type
    Tpoint     = record
                     a , b , cost            : longint;
                 end;
    Theap      = array[1..(Limit + 1) * (Limit + 1)] of Tpoint;
    Tindex     = array[0..Limit , 0..Limit] of longint;
    Tanswer    = array[0..Limit] of longint;

Var
    heap       : Theap;
    index      : Tindex;
    answer     : Tanswer;
    A , B , C ,
    D , T , tot ,
    ans        : longint;

procedure init;
begin
    dec(T);
    readln(A , B , C , D);
    fillchar(heap , sizeof(heap) , 0);
    fillchar(index , sizeof(index) , $FF);
end;

procedure down(p : longint);
var
    newp       : longint;
    tmp        : Tpoint;
begin
    while p * 2 <= tot do
      begin
          newp := p;
          if heap[p * 2].cost < heap[newp].cost then newp := p * 2;
          if (p * 2 < tot) and (heap[p * 2 + 1].cost < heap[newp].cost) then newp := p * 2 + 1;
          if newp = p
            then break
            else begin
                     tmp := heap[p]; heap[p] := heap[newp]; heap[newp] := tmp;
                     index[heap[p].a , heap[p].b] := p;
                     index[heap[newp].a , heap[newp].b] := newp;
                     p := newp;
                 end;
      end;
end;

procedure del_top;
begin
    index[heap[1].a , heap[1].b] := -1;
    heap[1] := heap[tot];
    index[heap[1].a , heap[1].b] := 1;
    dec(tot);
    down(1);
end;

procedure shift(p : longint);
var
    tmp        : Tpoint;
begin
    while p <> 1 do
      if heap[p div 2].cost <= heap[p].cost
        then break
        else begin
                 tmp := heap[p]; heap[p] := heap[p div 2]; heap[p div 2] := tmp;
                 index[heap[p].a , heap[p].b] := p;
                 index[heap[p div 2].a , heap[p div 2].b] := p div 2;
                 p := p div 2;
             end;
end;

procedure pour(var a0 , b0 : longint; A , B : longint; var addcost : longint);
begin
    if a0 + b0 < B
      then begin
               addcost := a0;
               inc(b0 , a0); a0 := 0;
           end
      else begin
               addcost := B - b0;
               dec(a0 , addcost); b0 := B;
           end;
end;

procedure better(a0 , b0 , cost : longint);
var
    p          : longint;
begin
    p := index[a0 , b0];
    if (p <> -1) and (heap[p].cost > cost)
      then begin
               heap[p].cost := cost;
               shift(p);
           end;
end;

procedure work;
var
    i , j ,
    a0 , b0 , c0 ,
    addcost    : longint;
    key        : Tpoint;
begin
    tot := 0;
    for i := 0 to C do
      answer[i] := Maximum;
    for i := 0 to A do
      for j := 0 to B do
        if i + j <= C then
          begin
              inc(tot);
              heap[tot].a := i; heap[tot].b := j;
              if (i = 0) and (j = 0)
                then heap[tot].cost := 0
                else heap[tot].cost := Maximum;
              index[i , j] := tot;
          end;

    while tot > 0 do
      begin
          key := heap[1]; if key.cost >= Maximum then break;
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          if answer[a0] > key.cost then answer[a0] := key.cost;
          if answer[b0] > key.cost then answer[b0] := key.cost;
          if answer[c0] > key.cost then answer[c0] := key.cost;

          del_top;

          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(a0 , b0 , A , B , addcost); better(a0 , b0 , key.cost + addcost);
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(b0 , a0 , B , A , addcost); better(a0 , b0 , key.cost + addcost);
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(a0 , c0 , A , C , addcost); better(a0 , b0 , key.cost + addcost);
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(c0 , a0 , C , A , addcost); better(a0 , b0 , key.cost + addcost);
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(b0 , c0 , B , C , addcost); better(a0 , b0 , key.cost + addcost);
          a0 := key.a; b0 := key.b; c0 := C - key.a - key.b;
          pour(c0 , b0 , C , B , addcost); better(a0 , b0 , key.cost + addcost);
      end;

    ans := D;
    while answer[ans] >= maximum do dec(ans);
end;

procedure out;
begin
    writeln(answer[ans] , ' ' , ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
