{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10656.in';
    OutFile    = 'p10656.out';

Var
    first      : boolean;
    N , i  , p : longint;      

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N <> 0 do
        begin
            first := true;
            for i := 1 to N do
              begin
                   read(p);
                   if p <> 0 then
                     begin
                         if not first then write(' ');
                         write(p); 
                         first := false;
                     end;
              end;
            if first then write(0);
            writeln;
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
