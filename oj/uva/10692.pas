Const
    InFile     = 'p10692.in';
    OutFile    = 'p10692.out';
    maximum    = 1000000;
    Limit      = 10000;
    LimitN     = 10;

Type
    Tdata      = array[1..LimitN] of longint;
    Tnums      = array[0..Limit] of longint;

Var
    data       : Tdata;
    cases ,
    answer ,
    N , M      : longint;

function readnum(var p : longint) : boolean;
var
    c          : char;
begin
    readnum := false;
    p := 0;
    read(c); while not (c in ['0'..'9']) do
      begin if c = '#' then exit; read(c); end;
    while c in ['0'..'9'] do
      begin
          p := p * 10 + ord(c) - ord('0');
          if eoln then begin readln; break; end;
          read(c);
      end;
    if c = '#' then exit;
    readnum := true;
end;

function init : boolean;
var
    i          : longint;
begin
    inc(Cases);
    if not readnum(M)
      then init := false
      else begin
               init := true;
               readnum(N);
               for i := 1 to N do
                 readnum(data[i]);
           end;
end;

function cancalc(step : longint; var num : longint) : boolean;
var
    tmp , i    : longint;
begin
    if step = N
      then begin
               cancalc := true;
               num := data[step];
           end
      else if data[step] <= 1
             then begin
                      cancalc := true;
                      if data[step] = 1
                        then num := data[step]
                        else if cancalc(step + 1 , tmp)
                               then if tmp = 0
                                      then num := 1
                                      else num := 0
                               else num := 0;
                  end
             else if not cancalc(step + 1 , tmp)
                    then cancalc := false
                    else if ln(maximum) / ln(data[step]) > tmp
                           then begin
                                    cancalc := true;
                                    num := 1;
                                    for i := 1 to tmp do
                                      num := num * data[step];
                                end
                           else cancalc := false;
end;

function dfs(step , base , loop : longint) : longint;
var
    times ,
    path       : Tnums;
    i , p , num: longint;
begin
    fillchar(times , sizeof(times) , 0);
    p := data[step] mod loop; i := 1;
    path[0] := 1; times[1] := 0;
    while times[p] = 0 do
      begin
          path[i] := p;
          times[p] := i;
          inc(i); p := p * data[step] mod loop;
      end;
    if cancalc(step , num)
      then begin
               if num < base
                 then dfs := num
                 else dfs := (num - base) mod loop + base;
               exit;
           end
      else begin
               num := dfs(step + 1 , times[p] , i - times[p]);
               num := path[num];
               dfs := ((num - base) mod loop + loop) mod loop + base;
               exit;
           end;

end;

procedure work;
begin
    answer := dfs(1 , 0 , M);
end;

procedure out;
begin
    writeln('Case #' , cases , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
