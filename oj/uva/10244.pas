Const
    InFile     = 'p10244.in';
    LimitLen   = 5;
    LimitPoint = 15000;

Type
    Tvisited   = array[0..LimitPoint , 1..LimitLen] of boolean;
    Tstr       = string[LimitLen];

Var
    visited    : Tvisited;
    s , outs   : Tstr;
    Base ,
    N          : longint;
    first      : boolean;

procedure init;
var
    ch         : char;
begin
    fillchar(visited , sizeof(visited) , 0);
    s := '';
    repeat read(ch); until ch <> ' ';
    while ch <> ' ' do
      begin s := s + ch; read(ch); end;
    readln(N);
end;

procedure dfs_print(p : longint);
var
    found      : boolean;
    i          : longint;
begin
    found := false;
    for i := 1 to length(s) do
      if not visited[p , i] then
        begin
            visited[p , i] := true;
            dfs_print((p + (i - 1) * Base) div length(s));
            write(s[i]);
            found := true;
        end;
{    if not found and first then
      begin
          first := false;
          outs := '';
          for i := 1 to N - 1 do
            begin
                outs := s[p mod length(s) + 1] + outs;
                p := p div length(s);
            end;
          write(outs);
      end;}
end;

procedure workout;
var
    number     : extended;
    i          : longint;
begin
    number := 1; first := true;
    for i := 1 to N do number := number * length(s);
    number := N - 1 + number;
    writeln(number : 0 : 0);
    if number > 10000 then
      begin
          writeln('TOO LONG TO PRINT.');
          exit;
      end;
    Base := round(number - N + 1) div length(s);
    dfs_print(0);
    for i := 1 to N - 1 do write(s[1]);
    writeln;
end;

Begin
    while not eof do
      begin
          init;
          workout;
      end;
End.