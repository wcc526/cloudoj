Var
    ans1 , ans2 ,
    r , N , K  : extended;
    T          : longint;

procedure init;
begin
    dec(T);
    readln(r , N , K);
end;

function calc(count : extended) : extended;
var
    alpha , beta ,
    step ,
    r0          : extended;
begin
    step := 2 * pi / N;
    alpha := count / 2 * step;
    beta := step / 2;
    r0 := cos(alpha) + sin(alpha) * sin(beta) / cos(beta);
    calc := sin(2 * pi / N) * N / 2 * r0 * r0;
end;

procedure work;
begin
    if K = N / 2
      then ans1 := pi
      else ans1 := calc(N / 2 - K + 1);
    if K = 1
      then ans2 := 0
      else ans2 := calc(N / 2 - K + 2);
end;

procedure out;
begin
    writeln((ans1 - ans2) * r * r : 0 : 4);
end;

Begin
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.
