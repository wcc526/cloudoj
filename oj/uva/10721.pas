Const
    Limit      = 50;

Type
    Tdata      = array[1..Limit , 1..Limit , 1..Limit] of comp;

Var
    data       : Tdata;
    N , K , M  : longint;

function dfs(N , K , M : longint) : comp;
var
    i          : longint;
begin
    if N < 0
      then exit(0)
      else if K <= 1
            then if K = 0
                   then if N = 0 then exit(1) else exit(0)
                   else if (N >= 1) and (N <= M) then exit(1) else exit(0)
            else if N = 0 then exit(0) else
                 if data[N , K , M] >= 0
                   then exit(data[N , K , M])
                   else begin
                            data[N , K , M] := 0;
                            for i := 1 to M do
                              data[N , K , M] := data[N , K , M] + dfs(N - i , K - 1 , M);
                            exit(data[N , K , M]);
                        end;
end;

Begin
    for N := 1 to Limit do
      for K := 1 to Limit do
        for M := 1 to Limit do
          data[N , K , M] := -1;
    while not eof do
      begin
          readln(N , K , M);
          writeln(dfs(N , K , M) : 0 : 0);
      end;
End.