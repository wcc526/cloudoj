Const
    Limit      = 20000000;
    LimitSave  = 1300000;

Type
    Tdata      = array[0..Limit] of boolean;
    Tprimes    = array[1..LimitSave] of longint;

Var
    prime      : Tdata;
    count ,
    primes     : Tprimes;
    P , N , M  : longint;

procedure GetPrimes;
var
    i , j      : longint;
begin
    fillchar(prime , sizeof(prime) , 1);
    P := 0;
    for i := 2 to Limit do
      if prime[i] then
        begin
            for j := 2 to Limit div i do
              prime[j * i] := false;
            inc(P); primes[P] := i;
        end;
end;

function power(num , p : longint) : longint;
var
    tmp , res  : longint;
begin
    res := 1; tmp := num;
    while p <> 0 do
      begin
          if odd(p) then res := res * tmp mod 10;
          tmp := tmp * tmp mod 10;
          p := p div 2;
      end;
    exit(res);
end;

procedure work;
var
    i , tmp ,
    tot        : longint;
    sum        : array[0..9] of longint;
begin
    fillchar(count , sizeof(count) , 0);
    tot := P;
    for i := 1 to P do
      if primes[i] > N then
        begin
            tot := i - 1; break;
        end;
    for i := 1 to tot do
      begin
          tmp := N;
          while tmp >= primes[i] do
            begin
                tmp := tmp div primes[i]; inc(count[i] , tmp);
            end;
          tmp := N - M;
          while tmp >= primes[i] do
            begin
                tmp := tmp div primes[i]; dec(count[i] , tmp);
            end;
      end;
    if count[1] < count[3]
      then begin dec(count[3] , count[1]); count[1] := 0; end
      else begin dec(count[1] , count[3]); count[3] := 0; end;
    fillchar(sum , sizeof(sum) , 0);
    for i := 1 to tot do
      if count[i] <> 0 then
        inc(sum[primes[i] mod 10] , count[i]);
    writeln(power(2 , sum[2]) * power(3 , sum[3]) * power(5 , sum[5]) * power(7 , sum[7]) * power(9 , sum[9]) mod 10);
end;

Begin
    GetPrimes;
    while not eof do
      begin
          readln(N , M);
          work;
      end;
End.