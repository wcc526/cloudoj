#include <stdio.h>

int main()
{
  char a[5], b;
  short g[26][26], x[26];
  int f = 1, i, j, k, n;

  while(scanf("%d\n", &n) == 1 && n) {
    if (f == 1) f = 0;
    else putchar('\n');
    for (i = 26; i--; x[i] = 0)
      for (j = 26; j--; g[i][j] = 0);
    for (i = n; i--;)
      for (scanf("%c %c %c %c %c %c\n", a, a + 1, a + 2, a + 3, a + 4, &b),
             j = 5; j--;)
        x[a[j] - 'A'] = g[b - 'A'][a[j] - 'A'] = 1;
    for (k = 26; k--;)
      for (i = 26; i--;)
        for (j = 26; j--;)
          g[i][j] |= g[i][k] & g[k][j];
    for (i = 0; i < 26; ++i) {
      if (x[i]) {
        putchar(i + 'A');
        for (j = i; ++j < 26;)
          if (g[i][j] && g[j][i])
            putchar(' '), putchar(j + 'A'), x[j] = 0;
        putchar('\n');
      }
    }
  }

  return 0;
}
