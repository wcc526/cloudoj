/*
 * uva_11462.cpp
 *
 *  Created on: 2012-12-11
 *      Author: Administrator
 */
#include <cstdio>
#include <cstring>
#include <cctype>
inline int nextInt()
{
	int ans=0;
	while(1)
	{
		char c=getchar();
		if(!isdigit(c)) break;
		ans=ans*10+c-'0';
	}
	return ans;
}
int buf[10];
inline void printInt(int i)
{
	int p=0;
	if(i==0) p++;
	else while(i)
	{
		buf[p++]=i%10;
		i/=10;
	}
	for(int j=p-1;j>=0;--j) putchar('0'+buf[j]);
}
int main()
{
	int n,x,c[101];
	//freopen("in.txt","r",stdin);
	while(n=nextInt())
	{
		memset(c,0,sizeof(c));
		for(int i=0;i<n;++i)
		{
			//scanf("%d",&x);
			x=nextInt();
			c[x]++;
		}
		int first=1;
		for(int i=1;i<=100;++i)
			for(int j=0;j<c[i];++j)
			{
				if(!first) //printf(" ");
					putchar(' ');
				first=0;
				//printf("%d",i);
				printInt(i);
			}
		//puts("");
		putchar('\n');
	}
	return 0;
}






