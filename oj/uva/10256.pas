Const
    InFile     = 'p10256.in';
    Limit      = 510;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    A , B      : Tdata;
    N , M      : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(N , M);
    if (N = 0) or (M = 0) then exit(false);
    init := true;
    for i := 1 to N do
      readln(A[i].x , A[i].y);
    for i := 1 to M do
      readln(B[i].x , B[i].y);
end;

function cross(p , p1 , p2 : Tpoint) : longint;
begin
    cross := (p1.x - p.x) * (p2.y - p.y) - (p1.y - p.y) * (p2.x - p.x);
end;

function check(p1 , p2 , p3 : Tpoint) : boolean;
var
    code       : longint;
begin
    code := cross(p1 , p2 , p3);
    if code < 0 then exit(true);
    if code > 0 then exit(false);
    dec(p2.x , p1.x); dec(p2.y , p1.y);
    dec(p3.x , p1.x); dec(p3.y , p1.y);
    if p3.x * p2.x + p3.y * p2.y < sqr(p2.x) + sqr(p2.y)
      then exit(true)
      else exit(false);
end;

procedure Convex(A : Tdata; N : longint; var nA : Tdata; var nN : longint);
var
    i , min , j: longint;
    tmp        : Tpoint;
begin
    min := 1;
    for i := 2 to N do
      if (A[i].y < A[min].y) or (A[i].y = A[min].y) and (A[i].x < A[min].x) then
        min := i;
    tmp := A[min]; A[min] := A[1]; A[1] := tmp;
    for i := 2 to N do
      for j := i + 1 to N do
        if cross(A[1] , A[i] , A[j]) < 0 then
          begin
              tmp := A[i]; A[i] := A[j]; A[j] := tmp;
          end;
    fillchar(nA , sizeof(nA) , 0);
    nN := 1; nA[1] := A[1];
    for i := 2 to N do
      begin
          while (nN > 1) and check(nA[nN - 1] , nA[nN] , A[i]) do dec(nN);
          inc(nN); nA[nN] := A[i];
      end;
end;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function online(p1 , p2 , p : Tpoint) : boolean;
begin
    exit(zero(dist(p1 , p2) - dist(p1 , p) - dist(p , p2)));
end;

function inpoly(const A : Tdata; N : longint; p : Tpoint) : boolean;
var
    tarea ,
    i , area   : longint;
begin
    area := 0;
    for i := 2 to N - 1 do inc(area , abs(cross(A[1] , A[i] , A[i + 1])));
    tarea := 0;
    for i := 1 to N do
      inc(tarea , abs(cross(p , A[i] , A[i mod N + 1])));
    if area > 0 then exit(area = tarea);
    for i := 1 to N do
      if online(A[i] , A[i mod N + 1] , p) then
        exit(true);
    exit(false);
end;

function chk_cross(p1 , p2 , p3 , p4 : Tpoint) : boolean;
begin
    if extended(cross(p1 , p2 , p3)) * extended(cross(p1 , p2 , p4)) < 0 then
      if extended(cross(p3 , p4 , p1)) * extended(cross(p3 , p4 , p2)) < 0 then
        exit(true);
    exit(false);
end;

procedure work;
var
    i , j      : longint;
begin
    Convex(A , N , A , N);
    Convex(B , M , B , M);
    for i := 1 to N do
      if inPoly(B , M , A[i]) then
        begin writeln('No'); exit; end;
    for i := 1 to M do
      if inPoly(A , N , B[i]) then
        begin writeln('No'); exit; end;
    for i := 1 to N do
      for j := 1 to M do
        if chk_cross(A[i] , A[i mod N + 1] , B[j] , B[j mod M + 1]) then
          begin writeln('No'); exit; end;
    writeln('Yes');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        work;
//    Close(INPUT);
End.
