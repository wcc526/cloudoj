#include <stdio.h>
int main()
{
  int a, b, c, d, e, f, g, k, n, t;
  while (scanf("%d %d", &n, &k) > 1) {
    for (a = t = 0, b = c = 1, d = n; ++t <= k;)
      g = (n + b) / d, e = g * c - a, f = g * d - b, a = c, b = d, c = e, d = f;
    printf("%d/%d\n", a, b);
  }
  return 0;
}
