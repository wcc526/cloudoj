#include <cstdio>
#include <cstring>
#include <vector>
#include <functional>
using namespace std;
int Fact(int n) {
	int R=0;
	for(int i=2;n>1;)
	{
		if(n%i==0)
		{
			int C=0;
			while(n%i==0) ++C,n/=i;
			R+=C;
		}
		++i;
		if(i>n/i) i=n;
	}
	return R;
}
int main() {
	int T;
//	freopen("in.txt","r",stdin);
	scanf("%d",&T);
	for(int kase=1;kase<=T;++kase)
	{
		int flag=0;
		int N,M;
		int i,j,k;
		scanf("%d%d",&N,&M);
		for(i=0;i<N;++i)
		{
			int sum=0;
			for(j=0;j<M;++j)
			{
				int tmp;
				scanf("%d",&tmp);
				sum+=Fact(tmp);
			}
			flag^=sum;
		}
		if(flag)
			printf("Case #%d: YES\n",kase);
		else
			printf("Case #%d: NO\n",kase);
	}
	return 0;
}
