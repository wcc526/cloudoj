{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10502.in';
    OutFile    = 'p10502.out';
    Limit      = 100;

Type
    Tdata      = array[0..Limit , 0..Limit] of longint;

Var
    data       : Tdata;
    N , M , ans: longint;

function init : boolean;
var
    i , j      : longint;
    c          : char;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               readln(M);
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do
                 begin
                     for j := 1 to M do
                       begin
                           read(c);
                           data[i , j] := ord(c) - ord('0');
                           data[i , j] := data[i , j - 1] + data[i - 1 , j] - data[i - 1 , j - 1] + data[i , j];
                       end;
                     readln;
                 end;
           end;
end;

procedure work;
var
    x1 , y1 ,
    x2 , y2 ,
    tmp        : longint;
begin
    ans := 0;
    for x1 := 1 to N do
      for x2 := x1 to N do
        for y1 := 1 to M do
          for y2 := y1 to M do
            begin
                tmp := (x2 - x1 + 1) * (y2 - y1 + 1);
                if data[x2 , y2] - data[x1 - 1 , y2] - data[x2 , y1 - 1] + data[x1 - 1 , y1 - 1] = tmp
                  then inc(ans);
            end;
end;

procedure out;
begin
    writeln(ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
