{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10537.in';
    OutFile    = 'p10537.out';
    Limit      = 52;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tshortest  = array[1..Limit] of extended;
    Tvisited   = array[1..Limit] of boolean;
    Tfather    = array[1..Limit] of longint;

Var
    map        : Tmap;
    shortest   : Tshortest;
    visited    : Tvisited;
    father     : Tfather;
    cases ,
    st , ed ,
    num        : longint;

procedure read_num(var p : longint);
var
    c          : char;
begin
    read(c);
    while not (c in ['a'..'z' , 'A'..'Z']) do read(c);
    if c in ['a'..'z']
      then p := ord(c) - ord('a') + 27
      else p := ord(c) - ord('A') + 1;
end;

function init : boolean;
var
    p1 , p2 ,
    i , M      : longint;
begin
    inc(cases);
    fillchar(map , sizeof(map) , 0);
    readln(M);
    if M = -1
      then init := false
      else begin
               init := true;
               for i := 1 to M do
                 begin
                     read_num(p1);
                     read_num(p2);
                     map[p1 , p2] := true;
                     map[p2 , p1] := true;
                     readln;
                 end;
               read(num);
               read_num(ed); read_num(st);
               readln;
           end;
end;

function tax(p , num : extended) : extended;
var
    tmp        : extended;
begin
    if num > 26
      then tax := 1
      else begin
               tmp := int(p * 20.0 / 19) + 1;
               while tmp - int(tmp / 20 + 0.96) = p do tmp := tmp - 1;
               tax := tmp + 1 - p;
           end;
end;

procedure work;
var
    i , j , min: longint;
    tmp        : extended;
begin
    for i := 1 to 52 do shortest[i] := -1;
    fillchar(visited , sizeof(visited) , 0);
    shortest[st] := tax(num , st) + num;

    for i := 1 to 52 do
      begin
          min := 0;
          for j := 1 to 52 do
            if not visited[j] and (shortest[j] <> -1) then
              if (min = 0) or (shortest[min] > shortest[j]) then
                min := j;

          visited[min] := true;
          if min = ed then break;

          for j := 1 to 52 do
            if map[min , j] and not visited[j] then
              begin
                  tmp := shortest[min] + tax(shortest[min] , j);
                  if (shortest[j] = -1) or (shortest[j] > tmp) or (shortest[j] = tmp) and (min < father[j]) then
                    begin
                        shortest[j] := shortest[min] + tax(shortest[min] , j);
                        father[j] := min;
                    end;
              end;
      end;
end;

procedure print_name(p : longint);
begin
    if p > 26
      then write(chr(p + ord('a') - 1 - 26))
      else write(chr(p + ord('A') - 1));
end;

procedure out;
var
    p          : longint;
begin
    writeln('Case ' , cases , ':');
    if ed > 26
      then writeln(shortest[ed] - 1 : 0 : 0)
      else writeln(shortest[ed] - int(shortest[ed] / 20 + 0.96) : 0 : 0);
    p := ed;
    while p <> st do
      begin
          print_name(p);
          write('-');
          p := father[p];
      end;
    print_name(st);
//    if st = ed then
//      begin write('-'); print_name(st); end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
