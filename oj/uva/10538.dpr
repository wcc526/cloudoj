{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Type
    Tmatrix    = array[1..5 , 1..5] of longint;

Const
    InFile     = 'p10538.in';
    OutFile    = 'p10538.out';
    N          = 5;
    A          : Tmatrix
               = ((1 , 2 , 3 , 4 , 5) ,
                  (4 , 5 , 1 , 2 , 3) ,
                  (2 , 3 , 4 , 5 , 1) ,
                  (5 , 1 , 2 , 3 , 4) ,
                  (3 , 4 , 5 , 1 , 2));
    B          : Tmatrix
               = ((1 , 2 , 3 , 4 , 5) ,
                  (3 , 4 , 5 , 1 , 2) ,
                  (5 , 1 , 2 , 3 , 4) ,
                  (2 , 3 , 4 , 5 , 1) ,
                  (4 , 5 , 1 , 2 , 3));
    factorial  : array[0..5] of longint
               = (1 , 1 , 2 , 6 , 24 , 120);

Var
    matrix     : Tmatrix;
    totCase ,
    answer , 
    nowCase    : longint;

procedure readnum(var p : longint);
var
    c          : char;
begin
    while eoln do readln;
    read(c);
    while c = ' ' do read(c);
    if c = '-'
      then begin read(c); p := -1; end
      else begin
               p := 0;
               while c in ['0'..'9'] do
                 begin
                     p := p * 10 + ord(c) - ord('0');
                     if eoln then break else read(c);
                 end;
           end;
end;

procedure init;
var
    i , j      : longint;
begin
    inc(nowCase);
    for i := 1 to 5 do
      begin
          for j := 1 to 5 do
            readnum(matrix[i , j]);
          readln;
      end;
end;

function calc(tmp , A : Tmatrix) : longint;
var
    p1 , p2    : array[1..5] of longint;
    i , j      : longint;
begin
    fillchar(p1 , sizeof(p1) , 0);
    fillchar(p2 , sizeof(p2) , 0);
    calc := 0;
    for i := 1 to 5 do
      for j := 1 to 5 do
        if tmp[i , j] <> 0 then
          if (p1[tmp[i , j]] = 0) and (p2[A[i , j]] = 0) or (p1[tmp[i , j]] = A[i , j]) and (p2[A[i , j]] = tmp[i , j])
            then begin
                     p1[tmp[i , j]] := A[i , j];
                     p2[A[i , j]] := tmp[i , j];
                 end
            else exit;
    j := 0;
    for i := 1 to 5 do
      if p1[i] = 0 then
        inc(j);
    calc := factorial[j];
end;

procedure work;
var
    tmp        : Tmatrix;
    num1 , num2 ,
    num3 , num4 ,
    i , j      : longint;
begin
    for i := 1 to 5 do
      for j := 1 to 5 do
        if matrix[i , j] <> -1
          then tmp[i , j] := (matrix[i , j] - 1) div 5 + 1
          else tmp[i , j] := 0;
    num1 := calc(tmp , A);
    num3 := calc(tmp , B);
    for i := 1 to 5 do
      for j := 1 to 5 do
        if matrix[i , j] <> -1
          then tmp[i , j] := (matrix[i , j] - 1) mod 5 + 1
          else tmp[i , j] := 0;
    num2 := calc(tmp , B);
    num4 := calc(tmp , A);
    answer := num1 * num2 + num3 * num4;
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
