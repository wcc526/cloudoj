{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10700.in';
    OutFile    = 'p10700.out';
    Limit      = 12;

Type
    Tdata      = array[1..Limit] of extended;
    Toperator  = array[1..Limit] of char;
    Topt       = array[1..Limit , 1..Limit , 0..1] of extended;

Var
    data       : Tdata;
    operator   : Toperator;
    opt        : Topt;
    cases ,
    N          : longint;

procedure init;
var
    s          : string;
    code , p   : longint;
begin
    readln(s); s := s + '+';
    N := 0;
    while s <> '' do
      begin
          inc(N); p := 1;
          while not (s[p] in ['+' , '*']) do inc(p);
          val(copy(s , 1 , p - 1) , data[N] , code);
          operator[N] := s[p];
          delete(s , 1 , p);
      end;
end;

function calc(a , b : extended; oper : char) : extended;
begin
    if oper = '+'
      then calc := a + b
      else calc := a * b;
end;

function min(a , b : extended) : extended;
begin
    if a < b then min := a else min := b;
end;

function max(a , b : extended) : extended;
begin
    if a > b then max := a else max := b;
end;

procedure work;
var
    i , j , k  : longint;
begin
    for i := 1 to N do
      begin
          opt[i , i , 0] := data[i];
          opt[i , i , 1] := data[i];
      end;
    for i := N - 1 downto 1 do
      for j := i + 1 to N do
        begin
            opt[i , j , 0] := calc(opt[i , i , 0] , opt[i + 1 , j , 0] , operator[i]);
            opt[i , j , 1] := calc(opt[i , i , 1] , opt[i + 1 , j , 1] , operator[i]);
            for k := i + 1 to j - 1 do
              begin
                  opt[i , j , 0] := min(opt[i , j , 0] , calc(opt[i , k , 0] , opt[k + 1 , j , 0] , operator[k]));
                  opt[i , j , 1] := max(opt[i , j , 1] , calc(opt[i , k , 1] , opt[k + 1 , j , 1] , operator[k]));
              end;
        end;
end;

procedure out;
begin
    writeln('The maximum and minimum are ' , opt[1 , N , 1] : 0 : 0 , ' and ' , opt[1 , N , 0] : 0 : 0 , '.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
            dec(Cases);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
