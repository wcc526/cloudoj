Const
    InFile     = 'p10679.in';
    OutFile    = 'p10679.out';
    Limit      = 100010;
    LimitSave  = 16;
    LimitLen   = 1000;

Type
    Tdata      = array[1..Limit] of char;
    Tnums      = array[1..Limit] of longint;
    Trmq       = array[1..Limit , 0..LimitSave] of longint;
    Tsuffix    = object
                     tot , step              : longint;
                     source                  : Tdata;
                     SA , rank , height ,
                     lastRank                : Tnums;
                     rmq                     : Trmq;
                     procedure qk_pass_first(start , stop : longint; var mid : longint);
                     procedure qk_sort_first(start , stop : longint);
                     function qk_pass(start , stop : longint; var mid : longint) : boolean;
                     procedure qk_sort(start , stop : longint);
                     function compare(p1 , p2 : longint) : longint;
                     procedure Create_SA;
                     procedure Create_Height;
                     function LCQ(p1 , p2 : longint) : longint;
                 end;
    Tstr       = array[1..LimitLen] of char;

Var
    data       : Tsuffix;
    s          : Tstr;
    log2       : Tnums;
    Len , Q ,
    cases      : longint;

function min(a , b : longint) : longint;
begin
    if a < b then min := a else min := b;
end;

procedure Tsuffix.qk_pass_first(start , stop : longint; var mid : longint);
var
    tmp , key  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := SA[tmp]; SA[tmp] := SA[start];
    while start < stop do
      begin
          while (start < stop) and (source[SA[stop]] > source[key]) do dec(stop);
          SA[start] := SA[stop];
          if start < stop then inc(start);
          while (start < stop) and (source[SA[start]] < source[key]) do inc(start);
          SA[stop] := SA[start];
          if start < stop then dec(stop);
      end;
    SA[start] := key;
    mid := start;
end;

procedure Tsuffix.qk_sort_first(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass_first(start , stop , mid);
          qk_sort_first(start , mid - 1);
          qk_sort_first(mid + 1 , stop);
      end;
end;

function Tsuffix.qk_pass(start , stop : longint; var mid : longint) : boolean;
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := SA[tmp]; SA[tmp] := SA[start]; qk_pass := false;
    while start < stop do
      begin
          while (start < stop) and (compare(SA[stop] , key) > 0) do begin qk_pass := true; dec(stop); end;
          SA[start] := SA[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(SA[start] , key) < 0) do begin qk_pass := true; inc(start); end;
          SA[stop] := SA[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    SA[start] := key;
end;

procedure Tsuffix.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          if not qk_pass(start , stop , mid) then exit;
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function Tsuffix.compare(p1 , p2 : longint) : longint;
begin
    inc(p1 , step); inc(p2 , step);
    if (p1 > tot) or (p2 > tot)
      then compare := p2 - p1
      else compare := lastRank[p1] - lastRank[p2];
end;

procedure Tsuffix.Create_SA;
var
    i , p      : longint;
begin
    for i := 1 to tot do SA[i] := i;
    qk_sort_first(1 , tot);
    for i := 1 to tot do
      if (i = 1) or (source[SA[i]] <> source[SA[i - 1]]) then rank[i] := i else rank[i] := rank[i - 1];
    step := 1;
    while step < tot do
      begin
          lastRank := Rank; p := 1;
          for i := 2 to tot + 1 do
            if (i = tot + 1) or (lastRank[SA[i]] <> lastRank[SA[i - 1]])
              then begin qk_sort(p , i - 1); p := i; end;
          for i := 1 to tot do
            if (i = 1) or (lastRank[SA[i]] <> lastRank[SA[i - 1]]) or (compare(SA[i] , SA[i - 1]) <> 0)
              then rank[i] := i else rank[i] := rank[i - 1];
          step := step * 2;
      end;
end;

procedure Tsuffix.Create_Height;
var
    i , p      : longint;
begin
    source[tot + 1] := #0;
    for i := 1 to tot do
      begin
          if (i = 1) or (height[i - 1] = 0)
            then height[i] := 0
            else height[i] := height[i - 1] - 1;
          if rank[i] = 1 then continue;
          p := SA[rank[i] - 1];
          while source[p + height[i]] = source[i + height[i]] do inc(height[i]);
      end;
    for i := 1 to tot do rmq[i , 0] := height[i];
    step := 1; p := 1;
    while step * 2 <= tot do
      begin
          for i := 1 to tot do
            if i <= step
              then rmq[i , p] := rmq[i , p - 1]
              else rmq[i , p] := min(rmq[i , p - 1] , rmq[i - step , p - 1]);
          step := step * 2; inc(p);
      end;
end;

function Tsuffix.LCQ(p1 , p2 : longint) : longint;
var
    tmp        : longint;
begin
    if p1 = p2 then exit(tot - p1 + 1);
    p1 := rank[p1]; p2 := rank[p2];
    if p1 < p2 then begin tmp := p1; p1 := p2; p2 := tmp; end;
    inc(p1); tmp := log2[p1 - p2 + 1];
    LCQ := min(rmq[p2 , tmp] , rmq[p1 + 1 shl tmp - 1 , tmp]);
end;

procedure pre_process;
var
    i          : longint;
begin
    log2[1] := 0;
    for i := 2 to Limit do
      if 1 shl (log2[i - 1] + 1) = i then log2[i] := log2[i - 1] + 1
                                     else log2[i] := log2[i - 1];
end;

procedure init;
begin
    dec(Cases); data.tot := 0;
    while not eoln do begin inc(data.tot); read(data.source[data.tot]); end;
    readln;
end;

procedure process;
var
    start , stop ,
    p , mid , i ,
    maxwhere   : longint;
begin
    start := 1; stop := data.tot;
    p := 0; maxwhere := 0;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if maxwhere = 0
            then i := 1
            else i := min(data.LCQ(maxwhere , data.SA[mid]) , p) + 1;
          while (i <= Len) and (s[i] = data.source[data.SA[mid] + i - 1]) do inc(i);
          dec(i); if i = Len then break;
          if i > p then begin maxwhere := data.SA[mid]; p := i; end;
          if s[i + 1] < data.source[data.SA[mid] + i]
            then stop := mid - 1
            else start := mid + 1;
      end;
    if i = Len then writeln('y') else writeln('n');
end;

procedure workout;
var
    i          : longint;
begin
    data.create_SA;
    data.create_Height;
    readln(Q);
    for i := 1 to Q do
      begin
          Len := 0;
          while not eoln do begin inc(Len); read(s[Len]); end;
          readln;
          process;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.