#include <stdio.h>

#define EPS 1e-10

int e, n, ir[21], or[21], x[21], y[21];
double v[21];

double a(double x)
{
  return x < 0.0 ? -x : x;
}

void p(int i)
{
  int j, k, d, *r = ir;
  double t;
  for (k = 2; k--; r = or) {
    for (j = n; j-- && e == 0;) {
      if (i ^ j) {
        d = (x[i] - x[j]) * (x[i] - x[j])
          + (y[i] - y[j]) * (y[i] - y[j])
          - (r[i] + r[j]) * (r[i] + r[j]);
        if (d < 0) {
          e = 1;
          return;
        } else if (d == 0) {
          t = (-v[i] * r[i]) / r[j];
          if (v[j] != 0.0) {
            if (a(v[j] - t) > EPS)
              e = 2;
          } else {
            v[j] = t, p(j);
          }
        }
      }
    }
  }
}

int main()
{
  int c = 0, i;

  while (scanf("%d %d %d %d %lf %d", x, y, ir, or, v, &n) == 6) {
    for (++n, i = 0; ++i < n;)
      scanf("%d %d %d %d", x + i, y + i, ir + i, or + i), v[i] = 0.0;
    p(e = 0);
    printf("Simulation #%d\n", ++c);
    switch (e) {
    case 0:
      for (i = 0; ++i < n;)
        if (v[i] == 0.0)
          printf("%2d: Warning -- Idle Gear\n", i);
        else
          printf("%2d: %c %.2lf\n", i,
                 v[i] < 0 ? 'L' : 'R', v[i] < 0.0 ? -v[i] : v[i]);
      break;
    case 1:
      puts("Error -- Overlapping Gears");
      break;
    case 2:
      puts("Error -- Conflicting Gear Rotation");
      break;
    }
    putchar('\n');
  }

  return 0;
}
