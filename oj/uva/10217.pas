Var
    x , N      : extended;

Begin
    while not eof do
      begin
          readln(N);
          x := (1 + sqrt(1 + 4 * N)) / 2 - 1;
          writeln(x : 0 : 2 , ' ' , trunc(x) + 1);
      end;
End.