{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10604.in';
    OutFile    = 'p10604.out';
    Limit      = 6;
    LimitSave  = 6;
    LimitHash  = 999997;

Type
    Tmap       = array[1..Limit , 1..Limit] of
                   record
                       res , cost            : longint;
                   end;
    Tdata      = array[1..Limit] of byte;
    THash      = array[0..LimitHash] of
                   record
                       data                  : Tdata;
                       answer                : longint;
                       used                  : longint;
                   end;

Var
    map        : Tmap;
    data       : Tdata;
    hash       : Thash;
    M , K , T ,
    answer     : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(T);
    fillchar(map , sizeof(map) , 0);
    fillchar(data , sizeof(data) , 0);
    readln(M);
    for i := 1 to M do
      for j := 1 to M do
        readln(map[i , j].res , map[i , j].cost);
    readln(k);
    for i := 1 to k do
      begin
          read(j);
          inc(data[j]);
      end;
    readln; readln;
end;

function same(const data1 , data2 : Tdata) : boolean;
var
    i          : longint;
begin
    same := false;
    for i := 1 to M do
      if data1[i] <> data2[i]
        then exit;
    same := true;
end;

function hash_find(const data : Tdata; var res : longint) : boolean;
var
    num , i    : longint;
begin
    num := 0;
    for i := 1 to M do
      num := num * 7 + data[i];
    num := num mod LimitHash;
    while (hash[num].used = T) do
      if same(hash[num].data , data)
        then begin
                 hash_find := true;
                 res := hash[num].answer;
                 exit;
             end
        else begin
                 inc(num);
                 if num = LimitHash then num := 0;
             end;
    hash_find := false;
end;

procedure hash_add(const data : Tdata; res : longint);
var
    num , i    : longint;
begin
    num := 0;
    for i := 1 to M do
      num := num * 7 + data[i];
    num := num mod LimitHash;
    while (hash[num].used = T) do
      begin
          inc(num);
          if num = LimitHash then num := 0;
      end;
    hash[num].used := T;
    hash[num].data := data;
    hash[num].answer := res;
end;

function dfs(K : longint) : longint;
var
    res , i , j ,
    tmp        : longint;
    first      : boolean;
begin
    if K <= 1
      then dfs := 0
      else begin
               if (K <= LimitSave) and hash_find(data , res)
                 then begin dfs := res; exit; end;
               first := true;
               for i := 1 to M do
                 if data[i] > 0 then
                   begin
                       dec(data[i]);
                       for j := 1 to M do
                         if data[j] > 0 then
                           begin
                               dec(data[j]);
                               inc(data[map[i , j].res]);
                               tmp := map[i , j].cost + dfs(K - 1);
                               if first or (tmp < res) then res := tmp;
                               first := false;
                               dec(data[map[i , j].res]);
                               inc(data[j]);
                           end;
                       inc(data[i]);
                   end;
               dfs := res;
               if K <= LimitSave then
                 hash_add(data , res);
           end;
end;

procedure work;
begin
    answer := dfs(K);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      fillchar(hash , sizeof(hash) , $FF);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
