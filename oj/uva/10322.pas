Var
    area , tmp ,
    a , b , c ,
    cosA , cosB , cosC ,
    d , e , f , p ,
    R1 , R2 , R3 ,
    R , An     : double;
Begin
    while not eof do
      begin
          readln(R1 , R2 , R3);
          R1 := sqrt(R1 / pi); R2 := sqrt(R2 / pi); R3 := sqrt(R3 / pi);
          if R1 < R2 then begin tmp := R1; R1 := R2; R2 := tmp; end;
          if R2 < R3 then begin tmp := R2; R2 := R3; R3 := tmp; end;
          if R1 < R2 then begin tmp := R1; R1 := R2; R2 := tmp; end;
          a := R1 + R2; b := R2 + R3; c := R3 + R1;
          cosA := (b * b + c * c - a * a) / 2 / b / c;
          cosB := (a * a + c * c - b * b) / 2 / a / c;
          cosC := (a * a + b * b - c * c) / 2 / a / b;
          d := sqrt(2 * R1 * R1 * (1 - cosB));
          e := sqrt(2 * R2 * R2 * (1 - cosC));
          f := sqrt(2 * R3 * R3 * (1 - cosA));
          p := (d + e + f) / 2;
          area := sqrt(p * (p - d) * (p - e) * (p - f));
          R := r1 * r2 * r3 / (r1 * r2 + r1 * r3 + r2 * r3 - 2 * sqrt(r1 * r2 * r3 * (r1 + r2 + r3)));
          if R3 < R2 * R1 * (R1 + R2) / (R1 * R1 + R2 * R2 + R1 * R2) then R := R1 + R2;
          An := r1 * r2 * r3 / (r1 * r2 + r1 * r3 + r2 * r3 + 2 * sqrt(r1 * r2 * r3 * (r1 + r2 + r3)));
          writeln(abs(R) : 0 : 10 , ' ' , An : 0 : 10 , ' ' , area : 0 : 10);
      end;
End.