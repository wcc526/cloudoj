{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Var
    p1 , p2    : longint;

procedure read_num(var p : longint);
var
    tp         : longint;
    ch         : char;
begin
    p := 0;
    while not eoln do
      begin
          read(ch);
          ch := upcase(ch);
          if ch in ['A'..'Z'] then
            p := p + ord(ch) - ord('A') + 1;
      end;
    readln;
    while p >= 10 do
      begin
          tp := 0;
          while p <> 0 do begin inc(tp , p mod 10); p := p div 10; end;
          p := tp;
      end;
end;

Begin
    while not eof do
      begin
          read_num(p1);
          read_num(p2);
          if p1 < p2
            then writeln(p1 / p2 * 100 : 0 : 2 , ' %')
            else writeLN(p2 / p1 * 100 : 0 : 2 , ' %');
      end;
End.
