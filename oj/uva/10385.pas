Const
    InFile     = 'p10385.in';
    Limit      = 20;
    minimum    = 1e-6;

Type
    Tdata      = array[1..Limit] of
                   record
                       v1 , v2               : double;
                   end;

Var
    data       : Tdata;
    L ,
    bestx ,
    bestnum    : double;
    N          : longint;

procedure init;
var
    i          : longint;
begin
    readln(L , N);
    for i := 1 to N do
      readln(data[i].v1 , data[i].v2);
end;

procedure calc(x : double);
var
    i          : longint;
    k , b ,
    res        : double;
begin
    res := 1e10;
    for i := 1 to N - 1 do
      begin
          k := (1 / data[N].v1 - 1 / data[N].v2 - 1 / data[i].v1 + 1 / data[i].v2);
          b := L * (1 / data[N].v2 - 1 / data[i].v2);
          if -(k * x + b) < res then res := -(k * x + b);
      end;
    if res > bestnum then
      begin bestnum := res; bestx := x; end;
end;

procedure work;
var
    i , j      : longint;
    x ,
    k1 , b1 ,
    k2 , b2    : double;
begin
    bestnum := -1;
    calc(0); calc(L);
    for i := 0 to trunc(L * 1000) do
      calc(i / 1000);
    for i := 1 to N - 1 do
      for j := i + 1 to N - 1 do
        begin
            k1 := (1 / data[N].v1 - 1 / data[N].v2 - 1 / data[i].v1 + 1 / data[i].v2);
            k2 := (1 / data[N].v1 - 1 / data[N].v2 - 1 / data[j].v1 + 1 / data[j].v2);
            b1 := L * (1 / data[N].v2 - 1 / data[i].v2);
            b2 := L * (1 / data[N].v2 - 1 / data[j].v2);
            if abs(k1 - k2) > minimum then
             begin
                 x := (b2 - b1) / (k1 - k2);
                 if (x < 0) or (x > L) then continue;
                 calc(x);
             end;
        end;
end;

procedure out;
begin
    bestnum := bestnum * 3600;
    if bestnum >= -minimum
      then writeln('The cheater can win by ' , bestnum + minimum : 0 : 0 , ' seconds with r = ' , bestx : 0 : 2 , 'km and k = ' , L - bestx : 0 : 2 , 'km.')
      else writeln('The cheater cannot win.');
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
