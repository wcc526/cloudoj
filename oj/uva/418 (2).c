#include <stdio.h>

int t;
char m[4][13];

void C(int a, int b, int c, int d)
{
  int q, r, s, u, v, w, x, y, z;
  for (u = 1; u < 9; ++u)
    for (v = 1; v < 9; ++v)
      if (m[a][u] == m[c][v])
        for (w = u + 2; w < 11; ++w)
          for (x = 1; x < 9; ++x)
            if (m[a][w] == m[d][x])
              for (y = 1; y < 9; ++y)
                for (z = v + 2; z < 11; ++z)
                  if (m[b][y] == m[c][z] &&
                      (q = y + w - u) < 11 &&
                      (r = x + z - v) < 11 &&
                      m[b][q] == m[d][r] &&
                      t < (s = (w - u - 1) * (z - v - 1)))
                    t = s;
}

int main()
{
  while (scanf("%s %s %s %s", m[0], m[1], m[2], m[3]) == 4 && **m < 'Q') {
    t = 0;
    C(0, 1, 2, 3);
    C(0, 1, 3, 2);
    C(0, 2, 1, 3);
    C(0, 2, 3, 1);
    C(0, 3, 1, 2);
    C(0, 3, 2, 1);
    C(1, 0, 2, 3);
    C(1, 0, 3, 2);
    C(2, 0, 1, 3);
    C(2, 0, 3, 1);
    C(3, 0, 1, 2);
    C(3, 0, 2, 1);
    printf("%d\n", t);
  }

  return 0;
}
