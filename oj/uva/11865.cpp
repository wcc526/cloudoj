/* 【题意】给出一些有向边，边有带宽，还有一个修建费用，然后开始的时候你有cost钱 问你怎么修建一个从0到达其他所有点的网络，使得所有网络中最小的带宽值最大 【算法】因为你有一个初始的金钱，修建一副能从0到达其他所有点的网络，修建费用是 边权，那么很容易想到是最小生成树，因为给出的是有向图，那么就是最小树形图了 然后怎么求得网络的带宽最大呢，可以想到用二分逼近，证明是：假如当前最小 带宽是x，那么边的带宽小于x的当他们不存在在图上，当x很小的时候，图中存在 的边就会变得少，那么构成树形图的机会就更小，即使是构成树形图，那么这个树形图 跟边多的时候的树形图比较，最小权的概率也比密集图小，那么容易证明，是存在 单调性的。二分枚举最大带宽，去掉图上带宽值比这个小的边，对图求一次最小树形图 看边权和是不是不大于cost，然后逼近就行了，记得还要判连通*/
#include<iostream>
#include<cmath>
#include <cstdio>
#include <cstring>
using namespace std;
#define MAXN 101
#define INF 1e10
struct POINT {
	int x, y;
} poi[MAXN];
double gmap[MAXN][MAXN], ans;
int n, m, father[MAXN];
bool vis[MAXN], circle[MAXN];
double length(POINT &a, POINT &b) {
	return sqrt(double((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)));
}
void dfs(int x) {
	vis[x] = true;
	for (int i = 1; i <= n; ++i)
		if (!vis[i] && gmap[x][i] < INF)
			dfs(i);
}
bool connect() {
	memset(vis, 0, sizeof(vis));
	dfs(1);
	for (int i = 1; i <= n; ++i)
		if (!vis[i])
			return false;
	return true;
}
int exist_circle() {
	father[1] = 1; //根的父节点为根 
	for (int i = 2; i <= n; ++i) //为所有点找最小入度边 
			{
		if (!circle[i]) //点没有收缩
		{
			father[i] = i;
			gmap[i][i] = INF; //消除自环 
			for (int j = 1; j <= n; ++j) //找最小入度边 
					{
				if (!circle[j] && gmap[j][i] < gmap[father[i]][i])
					father[i] = j;
			}
		}
	}
	for (int i = 2; i <= n; ++i) //判断是否存在有向环 
			{
		if (circle[i])
			continue;
		memset(vis, 0, sizeof(vis));
		int j = i;
		while (!vis[j]) {
			vis[j] = true;
			j = father[j];
		}
		if (j == 1)
			continue; //最后父节点为根，不是环
		else
			return j; //否则返回环的一个顶点
	}
	return -1; //这个图已经是最小树型图了
}
void update(int t) //收缩环为一点t，并更新边权，这里理论比较复杂，实现不难
		{ /*新图中最小树形图的权加上旧图中被收缩的那个环的权和，就是原图中最小树形图的权。*/
	ans += gmap[father[t]][t];
	for (int i = father[t]; i != t; i = father[i]) //把这个环里面的值全都加起来 
			{
		ans += gmap[father[i]][i];
		circle[i] = true;
	}
	for (int i = 1; i <= n; ++i) //更新所有跟这个点t有关点的边权
			{
		if (!circle[i] && gmap[i][t] != INF)
			gmap[i][t] -= gmap[father[t]][t];
	}
	for (int i = father[t]; i != t; i = father[i]) //更新所有跟环中的点有关的边权
			{
		for (int j = 1; j <= n; ++j) {
			if (circle[j])
				continue;
			if (gmap[j][i] != INF)
				gmap[j][t] = min(gmap[j][t], gmap[j][i] - gmap[father[i]][i]); //更新入边权 /*如果新图中e的权w'(e)是原图中e的权w(e)减去in[u]权的话，那么在我们删除掉in[u]， 并且将e恢复为原图状态的时候，这个树形图的权仍然是新图树形图的权加环的权， 而这个权值正是最小树形图的权值*/ 
			gmap[t][j] = min(gmap[i][j], gmap[t][j]); //更新出边权 
		}
	}
}
void solve() {
	memset(circle, 0, sizeof(circle));
	int t;
	while ((t = exist_circle()) != -1)
		update(t); //存在环，收缩环，直到不存在环为止 
	for (t = 2; t <= n; ++t)
		if (!circle[t])
			ans += gmap[father[t]][t]; //把所有不成环的边权加上去
}
struct Edge {
	int u, v, flow, cost;
	void get() {
		scanf("%d %d %d %d", &u, &v, &flow, &cost);
		v++, u++;
	}
} edge[11000];
int max_cost;
bool build(int mid) {
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			gmap[i][j] = INF;
	for (int i = 0; i < m; i++) {
		if (edge[i].flow >= mid) {
			int u = edge[i].u, v = edge[i].v, cost = edge[i].cost;
			if (gmap[u][v] > cost)
				gmap[u][v] = cost;
		}
	}
	if (!connect())
		return false;
	ans = 0;
	solve();
	return ans <= max_cost;
}
void bs() {
	int left = 1, right = 1000100;
	while (left <= right) {
		int mid = (left + right) >> 1;
		if (build(mid))
			left = mid + 1;
		else
			right = mid - 1;
	}
	if (right)
		printf("%d kbps\n", right);
	else
		puts("streaming not possible.");
}
int main() {
	//freopen("in", "r", stdin);
	int i;
	int T;
	scanf("%d", &T);
	while (T--) {
		scanf("%d %d %d", &n, &m, &max_cost);
		for (i = 0; i < m; ++i)
			edge[i].get();
		bs();
	}
	return 0;
}
