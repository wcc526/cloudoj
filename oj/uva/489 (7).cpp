#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
using namespace std;
char ans[1005];
char guess[1005];
bool vis[1005];
int main()
{
    int i,j,cas,error;
    while(cin>>cas,cas!=-1)
    {
        error=0;

        cin>>ans>>guess;

        int top=strlen(ans),l=top;

        sort(ans,ans+top);

        memset(vis,false,sizeof(vis));

        for(i=0;l>0&&guess[i]&&error<7;i++)
        {
            for(j=0;ans[j];j++)
            if(ans[j]==guess[i])
            break;
            if(ans[j]&&!vis[j])
            {
                int a=j;
                while(guess[i]==ans[j])
                vis[j++]=true;
                l-=(j-a);
            }
            else if(!vis[j])
            error++;
        }
        printf("Round %d\n",cas);
        if(error==7)
        puts("You lose.");
        else if(l>0)
        puts("You chickened out.");
        else
        puts("You win.");
    }
    return 0;
}
