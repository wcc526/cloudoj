{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Var
    L , f      : longint;
    
Begin
    readln(L , f);
    while L + f <> 0 do
      begin
          writeln(sqrt(2 * L * f) : 0 : 10 , ' ' , sqrt(f / 2 / L) * 3600 : 0 : 10);
          readln(L , f);
      end;
End.
