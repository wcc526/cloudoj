{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10610.in';
    OutFile    = 'p10610.out';
    Limit      = 1100;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tqueue     = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of longint;

Var
    data       : Tdata;
    queue      : Tqueue;
    visited    : Tvisited;
    V , M ,
    MaxLen , N ,
    answer     : longint;

function init : boolean;
begin
    readln(V , M);
    if V = 0
      then init := false
      else begin
               init := true;
               readln(data[1].x , data[1].y);
               readln(data[2].x , data[2].y);
               N := 2;
               while not eoln do
                 begin
                     inc(N);
                     readln(data[N].x , data[N].y);
                 end;
               readln;
           end;
end;

procedure work;
var
    open , closed ,
    i          : longint;
    key        : Tpoint;
begin
    MaxLen := V * M * 60;
    fillchar(queue , sizeof(queue) , 0);
    fillchar(visited , sizeof(visited) , $FF);
    visited[1] := 0; queue[1] := 1;
    open := 1; closed := 1;
    while open <= closed do
      begin
          key := data[queue[open]];
          for i := 1 to N do
            if visited[i] = -1 then
              if sqr(data[i].x - key.x) + sqr(data[i].y - key.y) <= sqr(MaxLen) then
                begin
                    inc(closed);
                    queue[closed] := i;
                    visited[i] := visited[queue[open]] + 1;
                end;
          inc(open);
      end;
    answer := visited[2];
end;

procedure out;
begin
    if answer = -1
      then writeln('No.')
      else writeln('Yes, visiting ' , answer - 1 , ' other holes.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
