/**
 * UVa 100 3n+1
 * Author: chchwy at
 * Last Modified: 2011.01.30
 * Tag: Brute Force
 */
#include<cstdio>
#include<algorithm>
using namespace std;

int cycle_length(int n) {
    int length = 1;
    while (n != 1) {
        if (n % 2) //odd
            n = 3*n + 1;
        else //even
            n = n / 2;
        ++length;
    }
    return length;
}

int main() {
    int a, b; //two input
    while (scanf("%d %d", &a, &b) == 2) {

        int max_cycle = 0; // max cycle length

        for (int i=min(a,b); i<=max(a,b); ++i) {
            int tmp = cycle_length(i);
            if (tmp > max_cycle)
                max_cycle = tmp;
        }
        printf("%d %d %d\n",a ,b, max_cycle);
    }
    return 0;
}