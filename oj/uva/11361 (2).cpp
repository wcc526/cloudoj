#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int f[11][110][110], a, b, k;
int value[11], num[11];
int dp(int d, int m1, int m2) {
	if (d == 0) {
		if (m1 == 0 && m2 == 0)
			return 1;
		else
			return 0;
	}
	if (f[d][m1][m2] != -1)
		return f[d][m1][m2];
	f[d][m1][m2] = 0;
	for (int x = 0; x <= 9; x++) {
		f[d][m1][m2] += dp(d - 1, ((m1 - x) % k + k) % k,
				((m2 - (x % k * value[d - 1])) % k + k) % k);
	}
	return f[d][m1][m2];
}
int cal(int n) {
	int i, j, tot = 1, ans = 0;
	while (n) {
		num[tot++] = n % 10;
		n /= 10;
	}
	int count = 0, count1 = 0, temp, temp1;
	for (i = tot - 1; i >= 1; i--) {
		for (j = 0; j < num[i]; j++) {
			temp = count * 10 + j;
			temp1 = count1 + j;
			ans += dp(i - 1, (k - temp1 % k) % k,
					(k - (temp % k * value[i - 1]) % k) % k);
		}
		count = (count * 10 + num[i]) % k;
		count1 = (count1 + num[i]) % k;
	}
	ans += dp(0, (k - count) % k, (k - count1) % k);
	return ans;
}
int main() {
	int i, kcase, mx;
	scanf("%d", &kcase);
	while (kcase--) {
		memset(f, -1, sizeof(f));
		scanf("%d%d%d", &a, &b, &k);
		if (k > 100) {
			printf("0\n");
			continue;
		}
		mx = 1;
		for (i = 0; i < 11; i++) {
			value[i] = mx % k;
			mx = 10 * value[i];
		}
		printf("%d\n", cal(b) - cal(a - 1));
	}
	return 0;
}
