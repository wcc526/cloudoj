Const
    InFile     = 'p10793.in';
    OutFile    = 'p10793.out';
    Limit      = 100;
    maximum    = 1000000000;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;

Var
    data       : Tmap;
    N , totCases ,
    answer ,
    cases      : longint;

function min(a , b : longint) : longint;
begin
    if a < b then min := a else min := b;
end;

procedure init;
var
    i , j ,
    p1 , p2 , C
               : longint;
begin
    inc(Cases);
    read(N);
    for i := 1 to N do
      for j := 1 to N do
        if i <> j then
          data[i , j] := maximum
        else
          data[i , j] := 0;
    readln(j);
    for i := 1 to j do
      begin
          readln(p1 , p2 , C);
          data[p1 , p2] := min(data[p1 , p2] , C);
          data[p2 , p1] := min(data[p2 , p1] , C);
      end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          data[i , j] := min(data[i , j] , data[i , k] + data[k , j]);

    answer := -1;
    for i := 6 to N do
      if (data[i , 1] = data[i , 2]) and (data[i , 2] = data[i , 3]) and
          (data[i , 3] = data[i , 4]) and (data[i , 4] = data[i , 5]) then
        begin
            k := 0;
            for j := 1 to N do
              if k < data[i , j] then
                k := data[i , j];
            if (k < maximum) and ((answer = -1) or (answer > k)) then
              answer := k;
        end;
end;

procedure out;
begin
    writeln('Map ' , cases , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCases); cases := 0;
      while cases < totCases do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.