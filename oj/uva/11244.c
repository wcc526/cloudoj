#include <stdio.h>

int main()
{
  int c, r, i, j, n;
  char g[100][101], p[100][100];

  while (scanf("%d %d", &r, &c) == 2 && r && c) {
    for (i = r; i--;)
      scanf("%s", g[i]);
    for (i = r; i--;) {
      for (j = c; j--;) {
        if ((i && j && g[i - 1][j - 1] == '*') ||
            (i && g[i - 1][j] == '*') ||
            (i && j + 1 < c && g[i - 1][j + 1] =='*') ||
            (j && g[i][j - 1] == '*') ||
            (j + 1 < c && g[i][j + 1] == '*') ||
            (i + 1 < r && j && g[i + 1][j - 1] == '*') ||
            (i + 1 < r && g[i + 1][j] == '*') ||
            (i + 1 < r && j + 1 < c && g[i + 1][j + 1] == '*'))
          p[i][j] = '.';
        else
          p[i][j] = g[i][j];
      }
    }
    for (n = 0, i = r; i--;)
      for (j = c; j--;)
        if (p[i][j] == '*')
          ++n;
    printf("%d\n", n);
  }

  return 0;
}
