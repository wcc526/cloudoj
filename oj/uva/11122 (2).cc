#include <algorithm>
#include <cstdio>
#include <vector>

#define EPS 1e-8
#define ORDER 1 // 1 is clockwise

#define cross(a, b, c, d) ((b.x - a.x)*(d.y - c.y) - (d.x - c.x)*(b.y - a.y))

using namespace std;

typedef struct _point {
  double x, y;

  bool operator<(const _point &p) const {
    return (y + EPS < p.y) || ((y < p.y + EPS) && (x + EPS < p.x));
  }

  bool operator==(const _point &p) const {
    return !(*this < p) && !(p < *this);
  }
} point;

point p0;

int direction(const point &a, const point &b, const point &p)
{
  double r = cross(a, b, a, p);

  if (r > EPS)
    return -1;
  if (r < EPS)
    return 1;
  return 0;
}

bool concave(point &a, point &b, point &c)
{
  return direction(a, b, c) != ORDER;
}

bool radial_less_than(const point &a, const point &b)
{
  return direction(p0, a, b) == ORDER;
}

bool inside_triangle(point (&t)[3], point &p)
{
  int d1 = direction(t[0], t[1], p);
  int d2 = direction(t[1], t[2], p);
  int d3 = direction(t[2], t[0], p);
  return d1 != -ORDER && d2 != -ORDER && d3 != -ORDER;
}

int isect_line_segs(point &a, point &b, point &c, point &d, point &p)
{
  double dn, n1, n2, r,
    x1 = a.x - c.x,
    y1 = a.y - c.y,
    x2 = b.x - a.x,
    y2 = b.y - a.y,
    x3 = d.x - c.x,
    y3 = d.y - c.y;

  dn = x2 * y3 - x3 * y2;
  n1 = x3 * y1 - x1 * y3;
  n2 = x2 * y1 - x1 * y2;

  if (dn < 0) {
    dn = -dn;
    n1 = -n1;
    n2 = -n2;
  }

  if (dn > EPS &&
      -EPS < n1 && n1 < dn + EPS &&
      -EPS < n2 && n2 < dn + EPS) {
    r = n1 / dn;
    p.x = a.x + r * x2;
    p.y = a.y + r * y2;
    return 1;
  }

  return 0;
}

double area_poly(vector<point> &p)
{
  double a = 0.0;
  vector<point>::iterator u, v;

  for (u = p.end() - 1, v = p.begin(); v != p.end(); u = v++)
    a += u->x * v->y - u->y * v->x;

  return 0.5 * a;
}

double isect_triangles_area(point (&t)[2][3])
{
  vector<point> P;
  vector<point>::iterator e, n, s;
  point p;
  int i, j, u, v, x, y;
  double r = 0.0;

  for (i = 1, j = 0; j < 2; i = j++)
    for (u = 2, v = 0; v < 3; u = v++)
      if (inside_triangle(t[i], t[j][v]))
        P.push_back(t[j][v]);

  for (u = 2, v = 0; v < 3; u = v++)
    for (x = 2, y = 0; y < 3; x = y++)
      if (isect_line_segs(t[0][u], t[0][v], t[1][x], t[1][y], p))
        P.push_back(p);

  if (!P.empty()) {
    s = P.begin();
    e = P.end();
    sort(s, e);
    P.erase(unique(s, e), e);
    if (P.size() >= 3) {
      p0 = P[0];
      sort(s + 1, P.end(), radial_less_than);
      r = area_poly(P);
    }
  }

  return r;
}

#include <ctime>
int main(void)
{
  int c = 0, t;
  point p[2][3];

  scanf("%d", &t);

  for (++t; --t;) {
    scanf("%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
          &p[0][0].x, &p[0][0].y,
          &p[0][1].x, &p[0][1].y,
          &p[0][2].x, &p[0][2].y,
          &p[1][0].x, &p[1][0].y,
          &p[1][1].x, &p[1][1].y,
          &p[1][2].x, &p[1][2].y);

    p0 = p[0][0];
    sort(p[0] + 1, p[0] + 3, radial_less_than);
    p0 = p[1][0];
    sort(p[1] + 1, p[1] + 3, radial_less_than);

    printf("pair %d: ", ++c);

    double a = isect_triangles_area(p);
    if (a < -EPS || a > EPS)
      puts("yes");
    else
      puts("no");
  }

  return 0;
}
