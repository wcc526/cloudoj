Const
    InFile     = 'p10448.in';
    OutFile    = 'p10448.out';
    Limit      = 50;
    LimitM     = 200;
    LimitCost  = 100000;
    Maximum    = 1000000000;

Type
    Tkey       = record
                     p1 , p2 , cost          : longint;
                 end;
    Tmap       = array[1..LimitM] of Tkey;
    Tindex     = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tcosts     = array[1..Limit] of longint;
    Topt       = array[0..LimitCost] of longint;

Var
    map        : Tmap;
    index      : Tindex;
    costs      : Tcosts;
    visited    : Tvisited;
    opt        : Topt;
    Q , i , tests ,
    S , T , cost ,
    tot ,
    N , M      : longint;

procedure init;
var
    p1 , p2 , cost ,
    i          : longint;
begin
    dec(Tests);
    readln(N , M);
    for i := 1 to M do
      begin
          read(p1 , p2 , cost);
          map[i].p1 := p1; map[i].p2 := p2; map[i].cost := cost;
          map[i + M].p1 := p2; map[i + M].p2 := p1; map[i + M].cost := cost;
      end;
    M := M * 2;
end;

procedure build_graph;
var
    i , j      : longint;
    tmp        : Tkey;
begin
    for i := 1 to M do
      for j := i + 1 to M do
        if map[i].p1 > map[j].p1 then
          begin
              tmp := map[i]; map[i] := map[j]; map[j] := tmp;
          end;
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[map[i].p1] = 0
        then index[map[i].p1] := i;
end;

function dfs(root : longint) : boolean;
var
    i          : longint;
begin
    if Root = T then exit(true);
    visited[root] := true;
    i := index[root];
    while (i <> 0) and (i <= M) and (map[i].p1 = root) do
      begin
          if not visited[map[i].p2] and dfs(map[i].p2)
            then begin
                     inc(tot); costs[tot] := map[i].cost;
                     exit(true);
                 end;
          inc(i);
      end;
    exit(false);
end;

procedure process;
var
    i , j      : longint;
begin
    readln(S , T , cost); tot := 0;
    fillchar(visited , sizeof(visited) , 0);
    dfs(S);
    for i := 1 to tot do dec(cost , costs[i]);
    if cost < 0 then begin writeln('No'); exit; end;
    for i := 0 to cost do opt[i] := maximum;
    opt[0] := tot;
    for i := 2 to tot do
      for j := 0 to cost - costs[i] * 2 do
        if opt[j + costs[i] * 2] > opt[j] + 2 then
          opt[j + costs[i] * 2] := opt[j] + 2;
    if opt[cost] >= maximum
      then writeln('No')
      else writeln('Yes ' , opt[cost]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(Tests);
    while Tests > 0 do
      begin
          init;
          Build_Graph;
          readln(Q);
          for i := 1 to Q do process;
          if Tests <> 0 then writeln;
      end;
End.