#include <stdio.h>

int g[30][30], n, p[30], s[30];

int path(int i)
{
  int j, k;
  if (p[i] < 0)
    for (p[i] = 1, j = n; j--;)
      if (g[i][j] && p[i] < (k = g[i][j] + path(j)))
        p[i] = k, s[i] = j;
  return p[i];
}

int main()
{
  int b[30][10], d, i, j, k, t;

  while (scanf("%d %d", &n, &d) == 2) {
    for (i = n; i--; p[i] = s[i] = -1)
      for (j = d; j--;)
        scanf("%d", &b[i][j]);
    for (k = n; k--;)
      for (i = 1; i < d; ++i, b[k][j + 1] = t)
        for (t = b[k][j = i]; j-- && b[k][j] > t; b[k][j + 1] = b[k][j]);
    for (i = n; i--;)
      for (j = n; j--;)
        for (g[i][j] = 1, k = d; k-- && g[i][j];)
          g[i][j] &= b[i][k] < b[j][k];
    for (t = k = -1, i = n; i--;)
      if (t < (j = path(i)))
        t = j, k = i;
    printf("%d\n", t);
    for (i = k; k > -1; k = s[k])
      printf("%d%c", n - k, s[k] < 0 ? '\n' : ' ');
  }

  return 0;
}
