#include <algorithm>
#include <cstdio>
#include <iterator>
#include <vector>

using namespace std;

class box {
public:
  vector<int> d;
  int i;

  box(int n, int i) : i(i) {
    d.reserve(n);
  }

  void s(void) {
    sort(d.begin(), d.end());
  }

  bool lt(const box &s) {
    vector<int>::size_type i;
    for (i = 0; i < d.size() && d[i] < s.d[i]; ++i);
    return i == d.size();
  }

  friend bool operator<(const box &d, const box &s) {
    vector<int>::size_type i;
    for (i = 0; i < d.d.size(); ++i)
      if (d.d[i] > s.d[i])
        return false;
      else if (d.d[i] < s.d[i])
        return true;
    return true;
  }
};

int b[30], p[30];

void lis(vector<box> &a, vector<int> &r)
{
  vector<int>::size_type i, j;
  int m = 0;

  for (i = 0; i < a.size(); ++i)
    b[i] = 1, p[i] = i;

  for (i = 1; i < a.size(); ++i)
    for (j = 0; j < i; ++j)
      if (a[j].lt(a[i]) && b[i] < b[j] + 1)
        b[i] = b[j] + 1, p[i] = j;

  for (i = 0; i < a.size(); ++i)
    if (m < b[i]) {
      m = b[i];
      j = i;
    }

  for (i = m; i--; j = p[j])
    r.push_back(j);
}

int main(void)
{
  int i, j, k, n, t;
  vector<box> b;
  vector<int> s;
  vector<int>::size_type x;

  while (scanf("%d %d", &k, &n) == 2) {
    b.clear();
    s.clear();
    b.reserve(k);
    for (i = 0; ++i <= k;) {
      b.push_back(box(n, i));
      box &c = b.back();
      vector<int> &d = c.d;
      for (j = n; j--;) {
        scanf("%d", &t);
        d.push_back(t);
      }
      c.s();
    }
    sort(b.begin(), b.end());
    lis(b, s);
    printf("%d\n", s.size());
    for (x = s.size(); x--;)
      printf("%d%s", b[s[x]].i, x ? " " : "");
    printf("\n");
  }

  return 0;
}
