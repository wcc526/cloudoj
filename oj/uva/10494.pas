Var
    data       : array[1..10000] of longint;
    len , tmp ,
    i          : longint;
    N , last   : comp;
    first      : boolean;
    ch         : char;
Begin
    while not eof do
      begin
          len := 0; read(ch);
          while ch <> ' ' do
            begin
                inc(len); data[len] := ord(ch) - ord('0');
                read(ch);
            end;
          while ch = ' ' do read(ch);
          readln(N);
          if ch = '/'
            then begin
                     last := 0; first := true;
                     for i := 1 to len do
                       begin
                           last := last * 10 + data[i];
                           tmp := trunc(last / N);
                           last := last - tmp * N;
                           if first
                             then if tmp = 0
                                    then
                                    else begin write(tmp); first := false; end
                             else write(tmp);
                       end;
                     if first then write('0');
                     writeln;
                 end
            else begin
                     last := 0;
                     for i := 1 to len do
                       begin
                           last := last * 10 + data[i];
                           last := last - int(last / N) * N;
                       end;
                     writeln(last : 0 : 0);
                 end;
      end;
End.