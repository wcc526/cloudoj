import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner s = new Scanner(System.in);

		int[][] tabell;
		Cell[][] res;

		while (s.hasNext()) {

			int x = s.nextInt();
			int y = s.nextInt();
			
			tabell = new int[x][y];
			res = new Cell[x][y];
			
			for (int i = 0; i < x; i++) {
				
				for (int j = y - 1; j >= 0; j--) {
					tabell[i][j] = s.nextInt();
					res[i][j] = new Cell();
				}
				
			}

			for (int r = 0; r < x; r++) {
				
				res[r][0].weight = tabell[r][0];
				res[r][0].prev = -1;
//				System.out.println(res[r][0].weight);
			}

			for (int i = 1; i < y; i++) {
				for (int j = 0; j < x; j++) {
					
					int min = Integer.MAX_VALUE;
					int prev = Integer.MAX_VALUE;
					
					for (int k = -1; k <= 1; k++) {
						int r0 = j + k;
						if (r0 < 0)
							r0 = x - 1;
						
						else if (r0 == x)
							r0 = 0;
												
						if (min > res[r0][i - 1].weight) {
							min = res[r0][i - 1].weight;
							prev = r0;
						} 
						else if (min == res[r0][i - 1].weight) 
						{
							if (prev > r0)
								prev = r0;
						}
					}
					
					res[j][i].weight = min + tabell[j][i];
					res[j][i].prev = prev;
				
				}
			}

			
			
			int min = Integer.MAX_VALUE;
			int prev = -1;
			for (int i = 0; i < x; i++) {
				
				
				if (min > res[i][y - 1].weight) {
					min = res[i][y - 1].weight;
					prev = i;
					
					
				}
			}

			
			for (int i = y - 1; i >= 0; i--) {
				
				
				System.out.print(prev + 1);
				if (i != 0){
					System.out.print(" ");
				}
				
				prev = res[prev][i].prev;
			}

			System.out.println("\n" + min);

		}

	}
}

class Cell {
	public int weight;
	public int prev;
}
