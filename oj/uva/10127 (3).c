#include <stdio.h>

int main()
{
  int i, j, n;

  while (scanf("%d", &n) == 1) {
    i = j = 0;
    do
      ++i, j = j * 10 + 1;
    while (j %= n);
    printf("%d\n", i);
  }

  return 0;
}
