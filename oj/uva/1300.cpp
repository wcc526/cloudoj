/*
 一道经典DP,有点稍难，如果不是因为事先知道它是经典DP的话还真的不好下手。这道题难就难在它很麻烦，理解题意麻烦，验证算法的正确性

 就更麻烦了－我足足用了一整页草稿纸.

 1. Sample中的结果如何得来.

 显然，S只可能是3种结果,分别为0, 4, 8。
 设两条程序分别为Prog1,Prog2.
 如果最后一条运行的指令为Prog1,则S=4,所以结果为4的概率至少为1/2;
 如果Prog1运行完之后再运行Prog2,则S=8, p(s=8) = 1/2*1/2*1/2*1/2 = 1/16;
 如果Prog1运行完之后,Prog2刚好只运行了第一条指令，则也有S=4, 概率p= (1/2的5次方) * 4= 1/8 (注意Prog2的第一条指令可能在4个位置)
 所以有 S(exp) = 4*(1/2) + 8*(1/16) + 4*(8/1) = 3.


 2. 一个错误的dp

 设一个状态结构为Status={num, val[], R1[2], R2[2]}，num为这个状态可能出现的次数，val[]为变量的期望值，R1[2],R2[2]分别为四个寄

 存器的期望值.设 Status s[i][j],为Prog1运行i条指令，Prog2运行j条指令后的状态。则有:

 s[i][j].num = s[i-1][j].num + s[i][j-1].num

 s[i][j].val[] = (s[i-1][j].RunProg1(i).val[] * s[i-1][j].num + s[[i][i-1].RunProg2(j).val[]*s[i][j-1].num)/s[i][j].num

 R1[2]和R2[2]的处理同val[].

 这样计算得出的结果为2.34. 因为错把每一种组合的可能性当成是一样的了。

 3. 正确的dp

 修改上面的状态结构为 Status={p,val[],R1[],R2[]}, p为该状态可能出现的概率. 其它符号的意义不变,设N1,N2分别为Prog1和Prog2的总指

 令数,则考虑四种情况.(以下i,j均不为0, i,j为0的状态由初始化时确定)

 1. i<N1, j<N2 则有

 s[i][j].p = s[i-1].p * 1/2 + s[i][j-1].p *1/2

 2. i<N1, j=N2,则有

 s[i][j].p = s[i-1].p + s[i][j-1].p * 1/2

 3. i=N1, j<N2,则有

 s[i][j].p = s[i-1].p * 1/2 + s[i].[j-1].p

 4. i=N1, j=N2,则有

 s[i][j].p = s[i-1].p + s[i][j-1].p

 对于变量和寄存器的处理只需根据这四种情况相应调整即可.


 4. 需要注意的问题

 a.变量不分大小写
 b.输入可能有空行,这个要特别注意,我在这里RE了至少二十次.郁闷.

 5.源码,(用时32ms,因为变量表已排序，可以尝试将查找变量的顺序查找改成二分法来提速,不过由于总变量数才10个，所以估计影响不大)

 //========================================
 // acm-pku.1074. Parallel Expectations.
 // by:atyuwen 2008.7.19
 //========================================
 */
#include <iostream>
#include <iomanip>  
#include <string>
#include <cstdlib>
#include <cstring>
#include <cstdio>
using namespace std;

string varList[10];
int numVar;

void AddVar(string &var) {
	if (var[0] >= '0' && var[0] <= '9')
		return;
	for (int i = 0; i < numVar; ++i)
		if (varList[i] == var)
			return;
	varList[numVar++] = var;
}

void SortVarList() {
	for (int i = 0; i < numVar - 1; ++i)
		for (int j = i + 1; j < numVar; ++j)
			if (varList[i] > varList[j]) {
				string temp = varList[i];
				varList[i] = varList[j];
				varList[j] = temp;
			}
}

struct Instruction {
	string opera[3];
	bool isAdd;

	void Set(string s) {
		int n = 0;
		char c;
		isAdd = false;
		opera[0] = opera[1] = opera[2] = "";
		for (int i = 0; i != s.length(); ++i) {
			switch (c = s[i]) {
			case '+':
				isAdd = true;
			case '-':
			case '=':
				++n;
				continue;
			}
			if (c != ':' && c != ' ' && c != '\t') {
				if ('a' <= c && c <= 'z')
					c -= 32;
				opera[n] += c;
			}
		}
		AddVar(opera[0]);
		AddVar(opera[1]);
		AddVar(opera[2]);
	}
};
Instruction insList[2][25];
int numIns[2];

struct Status {
	double valList[10];
	double possibility;
	double R1[2], R2[2];

	int GetIndex(string &var, int a, int b) {
		int i;
		for (i = 0; i < numVar; ++i)
			if (varList[i] == var)
				break;
		return i;
	}

	void SetVal(string &var, double value) {
		valList[GetIndex(var, 0, numVar)] = value;
	}

	double GetVal(string &var) {
		if (var[0] >= '0' && var[0] <= '9')
			return atoi(var.c_str());
		else
			return valList[GetIndex(var, 0, numVar)];
	}
};
Status status[101][101];

void Merge(Status &a, Status &b, Status &c, int kind) {
	double p1, p2;
	switch (kind) {
	case 0:
		p1 = b.possibility / 2;
		p2 = c.possibility / 2;
		break;
	case 1:
		p1 = b.possibility / 2;
		p2 = c.possibility;
		break;
	case 2:
		p1 = b.possibility;
		p2 = c.possibility / 2;
		break;
	case 3:
		p1 = b.possibility;
		p2 = c.possibility;
		break;
	}
	a.possibility = p1 + p2;
	for (int i = 0; i != 10; ++i)
		a.valList[i] = (b.valList[i] * p1 + c.valList[i] * p2) / a.possibility;
	a.R1[0] = (b.R1[0] * p1 + c.R1[0] * p2) / a.possibility;
	a.R1[1] = (b.R1[1] * p1 + c.R1[1] * p2) / a.possibility;
	a.R2[0] = (b.R2[0] * p1 + c.R2[0] * p2) / a.possibility;
	a.R2[1] = (b.R2[1] * p1 + c.R2[1] * p2) / a.possibility;
}

void Calculate(Status &sOut, Status &sIn, int n, int line) {
	sOut = sIn;
	Instruction ins = insList[n][(line - 1) / 4];
	int j = (line - 1) % 4;
	switch (j) {
	case 0:
		sOut.R1[n] = sIn.GetVal(ins.opera[1]);
		break;
	case 1:
		sOut.R2[n] = sIn.GetVal(ins.opera[2]);
		break;
	case 2:
		if (ins.isAdd)
			sOut.R1[n] += sOut.R2[n];
		else
			sOut.R1[n] -= sOut.R2[n];
		break;
	case 3:
		sOut.SetVal(ins.opera[0], sOut.R1[n]);
		break;
	}
}

void Solve() {
	int i, j;
	SortVarList();
	memset(&status[0][0], 0, sizeof(Status));
	status[0][0].possibility = 1;

	for (i = 1; i <= 4 * numIns[0]; ++i) {
		Calculate(status[i][0], status[i - 1][0], 0, i);
		status[i][0].possibility = status[i - 1][0].possibility / 2;
	}
	for (j = 1; j <= 4 * numIns[1]; ++j) {
		Calculate(status[0][j], status[0][j - 1], 1, j);
		status[0][j].possibility = status[0][j - 1].possibility / 2;
	}

	for (j = 1; j <= 4 * numIns[1]; ++j)
		for (i = 1; i <= 4 * numIns[0]; ++i) {
			int kind;
			if (i < 4 * numIns[0] && j < 4 * numIns[1])
				kind = 0;
			else if (j < 4 * numIns[1])
				kind = 1;
			else if (i < 4 * numIns[0])
				kind = 2;
			else
				kind = 3;
			Status t1, t2;
			Calculate(t1, status[i - 1][j], 0, i);
			Calculate(t2, status[i][j - 1], 1, j);
			Merge(status[i][j], t1, t2, kind);
		}
	cout.setf(ios::fixed);
	cout.precision(4);
	for (int k = 0; k < numVar; ++k)
		cout << status[i - 1][j - 1].valList[k] << endl;
}

int main() {
	int nCase;
	cin >> nCase;
	cin.get();
	char buffer[255];
	while (nCase--) {
		int n = 0;
		numVar = 0;
		numIns[0] = numIns[1] = 0;
		while (n <= 1 && cin.getline(buffer, 255)) {
			string strLine;
			strLine += buffer;
			if (strLine == "")
				continue;
			else if (strLine == "END")
				++n;
			else
				insList[n][numIns[n]++].Set(strLine);
		}
		Solve();
		cout << endl;
	}
	return 0;
}
