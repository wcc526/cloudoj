Var
    s , stack  : string[255];
    r          : longint;

procedure init;
var
    ch         : char;
begin
    s := '';
    read(ch); while ch = ' ' do read(ch);
    while ch <> ' ' do begin s := s + ch; read(ch); end;
    readln(r);
end;

procedure sort;
var
    i , j      : longint;
    tmp        : char;
begin
    for i := 1 to length(s) do
      for j := i + 1 to length(s) do
        if s[i] > s[j] then
          begin
              tmp := s[i]; s[i] := s[j]; s[j] := tmp;
          end;
end;

procedure dfs(p , r : longint; last : boolean);
begin
    if r = 0
      then writeln(stack)
      else begin
               if length(s) - p + 1 < r then exit;
               if (p > 1) and (s[p] = s[p - 1]) and not last then
                 begin
                     dfs(p + 1 , r , false);
                     exit;
                 end;
               stack := stack + s[p];
               dfs(p + 1 , r - 1 , true);
               dec(stack[0]);
               dfs(p + 1 , r , false);
           end;
end;

Begin
    while not eof do
      begin
          init;
          sort;
          stack := '';
          dfs(1 , r , true);
      end;
End.