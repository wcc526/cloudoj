{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10696.in';
    OutFile    = 'p10696.out';

Var
    N          : longint;

function f(p : longint) : longint;
begin
    if p >= 101
      then f := p - 10
      else f := 91;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N <> 0 do
        begin
            writeln('f91(' , N , ') = ' , f(N));
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
