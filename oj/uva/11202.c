#include <stdio.h>

int main()
{
  int n, m, t;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %d", &n, &m);
    if (n == m)
      n = (n + 1) >> 1, printf("%d\n", (n * (n + 1)) >> 1);
    else
      printf("%d\n", ((n + 1) >> 1) * ((m + 1) >> 1));
  }

  return 0;
}
