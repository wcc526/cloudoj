Const
    InFile     = 'p10348.in';
    Limit      = 50;
    LimitK     = 20;
    LimitN     = 100;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tpoly      = record
                     tot      : longint;
                     data     : array[1..LimitK] of Tpoint;
                 end;
    Tdata      = array[1..Limit] of Tpoly;
    Tsub       = array[1..LimitN] of
                   record
                       p1 , p2               : Tpoint;
                   end;

Var
    data       : Tdata;
    sub        : Tsub;
    N , M      : longint;

procedure init;
var
    i , j      : longint;
begin
    fillchar(sub , sizeof(sub) , 0);
    fillchar(data , sizeof(data) , 0);
    read(N);
    for i := 1 to N do
      with sub[i] do
        read(p1.x , p1.y , p2.x , p2.y);
    read(M);
    for i := 1 to M do
      with data[i] do
        begin
            read(tot);
            for j := 1 to tot do
              with data[j] do
                read(x , y);
        end;
    readln;
end;

function cross_product(p1 , p2 : Tpoint) : extended;
begin
    exit(p1.x * p2.y - p1.y * p2.x);
end;

function dot_product(p1 , p2 : Tpoint) : extended;
begin
    exit(p1.x * p2.x + p1.y * p2.y);
end;

function cross(p , v , p1 , p2 : Tpoint) : boolean;
begin
    p1.x := p1.x - p.x; p1.y := p1.y - p.y;
    p2.x := p2.x - p.x; p2.y := p2.y - p.y;
    if cross_product(p1 , v) * cross_product(p2 , v) > 0
      then exit(false)
      else if cross_product(p1 , v) * cross_product(p1 , p2) < 0
             then exit(false)
             else exit(true);
end;

function chk_sub(const poly : Tpoly; p : Tpoint) : longint;
var
    v          : Tpoint;
    count      : longint;
    alpha      : extended;
    i          : longint;
begin
    alpha := random(10000) / 10000 * pi * 2;
    v.x := cos(alpha); v.y := sin(alpha);
    count := 0;
    for i := 1 to poly.tot do
      if cross(p , v , poly.data[i] , poly.data[i mod poly.tot + 1]) then
        inc(count);
    if odd(count)
      then chk_sub := 1
      else chk_sub := -1;
end;

function chk(const poly : Tpoly; p : Tpoint) : longint;
var
    i , count  : longint;
begin
    count := 0;
    for i := 1 to 9 do
      inc(count , chk_sub(poly , p));
    if count < 0
      then chk := -1
      else chk := 1;
end;

function in_polys(p : Tpoint) : boolean;
var
    i          : longint;
begin
    for i := 1 to M do
      if chk(data[i] , p) = 1 then
        exit(true);
    exit(false);
end;

function cross_product1(p , p1 , p2 : Tpoint) : extended;
begin
    p1.x := p1.x - p.x; p1.y := p1.y - p.y;
    p2.x := p2.x - p.x; p2.y := p2.y - p.y;
    exit(cross_product(p1 , p2));
end;

function chk_cross_sub(p : longint; p1 , p2 , p3 : Tpoint) : boolean;
begin
    if cross_product1(sub[p].p1 , p1 , sub[p].p2) * cross_product1(sub[p].p1 , p2 , sub[p].p2) >= 0
      then exit(false);
    if cross_product1(p1 , sub[p].p1 , p2) * cross_product1(p1 , sub[p].p2 , p2) >= 0
      then exit(false);
    exit(true);
end;

function chk_cross(p : longint) : boolean;
var
    i , j      : longint;
begin
    for i := 1 to M do
      with data[i] do
        for j := 1 to tot do
          if chk_cross_sub(p , data[j] , data[j mod tot + 1] , data[(j + 1) mod tot + 1]) then
            exit(true);
    exit(false);
end;

procedure process(p : longint);
var
    b1 , b2    : boolean;
begin
    b1 := in_polys(sub[p].p1);
    b2 := in_polys(sub[p].p2);
    if b1 = b2
      then if chk_cross(p)
             then writeln('Submarine ' , p , ' is partially on land.')
             else if b1
                    then writeln('Submarine ' , p , ' is completely on land.')
                    else writeln('Submarine ' , p , ' is still in water.')
      else writeln('Submarine ' , p , ' is partially on land.');
end;

procedure workout;
var
    i          : longint;
begin
    for i := 1 to N do
      process(i);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(INPUT);
End.
