#include <stdio.h>

#define S 60

char p[S][S + 1];
int c5[27] = {
  0x0e8fe31, 0x01e8fa3e, 0x0f8c20f, 0x1e8c63e, 0x1f8721f, 0x1f87210, 0x0f84e2e,
  0x118fe31, 0x01f2109f, 0x0710a4c, 0x1197251, 0x108421f, 0x11dd631, 0x11cd671,
  0x0e8c62e, 0x01e8fa10, 0x0e8c66f, 0x1e8fa51, 0x0f8383e, 0x1fa908e, 0x118c62e,
  0x118a944, 0x0118d771, 0x1151151, 0x1151084, 0x1f1111f
};

void C()
{
  int i, j;
  for (i = S; i--;) {
    for (j = S; j--;)
      p[i][j] = '.';
    p[i][S] = 0;
  }
}

void P()
{
  int i;
  for (i = 0; i < S; ++i)
    puts(p[i]);
  putchar('\n');
  puts("------------------------------------------------------------");
  putchar('\n');
}

void W(int x, int y, char *s)
{
  while (y < 0)
    ++s, ++y;
  while (*s && y < S)
    if (*s == ' ')
      ++s, ++y;
    else
      p[x][y++] = *s++;
}

void B(int x, int y, char *s)
{
  int b, i, j;
  while (*s) {
    b = *s == ' ' ? 0 : c5[*s - 'A'], ++s;
    for (i = 5; i--;) {
      for (j = 5; j--;) {
        if (b & 1 &&
            x + i < S && x + i >= 0 &&
            y + j < S && y + j >= 0)
          p[x + i][y + j] = '*';
        b >>= 1;
      }
    }
    y += 6;
  }
}

int main(void)
{
  int f, x, y, u, v;
  char c[4], s[S + 1];

  C();
  while (scanf("%s", c) == 1) {
    switch (c[1]) {
    case 'C':
      scanf(" C%d %d |%n%60[A-Z .*]%n|", &f, &x, &u, s, &v);
      if (f == 1)
        W(x - 1, S + u - v + 1 >> 1, s);
      else
        B(x - 1, S + (u - v) * 6 + 1 >> 1, s);
      break;
    case 'L':
      scanf(" C%d %d |%60[A-Z .*]|", &f, &x, s);
      if (f == 1)
        W(x - 1, 0, s);
      else
        B(x - 1, 0, s);
      break;
    case 'P':
      scanf(" C%d %d %d |%60[A-Z .*]|", &f, &x, &y, s);
      if (f == 1)
        W(x - 1, y - 1, s);
      else
        B(x - 1, y - 1, s);
      break;
    case 'R':
      scanf(" C%d %d |%n%60[A-Z .*]%n|", &f, &x, &u, s, &v);
      if (f == 1)
        W(x - 1, S + u - v, s);
      else
        B(x - 1, S + (u - v) * 6, s);
      break;
    case 'E':
      P();
      C();
      break;
    default:
      puts(c);
    }
  }

  return 0;
}
