#include <set>
#include <map>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <cctype>
#include <cstdio>
#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int input;
	int iter = 0;
	while(cin >> input)
	{
	    if(input < 0)
            break;
	    iter = iter + 1;
	    float count;
	    count = ceil( log(input)/ log(2) );

	    cout << "Case " << iter << ": " << count << endl;
	}

	return 0;
}

