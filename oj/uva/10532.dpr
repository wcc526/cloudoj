{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
Const
    InFile     = 'p10532.in';
    OutFile    = 'p10532.out';
    Limit      = 50;

Type
    Tanswer    = array[0..Limit] of extended;
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    answer     : Tanswer;
    N , M ,
    Cases      : longint;

function init : boolean;
var
    i , p      : longint;
begin
    read(N , M);
    if N = 0
      then init := false
      else begin
               inc(cases);
               init := true;
               fillchar(answer , sizeof(answer) , 0);
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do
                 begin
                     read(p);
                     inc(data[p]);
                 end;
           end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    answer[0] := 1;
    for i := 1 to N do
      for k := N downto 0 do
        for j := 1 to data[i] do
          if k + j <= N then
            answer[k + j] := answer[k + j] + answer[k];
end;

procedure out;
var
    i , R      : longint;
begin
    writeln('Case ' , Cases , ':');
    for i := 1 to M do
      begin
          read(r);
          writeln(answer[r] : 0 : 0);
      end;
end;

Begin
    cases := 0;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWRite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
