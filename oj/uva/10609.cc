#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iterator>
#include <stack>
#include <vector>

using namespace std;

#define EPS 1e-8

class point {
public:
  double x, y;
  point(double x, double y) : x(x), y(y) {
  }

  bool operator<(const point &r) const {
    return x - r.x < -EPS || (fabs(x - r.x) < EPS && y - r.y < -EPS);
  }
};

class line {
public:
  point a, b;
  line(point a, point b) : a(a), b(b) {
  }
};

#define PI 3.1415926535897932384626433832795

int main()
{
  stack<line> s;
  vector<point> p;
  int z = 0;
  point a(0, 0), b(0, 0);
  double t;

  const double ot = 1.0 / 3.0;
  const double oq = 1.0 / 4.0;
  const double oh = 2.0 * oq;
  const double tq = 3.0 * oq;
  const double cs = cos(PI * ot);
  const double ss = sin(PI * ot);

  while (scanf("%lf %lf %lf %lf %lf", &a.x, &a.y, &b.x, &b.y, &t) == 5) {
    if (t - 1.0 < -EPS)
      break;
    if (sqrt((a.x - b.x) * (a.x - b.x) +
             (a.y - b.y) * (a.y - b.y)) - t > -EPS) {
      p.push_back(a);
      p.push_back(b);
      s.push(line(a, b));
    }
    while (s.size() > 0) {
      line c = s.top();
      s.pop();
      point d(c.b.x - c.a.x, c.b.y - c.a.y);
      point u(c.a.x + oq * d.x, c.a.y + oq * d.y);
      point v(c.a.x + tq * d.x, c.a.y + tq * d.y);
      point w(u.x + oh * (d.x * cs - d.y * ss),
              u.y + oh * (d.x * ss + d.y * cs));
      double l = sqrt(d.x * d.x + d.y * d.y);
      if (oh * l - t > -EPS) {
        p.push_back(u);
        p.push_back(v);
        p.push_back(w);
        if (oq * l - t > -EPS) {
          s.push(line(u, w));
          s.push(line(w, v));
        }
      }
    }

    sort(p.begin(), p.end());
    printf("Case %d:\n%u\n", ++z, p.size());
    for (vector<point>::size_type i = 0; i < p.size(); ++i)
      printf("%.5lf %.5lf\n", p[i].x, p[i].y);
    p.clear();
  }

  return 0;
}
