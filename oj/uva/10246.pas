Const
    InFile     = 'p10246.in';
    Limit      = 80;
    maximum    = 1000000000;

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;
    Tkey       = record
                     name , C                : longint;
                 end;
    Tcost      = array[1..Limit] of Tkey;

Var
    answer ,
    shortest ,
    data       : Tdata;
    cost       : Tcost;
    cases ,
    N , Q      : longint;

function init : boolean;
var
    R , i , j ,
    p1 , p2 , C: longint;
begin
    readln(N , R , Q);
    if N + R + Q = 0 then exit(false);
    init := true;
    for i := 1 to N do read(cost[i].C);
    for i := 1 to N do cost[i].name := i;
    for i := 1 to N do
      for j := 1 to N do
        data[i , j] := maximum;
    shortest := data;
    answer := data;
    for i := 1 to R do
      begin
          readln(p1 , p2 , C);
          if data[p1 , p2] > C then data[p1 , p2] := C;
          if data[p2 , p1] > C then data[p2 , p1] := C;
      end;
end;

procedure work;
var
    tmp        : Tkey;
    i , j , k  : longint;
begin
    for i := 1 to N do
      for j := i + 1 to N do
        if cost[i].C > cost[j].C
          then begin
                   tmp := cost[i]; cost[i] := cost[j]; cost[j] := tmp;
               end;
    shortest := data;
    for k := 1 to N do
      begin
          for i := 1 to N do
            for j := 1 to N do
              if shortest[i , cost[k].name] + shortest[cost[k].name , j] < shortest[i , j] then
                shortest[i , j] := shortest[i , cost[k].name] + shortest[cost[k].name , j];
          for i := 1 to k do
            for j := 1 to k do
              if shortest[cost[i].name , cost[j].name] + cost[k].C < answer[cost[i].name , cost[j].name]
                then answer[cost[i].name , cost[j].name] := shortest[cost[i].name , cost[j].name] + cost[k].C;
      end;
end;

procedure out;
var
    i , p1 , p2: longint;
begin
    inc(Cases);
    if Cases > 1 then writeln;
    writeln('Case #' , cases);
    for i := 1 to Q do
      begin
          readln(p1 , p2);
          if answer[p1 , p2] >= maximum
            then writeln(-1)
            else writeln(answer[p1 , p2]);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.