#include <iostream>
#include <sstream>
#include <vector>
#include <set>

using namespace std;

int main()
{
    string line;
    int numCase = 0;
    while (getline(cin, line)) {
        numCase++;
        int K, E;
        istringstream is(line);
        is >> K; is >> E;
        vector<string> keywords;
        for (int i=0; i<K; ++i) {
            getline(cin, line);
            for (int j=0; j<line.size(); ++j) {
                line[j] = tolower(line[j]);
            }
            keywords.push_back(line);
        }
        int maxMatch = 0;
        set<int>maxIdx;
        vector<string> lines;
        for (int i=0; i<E; ++i) {
            getline(cin, line);
            lines.push_back(line);
            int match = 0;
            for (int j=0; j<keywords.size(); ++j) {
                int keywordIdx = 0;
                for (int k=0; k<line.size(); ++k) {
                    if (tolower(line[k]) != keywords[j][keywordIdx]) {
                        keywordIdx = 0;
                    } else {
                        keywordIdx++;
                        if (keywordIdx == keywords[j].size()) {
                            // check if surrounded by nonalpha characters
                            if ((k - keywords[j].size() + 1) == 0
                                || !isalpha(line[k-keywords[j].size()])) {
                                if ((k+1 == keywords[j].size()
                                || !isalpha(line[k+1]))) {
                                    match++;
                                }
                            }
                            keywordIdx = 0;
                        //    k--;
                        }
                    }
                }
            }
            if (maxMatch < match) {
                maxMatch = match;
                maxIdx.clear();
                maxIdx.insert(i);
            } else if (maxMatch == match) {
                maxIdx.insert(i);
            }
        }
        cout << "Excuse Set #" << numCase << endl;
        for (set<int>::iterator it = maxIdx.begin(); it != maxIdx.end(); ++it) {
            cout << lines[*it] << endl;
        }
        cout << endl;
    }

    return 0;
}
