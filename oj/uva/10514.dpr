{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10514.in';
    OutFile    = 'p10514.out';
    Limit      = 500;
    minimum    = 1e-5;

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    Tmap       = array[1..Limit , 1..Limit] of extended;
    Tshortest  = array[1..Limit] of extended;
    Tvisited   = array[1..Limit] of boolean;
    Tdata      = array[1..Limit] of Tpoint;
    Tindex     = array[1..Limit] of longint;
    TLine      = record
                     A , B , C               : extended;
                 end;

Var
    map        : Tmap;
    data       : Tdata;
    index      : Tindex;
    shortest   : Tshortest;
    visited    : Tvisited;
    N , M , T  : longint;
    answer     : extended;

procedure init;
var
    r1 , r2 , t ,
    i , j , num: longint;
begin
    read(r1 , r2 , t);
    fillchar(index , sizeof(index) , 0);
    index[1] := 1; index[2] := r1 + 1; index[3] := r1 + r2 + 1;
    for i := 1 to r1 + r2 do read(data[i].x , data[i].y);
    for i := 1 to t do
      begin
          read(num); index[i + 3] := index[i + 2] + num;
          for j := index[i + 2]  to index[i + 3] - 1 do
            read(data[j].x , data[j].y);
      end;
    M := t + 2;
    N := index[M + 1] - 3;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p1.x * p2.y - p1.y * p2.x;
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function Get_Crossing(L1 , L2 : TLine; var p : Tpoint) : boolean;
var
    tmp        : extended;
begin
    tmp := L2.B * L1.A - L1.B * L2.A;
    if abs(tmp) <= minimum
      then begin
               Get_Crossing := false;
               exit;
           end;
    Get_Crossing := true;
    p.x := (L2.C * L1.B - L2.B * L1.C) / tmp;
    p.y := -(L2.C * L1.A - L2.A * L1.C) / tmp;
end;

function in_line(p1 , p2 , p : Tpoint) : boolean;
begin
    in_line := (abs(dist(p1 , p2) - dist(p1 , p) - dist(p , p2)) <= minimum);
end;

function min(a , b : extended) : extended;
begin
    if a < b then min := a else min := b;
end;

procedure vertical(p , p1 , p2 : Tpoint; var res : extended);
var
    L , nL     : TLine;
    np         : Tpoint;
begin
    Get_Line(p1 , p2 , L);
    nL.A := L.B; nL.B := -L.A;
    nL.C := -nL.A * p.x - nL.B * p.y;
    if not Get_Crossing(nL , L , np) then exit;
    if not in_line(p1 , p2 , np) then exit;
    res := min(res , dist(np , p));
end;

function dist_line(p1 , q1 , p2 , q2 : Tpoint) : extended;
var
    L1 , L2    : TLine;
    res        : extended;
    p          : Tpoint;
begin
    Get_Line(p1 , q1 , L1);
    Get_Line(p2 , q2 , L2);
    if Get_Crossing(L1 , L2 , p) and in_line(p1 , q1 , p) and in_line(p2 , q2 , p)
      then begin dist_line := 0; exit; end;
    res := dist(p1 , p2);
    res := min(dist(p1 , q2) , res);
    res := min(dist(p2 , p1) , res);
    res := min(dist(p2 , q1) , res);
    vertical(p2 , p1 , q1 , res);
    vertical(q2 , p1 , q1 , res);
    vertical(p1 , p2 , q2 , res);
    vertical(q1 , p2 , q2 , res);
    dist_line := res;
end;

procedure work;
var
    i , j , min ,
    p1 , p2 ,
    q1 , q2 ,
    n1 , n2    : longint;
begin
    fillchar(map , sizeof(map) , 0);
    for i := 1 to M do
      for j := i + 1 to M do
        for p1 := index[i] to index[i + 1] - 1 do
          for p2 := index[j] to index[j + 1] - 1 do
            if (i > 2) or (p1 <> index[i + 1] - 1) then
              if (j > 2) or (p2 <> index[j + 1] - 1) then
                begin
                    if p1 = index[i + 1] - 1 then q1 := index[i] else q1 := p1 + 1;
                    if p2 = index[j + 1] - 1 then q2 := index[j] else q2 := p2 + 1;
                    n1 := p1; if i = 2 then dec(n1); if i > 2 then dec(n1 , 2);
                    n2 := p2; if j = 2 then dec(n2); if j > 2 then dec(n2 , 2);
                    map[n1 , n2] := dist_line(data[p1] , data[q1] , data[p2] , data[q2]);
                    map[n2 , n1] := map[n1 , n2];
                end;

    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to N do shortest[i] := 1e10;
    for i := 1 to index[2] - 2 do shortest[i] := 0;
    for i := 1 to N do
      begin
          min := 0;
          for j := 1 to N do
            if not visited[j] then
              if (min = 0) or (shortest[min] > shortest[j]) then
                min := j;
                
          visited[min] := true;
          for j := 1 to N do
            if not visited[j] then
              if shortest[min] + map[min , j] < shortest[j] then
                shortest[j] := shortest[min] + map[min , j];
      end;
    answer := shortest[index[2] - 1];
end;

procedure out;
begin
    writeln(answer : 0 : 3);
    dec(T);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
