Var
    F          : array[0..50] of extended;
    N ,
    i , T      : longint;

Begin
    fillchar(F , sizeof(F) , 0);
    F[0] := 1; F[1] := 2;
    for i := 2 to 50 do F[i] := F[i - 1] + F[i - 2];
    readln(T);
    for i := 1 to T do
      begin
          readln(N);
          writeln('Scenario #' , i , ':');
          writeln(F[N] : 0 : 0);
          if i <> T then writeln;
      end;
End.