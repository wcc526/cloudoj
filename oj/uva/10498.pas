Const
    InFile     = 'p10498.in';
    OutFile    = 'p10498.out';
    Limit      = 40;
    minimum    = 1e-6;

Type
    Tvisited   = array[1..Limit] of boolean;
    Tcost      = array[1..Limit] of double;
    Tnums      = array[1..Limit] of longint;
    Tmatrix    = array[1..Limit , 1..Limit + 1] of double;

Var
    cost ,
    answer     : Tcost;
    position   : Tnums;
    base       : Tvisited;
    matrix     : Tmatrix;
    count ,
    N , M      : longint;
    bestans    : double;

procedure init;
var
    i , j      : longint;
begin
    bestans := 0;
    read(M , N);
    fillchar(cost , sizeof(cost) , 0);
    fillchar(matrix , sizeof(matrix) , 0);
    fillchar(base , sizeof(base) , 0);
    fillchar(position , sizeof(position) , 0);
    for i := 1 to M do read(cost[i]);
    for i := 1 to N do
      begin
          for j := 1 to M do read(matrix[i , j]);
          matrix[i , M + i] := 1;
          read(matrix[i , N + M + 1]);
          base[M + i] := true; position[i] := M + i;
      end;
    M := N + M;
    readln;
end;

function zero(num : double) : boolean;
begin
    exit(abs(num) <= minimum);
end;

function extend : boolean;
var
    i , max , j ,
    minp       : longint;
    t ,
    a , b , c  : double;
begin
    max := 0;
    for i := 1 to M do
      if not base[i] then
        if (max = 0) or (cost[max] < cost[i]) then
          max := i;
    if cost[max] <= minimum then exit(false);
    minp := 0;
    for i := 1 to N do
      if matrix[i , max] >= minimum then
        if (minp = 0) or (matrix[minp , M + 1] / matrix[minp , max] > matrix[i , M + 1] / matrix[i , max]) then
          minp := i;
    a := matrix[minp , max]; b := matrix[minp , position[minp]]; c := matrix[minp , M + 1];
    for j := 1 to M + 1 do
      matrix[minp , j] := matrix[minp , j] / a;
    for i := 1 to N do
      if i <> minp then
        begin
            t := matrix[i , max];
            for j := 1 to M + 1 do
              matrix[i , j] := matrix[i , j] - matrix[minp , j] * t;
        end;
    base[position[minp]] := false; position[minp] := max;
    base[max] := true;
    fillchar(answer , sizeof(answer) , 0);
    for i := 1 to N do
      begin
          answer[position[i]] := matrix[i , M + 1];
          for j := 1 to M do
            if position[i] <> j then
              answer[position[i]] := answer[position[i]] - answer[j] * matrix[i , j];
      end;
    t := cost[max];
    for j := 1 to M do
      cost[j] := cost[j] - matrix[minp , j] * t;
    bestans := bestans + matrix[minp , M + 1] * t;
    extend := true;
end;

procedure work;
begin
    while extend do;
end;

procedure out;
begin
    bestans := bestans * N;
    writeln('Nasa can spend ' , int(bestans + 1 - minimum) : 0 : 0 , ' taka.');
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
