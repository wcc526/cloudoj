#include <cstdio>
#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <map>
#include <vector>

using namespace std;
typedef double db;
const db eps=1e-9;
int N,M;
struct pt3D
{
    db x,y,z;
	pt3D(){}pt3D(db x,db y,db z):x(x),y(y),z(z){}
	pt3D operator /(db a){
		return pt3D(x/a,y/a,z/a);
	}
	pt3D operator +(pt3D a){
		return pt3D(x+a.x,y+a.y,z+a.z);
	}
	pt3D operator -(pt3D a){
		return pt3D(x-a.x,y-a.y,z-a.z);
	}
	pt3D operator &(pt3D b){
		return pt3D(y*b.z-b.y*z,b.x*z-b.z*x,x*b.y-b.x*y);
	}
	db operator *(pt3D a){
		return x*a.x+y*a.y+z*a.z;
	}
	pt3D operator ^(db a){
		return pt3D(x*a,y*a,z*a);
	}
    db len(){return sqrt(x*x+y*y+z*z);}
	void getP(){
		scanf("%lf%lf%lf",&x,&y,&z);
	}
	void outP(){
		printf("x:%.2f y:%.2f z:%.2f\n",x,y,z);	
	}
};

vector<pt3D>my[100000];

int sig(db a)
{
	if(fabs(a)<=eps) return 0;
	if(a>eps) return 1;
	return -1;
}

pt3D p1[100],p2[100],Fa[220000],P0[220000];
bool flag[61][61][61];

pt3D center(pt3D a,pt3D b,pt3D c,db A)
{
	return (a+b+c)^A;
}

pt3D KK,cP0;
bool cmp(pt3D a,pt3D b)
{
	return sig(KK*((a-cP0)&(b-cP0)))>0;
}

int check(vector<pt3D>kl,pt3D m,pt3D fa)
{
	int l=kl.size();
	kl.push_back(kl[0]);
	for(int i=0;i<l;++i)
	{
		if(sig(fa*((kl[i+1]-kl[i])&(m-kl[i])))<0)
			return 0;
	}
	return 1;
}

db solve(pt3D p[],int N)
{
	memset(flag,0,sizeof(flag));
	int ft=0,idx[100];
	pt3D point[100];
	db minn=1e30;
	pt3D a,b,c,m=pt3D(0,0,0);
	db volm=0;
	/*for(int i=0;i<N;++i) 
		p[i].outP();*/
	for(int i=0;i<N;++i)
		for(int j=i+1;j<N;++j)
			for(int k=j+1;k<N;++k)
			{
				//printf("%d %d %d:\n ",i,j,k);
				int it=0;
				if(flag[i][j][k]) continue;
				a=p[i],b=p[j],c=p[k];
				pt3D fa=(b-a)&(c-a);
				//fa.outP();
				int F=0;
				for(int v=0;v<N;++v)
				{
					int hj=sig(fa*(p[v]-a));
					if(hj==1)
					{
						swap(b,c);
						fa=(b-a)&(c-a);
						break;
					}
				}
                idx[it] = i,point[it++] = p[i];
				for(int v=0;v<N;++v)
				{
                    if (v == i) continue;
					int hj=sig(fa*(p[v]-a));
					//printf("%d : %d\n",v,hj);
					if(hj==1)
					{
						F=1;
						break;
					}
					else if(hj==0) idx[it]=v,point[it++]=p[v];
				}
				if(F) continue;
				for(int ii=0;ii<it;++ii)
					for(int jj=ii+1;jj<it;++jj)
						for(int kk=jj+1;kk<it;++kk)
							flag[idx[ii]][idx[jj]][idx[kk]]=1;
				KK=fa,cP0 = point[0];
				sort(point+1,point+it,cmp);
				//printf("%d %d %d:\n",i,j,k);for(int v=0;v<it;++v){printf("%d : ",v);point[v].outP();}
                point[it] = point[0];
				for(int v=0;v<it;++v)
				{
					//printf("%d :",v);
					db cd=(point[v] & point[v+1])*point[0];
					//printf("%.2f\n",cd);
                    pt3D cp = center(point[0],point[v],point[v+1],cd);
                    //cp.outP();
					volm+=cd;
					m=m+center(point[0],point[v],point[v+1],cd);
				}
                //m.outP();puts("");
				my[ft].clear();
				for(int v=0;v<it;++v)
					my[ft].push_back(point[v]);
                P0[ft] = point[0];
				Fa[ft++]=fa;
			}
	volm = fabs(volm);
	m=m/(4*volm);
	//printf("v = %.2f\n" ,volm/6);m.outP();
	for(int i=0;i<ft;++i)
	{
		if(check(my[i],m,Fa[i]))
			minn=min(minn,fabs(Fa[i]*(m-P0[i])/Fa[i].len()));
	}
	return minn;
}

int main()
{
    while(scanf("%d",&N)!=EOF)
	{
		memset(flag,0,sizeof(flag));
		for(int i=0;i<N;++i)
			p1[i].getP();
		scanf("%d",&M);
		for(int i=0;i<M;++i)
			p2[i].getP();
		//printf("M :%.2f\n",solve(p1,N));
		//printf("M :%.2f\n",solve(p2,M));
		printf("%.5f\n",solve(p1,N)+solve(p2,M));
	}
}
