{$R-,Q-,S-,I-}
Const
    InFile     = 'p10248.in';
    OutFile    = 'p10248.out';

Type
    Tmap       = array[0..9 , 0..9] of longint;
    Tdegree    = array[0..9] of longint;

Var
    map        : Tmap;
    indegree ,
    outdegree  : Tdegree;
    mustprint ,
    A , B      : longint;

procedure init;
begin
    readln(A , B);
    fillchar(map , sizeof(map) , 0);
    fillchar(indegree , sizeof(indegree) , 0);
    fillchar(outdegree , sizeof(outdegree) , 0);
end;

procedure dfs_print(p : longint);
var
    i , j      : longint;
    first ,
    found      : boolean;
begin
    found := false; first := true;
    for i := 0 to 9 do
      while map[i , p] > 0 do
        begin
            found := true;
            dec(map[i , p]);
            dec(indegree[p]); dec(outdegree[i]);
            if not first then mustprint := p;
            dfs_print(i);
            first := false;
        end;
    if (mustprint <> -1) and (mustprint <> p) then
      write(mustprint);
    mustprint := -1;
    write(p);
end;

procedure work;
var
    j , delta ,
    i , p1 , p2: longint;
    found      : boolean;
begin
    for i := A to B do
      if i >= 10 then
        begin
            p1 := i div 10; p2 := i mod 10;
            inc(map[p1 , p2]);
            inc(outdegree[p1]); inc(indegree[p2]);
        end;

    for i := A to 9 do if i <= B then
      begin
          found := false;
          for j := 10 to B do
            if (i = j div 10) or (i = j mod 10) then
              begin
                  found := true;
                  break;
              end;
          if not found then write(i);
      end;

    delta := 0;
    for i := 0 to 9 do inc(delta , abs(outdegree[i] - indegree[i]));
    delta := delta div 2;
    while delta > 1 do
      for i := 0 to 9 do
        for j := 0 to 9 do
          if (delta > 1) and (outdegree[i] > indegree[i]) and (indegree[j] > outdegree[j]) then
            begin
                inc(map[j , i]);
                inc(indegree[i]); inc(outdegree[j]);
                dec(delta);
            end;
    for i := 0 to 9 do
      while outdegree[i] < indegree[i] do
        begin
            mustprint := -1;
            dfs_print(i);
        end;
    for i := 0 to 9 do
      while indegree[i] <> 0 do
        begin
            mustprint := -1;
            dfs_print(i);
        end;
    writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
    while not eof do
      begin
          init;
          work;
      end;
    Close(INPUT);
    Close(OUTPUT);
End.
