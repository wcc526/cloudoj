{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10611.in';
    OutFile    = 'p10611.out';
    Limit      = 50000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    N , Q      : longint;

procedure init;
var
    i          : longint;
begin
    read(N);
    for i := 1 to N do
      read(data[i]);
end;

function binary_search1(p : longint) : longint;
var
    start , stop ,
    mid        : longint;
begin
    binary_search1 := -1;
    start := 1; stop := N;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if data[mid] < p
            then begin
                     binary_search1 := mid;
                     start := mid + 1;
                 end
            else stop := mid - 1;
      end;
end;

function binary_search2(p : longint) : longint;
var
    start , stop ,
    mid        : longint;
begin
    binary_search2 := -1;
    start := 1; stop := N;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if data[mid] > p
            then begin
                     binary_search2 := mid;
                     stop := mid - 1;
                 end
            else start := mid + 1;
      end;
end;

procedure workout;
var
    i , p ,
    p1 , p2    : longint;
begin
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Q);
      for i := 1 to Q do
        begin
            read(p);
            p1 := binary_search1(p);
            p2 := binary_search2(p);
            if p1 = -1 then write('X') else write(data[p1]);
            write(' ');
            if p2 = -1 then write('X') else write(data[p2]);
            writeln;
        end;
//    Close(OUTPUT);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      init;
      workout;
//    Close(INPUT);
End.
