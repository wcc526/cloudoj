Const
    InFile     = 'p10374.in';
    Limit      = 20;

Type
    Tstr       = string[100];
    Tdata      = array[1..Limit] of
                   record
                       s1 , s2               : Tstr;
                       count                 : longint;
                   end;

Var
    data       : Tdata;
    N , cases  : longint;

procedure main;
var
    i , j , count ,
    M , max    : longint;
    s          : Tstr;
begin
    dec(cases);
    fillchar(data , sizeof(data) , 0);
    readln(N);
    for i := 1 to N do
      begin
          readln(data[i].s1); readln(data[i].s2);
      end;
    readln(M);
    for i := 1 to M do
      begin
          readln(s);
          for j := 1 to N do
            if data[j].s1 = s then
              begin
                  inc(data[j].count);
                  break;
              end;
      end;
    max := -1;
    for i := 1 to N do
      if data[i].count > max then
        max := data[i].count;
    count := 0;
    for i := 1 to N do
      if data[i].count = max
        then inc(count);
    if count = 1
      then for i := 1 to N do
             if data[i].count = max then writeln(data[i].s2) else
      else writeln('tie');
    if cases > 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(cases);
      while cases > 0 do
        begin
            main;
        end;
//    Close(INPUT);
End.