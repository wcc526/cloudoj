{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10674.in';
    OutFile    = 'p10674.out';
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y    : extended;
                 end;
    Tcircle    = record
                     O        : Tpoint;
                     R        : extended;
                 end;
    Tans       = record
                     p1 , p2  : Tpoint;
                     Len      : extended;
                 end;
    Tanswer    = array[1..4] of Tans;

Var
    C1 , C2    : Tcircle;
    answer     : Tanswer;
    tot        : longint;

function init : boolean;
begin
    readln(C1.O.x , C1.O.y , C1.R , C2.O.x , C2.O.y , C2.R);
    init := (C1.O.x <> 0) or (C1.O.y <> 0) or (C1.R <> 0) or (C2.O.x <> 0) or (C2.O.y <> 0) or (C2.R <> 0); 
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function module(p : Tpoint) : extended;
begin
    module := sqrt(sqr(p.x) + sqr(p.y))
end;

procedure reduce(var vector : Tpoint; Len : extended);
var
    m          : extended;
begin
    m := module(vector);
    vector.x := vector.x / m * Len;
    vector.y := vector.y / m * Len;
end;

function zero(num : extended) : boolean;
begin
    zero := abs(num) <= minimum;
end;

procedure rotate(p : Tpoint; sinA , cosA : extended; var np : Tpoint);
begin
    np.x := p.x * cosA - p.y * sinA;
    np.y := p.x * sinA + p.y * cosA;
end;

procedure ins(ans : Tans);
begin
    inc(tot);
    answer[tot] := ans;
end;

function bigger(ans1 , ans2 : Tans) : boolean;
begin
    if not zero(ans1.p1.x - ans2.p1.x)
      then bigger := ans1.p1.x > ans2.p1.x
      else bigger := ans1.p1.y > ans2.p1.y;
end;

function same(ans1 , ans2 : Tans) : boolean;
begin
    same := zero(ans1.p1.x - ans2.p1.x) and zero(ans1.p2.x - ans2.p2.x)
        and zero(ans1.p1.y - ans2.p1.y) and zero(ans1.p2.y - ans2.p2.y)
        and zero(ans1.Len - ans2.Len);
end;

procedure work;
var
    p1 , p2 ,
    np1 , np2  : Tpoint;
    sinA , cosA ,
    d          : extended;
    changed    : boolean;
    i , j      : longint;
    ans        : Tans;
begin
    fillchar(answer , sizeof(answer) , 0); tot := 0;
    if zero(C1.O.x - C2.O.x) and zero(C1.O.y - C2.O.y) and zero(C1.R - C2.R) then
      begin tot := -1; exit; end;
    d := dist(C1.O , C2.O);
    if abs(C1.R - C2.R) <= d + minimum then
      begin
          if C1.R <= C2.R
            then begin p1.x := C1.O.x - C2.O.x; p1.y := C1.O.y - C2.O.y; end
            else begin p1.x := C2.O.x - C1.O.x; p1.y := C2.O.y - C1.O.y; end;
          p2 := p1;
          reduce(p1 , C1.R);
          reduce(p2 , C2.R);
          cosA := abs(C1.R - C2.R) / d;
          sinA := sqrt(abs(1 - sqr(cosA)));

          rotate(p1 , sinA , cosA , np1);
          rotate(p2 , sinA , cosA , np2);
          np1.x := np1.x + C1.O.x; np1.y := np1.y + C1.O.y;
          np2.x := np2.x + C2.O.x; np2.y := np2.y + C2.O.y;
          ans.p1 := np1; ans.p2 := np2; ans.Len := d * sinA;
          ins(ans);

          rotate(p1 , -sinA , cosA , np1);
          rotate(p2 , -sinA , cosA , np2);
          np1.x := np1.x + C1.O.x; np1.y := np1.y + C1.O.y;
          np2.x := np2.x + C2.O.x; np2.y := np2.y + C2.O.y;
          ans.p1 := np1; ans.p2 := np2; ans.Len := d * sinA;
          ins(ans);
      end;

    if C1.R + C2.R <= d + minimum then
      begin
          p1.x := C2.O.x - C1.O.x; p1.y := C2.O.y - C1.O.y;
          p2.x := C1.O.x - C2.O.x; p2.y := C1.O.y - C2.O.y;
          reduce(p1 , C1.R);
          reduce(p2 , C2.R);
          cosA := (C1.R + C2.R) / d;
          sinA := sqrt(abs(1 - sqr(cosA)));

          rotate(p1 , sinA , cosA , np1);
          rotate(p2 , sinA , cosA , np2);
          np1.x := np1.x + C1.O.x; np1.y := np1.y + C1.O.y;
          np2.x := np2.x + C2.O.x; np2.y := np2.y + C2.O.y;
          ans.p1 := np1; ans.p2 := np2; ans.Len := d * sinA;
          ins(ans);

          rotate(p1 , -sinA , cosA , np1);
          rotate(p2 , -sinA , cosA , np2);
          np1.x := np1.x + C1.O.x; np1.y := np1.y + C1.O.y;
          np2.x := np2.x + C2.O.x; np2.y := np2.y + C2.O.y;
          ans.p1 := np1; ans.p2 := np2; ans.Len := d * sinA;
          ins(ans);
      end;

    changed := true;
    while changed do
      begin
          changed := false;
          for i := 1 to tot do
            for j := i + 1 to tot do
              if bigger(answer[i] , answer[j]) then
                begin
                    changed := true;
                    ans := answer[i]; answer[i] := answer[j]; answer[j] := ans;
                end;
      end;
    i := 2; j := 1;
    while i <= tot do
      begin
          if not same(answer[i] , answer[j])
            then begin
                     inc(j);
                     answer[j] := answer[i];
                 end;
          inc(i);
      end;
    if j < tot then tot := j;
end;

procedure out;
var
    i          : longint;
begin
    writeln(tot);
    for i := 1 to tot do
      with answer[i] do
        writeln(p1.x : 0 : 5 , ' ' , p1.y : 0 : 5 , ' ' , p2.x : 0 : 5 , ' ' , p2.y : 0 : 5 , ' ' , Len : 0 : 5);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
