{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10635.in';
    OutFile    = 'p10635.out';
    Limit      = 250;
    LimitSave  = Limit * Limit;

Type
    Tdata      = array[1..LimitSave] of longint;

Var
    A , B ,
    indexA ,
    data ,
    answer     : Tdata;
    nowCase , 
    cases , 
    p , q , N ,
    top        : longint;

procedure init;
var
    i          : longint;
begin
    fillchar(A , sizeof(A) , 0);
    fillchar(B , sizeof(B) , 0);
    fillchar(indexA , sizeof(indexA) , 0);
    fillchar(data , sizeof(data) , 0);
    inc(nowCase);
    read(i , p , q);
    inc(p); inc(q);
    for i := 1 to p do read(A[i]);
    for i := 1 to q do read(B[i]);
end;

procedure work;
var
    start , stop ,
    mid , res , 
    i , j      : longint;
begin
    for i := 1 to p do indexA[A[i]] := i;
    N := 0;
    for j := 1 to q do
      if indexA[B[j]] <> 0 then
        begin
            inc(N);
            data[N] := indexA[B[j]];
        end;

    top := 1; answer[1] := B[1]; res := 0;
    for i := 2 to N do
      begin
          start := 1; stop := top;
          while start <= stop do
            begin
                mid := (start + stop) div 2;
                if answer[mid] < data[i]
                  then begin
                           start := mid + 1;
                           res := mid;
                       end
                  else stop := mid - 1;
            end;
          answer[res + 1] := data[i];
          if res + 1 > top then inc(top);
      end;
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , top);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      nowCase := 0;
      while Cases > nowCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
