{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10410.in';
    OutFile    = 'p10410.out';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of longint;
    Tanswer    = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..Limit] of longint;
                   end;
Var
    BFS , DFS ,
    index_DFS ,
    index_BFS  : Tdata;
    answer     : Tanswer;
    N          : longint;

procedure init;
var
    i          : longint;
begin
    readln(N);
    for i := 1 to N do answer[i].tot := 0;
    for i := 1 to N do read(BFS[i]);
    for i := 1 to N do read(DFS[i]);
    readln;
end;

procedure dfs_ans(father , start , stop : longint);
var
    p , nextp  : longint;
begin
    if father <> 0 then
      with answer[father] do
        begin
            inc(tot);
            data[tot] := DFS[start];
        end;
    if stop <= start then exit;
    p := DFS[start + 1];
    while (p <> -1) and (index_DFS[p] <= stop) and (index_DFS[p] >= start) do
      begin
          if index_BFS[p] < N then nextp := BFS[index_BFS[p] + 1] else nextp := -1;
          if (nextp <> -1) and (index_DFS[nextp] <= stop) and (index_DFS[nextp] >= start) and (index_DFS[nextp] > index_DFS[p]) and (nextp > p)
            then dfs_ans(DFS[start] , index_DFS[p] , index_DFS[nextp] - 1)
            else begin
                     dfs_ans(DFS[start] , index_DFS[p] , stop);
                     if (nextp = -1) or (index_DFS[p] > index_DFS[nextp]) or (p > nextp) then break;
                 end;
          p := nextp;
      end;
end;

procedure work;
var
    i          : longint;
begin
    for i := 1 to N do index_DFS[DFS[i]] := i;
    for i := 1 to N do index_BFS[BFS[i]] := i;
    dfs_ans(0 , 1 , N);
end;

procedure out;
var
    i , j , k ,
    tmp        : longint;
begin
    for i := 1 to N do
      begin
          write(i , ':');
          for j := 1 to answer[i].tot do
            write(' ' , answer[i].data[j]);
          writeln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWRite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
