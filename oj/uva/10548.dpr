{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10548.in';
    OutFile    = 'p10548.out';

Var
    a , b , c ,
    answer     : comp;
    cases      : longint;

procedure init;
begin
    dec(cases);
    readln(a , b , c);
end;

function Euclid_Extend(a , b , c : comp; var x , y : comp) : comp;
var
    tmp , 
    tx , ty    : comp;
begin
    if a = 0
      then begin
               if int(c / b) * b = c
                 then begin
                          Euclid_Extend := abs(b);
                          x := 0; y := c / b;
                      end
                 else Euclid_Extend := -1;
           end
      else begin
               tmp := Euclid_Extend(b - int(b / a) * a , a , c , tx , ty);
               if tmp <> -1 then
                 begin
                     y := tx;
                     x := ty - int(b / a) * tx;
                 end;
               Euclid_Extend := tmp;
           end;
end;

procedure work;
var
    x , y , gcd ,
    dx , dy ,
    up , down  : comp;
begin
    gcd :=  Euclid_Extend(a , b , c , x , y);
    if gcd = -1
      then answer := -1
      else begin
               if (a > 0) xor (b > 0)
                 then answer := -2
                 else begin
                          dx := b / gcd; dy := a / gcd;
                          if (-x / dx < 0) or (int(x / dx) * dx = x)
                            then down := int(-x / dx)
                            else down := int(-x / dx) + 1;
                          if (y / dy < 0) and (int(y / dy) * dy <> y)
                            then up := int(y / dy) - 1
                            else up := int(y / dy);
                          if up - down + 1 > 0
                            then answer := up - down + 1
                            else answer := -1;
                      end;
           end;
end;

procedure out;
begin
    if answer = -1
      then writeln('Impossible')
      else if answer = -2
             then writeln('Infinitely many solutions')
             else writeln(answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
