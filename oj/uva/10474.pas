Const
    InFile     = 'p10474.in';
    OutFile    = 'p10474.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    Cases , p ,
    where ,
    N , K      : longint;

function init : boolean;
var
    i          : longint;
begin
    read(N , K);
    inc(cases);
    if N = 0 then exit(false);
    init := true;
    for i := 1 to N do read(data[i]);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure binary_find(p : longint; var where : longint);
var
    start , stop ,
    mid        : longint;
begin
    start := 1; stop := N; where := -1;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if data[mid] >= p
            then begin
                     stop := mid - 1;
                     if data[mid] = p then where := mid;
                 end
            else start := mid + 1;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while init do
        begin
            qk_sort(1 , N);
            writeln('Case# ' , cases , ':');
            while K > 0 do
              begin
                  readln(p);
                  binary_find(p , where);
                  if where = -1
                    then writeln(p , ' not found')
                    else writeln(p , ' found at ' , where);
                  dec(K);
              end;
        end;
//    Close(INPUT);
End.