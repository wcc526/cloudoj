Const
    InFile     = 'p10732.in';
    OutFile    = 'p10732.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of double;

Var
    data       : Tdata;
    answer ,
    N , Cases  : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(N);
    if N = 0
      then exit(false)
      else begin
               fillchar(data , sizeof(data) , 0);
               for i := 1 to N do read(data[i]);
               inc(Cases);
               exit(true);
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : double;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function binary_find(start , stop : longint; p : double; order : boolean) : longint;
var
    mid , res  : longint;
begin
    if order then res := stop + 1 else res := start - 1;
    order := not order;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if (data[mid] > p) xor order then res := mid;
          if data[mid] > p
            then stop := mid - 1
            else start := mid + 1;
      end;
    exit(res);
end;

function divide(a , b : double) : double;
begin
    if b = 0
      then exit(1e20)
      else exit(a / b);
end;

procedure work;
var
    i ,
    bigger , smaller ,
    p          : longint;
begin
    qk_sort(1 , N); answer := 0;
    for i := 1 to N do
      begin
          if 2 * data[i] * (1 - sqr(data[i])) > 0 then
            dec(answer);
          bigger := binary_find(1 , N , -data[i] , true);
          smaller := binary_find(1 , N , -data[i] , false);
          if N >= bigger then
            begin
                p := binary_find(bigger , N , divide(1 , data[i]) , false xor (data[i] < 0));
                if data[i] < 0
                  then inc(answer , N - p + 1)
                  else inc(answer , p - bigger + 1);
            end;
          if smaller >= 1 then
            begin
                p := binary_find(1 , smaller , divide(1 , data[i]) , true xor (data[i] < 0));
                if data[i] < 0
                  then inc(answer , p)
                  else inc(answer , smaller - p + 1);
            end;
      end;
end;

procedure out;
begin
    writeln('Case ' , Cases , ': ' , answer div 2);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    Cases := 0;
    while init do
      begin
          work;
          out;
      end;
End.
