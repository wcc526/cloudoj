Var
    a          : array[1..4] of double;
    tot, cases : longint;
Begin
    readln(cases);
    while Cases > 0 do
      begin
          dec(Cases);
          tot := 0;
          while not eoln do
            begin
                inc(tot); read(a[tot]);
            end;
          readln;
          if tot = 3
            then begin
                     if abs(a[3]) <= 1e-6
                       then writeln(a[2] * pi * a[1] * a[1] / 2 : 0 : 3)
                       else writeln(a[2] * (arctan(sqrt(a[1] * a[1] - a[3] * a[3]) / a[3]) * a[1] * a[1] - sqrt(a[1] * a[1] - a[3] * a[3]) * a[3]) : 0 : 3);
                 end
            else begin
                     a[3] := a[1] / 2 - abs(a[3]); a[4] := a[1] / 2 - abs(a[4]);
                     writeln(a[3] * a[4] * 2 * a[2] : 0 : 3);
                 end;
      end;
End.