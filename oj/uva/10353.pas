Const
    K1         = 2.99935539946156340;
    K2         = 3.70940009497925630;

Var
    N , i      : longint;
    R          : extended;

Begin
    readln(N);
    for i := 1 to N do
      begin
          readln(R);
          writeln(R * K1 : 0 : 12 , ' ' , R * K2 : 0 : 12);
      end;
End.