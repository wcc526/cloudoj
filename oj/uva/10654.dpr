{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10654.in';
    OutFile    = 'p10654.out';
    Limit      = 100;
    names      : array[0..7] of string
               = ('NNN' , 'NNY' , 'NYN' , 'NYY' , 'YNN' , 'YNY' , 'YYN' , 'YYY');

Type
    Tdata      = array[1..Limit + 1 , 0..7] of longint;

Var
    data , opt : Tdata;
    N , T      : longint;

procedure init;
var
    i , j      : longint;
begin
    read(N);
    for i := 1 to N do
      for j := 0 to 7 do
        read(data[i , j]);
    dec(T);
end;

procedure work;
var
    t1 , t2 ,
    i , j , k ,
    min        : longint;
begin
    for j := 0 to 7 do opt[N + 1 , j] := j;
    for i := N downto 1 do
      for j := 0 to 7 do
        begin
            min := 0;
            for k := 1 to 2 do
              begin
                  t1 := j xor (1 shl min);
                  t2 := j xor (1 shl k);
                  if data[i , opt[i + 1 , t1]] > data[i , opt[i + 1 , t2]] then
                    min := k;
              end;
            opt[i , j] := opt[i + 1 , j xor (1 shl min)];
        end;
end;

procedure out;
begin
    writeln(names[opt[1 , 0]]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
