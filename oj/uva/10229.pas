Const
    InFile     = 'p10229.in';

Type
    Tmatrix    = array[1..2 , 1..2] of int64;

Var
    source ,
    A          : Tmatrix;
    Modulo ,
    N , M      : int64;

procedure init;
begin
    readln(N , M);
    Modulo := 1 shl M;
end;

procedure multi(A , B : Tmatrix; var C : Tmatrix);
var
    i , j , k  : longint;
begin
    fillchar(C , sizeof(C) , 0);
    for i := 1 to 2 do
      for j := 1 to 2 do
        for k := 1 to 2 do
          C[i , j] := (C[i , j] + A[i , k] * B[k , j]) mod Modulo;
end;

procedure work;
var
    power      : Tmatrix;
begin
    fillchar(source , sizeof(source) , 0); source[1 , 2] := 1;
    A[1 , 1] := 0; A[1 , 2] := 1; A[2 , 1] := 1; A[2 , 2] := 1;
    power := A;
    while N <> 0 do
      begin
          if odd(N) then multi(source , power , source);
          multi(power , power , power);
          N := N div 2;
      end;
end;

Begin
    while not eof do
      begin
          init;
          work;
          writeln(source[1 , 1]);
      end;
End.