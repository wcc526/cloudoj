#include <stdio.h>
#include <string.h>

#define MAXN 30000
#define MAXM 200000

struct Edge {
	int v;
	Edge *link;
}edge[MAXM], *adj[MAXN];

int totE, top, currT;
int dfn[MAXN], lowlink[MAXN], st[MAXN], f[MAXN], din[MAXN], dout[MAXN], total;
bool inStack[MAXN];

void dfs(int u) {
	dfn[u] = lowlink[u] = ++currT;
	st[top++] = u;
	inStack[u] = true;
	Edge *p = adj[u];
	while(p) {
		if(!dfn[p->v]) {
			dfs(p->v);
			if(lowlink[p->v] < lowlink[u]) lowlink[u] = lowlink[p->v];
		}
		else if(inStack[p->v] && dfn[p->v] < lowlink[u]) lowlink[u] = dfn[p->v];
		p = p->link;
	}
	if(dfn[u] != lowlink[u]) return;
	while(true) {
		int v = st[--top];
		inStack[v] = false;
		f[v] = u;
		if(v == u) break;
	}
	++total;
}

void addEdge(int u, int v) {
	Edge *p = &edge[totE++];
	p->v = v; p->link = adj[u]; adj[u] = p;
}

int main() {
	int cases;
	scanf("%d", &cases);
	while(cases--) {
		int n, m;
		totE = currT = total = 0;
		memset(adj, NULL, sizeof(adj));
		memset(din, 0, sizeof(din));
		memset(dout, 0, sizeof(dout));
		memset(dfn, 0, sizeof(dfn));
		scanf("%d%d", &n, &m);
		for(int i = 0; i < m; ++i) {
			int u, v;
			scanf("%d%d", &u, &v);
			addEdge(u, v);
		}
		for(int i = 1; i <= n; ++i)
			if(!dfn[i]) dfs(i);
		if(total == 1) {
			puts("0");
			continue;
		}
		for(int u = 1; u <= n; ++u) {
			Edge *p = adj[u];
			while(p) {
				if(f[u] != f[p->v]) {
					++dout[f[u]];
					++din[f[p->v]];
				}
				p = p->link;
			}
		}
		int totIn = 0, totOut = 0;
		for(int i = 1; i <= n; ++i) {
			if(f[i] == i) {
				if(dout[i] == 0) ++totOut;
				if(din[i] == 0) ++totIn;
			}
		}
		int res = totIn > totOut? totIn: totOut;
		printf("%d\n", res);
	}
	return 0;
}
