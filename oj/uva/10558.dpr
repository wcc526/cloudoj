{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10558.in';
    OutFile    = 'p105581.out';
    Limit      = 100;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tinterval  = array[1..Limit , 1..Limit] of longint;
    Topt       = array[1..Limit , 1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tdata      = array[1..Limit] of longint;

Var
    map        : Tmap;
    interval   : Tinterval;
    opt ,
    strategy   : Topt;
    visited    : Tvisited;
    data       : Tdata;
    N , S , A  : longint;

function init : boolean;
var
    i , x , y  : longint;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(interval , sizeof(interval) , 0);
    fillchar(strategy , sizeof(strategy) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(data , sizeof(data) , 0);
    read(N);
    if N = -1
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     read(x , y);
                     map[y , x] := true;
                 end;
               read(S);
               for i := 1 to S do
                 read(data[i]);
               read(A);
           end;
end;

procedure pre_process;
var
    i , j , k ,
    p          : longint;
begin
    for i := 1 to Limit - 1 do
      begin
          fillchar(visited , sizeof(visited) , 0);
          for j := i to Limit - 1 do
            for k := 1 to S - 1 do
              if visited[k]
                then inc(interval[i , j])
                else for p := data[k] to data[k + 1] - 1 do
                       if map[j , p] then
                         begin
                             inc(interval[i , j]);
                             visited[k] := true;
                             break;
                         end;
      end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    pre_process;
    for i := 1 to Limit - 1 do opt[i , 1] := interval[1 , i];
    for i := 2 to Limit - 1 do
      for j := 2 to A - 1 do
        if i >= j then
          for k := j to i do
            if interval[k , i] + opt[k - 1 , j - 1] > opt[i , j]
              then begin
                       strategy[i , j] := k;
                       opt[i , j] := interval[k , i] + opt[k - 1 , j - 1];
                   end;
end;

procedure out;
var
    path       : Tdata;
    i , p      : longint;
begin
    p := Limit - 1;
    for i := A - 1 downto 1 do
      begin
          path[i + 1] := p + 1;
          p := strategy[p , i] - 1;
      end;
    path[1] := 1;
    write(A);
    for i := 1 to A do
      write(' ' , path[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
