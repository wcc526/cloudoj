{$I-}
Const
    InFile     = 'p10382.in';
    Limit      = 10000;
    minimum    = 1e-6;

Type
    Tkey       = record
                     a , b    : double;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    N , answer : longint;
    L , W      : double;

procedure init;
var
    i          : longint;
    x , R ,
    delta      : double;
begin
    readln(N , L , W);
    for i := 1 to N do
      begin
          readln(x , R);
          delta := sqr(R) - sqr(W / 2);
          if delta <= minimum then delta := 0 else delta := sqrt(delta);
          data[i].a := x - delta;
          data[i].b := x + delta;
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].a > key.a) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].a < key.a) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i          : longint;
    last ,
    newlast    : double;
begin
    qk_sort(1 , N);
    last := 0; newlast := 0; i := 1; answer := 0;
    while (i <= N) and (last < L - minimum) do
      begin
          if data[i].a > last + minimum then begin answer := -1; exit; end;
          while (i <= N) and (data[i].a <= last + minimum) do
            begin
                if data[i].b > newlast then newlast := data[i].b;
                inc(i);
            end;
          last := newlast;
          inc(answer);
      end;
    if last < L - minimum then answer := -1;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            if N = 0 then break;
            work;
            writeln(answer);
        end;
//    Close(INPUT);
End.