Const
    InFile     = 'p10729.in';
    Limit      = 200;

Type
    Tmap       = array[1..Limit] of
                   record
                       ch     : char;
                       tot    : longint;
                       position ,
                       data   : array[1..Limit] of longint;
                   end;
    Tstr       = string[Limit];

Var
    map        : Tmap;
    s1 , s2    : Tstr;
    N , T      : longint;
    answer     : boolean;

procedure init;
var
    i          : longint;
begin
    dec(T);
    fillchar(map , sizeof(map) , 0);
    readln(s1);
    readln(s2);
    i := 1;
    while i <= length(s1) do begin if s1[i] = ',' then delete(s1 , i , 1); inc(i); end;
    i := 1;
    while i <= length(s2) do begin if s2[i] = ',' then delete(s2 , i , 1); inc(i); end;
end;

procedure dfs_build_tree(var p : longint; father : longint);
var
    now        : longint;
begin
    inc(N); map[N].ch := s1[p]; now := N;
    if father <> 0 then
      begin
          inc(map[N].tot); map[N].data[1] := father; map[N].position[1] := map[father].tot + 1;
          inc(map[father].tot); map[father].data[map[father].tot] := N;
          map[father].position[map[father].tot] := 1;
      end;
    inc(p); if p > length(s1) then exit;
    if s1[p] = '('
      then begin
               inc(p);
               while s1[p] <> ')' do
                 dfs_build_tree(p , now);
               inc(p);
           end;
end;

procedure dfs_print(p , start , count : longint; var s : Tstr);
var
    i          : longint;
begin
    s := s + map[p].ch;
    if count = 0 then exit;
    s := s + '(';
    for i := 1 to count do
      begin
          start := start mod map[p].tot + 1;
          dfs_print(map[p].data[start] , map[p].position[start] , map[map[p].data[start]].tot - 1 , s);
      end;
    s := s + ')';
end;

procedure work;
var
    p , i , j  : longint;
    s          : Tstr;
begin
    N := 0;
    answer := true;
    p := 1;
    dfs_build_tree(p , 0);
    if s1 = s2 then exit;
    for i := 1 to N do
      for j := 1 to map[i].tot do
        begin
            s := '';
            dfs_print(i , j , map[i].tot , s);
            if s = s2 then exit;
        end;
    answer := false;
end;

procedure out;
begin
    if answer
      then writeln('same')
      else writeln('different');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.