Const
    InFile     = 'p10317.in';
    OutFile    = 'p10317.out';
    Limit      = 16;
    LimitValue = 1600;

Type
    Tsign      = array[1..16] of longint;
    Tdata      = array[1..16] of longint;
    Topt       = array[0..Limit , 0..Limit , -LimitValue..LimitValue] of longint;

Var
    sign       : Tsign;
    answer ,
    stack ,
    data       : Tdata;
    opt        : Topt;
    equa , sum ,
    N , tot    : longint;

procedure init;
var
    K , i ,
    reverse    : longint;
    ch         : char;
begin
    reverse := 1; read(data[1]); sign[1] := 1; N := 1;
    while not eoln do
      begin
          repeat if eoln then break; read(ch); until ch <> ' ';
          if eoln then break;
          K := 1;
          if ch = '=' then reverse := -reverse;
          if ch = '-' then K := -1;
          inc(N);
          sign[N] := K * reverse; read(data[N]);
          if ch = '=' then equa := N;
      end;
    readln;
    tot := 0;
    for i := 1 to N do
      if sign[i] = 1 then
        inc(tot);
    sum := 0;
    for i := 1 to N do
      inc(sum , data[i]);
end;

procedure print;
var
    i ,
    p1 , p2    : longint;
begin
    p1 := 1; p2 := 1;
    for i := 1 to N do
      if stack[i] = 1
        then begin
                 while sign[p1] <> 1 do inc(p1);
                 answer[p1] := data[i];
                 inc(p1);
             end
        else begin
                 while sign[p2] <> -1 do inc(p2);
                 answer[p2] := data[i];
                 inc(p2);
             end;
    write(answer[1]);
    for i := 2 to equa - 1 do
      begin
          if sign[i] = 1 then write(' + ') else write(' - ');
          write(answer[i]);
      end;
    write(' = ' , answer[equa]);
    for i := equa + 1 to N do
      begin
          if sign[i] = -1 then write(' + ') else write(' - ');
          write(answer[i]);
      end;
    writeln;
end;

function dfs(last , step , count : longint) : boolean;
begin
    if count > tot then exit(false);
    if step > N then
      begin
          if (last = 0) and (count = tot)
            then begin print; dfs := true; end
            else dfs := false;
          exit;
      end;
    stack[step] := 1;
    if dfs(last + data[step] , step + 1 , count + 1) then exit(true);
    stack[step] := -1;
    if dfs(last - data[step] , step + 1 , count) then exit(true);
    exit(false);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            if not odd(sum) and dfs(0 , 1 , 0)
              then
              else writeln('no solution');
        end;
//    Close(INPUT);
End.