Type
    TLine      = record
                     A , B , C               : extended;
                 end;

Var
    s1 , s2 , s3
               : string[255];
    L1 , L2    : TLine;
    b1 , b2    : boolean;
    x0 , y0 , degree ,
    x1 , y1 , x2 , y2 ,
    x3 , y3 , x4 , y4
               : extended;

function zero(number : extended) : boolean;
begin
    exit(abs(number) <= 1e-8);
end;

procedure Get_Line(x1 , y1 , x2 , y2 : extended; var L : TLine);
begin
    L.A := 2 * (x2 - x1); L.B := 2 * (y2 - y1);
    L.C := x1 * x1 + y1 * y1 - x2 * x2 - y2 * y2;
end;

procedure Get_Line1(x1 , y1 , x2 , y2 : extended; var L : TLine);
begin
    L.A := y1 - y2; L.B := x2 - x1;
    L.C := x1 * y2 - x2 * y1;
end;

function Get_Crossing(L1 , L2 : TLine; var x , y : extended) : boolean;
var
    tmp        : extended;
begin
    tmp := L1.A * L2.B - L2.A * L1.B;
    if zero(tmp) then exit(false);
    Get_Crossing := true;
    x := -(L1.C * L2.B - L1.B * L2.C) / tmp;
    y := -(L1.A * L2.C - L1.C * L2.A) / tmp;
end;

function arccos(num : extended) : extended;
begin
    if zero(num)
      then exit(pi / 2)
      else if num < 0
             then arccos := pi - arccos(-num)
             else arccos := arctan(sqrt(1 - num * num) / num);
end;

function rotation(x0 , y0 , x1 , y1 , x2 , y2 : extended) : extended;
var
    a , b , c ,
    res        : extended;
begin
    x1 := x1 - x0; y1 := y1 - y0;
    x2 := x2 - x0; y2 := y2 - y0;
    a := sqrt(sqr(x1) + sqr(y1)); b := sqrt(sqr(x2) + sqr(y2));
    c := sqrt(sqr(x1 - x2) + sqr(y1 - y2));
    res := arccos((a * a + b * b - c * c) / (2 * a * b));
    if x1 * y2 - x2 * y1 < 0 then res := 2 * pi - res;
    res := res / (2 * pi) * 360;
    exit(res);
end;

Begin
    readln(x1 , y1 , x2 , y2 , x3 , y3 , x4 , y4);
    while not zero(x1) or not zero(y1) or not zero(x2) or not zero(y2) or
        not zero(x3) or not zero(y3) or not zero(x4) or not zero(y4) do
      begin
          Get_Line(x1 , y1 , x2 , y2 , L1);
          Get_Line(x3 , y3 , x4 , y4 , L2);
          b1 := zero(x1 - x2) and zero(y1 - y2);
          b2 := zero(x3 - x4) and zero(y3 - y4);
          if b1 and b2
            then writeln('No move.')
            else if b1 or b2 or Get_Crossing(L1 , L2 , x0 , y0)
                   then begin
                            if b1 or b2
                              then if b1
                                     then begin
                                              x0 := x1; y0 := y1;
                                              degree := rotation(x0 , y0 , x3 , y3 , x4 , y4);
                                          end
                                     else begin
                                              x0 := x3; y0 := y3;
                                              degree := rotation(x0 , y0 , x1 , y1 , x2 , y2);
                                          end
                              else degree := rotation(x0 , y0 , x1 , y1 , x2 , y2);
                            str(x0 : 0 : 1 , s1); if s1 = '-0.0' then s1 := '0.0';
                            str(y0 : 0 : 1 , s2); if s2 = '-0.0' then s2 := '0.0';
                            str(degree : 0 : 1 , s3); if s3 = '-0.0' then s3 := '0.0';
                            writeln('Rotation around (' , s1 , ',' , s2 , ') by ' , s3 , ' degrees counterclockwise.');
                        end
                   else begin
                            if zero(x2 - x1 + x3 - x4) and zero(y2 - y1 + y3 - y4)
                              then begin
                                       str(x2 - x1 : 0 : 1 , s1); if s1 = '-0.0' then s1 := '0.0';
                                       str(y2 - y1 : 0 : 1 , s2); if s2 = '-0.0' then s2 := '0.0';
                                       writeln('Translation by vector <' , s1 , ',' , s2 , '>.')
                                   end
                              else begin
                                       Get_Line1(x1 , y1 , x3 , y3 , L1);
                                       Get_Line1(x2 , y2 , x4 , y4 , L2);
                                       if not Get_Crossing(L1 , L2 , x0 , y0) then
                                         begin x0 := (x1 + x2) / 2; y0 := (y1 + y2) / 2; end;
                                       degree := rotation(x0 , y0 , x1 , y1 , x2 , y2);
                                       str(x0 : 0 : 1 , s1); if s1 = '-0.0' then s1 := '0.0';
                                       str(y0 : 0 : 1 , s2); if s2 = '-0.0' then s2 := '0.0';
                                       str(degree : 0 : 1 , s3); if s3 = '-0.0' then s3 := '0.0';
                                       writeln('Rotation around (' , s1 , ',' , s2 , ') by ' , s3 , ' degrees counterclockwise.');
                                   end;
                        end;
          readln(x1 , y1 , x2 , y2 , x3 , y3 , x4 , y4);
      end;
End.
