Var
    sinA , cosA ,
    sinB , cosB ,
    sinC , cosC ,
    R , D ,
    H1 , H2    : extended;
    i , T      : longint;
Begin
    readln(T);
    for i := 1 to T do
      begin
          readln(R , D , H1);
          cosC := (R - D) / R; sinC := sqrt(abs(1 - sqr(cosC)));
          sinA := 2 * sinC * cosC; cosA := sqr(cosC) - sqr(sinC);
          cosB := 1 - H1 / R; sinB := sqrt(abs(1 - sqr(cosB)));
          H2 := cosA * cosB + sinA * sinB;
          writeln('Case ' , i , ': ' , R * (1 - H2) : 0 : 4);
      end;
End.