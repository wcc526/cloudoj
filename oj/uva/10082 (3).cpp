/**
 * UVa 10082 WERTYU
 * Author: chchwy
 * Last Modified: 2010.04.21
 */
#include<cstdio>

int main(){

    char key[] = "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./";

    //set mapping
    char map[256];
    for(int i=0; key[i]!=NULL; ++i)
        map[ key[i] ] = key[i-1];
    map[' '] = ' ';
    map['\n'] = '\n';

	//run
    int c;
    while((c=getchar())!=EOF)
        putchar(map[c]);
        
    return 0;
}
