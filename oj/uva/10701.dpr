{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10701.in';
    OutFile    = 'p10701.out';

Var
    cases      : longint;
    s1 , s2    : string;

procedure init;
var
    num , i    : longint;
    c          : char;
begin
    dec(Cases);
    read(num);
    s1 := ''; s2 := '';
    read(c); for i := 1 to num do begin read(c); s1 := s1 + c; end;
    read(c); for i := 1 to num do begin read(c); s2 := s2 + c; end;
    readln;
end;

procedure workout(pre_ord , in_ord : string);
var
    p          : longint;
begin
    if length(pre_ord) <= 1
      then write(pre_ord)
      else begin
               p := pos(pre_ord[1] , in_ord);
               workout(copy(pre_ord , 2 , p - 1) , copy(in_ord , 1 , p - 1));
               workout(copy(pre_ord , p + 1 , length(pre_ord) - p) , copy(in_ord , p + 1 , length(in_ord)));
               write(pre_ord[1]); 
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout(s1 , s2);
            writeln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
