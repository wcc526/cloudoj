{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10607.in';
    OutFile    = 'p10607.out';
    LimitN     = 200;

Type
    Tmap       = array[0..LimitN + 1 , 0..LimitN + 1] of char;
    Tdata      = array[#32..#255 , #32..#255] of boolean;
    Tqueue     = array[1..255] of char;
    Tvisited   = array[#32..#255] of longint;
    Tappeared  = array[#32..#255] of boolean;

Var
    map        : Tmap;
    data       : Tdata;
    queue      : Tqueue;
    visited    : Tvisited;
    appeared   : Tappeared;
    N , M ,
    answer     : longint;

function init : boolean;
var
    i , j      : longint;
begin
    fillchar(map , sizeof(map) , #32);
    fillchar(data , sizeof(data) , 0);
    fillchar(appeared , sizeof(appeared) , 0);
    readln(N , M);
    if N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     for j := 1 to M do
                       begin
                           read(map[i , j]);
                           appeared[map[i , j]] := true;
                       end;
                     readln;
                 end;
           end;
end;

procedure work;
var
    i , j ,
    open , closed
               : longint;
    c1 , c2    : char;
begin
    for i := 0 to N do
      for j := 0 to M do
        begin
            data[map[i , j] , map[i + 1 , j]] := true; data[map[i + 1 , j] , map[i , j]] := true;
            data[map[i , j] , map[i , j + 1]] := true; data[map[i , j + 1] , map[i , j]] := true;
        end;
        
    fillchar(visited , sizeof(visited) , $FF);
    fillchar(queue , sizeof(queue) , 0);
    open := 1; closed := 1; visited[#32] := 0; queue[1] := #32;
    while open <= closed do
      begin
          c1 := queue[open];
          for c2 := #32 to #255 do
            if (visited[c2] = -1) and data[c1 , c2] then
              begin
                  visited[c2] := visited[c1] + 1;
                  if c2 <> #65 then
                    begin
                        inc(closed);
                        queue[closed] := c2;
                    end;
              end;
          inc(open);
      end;
    answer := -1;
    for c1 := #33 to #255 do
      if appeared[c1] and (visited[c1] = -1) then
        exit;
    if visited[#65] = -1 then exit;
    answer := visited[#65] - 2;
    for c1 := #32 to #255 do
      if data[#65 , c1] and (c1 <> #65) then
        inc(answer);
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
