Var
    a , b , N  : extended;
Begin
    readln(N);
    while N >= 0 do
      begin
          b := 4;
          a := N * 2 + 4;
          if N = 1
            then writeln(0 , '%')
            else writeln(N * 25 : 0 : 0 , '%');
          readln(N);
      end;
End.