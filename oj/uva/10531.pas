{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10531.in';
    OutFile    = 'p10531.out';
    LimitN     = 6;
    LimitM     = 5;

Type
    Tdata      = array[0..LimitN , 0..1 shl LimitN - 1 , 0..4 , 1..3] of longint;
    Tmap       = array[0..LimitM * LimitM , 0..LimitM * LimitM] of boolean;
    Tnum       = array[1..LimitN] of longint;

Var
    data       : Tdata;
    map        : Tmap;
    N , M ,
    answer     : longint;

procedure init;
begin
    assign(INPUT , InFile); ReSet(INPUT);
      readln(M , N);
    Close(INPUT);
end;

procedure Get_New_Status(k , p1 , p2 , j : longint; var np1 , np2 : longint);
var
    num1 , num2
               : Tnum;
    a , b      : array[1..3] of longint;
    i , ta , tb: longint;
begin
    fillchar(num1 , sizeof(num1) , 0);
    fillchar(num2 , sizeof(num2) , 0);
    for i := 1 to M do
      begin
          num1[i] := ord(k and (1 shl (i - 1)) <> 0);
          num2[i] := ord(j and (1 shl (i - 1)) <> 0);
      end;

    ta := 0; a[1] := 0; a[2] := 0; a[3] := 0;
    tb := 0; b[1] := 0; b[2] := 0; b[3] := 0;
    for i := M downto 1 do
      begin
          if (num1[i] = 0) and ((i = M) or (num1[i + 1] <> 0)) then
            begin inc(ta); a[ta] := i; end;
          if (num2[i] = 0) and ((i = M) or (num2[i + 1] <> 0)) then
            begin inc(tb); b[tb] := i + M; end;
      end;
    fillchar(map , sizeof(map) , 0);
    for i := 1 to M do
      if (num1[i] = 0) and (num2[i] = 0) then
        begin map[i , i + M] := true; map[i + M , i] := true; end;
    for i := 2 to M do
      begin
          if (num1[i] = 0) and (num1[i - 1] = 0) then begin map[i , i - 1] := true; map[i - 1 , i] := true; end;
          if (num2[i] = 0) and (num2[i - 1] = 0) then begin map[i + M , i + M - 1] := true; map[i + M - 1 , i + M] := true; end;
      end;
    case p1 of
      0        : ;
      1        : begin map[a[1] , a[2]] := true; map[a[2] , a[1]] := true; end;
      2        : begin map[a[2] , a[3]] := true; map[a[3] , a[2]] := true; end;
      3        : begin map[a[1] , a[3]] := true; map[a[3] , a[1]] := true; end;
      4        : begin
                     map[a[1] , a[2]] := true; map[a[2] , a[1]] := true;
                     map[a[2] , a[3]] := true; map[a[3] , a[2]] := true;
                 end;
    end;

    for k := 1 to M * 2 do
      for i := 1 to M * 2 do
        for j := 1 to M * 2 do
          if map[i , k] and map[k , j] then
            map[i , j] := true;
    np2 := 1;
    while (np2 <= tb) and not map[a[p2] , b[np2]] do inc(np2);
    if np2 > tb then begin np2 := 0; exit; end;
    np1 := 0;
    if map[b[1] , b[2]] then np1 := 1;
    if map[b[2] , b[3]] then np1 := 2;
    if map[b[1] , b[3]] then np1 := 3;
    if map[b[1] , b[2]] and map[b[2] , b[3]] then np1 := 4;
end;

procedure work;
var
    i , j , k ,
    p1 , p2 ,
    np1 , np2  : longint;
begin
    fillchar(data , sizeof(data) , 0);
    data[0 , 1 shl M - 2 , 0 , 1] := 1;
    for i := 1 to N do
      begin
          for k := 0 to 1 shl M - 2 do
            for p1 := 0 to 4 do
              for p2 := 1 to 3 do
                if data[i - 1 , k , p1 , p2] > 0 then
                  for j := 0 to 1 shl M - 2 do
                    begin
                        Get_New_Status(k , p1 , p2 , j , np1 , np2);
                        if np2 <> 0 then
                          inc(data[i , j , np1 , np2] , data[i - 1 , k , p1 , p2]);
                    end;
      end;

    answer := 0;
    for i := 0 to 1 shl M - 2 do
      for k := 0 to 4 do
        if i < 1 shl (M - 1) then
          if data[N , i , k , 1] <> 0 then
            inc(answer , data[N , i , k , 1]);
end;

procedure out;
begin
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      writeln(answer);
    Close(OUTPUT);
end;

Begin
    init;
    work;
    out;
End.
