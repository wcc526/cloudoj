Const
    maxn=400;
    maxm=2500;
    minimum=1e-8;

Type
    Tp=record
           x,y:extended;
       end;
    Tdata=record
              a,b:extended;
          end;

Var
    test,n,d,ans:longint;
    point:array[1..maxn]of Tp;
    data,data1:array[1..maxn]of Tdata;

function zero(p:extended):boolean;
begin
    exit(abs(p)<=minimum);
end;

function geta(x,y:extended):extended;
var t:extended;
begin
    if y=0 then begin
        if x>0 then geta:=0 else geta:=pi;
        exit;
    end;
    if x=0 then begin
        if y>0 then geta:=pi/2 else geta:=pi*3/2;
        exit;
    end;
    t:=arctan(abs(y/x));
    if y>0 then begin
        if x>0 then geta:=t else geta:=pi-t;
    end
    else begin
        if x>0 then geta:=pi*2-t else geta:=pi+t;
    end;
end;


procedure init;
var i,a,b,t,j:longint;
    dx,dy,tmp,agl:extended;
    f:boolean;
begin
    read(t,d);
    n:=0;
    for i:=1 to t do
      begin
          read(a,b);
          if a*a+b*b>d*d then begin
              inc(n);
              with point[n] do
                begin
                    x:=a; y:=b;
                    tmp:=geta(x,y);
                    agl:=arctan(d/sqrt(x*x+y*y-d*d));

                    data[n].a:=tmp+agl;
                    data[n].b:=tmp-agl;
                    if data[n].a>=pi*2 then data[n].a:=data[n].a-pi*2;
                    if data[n].b<0 then data[n].b:=data[n].b+pi*2;

                    if data[n].a>data[n].b then begin
                        tmp:=data[n].a;
                        data[n].a:=data[n].b;
                        data[n].b:=tmp;
                    end;
                    if data[n].a+pi<data[n].b then begin
                        tmp:=data[n].a;
                        data[n].a:=data[n].b;
                        data[n].b:=tmp+pi*2;
                    end;
                end;
          end;
      end;
end;

function bigger(var i,j:Tdata):boolean;
begin
    bigger:=true;
    if zero(i.a-j.a)
      then if (i.b>j.b) then exit else
      else if i.a>j.a then exit;
    bigger:=false;
end;

procedure sort(h,t:longint);
var mid:Tdata;
    i,j:longint;
begin
    if h<t then begin
        i:=h; j:=t;
        mid:=data[(i+j) div 2];
        data[(i+j) div 2]:=data[i];
        while (i<j) do
          begin
              while (i<j) and bigger(data[j],mid) do dec(j);
              data[i]:=data[j];
              if i<>j then inc(i);
              while (i<j) and bigger(mid,data[i]) do inc(i);
              data[j]:=data[i];
              if i<>j then dec(j);
          end;
        data[i]:=mid;
        sort(h,i-1); sort(i+1,t);
    end;
end;

procedure work;
var i,j,first,count,t:longint;
    last:extended;
    f:boolean;
begin
    t:=n; n:=0;
    for i:=1 to t do
      if (i=1) OR (not zero(data[i].a-data[n].a) or not zero(data[i].b-data[n].b))
      then begin
          inc(n); data[n]:=data[i];
      end;
    t:=n; n:=0;
    data1:=data;
    for i:=1 to t do
      begin
          f:=true;
          for j:=1 to t do
            if i<>j then begin
                if (data[i].a<=data1[j].a+minimum) and (data[i].b+minimum>=data1[j].b)
                then f:=false;
                if (data[i].a-pi*2<=data1[j].a+minimum) and
                   (data[i].b-pi*2>=data1[j].b-minimum)
                then f:=false;
            end;
          if f then begin
              inc(n);
              data[n]:=data[i];
          end;
      end;
    for i:=1 to n-1 do
      begin
          data[i+n].a:=data[i].a+pi*2;
          data[i+n].b:=data[i].b+pi*2;
      end;

    ans:=n;
    for first:=1 to n do
      begin
         count:=1; last:=data[first].b;
         for i:=1 to n-1 do
           begin
               if (last<data[i+first].a) and not zero(last-data[i+first].a) then begin
                   last:=data[i+first].b;
                   inc(count);
               end;
           end;
         if count<ans then ans:=count;
      end;
end;

Begin
    read(test);
    while (test>0) do
      begin
          init;
          sort(1,n);
          work;
          writeln(ans);
          dec(test);
      end;
End.
