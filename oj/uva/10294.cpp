/*
 * uva_10294.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
#include <cstdio>
#include <cstring>
typedef long long LL;
int main()
{
	int n,t;
	while(~scanf("%d%d",&n,&t)&&n)
	{
		LL pow[MXN];
		pow[0]=1;
		for(int i=1;i<=n;++i)
			pow[i]=pow[i-1]*t;
		LL a=0;
		for(int i=0;i<n;++i)
			a+=pow[gcd(i,n)];
		LL b=0;
		if(n%2==1) b=n*pow[(n+1)/2];
		else b=n/2*(pow[n/2+1]+pow[n/2]);
		printf("%lld %lld\n",a/n,(a+b)/2/n);
	}
	return 0;
}






