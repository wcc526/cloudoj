#include<cstdio>
#include<cstring>
const int N=2222;
int ev[N],nxt[N],head[N],e;
int vis[N],ans[N],cnt;
void init()
{
    memset(vis,0,sizeof(vis));
    memset(head,-1,sizeof(head));
    cnt=e=0;
}
void add(int u,int v)
{
    ev[e]=v,nxt[e]=head[u];head[u]=e++;
}
void dfs(int u)
{
    for(int i=head[u];~i;i=nxt[i]) if(!vis[i])
    {
        vis[i]=vis[i^1]=1;
        dfs(ev[i]);
    }
    ans[cnt++]=u;
}
int main()
{
    int t,n,u,v,cas=0;
    scanf("%d",&t);
    while(t--)
    {
        init();
        scanf("%d",&n);
        for(int i=0;i<n;i++)
        {
            scanf("%d%d",&u,&v);
            add(u,v);
            add(v,u);
        }
        dfs(u);
        if(cas) puts("");
        printf("Case #%d\n",++cas);
        if(ans[0]==ans[cnt-1]) for(int i=1;i<cnt;i++) printf("%d %d\n",ans[i-1],ans[i]);
        else puts("sonme beads may be lost");
    }
    return 0;
}
