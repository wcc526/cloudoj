#include <cstdio>
#include <vector>
#include <cmath>
#include <cstdlib>
using namespace std;
const double EPS=1e-6;
const int MXN=1000;
int vis[MXN][MXN];
struct Point3 {
	double x, y, z;
	Point3(double x = 0, double y = 0, double z = 0) :
			x(x), y(y), z(z) {
	}
};
typedef Point3 Vector3;
Vector3 Cross(Vector3 A, Vector3 B) {
	return Vector3(A.y * B.z - A.z * B.y, A.z * B.x - A.x * B.z,
			A.x * B.y - A.y * B.x);
}
double Dot(Vector3 A, Vector3 B) {
	return A.x * B.x + A.y * B.y + A.z * B.z;
}
struct Face {
	int v[3];
	Vector3 normal(Point3 *P) const {
		return Cross(P[v[1]] - P[v[0]], P[v[2]] - P[v[0]]);
	}
	int cansee(Point3* P, int i) const {
		return Dot(P[i] - P[v[0]], normal(P)) > 0 ? 1 : 0;
	}
};
vector<Face> CH3D(Point3 P[], int n) {
	vector<Face> cur;
	cur.push_back((Face){{0,1,2}});
	cur.push_back((Face){{2,1,0}});
	for(int i=3;i<n;++i)
	{
		vector<Face> next;
		for(int j=0;j<cur.size();++j)
		{
			Face& f=cur[j];
			int res=f.cansee(P,i);
			if(!res) next.push_back(f);
			for(int k=0;k<3;++k)
				vis[f.v[k]][f.v[(k+1)%3]]=res;
		}
		for(int j=0;j<cur.size();++j)
			for(int k=0;k<3;++k)
			{
				int a=cur[j].v[k],b=cur[j].v[(k+1)%3];
				if(vis[a][b]!=vis[b][a]&&vis[a][b])
					next.push_back((Face){{a,b,i}});
			}
		cur=next;
	}
	return cur;
}
double rand01()
{
	return rand()/(double)RAND_MAX;
}
double randeps()
{
	return (rand01()-0.5)*EPS;
}
Point3 add_noise(Point3 p)
{
	return Point3(p.x+randeps(),p.y+randeps(),p.x+randeps());
}
bool TriSetInter(Point3 P0,Point3 P1,Point3 P2,Point3 A,Point3 B,Point3 &P)
{
	Vector3 n=Cross(P1-P0,P2-P0);
	if(dcmp(Dot(n,B-A))==0) return 0;
	else
	{
		double t=Dot(n,P0-A)/Dot(n,B-A);
		if(dcmp(t)<0||dcmp(t-1)>0) return 0;
		P=A+(B-A)*t;
		return PointInTri(P,P0,P1,P2);
	}
}
bool TriTriInter(Point3 T1[],Point3 T2[])
{
	Point3 P;
	for(int i=0;i<3;++i)
	{
		if(TriSetInter(T1[0],T1[1],T1[2],T2[i],T2[(i+1)%3],P)) return 1;
		if(TriSetInter(T2[0],T2[1],T2[2],T1[i],T1[(i+1)%3],P)) return 1;
	}
	return 0;
}
int main()
{
	return 0;
}

