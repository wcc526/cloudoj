Const
    InFile     = 'p10461.in';
    OutFile    = 'p10461.out';
    Limit      = 500;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tdata      = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    map        : Tmap;
    data       : Tdata;
    visited    : Tvisited;
    sum1 , sum2 ,
    sum , cases ,
    N , M      : longint;

procedure init;
var
    i , u , v  : longint;
begin
    fillchar(map , sizeof(map) , 0);
    sum := 0;
    readln(N , M);
    for i := 1 to N do begin read(data[i]); inc(sum , data[i]); end;
    for i := 1 to M do
      begin
          read(u , v);
          map[u , v] := true;
      end;
end;

procedure dfs_positive(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    inc(sum1 , data[p]);
    for i := 1 to N do
      if map[p , i] and not visited[i] then
        dfs_positive(i);
end;

procedure dfs_negative(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    inc(sum2 , data[p]);
    for i := 1 to N do
      if map[i , p] and not visited[i] then
        dfs_negative(i);
end;

procedure workout;
var
    count , i ,
    p          : longint;
begin
    read(count);
    for i := 1 to count do
      begin
          read(p);
          fillchar(visited , sizeof(visited) , 0); sum1 := 0;
          dfs_positive(p);
          fillchar(visited , sizeof(visited) , 0); sum2 := 0;
          dfs_negative(p);
          writeln(sum - sum1 - sum2 + data[p]);
      end;
    readln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 1;
      while true do
        begin
            init;
            if N = 0 then break;
            if cases <> 1 then writeln;
            writeln('Case #' , cases , ':');
            workout;
            inc(cases);
        end;
//    Close(INPUT);
End.