{ $A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q+,R+,S+,T-,U-,V+,W-,X+,Y+,Z1}
{ $MINSTACKSIZE $00004000}
{ $MAXSTACKSIZE $00100000}
Program v10692;

{ $APPTYPE CONSOLE}

Const
  MaxN=10;
  MaxM=10010;
  Zero=1e-6;

Var
  N,M,T                             :Longint;
  Data                              :Array[1..MaxN] Of Longint;

  Procedure Init;
  Var
    I                               :Longint;

    Procedure GetNum(Var X:Longint);
    Var
      C                             :Char;
    Begin
      X:=0;
      Read(C);
      While C=' ' Do Read(C);
      If C='#' Then Begin
        Close(Input);Close(Output);
        Halt;
      End;
      While C<>' ' Do Begin
        X:=X*10+Ord(C)-48;
        Read(C);
      End;
    End;

  Begin
    GetNum(M);
    Read(N);
    For I:=1 To N Do
      Read(Data[I]);
    Readln;
  End;

  Procedure Work;

    Function GetAns(S,M:Longint):Longint;
    Var
      Temp,I,T,P                    :Longint;
      A                             :Array[0..MaxM] Of Longint;
      B                             :Array[0..MaxM] Of Longint;

      Function Max(S,Num:Longint):Boolean;
      Var
        Temp                        :Extended;
        Need                        :Longint;
      Begin
        If Num<=1 Then Begin
          Max:=True;
          Exit;
        End;
        If S=N+1 Then Begin
          Max:=False;
          Exit;
        End;
        If Data[S]=1 Then Begin
          Max:=False;
          Exit;
        End;
        Temp:=Ln(Num)/Ln(Data[S]);
        If Abs(Temp-Round(Temp))<Zero Then Need:=Round(Temp) Else Need:=Trunc(Temp)+1;
        Max:=Max(S+1,Need);
      End;

      Function Count(S:Longint):Longint;
      Var
        I,C                         :Longint;
      Begin
        If S=N+1 Then Begin
          Count:=1;
          Exit;
        End;
        If Data[S]=1 Then Begin
          Count:=1;
          Exit;
        End;
        C:=1;
        For I:=1 To Count(S+1) Do
          C:=C*Data[S];
        Count:=C;
      End;

    Begin
      If S=N+1 Then Begin
        GetAns:=1;
        Exit;
      End;
      Temp:=1;A[0]:=1;
      Fillchar(B,Sizeof(B),$FF);
      B[1]:=0;P:=0;
      For I:=1 To M+1 Do Begin
        Temp:=(Temp*Data[S]) Mod M;
        A[I]:=Temp;
        If B[A[I]]<>-1 Then Begin
          P:=B[A[I]];
          Temp:=I;
          Break;
        End;
        B[A[I]]:=I;
      End;
      If Max(S+1,P) Then Begin
        T:=GetAns(S+1,Temp-P);
        T:=(T-P+(Temp-P)*P) Mod (Temp-P);
        GetAns:=A[T+P];
      End
      Else
        GetAns:=A[Count(S+1)];
    End;

  Begin
    Writeln('Case #',T,': ',GetAns(1,M));
  End;

Begin
//  Assign(Input,'p10692.in');   Reset(Input);
//  Assign(Output,'Output.txt');

//  Rewrite(Output);
  T:=0;
  Repeat
    Inc(T);
    Init;
    Work;
  Until False;
End.
