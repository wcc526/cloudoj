#include <deque>
#include <cstdio>

using namespace std;

int main(void)
{
  deque<int> q;
  int f, n;

  while (scanf("%d", &n) == 1 && n) {
    q.clear();
    do
      q.push_front(n--);
    while (n);
    printf("Discarded cards:");
    f = 1;
    while (q.size() > 1) {
      printf("%s%d", f ? " " : ", ", q[0]);
      q.pop_front();
      q.push_back(q[0]);
      q.pop_front();
      f = 0;
    }
    printf("\nRemaining card: %d\n", q[0]);
  }

  return 0;
}
