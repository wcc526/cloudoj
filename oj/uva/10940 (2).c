#include <stdio.h>

int main(void)
{
  int n, i, c;

  while (scanf("%d", &n) == 1 && n) {
    c = 0;
    i = n;
    while ((i >>= 1) && ++c);
    i = (n - (1 << c)) << 1;
    if (i == 0)
      i = n;
    printf("%d\n", i);
  }

  return 0;
}
