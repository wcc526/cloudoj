/*
 * 从底层（大的）向上（小的）搜索，加几个剪枝：

 当前总体积加上上面最小所能达到的体积，若大于给定体积，剪掉

 当前总体积加上上面最大所能达到的体积，若小于给定体积，剪掉

 最重要的： if(2*(n-v0)/rr+s0>=min)return;

 要证也很容易:根据V/pi=r*r*h,S/pi=2*r*h，不等式化化除一除就能得到那个侧面积的下界。
 */
#include<cstdio>

#include<cmath>

using namespace std;

long n;

int m;

long min;

void search(int l, long v0, long s0, long rr, long hh) { //第几层，目前总体积，目前总表面积（包括底面积）,上一层半径，高度

	if (l == m + 1) {

		if (v0 == n && s0 < min)
			min = s0;

		return;

	}

	if (v0 + (m - l + 1) * (m - l + 1) * (m - l + 2) * (m - l + 2) / 4 > n)
		return;

	long sum = 0;

	for (int i = 1; i <= m - l + 1; i++)
		sum += (rr - i) * (rr - i) * (hh - i);

	if (v0 + sum < n)
		return;

	if (2 * (n - v0) / rr + s0 >= min)
		return; //!!!

	for (long r = rr - 1; r > m - l; r--)

		for (long h = hh - 1; h > m - l; h--) {

			if (l == 1)
				search(l + 1, r * r * h, r * r + 2 * r * h, r, h);

			else
				search(l + 1, v0 + r * r * h, s0 + 2 * r * h, r, h);

		}

}

int main()

{

	int t = 0;

	while (scanf("%ld%d", &n, &m) != EOF) {

		if (n == 0)
			return 0;

		min = 2000000000;

		search(1, 0, 0,
				(long) sqrt((n - m * m * (m - 1) * (m - 1) / 4) * 1.0 / m) + 1,
				(n - m * m * (m - 1) * (m - 1) / 4) / m / m + 1);

		printf("Case %d: %ld\n", ++t, min == 2000000000 ? 0 : min);

	}

	return 0;

}
