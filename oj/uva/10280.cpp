#include <iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int C,n,L[105],U[105],ans,dp[450000],vis[4510],v[20000];

int main()
{
    int t,cas=1;
    scanf("%d",&t);
    while(t--)
    {
        if(cas!=1)
        puts("");
        cas++;
        scanf("%d%d",&C,&n);
        C*=1000;
        int limit=0x3f3f3f3f;
        for(int i=0;i<n;i++)
        {
            scanf("%d%d",&L[i],&U[i]);
            limit=min(limit,L[i]*L[i]/(U[i]-L[i]));
        }
        if(C>=limit)
        {
            puts("0");
            continue;
        }
        memset(dp,0,sizeof(dp));
        memset(vis,0,sizeof(vis));
        int m=0;
        for(int i=0;i<n;i++)
        for(int j=L[i];j<=U[i];j++)
        if(!vis[j])
        {
            vis[j]=1;
            v[m++]=j;
        }
        for(int i=0;i<m;i++)
        for(int j=v[i];j<=C;j++)
        dp[j]=max(dp[j],dp[j-v[i]]+v[i]);
        printf("%d\n",C-dp[C]);
    }
    return 0;
}