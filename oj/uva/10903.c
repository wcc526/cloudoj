#include <stdio.h>

typedef struct {
  unsigned short p;
  union {
    unsigned short g[2];
    unsigned int s;
  } u;
} result;

int main(void)
{
  int i, n, k, p1, p2;
  char m1[9], m2[9];
  result r[101];

  for (i = 1; i <= 100; ++i)
    r[i].p = i;

  i = 1000;
  while (scanf("%d %d", &n, &k) == 2) {
    if (i < 1000)
      putc('\n', stdout);
    for (i = 1; i <= n; ++i)
      r[i].u.s = 0;
    for (i = (k * n * (n - 1)) >> 1; i--; ) {
      scanf("%d %8s %d %8s", &p1, m1, &p2, m2);
      if (*m1 == 'r') {
        if (*m2 == 'p') {
          ++r[p1].u.g[1];
          ++r[p2].u.g[0];
        } else if (*m2 == 's') {
          ++r[p1].u.g[0];
          ++r[p2].u.g[1];
        }
      } else if (*m1 == 'p') {
        if (*m2 == 'r') {
          ++r[p1].u.g[0];
          ++r[p2].u.g[1];
        } else if (*m2 == 's') {
          ++r[p1].u.g[1];
          ++r[p2].u.g[0];
        }
      } else {
        if (*m2 == 'r') {
          ++r[p1].u.g[1];
          ++r[p2].u.g[0];
        } else if (*m2 == 'p') {
          ++r[p1].u.g[0];
          ++r[p2].u.g[1];
        }
      }
    }
    for (i = 1; i <= n; ++i)
      if (r[i].u.s)
        printf("%.3lf\n", (double)r[i].u.g[0]/(r[i].u.g[0] + r[i].u.g[1]));
      else
        puts("-");
  }

  return 0;
}
