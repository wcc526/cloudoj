#include <algorithm>
#include <cstdio>
#include <map>

using namespace std;

int main()
{
  int i, k, n, r, t, x[5];
  long long c;

  while (scanf("%d", &n) == 1 && n) {
    map<long long, short> m;
    for (t = r = 0, i = n; i--;) {
      scanf("%d %d %d %d %d", x, x + 1, x + 2, x + 3, x + 4);
      sort(x, x + 5);
      c = (((x[0] << 9) | x[1]) << 9) | x[2];
      c = (((c << 9) | x[3]) << 9) | x[4];
      if (r < ++m[c])
        r = m[c];
    }
    for (map<long long, short>::iterator j = m.begin(); j != m.end(); ++j)
      if (j->second == r)
        t += j->second;
    printf("%d\n", t);
  }

  return 0;
}
