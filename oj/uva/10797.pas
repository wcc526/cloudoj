{$R-,Q-,S-}
Const
    InFile     = 'p10797.in';
    OutFile    = 'p10797.out';
    Limit      = 10001;

Type
    Tpoint     = record
                     number   : longint;
                     x , y    : extended;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    Left , Right
               : Tdata;
    cases ,
    midp ,
    ans1 , ans2 ,
    totL , totR: longint;
    swaped     : boolean;

function init : boolean;
var
    tmptot ,
    N , i      : longint;
    p          : Tpoint;
    tmp        : Tdata;
begin
    readln(N);
    swaped := false;
    if N = 0
      then exit(false)
      else begin
               totL := 0; totR := 0;
               for i := 1 to N do
                 begin
                     readln(p.x , p.y);
                     if p.x > 0
                       then begin p.number := totR; inc(totR); Right[totR] := p; end
                       else begin p.number := totL; inc(totL); Left[totL] := p; end;
                 end;
               if totL > totR then
                 begin
                     swaped := true;
                     tmp := Right; Right := Left; Left := tmp;
                     tmptot := totR; totR := totL; totL := tmptot;
                 end;
               exit(true);
           end;
end;

function cross(const p1 , p2 , p3 : Tpoint) : extended;
begin
    cross := (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
end;

procedure qk_pass(start , stop : longint; var mid : longint; var p : Tpoint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := {random(stop - start + 1) + start} (start + stop) div 2;
    key := Right[tmp]; Right[tmp] := Right[start];
    while start < stop do
      begin
          while (start < stop) and (cross(p , Right[stop] , key) > 0) do dec(stop);
          Right[start] := Right[stop];
          if start < stop then inc(start);
          while (start < stop) and (cross(p , key , Right[start]) > 0) do inc(start);
          Right[stop] := Right[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    Right[start] := key;
end;

procedure get_mid(p : Tpoint);
var
    start , stop ,
    mid        : longint;
begin
    start := 1; stop := totR;
    while start <= stop do
      begin
          qk_pass(start , stop , mid , p);
          if mid = midp
            then exit
            else if mid < midp
                   then start := mid + 1
                   else stop := mid - 1;
      end;
end;

procedure work;
var
    i , j , k ,
    q1 , q2,
    p1 , p2    : longint;
begin
    midp := totR div 2 + 1;
    for i := 1 to totL do
      begin
          Get_Mid(Left[i]);
          p1 := 0; p2 := 0;
          for j := 1 to totL do
            if i <> j then
              if cross(Left[i] , Right[midp] , Left[j]) > 0
                then inc(p1)
                else inc(p2);
          if p1 = p2 then
            begin
{                for j := 1 to totR do
                  if j <> midp then
                    if cross(Left[i] , Right[midp] , Right[j]) > 0
                      then inc(p1)
                      else inc(p2);
                if p1 <> p2 then
                  begin
                      writeln('NO!');
                      while true do;
                  end;               }
                ans1 := Left[i].number; ans2 := Right[midp].number;
                exit;
            end;
      end;
{    writeln('Not Found!');
    for i := 1 to totL do begin writeln(i);
      for j := 1 to totR do
        begin
            p1 := 0; p2 := 0;
            q1 := 0; q2 := 0;
            for k := 1 to totL do
              if k <> i then
                if cross(Left[i] , Right[j] , Left[k]) > 0
                  then inc(p1)
                  else inc(p2);
            for k := 1 to totR do
              if k <> j then
                if cross(Left[i] , Right[j] , Right[k]) > 0
                  then inc(q1)
                  else inc(q2);
            if (p1 = p2) and (q1 = q2) then
              begin
                  writeln(Left[i].number , ' ' , Right[j].number);
                  halt;
              end;
        end; end;
    while true do;}
end;

procedure out;
begin
    inc(Cases);
    write('Case ' , cases , ': ');
    if swaped then writeln(ans2 , ' ' , ans1) else writeln(ans1 , ' ' , ans2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
