#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int p[1024];
char d[16384];
char *r[1024];

int main()
{
  int c, i, n;
  char *s;

  gets(d);
  c = atoi(d);
  while (c--) {
    gets(d);
    gets(d);
    for (n = 0, s = strtok(d, " "); s; p[n++] = atoi(s), s = strtok(NULL, " "));
    gets(d);
    for (i = 0, s = strtok(d, " "); i < n; s = strtok(NULL, " "))
      r[p[i++] - 1] = s;
    for (i = 0; i < n; ++i)
      puts(r[i]);
    if (c)
      putchar('\n');
  }

  return 0;
}
