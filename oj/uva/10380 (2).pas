{$R-,Q-,S-}
Const
    InFile     = 'p10380.in';
    OutFile    = 'p10380.out';
    Limit      = 50;
    LimitP     = Limit * Limit;
    bound      = 2500;

Type
    Tdata      = array[1..Limit , 1..Limit , 1..2] of longint;
    Tsub_map   = array[1..LimitP] of
                   record
                       p , reverse , C       : smallint;
                   end;
    Tmap       = array[1..LimitP] of
                   record
                       tot    : longint;
                       data   : ^Tsub_map;
                   end;
    Tcount     = array[1..Limit] of longint;
    Tvisited   = array[1..LimitP] of boolean;

Var
    mark , data: Tdata;
    C          : Tmap;
    count      : Tcount;
    visited    : Tvisited;
    cases ,
    answer , tot ,
    S , T , Flow ,
    N , P , M  : longint;

procedure init;
var
    i , j      : longint;
    ch1 , ch2 ,
    ch3        : char;
begin
    for i := 1 to LimitP do C[i].tot := 0;
    fillchar(data , sizeof(data) , 0);
    fillchar(count , sizeof(count) , 0);
    dec(Cases);
    readln(N , M);
    GetMem(C[N + 1].data , N * N * 6);
    for i := 1 to LimitP do
      if i <> N + 1 then
        GetMem(C[i].data , 2 * N * 6);
    for i := 1 to N do
      begin
          for j := 1 to i - 1 do
            begin
                read(ch1 , ch2 , ch3);
                if ch1 = '-' then data[i , j , 1] := -1 else data[i , j , 1] := ord(ch1) - ord('0');
                if ch2 = '-' then data[i , j , 2] := -1 else data[i , j , 2] := ord(ch2) - ord('0');
            end;
          readln;
      end;
end;

function dfs(s : longint) : boolean;
var
    i , start ,
    stop      : longint;
begin
    if s = T then begin inc(Flow); exit(true); end;
    visited[s] := true;
    for i := 1 to C[s].tot do
      if (C[s].data[i].C > 0) and not visited[C[s].data[i].p] then
        if dfs(C[s].data[i].p) then
          begin
              dec(C[s].data[i].C); inc(C[C[s].data[i].p].data[C[s].data[i].reverse].C);
              exit(true);
          end;
    exit(false);
end;

function bfs : boolean;
type
    Tqueue     = array[1..LimitP] of longint;
var
    position ,
    father ,
    queue      : Tqueue;
    open , i , j ,
    closed     : longint;
begin
    open := 1; closed := 1; queue[open] := S;
    while open <= closed do
      begin
          for i := 1 to C[queue[open]].tot do
            if (C[queue[open]].data[i].C > 0) and not visited[C[queue[open]].data[i].p] then
              begin
                  inc(closed);
                  queue[closed] := C[queue[open]].data[i].p;
                  father[queue[closed]] := queue[open];
                  visited[queue[closed]] := true;
                  position[queue[closed]] := i;
                  if queue[closed] = T then break;
              end;
          if queue[closed] = T then break;
          inc(open);
      end;
    if queue[closed] <> T then exit(false);
    j := T;
    while j <> S do
      begin
          i := father[j];
          dec(C[i].data[position[j]].C);
          inc(C[j].data[C[i].data[position[j]].reverse].C);
          j := i;
      end;
    inc(Flow);
    exit(true);
end;

procedure add(p1 , p2 , capacity : longint);
begin
    inc(C[p1].tot); inc(C[p2].tot);
    C[p1].data[C[p1].tot].reverse := C[p2].tot;
    C[p2].data[C[p2].tot].reverse := C[p1].tot;
    C[p1].data[C[p1].tot].C := capacity;
    C[p2].data[C[p2].tot].C := 0;
    C[p1].data[C[p1].tot].p := p2;
    C[p2].data[C[p2].tot].p := p1;
end;

procedure work;
var
    k ,
    i , j , max: longint;
begin
    tot := 0;
    P := N + 2; S := N + 1; T := N + 2;
    for i := 1 to N do
      for j := 1 to i - 1 do
        for k := 1 to 2 do
          if data[i , j , k] <> -1
            then begin
                     inc(count[i] , data[i , j , k]);
                     inc(count[j] , 1 - data[i , j , k]);
                 end
            else begin
                     if (i = M) or (j = M) then
                       begin
                           if i = M then data[i , j , k] := 1 else data[i , j , k] := 0;
                           inc(count[M]); continue;
                       end;
                     inc(P);
                     mark[i , j , k] := P;
                     add(P , i , 1); add(P , j , 1);
                     add(S , P , 1); inc(tot);
                 end;
    max := 0;
    for i := 1 to N do if (i <> M) and (max < count[i]) then max := count[i];
    for i := 1 to N do if i <> M then add(i , T , max - count[i]);

    Flow := 0; answer := count[M] - max;
    while answer >= 0 do
      begin
          fillchar(visited , sizeof(visited) , 0);
          while (Flow < tot) and ((Flow < bound) and dfs(s) or (Flow >= bound) and bfs) do
            fillchar(visited , sizeof(visited) , 0);
          if Flow = tot then break;
          dec(answer); if answer < 0 then break;
          for i := 1 to N do if i <> M then inc(C[i].data[C[i].tot].C);
      end;
end;

procedure out;
var
    i , j , num ,
    q1 , q2 ,
    ni , nj ,
    p1 , p2    : longint;
begin
    if (Flow < tot) or (answer < 0)
      then writeln('Player ' , M , ' can''t win!')
      else begin
               writeln('Player ' , M , ' can win with ' , answer , ' point(s).');
               writeln;
               for i := 1 to N do
                 begin
                     num := 0;
                     for j := 1 to N do
                       if i = j
                         then write('-- ')
                         else begin
                                  if i < j
                                    then begin ni := j; nj := i; end
                                    else begin ni := i; nj := j; end;
                                  if data[ni , nj , 1] = -1
                                    then begin
                                             q1 := 1;
                                             while C[mark[ni , nj , 1]].data[q1].p <> ni do inc(q1);
                                             p1 := 1 - C[mark[ni , nj , 1]].data[q1].C;
                                         end
                                    else p1 := data[ni , nj , 1];
                                  if data[ni , nj , 2] = -1
                                    then begin
                                             q2 := 1;
                                             while C[mark[ni , nj , 2]].data[q2].p <> ni do inc(q2);
                                             p2 := 1 - C[mark[ni , nj , 2]].data[q2].C;
                                         end
                                    else p2 := data[ni , nj , 2];
                                  if i < j then begin p1 := 1 - p1; p2 := 1 - p2; end;
                                  write(p1 , p2 , ' ');
                                  inc(num , p1 + p2);
                              end;
                     writeln(': ' , num);
                 end;
           end;
    if Cases <> 0 then writeln;
    for i := 1 to LimitP do
      dispose(C[i].data);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
