{$I-}
Const
    InFile     = 'p10472.in';
    OutFile    = 'p10472.out';
    Limit      = 100;
    Maximum    = 10000000;
    Minimum    = 1e-5;
    cars       : array[1..4 , 1..5] of longint
               = ((10 , 5 , 1 , 2 , 2) ,
                  (30 , 20 , 2 , 10 , 3) ,
                  (50 , 20 , 2 , 16 , 10) ,
                  (40 , 2 , 5 , 1 , 30));

Type
    Tdata      = array[1..Limit , 1..Limit , 0..1] of longint;
    Tkey       = record
                     fare     : longint;
                     time     : extended;
                 end;
    Tshortest  = array[1..Limit , 1..Limit] of Tkey;

Var
    data       : Tdata;
    ans1 , ans2: Tshortest;
    start , stop ,
    N , cases  : longint;

procedure better(var num : longint; newnum : longint);
begin
    if (num = -1) or (newnum < num) then num := newnum;
end;

procedure init;
var
    i , M ,
    u , v , len
               : longint;
    ch         : char;
begin
    read(N , M);
    fillchar(data , sizeof(data) , $FF);
    read(start , stop);
    inc(start); inc(stop);
    inc(Cases);
    for i := 1 to M do
      begin
          read(u , v , len);
          inc(u); inc(v);
          repeat read(ch); until ch in ['N' , 'A' , 'M'];
          if (ch = 'A') or (ch = 'N') then
            begin better(data[u , v , 0] , len); better(data[v , u , 0] , len); end;
          if (ch = 'A') or (ch = 'M') then
            begin better(data[u , v , 1] , len); better(data[v , u , 1] , len); end;
      end;
end;

procedure better_floyd(var a : longint; b , c : longint);
begin
    if (b = -1) or (c = -1) then exit;
    if (a = -1) or (b + c < a) then a := b + c;
end;

procedure better_time(var key : Tkey; fare : longint; time : extended);
begin
    if time < key.time
      then begin key.fare := fare; key.time := time; end
      else if (abs(time - key.time) <= minimum) and (fare < key.fare)
             then begin key.fare := fare; key.time := time; end;
end;

procedure better_fare(var key : Tkey; fare : longint; time : extended);
begin
    if fare < key.fare
      then begin key.fare := fare; key.time := time; end
      else if (fare = key.fare) and (time < key.time)
             then begin key.fare := fare; key.time := time; end;
end;

procedure work;
var
    len ,
    i , j , k  : longint;
    key        : Tkey;
begin
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          begin
              better_floyd(data[i , j , 0] , data[i , k , 0] , data[k , j , 0]);
              better_floyd(data[i , j , 1] , data[i , k , 1] , data[k , j , 1]);
          end;
    for i := 1 to N do
      for j := 1 to N do
        begin
            ans1[i , j].fare := maximum; ans1[i , j].time := maximum;
            ans2[i , j].fare := maximum; ans2[i , j].time := maximum;
            for k := 1 to 4 do
              begin
                  if k = 1 then len := data[i , j , 0] else len := data[i , j , 1];
                  if len = -1 then continue;
                  if len <= cars[k , 3]
                    then key.fare := cars[k , 2]
                    else key.fare := cars[k , 2] + (len - cars[k , 3]) * cars[k , 4];
                  key.time := cars[k , 5] + len / cars[k , 1] * 60;
                  better_time(ans1[i , j] , key.fare , key.time);
                  better_fare(ans2[i , j] , key.fare , key.time);
              end;
        end;
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          begin
              better_time(ans1[i , j] , ans1[i , k].fare + ans1[k , j].fare , ans1[i , k].time + ans1[k , j].time);
              better_fare(ans2[i , j] , ans2[i , k].fare + ans2[k , j].fare , ans2[i , k].time + ans2[k , j].time);
          end;
end;

procedure out;
begin
    writeln('Case#' , cases);
    if ans1[start , stop].fare >= maximum
      then begin
               writeln('UNREACHABLE');
               writeln('UNREACHABLE');
           end
      else begin
               writeln(ans1[start , stop].fare , ' ' , ans1[start , stop].time : 0 : 2);
               writeln(ans2[start , stop].fare , ' ' , ans2[start , stop].time : 0 : 2);
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            init;
            if N = 0 then break;
            work;
            out;
        end;
//    Close(INPUT);
End.
