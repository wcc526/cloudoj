#include <stdio.h>

typedef struct {
  char a;
  char *i;
} op;

op i[16] = {
  { 2, "ADD " },
  { 2, "SUB " },
  { 2, "MUL " },
  { 2, "DIV " },
  { 2, "MOV " },
  { 1, "BREQ " },
  { 1, "BRLE " },
  { 1, "BRLS " },
  { 1, "BRGE " },
  { 1, "BRGR " },
  { 1, "BRNE " },
  { 1, "BR " },
  { 3, "AND " },
  { 3, "OR " },
  { 3, "XOR " },
  { 1, "NOT " }
};

int main(void)
{
  unsigned short o;
  char a, c;

  while ((c = getc(stdin)) != EOF) {
    if (c == '\n')
      continue;
    c -= (c & 0x40 ? 0x37 : 0x30);
    printf(i[(unsigned)c].i);
    for (a = i[(unsigned)c].a; a--;) {
      while ((c = getc(stdin)) == '\n');
      o = (o << 4) | (c - (c & 0x40 ? 0x37 : 0x30));
      while ((c = getc(stdin)) == '\n');
      o = (o << 4) | (c - (c & 0x40 ? 0x37 : 0x30));
      while ((c = getc(stdin)) == '\n');
      o = (o << 4) | (c - (c & 0x40 ? 0x37 : 0x30));
      while ((c = getc(stdin)) == '\n');
      o = (o << 4) | (c - (c & 0x40 ? 0x37 : 0x30));
      switch (o & 0xC000) {
      case 0x0000:
        putc('R', stdout);
        break;
      case 0x4000:
        putc('$', stdout);
        break;
      case 0x8000:
        printf("PC+");
        break;
      }
      printf("%d", o & 0x3FFF);
      putc(a ? ',' : '\n', stdout);
    }
  }

  return 0;
}
