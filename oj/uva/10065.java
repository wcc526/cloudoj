import java.util.Arrays;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		int n = s.nextInt();
		int tileNr = 0;

		while (n != 0) {
		
			tileNr++;

			Point[] points = new Point[n];

			for (int i = 0; i < n; i++) {
				Point p = new Point(s.nextInt(), s.nextInt());
				points[i] = p;
			}

			float tile = polygonArea(points);
			float box = polygonArea(concavePolygonArea(points));

			// System.out.println(tile + " " + box);
			System.out.printf("Tile #%d\n", tileNr);
			System.out.printf("Wasted Space = %.2f %%\n", (box - tile) / box * 100);
			System.out.println();
			n = s.nextInt();

			// Tile #1
			// Wasted Space = 25.00 %
			//
			// Tile #2
			// Wasted Space = 0.00 %

		}
	}

	public static Point[] concavePolygonArea(Point[] points) {
		int n = points.length;
		Arrays.sort(points);
		Point[] ans = new Point[2 * n];
		int k = 0;
		int start = 0;

		for (int i = 0; i < n; i++) {
			Point p = points[i];
			while (k - start >= 2 && 
			       p.sub(ans[k - 1]).cross(p.sub(ans[k - 2])) > 0) {
				k--;
			}
			ans[k++] = p;
		}

		k--;
		start = k;

		for (int i = n - 1; i >= 0; i--) {
			Point p = points[i];
			while (k - start >= 2 && p.sub(ans[k - 1]).cross(p.sub(ans[k - 2])) > 0)
				k--;
			ans[k++] = p;
		}
		k--;

		return Arrays.copyOf(ans, k);

	}

	static float polygonArea(Point[] points) {
		float area = 0;
		int j = points.length - 1;

		for (int i = 0; i < points.length; i++) {
			area = area + (points[i].x + points[j].x) * (points[i].y - points[j].y);
			j = i;
		}
		return Math.abs(area / 2);
	}

}

class Point implements Comparable<Point> {
	int x, y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int compareTo(Point other) {
		if (x == other.x)
			return y - other.y;
		else
			return x - other.x;
	}

	public int cross(Point p) {
		return x * p.y - y * p.x;
	}

	public Point sub(Point p) {
		return new Point(x - p.x, y - p.y);
	}
}
