#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

bool a[105][105];
int primes, prime[105];
int main()
{
	primes = 0;
	for (int i = 2; i <= 500; ++i) {
		bool isp = true;
		for (int j = 2; j * j <= i; ++j)
			if (i % j == 0) isp = false;
		if (isp) prime[++primes] = i;
	}		
	int cases;
	scanf("%d", &cases);
	for (int tcase = 1; tcase <= cases; ++tcase) {
		memset(a, 0, sizeof(a));
		int n;
		scanf("%d", &n);
		for (int i = 1; i <= n; ++i) {
			long long x;
			cin >> x;
			for (int j = 1; j <= primes; ++j) {
				if (x % prime[j] == 0) {
					int cnt = 0;
					while (x % prime[j] == 0) {
						x /= prime[j];
						cnt++;
					}
					cnt %= 2;
					if (cnt == 1) a[j][i] = true;
				}
			}
		}

		for (int i = 1; i <= primes; ++i) {
			for (int j = i; j <= primes; ++j) if (a[j][i]) {
				if (i != j) {
					for (int k = 1; k <= n; ++k) swap(a[i][k], a[j][k]);					
				}
				break;
			}
			if (!a[i][i])
			{
				for (int j = i + 1; j <= n; j++)
					if (a[i][j])
					{
						for (int k = i; k <= primes; k++)
							swap(a[k][i], a[k][j]);
						break;
					}
			}
			for (int j = 1; j <= primes; ++j) if (i != j && a[j][i] == 1) {
				for (int k = 1; k <= n; ++k)
					a[j][k] ^= a[i][k];
			}
		}
		unsigned long long ans = 1;
		for (int i = 1; i <= n; ++i)
			if (!a[i][i]) ans = ans * 2;
		ans--;
		cout << ans << endl;
	}
	return 0;
}
