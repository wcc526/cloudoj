Const
    InFile     = 'p10752.in';
    OutFile    = 'p10752.out';
    Limit      = 2001;
    LimitEdge  = 50000;

Type
    Tedge      = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..LimitEdge * 2] of Tedge;
    Tindex     = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tanswer    = array[1..Limit] of longint;
    Tfather    = array[1..Limit] of longint;

Var
    data       : Tdata;
    index      : Tindex;
    answer     : Tanswer;
    visited    : Tvisited;
    father     : Tfather;
    T , tot ,
    N , M      : longint;

procedure init;
var
    i          : longint;
begin
    dec(T);
    readln(N , M);
    for i := 1 to M do
      begin
          read(data[i].x , data[i].y);
          data[i + M].x := data[i].y;
          data[i + M].y := data[i].x;
      end;
    M := M * 2;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tedge;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].x > key.x) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].x < key.x) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure dfs(p : longint);
var
    i          : longint;
begin
    i := index[p]; visited[p] := true;
    while (i <> 0) and (i <= M) and (data[i].x = p) do
      begin
          if not visited[data[i].y] then
            begin
                father[data[i].y] := p;
                dfs(data[i].y);
            end;
          inc(i);
      end;
end;

procedure dfs_print(root : longint; direction : boolean);
var
    i          : longint;
begin
    i := index[root]; visited[root] := true;
    if direction then begin inc(tot); answer[tot] := root; end;
    while (i <> 0) and (i <= M) and (data[i].x = root) do
     begin
         if not visited[data[i].y] then
           dfs_print(data[i].y , not direction);
         inc(i);
     end;
    if not direction then begin inc(tot); answer[tot] := root; end;
end;

procedure work;
var
    i          : longint;
begin
    qk_sort(1 , M);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[data[i].x] = 0 then
        index[data[i].x] := i;

    fillchar(visited , sizeof(visited) , 0);
    fillchar(father , sizeof(father) , 0);
    dfs(1); tot := 0;
    for i := 1 to N do
      if not visited[i] then
        exit;
    fillchar(visited , sizeof(visited) , 0);
    dfs_print(1 , true);
    inc(tot); answer[tot] := 1;
end;

procedure print_way(p1 , p2 : longint);
var
    data1 , data2
               : array[1..4] of longint;
    i , j , k ,
    tot1 , tot2: longint;
begin
    tot1 := 0; tot2 := 0;
    while (p1 <> 0) and (tot1 < 4) do
      begin inc(tot1); data1[tot1] := p1; p1 := father[p1]; end;
    while (p2 <> 0) and (tot2 < 4) do
      begin inc(tot2); data2[tot2] := p2; p2 := father[p2]; end;
    for i := 1 to tot1 do
      for j := 1 to tot2 do
       if data1[i] = data2[j] then
         if (i + j - 2 <= 3) then
           begin
               write(i + j - 2);
               for k := 1 to i do write(' ' , data1[k]);
               for k := j - 1 downto 1 do write(' ' , data2[k]);
               writeln;
               exit;
           end;
end;

procedure out;
var
    i          : longint;
begin
    if tot = 0
      then writeln('Impossible')
      else for i := 1 to tot do
             print_way(answer[i] , answer[i + 1]);
    if T <> 0 then writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.