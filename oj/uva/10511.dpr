{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10511.in';
    OutFile    = 'p10511.out';
    Limit      = 1050;
    LimitLine  = 1000;
    LimitP     = 3050;
    LimitSave  = 80;
    LimitEdge  = 100000;

Type
    Tedge      = record
                     p1 , p2 , C , inverse   : longint;
                 end;
    Tdata      = array[1..LimitEdge] of Tedge;
    Tindex     = array[1..LimitP] of longint;
    Tstr       = string[90];
    Tnames     = record
                     tot                     : longint;
                     data                    : array[1..Limit] of Tstr;
                 end;
    Tquery     = array[1..Limit] of
                   record
                       person , p , tot      : longint;
                       club                  : array[1..LimitSave] of longint;
                   end;
    Tvisited   = array[1..LimitP] of boolean;


Var
    data       : Tdata;
    index      : Tindex;
    club , people ,
    party      : Tnames;
    query      : Tquery;
    visited    : Tvisited;
    Tests , sum ,
    N , M ,
    S , T      : longint;
    answer     : boolean;

function Get_Word(var word , s : Tstr) : boolean;
begin
    Get_Word := false;
    while (s <> '') and (s[1] = ' ') do delete(s , 1 , 1);
    if s = '' then exit;
    word := '';
    while (s <> '') and (s[1] <> ' ') do
      begin
          word := word + s[1]; delete(s , 1 , 1);
      end;
    Get_Word := true;
end;

procedure Ins(word : Tstr; var names : Tnames; var position : longint);
var
    i          : longint;
begin
    if names.tot > LimitLine then exit;
    for i := 1 to names.tot do
      if names.data[i] = word then
        begin
            position := i;
            exit;
        end;
    inc(names.tot);
    names.data[names.tot] := word;
    position := names.tot;
end;

procedure process(s : Tstr);
var
    word       : Tstr;
begin
    Get_Word(word , s);
    Ins(word , people , query[sum].person);
    Get_Word(word , s);
    Ins(word , party , query[sum].p);
    while Get_Word(word , s) do
      begin
          inc(query[sum].tot);
          Ins(word , club , query[sum].club[query[sum].tot]);
      end;
end;

procedure init;
var
    s          : Tstr;
begin
    dec(Tests);
    sum := 0;
    fillchar(club , sizeof(club) , 0);
    fillchar(party , sizeof(party) , 0);
    fillchar(people , sizeof(people) , 0);
    fillchar(query , sizeof(query) , 0);
    readln(s);
    while (s <> '') do
      begin
          inc(sum);
          process(s);
          if eof then break;
          readln(s);
          while (s <> '') and (s[1] = ' ') do delete(s , 1 , 1);
      end;
end;

procedure add_edge(p1 , p2 , C : longint);
begin
    data[M + 1].p1 := p1; data[M + 1].p2 := p2; data[M + 1].C := C; data[M + 1].inverse := M + 2;
    data[M + 2].p1 := p2; data[M + 2].p2 := p1; data[M + 2].C := 0; data[M + 2].inverse := M + 1;
    inc(M , 2);
end;

procedure swap(p1 , p2 : longint);
var
    tmp        : Tedge;
begin
    tmp := data[p1]; data[p1] := data[p2]; data[p2] := tmp;
    if data[p1].inverse = p1
      then begin
               data[p1].inverse := p2; data[p2].inverse := p1;
           end
      else begin
               data[data[p1].inverse].inverse := p1;
               data[data[p2].inverse].inverse := p2;
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    swap(start , tmp);
    while start < stop do
      begin
          while (start < stop) and (data[stop].p1 > data[start].p1) do dec(stop);
          swap(start , stop);
          if start < stop then inc(start);
          while (start < stop) and (data[start].p1 < data[stop].p1) do inc(start);
          swap(stop , start);
          if start < stop then dec(stop);
      end;
    mid := start;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure pre_process;
var
    i , j      : longint;
begin
    N := 2 + club.tot + people.tot + party.tot; M := 0;
    S := 1; T := 2 + club.tot + people.tot + party.tot;
    for i := 1 to club.tot do add_edge(S , i + 1 , 1);
    for i := 1 to party.tot do add_edge(i + 1 + club.tot + people.tot , T , (club.tot - 1) div 2);
    for i := 1 to sum do
      begin
          add_edge(query[i].person + 1 + club.tot , query[i].p + 1 + club.tot + people.tot , 1);
          for j := 1 to query[i].tot do
            add_edge(query[i].club[j] + 1 , query[i].person + 1 + club.tot , 1);
      end;
    qk_sort(1 , M);
    fillchar(index , sizeof(index) , 0);
    for i := 1 to M do
      if index[data[i].p1] = 0 then
        index[data[i].p1] := i;
end;

function Get_Extend(root : longint) : boolean;
var
    i          : longint;
begin
    visited[root] := true;
    if root = T
      then Get_Extend := true
      else begin
               i := index[root];
               while (i <> 0) and (i <= M) and (data[i].p1 = root) do
                 begin
                     if not visited[data[i].p2] and (data[i].C > 0) then
                       if Get_Extend(data[i].p2) then
                         begin
                             Get_Extend := true;
                             dec(data[i].C); inc(data[data[i].inverse].C);
                             exit;
                         end;
                     inc(i);
                 end;
               Get_Extend := false;
           end;
end;

procedure work;
var
    i          : longint;
begin
    answer := false;
    if club.tot > LimitLine then exit;
    pre_process;

    for i := 1 to club.tot do
      begin
          fillchar(visited , sizeof(visited) , 0);
          if not Get_Extend(S) then
            exit;
      end;
    answer := true;
end;

procedure out;
var
    i , j      : longint;
begin
    if answer
      then begin
               for i := 1 to club.tot do
                 begin
                     j := index[i + 1];
                     while (j <> 0) and (j <= M) and (data[j].p1 = i + 1) do
                       if data[j].C = 0
                         then begin
                                  writeln(people.data[data[j].p2 - 1 - club.tot] , ' ' , club.data[i]);
                                  break;
                              end
                         else inc(j);
                 end;
           end
      else writeln('Impossible.');
    if Tests <> 0 then writeln;
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Tests); readln;
      while Tests > 0 do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
