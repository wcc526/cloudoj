#include <stdio.h>

int main()
{
  double d[100], m, t, v[10];
  int i, j, n, f, r;

  while (scanf("%d", &f) == 1 && f) {
    scanf("%d", &r);
    for (i = f; i--;)
      scanf("%lf", v + i), v[i] = 1.0 / v[i];
    for (n = 0, i = r; i--;)
      for (scanf("%lf", &t), j = f; j--;)
        d[n++] = t * v[j];
    for (i = 1; i < n; ++i, d[j + 1] = t)
      for (t = d[j = i]; j-- && d[j] > t;)
        d[j + 1] = d[j];
    for (m = 0, i = n; --i;)
      if (m < d[i] / d[i - 1])
        m = d[i] / d[i - 1];
    printf("%.2lf\n", m);
  }

  return 0;
}
