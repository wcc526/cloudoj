#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

class BigInt {
public:
    enum{MAX=61};
    int dig[MAX];
    int size;

    BigInt();
    BigInt operator+(const BigInt &b);
    BigInt operator=(const BigInt &right);
    BigInt operator=(const char* input);
    void print();
};

BigInt::BigInt(){
    size = 0;
    memset(dig,0,sizeof(dig));
}

BigInt BigInt::operator+(const BigInt &b){
    BigInt sum;
    sum.size = max(size, b.size);
    for(int i=0;i<MAX;i++){
        sum.dig[i] += dig[i] + b.dig[i];
        if(sum.dig[i]>9){
            sum.dig[i] -= 10;
            sum.dig[i+1]++;
        }
    }
    if(sum.dig[sum.size]!=0) ++sum.size;
    return sum;
}

BigInt BigInt::operator=(const BigInt &right){

    if( &right==this ) return *this;

    size = right.size;
    for(int i=0;i<MAX;i++)
        dig[i] = right.dig[i];

    return *this;
}

BigInt BigInt::operator=(const char* input){
    size = strlen(input);
    for(int i=0;i<size;i++){
        dig[i] = input[size-i-1] - '0';
    }
    return *this;
}

void BigInt::print(){
    for(int i=size-1;i>=0;i--)
        printf("%d",dig[i]);
}

int main(){

    BigInt pascal[2][300];
    int currentLength;
    int current=0 , next=1;

    //initial row1
    pascal[current][0] = "1";
    pascal[current][1] = "1";
    currentLength = 2;

    // output pascal triangle
    printf("1\n");
    printf("1 1\n");

    do{
        pascal[next][0] = "1";
        pascal[next][currentLength] = "1";

        printf("1 ");
        for(int i=1;i<currentLength;i++){
            pascal[next][i] = pascal[current][i-1] + pascal[current][i];
            pascal[next][i].print();
            putchar(' ');
        }
        printf("1\n");

        swap(current, next);
        currentLength++;

    } while(pascal[current][currentLength/2].size <61);
    //10的60次方是61位數
    return 0;
}

