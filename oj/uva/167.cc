#include <cstdio>
#include <cstring>
using namespace std;

int grid[9][9], k, col[9], max;

int abs(int num) {
    if (num < 0) return -num;
    else return num;
}

bool isOkay(int row, int trycol) {
    for (int i=1; i<row; ++i) {
        if (col[i] == trycol || (abs(col[i]-trycol) == abs(row-i))) {
            return false;
        }
    }
    return true;
}

void backtrack(int row, int sum) {
    for (int i=1; i<=8; ++i) {
        if (isOkay(row, i)) {
            col[row] = i;
            if (row == 8) {
                if (max < sum+grid[row][i]) {
                    max = sum+grid[row][i];
                }
            } else {
                backtrack(row+1, sum+grid[row][i]);
            }
        }
    }
}

int main()
{
    scanf("%d", &k);
    while (k--) {
        for (int i=1; i<=8; ++i) {
            for (int j=1; j<=8; ++j) {
                scanf("%d", &grid[i][j]);
            }
        }
        memset(col, 0, sizeof(col));
        max = 0;
        backtrack(1, 0);
        printf("%5d\n", max);
    }

    return 0;
}
