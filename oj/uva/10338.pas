Type
    Tfractorial
               = array[0..20] of int64;
    Tcount     = array['A'..'Z'] of longint;

Var
    fractorial : Tfractorial;
    count      : Tcount;
    answer     : int64;
    i , tot ,
    sum        : longint;
    ch         : char;

Begin
    readln(tot);
    fractorial[0] := 1;
    for i := 1 to 20 do
      fractorial[i] := fractorial[i - 1] * int64(i);

    for i := 1 to tot do
      begin
          sum := 0;
          fillchar(count , sizeof(count) , 0);
          while not eoln do
            begin
                read(ch);
                inc(sum);
                inc(count[ch]);
            end;
          readln;
          answer := fractorial[sum];
          for ch := 'A' to 'Z' do
            answer := answer div fractorial[count[ch]];
          writeln('Data set ' , i , ': ' , answer);
      end;
End.