Const
    Limit      = 1000;
    LimitLen   = 50;

Type
    Tstr       = string[LimitLen];
    Tfinger    = array['a'..'z'] of longint;
    Tcanuse    = array[1..10] of boolean;
    Tdata      = array[1..Limit] of Tstr;
    Tok        = array[1..Limit] of boolean;

Var
    finger     : Tfinger;
    canuse     : Tcanuse;
    data       : Tdata;
    ok         : Tok;
    max ,
    F , N      : longint;

procedure pre_process;
begin
    finger['q'] := 1; finger['a'] := 1; finger['z'] := 1;
    finger['w'] := 2; finger['s'] := 2; finger['x'] := 2;
    finger['e'] := 3; finger['d'] := 3; finger['c'] := 3;
    finger['r'] := 4; finger['f'] := 4; finger['v'] := 4;
    finger['t'] := 4; finger['g'] := 4; finger['b'] := 4;
    finger['y'] := 7; finger['h'] := 7; finger['n'] := 7;
    finger['u'] := 7; finger['j'] := 7; finger['m'] := 7;
    finger['i'] := 8; finger['k'] := 8; finger['k'] := 8;
    finger['o'] := 9; finger['l'] := 9; finger['l'] := 9;
    finger['p'] :=10; finger['p'] :=10; finger['p'] :=10;
end;

procedure init;
var
    i , p      : longint;
begin
    fillchar(canuse , sizeof(canuse) , 1);
    read(F , N);
    for i := 1 to F do
      begin
          read(p); canuse[p] := false;
      end;
    readln;
    for i := 1 to N do
      readln(data[i]);
end;

procedure work;
var
    tmp        : Tstr;
    max ,
    i , j      : longint;
begin
    fillchar(ok , sizeof(ok) , 1);
    for i := 1 to N do
      for j := 1 to length(data[i]) do
        begin
            if (data[i , j] in ['a'..'z']) and not canuse[finger[data[i , j]]] then
              ok[i] := false;
        end;
    max := 0;
    for i := 1 to N do
      if ok[i] and (length(data[i]) > max) then
        max := length(data[i]);
    i := 0;
    for j := 1 to N do
      if ok[j] and (length(data[j]) = max) then
        begin
            inc(i);
            data[i] := data[j];
        end;
    N := i;
    for i := 1 to N do
      for j := i + 1 to N do
        if data[i] > data[j] then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;
    i := 1; j := 2;
    while j <= N do
      if data[i] = data[j]
        then inc(j)
        else begin inc(i); data[i] := data[j]; inc(j); end;
    if i < N then N := i;
    writeln(N);
    for i := 1 to N do
      writeln(data[i]);
end;

Begin
    pre_process;
//    assign(INPUT , 'p10393.in'); ReSet(INPUT);
    while not eof do
      begin
          init;
          work;
      end;
End.