/**
 * UVa 490 Rotating Sentences
 * Author: chchwy
 * Last modified: 2010.04.23
 */
#include<iostream>
#include<vector>
using namespace std;

//find the max length of strings
int findMaxX(vector<string> &str){
    int max=0;
    for(int i=0;i<str.size();++i)
        if(str[i].size()>max)
            max = str[i].size();
    return max;
}

int main(){
    #ifndef ONLINE_JUDGE
    freopen("490.in","r",stdin);
    #endif

    vector<string> str;

    //read input
    string line;
    while( getline(cin, line) )
        str.push_back(line);

    //find the bound
    int max_y = str.size();
    int max_x = findMaxX(str);

    //output
    for(int i=0;i<max_x;++i){
        for(int j=max_y-1;j>=0;--j){
            char c = (i<str[j].size())? str[j][i] :  ' ';
            putchar(c);
        }
        putchar('\n');
    }
    return 0;
}

