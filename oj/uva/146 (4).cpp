/**
 * UVa 146 ID Codes
 * Author: chchwy
 * Last Modified: 2011.04.04
 * Blog: http://chchwy.blogspot.com
 */
#include<iostream>
#include<algorithm>
using namespace std;

int main() {

    string id;
    while( getline(cin, id) ) {
        if( id=="#" )
            break;

        if ( next_permutation(id.begin(), id.end()) )
            cout<<id<<endl;
        else
            cout<<"No Successor"<<endl;
    }
    return 0;
}
