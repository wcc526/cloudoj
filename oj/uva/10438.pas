Const
    InFile     = 'p10438.in';
    OutFile    = 'p10438.out';
    Limit      = 100000;
    LimitLen   = 50;
    spaces     = [#9 , #32];

Type
    Tstr       = string[LimitLen];
    Tdata      = array[1..Limit] of Tstr;

Var
    data       : Tdata;
    N          : longint;

function read_str(var s : Tstr) : boolean;
var
    ch         : char;
begin
    s := '';
    repeat
      if eoln or eof then exit(false) else read(ch);
    until not (ch in spaces);
    while not (ch in spaces) do
      begin
          s := s + ch;
          if eoln or eof then ch := ' ' else read(ch);
      end;
    exit(true);
end;

procedure init;
var
    s          : Tstr;
begin
    N := 0;
    while read_str(s) do
      begin
          inc(N);
          data[N] := s;
      end;
end;

function check(i , j : longint) : boolean;
var
    k          : longint;
begin
    for k := 0 to j - i - 1 do
      if data[i + k] <> data[j + k] then
        exit(false);
    exit(true);
end;

function process : boolean;
var
    i , j , k  : longint;
begin
    for i := 1 to N do
      for j := i + 1 to (N + i + 1) div 2 do
        if check(i , j) then
          begin
              for k := j to N do
                data[i + k - j] := data[k];
              N := i + N - j;
              exit(true);
          end;
    exit(false);
end;

procedure work;
begin
    while process do;
end;

procedure out;
var
    i          : longint;
begin
    write(data[1]);
    for i := 2 to N do
      write(' ' , data[i]);
    writeln;
end;

Begin
    while not eof do
      begin
          init;
          work;
          out;
          if not eof then readln;
      end;
End.