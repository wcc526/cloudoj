#include <cstdio>
#include <map>
#include <queue>

using namespace std;

int main(void)
{
  int i;
  map<int, int> m;
  queue<int> q;

  while (scanf("%d", &i) == 1) {
    if (m.find(i) == m.end()) {
      q.push(i);
      m.insert(make_pair(i, 1));
    } else {
      ++m[i];
    }
  }

  while (q.size()) {
    i = q.front();
    printf("%d %d\n", i, m[i]);
    q.pop();
  }

  return 0;
}
