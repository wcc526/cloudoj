{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10529.in';
    OutFile    = 'p10529.out';
    Limit      = 1000;

Var
    N          : longint;
    f          : array[0..Limit] of extended;
    a , b ,
    answer     : extended;

function init : boolean;
begin
    read(N);
    if N = 0
      then begin init := false; exit; end;
    init := true;
    read(a , b);
end;

procedure work;
var
    i , j      : longint;
    tmp        : extended;
begin
    f[0] := 0;
    for i := 1 to N do
      begin
          f[i] := 1e10;
          for j := 1 to i do
            begin
                tmp := (1 + (1 - a) * f[j - 1] + (1 - b) * f[i - j]) / (1 - a - b);
                if tmp < f[i] then f[i] := tmp; 
            end;
      end;
    answer := f[N];
end;

procedure out;
begin
    writeln(answer : 0 : 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
