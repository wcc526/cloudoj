Const
    InFile     = 'p10475.in';
    OutFile    = 'p10475.out';
    Limit      = 16;
    LimitLen   = 15;

Type
    Tstr       = string[LimitLen];
    Tdata      = array[1..Limit] of Tstr;
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tstack     = array[1..Limit] of longint;

Var
    data       : Tdata;
    map        : Tmap;
    stack      : Tstack;
    tot , Cases ,
    N , S      : longint;

procedure init;
var
    M , i , j ,
    p1 , p2    : longint;
    s1 , s2 ,
    tmp        : Tstr;
    ch         : char;
begin
    inc(Cases);
    fillchar(map , sizeof(map) , 0);
    readln(N , M , S);
    for i := 1 to N do begin readln(data[i]); data[i] := upcase(data[i]); end;
    for i := 1 to N do
      for j := i + 1 to N do
        if (length(data[i]) < length(data[j])) or (length(data[i]) = length(data[j])) and (data[i] > data[j]) then
          begin
              tmp := data[i]; data[i] := data[j]; data[j] := tmp;
          end;
    for i := 1 to M do
      begin
          s1 := ''; s2 := '';
          read(ch); while ch <> ' ' do begin s1 := s1 + ch; read(ch); end;
          read(ch); while ch <> ' ' do begin s2 := s2 + ch; if eoln then ch := ' ' else read(ch); end;
          s1 := upcase(s1); s2 := upcase(s2);
          readln;
          p1 := 1; while data[p1] <> s1 do inc(p1);
          p2 := 1; while data[p2] <> s2 do inc(p2);
          map[p1 , p2] := true; map[p2 , p1] := true;
      end;
end;

procedure dfs_print(now , top : longint);
var
    i          : longint;
    ok         : boolean;
begin
    if top > S
      then begin
               for i := 1 to S - 1 do write(data[stack[i]] , ' ');
               writeln(data[stack[S]]);
               exit;
           end;
    if now > N then exit;
    ok := true;
    for i := 1 to top - 1 do
      if map[stack[i] , now] then
        begin ok := false; break; end;
    if ok then
      begin
          stack[top] := now;
          dfs_print(now + 1 , top + 1);
          stack[top] := 0;
      end;
    dfs_print(now + 1 , top);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(tot); Cases := 0;
      while Cases < tot do
        begin
            init;
            writeln('Set ' , cases , ':');
            dfs_print(1 , 1);
            writeln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
