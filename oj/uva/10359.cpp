#include <stdio.h>
#include <string.h>
#define MAX 124
char f[252][MAX];
void add(char *str1,char *str2,char *str3)
{
	int len1 = strlen(str1), len2 = strlen(str2);
	int m, n;
	int i, j, k = 0, carry = 0;
	int tmp;
	char ch;
	char st[MAX];
	for(m = 0; m < len1; m++)
		if(str1[m] == '.')
			break;
	for(n = 0; n < len2; n++)
		if(str2[n] == '.')
			break;
	for(i=m+1,j=n+1; i<len1 && j<len2; i++,j++)
	{
		tmp = str1[i] - '0' + str2[j] -'0' + carry;
		carry = tmp / 10;
		st[k++] = tmp % 10 + '0';
	}
	for(i=m-1,j=n-1; i>=0 && j>=0; i--,j--)
	{
		tmp = str1[i] - '0' + str2[j] -'0' + carry;
		carry = tmp / 10;
		str3[k++] = tmp % 10 + '0';
	}
	while(i >= 0)
	{
		tmp = str1[i--] - '0' + carry;
		carry = tmp / 10;
		str3[k++] = tmp % 10 + '0';
	}
	while(j >= 0)
	{
		tmp = str2[j--] - '0' + carry;
		carry = tmp / 10;
		str3[k++] = tmp % 10 + '0';
	}
	if(carry)	str3[k++] = carry + '0';
	str3[k] = '\0';
	for(i=0; i<k/2; i++)
		ch = str3[i], str3[i] = str3[k-i-1], str3[k-i-1] = ch;
}
int main()
{
	int n,i;
	char t[MAX];
	f[0][0]='1';f[0][1]=0;
	f[1][0]='1';f[1][1]=0;
	for(i=2;i<=250;i++)
	{
		t[0]=0;
		add(f[i-2],f[i-2],t);
		add(f[i-1],t,f[i]);
	}
	while(scanf("%d",&n)!=EOF)
		printf("%s\n",f[n]);
	return 0;
}