Const
    InFile     = 'E.1.dat';
    OutFile    = 'p10869.out';
    Limit      = 200000;
    LimitTree  = 65536 * 2 * 2 - 1;

Type
    Tpoint     = array[1..3] of longint;
    Tdata      = array[1..Limit] of Tpoint;
    Tcount     = array[1..Limit , 1..4] of longint;
    Ttree      = array[1..LimitTree * 2 + 1] of longint;
    Tonline    = array[1..Limit] of longint;

Var
    answer ,
    nums ,
    data       : Tdata;
    count      : Tcount;
    tree       : Ttree;
    online     : Tonline;
    ans ,
    N , T      : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(N);
    if N = 0 then exit(false);
    for i := 1 to N do read(data[i , 1] , data[i , 2]);
    for i := 1 to N do data[i , 3] := i;
    exit(true);
end;

procedure qk_pass(var data : Tdata; k , start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and ((data[stop , k] > key[k]) or (data[stop , k] = key[k]) and (data[stop , 3 - k] > key[3 - k])) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and ((data[start , k] < key[k]) or (data[start , k] = key[k]) and (data[start , 3 - k] < key[3 - k])) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start; data[start] := key;
end;

procedure qk_sort(var data : Tdata; k , start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(data , k , start , stop , mid);
          qk_sort(data , k , start , mid - 1);
          qk_sort(data , k , mid + 1 , stop);
      end;
end;

procedure pre_process;
var
    i , j      : longint;
begin
    for i := 1 to 2 do
      begin
          for j := 1 to N do
            begin
                nums[j , 1] := data[j , i]; nums[j , 2] := j;
            end;
          qk_sort(nums , 1 , 1 , N);
          data[nums[1 , 2] , i] := 1;
          for j := 2 to N do
            if nums[j , 1] = nums[j - 1 , 1]
              then data[nums[j , 2] , i] := data[nums[j - 1 , 2] , i]
              else data[nums[j , 2] , i] := data[nums[j - 1 , 2] , i] + 1;
      end;
end;

procedure tree_add(p : longint);
begin
    inc(p , LimitTree);
    while p <> 0 do
      begin
          inc(tree[p]); p := p div 2;
      end;
end;

function tree_query(Left , Right : longint) : longint;
var
    res        : longint;
begin
    res := 0; inc(Left , LimitTree); inc(Right , LimitTree);
    while Left <= Right do
      begin
          if Left = Right then begin inc(res , tree[Left]); break; end;
          if odd(Left) then begin inc(res , tree[Left]); inc(Left); end;
          if odd(right - 1) then begin inc(res , tree[Right]); dec(Right); end;
          Left := Left div 2; Right := Right div 2;
      end;
    exit(res);
end;

procedure work;
var
    thisline ,
    min , max ,
    i , j , tmp: longint;
begin
    pre_process;
    for i := 1 to 4 do
      begin
          fillchar(tree , sizeof(tree) , 0);
          fillchar(online , sizeof(online) , 0);
          qk_sort(data , 1 , 1 , N);
          thisline := 0;
          for j := 1 to N do
            begin
                if (j <> 1) and (data[j , 1] = data[j - 1 , 1])
                  then inc(thisline)
                  else thisline := 1;
                tree_add(data[j , 2]);
                count[data[j , 3] , i] := tree_query(1 , data[j , 2]) - online[data[j , 2]] - thisline;
                inc(online[data[j , 2]]);
            end;
          for j := 1 to N do
            begin
                tmp := data[j , 1];
                data[j , 1] := data[j , 2];
                data[j , 2] := N - tmp + 1;
            end;
      end;

    fillchar(answer , sizeof(answer) , 0);
    T := 0; ans := -1;
    qk_sort(data , 1 , 1 , N);
    i := 1;
    while i <= N do
      begin
          j := i; min := maxlongint; max := -1;
          while (j <= N) and (data[i , 1] = data[j , 1]) do
            begin
                tmp := count[data[j , 3] , 1] + count[data[j , 3] , 3];
                if tmp < min then min := tmp;
                tmp := count[data[j , 3] , 2] + count[data[j , 3] , 4];
                if tmp > max then max := tmp;
                inc(j);
            end;
          if min = ans then begin inc(T); answer[T , 1] := max; end;
          if min > ans then begin ans := min; T := 1; answer[T , 1] := max; end;
          i := j;
      end;
    qk_sort(answer , 1 , 1 , T);
end;

procedure out;
var
    i          : longint;
begin
    write('Stan: ' , ans , '; Ollie:');
    for i := 1 to T do
      if (i = 1) or (answer[i , 1] <> answer[i - 1 , 1])
        then write(' ' , answer[i , 1]);
    writeln(';');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
