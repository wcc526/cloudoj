#include <stdio.h>

int main(void)
{
  int c, i, l, m, n, t;

  while (scanf("%d", &n) == 1 && n) {
    while (getchar() > '\n');
    m = t = 0;
    for (i = n; i--;) {
      l = 0;
      while ((c = getchar()) > '\n')
        if (c == 'X')
          ++l;
      if (m < l)
        m = l;
      t += l;
    }
    printf("%d\n", m * n - t);
  }

  return 0;
}
