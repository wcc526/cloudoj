{$R-,Q-,S-}
Const
    InFile     = 'p10761.in';
    OutFile    = 'p10761.out';

Type
    Tdata      = array['a'..'z'] of boolean;
    Tstr       = string[255];

Var
    now ,
    broken ,
    needed     : Tdata;
    s1 , s2 , s3 ,
    s          : Tstr;
    ch         : char;
    count , T  : longint;

procedure convert(s : Tstr; var data : Tdata);
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    s := upcase(s);
    for i := 1 to length(s) do
      if s[i] in ['A'..'Z'] then
        data[chr(ord(s[i]) - ord('A') + ord('a'))] := true;
end;

function cross(const data1 , data2 : Tdata) : boolean;
var
    ch         : char;
begin
    for ch := 'a' to 'z' do
      if data1[ch] and data2[ch] then
        exit(true);
    exit(false);
end;

procedure add(var data1 : Tdata; const data2 : Tdata);
var
    ch         : char;
begin
    for ch := 'a' to 'z' do
      data1[ch] := data1[ch] or data2[ch];
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    writeln('+----------+----------------+-----------------------------+');
    writeln('| Keyboard | # of printable | Additionally, the following |');
    writeln('|          |      lines     |  letter keys can be broken  |');
    writeln('+----------+----------------+-----------------------------+');
    readln(s); T := 0;
    while s <> 'finish' do
      begin
          inc(T);
          fillchar(needed , sizeof(needed) , 0);
          convert(s , broken);
          count := 0;
          repeat
                readln(s);
                convert(s , now);
                if not cross(broken , now) then
                  begin
                      inc(count);
                      add(needed , now);
                  end;
          until s = 'END';
          readln(s);
          str(T , s1);
          while length(s1) < 3 do s1 := ' ' + s1;
          str(count , s2);
          while length(s2) < 3 do s2 := ' ' + s2;
          s3 := '';
          for ch := 'a' to 'z' do
            if not needed[ch] and not broken[ch] then
              s3 := s3 + ch;
          while length(s3) < 28 do s3 := s3 + ' ';
          writeln('|   ' + s1 + '    |      ' + s2 + '       | ' + s3 + '|');
          writeln('+----------+----------------+-----------------------------+');
      end;
End.