#include <math.h>
#include <stdio.h>

short g()
{
  short r;
  char c;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  while ((c = getc(stdin)) >= '0')
    r = r * 10 + c - '0';
  return r;
}

int main()
{
  short a, b, c, i, j, n, v[50];

  while ((n = g())) {
    for (i = n; i--;)
      v[i] = g();
    for (c = 0, i = n; i--;) {
      for (j = i; j--;) {
        if ((v[i] & 1) == 0 && (v[j] & 1) == 0)
          continue;
        a = v[i];
        b = v[j];
        do
          if ((a & 1) == 0)
            a >>= 1;
          else if ((b & 1) == 0)
            b >>= 1;
          else if (a >= b)
            a = (a - b) >> 1;
          else
            b = (b - a) >> 1;
        while (a > 0);
        if (b == 1)
          ++c;
      }
    }
    if (c)
      printf("%.6lf\n", sqrt(3.0 * n * (n - 1) / c));
    else
      puts("No estimate for this data set.");
  }

  return 0;
}
