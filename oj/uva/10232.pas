Const
    InFile     = 'p10232.in';
    OutFile    = 'p10232.out';
    Len        = 31;

Type
    Tdata      = array[1..Len] of longint;
    TC         = array[0..Len , 0..Len] of longint;

Var
    C          : TC;
    data       : Tdata;
    N , answer : longint;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(C , sizeof(C) , 0);
    C[0 , 0] := 1;
    for i := 1 to Len do
      begin
          C[i , 0] := 1; C[i , i] := 1;
          for j := 1 to i - 1 do
            C[i , j] := C[i - 1 , j - 1] + C[i - 1 , j];
      end;
end;

procedure work;
var
    count , i  : longint;
begin
    fillchar(data , sizeof(data) , 0);
    count := 0;
    while C[Len , count] <= N do
      begin
          dec(N , C[Len , count]);
          inc(count);
      end;
    i := Len;
    while i >= 1 do
      begin
          if C[i - 1 , count] <= N
            then begin
                     dec(N , C[i - 1 , count]);
                     dec(count);
                     data[i] := 1;
                 end;
          dec(i);
      end;
    answer := 0;
    for i := Len downto 1 do
      answer := answer * 2 + data[i];
end;

Begin
    pre_process;
    while not eof do
      begin
          readln(N);
          work;
          writeln(answer);
      end;
End.