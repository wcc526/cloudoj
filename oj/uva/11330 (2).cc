#include <cstdio>
#include <map>

#define N 10001

using namespace std;

short a[N], b[N], c[N], d[N], y;
map<short, short> x;

int g(int i)
{
  return x[i] = (x[i] ? x[i] : ++y);
}

int main()
{
  int i, j, n, t, u, v;

  scanf("%d", &t);
  while (t--) {
    x.clear(), y = 0;
    scanf("%d", &n);
    for (i = 0; i++ < n;)
      scanf("%d %d", &u, &v), a[i] = g(u), b[g(v)] = i;
    for (i = 0; i++ < n; d[i] = 1)
      c[i] = b[a[i]];
    for (u = i = 0; i++ < n; d[i] = 0)
      if (d[i])
        for (j = c[i]; i ^ j; d[j] = 0, j = c[j], ++u);
    printf("%d\n", u);
  }

  return 0;
}
