Var
    i , j , tot ,
    sign , tmp ,
    N , M      : longint;
    bigger     : boolean;
    data       : array[1..15] of longint;
    answer     : extended;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then exit(b)
      else exit(gcd(b mod a , a));
end;

Begin
    while not eof do
      begin
          readln(N , M);
          for i := 1 to M do read(data[i]);
          readln;
          answer := 0;
          for i := 0 to 1 shl M - 1 do
            begin
                sign := 1; tot := 1; bigger := false;
                for j := 1 to M do
                  if (1 shl (j - 1)) and i <> 0 then
                    begin
                        sign := -sign;
                        tmp := gcd(tot , data[j]);
                        if double(tot div tmp) * data[j] > N
                          then begin bigger := true; break; end
                          else tot := tot div tmp * data[j];
                    end;
                if not bigger
                  then answer := answer + N div tot * sign;
            end;
          writeln(answer : 0 : 0);
      end;
End.