{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10634.in';
    OutFile    = 'p10634.out';
    Limit      = 1000;

Type
    Tdata      = array[0..Limit , 0..Limit] of extended;

Var
    data       : Tdata;
    N , V      : longint;
    answer     : extended;

procedure pre_process;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    for i := 1 to Limit do data[0 , i] := 1;
    for i := 1 to Limit do
      for j := 1 to Limit do
        data[i , j] := data[i - 1 , j] + data[i , j - 1];
end;

function init : boolean;
begin
    readln(N , V);
    init := (N + V <> 0);
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := 0 to N do
      if odd(i) = odd(N) then
        answer := answer + data[i , V];
end;

procedure print(num : extended);
begin
    if num = 0
      then exit
      else begin
               print(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0); 
           end;
end;

procedure out;
begin
    print(answer);
    if answer = 0 then write(0);
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
