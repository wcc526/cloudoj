#include <stdio.h>

int main()
{
  long long b, m, p, r;

  while (scanf("%lld %lld %lld", &b, &p, &m) == 3) {
    for (r = 1; p; p >>= 1, b = b * b % m)
      if (p & 1)
        r = r * b % m;
    printf("%lld\n", r);
  }

  return 0;
}
