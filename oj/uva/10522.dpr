{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10522.in';
    OutFile    = 'p10522.out';

Var
    ha , hb , hc ,
    A , B , C  : extended;
    last       : longint;

procedure init;
begin
    read(ha , hb , hc);
end;

procedure workout;
var
    a1 , b1 , c1 ,
    p , k      : extended;
begin
    if (hb <= 0) or (hc <= 0) then
      begin
          writeln('These are invalid inputs!');
          dec(last);
          exit;
      end;
    a1 := 1; b1 := ha / hb; c1 := ha / hc;
    if (a1 < b1 + c1) and (b1 < c1 + a1) and (c1 < a1 + b1)
      then begin
               p := (a1 + b1 + c1) / 2;
               k := ha / 2 / sqrt(p * (p - a1) * (p - b1) * (p - c1));
               A := a1 * k;
               B := b1 * k;
               C := c1 * k;
               writeln(A * ha / 2 : 0 : 3); 
           end
      else begin
               writeln('These are invalid inputs!');
               dec(last);
               exit;
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(last);
      while last > 0 do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
