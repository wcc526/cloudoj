#include <cstdio>
#include <cmath>
const double EPS = 1e-7;
int x[25], y[25], n, i, len, px, py, kase=0;
double ra, rb, r;
char dr;
bool check(double ox, double oy) {
	int i, s;
	s = 0;
	for (i = 0; i < n; ++i)
		if (x[i] > ox && ((y[i] > oy) ^ (y[i + 1] > oy)))
			++s;
	if (s % 2 == 0)
		return 0;
	for (i = 0; i < n; ++i) {
		if ((x[i] - ox) * (x[i] - ox) + (y[i] - oy) * (y[i] - oy)
				< (r - EPS) * (r - EPS))
			return 0;
		if (x[i] == x[i + 1] && ((y[i] > oy) ^ (y[i + 1] > oy))
				&& fabs(x[i] - ox) < r - EPS)
			return 0;
		if (y[i] == y[i + 1] && ((x[i] > ox) ^ (x[i + 1] > ox))
				&& fabs(y[i] - oy) < r - EPS)
			return 0;
	}
	return 1;
}
bool ok() {
	int i, j;
	double di, dd, mx, my, dx, dy;
	for (i = 0; i < n; ++i)
		if (x[i] == x[i + 1])
			for (j = 0; j < n; ++j)
				if (y[j] == y[j + 1]) {
					if (check(x[i] + r, y[j] + r))
						return 1;
					if (check(x[i] + r, y[j] - r))
						return 1;
					if (check(x[i] - r, y[j] + r))
						return 1;
					if (check(x[i] - r, y[j] - r))
						return 1;
				}
	for (i = 0; i < n; ++i)
		for (j = 0; j < n; ++j)
			if (x[i] == x[i + 1]) {
				di = fabs(x[j] - (x[i] + r));
				if (di < r) {
					dd = sqrt(r * r - di * di);
					if (check(x[i] + r, y[j] + dd))
						return 1;
					if (check(x[i] + r, y[j] - dd))
						return 1;
				}
				di = fabs(x[j] - (x[i] - r));
				if (di < r) {
					dd = sqrt(r * r - di * di);
					if (check(x[i] - r, y[j] + dd))
						return 1;
					if (check(x[i] - r, y[j] - dd))
						return 1;
				}
			} else {
				di = fabs(y[j] - (y[i] + r));
				if (di < r) {
					dd = sqrt(r * r - di * di);
					if (check(x[j] + dd, y[i] + r))
						return 1;
					if (check(x[j] - dd, y[i] + r))
						return 1;
				}
				di = fabs(y[j] - (y[i] - r));
				if (di < r) {
					dd = sqrt(r * r - di * di);
					if (check(x[j] + dd, y[i] - r))
						return 1;
					if (check(x[j] - dd, y[i] - r))
						return 1;
				}
			}
	for (i = 0; i < n - 1; ++i)
		for (j = i + 1; j < n; ++j) {
			mx = (x[i] + x[j]) / 2.0;
			my = (y[i] + y[j]) / 2.0;
			di = sqrt((x[i] - mx) * (x[i] - mx) + (y[i] - mx) * (y[i] - mx));
			if (di > 0 && di < r) {
				dd = sqrt(r * r - di * di);
				dx = (my - y[i]) / di * dd;
				dy = (x[i] - mx) / di * dd;
				if (check(mx + dx, my + dy))
					return 1;
				if (check(mx - dx, my - dy))
					return 1;
			}
		}
	return 0;
}
int main() {
	//freopen("in.txt", "r", stdin);
	//puts("ok");
	while(scanf("%d",&n)&&n)
	{
		if(kase) printf("\n");
		px=0;py=0;
		for(i=1;i<=n;++i)
		{
			scanf("%d %c",&len,&dr);
			if(dr=='R') px+=len;
			if(dr=='L') px-=len;
			if(dr=='U') py+=len;
			if(dr=='D') py-=len;
			x[i]=px;
			y[i]=py;
		}
		ra=0;rb=999;
		while(rb-ra>EPS)
		{
			r=(rb+ra)/2;
			if(ok()) ra=r;
			else rb=r;
		}
		printf("Case Number %d radius is: %.2lf\n",++kase,r);
	}
	
	return 0;
}
