/*
 * uva_11292.cpp
 *
 *  Created on: 2012-12-8
 *      Author: Administrator
 */
#include <cstdio>
#include <algorithm>
using namespace std;

const int MXN=20000+5;
int A[MXN],B[MXN];
int N,M;
int main()
{
//	freopen("in.txt","r",stdin);
	while(~scanf("%d%d",&N,&M))
	{
		if(N==0) break;
		for(int i=0;i<N;++i) scanf("%d",&A[i]);
		for(int i=0;i<M;++i) scanf("%d",&B[i]);
		sort(A,A+N);
		sort(B,B+M);
		int cur=0;
		int cost=0;
		for(int i=0;i<M;++i)
		{
			if(B[i]>=A[cur])
			{
				cost+=B[i];
				if(++cur==N) break;
			}
		}
		if(cur<N) puts("Loowater is doomed!");
		else printf("%d\n",cost);
	}
	return 0;
}






