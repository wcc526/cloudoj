{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10695.in';
    OutFile    = 'p10695.out';
    minimum    = 1e-12;

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    TLine      = record
                     A , B , C               : extended;
                 end;

Var
    A , B , C  : Tpoint;
    cases      : longint;
    dA , dB , dC ,
    dA1 , dB1 , dC1 
               : extended;

function zero(num : extended) : boolean;
begin
    zero := abs(num) <= minimum;
end;

function init : boolean;
begin
    readln(A.x , A.y , B.x , B.y , C.x , C.y , dC , dA , dB);
    if zero(A.x) and zero(A.y) and zero(B.x) and zero(B.y) and zero(C.x) and zero(C.y)
      then init := false
      else begin
               init := true;
               inc(cases);
               writeln('Case ' , cases , ':');
           end;
end;

function dist(p1 , p2 : Tpoint) : extended;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function acos(cosA : extended) : extended;
begin
    if zero(cosA)
      then acos := pi / 2
      else if cosA < 0
             then acos := pi - arctan(sqrt(1 - sqr(cosA)) / -cosA)
             else acos := arctan(sqrt(1 - sqr(cosA)) / cosA); 
end;

procedure Get_Angle(p1 , p2 , p3 : Tpoint; var angle : extended);
var
    cosA ,
    L12 , L23 , L13
               : extended;
begin
    L12 := dist(p1 , p2);
    L23 := dist(p2 , p3);
    L13 := dist(p1 , p3);
    cosA := (sqr(L12) + sqr(L23) - sqr(L13)) / 2 / L12 / L23;
    angle := acos(cosA);
end;

procedure turn(sinA , cosA , sinB , cosB : extended; var sinC , cosC : extended);
begin
    sinC := sinA * cosB + cosA * sinB;
    cosC := cosA * cosB - sinA * sinB;
end;

procedure Get_Crossing(L1 , L2 : TLine; var p : Tpoint);
begin
    p.x := (L1.C * L2.B - L2.C * L1.B) / (L2.A * L1.B - L1.A * L2.B);
    p.y := (L1.C * L2.A - L2.C * L1.A) / (L1.A * L2.B - L2.A * L1.B);
end;

procedure workout;
var
    start , stop ,
    alpha , beta ,
    tmp        : extended;
    p , tmpP , 
    dirA , dirB: Tpoint;
    L1 , L2    : TLine;
    s1 , s2    : string;
begin
    if (B.x - A.x) * (C.y - A.y) - (B.y - A.y) * (C.x - A.x) < 0 then
      begin
          tmpP := B; B := C; C := tmpP;
          tmp := dB; dB := dC; dC := tmp;
      end;
    dA := dA / 180 * pi;
    dB := dB / 180 * pi;
    dC := dC / 180 * pi;
    Get_Angle(C , A , B , dA1);
    Get_Angle(A , B , C , dB1);
    Get_Angle(B , C , A , dC1);
    if not zero(dA + dB + dC - pi * 2) or (dA <= dA1 + minimum) or (dB <= dB1 + minimum) or (dC <= dC1 + minimum)
      or (dA >= pi - minimum) or (dB >= pi - minimum) or (dC >= pi - minimum)
      then begin
               writeln('IMPOSSIBLE');
               exit;
           end;
    start := 0; stop := dA1;
    if pi - dC - dA1 < 0 then stop := pi - dC;
    while start + minimum <= stop do
      begin
          alpha := (start + stop) / 2;
          beta := pi - alpha - dC;
          turn((B.y - A.y) / dist(A , B) , (B.x - A.x) / dist(A , B) , sin(alpha) , cos(alpha) , dirA.y , dirA.x);
          turn((A.y - B.y) / dist(A , B) , (A.x - B.x) / dist(A , B) , sin(-beta) , cos(-beta) , dirB.y , dirB.x);
          L1.A := dirA.y; L1.B := -dirA.x; L1.C := A.y * dirA.x - A.x * dirA.y;
          L2.A := dirB.y; L2.B := -dirB.x; L2.C := B.y * dirB.x - B.x * dirB.y;
          Get_Crossing(L1 , L2 , p);
          Get_Angle(A , p , C , tmp);
          if tmp < dB
            then start := alpha
            else stop := alpha;
      end;
    str(p.x : 0 : 6 , s1); if s1 = '-0.000000' then s1 := '0.000000';
    str(p.y : 0 : 6 , s2); if s2 = '-0.000000' then s2 := '0.000000';
    writeln(s1 , ' ' , s2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        workout;
//    Close(OUTPUT);
//    Close(INPUT);
End.
