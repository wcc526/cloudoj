Var
    N , i      : longint;
    ans        : extended;

begin
    while not eof do
      begin
          readln(N);
          if (N > 13) or (N < 0) and odd(N)
            then writeln('Overflow!')
            else begin
                     ans := 1;
                     for i := 1 to N do
                       ans := ans * i;
                     if ans < 10000
                       then writeln('Underflow!')
                       else writeln(ans : 0 : 0);
                 end;
      end;
End.