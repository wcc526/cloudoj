#include <stdio.h>

#define u(x, y) (x < y ? x : y)
#define v(x, y, z) u(u(x, y), z)

int main()
{
  int c = 0, i, j, k, n, t;
  short a[1000][1000];
  char s[1001];

  for (a[0][0] = 0, i = 1000; --i;)
    a[i][i] = a[i][i - 1] = 0;

  for (scanf("%d\n", &t); t-- && scanf("%s%n\n", s, &n);
       printf("Case %d: %hd\n", ++c, a[0][n - 1]))
    for (k = 1; k < n; ++k)
      for (i = 0, j = k; j < n; ++i, ++j)
        a[i][j] = s[i] == s[j] ? a[i + 1][j - 1] :
          1 + v(a[i + 1][j], a[i + 1][j - 1], a[i][j - 1]);

  return 0;
}
