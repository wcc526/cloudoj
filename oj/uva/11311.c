#include <stdio.h>

int main()
{
  int m, n, r, c, t;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %d %d %d", &m, &n, &r, &c);
    if (r ^ c ^ (m - r - 1) ^ (n - c - 1))
      puts("Gretel");
    else
      puts("Hansel");
  }

  return 0;
}
