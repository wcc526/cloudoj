Const
    Limit      = 5000;

Type
    Tprimes    = array[1..Limit] of longint;

Var
    primes     : Tprimes;
    i , j ,
    p1 , p2 ,
    N , M , T ,
    answer ,
    cases ,
    tot        : longint;

function check(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to trunc(sqrt(num)) do
      if num mod i = 0 then
        exit(false);
    exit(true);
end;

procedure get_prime;
var
    i          : longint;
begin
    tot := 0;
    for i := 2 to Limit do
      if check(i) then
        begin
            inc(tot);
            primes[tot] := i;
        end;
end;

function process(num , p : longint) : longint;
var
    res        : longint;
begin
    res := 0;
    while num mod p = 0 do
      begin
          inc(res);
          num := num div p;
      end;
    exit(res);
end;

Begin
    Get_Prime;
    readln(T);
    for cases := 1 to T do
      begin
          writeln('Case ' , cases , ':');
          readln(M , N);
          answer := -1;
          for i := 1 to tot do
            if (primes[i] <= M)
              then begin
                       if M mod primes[i] <> 0 then continue;
                       p1 := process(M , primes[i]);
                       p2 := 0;
                       for j := 2 to N do
                         inc(p2 , process(j , primes[i]));
                       if (answer = -1) or (answer > p2 div p1) then
                         answer := p2 div p1;
                   end
              else break;
          if answer = 0
            then writeln('Impossible to divide')
            else writeln(answer);
      end;
End.