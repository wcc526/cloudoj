#include <ctype.h>
#include <stdio.h>

int main(void)
{
  int c, i = 0;

  while ((c = getchar()) > 0) {
    if (isalpha(c)) {
      while ((c = getchar()) > 0 && isalpha(c));
      ++i;
    }
    if (c == '\n')
      printf("%d\n", i), i = 0;
  }

  return 0;
}
