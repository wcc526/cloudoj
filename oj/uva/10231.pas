Const
    InFile     = 'p10231.in';
    Limit      = 30;
    LimitT     = 10;
    maximum    = 10000;
    LimitQueue = 100000;
    LimitNode  = (1 shl LimitT) * (LimitT + 1);
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tmap       = array[1..Limit , 1..Limit] of char;
    Tdata      = array[1..LimitT] of
                   record
                       x , y , maxtime       : longint;
                       shortest              : array[1..Limit , 1..Limit] of longint;
                   end;
    Topt       = array[0..LimitNode] of longint;
    Tcount     = array[0..1 shl LimitT] of longint;

Var
    map        : Tmap;
    data       : Tdata;
    opt        : Topt;
    count      : Tcount;
    answer , time ,
    startx , starty ,
    cases , Base ,
    N , M , T  : longint;

procedure init;
var
    i , j      : longint;
begin
    readln(N , M);
    for i := 1 to N do
      begin
          for j := 1 to M do read(map[i , j]);
          readln;
      end;
end;

procedure bfs(T : longint);
type
    Tqueue     = array[1..Limit * Limit] of
                   record
                       x , y  : longint;
                   end;
var
    queue      : Tqueue;
    i , j ,
    nx , ny ,
    open , closed
               : longint;
begin
    with data[T] do
      begin
          fillchar(shortest , sizeof(shortest) , 1);
          queue[1].x := x; queue[1].y := y; open := 1; closed := 1;
          shortest[x , y] := 0;
          while open <= closed do
            begin
                for i := 1 to 4 do
                  begin
                      nx := queue[open].x + dirx[i]; ny := queue[open].y + diry[i];
                      if (nx <= N) and (nx >= 1) and (ny <= M) and (ny >= 1) then
                        if map[nx , ny] <> '#' then
                          if shortest[nx , ny] > shortest[queue[open].x , queue[open].y] + 1 then
                            begin
                                inc(closed); queue[closed].x := nx; queue[closed].y := ny;
                                shortest[nx , ny] := shortest[queue[open].x , queue[open].y] + 1;
                            end;
                  end;
                inc(open);
            end;
          maxtime := maximum;
          for i := 1 to N do
            for j := 1 to M do
              if (map[i , j] = 'X') and (shortest[i , j] < maxtime) then
                maxtime := shortest[i , j];
      end;
end;

procedure pre_process;
var
    i , j      : longint;
begin
    T := 0;
    for i := 1 to N do
      for j := 1 to M do
        if map[i , j] = '*' then
          begin
              inc(T);
              data[T].x := i; data[T].y := j;
              bfs(T);
          end
        else
          if map[i , j] = 'O' then
            begin startx := i; starty := j; end;
end;

procedure work;
type
    Tqueue     = array[1..LimitQueue] of longint;
    Tinqueue   = array[0..LimitNode] of boolean;
var
    queue      : Tqueue;
    inqueue    : Tinqueue;
    open , closed ,
    nowx , nowy ,
    i , p , father ,
    stay , tmp ,
    newstatus  : longint;
begin
    pre_process;
    count[0] := 0;
    for i := 1 to 1 shl LimitT do count[i] := count[i div 2] + i mod 2;
    Base := 1 shl T;

    answer := 0; time := 0;
    fillchar(opt , sizeof(opt) , $FF); opt[0] := 0;
    open := 1; closed := 1; queue[1] := 0;
    fillchar(inqueue , sizeof(inqueue) , 0); inqueue[1] := true;
    while open <= closed do
      begin
          p := queue[open]; inqueue[p] := false; father := opt[p];
          stay := p div Base; p := p mod Base;
          if (count[p] > answer) or (count[p] = answer) and (father < time) then
            begin answer := count[p]; time := father; end;

          if stay = 0
            then begin nowx := startx; nowy := starty; end
            else begin nowx := data[stay].x; nowy := data[stay].y; end;
          for i := 1 to T do
            if (1 shl (i - 1)) and p = 0 then
              begin
                  tmp := father + data[i].shortest[nowx , nowy] + 1;
                  if tmp < data[i].maxtime then
                    begin
                        newstatus := 1 shl (i - 1) or p + i * Base;
                        if (opt[newstatus] = -1) or (opt[newstatus] > tmp) then
                          begin
                              opt[newstatus] := tmp;
                              if not inqueue[newstatus] then
                                begin
                                    inc(closed); queue[closed] := newstatus;
                                    inqueue[newstatus] := true;
                                end;
                          end;
                    end;
              end;
          inc(open);
      end;
end;

procedure out;
begin
    inc(Cases);
    writeln('Case ' , cases , ':');
    if answer = 0
      then writeln('No treasures can be collected.')
      else begin
               writeln('Maximum number of collectible treasures: ' , answer , '.');
               writeln('Minimum Time: ' , time , ' sec.');
           end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.