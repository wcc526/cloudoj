Var
    x0 , y0 ,
    x1 , y1 ,
    tot        : extended;
    tmp ,
    cases      : longint;
Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          tot := 0; readln(x0 , y0);
          while not eof and not eoln do
            begin
                readln(x0 , y0 , x1 , y1);
                tot := tot + sqrt(sqr(x0 - x1) + sqr(y0 - y1)) * 2;
            end;
          tot := round(tot / 1000 / 20 * 60);
          tmp := trunc(tot - int(tot / 60) * 60);
          writeln(int(tot / 60) : 0 : 0 , ':' , tmp div 10 , tmp mod 10);
          if cases <> 0 then writeln;
      end;
End.