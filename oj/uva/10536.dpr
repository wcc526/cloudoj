{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10536.in';
    OutFile    = 'p10536.out';

Type
    Tmap       = array[1..4 , 1..4] of boolean;
    Topt       = array[0..65535] of boolean;

Var
    map        : Tmap;
    opt        : Topt;
    Cases      : longint;

procedure decode(num : longint; var map : Tmap);
var
    i , j      : longint;
begin
    for i := 4 downto 1 do
      for j := 4 downto 1 do
        begin
            map[i , j] := odd(num);
            num := num div 2;
        end;
end;

function encode(map : Tmap) : longint;
var
    i , j , num: longint;
begin
    num := 0;
    for i := 1 to 4 do
      for j := 1 to 4 do
        num := num * 2 + ord(map[i , j]);
    encode := num;
end;

function check(map : Tmap) : boolean;
var
    i , j      : longint;
begin
    check := true;
    for i := 1 to 4 do
      begin
          for j := 1 to 4 do
            if not map[i , j] then
              begin
                  map[i , j] := true;
                  if not opt[encode(map)] then exit;
                  map[i , j] := false;
              end;
              
          if not map[i , 1] and not map[i , 2] then
            begin
                map[i , 1] := true; map[i , 2] := true;
                if not opt[encode(map)] then exit;
                if not map[i , 3] then
                  begin
                      map[i , 3] := true;
                      if not opt[encode(map)] then exit;
                      map[i , 3] := false;
                  end;
                map[i , 1] := false; map[i , 2] := false;
            end;

          if not map[i , 4] and not map[i , 3] then
            begin
                map[i , 4] := true; map[i , 3] := true;
                if not opt[encode(map)] then exit;
                if not map[i , 2] then
                  begin
                      map[i , 2] := true;
                      if not opt[encode(map)] then exit;
                      map[i , 2] := false;
                  end;
                map[i , 4] := false; map[i , 3] := false;
            end;

          if not map[1 , i] and not map[2 , i] then
            begin
                map[1 , i] := true; map[2 , i] := true;
                if not opt[encode(map)] then exit;
                if not map[3 , i] then
                  begin
                      map[3 , i] := true;
                      if not opt[encode(map)] then exit;
                      map[3 , i] := false;
                  end;
                map[1 , i] := false; map[2 , i] := false;
            end;

          if not map[4 , i] and not map[3 , i] then
            begin
                map[4 , i] := true; map[3 , i] := true;
                if not opt[encode(map)] then exit;
                if not map[2 , i] then
                  begin
                      map[2 , i] := true;
                      if not opt[encode(map)] then exit;
                      map[2 , i] := false;
                  end;
                map[4 , i] := false; map[3 , i] := false;
            end;
      end;
    check := false;
end;

procedure pre_process;
var
    source     : Tmap;
    i          : longint;
begin
    opt[65535] := true;
    for i := 65534 downto 0 do
      begin
          decode(i , source);
          opt[i] := check(source);
      end;
end;

procedure work;
var
    i , j      : longint;
    c          : char;
begin
    readln;
    for i := 1 to 4 do
      begin
          for j := 1 to 4 do
            begin
                read(c);
                map[i , j] := (c = 'X');
            end;
          readln;
      end;
    if opt[encode(map)]
      then writeln('WINNING')
      else writeln('LOSING');
    dec(Cases);
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        work;
//    Close(OUTPUT);
//    Close(INPUT);
End.
