{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10562.in';
    OutFile    = 'p10562.out';
    Limit      = 201;

Type
    Tdata      = array[-1..Limit + 1 , -1..Limit + 1] of char;

Var
    data       : Tdata;
    N , cases  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(cases);
    fillchar(data , sizeof(data) , 32);
    i := 1;
    while true do
      begin
          j := 1;
          while not eoln do
            begin
                read(data[i , j]);
                inc(j);
            end;
          readln;
          if data[i , 1] = '#' then
            begin
                data[i , 1] := #32;
                break;
            end;
          inc(i);
      end;
    N := i - 1;
end;

procedure dfs_print(x , y : longint);
var
    left , right ,
    i          : longint;
begin
    write(data[x , y]);
    write('(');
    data[x , y] := #32;
    if data[x + 1 , y] = '|'
      then begin
               data[x + 1 , y] := #32;
               left := y; right := y;
               while data[x + 2 , left - 1] = '-' do dec(left);
               while data[x + 2 , right + 1] = '-' do inc(right);
               for i := left to right do
                 begin
                     data[x + 2 , i] := #32;
                     if data[x + 3 , i] <> #32 then
                       dfs_print(x + 3 , i);
                 end;
           end;
    write(')');
end;

procedure workout;
var
    i , j      : longint;
begin
    write('(');
    for i := 1 to N do
      for j := 1 to Limit do
        if data[i , j] <> #32 then
          dfs_print(i , j);
    writeln(')');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
