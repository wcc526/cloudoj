{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10593.in';
    OutFile    = 'p10593.out';
    Limit      = 500;

Type
    Tdata      = array[0..Limit + 1 , 0..Limit + 1] of boolean;
    Topt       = array[0..Limit + 1 , 0..Limit + 1] of longint;

Var
    data       : Tdata;
    opt        : Topt;
    N , answer : longint;

procedure init;
var
    i , j      : longint;
    ch         : char;
begin
    fillchar(data , sizeof(data) , 0);
    readln(N);
    for i := 1 to N do
      begin
          for j := 1 to N do
            begin
                read(ch);
                data[i , j] := ch = 'x';
            end;
          readln;
      end;
end;

function min(a , b , c : longint) : longint;
begin
    if a > b then a := b;
    if a > c then min := c else min := a;
end;

procedure work;
var
    i , j      : longint;
begin
    answer := 0;
    fillchar(opt , sizeof(opt) , 0);
    for i := 1 to N do
      for j := 1 to N do
        if data[i , j] then
          begin
              opt[i , j] := min(opt[i - 1 , j] , opt[i , j - 1] , opt[i - 1 , j - 1]) + 1;
              inc(answer , opt[i , j] - 1);
          end;
    for i := 1 to N do
      for j := 1 to N do
        if data[i , j] then
          begin
              if data[i - 1 , j]
                then opt[i , j] := min(opt[i - 2 , j] , opt[i - 1 , j - 1] , opt[i - 1 , j + 1]) + 1
                else opt[i , j] := 1;
              inc(answer , opt[i , j] - 1);
          end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
