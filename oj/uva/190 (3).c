#include <math.h>
#include <stdio.h>

typedef struct {
  double x;
  double y;
} pt;

int main(void)
{
  pt p[3], b[2], v[2];
  double s, t;
  int i;

  while (scanf("%lf %lf %lf %lf %lf %lf",
               &p[0].x, &p[0].y, &p[1].x, &p[1].y, &p[2].x, &p[2].y) == 6) {
    for (i = 2; i--;) {
      t = 1 / sqrt((p[i].x - p[i + 1].x) * (p[i].x - p[i + 1].x) +
                   (p[i].y - p[i + 1].y) * (p[i].y - p[i + 1].y));
      v[i].x = (p[i].y - p[i + 1].y) * t;
      v[i].y = (p[i + 1].x - p[i].x) * t;
      b[i].x = (p[i].x + p[i + 1].x) * 0.5;
      b[i].y = (p[i].y + p[i + 1].y) * 0.5;
    }
    s = (v[0].x * (b[0].y - b[1].y) - v[0].y * (b[0].x - b[1].x)) /
      (v[0].x * v[1].y - v[1].x * v[0].y);
    p[0].x = b[1].x + v[1].x * s;
    p[0].y = b[1].y + v[1].y * s;
    t = (p[0].x - p[1].x) * (p[0].x - p[1].x) +
      (p[0].y - p[1].y) * (p[0].y - p[1].y);
    printf("(x %c %.3lf)^2 + (y %c %.3lf)^2 = %.3lf^2\n",
           p[0].x < 0 ? '+' : '-', fabs(p[0].x),
           p[0].y < 0 ? '+' : '-', fabs(p[0].y), sqrt(t));
    t -= p[0].x * p[0].x + p[0].y * p[0].y;
    printf("x^2 + y^2 %c %.3lfx %c %.3lfy %c %.3lf = 0\n",
           p[0].x < 0 ? '+' : '-', 2 * fabs(p[0].x),
           p[0].y < 0 ? '+' : '-', 2 * fabs(p[0].y),
           t < 0 ? '+' : '-', fabs(t));
    putchar('\n');
  }

  return 0;
}
