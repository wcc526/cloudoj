{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10660.in';
    OutFile    = 'p10660.out';

Type
    Tdata      = array[1..25] of longint;
    Tanswer    = record
                     p1 , p2 , p3 , p4 , p5 ,
                     cost                                   : longint;
                 end;
    Tdist      = array[1..25 , 1..25] of longint;

Var
    data       : Tdata;
    answer     : Tanswer;
    dist       : Tdist;
    cases      : longint;

procedure pre_process;
var
    i , j ,
    x1 , y1 ,
    x2 , y2    : longint;
begin
    for i := 1 to 25 do
      for j := 1 to 25 do
        begin
            x1 := (i - 1) mod 5; y1 := (i - 1) div 5;
            x2 := (j - 1) mod 5; y2 := (j - 1) div 5;
            dist[i , j] := abs(x1 - x2) + abs(y1 - y2);
        end;
end;

procedure init;
var
    p1 , p2 ,
    i , N      : longint;
begin
    dec(cases);
    read(N);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to N do
      begin
          read(p1 , p2);
          read(data[p1 * 5 + p2 + 1]);
      end;
end;

procedure work;
var
    i , sum , tmp ,
    p1 , p2 , p3 ,
    p4 , p5    : longint;
begin
    answer.cost := maxlongint;
    for p1 := 1 to 25 do
      for p2 := p1 + 1 to 25 do
        for p3 := p2 + 1 to 25 do
          for p4 := p3 + 1 to 25 do
            for p5 := p4 + 1 to 25 do
              begin
                  sum := 0;
                  for i := 1 to 25 do
                    begin
                        tmp := dist[i , p1];
                        if dist[i , p2] < tmp then tmp := dist[i , p2];
                        if dist[i , p3] < tmp then tmp := dist[i , p3];
                        if dist[i , p4] < tmp then tmp := dist[i , p4];
                        if dist[i , p5] < tmp then tmp := dist[i , p5];
                        inc(sum , tmp * data[i]);
                    end;
                  if sum < answer.cost then
                    begin
                        answer.cost := sum;
                        answer.p1 := p1; answer.p2 := p2; answer.p3 := p3;
                        answer.p4 := p4; answer.p5 := p5;
                    end;
              end;
end;

procedure out;
begin
    with answer do
      writeln(p1 - 1 , ' ' , p2 - 1 , ' ' , p3 - 1 , ' ' , p4 - 1 , ' ' , p5 - 1);
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
