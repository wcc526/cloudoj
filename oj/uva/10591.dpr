{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10591.in';
    OutFile    = 'p10591.out';
    LimitSave  = 1000;

Type
    Tdata      = array[1..LimitSave] of boolean;

Var
    data ,
    visited    : Tdata;
    N , nowCase ,
    cases      : longint;

function calc(num : longint) : longint;
begin
    if num = 0
      then calc := 0
      else calc := calc(num div 10) + sqr(num mod 10);
end;

function check(num : longint) : boolean;
begin
    if visited[num] then begin check := false; exit; end;
    visited[num] := true;
    if num = 1
      then check := true
      else check := check(calc(num));
end;

procedure pre_process;
var
    i          : longint;
begin
    for i := 1 to LimitSave do
      begin
          fillchar(visited , sizeof(visited) , 0);
          data[i] := check(i);
      end;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases); nowCase := 0;
      while nowCase < cases do
        begin
            inc(nowCase);
            readln(N);
            write('Case #' , nowCase , ': ' , N , ' is ');
            if data[calc(N)]
              then writeln('a Happy number.')
              else writeln('an Unhappy number.');
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
