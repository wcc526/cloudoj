#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define D 3.14159265358979 / 180.0
#define F 1.0 / 3600.0
#define R 10.0
#define T 5.0

#define G(i,j)                                  \
  x[i] = p[i][j]->d * cos(p[i][j]->a * D),      \
    y[i] = p[i][j]->d * sin(p[i][j]->a * D)

#define P(i,j,s) printf("%5s -- %s\n", p[i][j]->l, s)

typedef struct {
  short n;
  double a, d, s;
  char l[6];
} pt;

int ptcmp(const pt **a, const pt **b)
{
  return (*a)->n - (*b)->n;
}

int main(void)
{
  int c = 0, i, j, n[2];
  double d, x[2], y[2];
  pt *p[2][100], *t;

  for (;;) {
    for (i = 0; i < 2; ++i) {
      if (scanf("%d", &n[i]) < 1)
        return 0;
      for (j = n[i]; j--;) {
        t = p[i][j] = malloc(sizeof *t);
        scanf("%s %lf %lf %lf", t->l, &t->a, &t->d, &t->s);
        sscanf(t->l, "%hd", &t->n);
      }
    }

    printf("Scenario # %d\n", ++c);
    qsort(p[0], n[0], sizeof(t), (int (*)(const void *, const void *))ptcmp);
    qsort(p[1], n[1], sizeof(t), (int (*)(const void *, const void *))ptcmp);

    for (i = j = 0; i < n[0] && j < n[1];) {
      if (p[0][i]->n < p[1][j]->n) {
        P(0, i, p[0][i]->d + p[0][i]->s * F * T * 1.1 > R ?
          "domain exited" : "domain loss"), ++i;
      } else if (p[0][i]->n > p[1][j]->n) {
        P(1, j, p[1][j]->d + p[1][j]->s * F * T * 1.1 > R ?
          "new intrusion" : "new aloft"), ++j;
      } else {
        G(0, i);
        G(1, j);
        d = sqrt((x[0] - x[1]) * (x[0] - x[1]) +
                 (y[0] - y[1]) * (y[0] - y[1])) / T;
        if (fabs(0.5 * (p[0][i]->s + p[1][j]->s) * F - d) - d * 0.1 > 1e-12)
          P(0, i, "equipment warning");
        ++i, ++j;
      }
    }
    while (i < n[0])
      P(0, i, p[0][i]->d + p[0][i]->s * F * T * 1.1 > R ?
        "domain exited" : "domain loss"), ++i;
    while (j < n[1])
      P(1, j, p[1][j]->d + p[1][j]->s * F * T * 1.1 > R ?
        "new intrusion" : "new aloft"), ++j;

    putchar('\n');

    for (i = 2; i--;)
      for (j = n[i]; j--;)
        free(p[i][j]);
  }
}
