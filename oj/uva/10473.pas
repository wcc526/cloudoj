Const
    list       : array[0..15] of char
               = '0123456789ABCDEF';

Var
    s          : string[255];
    num , base : longint;

Begin
    readln(s);
    while s[1] <> '-' do
      begin
          if copy(s , 1 , 2) = '0x'
            then begin
                     delete(s , 1 , 2); base := 16;
                 end
            else base := 10;
          num := 0;
          while s <> '' do
            begin
                num := num * base;
                if s[1] in ['0'..'9']
                  then inc(num , ord(s[1]) - ord('0'))
                  else inc(num , ord(s[1]) - ord('A') + 10);
                delete(s , 1 , 1);
            end;
          base := 26 - base;
          while num <> 0 do
            begin
                s := list[num mod base] + s;
                num := num div base;
            end;
          if s = '' then s := '0';
          if base = 16 then s := '0x' + s;
          writeln(s);
          readln(s);
      end;
End.
