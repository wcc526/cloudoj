{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10430.in';
    OutFile    = 'p10430.out';
    LimitLen   = 200;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;

Var
    T , N      : longint;
    X , K      : lint;

procedure lint.init;
begin
    fillchar(data , sizeof(data) , 0);
    len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    init;
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
    if len = 0 then write(0);
end;

procedure init;
begin
    readln(T , N);
end;

procedure work;
var
    power ,
    newpower   : lint;
    i , j      : longint;
begin
    X.init; K.init;
    power.init; power.len := 1; power.data[1] := 1;
    for i := 1 to N do
      begin
          X.add(power , X);
          newpower.init;
          for j := 1 to T do
            newpower.add(power , newpower);
          power := newpower;
      end;
    K := power;
end;

procedure out;
begin
    write('X = '); X.print; writeln;
    write('K = '); K.print; writeln;
    if not eof then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      writeln('Dear GOD, Pardon Me');
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
