{ $R-,Q-,S-}
Const
    Limit      = 10000;
    LimitLen   = 12;
    LimitSave  = 200;
    Base       = 100000000;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure add(num1 , num2 : lint);
                     function compare(num : lint) : longint;
                     procedure print;
                 end;

Var
    choice ,
    i , j      : longint;
    data       : array[0..Limit] of lint;
    power      : array[0..LimitSave] of lint;
    tmp        : lint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw , tmp
               : longint;
begin
    i := 1; jw := 0; init;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div Base;
          data[i] := tmp mod Base;
          inc(i);
      end;
    len := i - 1;
end;

function lint.compare(num : lint) : longint;
var
    i          : longint;
begin
    if len <> num.len
      then exit(len - num.len)
      else for i := len downto 1 do
             if data[i] <> num.data[i]
               then exit(data[i] - num.data[i]);
    exit(0);
end;

procedure lint.print;
var
    i          : longint;
    s          : string[10];
begin
    write(data[len]);
    for i := len - 1 downto 1 do
      begin
          str(data[i] , s);
          while length(s) < 8 do s := '0' + s;
          write(s);
      end;
end;

Begin
    data[0].init;
    data[1].init; data[1].data[1] := 1;
    power[0].init;
    for i := 1 to LimitSave do
      begin
          power[i].add(power[i - 1] , power[i - 1]);
          power[i].add(power[i] , data[1]);
      end;

    for i := 2 to Limit do
      begin
          data[i].init; data[i].len := LimitLen;
          for j := i - 1 downto i - LimitSave + 1 do
            if j <= 0
              then break
              else begin
                       tmp.add(data[j] , data[j]);
                       tmp.add(power[i - j] , tmp);
                       if tmp.compare(data[i]) < 0 then
                         begin data[i] := tmp; choice := j; end;
                   end;
      end;

    while not eof do
      begin
          readln(i);
          data[i].print;
          writeln;
      end;
End.