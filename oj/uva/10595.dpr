{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10595.in';
    OutFile    = 'p10595.out';
    LimitSave  = 200;
    Limit      = 10000;
    dirx       : array[1..6] of longint = (-1 , -1 , 0 , 1 , 1 , 0);
    diry       : array[1..6] of longint = (0 , -1 , -1 , 0 , 1 , 1);
    dx         : array[1..12] of longint = (-1 , -2 , -3 , -3 , -2 , -1 , 1 , 2 , 3 , 3 , 2 , 1);
    dy         : array[1..12] of longint = (2 , 1 , -1 , -2 , -3 , -3 , -2 , -1 , 1 , 2 , 3 , 3);

Type
    Tdata      = array[-LimitSave..LimitSave , -LimitSave..LimitSave] of longint;
    Tgraph     = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..12] of longint;
                   end;
    Tvisited   = array[1..Limit] of boolean;
    Tminstep   = array[1..Limit] of longint;
    Tqueue     = array[1..Limit] of longint;

Var
    data       : Tdata;
    graph      : Tgraph;
    visited ,
    blocked    : Tvisited;
    minstep , f: Tminstep;
    queue      : Tqueue;
    answer , 
    N , S , T  : longint;

function read_num(var num : longint) : boolean;
var
    ch         : char;
begin
    while not eof do
      begin
          read(ch);
          if ch in ['0'..'9'] then break;
      end;
    read_num := false;
    if eof then exit;
    read_num := true;
    num := 0;
    while ch in ['0'..'9'] do
      begin
          num := num * 10 + ord(ch) - ord('0');
          if eof then break else read(ch);
      end;
end;

procedure pre_process;
var
    x , y ,
    i , dir , k ,
    nx , ny    : longint;
begin
    fillchar(data , sizeof(data) , 0);
    data[0 , 0] := 1;
    x := -1; y := 0; dir := 3;
    for i := 2 to Limit do
      begin
          data[x , y] := i;
          if data[x + dirx[dir mod 6 + 1] , y + diry[dir mod 6 + 1]] = 0
            then dir := dir mod 6 + 1;
          x := x + dirx[dir]; y := y + diry[dir];
      end;
    for x := -LimitSave + 10 to LimitSave - 10 do
      for y := -LimitSave + 10 to LimitSave - 10 do
        if data[x , y] <> 0 then
          for k := 1 to 12 do
            begin
                nx := x + dx[k]; ny := y + dy[k];
                if data[nx , ny] <> 0 then
                  begin
                      inc(graph[data[x , y]].tot);
                      graph[data[x , y]].data[graph[data[x , y]].tot] := data[nx , ny];
                  end;
            end;
end;

function init : boolean;
var
    i , p , M  : longint;
begin
    if not read_num(N) then begin init := false; exit; end;
    init := true;
    read(M);
    fillchar(blocked , sizeof(blocked) , 0);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(minstep , sizeof(minstep) , 0);
    for i := 1 to M do
      begin
          read(p);
          blocked[p] := true;
      end;
    readln(S , T);
end;

procedure work;
var
    open , closed ,
    p , i      : longint;
begin
    answer := -1;
    open := 1; closed := 1; queue[1] := S; minstep[S] := 0; visited[S] := true;
    while open <= closed do
      begin
          p := queue[open];
          for i := 1 to graph[p].tot do
            if not visited[graph[p].data[i]] and not blocked[graph[p].data[i]] and (graph[p].data[i] <= N) then
              begin
                  inc(closed);
                  queue[closed] := graph[p].data[i];
                  minstep[graph[p].data[i]] := minstep[p] + 1;
                  visited[graph[p].data[i]] := true;
                  f[graph[p].data[i]] := p;
              end;
          if visited[T] then
            begin answer := minstep[T]; break; end;
          inc(open);
      end;
end;

procedure out;
begin
    if answer = -1
      then writeln('Impossible.')
      else writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
