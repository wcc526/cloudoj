#include <stdio.h>
#include <string.h>

int main()
{
  char b[128], c[11680], w, l[] = { 'A', 'M', 'T', 'W', 'R', 'F', 'Z' };
  short o[] = { 0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
  int d, f, h1, h2, i, j, k, m, n, p, m1, m2, t;

  memset(c, 1, sizeof(c));
  gets(b);
  sscanf(b, "%c %d %d", &w, &m, &d);
  for (i = 7; w ^ l[--i];);
  p = o[m] + d - 1;
  w = (7 + i - p % 7) % 7;
  p <<= 5;
  gets(b);
  sscanf(b, "%d %d", &n, &t);
  t /= 15;
  for (;;) {
    gets(b);
    if (strcmp(b, "done") == 0)
      break;
    for (;;) {
      gets(b);
      if (strcmp(b, "done") == 0)
        break;
      sscanf(b, "%*c %d %d %2d%2d %2d%2d", &m, &d, &h1, &m1, &h2, &m2);
      d = (o[m] + d - 1) << 5;
      h1 = ((h1 - 9) << 2) + m1 / 15;
      h2 = ((h2 - 9) << 2) + m2 / 15;
      for (i = h1; i < h2; ++i)
        c[d + i] = 0;
    }
  }
  for (i = p; i < sizeof(c) && n; i += 32) {
    if (((i >> 5) + w) % 7 == 0 || ((i >> 5) + w) % 7 == 6)
      continue;
    for (f = j = 0; j < 32 && n; ++j) {
      if (c[i + j]) {
        if (++f == t) {
          f = 0, --n;
          d = i + j - t + 1;
          m1 = d & 31;
          h1 = m1 >> 2;
          m1 = m1 & 3;
          d = d >> 5;
          for (k = 12; o[k] > d; --k);
          printf("%c %d %d %02d%02d\n", l[(d + w) % 7],
                 k, d + 1 - o[k], h1 + 9, m1 * 15);
        }
      } else {
        f = 0;
      }
    }
  }
  w = (w + 365) % 7;
  for (i = 0; i < p && n; i += 32) {
    if (((i >> 5) + w) % 7 == 0 || ((i >> 5) + w) % 7 == 6)
      continue;
    for (f = j = 0; j < 32 && n; ++j) {
      if (c[i + j]) {
        if (++f == t) {
          f = 0, --n;
          d = i + j - t + 1;
          m1 = d & 31;
          h1 = m1 >> 2;
          m1 = m1 & 3;
          d = d >> 5;
          for (k = 12; o[k] > d; --k);
          printf("%c %d %d %02d%02d\n", l[(d + w) % 7],
                 k, d + 1 - o[k], h1 + 9, m1 * 15);
        }
      } else {
        f = 0;
      }
    }
  }
  if (n)
    puts("No more times available.");

  return 0;
}
