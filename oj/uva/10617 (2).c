#include <stdio.h>

int main()
{
  long long a[60][60];
  int n, t;
  short c, i, j, k;
  char s[61];

  for (a[59][59] = a[58][58] = 2, a[59][58] = 1, i = 58; i--;)
    a[i][i] = 2, a[i + 1][i] = a[i + 2][i] = 1;

  for (scanf("%d\n", &t); t--; printf("%lld\n", a[0][n - 1] - 1))
    for (scanf("%s%n\n", s, &n), c = 1; c < n; ++c)
      for (i = 0, j = c; j < n; ++i, ++j)
        for (a[i][j] = a[i + 1][j], k = i; k <= j; ++k)
          if (s[i] == s[k])
            a[i][j] += k > 0 ? a[i + 1][k - 1] : 1;

  return 0;
}

/* #include <string.h> */

/* long long a[60][60]; */
/* char s[61]; */

/* long long p(short i, short j) */
/* { */
/*   long long r; */
/*   short k; */
/*   if (i > j) */
/*     return 1; */
/*   if (i == j) */
/*     return 2; */
/*   if (a[i][j] > 0) */
/*     return a[i][j]; */
/*   r = p(i + 1, j); */
/*   for (k = i; k <= j; ++k) */
/*     if (s[i] == s[k]) */
/*       r += p(i + 1, k - 1); */
/*   a[i][j] = r; */
/*   return r; */
/* } */

/* int main() */
/* { */
/*   int n, t; */

/*   scanf("%d\n", &t); */
/*   while (t--) { */
/*     scanf("%s%n\n", s, &n); */
/*     memset(a, 0xFF, sizeof(a)); */
/*     printf("%lld\n", p(0, n - 1) - 1); */
/*   } */

/*   return 0; */
/* } */
