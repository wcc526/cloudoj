#include <stdio.h>
#include <string.h>

int main()
{
  int c, i, j, m, n, p[20], r[20], s, t;

  scanf("%d", &t);
  while (t--) {
    scanf("%d", &n);
    memset(r, 0, n * sizeof(*r));
    for (m = i = 0; i < n; ++i)
      scanf("%d", p + i), m += p[i];
    for (m >>= 1, c = 1 << n; --c;) {
      for (s = j = 0, i = c; i; i >>= 1, ++j)
        if (i & 1)
          s += p[j];
      if (s > m)
        for (j = 0, i = c; i; i >>= 1, ++j)
          if (i & 1 && s - p[j] <= m)
            ++r[j];
    }
    for (i = 0; i < n; ++i)
      printf("party %d has power index %d\n", i + 1, r[i]);
    putchar('\n');
  }

  return 0;
}
