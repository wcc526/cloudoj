Const
    InFile     = 'p10441.in';
    OutFile    = 'p10441.out';
    Limit      = 1000;

Type
    Tstr       = string[50];
    Tdata      = array[1..Limit] of Tstr;
    Tindex     = array['a'..'z'] of longint;

Var
    answer ,
    data       : Tdata;
    degree ,
    index      : Tindex;
    N , tot , T: longint;

procedure init;
var
    i          : longint;
begin
    dec(T); readln(N);
    for i := 1 to N do readln(data[i]);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tstr;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure dfs_print(ch : char; father : longint);
var
    p          : longint;
begin
    while (index[ch] <> 0) and (index[ch] <= N) and (data[index[ch] , 1] = ch) do
      begin
          inc(index[ch]);
          dfs_print(data[index[ch] - 1 , length(data[index[ch] - 1])] , index[ch] - 1);
      end;
    if father <> 0
      then begin inc(tot); answer[tot] := data[father]; end;
end;

procedure work;
var
    i          : longint;
    ch , c2    : char;
begin
    fillchar(index , sizeof(index) , 0);
    fillchar(degree , sizeof(degree) , 0);
    for i := 1 to N do
      begin
          inc(degree[data[i , 1]]);
          dec(degree[data[i , length(data[i])]]);
      end;
    qk_sort(1 , N);
    for i := 1 to N do
      if index[data[i , 1]] = 0 then
        index[data[i , 1]] := i;

    tot := 0;
    ch := 'a';
    while (ch <= 'z') and (degree[ch] <= 0) do inc(ch);
    if (ch <= 'z') and (degree[ch] <> 1) then exit;
    if ch > 'z'
      then ch := data[1 , 1]
      else for c2 := 'a' to 'z' do
             if (c2 <> ch) and (degree[c2] > 0) then
               exit;
    dfs_print(ch , 0);
end;

procedure out;
var
    i          : longint;
begin
    if tot <> N
      then writeln('***')
      else begin
               for i := tot downto 2 do
                 write(answer[i] , '.');
               writeln(answer[1]);
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          out;
      end;
End.
