#include <math.h>
#include <stdio.h>

int u[] = {
  3,
  5,
  8,
  12,
  20,
  34,
  57,
  98,
  170,
  300,
  536,
  966,
  1754,
  3210,
  5910,
  10944,
  20366,
  38064,
  71421,
  134480,
  254016
};

int main(void)
{
  int i;
  int c = 1, p, v[23];
  double a = log(2.0), s = 1.0, t;

  for (i = 1, p = 2; i < 23; ++i, p <<= 1) {
    t = p * a + 1;
    while (s < t)
      s += log(++c);
    v[i] = c - 1;
  }

  while (scanf("%d", &i) == 1 && i) {
    i -= 1940;
    i /= 10;
    printf("%d\n", v[i]);
  }

/*
  while (scanf("%d", &i) == 1 && i) {
    i -= 1960;
    i /= 10;
    printf("%d\n", u[i]);
  }
 */

  return 0;
}
