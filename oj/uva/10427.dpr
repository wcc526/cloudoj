{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10427.in';
    OutFile    = 'p10427.out';

Var
    N , answer : longint;

procedure init;
begin
    readln(N);
end;

procedure work;
var
    p , base , i , 
    num        : longint;
begin
    p := 1; base := 9;
    while p * base < N do
      begin
          dec(N , p * base);
          inc(p); base := base * 10;
      end;
    num := (N - 1) div p + Base div 9;
    i := p - (N - 1) mod p;
    while i > 1 do
      begin
          num := num div 10; dec(i);
      end;
    answer := num mod 10;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
