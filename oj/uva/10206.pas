Const
    InFile     = 'p10206.in';
    Limit      = 1000;
    minimum    = 1e-6;

Type
    Tpoint     = record
                     x , y , bright          : longint;
                 end;
    Tdata      = object
                     tot                     : longint;
                     data                    : array[1..Limit] of Tpoint;
                     procedure qk_pass(start , stop : longint; var mid : longint);
                     procedure qk_sort(start , stop : longint);
                 end;
    Tconstel   = object
                     tot                     : longint;
                     data                    : array[1..Limit] of Tdata;
                     bright                  : array[1..Limit] of longint;
                     procedure ins(const stars : Tdata);
                     procedure printmax;
                 end;

Var
    sample ,
    data       : Tdata;
    constel    : Tconstel;
    cases      : longint;

function compare(const p1 , p2 : Tpoint) : longint;
begin
    if p1.x <> p2.x
      then exit(p1.x - p2.x)
      else exit(p1.y - p2.y);
end;

procedure Tdata.qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (compare(data[stop] , key) > 0) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(data[start] , key) < 0) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure Tdata.qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function same(const s1 , s2 : Tdata) : boolean;
var
    i          : longint;
begin
    if s1.tot <> s2.tot then exit(false);
    for i := 1 to s1.tot do
      if (s1.data[i].x <> s2.data[i].x) or (s1.data[i].y <> s2.data[i].y) then
        exit(false);
    exit(true);
end;

procedure Tconstel.ins(const stars : Tdata);
var
    i          : longint;
begin
    for i := 1 to tot do
      if same(data[i] , stars) then exit;
    inc(tot);
    data[tot] := stars;
end;

procedure Tconstel.printmax;
var
    i , j , max: longint;
begin
    for i := 1 to tot do bright[i] := 0;
    for i := 1 to tot do
      for j := 1 to data[tot].tot do
        inc(bright[i] , data[i].data[j].bright);
    max := 1;
    for i := 2 to tot do
      if bright[i] > bright[max] then
        max := i;
    write('Brightest occurrence:');
    with data[max] do
      for i := 1 to tot do
        write(' (' , data[i].x , ',' , data[i].y , ')');
    writeln;
end;

function init : boolean;
var
    i          : longint;
begin
    readln(data.tot);
    if data.tot = 0 then exit(false);
    init := true;
    with data do
      for i := 1 to tot do
        read(data[i].x , data[i].y , data[i].bright);
    data.qk_sort(1 , data.tot);
end;

function binary_search(p : Tpoint; var res : Tpoint) : boolean;
var
    start , stop ,
    mid , code : longint;
begin
    start := 1; stop := data.tot;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          code := compare(p , data.data[mid]);
          if code = 0
            then begin res := data.data[mid]; exit(true); end
            else if code > 0
                   then start := mid + 1
                   else stop := mid - 1;
      end;
    exit(false);
end;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function change(p1 , p2 : Tpoint; var res : Tdata) : boolean;
var
    d , cosA , sinA ,
    d1 , cosB , sinB ,
    cosC , sinC ,
    x1 , y1 ,
    dx , dy    : double;
    p          : Tpoint;
    i          : longint;
begin
    d := sqrt(sqr(sample.data[2].x - sample.data[1].x) + sqr(sample.data[2].y - sample.data[1].y));
    cosA := (sample.data[2].x - sample.data[1].x) / d; sinA := (sample.data[2].y - sample.data[1].y) / d;
    res.tot := sample.tot; res.data[1] := p1; res.data[2] := p2;
    x1 := p2.x - p1.x; y1 := p2.y - p1.y;
    for i := 3 to sample.tot do
      begin
          d1 := sqrt(sqr(sample.data[i].x - sample.data[1].x) + sqr(sample.data[i].y - sample.data[1].y));
          cosB := (sample.data[i].x - sample.data[1].x) / d1; sinB := (sample.data[i].y - sample.data[1].y) / d1;
          cosC := cosB * cosA + sinB * sinA;
          sinC := sinB * cosA - cosB * sinA;
          dx := cosC * x1 - sinC * y1; dy := sinC * x1 + cosC * y1;
          dx := dx / d * d1 + p1.x; dy := dy / d * d1 + p1.y;
          p.x := round(dx); p.y := round(dy);
          if not zero(dx - p.x) or not zero(dy - p.y) then exit(false);
          if not binary_search(p , res.data[i]) then exit(false);
      end;
    res.qk_sort(1 , res.tot);
    exit(true);
end;

procedure workout;
var
    T ,
    i , j , M  : longint;
    tmp        : Tdata;
    s          : string[255];
begin
    inc(Cases);
    writeln('Map #' , cases);
    readln(M);
    for T := 1 to M do
      begin
          read(sample.tot);
          readln(s);
          while s[1] = ' ' do delete(s , 1 , 1);
          while s[length(s)] = ' ' do delete(s , length(s) , 1);
          with sample do
            for i := 1 to sample.tot do
              readln(data[i].x , data[i].y);
          sample.qk_sort(1 , sample.tot);
          constel.tot := 0;
          if sample.tot <= 1
            then begin
                     for i := 1 to data.tot do
                       begin
                           tmp.tot := 1; tmp.data[1] := data.data[1];
                           constel.ins(tmp);
                       end;
                 end
            else begin
                     for i := 1 to data.tot do
                       for j := 1 to data.tot do
                         if i <> j then
                           if change(data.data[i] , data.data[j] , tmp) then
                             constel.ins(tmp);
                 end;
          writeln;
          writeln(s , ' occurs ' , constel.tot , ' time(s) in the map.');
          if constel.tot > 0 then
            constel.printmax;
      end;
    writeln('-----');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while init do
        workout;
//    Close(INPUT);
End.
