#include <stdio.h>

#define MAX 64

int main()
{
  int i, k;
  long long n, v[MAX][MAX];

  for (i = 0; i < MAX; i++)
    v[0][i] = v[i][0] = 0;

  for (i = 1; i < MAX; i++)
    for (k = 1; k < MAX; k++)
      v[i][k] = v[i][k - 1] + v[i - 1][k - 1] + 1;

  while (scanf("%d %lld", &k, &n) == 2 && k) {
    if (k > MAX - 1)
      k = MAX - 1;
    for (i = 0; ++i < MAX && v[k][i] < n;);
    if (i == MAX)
      printf("More than %d trials needed.\n", MAX - 1);
    else
      printf("%d\n", i);
  }

  return 0;
}
