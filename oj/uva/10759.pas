{$R-,Q-,S-}
Const
    Limit      = 24;
    LimitSum   = 150;

Type
    Tfraction  = object
                     signal   : longint;
                     A , B    : extended;
                     procedure init;
                     procedure add(num1 , num2 : Tfraction);
                     procedure reduce;
                     procedure print;
                 end;
    Tdata      = array[0..Limit , 0..LimitSum] of Tfraction;

Var
    data       : Tdata;
    sum        : Tfraction;
    i ,
    N , X      : longint;

procedure Get_GCD(const A , B : extended; var GCDNum : extended);
var
    tmp 	: extended;
begin
    if A = 0
      then GCDNum := B
      else begin
               tmp := B - int(B / A) * A;
               Get_GCD(tmp , A , GCDNum);
           end;
end;

procedure Tfraction.init;
begin
    signal := 1; A := 0; B := 1;
end;

procedure Tfraction.add(num1 , num2 : Tfraction);
begin
    if num1.B <> num2.B
      then begin
               while num1.B < num2.B do begin num1.A := num1.A * 6; num1.B := num1.B * 6; end;
               while num1.B > num2.B do begin num2.A := num2.A * 6; num2.B := num2.B * 6; end;
           end;
    B := num1.B;
    A := num1.A + num2.A;
end;

procedure Tfraction.reduce;
var
    GCDnum     : extended;
begin
    Get_Gcd(A , B , GCDnum);
    A := A / GCDNum;
    B := B / GCDNum;
end;

procedure print_Num(num : extended);
begin
    if num = 0
      then
      else begin
               print_num(int(num / 10));
               write(num - int(num / 10) * 10 : 0 : 0);
           end;
end;

procedure Tfraction.print;
begin
    if A = 0
      then begin write(0); exit; end;
    print_Num(A);
    if B = 1
      then
      else begin
               write('/');
               print_num(B);
           end;
end;

procedure process;
var
    i , j , k ,
    start , stop
               : longint;
    tmp	: Tfraction;
begin
    for i := 0 to Limit do
      for j := 0 to LimitSum do
        data[i , j].init;
    data[0 , 0].A := 1;
    for i := 1 to Limit do
      for j := 1 to i * 6 do
        begin
            start := j - 6; stop := j - 1;
            if start < 0 then start := 0;
            for k := start to stop do
              begin
                  tmp := data[i - 1 , k];
                  tmp.B := tmp.B * 6;
                  data[i , j].add(data[i , j] , tmp);
              end;
        end;
end;

Begin
    process;
    readln(N , X);
    while N <> 0 do
      begin
          sum.init;
          for i := X to N * 6 do
            sum.add(sum , data[N , i]);
          sum.reduce;
          sum.print;
          writeln;
          readln(N , X);
      end;
End.
