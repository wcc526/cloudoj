#include <stdio.h>

int main()
{
  double r;
  long long c, t, u;
  int n;

  while (scanf("%d", &n) == 1 && n) {
    c = 100000, u = 0;
    while (n--) {
      scanf("%lf", &r);
      t = (0.97 / r) * c;
      u = u < t ? t : u;
      t = (0.97 * r) * u;
      c = c < t ? t : c;
    }
    printf("%.2lf\n", 0.01 * c);
  }

  return 0;
}
