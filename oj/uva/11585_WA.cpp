    #include<iostream>

    using namespace std;

    int r,c,ans;
    char arr[102][102],s[102];
    bool cnt[102][102],fl;

    void dfs1(int i,int j)
    {
         cnt[i][j]=1;
         ans++;
         
         if(i-1>=0 && arr[i-1][j]=='#' && cnt[i-1][j]==0) dfs1(i-1,j);
         if(i+1<r && arr[i+1][j]=='#' && cnt[i+1][j]==0) dfs1(i+1,j);
         if(j-1>=0 && arr[i][j-1]=='#' && cnt[i][j-1]==0) dfs1(i,j-1);
         if(j+1<c && arr[i][j+1]=='#' && cnt[i][j+1]==0) dfs1(i,j+1);         
    }

    void dfs(int i,int j)
    {
         cnt[i][j]=1;
         ans++;

         if(i-1>=0 && arr[i-1][j]=='.' && cnt[i-1][j]==0) dfs(i-1,j);
         if(i+1<r && arr[i+1][j]=='.' && cnt[i+1][j]==0) dfs(i+1,j);
         if(j-1>=0 && arr[i][j-1]=='.' && cnt[i][j-1]==0) dfs(i,j-1);
         if(j+1<c && arr[i][j+1]=='.' && cnt[i][j+1]==0) dfs(i,j+1);
    }

    void dfs2(int i,int j)
    {
         if(i==0 || j==0 || i+1==r || j+1==c)
         fl=1;
         
         cnt[i][j]=1;
         
         if(i-1>=0 && arr[i-1][j]=='.' && cnt[i-1][j]==0) dfs2(i-1,j);
         if(i+1<r && arr[i+1][j]=='.' && cnt[i+1][j]==0) dfs2(i+1,j);
         if(j-1>=0 && arr[i][j-1]=='.' && cnt[i][j-1]==0) dfs2(i,j-1);
         if(j+1<c && arr[i][j+1]=='.' && cnt[i][j+1]==0) dfs2(i,j+1);
         
         if(i-1>=0 && j-1>=0 && arr[i-1][j-1]=='.' && cnt[i-1][j-1]==0) dfs2(i-1,j-1);
         if(i+1<r && j-1>=0 && arr[i+1][j-1]=='.' && cnt[i+1][j-1]==0) dfs2(i+1,j-1);
         if(i+1<r && j+1<c && arr[i+1][j+1]=='.' && cnt[i+1][j+1]==0) dfs2(i+1,j+1);
         if(i-1>=0 && j+1<c && arr[i-1][j+1]=='.' && cnt[i-1][j+1]==0) dfs2(i-1,j+1);     
    }

    int main()
    {
        int t,i,j,k,e[50002][3],ans1,d;
        bool f;
        scanf("%d",&t);
       
        while(t--)
        {
            scanf("%d %d %d",&r,&c,&d);
            memset(cnt,0,sizeof(cnt));
           
            for(i=0;i<d;i++)
            scanf("%d %d %d",&e[i][0],&e[i][1],&e[i][2]);
           
            gets(s);
            for(i=0;i<r;i++)
            gets(arr[i]);
           
            ans=0;
            ans1=0;
            f=0;
            for(i=0;i<r;i++)
            for(j=0;j<c;j++)
            {
                if(arr[i][j]=='#' && f==0)
                {
                    dfs1(i,j);
                    f=1;
                }
                if(arr[i][j]=='#')
                ans1++;
            }
           
            if(ans1!=ans)
            {
                puts("not solved");
                continue;
            }
           
            memset(cnt,0,sizeof(cnt));
           
            for(i=0;i<d;i++)
            {
                ans=0;
                dfs(e[i][0],e[i][1]);
               
                if(ans!=e[i][2])
                break;
            }
           
            if(i!=d)
            {
                puts("not solved");
                continue;
            }
           
            f=0;
            for(i=0;i<r-1;i++)
            {
                for(j=0;j<c-1;j++)
                {
                     if(arr[i][j]=='#' && arr[i+1][j]=='#' && arr[i+1][1+j]=='#' && arr[i][j+1]=='#')
                     {
                       f=1;
                       break;
                     }
                }
               
                if(f) break;
            }
           
            if(f)
            {
                puts("not solved");
                continue;
            }
           
            memset(cnt,0,sizeof(cnt));
           
            f=0;
            for(i=0;i<r;i++)
            {
                  for(j=0;j<c;j++)
                  {
                        if(arr[i][j]=='.' && cnt[i][j]==0)
                        {
                           fl=0;
                           dfs2(i,j);
                           if(fl==0)
                           {
                           f=1;
                           break;
                           }
                        }
                  }
                  if(f)
                  break;
            }
           
            if(f)
            {
                puts("not solved");
                continue;
            }
           
            else
            {
                puts("solved");
            }
        }
        system("pause");
        return 0;
    }
