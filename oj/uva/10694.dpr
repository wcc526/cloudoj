{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10694.in';
    OutFile    = 'p10694.out';
    Limit      = 1000;
    LimitLen   = 300;

Type
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure transfer(num : longint);
                     procedure add(num1 , num2 : lint);
                     procedure print;
                 end;
    Tdata      = array[1..Limit] of lint;

Var
    data       : Tdata;
    cases , N  : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
end;

procedure lint.transfer(num : longint);
begin
    init;
    while num <> 0 do
      begin
          inc(len); data[len] := num mod 10; num := num div 10;
      end;
end;

procedure lint.add(num1 , num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0; init;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
    writeln;
end;

procedure pre_process;
var
    tmp        : lint;
    i          : longint;
begin
    data[1].transfer(1);
    data[2].transfer(3);
    for i := 3 to Limit do
      begin
          tmp.transfer(i);
          tmp.add(tmp , data[i - 1]);
          data[i].add(data[i - 2] , tmp);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      readln(Cases);
      while Cases > 0 do
        begin
            dec(Cases);
            readln(N);
            if N <= 0
              then writeln(0)
              else data[N].print;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
