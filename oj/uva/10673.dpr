{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10673.in';
    OutFile    = 'p10673.out';

Var
    p , q , 
    x , y      : comp;
    T          : longint;

procedure init;
begin
    dec(T);
    readln(p , q);
end;

procedure Euclid_Extend(A , B , C : comp; var x , y : comp);
var
    t          : comp;
begin
    if A = 0
      then begin
               x := 0; y := int(C / B);
           end
      else begin
               Euclid_Extend(B - int(B / A) * A , A , C , y , t);
               x := t - int(B / A) * y;
           end;
end;

procedure work;
var
    a , b , c  : comp;
begin
    A := int(p / q);
    if A * q = p then B := A else B := A + 1;
    C := p;
    Euclid_Extend(A , B , C , x , y);
end;

procedure out;
begin
    writeln(x : 0 : 0 , ' ' , y : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
