Const
    InFile     = 'p10318.in';
    OutFile    = 'p10318.out';
    Limit      = 5;

Type
    Tdata      = array[1..Limit] of longint;
    Tchange    = array[1..3] of longint;
    Tmap       = array[1..3] of Tchange;
    Tcount     = array[0..1 shl Limit] of longint;

Var
    bestway ,
    data       : Tdata;
    map        : Tmap;
    count      : Tcount;
    cases ,
    N , M ,
    bestans    : longint;

function init : boolean;
var
    i , j      : longint;
    ch         : char;
begin
    readln(N , M);
    if N = 0 then exit(false);
    init := true;
    count[0] := 0;
    for i := 1 to 1 shl M - 1 do count[i] := count[i div 2] + i mod 2;
    for i := 1 to 3 do
      begin
          for j := 1 to 3 do
            begin
                read(ch);
                if ch = '*' then map[i , j] := 1 else map[i , j] := 0;
            end;
          readln;
      end;
end;

function calc(const change : Tchange; option : longint) : longint;
var
    tmp        : longint;
begin
    tmp := (option * change[2]) xor (option div 2 * change[3]) xor (option * 2 * change[1] mod (1 shl M));
    calc := tmp;
end;

procedure check(data : Tdata; option : longint);
var
    way        : Tdata;
  procedure dfs(data : Tdata; step , last : longint);
  var
      j        : longint;
  begin
      if step > N then
        begin
            if data[N] = 0 then
              if last < bestans then
                begin
                    bestans := last;
                    bestway := way;
                end;
            exit;
        end;
      for j := 0 to 1 shl M - 1 do
        if data[step - 1] xor calc(map[1] , j) = 0 then
          begin
              way[step] := j;
              data[step] := data[step] xor calc(map[2] , j);
              if step <> N then
                data[step + 1] := data[step + 1] xor calc(map[3] , j);
              dfs(data , step + 1 , last + count[j]);
              data[step] := data[step] xor calc(map[2] , j);
              if step <> N then
                data[step + 1] := data[step + 1] xor calc(map[3] , j);
          end;
  end;

begin
    data[1] := data[1] xor calc(map[2] , option);
    data[2] := data[2] xor calc(map[3] , option);
    way[1] := option;
    dfs(data , 2 , count[option]);
end;

procedure work;
var
    i          : longint;
begin
    bestans := maxlongint;
    for i := 1 to N do
      data[i] := 1 shl M - 1;
    for i := 0 to 1 shl M - 1 do
      check(data , i);
end;

procedure out;
var
    i , j , K  : longint;
begin
    inc(cases);
    writeln('Case #' , cases);
    if bestans = maxlongint
      then writeln('Impossible.')
      else begin
               K := 0;
               for i := 1 to N do
                 for j := 1 to M do
                   begin
                       inc(K);
                       if 1 shl (M - j) and bestway[i] <> 0 then
                         write(K , ' ');
                   end;
               writeln;
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.