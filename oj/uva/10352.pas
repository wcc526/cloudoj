// Wrong Answer!
Const
    InFile     = 'p10352.in';
    Limit      = 8100;

Type
    Tstr       = string[5];
    Tdata      = array[1..Limit] of Tstr;
    Tcount     = array[1..Limit] of longint;

Var
    data       : Tdata;
    count      : Tcount;
    ch         : char;
    N , cases  : longint;

procedure read_word(var s : Tstr);
begin
    s := '';
    repeat
      ch := ' ';
      if eof then exit;
      if eoln then readln else read(ch);
    until ch <> ' ';
    while ch <> ' ' do
      begin
          if length(s) < 5 then s := s + ch;
          ch := ' '; if eof then break;
          if eoln then readln else read(ch);
      end;
end;

function compare(s1 , s2 : Tstr) : longint;
begin
    if length(s1) >= 3 then s1[3] := #0;
    if length(s2) >= 3 then s2[3] := #0;
    if s1 = s2
      then exit(0)
      else if s1 > s2
             then exit(1)
             else exit(-1);
end;

function binary_search(s : Tstr) : boolean;
var
    start , stop ,
    mid , code : longint;
begin
    start := 1; stop := N;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          code := compare(data[mid] , s);
          if code = 0
            then begin
                     data[mid] := s; inc(count[mid]); exit(true);
                 end
            else if code > 0
                   then stop := mid - 1
                   else start := mid + 1;
      end;
    exit(false);
end;

procedure work;
var
    s          : Tstr;
    i          : longint;
begin
    inc(Cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(count , sizeof(count) , 0);
    N := 0;
    while true do
      begin
          read_word(s);
          if (s = '#') or (s = '') then break;
          if not binary_search(s) then
            begin
                i := N + 1;
                while (i > 1) and (compare(data[i - 1] , s) > 0) do
                  begin
                      data[i] := data[i - 1]; count[i] := count[i - 1];
                      dec(i);
                  end;
                inc(N); data[i] := s; count[i] := 1;
            end;
          if ch = '#' then break;
      end;
end;

procedure out;
var
    i          : longint;
begin
    writeln('Set #' , cases , ':');
    for i := 1 to N do
      writeln(data[i] , ' ' , count[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            work;
            if (N = 0) and eof then break;
            out;
        end;
//    Close(INPUT);
End.
