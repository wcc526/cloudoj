#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;

struct Point {
       double x , y;
       Point(double x = 0 , double y = 0):x(x) , y(y) {}
       void read_point() { scanf("%lf %lf" , &x , &y);}
       void print() {
            printf("%lf %lf\n" , x , y);
       }
};

typedef Point Vector;
Vector operator + (Vector A , Vector B) { return Vector(A.x+B.x , A.y+B.y);}
Vector operator - (Vector A , Vector B) { return Vector(A.x-B.x , A.y-B.y);}
Vector operator * (Vector A , double p) { return Vector(A.x*p , A.y*p);}
Vector operator / (Vector A , double p) { return Vector(A.x/p , A.y/p);}
Vector Rotate(Vector A , double rad) {
       return Vector(A.x*cos(rad)-A.y*sin(rad) , A.x*sin(rad)+A.y*cos(rad));
}
double Dot(Vector A , Vector B) { return A.x*B.x + A.y*B.y;}
double Length(Vector A) { return sqrt(Dot(A,A));}
double Cross(Vector A , Vector B) { return A.x*B.y - A.y*B.x;}
double Angle(Vector A , Vector B) {return acos(Dot(A,B)/Length(A)/Length(B));}
Point GetLineIntersection(Point P , Vector v , Point Q , Vector w) {
      Vector u = P - Q;
      double t = Cross(w , u) / Cross(v , w);
      return P + v*t;
}



Point get_D(Point A , Point B , Point C) {
      Point D;
      Vector v1 = C - B  , v2 = B - C;
      Vector v = Rotate(v1 , Angle(C-B , A-B)/3) , 
             w = Rotate(v2 , -Angle(B-C , A-C)/3);
      D = GetLineIntersection(B , v , C , w);
      return D;
}

int main() {
    int t;
    Point A , B , C , D , E , F;
    scanf("%d" , &t);
    while(t--) {
               A.read_point();
               B.read_point();
               C.read_point();
               D = get_D(A , B , C);
               E = get_D(B , C , A);
               F = get_D(C , A , B);
               printf("%.6lf %.6lf %.6lf %.6lf %.6lf %.6lf\n" , D.x , D.y , E.x , E.y , F.x , F.y);
    }
    return 0;
}
