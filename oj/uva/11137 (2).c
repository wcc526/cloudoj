#include <stdio.h>

int c[23];
long long r[22][10000];

int main(void)
{
  int i, j, k;

  i = 22;
  c[i] = 10000;
  while (i--) {
    c[i] = i * i * i;
    r[i][0] = 0;
    r[i][1] = 1;
  }
  for (i = 0; i < 10000; ++i)
    r[1][i] = 1;
  for (i = 2; i < 10000; ++i) {
    for (j = 2; c[j] <= i; ++j) {
      r[j][i] = 0;
      for (k = i; k > 0; k -= c[j])
        r[j][i] += r[j - 1][k];
      if (k == 0)
        ++r[j][i];
    }
    for (; j < 22; ++j)
      r[j][i] = r[j - 1][i];
  }

  while (scanf("%d", &i) == 1)
    printf("%lld\n", r[21][i]);

  return 0;
}
