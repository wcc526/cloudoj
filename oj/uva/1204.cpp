#include "stdio.h"
#include "math.h"
#include "algorithm"
using namespace std;
typedef struct Point
{
int x,y;
}Point;
Point p[54];
int ans[54][54];
int INF = 2100000000;
int mult(Point A,Point B)
{
return A.x*B.y - A.y*B.x;
}
int AREA(Point A,Point B,Point C)
{
int ans = mult(A,B) + mult(B,C) + mult(C,A);
if(ans < 0) ans = -ans;
return ans;
}
int MAX(int m,int n)
{
if(m > n) return m;
return n;
}
int multi(Point A,Point B,Point C)
{
return (B.x-A.x) * (C.y-A.y) - (B.y-A.y)*(C.x-A.x);
}
int NOPointIN(int i,int j,int k,int n)
{
int r,s,t;
      s = AREA(p[i],p[j],p[k]);
for(r = 1;r <= n;r ++)
{
     if(r == i || r == j || r == k) continue;
     t = AREA(p[i],p[j],p[r]) + AREA(p[i],p[k],p[r]) + AREA(p[j],p[k],p[r]);
     if(s == t) return 0;
}
return 1;
}
int main()
{
int T,n,j,k,s,s1,i,r,sum;
scanf("%d",&T);
while(T--)
{
     scanf("%d",&n);
     for(i = 1;i <= n;i ++)
      scanf("%d%d",&p[i].x,&p[i].y);
     for(i = 1;i <= n - 2;i ++)
      ans[i][i+2] = AREA(p[i],p[i+1],p[i+2]);
     for(r = 3;r < n; r ++)
     {
      for(i = 1;i + r <= n;i ++)
      {
       j = i + r;
       sum = INF;
       if(NOPointIN(i,j-1,j,n))
        sum = MAX(AREA(p[i],p[j-1],p[j]),ans[i][j-1]);
       if(NOPointIN(i,i+1,j,n))
       {
         s = MAX(AREA(p[i],p[i+1],p[j]),ans[i+1][j]);
          if(s < sum) sum = s;
       }
       for(k = i + 2;k <= j - 2;k ++)
       {
        if(!NOPointIN(i,k,j,n)) continue;
        s = AREA(p[i],p[j],p[k]);
        s1 = ans[i][k];
        if(s1 > s) s = s1;
        s1 = ans[k][j];
        if(s1 > s) s = s1;
        if(s < sum) sum = s;
       }
       ans[i][j] = sum;
      }
     }
     printf("%d",ans[1][n] / 2);
     if(ans[1][n]%2) printf(".5\n");
     else printf(".0\n");
}
return 0;
}