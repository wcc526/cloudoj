Var
    a , b      : array['a'..'z'] of longint;
    i          : longint;
    ch         : char;

function min(a , b : longint) : longint;
begin
    if a < b then min := a else min := b;
end;

Begin
    while not eof do
      begin
          fillchar(a , sizeof(a) , 0);
          fillchar(b , sizeof(b) , 0);
          while not eoln do
            begin
                read(ch); if ch in ['a'..'z'] then inc(a[ch]);
            end;
          readln;
          while not eoln do
            begin
                read(ch); if ch in ['a'..'z'] then inc(b[ch]);
            end;
          readln;
          for ch := 'a' to 'z' do
            for i := 1 to min(a[ch] , b[ch]) do
              write(ch);
          writeln;
      end;
End.