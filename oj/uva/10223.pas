Type
    Tdata      = array[0..10000] of extended;

Var
    data       : Tdata;
    sum        : extended;
    tot , i    : longint;

Begin
    data[0] := 1; tot := 0;
    while data[tot] < 5000000000 do
      begin
          inc(tot); data[tot] := 0;
          for i := 0 to tot - 1 do
            data[tot] := data[tot] + data[i] * data[tot - 1 - i];
      end;
    while not eof do
      begin
          readln(sum);
          i := 1;
          while data[i] < sum do inc(i);
          writeln(i);
      end;
End.