{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10519.in';
    OutFile    = 'p10519.out';
    LimitLen   = 300;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                 end;

Var
    N , answer : lint;

function init : boolean;
var
    c          : char;
    i , tmp    : longint;
begin
    if eof
      then init := false
      else begin
               init := true;
               fillchar(N , sizeof(N) , 0);
               while not eoln do
                 begin
                     read(c);
                     inc(N.len); N.data[N.len] := ord(c) - ord('0');
                 end;
               readln;
               for i := 1 to N.len div 2 do
                 begin
                     tmp := N.data[i]; N.data[i] := N.data[N.len - i + 1];
                     N.data[N.len - i + 1] := tmp; 
                 end;
               if N.len = 0 then init := false;
               while (N.len > 1) and (N.data[N.len] = 0) do dec(N.len);
           end;
end;

procedure times(num1 , num2 : lint; var num3 : lint);
var
    i , j ,
    jw , tmp   : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := num1.data[i] * num2.data[j] + num3.data[i + j - 1] + jw;
                jw := tmp div 10;
                num3.data[i + j - 1] := tmp mod 10;
                inc(j);
            end;
          dec(j);
          if i + j - 1 > num3.len then num3.len := i + j - 1;
      end;
    while (num3.len > 1) and (num3.data[num3.len] = 0) do dec(num3.len);
end;

procedure minus_one(var num : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 1;
    while jw <> 0 do
      begin
          tmp := num.data[i] - jw;
          jw := 0;
          if tmp < 0 then
            begin
                inc(tmp , 10); jw := 1;
            end;
          num.data[i] := tmp;
          inc(i);
      end;
    while (num.len > 1) and (num.data[num.len] = 0) do dec(num.len);
end;

procedure add(num1 , num2 : lint; var num3 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          num3.data[i] := tmp mod 10;
          inc(i);
      end;
    num3.len := i - 1;
end;

procedure work;
var
    M , two    : lint;
begin
    fillchar(answer , sizeof(answer) , 0);
    if (N.len = 1) and (N.data[1] = 0) then
      begin
          answer.len := 1; answer.data[1] := 1; exit;
      end;
    M := N;
    minus_one(M);
    times(N , M , answer);
    fillchar(two , sizeof(two) , 0);
    two.len := 1; two.data[1] := 2;
    add(answer , two , answer);
end;

procedure out;
var
    i          : longint;
begin
    for i := answer.len downto 1 do
      write(answer.data[i]);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
