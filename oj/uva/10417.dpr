{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10417.in';
    OutFile    = 'p10417.out';
    Limit      = 12;

Type
    Tdata      = array[1..Limit , 1..5] of extended;
    Tcount     = array[1..5] of longint;
    Tanswer    = array[1..Limit] of extended;

Var
    data       : Tdata;
    count      : Tcount;
    answer     : Tanswer;
    N , cases  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    read(N);
    for i := 1 to 5 do read(count[i]);
    for i := 1 to N do
      for j := 1 to 5 do
        read(data[i , j]);
    fillchar(answer , sizeof(answer) , 0);
end;

procedure dfs(step , first : longint; last : extended);
var
    i          : longint;
begin
    if step > N
      then answer[first] := answer[first] + last
      else for i := 1 to 5 do
             if count[i] > 0 then
               begin
                   dec(count[i]);
                   if step = 1
                     then dfs(step + 1 , i , last * data[step , i])
                     else dfs(step + 1 , first , last * data[step , i]);
                   inc(count[i]);
               end;
end;

procedure work;
begin
    dfs(1 , 0 , 1);
end;

procedure out;
var
    max , i    : longint;
    sum        : extended;
begin
    sum := answer[1] + answer[2] + answer[3] + answer[4] + answer[5];
    for i := 1 to 5 do answer[i] := answer[i] / sum;
    max := 0;
    for i := 1 to 5 do
      if count[i] <> 0 then
        if (max = 0) or (answer[max] / count[max] < answer[i] / count[i]) then
          max := i;
    writeln(max , ' ' , answer[max] / count[max] : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
