Type
    Tpoint     = record
                     x , y    : double;
                 end;
    TLine      = record
                     A , B , C: double;
                 end;

Var
    p1 , p2 , p3
               : Tpoint;
    cosA , sinA ,
    _DM , _DI ,
    _DG , _DO  : double;
    cases ,
    a , b , c  : longint;

procedure swap(var a , b : longint);
var
    tmp        : longint;
begin
    tmp := a; a := b; b := tmp;
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    dist := sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
end;

function calc(x , y : double) : double;
var
    p          : Tpoint;
begin
    p.x := x; p.y := y;
    calc := dist(p , p1) + dist(p , p2) + dist(p , p3);
end;

procedure rotate(p1 , p2 : Tpoint; var p : Tpoint; angle : double);
begin
    p2.x := p2.x - p1.x; p2.y := p2.y - p1.y;
    p.x := p2.x * cos(angle) - p2.y * sin(angle) + p1.x;
    p.y := p2.x * sin(angle) + p2.y * cos(angle) + p1.y;
end;

procedure GetLine(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p2.y - p1.y;
    L.B := p1.x - p2.x;
    L.C := p2.x * p1.y - p1.x * p2.y;
end;

procedure GetCrossing(L1 , L2 : TLine; var p : Tpoint);
var
    tmp        : double;
begin
    tmp := L1.A * L2.B - L2.A * L1.B;
    p.x := -(L1.C * L2.B - L1.B * L2.C) / tmp;
    p.y := -(L1.A * L2.C - L1.C * L2.A) / tmp;
end;

procedure calc_DM;
var
    p ,
    CC , BB    : Tpoint;
    L1 , L2    : TLine;
begin
    if cosA <= cos(pi / 3 * 2)
      then _DM := calc(p3.x , p3.y)
      else begin
               rotate(p1 , p3 , CC , pi / 3);
               rotate(p2 , p3 , BB , -pi / 3);
               GetLine(CC , p2 , L1);
               GetLine(BB , p1 , L2);
               GetCrossing(L1 , L2 , p);
               _DM := calc(p.x , p.y);
           end;
end;

procedure calc_DI;
var
    x , y      : double;
begin
    x := (p1.x * c + p2.x * b + p3.x * a) / (a + b + c);
    y := (p1.y * c + p2.y * b + p3.y * a) / (a + b + c);
    _DI := calc(x , y);
end;

procedure calc_DG;
var
    x , y      : double;
begin
    x := (p1.x + p2.x + p3.x) / 3;
    y := (p1.y + p2.y + p3.y) / 3;
    _DG := calc(x , y);
end;

procedure calc_DO;
var
    R          : double;
begin
    if b + c = a then begin _DO := -1; exit; end;
    R := a / sinA / 2;
    _DO := R * 3;
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          readln(a , b , c); dec(Cases);
          _DM := -1; _DI := -1; _DG := -1; _DO := -1;
          if (a + b < c) or (abs(a - b) > c)
            then
            else begin
                     if a < b then swap(a , b);
                     if a < c then swap(a , c);
                     p1.x := 0; p1.y := 0; p2.x := a; p2.y := 0;
                     p3.x := (b * b + a * a - c * c) / 2 / a;
                     p3.y := sqrt(b * b - sqr(p3.x));
                     cosA := (b * b + c * c - a * a) / 2 / b / c;
                     sinA := sqrt(abs(1 - sqr(cosA)));
                     calc_DM;
                     calc_DI;
                     calc_DG;
                     calc_DO;
                 end;
          writeln(_DM : 0 : 3 , ' ' , _DI : 0 : 3 , ' ' , _DG : 0 : 3 , ' ' , _DO : 0 : 3);
      end;
End.