Const
    maxn=70;

Type
    Tp=record
           x,y:longint;
       end;

Var
    n,k,area:longint;
    s:Tp;
    data:array[0..maxn]of Tp;

function getarea(var p0,p1,p2:Tp):longint;
begin
    getarea:=abs((p1.x-p0.x)*(p2.y-p0.y)-(p1.y-p0.y)*(p2.x-p0.x));
end;

procedure init;
var i:longint;
begin
    read(n);
    for i:=0 to n-1 do read(data[i].x,data[i].y);
    for i:=0 to n-1 do data[i+n]:=data[i];
    read(k);
    area:=0;
    for i:=1 to n-2 do
      area:=area+getarea(data[0],data[i],data[i+1]);
end;

function gcd(a,b:longint):longint;
begin
    if b=0 then gcd:=a
    else gcd:=gcd(b,a mod b);
end;

procedure work;
var first,i,sum,t,fz,fm,sign,tmp:longint;
begin
    for i:=0 to n-1 do
      if (getarea(s,data[i],data[i+1])=0) and
             (abs(data[i].x-data[i+1].x)=abs(data[i].x-s.x)+abs(data[i+1].x-s.x)) and
             (abs(data[i].y-data[i+1].y)=abs(data[i].y-s.y)+abs(data[i+1].y-s.y)) then begin
          first:=i; break;
      end;

    sum:=0;
    for i:=first+1 to first+n-1 do
      begin
          t:=getarea(s,data[i],data[i+1]);
          if (sum*2<=area) and ((sum+t)*2>=area) then begin
              fm:=2*t;
              fz:=data[i].x*2*t+(area-sum*2)*(data[i+1].x-data[i].x);
              if fz<0 then begin
                  sign:=-1; fz:=-fz;
              end
              else sign:=1;
              tmp:=gcd(fm,fz);
//              if sign<0 then write('-');
              if sign < 0
                then write('(-',fz div tmp,'/',fm div tmp,'),')
                else write('(',fz div tmp,'/',fm div tmp,'),');

              fm:=2*t;
              fz:=data[i].y*2*t+(area-sum*2)*(data[i+1].y-data[i].y);
              if fz<0 then begin
                  sign:=-1; fz:=-fz;
              end
              else sign:=1;
              tmp:=gcd(fm,fz);
//              if sign<0 then write('-');
              if sign < 0
                then write('(-',fz div tmp,'/',fm div tmp,')')
                else write('(',fz div tmp,'/',fm div tmp,')');
              writeln;
              break;
          end;
          sum:=sum+t;
      end;
end;

Begin
//    assign(INPUT , 'p10456.in'); ReSet(INPUT);
    while not(eof) do
      begin
          init;
          while k>0 do
            begin
                read(s.x,s.y);
                work;
                dec(k);
            end;
          readln;
      end;
End.
