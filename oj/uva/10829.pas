Const
    InFile     = 'p10829.in';
    OutFile    = 'p10829.out';
    Limit      = 50010;
    LimitSave  = 10;

Type
    Tdata      = array[1..Limit] of char;
    Tnums      = array[1..Limit , 1..LimitSave] of longint;

Var
    data       : Tdata;
    rep        : Tnums;
    T , cases ,
    answer ,
    N , G      : longint;

procedure init;
var
    ch         : char;
begin
    read(G); N := 0;
    repeat read(ch); until ch <> ' ';
    while ch <> ' ' do
      begin
          inc(N); data[N] := ch;
          if eoln or eof then ch := ' ' else read(ch);
      end;
end;

procedure process(L : longint);
var
    Len , delta ,
    i , last ,
    j , gcd ,
    p1 , p2    : longint;
    ok         : boolean;
begin
    gcd := G; while L mod gcd <> 0 do dec(gcd);

    i := L; last := L; delta := -1;
    while i <= N - G - L do
      begin
          ok := false;
          if last > gcd then
            begin
                ok := true;
                p1 := i - last + 1; p2 := p1 + L + G;
                if (rep[p1 , gcd] * gcd < last) or (rep[p2 , gcd] * gcd < last)
                  then ok := false
                  else for j := 0 to gcd - 1 do
                        if data[p1 + j] <> data[p2 + j] then
                          begin ok := false; break; end;
            end;
          if not ok then
            begin
                j := i;
                while (i - j + 1 <= last) and (data[j] = data[j + L + G]) do
                  dec(j);
            end;
          if ok or (i - j + 1 > last)
            then begin
                     delta := rep[i - L + 1 , gcd] * gcd - L * 2 - G + 1;
                     if delta < 1 then delta := 1;
                     inc(answer , delta); inc(i , delta); last := 1;
                 end
            else begin last := L - (i - j); i := j + L; end;
      end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    answer := 0;
    for i := N downto 1 do
      for j := 1 to LimitSave do
        if i + j - 1 > N
          then rep[i , j] := 0
          else begin
                   rep[i , j] := rep[i + j , j] + 1;
                   if (j > 1) and (rep[i , 1] >= j * 2) then continue;
                   for k := 0 to j - 1 do
                     if data[i + k] <> data[i + j + k] then
                       begin rep[i , j] := 1; break; end;
               end;
    for i := (N - G) div 2 downto 1 do
      process(i);
end;

procedure out;
begin
    inc(Cases);
    writeln('Case ' , cases , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(T); cases := 0;
      while cases < T do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.
