Var
    i , j , tmp ,
    N , M      : longint;
    backupA ,
    p1 , p2 ,
    A , B      : array[1..100] of longint;
    answer     : array[1..100 , 1..100] of longint;
    noanswer   : boolean;
Begin
    readln(N , M);
    while N + M <> 0 do
      begin
          for i := 1 to N do read(A[i]);
          for i := 1 to M do read(B[i]);
          for i := 1 to N do p1[i] := i;
          for i := 1 to M do p2[i] := i;
          for i := 1 to N do
            for j := i + 1 to N do
              if A[p1[i]] < A[p1[j]] then
                begin tmp := p1[i]; p1[i] := p1[j]; p1[j] := tmp; end;
          for i := 1 to M do
            for j := i + 1 to M do
              if B[p2[i]] < B[p2[j]] then
                begin tmp := p2[i]; p2[i] := p2[j]; p2[j] := tmp; end;
          backupA := A;
          noanswer := false;
          for i := 1 to N do
            begin
                j := 1;
                while (j <= M) and (A[p1[i]] > 0) do
                  begin
                      if B[p2[j]] > 0 then
                        begin dec(B[p2[j]]); answer[p1[i] , A[p1[i]]] := p2[j]; dec(A[p1[i]]); end;
                      inc(j);
                  end;
                if A[p1[i]] <> 0 then
                  begin noanswer := true; break; end;
            end;
          if noanswer
            then writeln(0)
            else begin
                     writeln(1);
                     for i := 1 to N do
                       begin
                           for j := 1 to backupA[i] do
                             write(answer[i , j] , ' ');
                           writeln;
                       end;
                 end;
          readln(N , M);
      end;
End.