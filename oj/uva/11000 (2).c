#include <stdio.h>

int main(void)
{
  int n;
  unsigned int f, m, t;

  while (scanf("%d", &n) == 1 && n >= 0) {
    f = 1, m = 0;
    while (n--)
      t = m + f, f = m + 1, m = t;
    printf("%u %u\n", m, m + f);
  }

  return 0;
}
