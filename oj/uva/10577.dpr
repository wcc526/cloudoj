{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10577.in';
    OutFile    = 'p10577.out';

Type
    Tpoint     = record
                     x , y                   : extended;
                 end;
    TLine      = record
                     A , B , C               : extended;
                 end;

Var
    p1 , p2 , p3
               : Tpoint;
    N , Cases  : longint;
    answer     : extended;

function init : boolean;
begin
    read(N);
    if N = 0
      then init := false
      else begin
               init := true;
               read(p1.x , p1.y , p2.x , p2.y , p3.x , p3.y);
           end;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p1.y - p2.y; L.B := p2.x - p1.x;
    L.C := p1.x * P2.y - p2.x * p1.y;
end;

procedure Get_Vertical_Line(p1 , p2 : Tpoint; var L : TLine);
var
    L1         : TLine;
    p          : Tpoint;
begin
    Get_Line(p1 , p2 , L1);
    L.A := -L1.B; L.B := L1.A;
    p.x := (p1.x + p2.x) / 2; p.y := (p1.y + p2.y) / 2;
    L.C := -(p.x * L.A + p.y * L.B);
end;

procedure Get_Crossing(L1 , L2 : TLine; var p : Tpoint);
begin
    p.x := (L2.C * L1.B - L1.C * L2.B) / (L1.A * L2.B - L2.A * L1.B);
    p.y := (L2.C * L1.A - L1.C * L2.A) / (L1.B * L2.A - L2.B * L1.A);
end;

procedure work;
var
    L1 , L2    : TLine;
    O , p      : Tpoint;
    minx , maxx ,
    miny , maxy ,
    sinA , cosA ,
    sinB , cosB ,
    sinC , cosC ,
    R          : extended;
    i          : longint;
begin
    Get_Vertical_Line(p1 , p2 , L1);
    Get_Vertical_Line(p2 , p3 , L2);
    Get_Crossing(L1 , L2 , O);

    R := sqrt(sqr(O.x - p1.x) + sqr(O.y - p1.y));
    sinA := (p1.y - O.y) / R; cosA := (p1.x - O.x) / R;

    minx := 1e10; maxx := -1e10; miny := 1e10; maxy := -1e10;
    for i := 1 to N do
      begin
          sinB := sin(2 * pi / N * i); cosB := cos(2 * pi / N * i);
          sinC := sinA * cosB + sinB * cosA;
          cosC := cosA * cosB - sinA * sinB;
          p.x := O.x + cosC * R; p.y := O.y + sinC * R;
          if p.x < minx then minx := p.x;
          if p.x > maxx then maxx := p.x;
          if p.y < miny then miny := p.y;
          if p.y > maxy then maxy := p.y; 
      end;
    answer := (maxx - minx) * (maxy - miny);
end;

procedure out;
begin
    inc(cases);
    writeln('Polygon ' , cases , ': ' , answer : 0 : 3);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      cases := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
