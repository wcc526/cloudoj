{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10403.in';
    OutFile    = 'p10403.out';
    Limit      = 10000;
    LimitN     = 26;

Type
    Tkey       = record
                     signal , max            : longint; { 0 : =; 1 : >; -1 : < }
                     data                    : array[1..LimitN] of longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;
    Tpath      = array[1..LimitN] of longint;
    Tvisited   = array[1..LimitN] of boolean;

Var
    data       : Tdata;
    path       : Tpath;
    visited    : Tvisited;
    N , M      : longint;

procedure init;
var
    i , p , j  : longint;
    ch         : char;
begin
    readln(N , M);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to M do
      with data[i] do
        begin
            j := 1;
            while not eoln do
              begin
                  read(ch); ch := upCase(ch);
                  case ch of
                    '='       : begin signal := 0; j := -1; end;
                    '>'       : begin signal := 1; j := -1; end;
                    '<'       : begin signal := -1; j := -1; end;
                    'A'..'Z'  : begin
                                    p := ord(ch) - ord('A') + 1;
                                    if p > max then max := p;
                                    data[p] := j;
                                end;
                  end;
              end;
            readln;
        end;
end;

function check(i , signal : longint; equal : boolean) : boolean;
var
    sum , j , 
    p1 , p2    : longint;
begin
    p1 := 0; p2 := N + 1; sum := 0;
    for j := 1 to N do
      if data[i].data[j] <> 0 then
        if path[j] <> 0
          then inc(sum , path[j] * data[i].data[j])
          else if data[i].data[j] * signal > 0
                 then begin
                          repeat dec(p2); until not visited[p2];
                          inc(sum , p2 * data[i].data[j]);
                      end
                 else begin
                          repeat inc(p1); until not visited[p1];
                          inc(sum , p1 * data[i].data[j]);
                      end;
    check := (sum * signal > 0) or equal and (sum = 0);
end;

function dfs(step , p : longint) : boolean;
var
    sum , i    : longint;
begin
    dfs := false;
    while (p <= M) and (data[p].max = step - 1) do
      begin
          sum := 0;
          for i := 1 to step - 1 do sum := sum + path[i] * data[p].data[i];
          if data[p].signal = 0
            then if sum <> 0 then exit
                             else
            else if sum * data[p].signal < 0 then exit
                                             else;
          inc(p);
      end;
    for i := p to M do
      if data[i].signal <> 0
        then if not check(i , data[i].signal , false) then exit else
        else if not check(i , -1 , true) or not check(i , 1 , true) then exit else;
    if step > N then
      begin
          write('Solution:');
          for i := 1 to N do
            write(' ' , path[i]);
          writeln;
          dfs := true;
          exit;
      end;
    for i := 1 to N do
      if not visited[i] then
        begin
            visited[i] := true;
            path[step] := i;
            if dfs(step + 1 , p)
              then begin
                       dfs := true;
                       exit;
                   end;
            path[step] := 0;
            visited[i] := false;
        end;
end;

procedure workout;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(path , sizeof(path) , 0);
    if not dfs(1 , 1) then
      writeln('No solution possible!');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
