Var
    p          : int64;
    answer     : array[0..50] of int64;
    name       : array[1..50] of string[50];
    N , i , j  : longint;
Begin
    for i := 2 to 50 do
      begin
          str(i , name[i]);
          name[i] := '^' + name[i];
      end;
    name[1] := '';
    while not eof do
      begin
          readln(N);
          answer[0] := 1;
          for j := 1 to N do
            begin
                read(p);
                for i := j downto 1 do
                  answer[i] := answer[i - 1];
                answer[0] := 0;
                for i := 0 to j - 1 do
                  answer[i] := answer[i] - answer[i + 1] * p;
            end;
          readln;
          write('x' , name[N]);
          for i := N - 1 downto 1 do
            if answer[i] <> 0
              then if answer[i] > 0
                     then if answer[i] = 1
                            then write(' + ' , 'x' , name[i])
                            else write(' + ' , answer[i] , 'x' , name[i])
                     else if answer[i] = -1
                            then write(' - ' , 'x' , name[i])
                            else write(' - ' , -answer[i] , 'x' , name[i]);
          if answer[0] >= 0
            then writeln(' + ' , answer[0] , ' = 0')
            else writeln(' - ' , -answer[0] , ' = 0');
      end;
End.