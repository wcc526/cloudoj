Const
    InFile     = 'p10376.in';
    Limit      = 1000;
    minimum    = 1e-6;

Type
    Tcircle    = record
                     x , y ,
                     R        : double;
                 end;
    Tdata      = array[1..Limit] of Tcircle;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    Left , Right
               : double;
    N , cases  : longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    readln(N);
    for i := 1 to N do
      with data[i] do
        readln(x , y , R);
end;

function touch(i : longint; y : double) : boolean;
begin
    touch := abs(y - data[i].y) < data[i].R - minimum;
end;

procedure touch_Left(p : longint);
var
    tmp        : double;
begin
    if data[p].x < data[p].R - minimum then
      begin
          tmp := -sqrt(sqr(data[p].R) - sqr(data[p].x)) + data[p].y;
          if tmp < Left then Left := tmp;
      end;
end;

procedure touch_Right(p : longint);
var
    tmp        : double;
begin
    if 1000 - data[p].x < data[p].R - minimum then
      begin
          tmp := -sqrt(sqr(data[p].R) - sqr(1000 - data[p].x)) + data[p].y;
          if tmp < Right then Right := tmp;
      end;
end;

function dfs(p : longint) : boolean;
var
    i          : longint;
begin
    touch_Left(p); touch_Right(p);
    if touch(p , 0) then exit(false);
    visited[p] := true;
    for i := 1 to N do
      if not visited[i] then
        if sqrt(sqr(data[i].x - data[p].x) + sqr(data[i].y - data[p].y)) < data[i].R + data[p].R - minimum then
          if not dfs(i) then
            exit(false);
    exit(true);
end;

procedure work;
var
    i          : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    Left := 1000; Right := 1000;
    for i := 1 to N do
      if not visited[i] then
        if touch(i , 1000) then
          if not dfs(i) then
            begin
                writeln('Bill will be bitten.');
                exit;
            end;
    writeln('Bill enters at (0.00, ' , Left : 0 : 2 , ') and leaves at (1000.00, ' , Right : 0 : 2 , ').');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            if cases > 0 then writeln;
        end;
//    Close(INPUT);
End.
