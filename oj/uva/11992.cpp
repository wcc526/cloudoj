#include<cstdio>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;
int r,c,m;
const int maxn = 1000010;
const int inf = 1000000009;
int amax[22],amin[22],asum[22];
struct node{
    int l,r,sum,min,max,add,cov;
}T[maxn*5];
void build(int i,int id,int l,int r){
    int t=4*i*c;
    T[id+t].l=l;  T[id+t].r=r;  T[id+t].add=0;  T[id+t].cov=-1;T[id+t].sum=T[id+t].min=T[id+t].max=0;
    if(l==r)   return ;
    int m=(l+r)>>1;
    build(i,id<<1,l,m);build(i,id<<1|1,m+1,r);
}
void pushDown(int k,int id){
    int t=4*k*c;
    if(T[t+id].cov!=-1){
        if(T[t+id].l!=T[t+id].r){
        T[t+(id<<1)].add=T[t+(id<<1|1)].add=0;
        T[t+(id<<1)].cov=T[t+(id<<1|1)].cov=T[t+id].cov;
        T[t+(id<<1)].max=T[t+(id<<1|1)].max=T[t+id].cov;
        T[t+(id<<1)].min=T[t+(id<<1|1)].min=T[t+id].cov;
        T[t+(id<<1)].sum=(T[t+(id<<1)].r-T[t+(id<<1)].l+1)*T[t+id].cov;
        T[t+(id<<1|1)].sum=(T[t+(id<<1|1)].r-T[t+(id<<1|1)].l+1)*T[t+id].cov;

        }
        T[t+id].cov=-1;
    }
    int tt=T[t+id].add;
    if(T[t+id].add>0){
        if(T[t+id].l!=T[t+id].r){
        T[t+(id<<1)].add+=tt; T[t+(id<<1|1)].add+=tt;
        T[t+(id<<1)].max+=tt; T[t+(id<<1|1)].max+=tt;
        T[t+(id<<1)].min+=tt; T[t+(id<<1|1)].min+=tt;
        T[t+(id<<1)].sum+=tt*(T[t+(id<<1)].r-T[t+(id<<1)].l+1);
        T[t+(id<<1|1)].sum+=tt*(T[t+(id<<1|1)].r-T[t+(id<<1|1)].l+1);

        }
        T[t+id].add=0;
    }
}
void pushUp(int k,int id){
    int t=4*k*c;
    T[t+id].max=max(T[t+(id<<1)].max,T[t+(id<<1|1)].max);
    T[t+id].min=min(T[t+(id<<1)].min,T[t+(id<<1|1)].min);
    T[t+id].sum=T[t+(id<<1)].sum+T[t+(id<<1|1)].sum;
}
void update_add(int k,int id,int l,int r,int val){
     int t=4*k*c+id;
     if(T[t].l==l&&T[t].r==r){
        T[t].add+=val;
        T[t].max+=val;
        T[t].min+=val;
        T[t].sum+=val*(r-l+1);
        return ;
     }
     pushDown(k,id);
     int m=(T[t].l+T[t].r)>>1;
     if(m>=r)   update_add(k,id<<1,l,r,val);
     else if(m<l) update_add(k,id<<1|1,l,r,val);
     else{
          update_add(k,id<<1,l,m,val);
          update_add(k,id<<1|1,m+1,r,val);
     }
     pushUp(k,id);
}
void update_set(int k,int id,int l,int r,int val){
     int t=4*k*c+id;
     if(T[t].l==l&&T[t].r==r){
        T[t].cov=val;  T[t].add=0;
        T[t].max=val;
        T[t].min=val;
        T[t].sum=val*(r-l+1);
        return ;
     }
     pushDown(k,id);
     int m=(T[t].l+T[t].r)>>1;
     if(m>=r)   update_set(k,id<<1,l,r,val);
     else if(m<l) update_set(k,id<<1|1,l,r,val);
     else{
          update_set(k,id<<1,l,m,val);
          update_set(k,id<<1|1,m+1,r,val);
     }
     pushUp(k,id);
}
void query(int k,int id,int l,int r){
    int t=4*k*c+id;
    if(T[t].l==l&&T[t].r==r){
         asum[k]+=T[t].sum;
         amax[k]=max(amax[k],T[t].max);
         amin[k]=min(amin[k],T[t].min);
         return ;
    }
     pushDown(k,id);
     int m=(T[t].l+T[t].r)>>1;

     if(m>=r)   query(k,id<<1,l,r);
     else if(m<l) query(k,id<<1|1,l,r);
     else{
          query(k,id<<1,l,m);
          query(k,id<<1|1,m+1,r);
     }
     pushUp(k,id);

}
int main(){
    while(scanf("%d%d%d",&r,&c,&m)!=EOF){
         for(int i=1;i<=r;i++)
         build(i,1,1,c+1);
         int id,x1,y1,x2,y2,v;
         while(m--){
             scanf("%d",&id);
             if(id==1){
                 scanf("%d%d%d%d%d",&x1,&y1,&x2,&y2,&v);
                 for(int i=x1;i<=x2;i++)
                    update_add(i,1,y1,y2,v);
             }
             else if(id==2){
                 scanf("%d%d%d%d%d",&x1,&y1,&x2,&y2,&v);
                 for(int i=x1;i<=x2;i++)
                    update_set(i,1,y1,y2,v);
             }
             else {
                 memset(asum,0,sizeof(asum));
                 scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
                 int sum=0,rmax=-inf,rmin=inf;
                 for(int i=x1;i<=x2;i++) amax[i]=-inf,amin[i]=inf;

                 for(int i=x1;i<=x2;i++){
                     query(i,1,y1,y2);
                     sum+=asum[i];
                     rmax=max(rmax,amax[i]);
                     rmin=min(rmin,amin[i]);
                 }
                 printf("%d %d %d\n",sum,rmin,rmax);
             }

         }
    }
    return 0;
}

