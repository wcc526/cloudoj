{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Var
    ch         : char;
    s          : string;
    num1 , num2 , 
    x1 , y1 , z1 ,
    r1 , x2 , y2 ,
    z2 , r2 , x3 ,
    y3 , z3    : comp;
    x , y , d , 
    x0 , y0 , z0 ,
    xa , ya , za ,
    xb , yb , zb 
               : extended;
Begin
    while not eof do
      begin
          read(ch); s := '';
          while ch <> ' ' do begin s := s + ch; read(ch); end;
          readln(x1 , y1 , z1 , r1 , x2 , y2 , z2 , r2 , x3 , y3 , z3);
          num1 := sqr(x1 - x3) + sqr(y1 - y3) + sqr(z1 - z3);
          num2 := sqr(x2 - x3) + sqr(y2 - y3) + sqr(z2 - z3);
          writeln(s);
          if num1 <= sqr(r1)
            then writeln('y')
            else writeln('n');
          if num2 <= sqr(r2)
            then writeln('y')
            else writeln('n');
          if (num1 <= sqr(r1)) or (num2 <= sqr(r2))
            then begin
                     d := sqrt(sqr(x1 - x2) + sqr(y1 - y2) + sqr(z1 - z2));
                     x := -(r2 * r2 - r1 * r1 - d * d) / 2 / d;
                     y := d - x;
                     x0 := (x1 * y + x2 * x) / d;
                     y0 := (y1 * y + y2 * x) / d;
                     z0 := (z1 * y + z2 * x) / d;
                     xa := x3 - x0; ya := y3 - y0; za := z3 - z0;
                     xb := x1 - x2; yb := y1 - y2; zb := z1 - z2;
                     if abs(xa * xb + ya * yb + za * zb) <= 1e-6
                       then writeln('n')
                       else writeln('y')
                 end
            else writeln('n');
      end;
End.
