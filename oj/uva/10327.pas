Var
    i , j , tmp ,
    count , N  : longint;
    data       : array[1..1000] of longint;

Begin
    while not eof do
      begin
          readln(N);
          for i := 1 to N do read(data[i]);
          readln;
          count := 0;
          for i := 1 to N do
            for j := 1 to N - 1 do
              if data[j] > data[j + 1] then
                begin
                    inc(count);
                    tmp := data[j]; data[j] := data[j + 1]; data[j + 1] := tmp;
                end;
          writeln('Minimum exchange operations : ' , count);
      end;
End.