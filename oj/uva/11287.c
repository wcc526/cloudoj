#include <stdio.h>
#include <string.h>

int main()
{
  long long a, p, r, x, y;
  char t[31624];
  int i, j;

  memset(t, 1, sizeof(t));
  for (i = 2; i < 260; ++i)
    if (t[i])
      for (j = i * i; j < 31624; j += i)
        t[j] = 0;

  while (scanf("%lld %lld", &p, &a) == 2 && a && p) {
    if (p < 31624 && t[p])
      goto no;
    for (r = 1, x = a, y = p; y; y >>= 1, x = x * x % p)
      if (y & 1)
        r = r * x % p;
    if (r ^ a)
      goto no;
    for (i = 2; i * i < p; ++i) {
      if (t[i] && p % i == 0) {
        puts("yes");
        goto yes;
      }
    }
  no:
    puts("no");
  yes:;
  }

  return 0;
}
