Const
    InFile     = 'p10763.in';
    OutFile    = 'p10763.out';
    Limit      = 500000;

Type
    Tkey       = record
                     p1 , p2 , signal        : longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    answer     : boolean;
    N          : longint;

function init : boolean;
var
    i , tmp    : longint;
begin
    readln(N);
    if N = 0
      then exit(false)
      else begin
               init := true;
               for i := 1 to N do
                 with data[i] do
                   begin
                       readln(p1 , p2);
                       if p1 < p2
                         then signal := -1
                         else begin
                                  signal := 1;
                                  tmp := p1; p1 := p2; p2 := tmp;
                              end;
                   end;
           end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and ((data[stop].p1 > key.p1) or (data[stop].p1 = key.p1) and (data[stop].p2 > key.p2)) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and ((data[start].p1 < key.p1) or (data[start].p1 = key.p1) and (data[start].p2 < key.p2)) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    last , i   : longint;
begin
    qk_sort(1 , N);
    answer := false;
    last := 0;
    for i := 1 to N do
      if (i > 1) and (data[i - 1].p1 = data[i].p1) and (data[i - 1].p2 = data[i].p2)
        then inc(last , data[i].signal)
        else begin
                 if last <> 0 then exit;
                 last := data[i].signal;
             end;
    if last <> 0 then exit;
    answer := true;
end;

procedure out;
begin
    if answer then writeln('YES') else writeln('NO');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.