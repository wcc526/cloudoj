Const
    Limit      = 30;

Type
    Tdata      = array[0..Limit , 1..Limit] of extended;
    Tvisited   = array[0..Limit , 1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    N          : longint;

function C(N : extended; M : longint) : extended;
var
    i          : longint;
    res        : extended;
begin
    res := 1;
    for i := 1 to M do
      res := res * (N - i + 1);
    for i := 1 to M do
      res := res / i;
    exit(res);
end;

function dfs(N , x : longint) : extended;
var
    k          : longint;
    t1 , t2 , t3
               : extended;
begin
    if N = 0 then exit(1);
    if x = 0 then exit(0);
    if N = 1 then exit(1);
    if visited[N , x] then exit(data[N , x]);
    visited[N , x] := true; data[N , x] := 0;
    for k := N div x downto 0 do
      begin
          if x = 1 then t1 := 1 else t1 := dfs(x , x - 1);
          t2 := C(t1 + k - 1 , k);
          t3 := dfs(N - k * x , x - 1);
          data[N , x] := data[N , x] + t2 * t3;
      end;
    exit(data[N , x]);
end;

Begin
    fillchar(visited , sizeof(visited) , 0);
    readln(N);
    while N <> 0 do
      begin
          if N = 1
            then writeln(1)
            else writeln(dfs(N , N - 1) * 2 : 0 : 0);
          readln(N);
      end;
End.