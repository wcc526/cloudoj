#include <stdio.h>

int main()
{
  long long t[41];
  int c, n;

  t[0] = t[1] = 1, t[2] = 5;
  for (n = 3; n < 41; ++n)
    t[n] = t[n - 1] + (t[n - 2] << 2) + (t[n - 3] << 1);

  scanf("%d", &c);
  while (c--)
    scanf("%d", &n), printf("%lld\n", t[n]);

  return 0;
}
