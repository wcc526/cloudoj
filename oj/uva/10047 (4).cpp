#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
const int inf=0x3f3f3f3f;
int n,m;
char s[33][33];
int dis[33][33][5][4];
//0:up 1:right 2:down 3:left
int dx[]={-1,0,1,0};
int dy[]={0,1,0,-1};
struct node
{
    int x,y,c,d;
    node(int x=0,int y=0,int c=0,int d=0):x(x),y(y),c(c),d(d){}
};
int bfs()
{
    memset(dis,63,sizeof(dis));
    queue<node>q;
    for(int i=0;i<n;i++) for(int j=0;j<m;j++) if(s[i][j]=='S') q.push(node(i,j,0,0)),dis[i][j][0][0]=0;
    while(!q.empty())
    {
        node v,u=q.front();q.pop();
        if(s[u.x][u.y]=='T'&&u.c==0) return dis[u.x][u.y][u.c][u.d];

        v=u;
        v.d=(v.d+1)%4;
        if(dis[v.x][v.y][v.c][v.d]==inf)
            dis[v.x][v.y][v.c][v.d]=dis[u.x][u.y][u.c][u.d]+1,q.push(v);

        v=u;
        v.d=(v.d+3)%4;
        if(dis[v.x][v.y][v.c][v.d]==inf)
            dis[v.x][v.y][v.c][v.d]=dis[u.x][u.y][u.c][u.d]+1,q.push(v);

        v=u;
        v.x+=dx[v.d];
        v.y+=dy[v.d];
        v.c=(v.c+1)%5;
        if(~v.x&&~v.y&&v.x<n&&v.y<m&&dis[v.x][v.y][v.c][v.d]==inf&&s[v.x][v.y]!='#')
            dis[v.x][v.y][v.c][v.d]=dis[u.x][u.y][u.c][u.d]+1,q.push(v);
    }
    return -1;
}
int main()
{
    int cas=0;
    while(scanf("%d%d",&n,&m),n||m)
    {
        for(int i=0;i<n;i++) scanf("%s",s[i]);
        int ans=bfs();if(cas) puts("");
        printf("Case #%d\n",++cas);
        if(ans==-1) puts("destination not reachable");
        else printf("minimum time = %d sec\n",ans);
    }
    return 0;
}
