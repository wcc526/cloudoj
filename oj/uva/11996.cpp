#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>

using namespace std;
typedef long long LL;
typedef unsigned int uint;
const int Max = 500010;
const int P = 3;
const int Mod1 = 395373773;
const int Mod2 = 1000003;
int F1[Max];//,F2[Max];
struct node{
    char rvs,rf;//data needed for reverse operations
    char d;
    int sz;
    uint v1[2];//,v2[2];
    node *f,*s[2];
}a[Max],*ms[Max],*rQ[Max];
int na = 0,ns = 0;
struct Splay{
    node *root;
    Splay(){root = NULL;}
    void reset(){root = NULL;}
    void output(node *x,int d) {
        if (x){
            //pushD(x);
            output(x->s[0],d+1);
            for (int i = 0;i < d;i++) putchar('\t');
            //printf("[%d](%d) %d:%d(%d),%d(%d) (%d,%d)|%d\n",x-a,x->sz,x->d,x->v1[0],x->v2[0],x->v1[1],x->v2[1],x->rvs,x->rf,x->f?x->f-a:-1);
            printf("[%d](%d) %c:%d,%d (%d,%d)|%d\n",x-a,x->sz,x->d+'a',x->v1[0],x->v1[1],x->rvs,x->rf,x->f?x->f-a:-1);
            output(x->s[1],d+1);
        }
    }
    void output2(node *x,int d) {
        if (x){
            pushD(x);
            output2(x->s[x->rvs],d+1);
            //for (int i = 0;i < d;i++) putchar('\t');
            //printf("[%d](%d) %d:%d(%d),%d(%d) (%d,%d)|%d\n",x-a,x->sz,x->d,x->v1[0],x->v2[0],x->v1[1],x->v2[1],x->rvs,x->rf,x->f?x->f-a:-1);
            //printf("[%d](%d) %c:%d,%d (%d,%d)|%d\n",x-a,x->sz,x->d+'a',x->v1[0],x->v1[1],x->rvs,x->rf,x->f?x->f-a:-1);
            putchar(x->d+'0');
            output2(x->s[!x->rvs],d+1);
        }
    }
    ///////////////////////////////////////////////
    void rotate(node *x,char c){//zig or zag
        node *y = x->f;
        char nc = c ^ 1;
        pushD(y),pushD(x);
        char xc = c ^ x->rvs ^ y->rvs;
        if (y->s[nc] = x->s[xc])
            y->s[nc]->f = y;
        if (x->f = y->f)
            x->f->s[x->f->s[1] == y] = x;
        x->s[xc] = y,y->f = x;
        pushU(y);
    }
    /*splay x to be a son of gf(may be NULL)*/
    void splay(node *x,node *gf){
        if (x == NULL) return;
        pushD(x);
        while (x->f != gf){
            if (x->f->f == gf)
                rotate(x,x->f->s[0] == x);
            else{
                node *y = x->f;
                char c = y->f->s[0] == y;
                if (y->s[c] == x) rotate(x,!c);
                else rotate(y,c);
                rotate(x,c);
            }
        }
        pushU(x);
    }
    ///////////////////////////////////////////////
    node *newNode(char d){
        node *x;
        if (ns) x = ms[--ns];//ask for memory
        else x = &a[na++];
        x->rf = x->rvs = 0;
        x->d = d,x->sz = 1;
        x->f = x->s[0] = x->s[1] = NULL;
        x->v1[0] = x->v1[1] = d;
        //x->v1[0] = x->v1[1] = d;
        return x;
    }
    node *maxValue(node *x){
        while (x->s[!x->rvs]){
            pushD(x);
            x = x->s[!x->rvs];
        }
        return x;
    }
    node *minValue(node *x){
        while (x->s[x->rvs]){
            pushD(x);
            x = x->s[x->rvs];
        }
        return x;
    }
    node *findK(node *&x,int k){//counted from 0
        //printf("findK for %d\n",k);
        if (x == NULL || k >= x->sz) return NULL;
        node *f = x->f;
        int d;
        while (1){
            pushD(x);
            char c = x->rvs;//replace 0 with c and 1 with !c
            d = x->s[c]?x->s[c]->sz:0;
            //printf("%d %d\n",d,k);
            if (k == d) break;
            if (k < d) x = x->s[c];
            else x = x->s[!c],k -= d+1;
        }
        splay(x,f);//?
        return x;
    }
    node *findInterval(int a,int b){//counted from 1
        //printf("int [%d,%d]\n",a,b);
        if (a == 1 && b == root->sz)
            return root;
        if (a == 1){
            root = findK(root,b-a+1);
            return root->s[root->rvs];
        }
        root = findK(root,a-2);
        if (b == root->sz)
            return root->s[!root->rvs];
        char c = root->rvs;
        node *p = root->s[!c];
        p = findK(p,b-a+1);
        root->s[!c] = p;
        return p->s[p->rvs];
    }
    /*del the kth element of the tree rooted by x*/
    void delKth(node *&x,int k){
        if (k >= x->sz) return;
        findK(x,k);
        char c = x->rvs;//replace 0 with c and 1 with !c
        if (x->s[c] == NULL){
            ms[ns++] = x;
            x = x->s[!c];
            x->f = NULL;
        }else{
            node *p = maxValue(x->s[c]);
            x->s[c]->f = NULL;
            splay(p,NULL);
            if (p->s[!p->rvs] = x->s[!c])
                x->s[!c]->f = p;
            ms[ns++] = x;
            pushU(x = p);
        }
    }
    /*insert new item just behind the ath node*/
    void insAfter(int a,char d){
        node *x = newNode(d);
        if (a == 0){
            x->s[1] = root;
            if (root) root->f = x;
            pushU(root = x);
            return ;
        }
        root = findK(root,a-1);
        pushD(root);
        char c = root->rvs;
        if (root->s[!c]) root->s[!c]->f = x;
        x->s[c] = root,root->f = x;
        x->s[!c] = root->s[!c],root->s[!c] = NULL;
        x->rvs = root->rvs;
        pushU(root);
        pushU(root = x);
    }
    ///////////////////////////////////////////////
    void out(){output(root,0);}
    void out2(){output2(root,0);putchar('\n');}
    void pushU(node *x){
        node *p;
        x->sz = 0;
        char c = x->rvs;
        x->v1[0] = x->v1[1] = 0;
        //x->v2[0] = x->v2[1] = 0;
        int cl = 0;
        if (p = x->s[x->rvs]){
            x->sz += p->sz;
            x->v1[c] += p->v1[p->rvs];
            //x->v2[c] += p->v2[p->rvs];
            cl += p->sz;
        }
        x->sz++;
        x->v1[c] += (LL)F1[cl]*x->d;
        //x->v2[c] += (LL)F2[cl]*x->d % Mod2;
        cl++;
        if (p = x->s[!x->rvs]){
            x->sz += p->sz;
            x->v1[c] += (LL)F1[cl]*p->v1[p->rvs];
            //x->v2[c] += (LL)F2[cl]*p->v2[p->rvs] % Mod2;
            cl += p->sz;
        }
        cl = 0;
        if (p = x->s[!x->rvs]){
            x->v1[!c] += p->v1[!p->rvs];
            //x->v2[!c] += p->v2[p->rvs];
            cl += p->sz;
        }
        x->v1[!c] += (LL)F1[cl]*x->d;
        //x->v2[!c] += (LL)F2[cl]*x->d % Mod2;
        cl++;
        if (p = x->s[x->rvs]){
            x->v1[!c] += (LL)F1[cl]*p->v1[!p->rvs];
            //x->v2[!c] += (LL)F2[cl]*p->v2[p->rvs] % Mod2;
            cl += p->sz;
        }
        //x->v1[0] %= Mod1,x->v1[1] %= Mod1;
        //x->v2[0] %= Mod2,x->v2[1] %= Mod2;
    }
    void pushD(node *x){
        if (x->rf){
            node *p;
            for (char c = 0;c < 2;c++)
                if (p = x->s[c])
                    p->rvs ^= 1,p->rf ^= 1;
            x->rf = 0;
        }
    }
    node *build(int d,int s,int t,node *f){
        if (s > t) return NULL;
        node *p = &a[d];
        p->f = f;
        p->s[0] = build((s+d-1) >> 1,s,d-1,p);
        p->s[1] = build((d+1+t) >> 1,d+1,t,p);
        pushU(p);
        return p;
    }
    void init(char *st,int n){
        na = n;
        for (int i = 0;i < n;i++){
            a[i].rf = a[i].rvs = 0;
            a[i].v1[0] = a[i].v1[1] =
            //a[i].v2[0] = a[i].v2[1] =
            a[i].d = st[i]-'0';
        }
        root = build((n-1) >> 1,0,n-1,NULL);
    }
    bool checkLCP(int x,int y,int d){
        //printf("checkLCP %d %d (%d)\n",x,y,d);
        node *p = findInterval(x,x+d-1);
        uint rx1 = p->v1[p->rvs];
        //out();printf("rx = %d\n",rx1);
        //int rx2 = p->v2[p->rvs];
        p = findInterval(y,y+d-1);
        uint ry1 = p->v1[p->rvs];
        //out();printf("ry = %d | %d\n",ry1,p->sz);
        //int ry2 = p->v2[p->rvs];
        return rx1 == ry1;// && rx2 == ry2;
    }
    int LCP(int x,int y){
        if (x > y) swap(x,y);
        if (y > root->sz) return 0;
        int n = root->sz;
        int s = 0,t = n-y+2;
        while (s+1 < t){
            int d = (s+t) / 2;
            if (checkLCP(x,y,d)) s = d;
            else t = d;
        }
        return s;
    }
    void reverse(int x,int y){
        if (x > y) swap(x,y);
        x = max(x,1);
        y = min(y,root->sz);
        node *p = findInterval(x,y);
        pushD(p);
        p->rf ^= 1,p->rvs ^= 1;
        pushU(p);
        splay(root = p,NULL);
    }
    void modify(int x,int c){
        if (x > root->sz) return ;
        root = findK(root,x-1);
        pushD(root);
        root->d = c;
        pushU(root);
    }
    void makeSt(node *x,char *st,int &c){
        if (x == NULL) return ;
        pushD(x);
        makeSt(x->s[x->rvs],st,c);
        st[c++] = '#';
        st[c++] = x->d+'a';
        makeSt(x->s[!x->rvs],st,c);
        pushU(x);
    }
    int readSt(char *st){
        int cl = 0;
        makeSt(root,st,cl);
        return cl;
    }
}ST;
char st[Max];
int main(){
    F1[0] = 1;//,F2[0] = 1;
    for (int i = 1;i < Max;i++){
        F1[i] = F1[i-1]*P;
        //F2[i] = F2[i-1]*P % Mod2;
    }
    //freopen("W:\\lcp.in","r",stdin);freopen("W:\\zt.out","w",stdout);
    //while (scanf("%s",st) != EOF){
    {
        int L,Q;
        scanf("%d%d%s",&L,&Q,st);
        ST.init(st,L);
        for (int cq = 0;cq < Q;cq++){
            //ST.out2();//ST.out();
            int qt;
            scanf("%d",&qt);
            if (qt == 4){
                int qx,qy;
                scanf("%d%d",&qx,&qy);
                printf("%d\n",ST.LCP(qx,qy));
            }else if (qt == 3){
                int qx,qy;
                scanf("%d%d",&qx,&qy);
                ST.reverse(qx,qy);
            }else if (qt == 2){
                int qx;
                scanf("%d",&qx);
                ST.delKth(ST.root,qx-1);
            }else if (qt == 1){
                int qx,qc;
                scanf("%d%d",&qx,&qc);
                ST.insAfter(qx,qc);
            }
        }
    }
    return 0;
}
