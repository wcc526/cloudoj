Const
    InFile     = 'p10369.in';
    Limit      = 500;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    cases ,
    N , S      : longint;
    D , ans    : double;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    readln(S , N);
    for i := 1 to N do
      read(data[i].x , data[i].y);
end;

procedure dfs(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    for i := 1 to N do
      if not visited[i] then
        if sqr(data[i].x - data[p].x) + sqr(data[i].y - data[p].y) <= D then
          dfs(i);
end;

function check(num : double) : boolean;
var
    i , count  : longint;
begin
    D := sqr(num);
    fillchar(visited , sizeof(visited) , 0);
    count := 0;
    for i := 1 to N do
      if not visited[i] then
        begin
            inc(count);
            dfs(i);
        end;
    check := count <= S;
end;

procedure work;
var
    start , stop ,
    mid        : double;
    i          : longint;
begin
    start := 0; stop := 15000;
    for i := 1 to 50 do
      begin
          mid := (start + stop) / 2;
          if check(mid)
            then begin ans := mid; stop :=  mid; end
            else start := mid;
      end;
end;

Begin
    readln(Cases);
    while Cases > 0 do
      begin
          init;
          work;
          writeln(ans : 0 : 2);
      end;
End.