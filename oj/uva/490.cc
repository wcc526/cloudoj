#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    string line;
    vector<string> lines;
    int maxLen = 0;
    while (getline(cin, line)) {
        lines.push_back(line);
        if (maxLen < line.size()) maxLen = line.size();
    }
    for (int i=0; i<maxLen; ++i) {
        for (int j=lines.size()-1; j>=0; --j) {
            if (lines[j].size() <= i) cout << " ";
            else cout << lines[j][i];
        }
        cout << endl;
    }

    return 0;
}
