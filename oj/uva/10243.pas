Const
    InFile     = 'p10243.in';
    Limit      = 1000;

Type
    Tdata      = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..Limit] of longint;
                   end;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    visited    : Tvisited;
    answer ,
    N          : longint;

function init : boolean;
var
    i , j      : longint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(visited , sizeof(visited) , 0);
    readln(N); if N = 0 then exit(false);
    init := true;
    for i := 1 to N do
      begin
          read(data[i].tot);
          for j := 1 to data[i].tot do
            read(data[i].data[j]);
          readln;
      end;
end;

function dfs(p : longint) : boolean;
var
    i , add    : longint;
begin
    visited[p] := true;
    add := 0;
    for i := 1 to data[p].tot do
      if not visited[data[p].data[i]] and not dfs(data[p].data[i]) then
        add := 1;
    inc(answer , add);
    exit(add = 1);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            answer := 0;
            dfs(1);
            if answer = 0 then answer := 1;
            writeln(answer);
        end;
//    Close(INPUT);
End.