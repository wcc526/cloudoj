{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10510.in';
    OutFile    = 'p10510.out';
    Limit      = 10000;

Type
    Tedge      = record
                     p1 , p2  : longint;
                 end;
    Tdata      = array[1..Limit] of Tedge;
    Tindex     = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tstack     = record
                     top      : longint;
                     data     : array[1..Limit] of longint;
                 end;

Var
    data       : Tdata;
    index      : Tindex;
    stack      : Tstack;
    visited ,
    instack ,
    covered    : Tvisited;
    T , N , M  : longint;
    answer     : boolean;

procedure init;
var
    i          : longint;
begin
    dec(T);
    fillchar(data , sizeof(data) , 0);
    fillchar(index , sizeof(index) , 0);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(instack , sizeof(instack) , 0);
    read(N , M);
    for i := 1 to M do
      begin
          read(data[i].p1 , data[i].p2);
          inc(data[i].p1); inc(data[i].p2);
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tedge;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop].p1 > key.p1) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start].p1 < key.p1) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function dfs(root : longint) : boolean;
var
    i , p      : longint;
begin
    inc(stack.top); stack.data[stack.top] := root; covered[stack.top] := false;
    visited[root] := true; instack[root] := true;
    i := index[root]; dfs := false;
    while (i <> 0) and (i <= M) and (data[i].p1 = root) do
      begin
          if visited[data[i].p2]
            then if instack[data[i].p2]
                   then begin
                            p := stack.top;
                            while stack.data[p] <> data[i].p2 do
                              begin
                                  if covered[p] then exit;
                                  covered[p] := true;
                                  dec(p);
                              end;
                        end
                   else exit
            else if not dfs(data[i].p2)
                   then exit;
          inc(i);
      end;
    dfs := true;
    dec(stack.top);
    instack[root] := false;
    covered[root] := false;
end;

procedure work;
var
    i          : longint;
begin
    qk_sort(1 , M);
    for i := 1 to M do
      if index[data[i].p1] = 0 then
        index[data[i].p1] := i;
    stack.top := 0;
    answer := dfs(1);
    if not answer then exit;
    for i := 2 to N do
      if not visited[i] then
        begin
            answer := false;
            exit;
        end;
end;

procedure out;
begin
    if answer
      then writeln('YES')
      else writeln('NO');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(T);
      while T > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
