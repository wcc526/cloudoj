{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10542.in';
    OutFile    = 'p10542.out';
    Limit      = 10;

Type
    Tvector    = array[1..Limit] of longint;

Var
    vector     : Tvector;
    totCase ,
    nowCase , D: longint;
    answer     : comp;

procedure init;
var
    i , tmp    : longint;
begin
    inc(nowCase);
    read(D);
    for i := 1 to D do read(vector[i]);
    for i := 1 to D do
      begin
          read(tmp);
          vector[i] := abs(vector[i] - tmp);
      end;
end;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then gcd := b
      else gcd := gcd(b mod a , a);
end;

procedure work;
var
    i , gcd_num ,
    tmp , times , j
               : longint;
begin
    answer := 0;
    
    for i := 0 to 1 shl D - 1 do
      begin
          tmp := i; times := -1; gcd_num := 0; j := 1;
          while tmp <> 0 do
            begin
                if odd(tmp) then
                  begin
                      times := times * (-1);
                      gcd_num := gcd(gcd_num , vector[j]);
                  end;
                inc(j); tmp := tmp div 2;
            end;
          answer := answer + gcd_num * times;
      end;
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
