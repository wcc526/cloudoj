#include<cstdio>

struct Point {
    float x,y;
};

bool intersect( Point start, Point end, Point left_top, Point right_bottom ){
    //
}

int main() {

    Point start, end, left_top, right_bottom;

    int num_case;
    scanf("%d", &num_case);

    while( num_case-- ){

        scanf("%f%f", &start.x , &start.y);
        scanf("%f%f", &end.x , &end.y);
        scanf("%f%f", &left_top.x , &left_top.y );
        scanf("%f%f", &right_bottom.x , &right_bottom.y);

        if( intersect(start,end, left_top,right_bottom) )
            puts("T");
        else
            puts("F");
    }

    return 0;
}
