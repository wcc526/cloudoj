{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $01000000}
{$MAXSTACKSIZE $01000000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10557.in';
    OutFile    = 'p10557.out';
    Limit      = 100;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[1..Limit , 1..Limit * Limit] of boolean;
    Tqueue     = array[1..Limit * Limit * Limit] of
                   record
                       p , energy            : longint;
                   end;
    Tdata      = array[1..Limit] of longint;

Var
    map        : Tmap;
    visited    : Tvisited;
    queue      : Tqueue;
    data       : Tdata;
    N , open ,
    closed     : longint;
    answer     : boolean;

function init : boolean;
var
    i , tot , p ,
    j          : longint;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(data , sizeof(data) , 0);
    read(N);
    if N = -1
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 begin
                     read(data[i] , tot);
                     for j := 1 to tot do
                       begin
                           read(p);
                           map[i , p] := true;
                       end;
                 end;
           end;
end;

function bfs : boolean;
var
    i , p ,
    energy     : longint;
begin
    open := 1; closed := 1; visited[1 , 100] := true;
    queue[1].p := 1; queue[1].energy := 100;
    bfs := true;
    while open <= closed do
      begin
          p := queue[open].p; energy := queue[open].energy;
          for i := 1 to N do
            if map[p , i] and (data[i] + energy <= Limit * Limit) and (energy > -data[i]) then
              if not visited[i , data[i] + energy] then
                begin
                    inc(closed);
                    queue[closed].p := i;
                    queue[closed].energy := data[i] + energy;
                    visited[i , data[i] + energy] := true;
                    if i = N then exit;
                end;
          inc(open);
      end;
    bfs := false;
end;

procedure work;
begin
    answer := bfs;
end;

procedure out;
begin
    if answer
      then writeln('winnable')
      else writeln('hopeless');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
