/*
 * uva_11401.cpp
 *
 *  Created on: 2012-12-12
 *      Author: Administrator
 */
#include <iostream>
using namespace std;
typedef long long LL;
LL f[1000010];
int main()
{
	f[3]=0;
	for(LL x=4;x<=1000000;++x)
		f[x]=f[x-1]+((x-1)*(x-2)/2-(x-1)/x)/2;
	int n;
	while(cin>>n)
	{
		if(n<3) break;
		cout << f[n]<< endl;
	}
	return 0;
}






