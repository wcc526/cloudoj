{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{ $R+,Q+,S+}
Const
    Limit     = 60;

Type
    Tdata     = array[1..Limit] of char;
    Topt      = array[0..Limit , 0..Limit] of int64;

Var
    data      : Tdata;
    opt       : Topt;
    T , N     : longint;

procedure init;
begin
    N := 0;
    fillchar(data , sizeof(data) , 0);
    while not eoln do
      begin
          inc(N);
          read(data[N]);
          if not (data[N] in ['A'..'Z']) then
            dec(N);
      end;
    readln;
end;

procedure work;
var
    i , j , k : longint;
begin
    fillchar(opt , sizeof(opt) , 0);
    opt[1 , 0] := 1;
    for i := 1 to N do
      begin
          opt[i , i] := 2;
          opt[i , i - 1] := 1;
      end;
    for i := N downto 1 do
      for j := i + 1 to N do
        begin
            if data[i] = data[j] then
              opt[i , j] := opt[i , j] + opt[i + 1 , j - 1];
            opt[i , j] := opt[i , j] + opt[i + 1 , j - 1];
            opt[i , j] := opt[i , j] + 2;
            for k := i + 1 to j - 1 do
              begin
                  if data[i] = data[k] then
                    opt[i , j] := opt[i , j] + opt[i + 1 , k - 1];
                  if data[k] = data[j] then
                    opt[i , j] := opt[i , j] + opt[k + 1 , j - 1];
              end;
        end;
end;

Begin
    readln(T);
    while T > 0 do
      begin
          init;
          work;
          writeln(opt[1 , N] - 1);
          dec(T);
      end;
End.
