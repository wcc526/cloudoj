{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10659.in';
    OutFile    = 'p10659.out';
    Limit      = 1000;
    LimitParagraph
               = 100;

Type
    Tparagraph = record
                     tot      : longint;
                     data     : array[1..Limit] of longint;
                 end;
    Tdata      = record
                     tot      : longint;
                     para     : array[1..LimitParagraph] of Tparagraph;
                 end;

Var
    data       : Tdata;
    cases ,
    X , Y , P  : longint;

procedure init;
var
    i          : longint;
    ch         : char; 
begin
    dec(Cases);
    readln(data.tot);
    for i := 1 to data.tot do
      with data.para[i] do
        begin
            tot := 1;
            repeat
              read(ch);
              if ch = ' ' then inc(tot) else inc(data[tot]);
            until eoln;
            readln;
        end;
    readln(X , Y);
end;

function check(X , Y : longint) : boolean;
var
    i , st , j : longint;
begin
    check := false;
    for i := 1 to data.tot do
      with data.para[i] do
        begin
            st := 0;
            for j := 1 to tot do
              begin
                  if X < P then exit;
                  if data[j] * P > Y then exit;
                  if Y - st < (data[j] + 1) * P
                    then if Y - st < data[j] * P
                           then begin st := (data[j] + 1) * P; dec(X , P); end
                           else if j = tot
                                  then continue
                                  else begin st := 0; dec(X , P); end 
                    else inc(st , (data[j] + 1) * P);
              end;
            if (st > 0) and (X < P) then exit;
            dec(X , P);
        end;
    check := true;
end;

procedure work;
begin
    P := 28;
    while (P >= 8) and not check(Y , X) do dec(p);
end;

procedure out;
begin
    if P < 8
      then writeln('No solution')
      else writeln(P);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
