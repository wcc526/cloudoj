Var
    i , C , N  : longint;

procedure solve(N : longint);
var
    a , b , c ,
    bestans    : longint;
begin
    bestans := maxlongint;
    for a := 10 downto 1 do
      if N mod a = 0 then
        for b := trunc(sqrt(N div a)) downto 1 do
          if N div a mod b = 0 then
            begin
                c := N div a div b;
                if a * b + b * c + c * a < bestans then
                  bestans := a * b + b * c + c * a;
                break;
            end;
    writeln(bestans * 2);
end;

Begin
    readln(C);
    for i := 1 to C do
      begin
          readln(N);
          solve(N);
      end;
End.