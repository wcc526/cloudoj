Var
    count ,
    a , b , i  : longint;
    answer     : extended;
Begin
    readln(a , b);
    count := 0;
    while (a + b <> 0) do
      begin
          answer := 0;
          inc(count);
          for i := 1 to b do
            answer := answer + (extended(a) * a * (i - 1) - extended(a) * (b - i) + extended(a) * (a + 1) / 2 * (b - 2 * i + 1));
          write('Case ' , count , ': ');
          answer := answer / 2;
          writeln(answer : 0 : 0);
          readln(a , b);
      end;
End.