{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10580.in';
    OutFile    = 'p10580.out';
    Limit      = 200000;

Type
    Tdata      = array[1..Limit] of char;
    Tsorted    = array[1..Limit] of longint;
    Tanswer    = record
                     tot      : longint;
                     data     : array[1..Limit] of
                                  record
                                      st , len              : longint;
                                  end;
                 end;

Var
    A , B      : Tdata;
    sorted     : Tsorted;
    answer     : Tanswer;
    N , M      : longint;

procedure init;
begin
//    assign(INPUT , InFile); ReSet(INPUT);
      N := 0; M := 0;
      while not eoln do begin inc(N); read(A[N]); end;
      readln;
      while not eof do begin inc(M); read(B[M]); end;
//    Close(INPUT);
end;

function bigger(p1 , p2 : longint) : boolean;
begin
    while (p1 <= M) and (p2 <= M) do
      if upcase(B[p1]) <> upcase(B[p2])
        then begin bigger := upcase(B[p1]) > upcase(B[p2]); exit; end
        else begin inc(p1); inc(p2); end;
    bigger := p2 > M;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key , tmp  : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := sorted[tmp]; sorted[tmp] := sorted[start];
    while start < stop do
      begin
          while (start < stop) and bigger(sorted[stop] , key) do dec(stop);
          sorted[start] := sorted[stop];
          if start < stop then inc(start);
          while (start < stop) and bigger(key , sorted[start]) do inc(start);
          sorted[stop] := sorted[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    sorted[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function _compare(p1 , p2 : longint; var len : longint) : boolean;  {bigger}
begin
    len := 0;
    while (p1 + len <= N) and (p2 + len <= M) do
      if upcase(A[p1 + len]) <> upcase(B[p2 + len])
        then begin
                 _compare := upcase(A[p1 + len]) > upcase(B[p2 + len]);
                 dec(len);
                 exit;
             end
        else inc(len);
    _compare := p2 + len > M;
    dec(len);
end;

procedure work;
var
    i , st , ed ,
    mid , maxlen ,
    tmp , maxst: longint;
begin
    for i := 1 to M do sorted[i] := i;
    qk_sort(1 , M);

    answer.tot := 0; i := 1;
    while (i <= N) and (A[i] = ' ') do inc(i);
    while i <= N do
      begin
          st := 1; ed := M; maxlen := -1; maxst := 0;
          while st <= ed do
            begin
                mid := (st + ed) div 2;
                if _compare(i , sorted[mid] , tmp)
                  then st := mid + 1
                  else ed := mid - 1;
                if tmp > maxlen then
                  begin maxst := sorted[mid]; maxlen := tmp; end;
            end;
          inc(answer.tot);
          answer.data[answer.tot].st := maxst;
          answer.data[answer.tot].len := maxlen;
          inc(i , maxlen + 1);
          while (i <= N) and (A[i] = ' ') do inc(i);
      end;
end;

procedure out;
var
    i , j      : longint;
begin
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      writeln(answer.tot);
      for i := 1 to answer.tot do
        begin
            for j := answer.data[i].st to answer.data[i].st + answer.data[i].len do
              write(B[j]);
            writeln;
        end;
//    Close(OUTPUT);
end;

Begin
    init;
    work;
    out;
End.
