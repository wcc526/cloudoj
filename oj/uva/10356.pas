Const
    InFile     = 'p10356.in';
    Limit      = 500;
    Maximum    = 1000000;

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;
    Tshortest  = array[1..Limit , 0..1] of longint;
    Tvisited   = array[1..Limit , 0..1] of boolean;

Var
    data       : Tdata;
    shortest   : Tshortest;
    visited    : Tvisited;
    M , cases ,
    count , N  : longint;

procedure init;
var
    p1 , p2 , cost ,
    i , j      : longint;
begin
    read(N , M);
    for i := 1 to N do
      for j := 1 to N do
        data[i , j] := maximum;
    for i := 1 to M do
      begin
          read(p1 , p2 , cost);
          inc(p1); inc(p2);
          if data[p1 , p2] > cost then data[p1 , p2] := cost;
          if data[p2 , p1] > cost then data[p2 , p1] := cost;
      end;
    readln;
end;

procedure work;
var
    i , j , k ,
    min1 , min2
               : longint;
begin
    for i := 1 to N do
      for j := 0 to 1 do
        shortest[i , j] := maximum;
    fillchar(visited , sizeof(visited) , 0);
    shortest[1 , 0] := 0;
    for i := 1 to N * 2 do
      begin
          min1 := 0; min2 := 0;
          for j := 1 to N do
            for k := 0 to 1 do
              if not visited[j , k] then
                if (min1 = 0) or (shortest[j , k] < shortest[min1 , min2]) then
                  begin
                      min1 := j; min2 := k;
                  end;

          if shortest[min1 , min2] >= maximum then break;
          if (min1 = N) and (min2 = 0) then break;
          visited[min1 , min2] := true;

          for j := 1 to N do
            if not visited[j , 1 - min2] then
              if shortest[j , 1 - min2] > shortest[min1 , min2] + data[min1 , j] then
                shortest[j , 1 - min2] := shortest[min1 , min2] + data[min1 , j];
      end;
end;

procedure out;
begin
    inc(Cases);
    writeln('Set #' , cases);
    if shortest[N , 0] >= maximum
      then writeln('?')
      else writeln(shortest[N , 0]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      cases := 0;
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.