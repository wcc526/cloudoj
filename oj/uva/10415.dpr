{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10415.in';
    OutFile    = 'p10415.out';
    press      : array[1..14 , 1..10] of longint
//                 1   2   3   4   5   6   7   8   9  10
               = ((0 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 1 , 1) ,         // c
                  (0 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 1 , 0) ,         // d
                  (0 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 0 , 0) ,         // e
                  (0 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 0) ,         // f
                  (0 , 1 , 1 , 1 , 0 , 0 , 0 , 0 , 0 , 0) ,         // g
                  (0 , 1 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0) ,         // a
                  (0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0) ,         // b
                  (0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0) ,         // C
                  (1 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 1 , 0) ,         // D
                  (1 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 0 , 0) ,         // E
                  (1 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 0) ,         // F
                  (1 , 1 , 1 , 1 , 0 , 0 , 0 , 0 , 0 , 0) ,         // G
                  (1 , 1 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0) ,         // A
                  (1 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0));         // B

Type
    Tdata      = array[1..10] of longint;

Var
    count ,
    visited    : Tdata;
    ch         : char;
    cases ,
    p , i      : longint;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            fillchar(count , sizeof(count) , 0);
            fillchar(visited , sizeof(visited) , 0);
            dec(Cases);
            while not eoln do
              begin
                  read(ch);
                  p := 0;
                  case ch of
                    'c'       : p := 1;
                    'd'       : p := 2;
                    'e'       : p := 3;
                    'f'       : p := 4;
                    'g'       : p := 5;
                    'a'       : p := 6;
                    'b'       : p := 7;
                    'C'       : p := 8;
                    'D'       : p := 9;
                    'E'       : p := 10;
                    'F'       : p := 11;
                    'G'       : p := 12;
                    'A'       : p := 13;
                    'B'       : p := 14;
                  end;
                  if p <> 0 then
                    for i := 1 to 10 do
                      if press[p , i] = 1
                        then begin
                                 if visited[i] = 0 then inc(count[i]);
                                 visited[i] := 1;
                             end
                        else visited[i] := 0;
              end;
            for i := 1 to 9 do write(count[i] , ' ');
            writeln(count[10]);
            readln;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
