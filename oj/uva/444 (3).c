#include <stdio.h>

int main(void)
{
  char b[243], *c, o[243];
  int i, v;

  o[242] = 0;

  while (gets(b)) {
    i = 242;
    c = b;
    if (b[0] < '0' || b[0] > '9') {
      while ((v = *c++)) {
        if (v >= 100)
          o[--i] = '1';
        o[--i] = (v % 100) / 10 + '0';
        o[--i] = (v % 10) + '0';
      }
    } else {
      while (*c) {
        v = (c[1] - '0') * 10 + (c[0] - '0');
        if (v < 32)
          v += 100, ++c;
        c += 2;
        o[--i] = v;
      }
    }
    puts(o + i);
  }

  return 0;
}
