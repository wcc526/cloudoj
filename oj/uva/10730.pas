Const
    InFile     = 'p10730.in';
    OutFile    = 'p10730.out';
    Limit      = 10000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    position ,
    data       : Tdata;
    answer     : boolean;
    N          : longint;

function read_num(var num : longint);
var
    ch         : char;
begin
    num := 0;
    repeat read(ch); until ch <> ' ';
    while (ch in ['0'..'9']) do
      begin
          num := num * 10 + ord(ch) - ord('0');
          if eof or eoln then ch := ' ' else read(ch);
      end;
end;

function init : boolean;
var
    i          : longint;
begin
    fillchar(position , sizeof(position) , 0);
    fillchar(data , sizeof(data) , 0);
    read_num(N);
    if N = 0
      then exit(false)
      else begin
               for i := 1 to N do begin read(data[i]); inc(data[i]); position[data[i]] := i; end;
               readln;
               exit(true);
           end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    answer := false;
    for i := 1 to N do
      for j := i + 1 to (i + N) div 2 do
        begin
            k := j * 2 - i;
            if (position[j] > position[i]) = (position[k] > position[j]) then exit;
        end;
    answer := true;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          if answer then writeln('yes') else writeln('no');
      end;
End.