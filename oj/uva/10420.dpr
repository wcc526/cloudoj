{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10420.in';
    OutFile    = 'p10420.out';
    Limit      = 5000;
    LimitLen   = 75;

Type
    Tstr       = string[LimitLen];
    Tdata      = array[1..Limit] of Tstr;

Var
    data       : Tdata;
    N          : longint;

procedure init;
var
    i , j      : longint;
    s          : Tstr;
begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(N);
      for i := 1 to N do
        begin
            readln(s);
            j := pos(' ' , s);
            s := copy(s , 1 , j - 1);
            data[i] := s;
        end;
//    Close(INPUT);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    tmp        : longint;
    key        : Tstr;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and (data[stop] > key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and (data[start] < key) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure out;
var
    i , p      : longint;
begin
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      data[N + 1] := '#';
      p := 1;
      for i := 1 to N + 1 do
        if data[i] <> data[p] then
          begin
              writeln(data[p] , ' ' , i - p);
              p := i;
          end;
//    Close(OUTPUT);
end;

Begin
    init;
    qk_sort(1 , N);
    out;
End.
