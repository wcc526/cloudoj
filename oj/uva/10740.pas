{$R-,Q-,S-}
Const
    InFile     = 'p10740.in';
    OutFile    = 'p10740.out';
    Limit      = 100;
    LimitK     = 11;

Type
    Theap      = object
                     tot , bound             : longint;
                     data                    : array[1..LimitK * 2 + 1] of longint;
                     procedure init(initBound : longint);
                     procedure process_Limit;
                     procedure add(num : longint);
                     function pop(del : boolean) : longint;
                     procedure down(p : longint);
                     procedure shift(p : longint);
                 end;
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tanswer    = array[1..Limit , 1..Limit] of
                   record
                       tot                   : longint;
                       data                  : array[1..LimitK] of longint;
                   end;
    Tshortest  = array[1..Limit] of Theap;

Var
    map        : Tmap;
    answer     : Tanswer;
    shortest   : Tshortest;
    x , y , K ,
    N , Q      : longint;

procedure Theap.init(initBound : longint);
begin
    fillchar(self , sizeof(self) , 0);
    bound := initBound;
end;

procedure Theap.process_Limit;
var
    tmp        : array[1..LimitK * 2 + 1] of longint;
    i          : longint;
begin
    if tot > Bound * 2 then
      begin
          i := 0;
          while tot > 0 do
            begin
                inc(i);
                tmp[i] := pop(true);
            end;
          tot := 0;
          for i := 1 to bound do add(tmp[i]);
      end;
end;

procedure Theap.add(num : longint);
begin
    inc(tot); data[tot] := num;
    shift(tot);
    process_Limit;
end;

function Theap.pop(del : boolean) : longint;
begin
    if tot = 0 then exit(-1);
    pop := data[1];
    if del then
      begin
          data[1] := data[tot];
          dec(tot); if tot <> 0 then down(1);
      end;
end;

procedure Theap.down(p : longint);
var
    key ,
    np         : longint;
begin
    key := data[p];
    while p * 2 <= tot do
      begin
          np := p;
          if data[p * 2] < key then np := p * 2;
          if (p * 2 < tot) and (data[p * 2 + 1] < key) and (data[p * 2 + 1] < data[p * 2]) then
            np := p * 2 + 1;
          data[p] := data[np];
          if p = np then break;
          p := np;
      end;
    data[p] := key;
end;

procedure Theap.shift(p : longint);
var
    key        : longint;
begin
    key := data[p];
    while p <> 1 do
      if key < data[p div 2] then
        begin
            data[p] := data[p div 2];
            p := p div 2;
        end
      else break;
    data[p] := key;
end;

function init : boolean;
var
    i , M ,
    p1 , p2    : longint;
begin
      fillchar(map , sizeof(map) , $FF);
      read(N , M);
      if N + M = 0 then exit(false);
      read(x , y , K);
      if x = y then inc(K);
      for i := 1 to M do
        begin
            read(p1 , p2);
            read(map[p1 , p2]);
        end;
      exit(true);
end;

procedure process(startp : longint);
var
    i , min ,
    minnum     : longint;
begin
    for i := 1 to N do
      shortest[i].init(LimitK);
    shortest[startp].add(0);
    while true do
      begin
          minnum := -1;
          for i := 1 to N do
            if shortest[i].pop(false) <> -1 then
              if (minnum = -1) or (shortest[i].pop(false) < minnum) then
                begin
                    minnum := shortest[i].pop(false);
                    min := i;
                end;

          if minnum = -1 then break;
          inc(answer[startp , min].tot);
          answer[startp , min].data[answer[startp , min].tot] := shortest[min].pop(true);
          dec(shortest[min].bound);
          shortest[min].process_Limit;

          for i := 1 to N do
            if map[min , i] >= 0 then
              shortest[i].add(map[min , i] + minnum);
      end;
end;

procedure work;
var
    i          : longint;
begin
    fillchar(answer[x] , sizeof(answer[x]) , 0);
    process(x);
end;

procedure out;
var
    i          : longint;
begin
          if answer[x , y].tot >= K
            then writeln(answer[x , y].data[K])
            else writeln(-1);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    while init do
      begin
          work;
          out;
      end;
End.
