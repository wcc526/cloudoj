{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10507.in';
    OutFile    = 'p10507.out';

Type
    Tmap       = array['A'..'Z' , 'A'..'Z'] of boolean;
    Tdata      = array[1..2 , 'A'..'Z'] of boolean;

Var
    map        : Tmap;
    data       : Tdata;
    N , M , now ,
    ans        : longint;

function init : boolean;
var
    c1 , c2 ,
    c3         : char;
    i          : longint;
begin
    if eof
      then init := false
      else begin
               init := true;
               fillchar(map , sizeof(map) , 0);
               fillchar(data , sizeof(data) , 0);
               readln(N , M);
               readln(c1 , c2 , c3);
               data[1 , c1] := true; data[1 , c2] := true; data[1 , c3] := true;
               for i := 1 to M do
                 begin
                     readln(c1 , c2);
                     map[c1 , c2] := true; map[c2 , c1] := true;
                 end;
           end;
end;

procedure work;
var
    changed    : boolean;
    good , num : longint;
    i , j      : char;
begin
    now := 1; ans := 0; changed := true; good := 3;
    while changed and (good < N) do
      begin
          now := 3 - now; inc(ans);
          fillchar(data[now] , sizeof(data[now]) , 0);
          changed := false;
          for i := 'A' to 'Z' do
            if not data[3 - now , i] then
              begin
                  num := 0;
                  for j := 'A' to 'Z' do
                    if map[i , j] and data[3 - now , j] then
                      inc(num);
                  if num >= 3 then
                    begin
                        data[now , i] := true;
                        changed := true;
                        inc(good);
                    end;
              end
            else
              data[now , i] := true;
      end;
    if good <> N then ans := -1;
end;

procedure out;
begin
    if ans <> -1
      then writeln('WAKE UP IN, ' , ans , ', YEARS')
      else writeln('THIS BRAIN NEVER WAKES UP');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
