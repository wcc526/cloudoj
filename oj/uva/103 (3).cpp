#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;

enum{MAX_DIMENSION=10, MAX_BOX=30};

class Box{
public:
    static int dimension;

    int no;
    int edge[MAX_DIMENSION];

    static int lexico_less(const Box& a, const Box& b);

    bool operator<(const Box &right);
    void sort_edges();
};

int Box::dimension;

int Box::lexico_less(const Box& a, const Box& b) {
    for(int i=0;i<dimension;++i)
        if( !(a.edge[i]<=b.edge[i]) )
            return false;
    return true;
}

void Box::sort_edges(){
    sort(edge, edge+dimension);
}

bool Box::operator<(const Box &rh){
    for(int i=0;i<dimension;++i)
        if( ! (edge[i]<rh.edge[i]) )
            return false;
    return true;
}

int main(){

#ifndef ONLINE_JUDGE
    freopen("103.in","r",stdin);
#endif

    int num_box;
    int dimension;
    Box box[MAX_BOX];

    /* for each case */
    while( scanf("%d %d",&num_box,&dimension)==2 ) {

        // input
        Box::dimension = dimension;
        for(int i=0;i<num_box;++i){

            for(int j=0;j<dimension;++j)
                scanf("%d",&box[i].edge[j]);

            box[i].no = i+1;
            box[i].sort_edges();
        }

        // sort box by lexicographic order
        sort(box, box+num_box, Box::lexico_less);
        //for(int i=0;i<num_box;++i) box[i].print();

        // LIS
        int length[MAX_BOX];
        int   prev[MAX_BOX];

        for(int i=0;i<num_box;++i) length[i] = 1;
        for(int i=0;i<num_box;++i) prev[i] = -1;

        for(int i=0;i<num_box;++i)
            for(int j=i+1;j<num_box;++j) {
                if( box[i]<box[j] ) {
                    if(length[i]+1 > length[j] ){
                        length[j] = length[i]+1;
                        prev[j] = i;
                    }
                }
            }

        int max = 0;
        for(int i=0;i<num_box;++i) {
            if( length[i] > length[max] ){
                max = i;
            }
        }

        //print the result
        printf("%d\n", length[max]);

        int output_seq[MAX_BOX];
        int size = 0;

        int cur = max;
        do {
            output_seq[size++] = box[cur].no;
            cur = prev[cur];
        } while( cur!= -1 );

        for(int i=size-1;i>0;--i)
            printf("%d ", output_seq[i]);
        printf("%d\n", output_seq[0]);
    }
    return 0;
}

