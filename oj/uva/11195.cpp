#include<cstdio>
char s[17];
int n,res,c[17],h;
void dfs(int row,int now,int lmt,int rmt)
{
	if(row==n)
	{
		res++;
		return;
	}
	int can=c[row];
	can&=h&~(now|lmt|rmt);
	while(can)
	{
		int nr=can&-can;
		can-=nr;
		dfs(row+1,now|nr,(lmt|nr)<<1,(rmt|nr)>>1);
	}
}
int main()
{
	int cas=0;
	while(scanf("%d",&n),n)
	{
		h=(1<<n)-1;
		for(int i=0;i<n;i++)
		{
			c[i]=h;
			scanf("%s",s);
			for(int j=0;j<n;j++)
			if(s[j]=='*')
			c[i]^=1<<j;
		}
		res=0;
		dfs(0,0,0,0);
		printf("Case %d: %d\n",++cas,res);
	}
}