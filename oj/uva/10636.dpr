{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10636.in';
    OutFile    = 'p10636.out';
    Limit      = 15000;
    minimum    = 1e-16;

Var
    x1 , y1 ,
    d          : comp;
    ab         : longint;
    alpha      : extended;

function init : boolean;
begin
    readln(x1 , y1 , d , ab , alpha);
    init := x1 >= 0;
end;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then gcd := b
      else gcd := gcd(b mod a , a);
end;

function check(a , b : longint) : boolean;
var
    maxx , maxy ,
    tmp , 
    x , y , i ,
    tx , ty    : longint;
    ta , tb ,
    tg         : extended;
begin
    if (x1 / a <= Limit) and (y1 / b <= Limit)
      then begin
               x := trunc(x1 / a); y := trunc(y1 / b);
               check := false;
               if gcd(x , y) <> 1 then exit;
               if y = 0
                 then begin maxx := 15000; maxy := 1; end
                 else begin
                          maxx := 0; maxy := 1;
                          for i := 1 to 15000 do
                            begin
                                ty := i; tx := (i * x + y - 1) div y - 1;
                                if tx > 15000 then break;
                                if maxx * ty < tx * maxy then
                                  begin
                                      maxx := tx; maxy := ty;
                                  end;
                            end;
                      end;
               ta := a; tb := b; ta := ta * a; tb := tb * b;
               tmp := gcd(maxx , maxy);
               maxx := maxx div tmp; maxy := maxy div tmp;
               tg := (maxy * x - maxx * y) / (ta * x * maxx + tb * y * maxy) * b * a;
               tg := arctan(tg) / pi * 180;
               if abs(tg - alpha) <= minimum
                 then begin
                          writeln(maxx * d * a : 0 : 0 , ' ' , maxy * d * b : 0 : 0);
                          check := true;
                      end;
           end
      else check := false;
end;

procedure work;
var
    i , a , b  : longint;
begin
    x1 := x1 / d; y1 := y1 / d;
    for i := 1 to round(sqrt(ab)) do
      if (ab mod i = 0) then
        begin
            a := i;
            b := ab div i;
            if (int(x1 / a) * a = x1) and (int(y1 / b) * b = y1) then
              if check(a , b) then exit;
            if (int(y1 / a) * a = y1) and (int(x1 / b) * b = x1) then
              if check(b , a) then exit;
        end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        work;
//    Close(OUTPUT);
//    Close(INPUT);
End.
