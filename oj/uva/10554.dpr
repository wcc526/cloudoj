{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10554.in';
    OutFile    = 'p10554.out';

Var
    fat , tot  : extended;

function read_in : boolean;
var
    s          : array[1..6] of string;
    ch         : char;
    i , code , j ,
    last , num ,
    first      : longint;
    firstsign  : boolean;
begin
    fillchar(s , sizeof(s) , 0);
    i := 1;
    while not eoln do
      begin
          read(ch);
          if ch = '-' then begin read_in := false; readln; exit; end;
          if ch = ' '
            then if s[i] <> '' then inc(i)
                               else
            else s[i] := s[i] + ch;
      end;
    readln;
    last := 0; num := 0;
    for i := 1 to 5 do
      begin
          ch := s[i][length(s[i])];
          delete(s[i] , length(s[i]) , 1);
          val(s[i] , j , code);
          if ch = '%'
            then inc(last , j)
            else if ch = 'g'
                   then case i of
                          1                  : inc(num , 9 * j);
                          2 , 3 , 4          : inc(num , 4 * j);
                          5                  : inc(num , 7 * j);
                        end
                   else inc(num , j);
          if i = 1 then
            if ch = '%'
              then begin
                       firstsign := false;
                       first := last;
                   end
              else begin
                       firstsign := true;
                       first := num;
                   end;
      end;
    tot := num / (1 - last / 100) + tot;
    if firstsign
      then fat := fat + first
      else fat := fat + num / (1 - last / 100) * first / 100;
    read_in := true;
end;

procedure main;
begin
    while true do
      begin
          fat := 0; tot := 0;
          while read_in do;
          if tot = 0 then break;
          writeln(fat / tot * 100 : 0 : 0 , '%');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      main;
//    Close(OUTPUT);
//    Close(INPUT);
End.
