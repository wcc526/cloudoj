Const
    Limit      = 1000;
    LimitLen   = 10000;

Type
    Tdata      = array[0..Limit , 0..Limit] of double;
    Tcount     = array[0..Limit , 0..Limit] of longint;
    Tvisited   = array[0..Limit , 0..Limit] of boolean;
    lint       = object
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure init;
                     procedure times(num : longint);
                     procedure divide(num : longint);
                     procedure minusone;
                     procedure print;
                 end;

Var
    data       : Tdata;
    visited    : Tvisited;
    p          : double;
    ans        : lint;
    M          : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    len := 1;
end;

procedure lint.times(num : longint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          tmp := data[i] * num + jw;
          jw := tmp div 10;
          data[i] := tmp mod 10;
          inc(i);
      end;
    len := i - 1;
end;

procedure lint.divide(num : longint);
var
    i , last   : longint;
begin
    last := 0;
    for i := len downto 1 do
      begin
          last := last * 10 + data[i];
          data[i] := last div num;
          last := last mod num;
      end;
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.minusone;
var
    i          : longint;
begin
    i := 1;
    while data[i] = 0 do inc(i);
    dec(data[i]);
    for i := i - 1 downto 1 do data[i] := 9;
    while (len > 1) and (data[len] = 0) do dec(len);
end;

procedure lint.print;
var
    i          : longint;
begin
    for i := len downto 1 do write(data[i]);
end;

function dfs(i , j : longint) : double;
begin
    if i = 0
      then exit(1)
      else if j = 0
             then exit(0)
             else if visited[i , j]
                    then exit(data[i , j])
                    else begin
                             data[i , j] := p * dfs(i - 1 , j) + (1 - p) * dfs(i , j - 1);
                             visited[i , j] := true;
                             exit(data[i , j]);
                         end;
end;

procedure main;
var
    p1 , p2 ,
    i , j      : longint;
begin
    repeat
    readln(p , M);
    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to M do
      begin
          readln(p1 , p2);
          if (p1 + p2 = 0)
            then begin writeln(-1.00000 : 0 : 5); end
            else writeln(dfs(p1 , p2) : 0 : 5);
          ans.init; ans.data[1] := 1;
          for j := p1 + p2 downto p1 + 1 do
            ans.times(j);
          for j := 2 to p2 do
            ans.divide(j);
          ans.minusone;
          ans.times(2);
          if (p1 = 0) xor (p2 = 0)
            then write(1)
            else ans.print;
          writeln;
      end;
    writeln;
    until M = 0;
end;

Begin
    main;
End.
