Const
    Limit      = 200;

Type
    Tstr       = string[Limit];
    Tposition  = array['a'..'z'] of longint;

Var
    s          : Tstr;
    answer     : longint;
    min , max  : Tposition;
    T          : longint;

procedure Get_Position;
var
    ch         : char;
    i          : longint;
begin
    for ch := 'a' to 'z' do
      begin
          min[ch] := Limit + 1;
          max[ch] := 0;
      end;
    for i := 1 to length(s) do
      begin
          if min[s[i]] > i then min[s[i]] := i;
          if max[s[i]] < i then max[s[i]] := i;
      end;
end;

procedure process;
var
    ch ,
    minch      : char;
    count      : longint;
begin
    answer := 0;
    while length(s) > 1 do
      begin
          Get_Position;
          count := 0; minch := #0;
          for ch := 'a' to 'z' do
            begin
                if min[ch] = max[ch] then begin inc(count); continue; end;
                if min[ch] <> 0 then
                  if (minch = #0) or (min[ch] - max[ch] < min[minch] - max[minch]) then
                    minch := ch;
            end;
          if count > 1 then begin writeln('Impossible'); exit; end;
          inc(answer , min[minch] - 1);
          inc(answer , length(s) - max[minch]);
          delete(s , min[minch] , 1);
          delete(s , max[minch] - 1 , 1);
      end;
    writeln(answer);
end;

Begin
    readln(T);
    while T > 0 do
      begin
          readln(s);
          process;
          dec(T);
      end;
End.