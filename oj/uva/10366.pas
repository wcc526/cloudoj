{$R-,Q-,S-,I-}
Const
    InFile     = 'p10366.in';
    Limit      = 1000;

Type
    Tdata      = array[0..Limit] of comp;
    Tkey       = record
                     p        : longint;
                     H        : comp;
                 end;
    Tqueue     = array[0..Limit] of Tkey;

Var
    data       : Tdata;
    Left , Right ,
    N          : longint;
    tot        : comp;

function init : boolean;
var
    p1 , p2 , i: longint;
begin
    read(p1 , p2);
    if p1 >= p2 then exit(false);
    N := (p2 - p1) div 2;
    for i := 0 to N do read(data[i]);
    Left := abs(p1) div 2; Right := Left + 1;
end;

function min(a , b : comp) : comp;
begin
    if a < b then min := a else min := b;
end;

procedure work;
var
    i , j ,
    tot1 , tot2: longint;
    sum1 , sum2: comp;
    queue1 ,
    queue2     : Tqueue;

  function chk_Left : boolean;
  begin
      if queue1[tot1].H <= data[Left] then exit(true);
      if queue1[tot1].H > data[Left] then exit(false);
  end;

  function chk_Right : boolean;
  begin
      if queue2[tot2].H <= data[Right] then exit(true);
      if queue2[tot2].H > data[Right] then exit(false);
  end;

begin
    while true do
      begin
          if data[Left] < data[Right] then tot := data[Left] else tot := data[Right];
          tot := tot * (Right - Left);
          i := 0; j := 0;
          if data[Left] <= data[Right] then
            begin
                tot1 := 0;
                queue1[0].p := 0; queue1[0].H := data[Left];
                repeat
                  if Left - i = 0 then break;
                  inc(i);
                  inc(tot1); queue1[tot1].p := i; queue1[tot1].H := data[Left - i];
                  while (tot1 > 0) and (queue1[tot1].H > queue1[tot1 - 1].H) do
                    begin queue1[tot1 - 1] := queue1[tot1]; dec(tot1); end;
                until tot1 = 0;
                sum1 := 0;
                for i := 1 to tot1 do
                  sum1 := sum1 + queue1[i].H * (queue1[i].p - queue1[i - 1].p);
            end;
          if data[Left] >= data[Right] then
            begin
                tot2 := 0;
                queue2[0].p := 0; queue2[0].H := data[Right];
                repeat
                  if Right + j = N then break;
                  inc(j);
                  inc(tot2); queue2[tot2].p := j; queue2[tot2].H := data[Right + j];
                  while (tot2 > 0) and (queue2[tot2].H > queue2[tot2 - 1].H) do
                    begin queue2[tot2 - 1] := queue2[tot2]; dec(tot2); end;
                until tot2 = 0;
                sum2 := 0;
                for j := 1 to tot2 do
                  sum2 := sum2 + queue2[j].H * (queue2[j].p - queue2[j - 1].p);
            end;

          if data[Left] = data[Right]
            then if chk_Left or chk_Right
                   then if chk_Left and chk_Right
                          then begin tot := tot + min(sum1 , sum2) * 2; break; end
                          else begin
                                   if not chk_Left
                                     then begin
                                              sum1 := sum2;
                                              sum2 := data[Left] * queue1[0].p;
                                          end
                                     else sum2 := data[Right] * queue2[0].p;
                                   tot := tot + min(sum1 , sum2) * 2 + sum1 - min(sum1 , sum2);
                                   break;
                               end
                   else begin
                            dec(Left , queue1[0].p); inc(Right , queue2[0].p);
                        end
            else if data[Left] < data[Right]
                   then if not chk_Left
                          then dec(Left , queue1[0].p)
                          else begin
                                   tot := tot + sum1; break;
                               end
                   else if not chk_Right
                          then inc(Right , queue2[0].p)
                          else begin
                                   tot := tot + sum2; break;
                               end;
      end;
end;

procedure print_num(num : comp);
begin
    if num <> 0 then
      begin
          print_num(int(num / 10));
          write(num - int(num / 10) * 10 : 0 : 0);
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        begin
            if N <= 0 then break;
            work;
            print_num(tot * 2);
            if tot = 0 then write(0);
            writeln;
        end;
//    Close(INPUT);
End.
