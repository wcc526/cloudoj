Const
    InFile     = 'p10782.in';
    OutFile    = 'p10782.out';
    Limit      = 6;

Type
    Tstr       = string[255];
    Tdata      = array[1..Limit] of Tstr;
    Tans       = array['A'..'Z'] of longint;
    Tused      = array[0..9] of char;

Var
    data       : Tdata;
    ans        : Tans;
    used       : Tused;
    cases ,
    totCase ,
    maxlen ,
    N          : longint;

procedure init;
var
    i          : longint;
    ch         : char;
begin
    inc(Cases);
    readln(N); maxlen := 0;
    for i := 1 to N do
      begin
          read(ch); while ch = ' ' do read(ch);
          data[i] := '';
          while ch <> ' ' do begin data[i] := ch + data[i]; if not eoln then read(ch) else ch := ' '; end;
          if length(data[i]) > maxlen then maxlen := length(data[i]);
      end;
    readln;
end;

function dfs(i , j , jw : longint) : boolean;
var
    beforeans ,
    start , stop ,
    k          : longint;
    before     : char;
begin
    if (i > maxlen) and (jw = 0) then exit(true);
    dfs := false;
    start := 0; stop := 9;
    if i > length(data[j])
      then begin start := 0; stop := 0; end
      else if ans[data[j , i]] <> -1 then
             begin start := ans[data[j , i]]; stop := ans[data[j , i]]; end;
    if (i = length(data[j])) and (j = N) and (start = 0) then start := 1;
    for k := start to stop do
      if (i > length(data[j])) or (used[k] = #0) or (used[k] = data[j , i]) then
        begin
            before := used[k]; if i <= length(data[j]) then beforeans := ans[data[j , i]];
            if (i <= length(data[j])) then used[k] := data[j , i];
            if (i <= length(data[j])) then ans[data[j , i]] := k;
            if j <> N
              then if dfs(i , j + 1 , k + jw)
                     then exit(true)
                     else
              else if k = jw mod 10
                     then if dfs(i + 1 , 1 , jw div 10)
                            then exit(true)
                            else
                     else;
            used[k] := before; if i <= length(data[j]) then ans[data[j , i]] := beforeans;
        end;
end;

procedure work;
begin
    fillchar(ans , sizeof(ans) , $FF);
    fillchar(used , sizeof(used) , 0);
    dfs(1 , 1 , 0);
end;

procedure out;
var
    ch         : char;
    first      : boolean;
begin
    first := true;
    for ch := 'A' to 'Z' do
      if ans[ch] <> -1 then
        begin
            if not first then write(' ') else first := false;
            write(ch , '=' , ans[ch]);
        end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase); cases := 0;
      while cases < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.