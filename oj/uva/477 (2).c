#include <math.h>
#include <stdio.h>

typedef struct {
  double x, y;
} pt;

typedef struct {
  char s;
  pt t, b;
} rt;

int main(void)
{
  rt r[10];
  pt p;
  int c = 0, f, i, n = 0;
  char t[2];

  while (scanf("%1s", t) == 1 && *t != '*') {
    r[n].s = *t;
    scanf("%lf %lf %lf", &r[n].t.x, &r[n].t.y, &r[n].b.x);
    if (*t == 'r')
      scanf("%lf", &r[n].b.y);
    ++n;
  }

  while (scanf("%lf %lf", &p.x, &p.y) == 2 &&
         !(p.x == 9999.9 && p.y == 9999.9)) {
    for (++c, f = i = 0; i < n; ++i) {
      switch (r[i].s) {
      case 'r':
        if (p.x > r[i].t.x && p.x < r[i].b.x &&
            p.y > r[i].b.y && p.y < r[i].t.y)
          printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
        break;
      case 'c':
        if (sqrt((p.x - r[i].t.x) * (p.x - r[i].t.x) +
                 (p.y - r[i].t.y) * (p.y - r[i].t.y)) < r[i].b.x)
          printf("Point %d is contained in figure %d\n", c, i + 1), ++f;
        break;
      }
    }
    if (f == 0)
      printf("Point %d is not contained in any figure\n", c);
  }

  return 0;
}
