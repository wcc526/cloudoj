#include <stdio.h>
#define MAX 249600
#define PRIME 22002
typedef long long LL;
LL f[PRIME];
bool vis[MAX];
void cal()
{
	int cnt=0;
	LL i,j,a=0,b=1,s;
	for(i=2;i<MAX && cnt<PRIME;i++)
	{
		s=a+b;
		a=b;
		b=s;
		if(s>=1e18)
		{
			s/=10;a/=10;b/=10;
		}
		if(!vis[i])
		{
			for(j=i*i;j<MAX;j+=i) vis[j]=true;
			f[++cnt]=b;
			while(f[cnt]>1e9) f[cnt]/=10;
		}
	}
}
int main()
{
	int n;
	cal();
	while(scanf("%d",&n)!=EOF)
	{
		if(n==1) printf("2\n");
		else if(n==2) printf("3\n");
		else printf("%lld\n",f[n]);
	}
	return 0;
}