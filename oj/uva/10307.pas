Const
    InFile     = 'p10307.in';
    Limit      = 50;
    LimitP     = 120;
    dirx       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    diry       : array[1..4] of longint = (0 , -1 , 0 , 1);

Type
    Tmap       = array[1..Limit , 1..Limit] of char;
    Tmark      = array[1..Limit , 1..Limit] of longint;
    Tcost      = array[1..LimitP , 1..LimitP] of longint;
    Tused      = array[1..LimitP] of boolean;
    Tminp      = array[1..LimitP] of
                   record
                       p , min               : longint;
                   end;

Var
    map        : Tmap;
    visited ,
    mark       : Tmark;
    cost       : Tcost;
    used       : Tused;
    minp       : Tminp;
    Cases , answer ,
    N , M , P  : longint;

procedure init;
var
    i , j      : longint;
begin
    dec(Cases);
    readln(M , N);
    fillchar(map , sizeof(map) , 32);
    for i := 1 to N do
      begin
          for j := 1 to M do
            begin
                if eoln then break;
                read(map[i , j]); if map[i , j] = 'S' then map[i , j] := 'A';
            end;
          readln;
      end;
end;

procedure bfs(x , y : longint);
type
    Tqueue     = array[1..Limit * Limit] of
                   record
                       x , y  : longint;
                   end;
var
    queue      : Tqueue;
    open , closed ,
    nx , ny , i ,
    start      : longint;
begin
    fillchar(visited , sizeof(visited) , $FF);
    queue[1].x := x; queue[1].y := y; visited[x , y] := 0;
    open := 1; closed := 1;
    start := mark[x , y];
    while open <= closed do
      begin
          x := queue[open].x; y := queue[open].y;
          if map[x , y] = 'A' then
            cost[start , mark[x , y]] := visited[x , y];
          for i := 1 to 4 do
            begin
                nx := x + dirx[i]; ny := y + diry[i];
                if map[nx , ny] <> '#' then
                  if visited[nx , ny] = -1 then
                    begin
                        visited[nx , ny] := visited[x , y] + 1;
                        inc(closed); queue[closed].x := nx; queue[closed].y := ny;
                    end;
            end;
          inc(open);
      end;
end;

procedure work;
var
    min ,
    i , j      : longint;
begin
    P := 0;
    for i := 1 to N do
      for j := 1 to M do
        if map[i , j] = 'A' then
          begin
              inc(P); mark[i , j] := P;
          end;
    for i := 1 to N do
      for j := 1 to M do
        if map[i , j] = 'A' then
         bfs(i , j);

    fillchar(used , sizeof(used) , 0); used[1] := true; answer := 0;
    for i := 2 to P do
      with minp[i] do
        begin p := 1; min := cost[i , 1]; end;
    answer := 0;
    for i := 2 to P do
      begin
          min := 0;
          for j := 1 to P do
            if not used[j] and ((min = 0) or (minp[j].min < minp[min].min)) then
              min := j;
          inc(answer , minp[min].min);
          used[min] := true;
          for j := 1 to P do
            if not used[j] then
              if cost[min , j] < minp[j].min then
                begin
                    minp[j].min := cost[min , j];
                    minp[j].p := min;
                end;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            writeln(answer);
        end;
//    Close(INPUT);
End.