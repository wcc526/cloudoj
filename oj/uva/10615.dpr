{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10615.in';
    OutFile    = 'p10615.out';
    Limit      = 100;

Type
    Tdata      = array[1..Limit , 1..Limit] of boolean;
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tmatch     = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;
    Tdegree    = array[1..Limit] of longint;

Var
    data ,
    tmpdata    : Tdata;
    map        : Tmap;
    matchx ,
    matchy     : Tmatch;
    visited    : Tvisited;
    degreex ,
    degreey    : Tdegree;
    Cases , N ,
    max , 
    answer     : longint;

procedure init;
var
    i , j      : longint;
    c          : char;
begin
    dec(Cases);
    fillchar(data , sizeof(data) , 0);
    fillchar(map , sizeof(map) , 0);
    readln(N);
    for i := 1 to N do
      begin
          for j := 1 to N do
            begin
                read(c);
                data[i , j] := c = '*';
            end;
          readln;
      end;
end;

function dfs_1(side , root : longint) : boolean;
var
    i          : longint;
begin
    if side = 1
      then begin
               dfs_1 := true;
               if degreex[root] < max
                 then begin matchx[root] := 0; exit; end;
               for i := 1 to N do
                 if tmpdata[root , i] and not visited[i]
                   then if dfs_1(2 , i)
                          then begin
                                   matchx[root] := i; matchy[i] := root;
                                   exit;
                               end;
               dfs_1 := false;
           end
      else begin
               visited[root] := true;
               dfs_1 := (matchy[root] = 0) or dfs_1(1 , matchy[root]);
           end;
end;

function dfs_2(side , root : longint) : boolean;
var
    i          : longint;
begin
    if side = 2
      then begin
               dfs_2 := true;
               if degreey[root] < max
                 then begin matchy[root] := 0; exit; end;
               for i := 1 to N do
                 if tmpdata[i , root] and not visited[i]
                   then if dfs_2(1 , i)
                          then begin
                                   matchy[root] := i; matchx[i] := root;
                                   exit;
                               end;
               dfs_2 := false;
           end
      else begin
               visited[root] := true;
               dfs_2 := (matchx[root] = 0) or dfs_2(2 , matchx[root]);
           end;
end;

procedure Match;
var
    i , j      : longint;
begin
    fillchar(matchx , sizeof(matchx) , 0);
    fillchar(matchy , sizeof(matchy) , 0);
    fillchar(degreex , sizeof(degreex) , 0);
    fillchar(degreey , sizeof(degreey) , 0);
    fillchar(tmpdata , sizeof(tmpdata) , 0);
    max := 0;
    for i := 1 to N do
      for j := 1 to N do
        if data[i , j] then
          begin
              inc(degreex[i]); inc(degreey[j]);
              tmpdata[i , j] := true;
              if (degreex[i] > max) or (degreey[j] > max)
                then begin
                         inc(max);
                         fillchar(matchx , sizeof(matchx) , 0); fillchar(matchy , sizeof(matchy) , 0);
                         matchx[i] := j; matchy[j] := i;
                     end
                else begin
                         if (degreex[i] = max) and (matchx[i] = 0)
                           then if matchy[j] = 0
                                  then begin matchx[i] := j; matchy[j] := i; end
                                  else begin
                                           fillchar(visited , sizeof(visited) , 0);
                                           dfs_1(1 , i);
                                       end;
                         if (degreey[j] = max) and (matchy[j] = 0)
                           then if matchx[i] = 0
                                  then begin matchx[i] := j; matchy[j] := i; end
                                  else begin
                                           fillchar(visited , sizeof(visited) , 0);
                                           dfs_2(2 , j);
                                       end;
                     end;
          end;
end;

procedure work;
var
    i , j      : longint;
    find       : boolean;
begin
    answer := 0;
    while true do
      begin
          find := false;
          for i := 1 to N do
            if not find then
              for j := 1 to N do
                if data[i , j] then
                  begin
                      find := true;
                      break;
                  end;
          if not find then break;

          inc(answer);
          Match;
          for i := 1 to N do
            if matchx[i] <> 0 then
              begin
                  data[i , matchx[i]] := false;
                  map[i , matchx[i]] := answer;
              end;
      end;
end;

procedure out;
var
    i , j      : longint;
begin
    writeln(answer);
    for i := 1 to N do
      for j := 1 to N do
        begin
            write(map[i , j]);
            if j = N then writeln else write(' ');
        end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
