{$I-}
Const
    InFile     = 'p10459.in';
    OutFile    = 'p10459.out';
    Limit      = 5000;

Type
    Tedge      = record
                     p1 , p2                 : longint;
                 end;
    Tdata      = array[1..Limit * 2] of Tedge;
    Tindex     = array[1..Limit] of longint;
    Topt       = array[1..Limit , 1..2] of
                   record
                       child , bestlen       : longint;
                   end;
    Tbestfa    = array[1..Limit] of longint;
    Tkey       = record
                     best , number           : longint;
                 end;
    Tanswer    = array[1..Limit] of Tkey;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    father ,
    index      : Tindex;
    opt        : Topt;
    bestfa     : Tbestfa;
    answer     : Tanswer;
    visited    : Tvisited;
    N , M      : longint;

procedure init;
var
    i , j , k  : longint;
begin
    fillchar(index , sizeof(index) , 0);
    read(N); M := 0;
    if N <= 0 then exit;
    for i := 1 to N do
      begin
          read(K);
          for j := 1 to K do
            begin
                inc(M); data[M].p1 := i; read(data[M].p2);
            end;
//          readln;
      end;
    for i := 1 to M do
      if index[data[i].p1] = 0
        then index[data[i].p1] := i;
end;

procedure dfs_opt(root : longint);
var
    i , j , tmp: longint;
begin
    visited[root] := true;
    opt[root , 1].child := 0; opt[root , 1].bestlen := 0;
    i := index[root];
    while (i <> 0) and (i <= M) and (data[i].p1 = root) do
      begin
          if not visited[data[i].p2] then
            begin
                father[data[i].p2] := root;
                dfs_opt(data[i].p2);
                tmp := opt[data[i].p2 , 1].bestlen + 1;
                if tmp > opt[root , 1].bestlen
                  then j := 1
                  else if tmp > opt[root , 2].bestlen
                         then j := 2
                         else j := 3;
                if j <> 3 then
                  begin
                      if j = 1 then opt[root , 2] := opt[root , 1];
                      opt[root , j].bestlen := tmp; opt[root , j].child := data[i].p2;
                  end;
            end;
          inc(i);
      end;
end;

procedure dfs_father(root : longint);
var
    i , tmp    : longint;
begin
    visited[root] := true;
    if father[root] <> 0 then
      begin
          bestfa[root] := bestfa[father[root]] + 1;
          if opt[father[root] , 1].child = root
            then tmp := opt[father[root] , 2].bestlen
            else tmp := opt[father[root] , 1].bestlen;
          if tmp + 1 > bestfa[root] then bestfa[root] := tmp + 1;
      end;

    i := index[root];
    while (i <> 0) and (i <= M) and (data[i].p1 = root) do
      begin
          if not visited[data[i].p2] then
            dfs_father(data[i].p2);
          inc(i);
      end;
end;

function compare(p1 , p2 : Tkey) : longint;
begin
    if p1.best <> p2.best
      then exit(p1.best - p2.best)
      else exit(p1.number - p2.number);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := answer[tmp]; answer[tmp] := answer[start];
    while start < stop do
      begin
          while (start < stop) and (compare(answer[stop] , key) > 0) do dec(stop);
          answer[start] := answer[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(answer[start] , key) < 0) do inc(start);
          answer[stop] := answer[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    answer[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i          : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(father , sizeof(father) , 0);
    fillchar(opt , sizeof(opt) , $FF);
    dfs_opt(1);
    fillchar(visited , sizeof(visited) , 0);
    fillchar(bestfa , sizeof(bestfa) , 0);
    dfs_father(1);

    for i := 1 to N do
      begin
          answer[i].number := i;
          if bestfa[i] > opt[i , 1].bestlen
            then answer[i].best := bestfa[i]
            else answer[i].best := opt[i , 1].bestlen;
      end;
    qk_sort(1 , N);
end;

procedure out;
var
    i , j , k  : longint;
begin
    i := 1;
    while (i < N) and (answer[i + 1].best = answer[1].best) do inc(i);
    j := N;
    while (j > 1) and (answer[j - 1].best = answer[N].best) do dec(j);
    write('Best Roots  :');
    for k := 1 to i do write(' ' , answer[k].number);
    writeln;
    write('Worst Roots :');
    for k := j to N do write(' ' , answer[k].number);
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            if N <= 0 then break;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
