#include <stdio.h>
#include <math.h>
long long gcd(long long m,long long n)
{
	long long r;
	r=m%n;
	while(r!=0)
	{
		m=n;n=r;r=m%n;
	}
	return n;
}
int main()
{
	long long p,q,s,i,g,t;
	while(scanf("%lld%lld",&p,&q) && p+q)
	{
        if(p==0)
		{
			printf("0 2\n");
			continue;
		}
		g=gcd(p,q);
		p/=g;
		q/=g;
		for(i=2;i<=50000;i++)
		{
			if(i*(i-1)%q==0)
			{
				t=i*(i-1)/q;
				s=(long long)sqrt((double)t*p);
				if(s*(s+1)==t*p)
				{
					printf("%lld %lld\n",s+1,i-s-1);
					break;
				}
			}
		}
		if(i>50000) printf("impossible\n");
	}
	return 0;
}