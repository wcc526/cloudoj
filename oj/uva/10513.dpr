{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10513.in';
    OutFile    = 'p10513.out';
    Limit      = 15;
    LimitSave  = 40000;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tvisited   = array[-Limit..Limit * 2] of boolean;
    Tpath      = array[1..Limit] of integer;
    Tans       = record
                     total    : longint;
                     data     : array[1..LimitSave] of Tpath;
                 end;
    Tindex     = array[1..Limit + 1] of longint;

Var
    map        : Tmap;
    visited ,
    d1 , d2    : Tvisited;
    path ,
    degree     : Tpath;
    ans        : Tans;
    index      : Tindex;
    N , answer ,
    nowCase    : longint;

procedure dfs(step : longint);
var
    i          : longint;
begin
    if step > N
      then begin
               inc(ans.total);
               ans.data[ans.total] := path;
           end
      else begin
               for i := 1 to N do
                 if not visited[i] and not d1[step - i] and not d2[step + i] then
                   begin
                       if (step > 1) and (path[step - 1] <> 0) and (abs(path[step - 1] - i) = 2) then continue;
                       if (step > 2) and (path[step - 2] <> 0) and (abs(path[step - 2] - i) = 1) then continue;
                       visited[i] := true; d1[step - i] := true; d2[step + i] := true;
                       path[step] := i;
                       dfs(step + 1);
                       visited[i] := false; d1[step - i] := false; d2[step + i] := false;
                       path[step] := 0;
                   end;
           end;
end;

procedure pre_process;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(d1 , sizeof(d1) , 0);
    fillchar(d2 , sizeof(d2) , 0);
    fillchar(path , sizeof(path) , 0);
    fillchar(ans , sizeof(ans) , 0);
    N := 1;
    while N <= 15 do
      begin
          index[N] := ans.total + 1;
          dfs(1);
          inc(N);
      end;
    index[16] := ans.total + 1;
end;

function init : boolean;
var
    i , j      : longint;
    c          : char;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(degree , sizeof(degree) , 0);
    readln(N);
    if N = 0 then begin init := false; exit; end;
    for i := 1 to N do
      begin
          while not eoln do
            begin
                read(c);
                if c = '?'
                  then for j := 1 to N do map[i , j] := true
                  else map[i , ord(c) - ord('A') + 1] := true;
            end;
          readln;
      end;
    for i := 1 to N do
      for j := 1 to N do
        if map[i , j] then
          inc(degree[i]);
    init := true;
end;

function check(path : Tpath) : boolean;
var
    i          : longint;
begin
    check := false;
    for i := 1 to N do
      if not map[i , path[i]] then
        exit;
    check := true;
end;

procedure work;
var
    i          : longint;
begin
    answer := 0;
    for i := index[N] to index[N + 1] - 1 do
      if check(ans.data[i]) then
        inc(answer);
end;

procedure print(num : extended);
var
    s          : string;
    i          : longint;
begin
    s := '';
    while num > 0.5 do
      begin
          i := trunc(num - int(num / 10) * 10);
          s := chr(i + ord('0')) + s;
          num := int(num / 10);
      end;
    writeln(s);
end;

procedure out;
var
    total      : extended;
    i          : longint;
begin
    total := 1;
    for i := 1 to N do
      total := total * degree[i];
    inc(nowCase);
    write('Case ' , nowCase , ': ');
    print(total - answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      pre_process;
      nowCase := 0;
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
