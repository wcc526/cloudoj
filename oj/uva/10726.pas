Const
    InFile     = 'p10726.in';
    OutFile    = 'p10726.out';
    minimum    = 1e-6;

Var
    tot , now  : longint;
    answer ,
    S , M , low ,
    high       : extended;

procedure init;
begin
    readln(S , M , low , high);
    inc(now);
end;

procedure euclid_extend(a , b , c : extended; var x , y : extended);
var
    divnum , t : extended;
begin
    if a = 0
      then begin x := 0; y := c / b; end
      else begin
               divnum := int(b / a);
               euclid_extend(b - a * divnum , a , c , y , t);
               x := t - divnum * y;
           end;
end;

procedure work;
var
    k , y , tmp ,
    delta ,
    modulo     : extended;
    i          : longint;
begin
    answer := 0;
    if S - 1 >= 11 then exit;
    modulo := 1; tmp := 1;
    for i := 1 to trunc(S) do begin modulo := modulo * (S - 1); tmp := tmp * S / (S - 1); end;
    delta := (S - 1) * M * (tmp - 1);
    low := (low - delta) / tmp; high := (high - delta) / tmp;
    low := int((low + S - minimum) / S); high := int(high / S);
    euclid_extend(s , modulo , modulo - (S - 1) * M , k , y);
    low := low - k; high := high - k;
    y := int(low / modulo) * modulo;
    low := low - y; high := high - y;
    if (low < 0) then begin low := low + modulo; high := high + modulo; end;
    low := int((low + modulo - minimum) / modulo); high := int(high / modulo);
    answer := high - low + 1;
end;

procedure out;
begin
    writeln('Case ' , now , ': ' , answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
    now := 0; readln(tot);
    while now < tot do
      begin
          init;
          work;
          out;
      end;
End.