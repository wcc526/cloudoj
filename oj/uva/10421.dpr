{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10421.in';
    OutFile    = 'p10421.out';
    Limit      = 1000;

Type
    Tpoint     = record
                     x , y    : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    N , answer : longint;

procedure init;
var
    i          : longint;
begin
    readln(N);
    for i := 1 to N do
      readln(data[i].x , data[i].y);
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and ((data[stop].y > key.y) or (data[stop].y = key.y) and (data[stop].x > key.x)) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and ((data[start].y < key.y) or (data[start].y = key.y) and (data[start].x < key.x)) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

function go(var p , y , st : longint) : boolean;
begin
    go := false;
    while (p <= N) and (data[p].y = y) do
      if data[p].x > st
        then begin st := data[p].x; go := true; exit; end
        else inc(p);
end;

procedure test(p1 , p2 , y1 , y2 : longint);
var
    count ,
    st         : longint;
begin
    count := 0; st := -maxlongint;
    while true do
      begin
          if go(p1 , y1 , st) then inc(count) else break;
          if go(p2 , y2 , st) then inc(count) else break;
      end;
    if count > answer then answer := count;
end;

procedure work;
var
    p1 , p2 ,
    y1 , y2    : longint;
begin
    qk_sort(1 , N);
    answer := 0;
    p1 := 1; p2 := 1;
    while p1 <= N do
      begin
          while (p2 <= N) and (data[p2].y < data[p1].y + 2) do inc(p2);
          y1 := data[p1].y; y2 := y1 + 2;
          test(p1 , p2 , y1 , y2);
          test(p2 , p1 , y2 , y1);
          while (p1 <= N) and (data[p1].y = y1) do inc(p1);
      end;
end;

procedure out;
begin
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
