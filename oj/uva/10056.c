#include <stdio.h>

int main()
{
  int i, n, t;
  double b, p, x, y;

  scanf("%d", &t);
  while (t--) {
    scanf("%d %lf %d", &n, &p, &i);
    if (p > 0 && p < 1) {
      for (x = 1, b = 1 - p, --i; i; i >>= 1, b *= b)
        if (i & 1)
          x *= b;
      for (y = 1, b = 1 - p; n; n >>= 1, b *= b)
        if (n & 1)
          y *= b;
      printf("%.4lf\n", p * x / (1 - y));
    } else if (p == 1 && i == 1) {
      puts("1.0000");
    } else {
      puts("0.0000");
    }
  }

  return 0;
}
