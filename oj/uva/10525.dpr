{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10525.in';
    OutFile    = 'p10525.out';
    Limit      = 50;

Type
    Tkey       = record
                     len , time              : longint;
                 end;
    Tmap       = array[1..Limit , 1..Limit] of Tkey;

Var
    map        : Tmap;
    N , Cases  : longint;

procedure init;
var
    M , i , 
    p1 , p2 ,
    len , time : longint;
begin
    dec(Cases);
    read(N , M);
    fillchar(map , sizeof(map) , 1);
    for i := 1 to M do
      begin
          read(p1 , p2);
          read(time , len);
          map[p1 , p2].len := len; map[p1 , p2].time := time;
          map[p2 , p1].len := len; map[p2 , p1].time := time;
      end;
    for i := 1 to N do begin map[i , i].len := 0; map[i , i].time := 0; end;
end;

procedure floyd;
var
    i , j , k , 
    p1 , p2    : longint;
begin
    for k := 1 to N do
      for i := 1 to N do
        for j := 1 to N do
          begin
              p1 := map[i , k].time + map[k , j].time;
              p2 := map[i , k].len + map[k , j].len;
              if (p1 < map[i , j].time) or (p1 = map[i , j].time) and (p2 < map[i , j].len) then
                begin map[i , j].time := p1; map[i , j].len := p2; end;
          end;
end;

procedure work;
begin
    floyd;
end;

procedure out;
var
    Q , p1 , p2
               : longint;
begin
    read(Q);
    while Q > 0 do
      begin
          read(p1 , p2);
          if map[p1 , p2].len > 1000000
            then writeln('No Path.')
            else writeln('Distance and time to reach destination is ' , map[p1 , p2].len , ' & ' , map[p1 , p2].time , '.');
          dec(Q);
      end;
    if Cases <> 0 then writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
