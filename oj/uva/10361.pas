Var
    ss         : array[1..5] of string;
    s          : string;
    ch         : char;
    N , i , p  : longint;

Begin
    readln(N);
    for i := 1 to N do
      begin
          for p := 1 to 5 do ss[p] := '';
          p := 1;
          while not eoln do
            begin
                read(ch);
                if (ch = '<') or (ch = '>')
                  then inc(p)
                  else ss[p] := ss[p] + ch;
            end;
          readln;
          readln(s);
          while s[length(s)] <> '.' do delete(s , length(s) , 1);
          delete(s , length(s) - 2 , 3);
          writeln(ss[1] , ss[2] , ss[3] , ss[4] , ss[5]);
          writeln(s , ss[4] , ss[3] , ss[2] , ss[5]);
      end;
End.