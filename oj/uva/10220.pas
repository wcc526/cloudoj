Const
    LimitLen   = 3000;
    Limit      = 1000;

Type
    lint       = object
                     start ,
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                     procedure times(num : longint);
                     function sumup : longint;
                 end;
    Tanswer    = array[0..Limit] of longint;

Var
    answer     : Tanswer;
    num        : lint;
    N          : longint;

procedure lint.times(num : longint);
var
    i , jw     : longint;
begin
    i := start; jw := 0;
    while (i <= len) or (jw <> 0) do
      begin
          data[i] := data[i] * num + jw;
          jw := data[i] div 10;
          data[i] := data[i] mod 10;
          inc(i);
      end;
    dec(i); len := i;
    while data[start] = 0 do inc(start);
end;

function lint.sumup : longint;
var
    i , res    : longint;
begin
    res := 0;
    for i := start to len do
      inc(res , data[i]);
    exit(res);
end;

Begin
    fillchar(num , sizeof(num) , 0); num.len := 1; num.data[1] := 1;
    num.start := 1;
    for N := 1 to Limit do
      begin
          num.times(N);
          answer[N] := num.sumup;
      end;
    while not eof do
      begin
          readln(N);
          writeln(answer[N]);
      end;
End.