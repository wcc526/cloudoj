#include <stdio.h>
#include <string.h>

int main(void)
{
  char o[301], t[301];
  short i, r[300], v[27], x = 0;

  for (;;) {
    scanf("%s", t);
    memset(v, 0, sizeof v);
    if (x)
      puts("");
    x = 0;
    while (t[x])
      ++v[t[x++] ^ 0x40];
    for (i = 1; ++i <= 26;)
      v[i] += v[i - 1];
    for (i = x; i--;)
      r[i] = --v[t[i] ^ 0x40];
    o[x] = 0;
    scanf("%hd", &i);
    if (t[0] == 'E' && t[1] == 'N' && t[2] == 'D' && i == 0)
      break;
    for (--i; x--;) {
      o[x] = t[i];
      i = r[i];
    }
    puts(o);
  }

  return 0;
}
