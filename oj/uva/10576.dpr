{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10576.in';
    OutFile    = 'p10576.out';

Type
    Tsubsum    = array[0..12] of comp;

Var
    subsum     : Tsubsum;
    answer     : comp;
    S , D      : longint;

procedure init;
begin
    readln(S , D);
end;

procedure work;
var
    i , j      : longint;
    ok         : boolean;
begin
    answer := 0;
    for i := 0 to 1 shl 12 - 1 do
      begin
          subsum[0] := 0;
          for j := 1 to 12 do
            if i and (1 shl (j - 1)) = 0
              then subsum[j] := subsum[j - 1] - D
              else subsum[j] := subsum[j - 1] + S;
          ok := true;
          for j := 5 to 12 do
            if subsum[j - 5] <= subsum[j] then
              begin
                  ok := false;
                  break;
              end;
          if ok then
            if subsum[12] > answer then
              answer := subsum[12];
      end;
end;

procedure out;
begin
    if answer = 0
      then writeln('Deficit')
      else writeln(answer : 0 : 0);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
