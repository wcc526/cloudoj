{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10643.in';
    OutFile    = 'p10643.ou3';
    Limit      = 500;
    LimitLen   = 200;
    Base       = 10000;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                 end;
    Tdata      = array[0..Limit] of lint;

Var
    answer ,
    data       : Tdata;
    totCase , M ,
    nowCase    : longint;

procedure times(const num1 , num2 : lint; var num3 : lint);
var
    i , j , jw ,
    tmp        : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := num1.data[i] * num2.data[j] + num3.data[i + j - 1] + jw;
                jw := tmp div Base;
                num3.data[i + j - 1] := tmp mod Base;
                inc(j);
            end;
          dec(j);
          if i + j - 1 > num3.len then num3.len := i + j - 1;
      end;
    while (num3.len > 1) and (num3.data[num3.len] = 0) do dec(num3.len);
end;

procedure add(var num1 : lint; const num2 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div Base;
          num1.data[i] := tmp mod Base;
          inc(i);
      end;
    num1.len := i - 1;
end;

procedure pre_process;
var
    i , j ,
    p1 , p2    : longint;
    tmp        : lint;
begin
    fillchar(data , sizeof(data) , 0);
    data[0].len := 1; data[0].data[1] := 1;
    for i := 2 to Limit do
      if not odd(i) then
        for p1 := 0 to i - 2 do
          begin
              p2 := i - 2 - p1;
              times(data[p1] , data[p2] , tmp);
              add(data[i] , tmp);
          end;

    fillchar(answer , sizeof(answer) , 0);
    answer[0].len := 1; answer[0].data[1] := 1;
    for i := 1 to Limit do
      for j := 1 to i do
        if odd(j) then
          begin
              times(answer[i - j] , data[j - 1] , tmp);
              add(answer[i] , tmp);
          end;
end;

procedure print(const num : lint);
var
    i          : longint;
    s          : string;
begin
    for i := num.len downto 1 do
      if i = num.len
        then write(num.data[i])
        else begin
                 str(num.data[i] , s);
                 while length(s) <> 4 do s := '0' + s;
                 write(s);
             end;
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            readln(M);
            inc(nowCase);
            write('Case ' , nowCase , ': ');
            print(answer[M]);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
