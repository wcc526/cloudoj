#include <cstdio>
#include <map>
#include <queue>

#define FSHIFT 20
#define DSHIFT 16
#define PSHIFT 0
#define DMASK  0x000F
#define PMASK  0xFFFF

#define BIT(n) (1 << n)
#define POSN(x, y) (x + (y << 2))
#define FACE(n) (BIT(n + FSHIFT))

#define ISFACE(n, s) ((s >> (n + FSHIFT)) & 1)
#define ISFLEA(x, y, s) ((s >> (POSN(x, y) + PSHIFT)) & 1)

#define GETX(s) ((s >> (DSHIFT + 0)) & 0x3)
#define GETY(s) ((s >> (DSHIFT + 2)) & 0x3)

#define MOVE(a, b, s) (((s >> a) & (1 << FSHIFT)) << b)
#define SWAP(x, y, s) (s ^ (BIT(POSN(x, y)) | BIT(FSHIFT)))

#define DEFMOVE(n, dx, dy, m, b, f0, f1)                 \
  inline int n(int s)                                    \
  {                                                      \
    int x = GETX(s) + dx;                                \
    int y = GETY(s) + dy;                                \
    int r = (s & PMASK) | (POSN(x, y) << DSHIFT);        \
    r |= MOVE(0, m, s) | MOVE(m, 2, s) | MOVE(2, b, s) | \
      MOVE(b, 0, s) | (FACE(f0) & s) | (FACE(f1) & s);   \
    if (ISFACE(0, r) ^ ISFLEA(x, y, r))                  \
      r = SWAP(x, y, r);                                 \
    return r;                                            \
  }

DEFMOVE(left,  -1,  0, 1, 3, 4, 5)
DEFMOVE(right,  1,  0, 3, 1, 4, 5)
DEFMOVE(down,   0, -1, 4, 5, 1, 3)
DEFMOVE(up,     0,  1, 5, 4, 1, 3)

using namespace std;

int main()
{
  int d, n, r, s, t, x, y;
  char l[8];
  map<int, short> m;
  queue<int> q;

  for (x = 4; x--;)
    for (y = 4; y--;)
      s = (0x3F << FSHIFT) | (POSN(x, y) << DSHIFT),
        q.push(s), m.insert(make_pair(s, 0));

  for (d = 0; !q.empty(); ++d) {
    for (n = q.size(); n--;) {
      t = q.front(), q.pop();
      x = GETX(t), y = GETY(t);
      if (x > 0 && m.find(r = left(t)) == m.end())
        q.push(r), m.insert(make_pair(r, d));
      if (y > 0 && m.find(r = down(t)) == m.end())
        q.push(r), m.insert(make_pair(r, d));
      if (x < 3 && m.find(r = right(t)) == m.end())
        q.push(r), m.insert(make_pair(r, d));
      if (y < 3 && m.find(r = up(t)) == m.end())
        q.push(r), m.insert(make_pair(r, d));
    }
  }

  scanf("%d", &t);
  while (t--) {
    for (s = 0, y = 4; y--;)
      for (scanf("%s", l), x = 4; x--;)
        if (l[x] == 'X')
          s ^= BIT(POSN(x, y));
        else if (l[x] == 'D')
          s |= POSN(x, y) << DSHIFT;
    if (m.find(s) == m.end())
      puts("impossible");
    else
      printf("%d\n", m[s]);
  }

  return 0;
}
