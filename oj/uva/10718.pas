Var
    now , addnum ,
    N , L , U  : qword;
    i          : longint;

Begin
    while not eof do
      begin
          readln(N , L , U);
          now := 0;
          for i := 31 downto 0 do
            begin
                addnum := qword(1) shl i;
                if addnum + now > U then continue;
                if addnum - 1 + now < L then begin now := now + addnum; continue; end;
                if addnum or N > N
                  then begin now := now + addnum; continue; end;
            end;
          writeln(now);
      end;
End.