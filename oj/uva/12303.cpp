#include <iostream>
#include <cassert>
#include <cmath>
#include <cstring>
#include <cstdio>
using namespace std;
const double eps = 1e-8;
const double pi = acos(-1.0);
int sgn(const double x){return fabs(x) < eps?0:x < -eps?-1:1;}
#define sqr(x) ((x) * (x))
#define REP(i,x) for(int i = 0;i < x;i ++)
#define maxn 50005
#define maxm maxn
struct matrix
{
	int r,c;
	double v[10][10];
	matrix(){}
	matrix(int a,int b):r(a),c(b)
	{
		REP(i,r) 
			fill(v[i],v[i]+c,0.0);
		if(r==c)
		{
			REP(i,r)//unit matrix as default matrix
				v[i][i]=1;
		}
	}
	void init()
	{
		REP(i,r) 
			fill(v[i],v[i]+c,0.0);
		if(r==c)
		{
			REP(i,r)//unit matrix as default matrix
				v[i][i]=1;
		}
	}
	matrix operator * (const matrix p)
	{
		matrix ans(r,p.c);
		REP(i,r)
			REP(j,ans.c)
			{
				double tmp = 0.0;
				REP(k,c)
					tmp += v[i][k] * p.v[k][j];
				ans.v[i][j] = tmp;
			}
		return ans;
	}
};
matrix power(matrix a,int n)
{
	matrix e(a.r,a.c);
	if(n == 0)return e;
	matrix ans = power(a,n/2);
	ans = ans * ans;
	return (n%2)?ans*a:ans;
}
struct Tpoint
{
	double x,y,z;
	Tpoint(){}
	Tpoint(double a,double b,double c):x(a),y(b),z(c){}
	Tpoint operator - (const Tpoint p){return Tpoint(x-p.x,y-p.y,z-p.z);}
	Tpoint operator + (const Tpoint p){return Tpoint(x+p.x,y+p.y,z+p.z);}
	double operator * (const Tpoint p){return x*p.x+y*p.y+z*p.z;}
	double norm2(){return (*this)*(*this);}
	double norm(){return sqrt(norm2());}
	Tpoint gao(matrix h)
	{
		matrix o(4,1);
		o.v[0][0] = x;o.v[1][0] = y;o.v[2][0] = z;o.v[3][0] = 1;
		o = h * o;
		return Tpoint(o.v[0][0],o.v[1][0],o.v[2][0]);
	}
	void scan(){scanf("%lf%lf%lf",&x,&y,&z);}
	void print(){printf("%.2lf %.2lf %.2lf\n",x,y,z);}
}pt[maxn];
Tpoint tZero(0,0,0);
struct Tplane
{
	double a,b,c,d;
	//a + b + c = d
	Tplane(){}
	Tplane(double e,double f,double g,double h):a(e),b(f),c(g),d(-h){}
	//return a perpendicular vector of the given plane
	Tpoint ndir(){return Tpoint(a,b,c);}
	//shifting transform
	double norm2(){return sqr(a)+sqr(b)+sqr(c);}
	double norm(){return sqrt(norm2());}
	Tplane unit(){return Tplane(a/norm(),b/norm(),c/norm(),-d/norm());}
	void print()
	{printf("%.2lf %.2lf %.2lf %.2lf\n",a,b,c,-d);}
}tp[maxm];
int n,m,t;
char opt[15];
int main()
{
	double a,b,c,d;
	scanf("%d %d %d",&n,&m,&t);
	REP(i,n)
	{
		scanf("%lf %lf %lf",&a,&b,&c);
		pt[i] = Tpoint(a,b,c);
	}
	REP(i,m)
	{
		scanf("%lf %lf %lf %lf",&a,&b,&c,&d);
		tp[i] = Tplane(a,b,c,d);
	}
	matrix handle(4,4),tran(4,4);
	REP(i,t)
	{
		scanf("%s",opt);
		if(opt[0] == 'T')
		{
			scanf("%lf %lf %lf",&a,&b,&c);
			tran.init();
			tran.v[0][3] = a;
			tran.v[1][3] = b;
			tran.v[2][3] = c;
			handle = tran * handle;
		}
		else if(opt[0] == 'R')
		{
			scanf("%lf %lf %lf %lf",&a,&b,&c,&d);
			tran.init();
			double t = d / 180.0 * pi;
			double ct = cos(t),st = sin(t);
			tran.v[0][0] = (1.0 - ct) * a * a + ct;
			tran.v[0][1] = (1.0 - ct) * a * b - st * c;
			tran.v[0][2] = (1.0 - ct) * a * c + st * b;
			tran.v[1][0] = (1.0 - ct) * b * a + st * c;
			tran.v[1][1] = (1.0 - ct) * b * b + ct;
			tran.v[1][2] = (1.0 - ct) * b * c - st * a;
			tran.v[2][0] = (1.0 - ct) * c * a - st * b;
			tran.v[2][1] = (1.0 - ct) * c * b + st * a;
			tran.v[2][2] = (1.0 - ct) * c * c + ct;
			handle = tran * handle;
		}
		else
		{
			scanf("%lf %lf %lf",&a,&b,&c);
			tran.init();
			tran.v[0][0] = a;
			tran.v[1][1] = b;
			tran.v[2][2] = c;
			handle = tran * handle;
		}
	}
	REP(i,n)
	{
		pt[i] = pt[i].gao(handle);
		pt[i].print();
	}
	REP(i,m)
	{
		a = tp[i].a,b = tp[i].b,c = tp[i].c,d = tp[i].d;
		Tpoint on(1,1,(d - a - b) / (sgn(c) == 0?1:c));
		Tpoint dir = tp[i].ndir();
		Tpoint out = on + dir;
		on = on.gao(handle);
		out = out.gao(handle);
		dir = out - on;
		tp[i] = Tplane(dir.x,dir.y,dir.z,-(dir * on));
		tp[i].unit().print();
	}
}
