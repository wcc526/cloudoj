{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10516.in';
    OutFile    = 'p10516.out';
    LimitN     = 32;
    LimitD     = 16;
    LimitP     = 1024;
    LimitLen   = 300;

Type
    lint       = record
                     len      : longint;
                     data     : array[1..LimitLen] of longint;
                 end;
    Tdata      = array[1..LimitN , 0..LimitD] of lint;
    Tok        = array[1..LimitN , 0..LimitD] of longint;
    Tpower     = array[0..LimitN] of lint;

Var
    data , sum : Tdata;
    power ,
    power2     : Tpower;
    ok         : Tok;
    N , D      : longint;

procedure add(num1 , num2 : lint; var num3 : lint);
var
    i , jw ,
    tmp        : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    i := 1; jw := 0;
    while (i <= num1.len) or (i <= num2.len) or (jw <> 0) do
      begin
          tmp := num1.data[i] + num2.data[i] + jw;
          jw := tmp div 10;
          num3.data[i] := tmp mod 10;
          inc(i);
      end;
    num3.len := i - 1;
end;

procedure times(const num1 , num2 : lint; var num3 : lint);
var
    i , j ,
    jw , tmp   : longint;
begin
    fillchar(num3 , sizeof(num3) , 0);
    for i := 1 to num1.len do
      begin
          jw := 0; j := 1;
          while (j <= num2.len) or (jw <> 0) do
            begin
                tmp := num1.data[i] * num2.data[j] + num3.data[i + j - 1] + jw;
                jw := tmp div 10;
                num3.data[i + j - 1] := tmp mod 10;
                inc(j);
            end;
          dec(j);
          if i + j - 1 > num3.len then num3.len := i + j - 1;
      end;
    while (num3.len > 1) and (num3.data[num3.len] = 0) do dec(num3.len);
end;

procedure pre_process;
var
    i , j , k  : longint;
    tmp , tmp2 : lint;
begin
    fillchar(data , sizeof(data) , 0);
    fillchar(ok , sizeof(ok) , 0);
    for i := 1 to LimitN do
      begin
          ok[i , 0] := 1;
          for j := 1 to LimitD do
            if ok[i , j - 1] <= LimitP
              then ok[i , j] := ok[i , j - 1] * i
              else ok[i , j] := LimitP + 1;
      end;
    for i := 1 to LimitN do
      begin
          data[i , 0].len := 1; data[i , 0].data[1] := 1;
          sum[i , 0] := data[i , 0];
          data[i , 1].len := 1; data[i , 1].data[1] := 1;
          add(sum[i , 0] , data[i , 1] , sum[i , 1]);
          for j := 2 to LimitD do
            if ok[i , j] <= LimitP then
              begin
                  fillchar(power , sizeof(power) , 0);
                  power[0].len := 1; power[0].data[1] := 1;
                  fillchar(power2 , sizeof(power2) , 0);
                  power2[0].len := 1; power2[0].data[1] := 1;
                  for k := 1 to i - 1 do
                    begin
                        times(power[k - 1] , sum[i , j - 2] , power[k]);
                        times(power2[k - 1] , sum[i , j - 1] , power2[k]);
                    end;
                  data[i , j].len := 1;
                  for k := 0 to i - 1 do
                    begin
                        times(power[k] , power2[i - 1 - k] , tmp);
                        times(tmp , data[i , j - 1] , tmp2);
                        add(data[i , j] , tmp2 , data[i , j]);
                    end;
                  add(data[i , j] , sum[i , j - 1] , sum[i , j]);
              end;
      end;
end;

procedure print(const num : lint);
var
    i          : longint;
begin
    for i := num.len downto 1 do
      write(num.data[i]);
    writeln;
end;

Begin
    pre_process;
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N , D);
      while (N <> 0) or (D <> 0) do
        begin
            write(N , ' ' , D , ' ');
            print(data[N , D]);
            readln(N , D);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
