#include <iostream>
#include <string>
#include <cstring>
using namespace std;

bool noRepeat(long number) {
    bool count[10];
    for (int i=0; i<10; ++i) count[i] = false;
    while (number > 0) {
        if (count[number%10]) return false;
        count[number%10] = true;
        number = number / 10;
    }
    return true;
}

int main()
{
    char ch;
    long s1, N, numCases;
    cin >> numCases;
    string line;
    getline(cin, line);
    for (int i=0; i<numCases; ++i) {
        cin >> N;
        long max = 9876543210/N;
        for (long j=1; j<=max; ++j) {
            if (noRepeat(j)) {
                s1 = N * j;
                if (noRepeat(s1)) {
                    cout << s1 << " / " << j << " = " << N << endl;
                }
            }
        }
        if (i != numCases-1) cout << endl;
    }

    return 0;
}
