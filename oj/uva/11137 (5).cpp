#include <cstdio>
#include <cstring>
typedef unsigned long long LL;
const int MXI=22;
const int MXN=10002;
LL d[MXI][MXN];
int main()
{
	//freopen("in.txt","r",stdin);
	memset(d,0,sizeof(d));
	d[0][0]=1;
	int i,j,k;
	for(i=1;i<MXI;++i)
		for(j=0;j<MXN;++j)
			for(k=0;j+k*i*i*i<MXN;++k)
				d[i][j+k*i*i*i]+=d[i-1][j];
	int n;
	while(~scanf("%d",&n))
	{
		printf("%llu\n",d[21][n]);
	}
	return 0;
}
