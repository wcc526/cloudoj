#include <stdio.h>

#define C(a,b) (a < b)
#define S(a,b) n = a, a = b, b = n

int kth_element(short *z, int n, int k)
{
  int a, b, u = 0, v = n - 1, m;

  for (;;) {
    m = u + ((v - u) >> 1);
    if (C(z[u], z[m])) {
      if (C(z[m], z[v]))
        S(z[m], z[v]);
      else if (C(z[v], z[u]))
        S(z[u], z[v]);
    } else {
      if (C(z[u], z[v]))
        S(z[u], z[v]);
      else if (C(z[v], z[m]))
        S(z[m], z[v]);
    }

    m = z[v], a = u, b = v - 1;
    do {
      for (; C(z[a], m); ++a);
      for (; C(m, z[b]); --b);
      if (a < b) {
        S(z[a], z[b]);
        ++a, --b;
      } else if (a == b) {
        ++a, --b;
        break;
      }
    } while (a <= b);

    z[v] = z[a], z[a] = m;

    if (k < a)
      v = a - 1;
    else if (k > a)
      u = a + 1;
    else
      return z[a];
  }
}

int g(void)
{
  int r;
  short c;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  while ((c = getc(stdin)) >= '0')
    r = r * 10 + c - '0';
  return r;
}

int abs(int x)
{
  return x < 0 ? -x : x;
}

int main()
{
  int n, s;
  short h[500], i, m, r;

  for (n = g(); n--;) {
    r = g();
    for (i = r; i--;)
      h[i] = g();
    m = kth_element(h, r, r >> 1);
    for (s = 0, i = r; i--;)
      s += abs(m - h[i]);
    printf("%d\n", s);
  }

  return 0;
}
