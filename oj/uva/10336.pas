Const
    InFile     = 'p10336.in';
    Limit      = 100;
    dirx       : array[1..4] of longint = (-1 , 1 , 0 , 0);
    diry       : array[1..4] of longint = (0 , 0 , -1 , 1);

Type
    Tdata      = array[1..Limit , 1..Limit] of char;
    Tvisited   = array[1..Limit , 1..Limit] of boolean;
    Tcount     = array['a'..'z'] of longint;

Var
    data       : Tdata;
    visited    : Tvisited;
    count      : Tcount;
    W , H , tot ,
    Cases      : longint;

procedure init;
var
    i , j      : longint;
begin
    inc(Cases);
    readln(H , W);
    for i := 1 to H do
      begin
          for j := 1 to W do
            read(data[i , j]);
          readln;
      end;
end;

procedure dfs(x , y : longint);
var
    i , nx , ny: longint;
begin
    visited[x , y] := true;
    for i := 1 to 4 do
      begin
          nx := x + dirx[i]; ny := y + diry[i];
          if (nx >= 1) and (ny >= 1) and (nx <= H) and (ny <= W) then
            if data[x , y] = data[nx , ny] then
              if not visited[nx , ny] then
                dfs(nx , ny);
      end;
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    fillchar(count , sizeof(count) , 0);
    for i := 1 to H do
      for j := 1 to W do
        if not visited[i , j] then
          begin
              inc(count[data[i , j]]);
              dfs(i , j);
          end;
end;

procedure out;
type
    Tused      = array['a'..'z'] of boolean;
var
    used       : Tused;
    max        : longint;
    ch         : char;
begin
    fillchar(used , sizeof(used) , 0);
    writeln('World #' , cases);
    while true do
      begin
          max := 0;
          for ch := 'a' to 'z' do
            if not used[ch] and (count[ch] > max) then
              max := count[ch];
          if max = 0 then break;
          for ch := 'a' to 'z' do
            if count[ch] = max then
              begin
                  used[ch] := true;
                  writeln(ch , ': ' , count[ch]);
              end;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(tot);
      cases := 0;
      while Cases < tot do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.