Var
    i , T ,
    A , B      : longint;
Begin
    readln(T);
    for i := 1 to T do
      begin
          read(a , b);
          if not odd(A) then inc(A);
          if not odd(B) then dec(B);
          if A <= B
            then writeln('Case ' , i , ': ' , (A + B) * ((B - A) div 2 + 1) div 2)
            else writeln('Case ' , i , ': ' , 0);
      end;
End.