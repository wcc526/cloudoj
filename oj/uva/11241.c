#include <math.h>
#include <stdio.h>

int main()
{
  double d, h, t, u, v;
  char x[2], y[2];

  while (scanf("%s", x) == 1 && *x ^ 'E') {
    scanf("%lf %s %lf", &u, y, &v);
    if (*x == 'T')
      t = u;
    else if (*x == 'D')
      d = u;
    else if (*x == 'H')
      h = u;
    if (*y == 'T')
      t = v;
    else if (*y == 'D')
      d = v;
    else if (*y == 'H')
      h = v;
    if (*x == 'T' && *y == 'D' || *x == 'D' && *y == 'T') {
      h = t + 0.5555 * (6.11 * exp(5417.7530 * (1.0 / 273.16 - 1.0 /
                                                (d + 273.16))) - 10.0);
    } else if (*x == 'H' && *y == 'D' || *x == 'D' && *y == 'H') {
      t = h - 0.5555 * (6.11 * exp(5417.7530 * (1.0 / 273.16 - 1.0 /
                                                (d + 273.16))) - 10.0);
    } else {
      d = 1.0 / (1.0 / 273.16 - log(((h - t) / 0.5555 + 10.0) / 6.11) / 5417.7530) - 273.16;
    }
    printf("T %.1lf D %.1lf H %.1lf\n", t, d, h);
  }

  return 0;
}
