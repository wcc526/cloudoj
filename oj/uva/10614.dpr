{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10614.in';
    OutFile    = 'p10614.out';
    LimitLen   = 100;

Type
    Tdata      = array[1..LimitLen] of char;
    Tvector    = record
                     x , y , z               : longint;
                 end;
    Tnumber    = record
                     signal                  : longint; { 1 - scalar; 2 - vector }
                     scalar                  : longint;
                     vector                  : Tvector;
                 end;

Var
    data       : Tdata;
    N          : longint;
    answer     : Tnumber;
    Bang       : boolean;

function init : boolean;
var
    last       : boolean;
begin
    Bang := true;
    N := 0;
    fillchar(data , sizeof(data) , 0);
    last := false;
    while not eoln and not eof do
      begin
          inc(N);
          read(data[N]);
          if not (data[N] in ['#' , ' ' , '+' , '-' , '*' , 'x' , '0'..'9' , '(' , ')' , '[' , ']' , ',']) then
            begin init := true; readln; exit; end;
          if last and (N > 1) and (data[N] in ['0'..'9']) and (data[N - 1] in ['0'..'9']) then
            begin init := true; readln; exit; end;
          if data[N] = ' '
            then begin dec(N); last := true; end
            else last := false;
      end;
    if data[1] = '#'
      then init := false
      else begin
               init := true;
               readln;
           end;
    Bang := false;
end;

procedure Get_Next_Ball(var p : longint; stop : longint);
var
    left       : longint;
begin
    left := 1; inc(p);
    while p <= stop do
      begin
          if data[p] = '('
            then inc(left)
            else if data[p] = ')'
                   then dec(left);
          if left = 0 then exit;
          inc(p);
      end;
    p := -1;
end;

procedure Get_Next_Square(var p : longint; stop : longint);
var
    left       : longint;
begin
    left := 1; inc(p);
    while p <= stop do
      begin
          if data[p] = '['
            then inc(left)
            else if data[p] = ']'
                   then dec(left);
          if left = 0 then exit;
          inc(p);
      end;
    p := -1;
end;

function Get_num(start , stop : longint; var num : longint) : boolean;
var
    i          : longint;
begin
    Get_Num := false;
    num := 0;
    for i := start to stop do
      if data[i] in ['0'..'9']
        then num := num * 10 + ord(data[i]) - ord('0')
        else exit;
    Get_Num := true;
end;

procedure times(n1 , n2 : Tnumber; var res : Tnumber);
begin
    if (n1.signal = 1) and (n2.signal = 1)
      then begin res.signal := 1; res.scalar := n1.scalar * n2.scalar; end
      else if (n1.signal = 2) and (n2.signal = 2)
             then begin res.signal := 1; res.scalar := n1.vector.x * n2.vector.x + n1.vector.y * n2.vector.y + n1.vector.z * n2.vector.z; end
             else begin
                      res.signal := 2;
                      if n1.signal = 1
                        then begin
                                 res.vector.x := n1.scalar * n2.vector.x;
                                 res.vector.y := n1.scalar * n2.vector.y;
                                 res.vector.z := n1.scalar * n2.vector.z;
                             end
                        else begin
                                 res.vector.x := n2.scalar * n1.vector.x;
                                 res.vector.y := n2.scalar * n1.vector.y;
                                 res.vector.z := n2.scalar * n1.vector.z;
                             end;
                  end;
end;

function calc(start , stop : longint; var res : Tnumber) : boolean;
var
    minc       : char;
    minwhere , i ,
    p1 , p2    : longint;
    n1 , n2 , n3
               : Tnumber;
begin
    fillchar(res , sizeof(res) , 0);
    calc := false;
    if start > stop then exit;
    i := start; minwhere := 0;
    while i <= stop do
      begin
          if data[i] in [']' , ')'] then exit;
          if data[i] = '('
            then Get_Next_Ball(i , stop)
            else if data[i] = '['
                   then Get_Next_Square(i , stop)
                   else if data[i] in ['+' , '-' , '*' , 'x']
                          then if (minwhere = 0) or (data[i] in ['+' , '-']) or
                                   (data[i] in ['*' , 'x']) and (minc in ['*' , 'x'])
                                 then begin
                                          minwhere := i;
                                          minc := data[i];
                                      end;
          if i = -1 then exit;
          inc(i);
      end;
    if minwhere = 0
      then begin
               if data[start] = '('
                 then begin
                          i := start;
                          Get_Next_ball(i , stop);
                          if i <> stop then exit;
                          calc := calc(start + 1 , stop - 1 , res);
                          exit;
                      end
                 else if data[start] = '['
                        then begin
                                 p1 := 0; p2 := 0;
                                 i := start + 1;
                                 while i < stop do
                                   begin
                                       if (data[i] = ')') or (data[i] = ']') then exit;
                                       if data[i] = '('
                                         then Get_Next_Ball(i , stop)
                                         else if data[i] = '['
                                                then Get_Next_Square(i , stop)
                                                else if data[i] = ','
                                                       then if p1 = 0
                                                              then p1 := i
                                                              else if p2 = 0
                                                                     then p2 := i
                                                                     else exit;
                                       inc(i);
                                   end;
                                 if (p1 = 0) or (p2 = 0) then exit;
                                 res.signal := 2;
                                 if not calc(start + 1 , p1 - 1 , n1) then exit;
                                 if not calc(p1 + 1 , p2 - 1 , n2) then exit;
                                 if not calc(p2 + 1 , stop - 1 , n3) then exit;
                                 if (n1.signal = 2) or (n2.signal = 2) or (n3.signal = 2) then exit;
                                 res.vector.x := n1.scalar; res.vector.y := n2.scalar; res.vector.z := n3.scalar;
                             end
                        else begin
                                 if not Get_num(start , stop , res.scalar) then exit;
                                 res.signal := 1;
                             end;
           end
      else begin
               if not calc(start , minwhere - 1 , n1) then exit;
               if not calc(minwhere + 1 , stop , n2) then exit;
               if minc in ['+' , '-']
                 then begin
                          if n1.signal <> n2.signal
                            then exit
                            else if n1.signal = 1
                                   then if minc = '+'
                                          then res.scalar := n1.scalar + n2.scalar
                                          else res.scalar := n1.scalar - n2.scalar
                                   else if minc = '+'
                                          then begin
                                                   res.vector.x := n1.vector.x + n2.vector.x;
                                                   res.vector.y := n1.vector.y + n2.vector.y;
                                                   res.vector.z := n1.vector.z + n2.vector.z;
                                               end
                                          else begin
                                                   res.vector.x := n1.vector.x - n2.vector.x;
                                                   res.vector.y := n1.vector.y - n2.vector.y;
                                                   res.vector.z := n1.vector.z - n2.vector.z;
                                               end;
                          res.signal := n1.signal;
                      end
                 else if minc = '*'
                        then times(n1 , n2 , res)
                        else if (n1.signal <> 2) or (n2.signal <> 2)
                               then exit
                               else begin
                                        res.signal := 2;
                                        res.vector.x := n1.vector.y * n2.vector.z - n1.vector.z * n2.vector.y;
                                        res.vector.y := n1.vector.z * n2.vector.x - n1.vector.x * n2.vector.z;
                                        res.vector.z := n1.vector.x * n2.vector.y - n1.vector.y * n2.vector.x;
                                    end;
           end;
    calc := true;
end;

procedure work;
begin
    Bang := Bang or not calc(1 , N , answer);
end;

procedure out;
begin
    if Bang
      then writeln('Bang!')
      else if answer.signal = 1
             then writeln(answer.scalar)
             else writeln('[' , answer.vector.x , ',' , answer.vector.y , ',' , answer.vector.z , ']'); 
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
