{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10619.in';
    OutFile    = 'p10619.out';
    Limit      = 100000;
    maximum    = 1000000;

Type
    Tpoint     = record
                     x , t , key1 , key2 ,
                     index1 , index2         : longint;
                 end;
    Tdata      = array[1..Limit] of Tpoint;
    Tsorted    = array[1..Limit] of longint;
    Tcompare   = function(p1 , p2 : longint) : longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    data       : Tdata;
    sorted1 ,
    sorted2    : Tsorted;
    visited    : Tvisited;
    totCase ,
    nowCase ,
    answer ,
    N , M      : longint;

procedure init;
var
    i          : longint;
begin
    inc(nowCase);
    read(N , M);
    for i := 1 to N do
      with data[i] do
        begin
            read(t , x);
            key1 := t + x;
            key2 := x - t;
            sorted1[i] := i; sorted2[i] := i;
        end;
end;

function compare1(p1 , p2 : longint) : longint;
begin
    compare1 := data[p1].key1 - data[p2].key1;
end;

function compare2(p1 , p2 : longint) : longint;
begin
    compare2 := data[p1].key2 - data[p2].key2;
end;

procedure qk_pass(var sorted : Tsorted; start , stop : longint; var mid : longint; compare : Tcompare);
var
    key , tmp  : longint;        
begin
    tmp := random(stop - start + 1) + start;
    key := sorted[tmp]; sorted[tmp] := sorted[start];
    while start < stop do
      begin
          while (start < stop) and (compare(sorted[stop] , key) > 0) do dec(stop);
          sorted[start] := sorted[stop];
          if start < stop then inc(start);
          while (start < stop) and (compare(sorted[start] , key) < 0) do inc(start);
          sorted[stop] := sorted[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    sorted[start] := key;
end;

procedure qk_sort(var sorted : Tsorted; start , stop : longint; compare : Tcompare);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(sorted , start , stop , mid , compare);
          qk_sort(sorted , start , mid - 1 , compare);
          qk_sort(sorted , mid + 1 , stop , compare);
      end;
end;

function check(p : longint) : boolean;
var
    count , t ,  
    p1 , p2    : longint;
begin
    fillchar(visited , sizeof(visited) , 0);
    p1 := 1; p2 := 1; count := 0; check := false;
    while (p1 <= N) and (p2 <= N) do
      begin
          if count >= M then exit;
          t := data[sorted1[p1]].key1 - p * 2;
          while (p2 <= N) and (data[sorted2[p2]].key2 <= t) do
            begin
                visited[data[sorted2[p2]].index1] := true;
                inc(p2);
            end;
          while (p1 <= N) and visited[p1] do inc(p1);
          inc(count);
      end;
    check := true;
end;

procedure work;
var
    i , mid , 
    start , stop
               : longint;
begin
    qk_sort(sorted1 , 1 , N , compare1);
    qk_sort(sorted2 , 1 , N , compare2);
    stop := maximum;
    for i := 1 to N do
      begin
          data[sorted1[i]].index1 := i; data[sorted2[i]].index2 := i;
          if data[i].t < stop then stop := data[i].t;
      end;

    start := -maximum * 2; answer := 0;
    while start <= stop do
      begin
          mid := (start + stop) div 2;
          if check(mid)
            then begin
                     start := mid + 1;
                     answer := mid;
                 end
            else stop := mid - 1;
      end;
end;

procedure out;
begin
    writeln('Case ' , nowCase , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
