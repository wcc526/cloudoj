Const
    minimum    = 1e-8;
    Limit      = 100;

Type
    Ttri       = record
                     A , B , C               : extended;
                 end;

Var
    tri        : Ttri;
    open , closed ,
    totCase ,
    cases      : longint;

function zero(num : extended) : boolean;
begin
    exit(abs(num) <= minimum);
end;

function same(tri1 , tri2 : Ttri) : boolean;
begin
    exit(zero(tri1.B - tri2.B) and zero(tri1.C - tri2.C));
end;

procedure swap(var a , b : extended);
var
    tmp        : extended;
begin
    tmp := a; a := b; b := tmp;
end;

procedure maintain(var tri : Ttri);
begin
    if tri.A > tri.B then swap(tri.A , tri.B);
    if tri.B > tri.C then swap(tri.B , tri.C);
    if tri.A > tri.B then swap(tri.A , tri.B);
    tri.B := tri.B / tri.A; tri.C := tri.C / tri.A; tri.A := 1;
end;

procedure split(T : Ttri; var T1 , T2 : Ttri);
var
    cosA       : extended;
begin
    T1.A := T.A; T2.A := T.B;
    T1.B := T.C / 2; T2.B := T.C / 2;
    cosA := (sqr(T.A) + sqr(T.C) - sqr(T.B)) / 2 / T.A / T.C;
    T1.C := sqrt(abs(sqr(T1.A) + sqr(T1.B) - 2 * cosA * T1.A * T1.B));
    T2.C := T1.C;
    maintain(T1); maintain(T2);
end;

procedure bfs;
type
    Tqueue     = array[1..Limit * 2] of Ttri;
var
    queue      : Tqueue;
    tri1 , tri2
               : Ttri;

  procedure add(const tri : Ttri);
  var
      i        : longint;
  begin
      for i := 1 to closed do
        if same(tri , queue[i]) then exit;
      inc(closed); queue[closed] := tri;
  end;

begin
    open := 1; closed := 1; queue[1] := tri;
    while open <= closed do
      begin
          split(queue[open] , tri1 , tri2);
          add(tri1); add(tri2);
          inc(open);
      end;
end;

Begin
    readln(totCase); Cases := 0;
    while Cases < totCase do
      begin
          inc(cases);
          read(tri.A , tri.B , tri.C);
          maintain(tri);
          bfs;
          writeln('Triangle ' , cases , ': ' , closed);
      end;
End.