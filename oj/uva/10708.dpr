{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10708.in';
    OutFile    = 'p10708.out';
    minimum    = 1e-6;

Var
    cases      : longint;
    Cx , Cy ,
    Px , Py ,
    P , U ,
    V , L      : extended;

function Get_Angle(x , y : extended) : extended;
begin
    if y < -minimum
      then Get_Angle := pi * 2 - get_angle(x , -y)
      else if abs(x) <= minimum
             then Get_Angle := pi / 2
             else if x > 0
                    then Get_Angle := arctan(y / x)
                    else Get_Angle := pi - arctan(-y / x)
end;

procedure process;
var
    bestT ,
    x0 , y0 ,
    x1 , x2 ,
    x , y ,   
    A , B , C ,
    st1 , ed1 ,
    st2 , ed2 ,
    minT , angle ,
    tmp ,   
    delta      : extended;
    valid      : longint;
begin
    x0 := Px - Cx; y0 := Py - Cy;
    A := V * V - U * U;
    B := -2 * (x0 * cos(P / 180 * pi) + y0 * sin(P / 180 * pi)) * U;
    C := -sqr(x0) - sqr(y0);
    if A = 0
      then begin
               valid := 1;
               if abs(B) <= minimum
                 then if C = 0
                        then begin st1 := 0; ed1 := 1e10; end
                        else begin writeln('sorry, buddy'); exit; end
                 else if B < 0
                         then begin st1 := -1e10; ed1 := -C / B; end
                         else begin st1 := -C / B; ed1 := 1e10; end;
           end
      else begin
               delta := B * B - 4 * A * C;
               valid := 1;
               if delta < 0
                 then if A < 0
                        then begin writeln('sorry, buddy'); exit; end
                        else begin st1 := -1e10; ed1 := 1e10; end
                 else begin
                          delta := sqrt(delta);
                          x1 := (-B - delta) / 2 / A; x2 := (-B + delta) / 2 / A;
                          if x1 > x2 then
                            begin tmp := x1; x1 := x2; x2 := tmp; end;
                          if A < 0
                            then begin st1 := x1; ed1 := x2; end
                            else begin
                                     valid := 2;
                                     st1 := -1e10; ed1 := x1;
                                     st2 := x2; ed2 := 1e10;
                                 end;
                      end;
           end;
    while true do
      if valid = 1
        then begin
                 if ed1 < 0
                   then begin writeln('sorry, buddy'); exit; end
                   else begin if st1 < 0 then st1 := 0; end;
                 break;
             end
        else begin
                 if ed1 < 0
                   then begin st1 := st2; ed1 := ed2; valid := 1; continue; end
                   else begin if st1 < 0 then st1 := 0; end;
                 break;
             end;
{    if U = 0
      then minT := 0
      else minT := -(x0 * cos(P / 180 * pi) + y0 * sin(P / 180 * p)) / U;
    if (st1 <= minT) and (minT <= ed1) or (valid = 2) and (st2 <= minT) and (minT <= ed2)
      then bestT := minT
      else if minT <= st1
             then bestT := st1
             else if valid = 1
                    then bestT := ed1
                    else if minT <= st2
                           then if minT - ed1 < st2 - minT
                                  then bestT := ed1
                                  else bestT := st2
                           else bestT := ed2;      }
    bestT := st1;
    x := Px + bestT * U * cos(P / 180 * pi);
    y := Py + bestT * U * sin(P / 180 * pi);
    if sqrt(sqr(x - Cx) + sqr(y - Cy)) > L * V
      then writeln('sorry, buddy')
      else begin
               angle := Get_Angle(x - Cx , y - Cy);
               writeln(angle / pi * 180 : 0 : 2 , ' ' , bestT : 0 : 2 , ' ' , x : 0 : 2 , ' ' , y : 0 : 2);
           end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            dec(Cases);
            readln(Cx , Cy , Px , Py , P , U , V , L);
            process;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
