#include <algorithm>
#include <cstdio>
#include <iterator>
#include <vector>

using namespace std;

int main(void)
{
  vector<int> a, b;
  vector<int>::iterator i, j;
  int t, u, v, w;
  char c;

  for (;;) {
    a.clear();
    b.clear();

    do {
      if (scanf("%d", &t) < 1)
        return 0;
      a.push_back(t);
    } while (scanf("%c", &c) == 1 && c == ' ');
    do {
      scanf("%d", &t);
      b.push_back(t);
    } while (scanf("%c", &c) == 1 && c == ' ');
    sort(a.begin(), a.end());
    sort(b.begin(), b.end());

    i = a.begin();
    j = b.begin();

    u = v = w = 0;

    while (i != a.end() && j != b.end()) {
      if (*i < *j)
        do
          ++i, ++u;
        while (i != a.end() && *i < *j);
      else if (*j < *i)
        do
          ++j, ++v;
        while (j != b.end() && *j < *i);
      else
        ++i, ++j, ++w;
    }
    while (i != a.end())
      ++i, ++u;
    while (j != b.end())
      ++j, ++v;
    if (u > 0 && v > 0)
      if (w == 0)
        puts("A and B are disjoint");
      else
        puts("I'm confused!");
    else if (u > 0 && v == 0)
      puts("B is a proper subset of A");
    else if (v > 0 && u == 0)
      puts("A is a proper subset of B");
    else
      puts("A equals B");
  }

  return 0;
}
