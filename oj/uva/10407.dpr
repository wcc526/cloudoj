{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
Var
    answer , first ,
    p          : longint;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then gcd := b
      else gcd := gcd(b mod a , a);
end;

Begin
    read(first);
    while first <> 0 do
      begin
          answer := 0;
          read(p);
          while p <> 0 do
            begin
                answer := gcd(answer , abs(first - p));
                read(p);
            end;
          writeln(answer);
          read(first);
      end;
End.
