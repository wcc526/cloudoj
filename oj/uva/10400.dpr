{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10400.in';
    OutFile    = 'p10400.out';
    Limit      = 100;
    Bound      = 32000;

Type
    Tdata      = array[0..Limit] of longint;
    Topt       = array[0..Limit , -Bound..Bound] of
                   record
                       ok                    : boolean;
                       strategy              : byte;
                       father                : smallint;
                                                   { 1 : +
                                                     2 : -
                                                     3 : *
                                                     4 : / }
                   end;

Var
    data       : Tdata;
    opt        : Topt;
    N , Cases ,
    target     : longint;

procedure init;
var
    i          : longint;
begin
    dec(Cases);
    read(N);
    for i := 0 to N - 1 do read(data[i]);
    readln(target); 
end;

procedure work;
var
    i , j , k  : longint;
begin
    fillchar(opt , sizeof(opt) , 0);
    opt[0 , data[0]].ok := true;
    for i := 0 to N - 2 do
      for j := -Bound + 1 to Bound - 1 do
        if opt[i , j].ok then
          begin
              k := j + data[i + 1];
              if abs(k) < Bound then
                begin
                    opt[i + 1 , k].ok := true;
                    opt[i + 1 , k].father := j;
                    opt[i + 1 , k].strategy := 1;
                end;
              k := j - data[i + 1];
              if abs(k) < Bound then
                begin
                    opt[i + 1 , k].ok := true;
                    opt[i + 1 , k].father := j;
                    opt[i + 1 , k].strategy := 2;
                end;
              k := j * data[i + 1];
              if abs(k) < Bound then
                begin
                    opt[i + 1 , k].ok := true;
                    opt[i + 1 , k].father := j;
                    opt[i + 1 , k].strategy := 3;
                end;
              k := j div data[i + 1];
              if (abs(k) < Bound) and (k * data[i + 1] = j) then
                begin
                    opt[i + 1 , k].ok := true;
                    opt[i + 1 , k].father := j;
                    opt[i + 1 , k].strategy := 4;
                end;
          end;
end;

procedure out;
var
    path       : Tdata;
    s          : string;
    i , j      : longint;          
begin
    if opt[N - 1 , target].ok
      then begin
               j := target;
               for i := N - 1 downto 1 do
                 begin
                     path[i] := opt[i , j].strategy;
                     j := opt[i , j].father;
                 end;
               write(data[0]);
               s := '+-*/';
               for i := 1 to N - 1 do
//                 write('+-*/'[path[i]] , data[i]);
                   write(s[path[i]] , data[i]);
               writeln('=' , target);
           end
      else writeln('NO EXPRESSION');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
