Const
    InFile     = 'p10335.in';
    Limit      = 10;
    Minimum    = 1e-6;
    Minimum1   = 5e-3;

Type
    TLine      = record
                     A , B , C: double;
                 end;
    Tpoint     = record
                     x , y    : double;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    startp ,
    v          : Tpoint;
    data       : Tdata;
    N , M      : longint;

function zero(p : double) : boolean;
begin
    exit(abs(p) <= minimum);
end;

function zero1(p : double) : boolean;
begin
    exit(abs(p) <= minimum1);
end;

function init : boolean;
var
    i          : longint;
    angle      : double;
begin
    readln(N , M);
    if N = 0 then exit(false);
    init := true;
    read(startp.x , startp.y , angle);
    angle := angle / 180 * pi;
    v.x := cos(angle); v.y := sin(angle);
    for i := 1 to N do
      read(data[i].x , data[i].y);
    readln;
end;

procedure Get_Line(p1 , p2 : Tpoint; var L : TLine);
begin
    L.A := p2.y - p1.y; L.B := p1.x - p2.x;
    L.C := -p1.x * p2.y + p2.x * p1.y;
end;

function Get_Crossing(L1 , L2 : TLine; var p : Tpoint) : boolean;
var
    tmp        : double;
begin
    tmp := L1.A * L2.B - L1.B * L2.A;
    if zero(tmp) then exit(false);
    Get_Crossing := true;
    p.x := - (L1.C * L2.B - L1.B * L2.C) / tmp;
    p.y := - (L1.A * L2.C - L1.C * L2.A) / tmp;
end;

function dist(p1 , p2 : Tpoint) : double;
begin
    exit(sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)));
end;

function onLine(p1 , p2 , p : Tpoint) : boolean;
begin
    exit(zero(dist(p1 , p2) - dist(p1 , p) - dist(p , p2)));
end;

procedure workout;
var
    i , j      : longint;
    rotateV ,
    v1 , p     : Tpoint;
    L1 , L2    : TLine;
begin
    i := 1;
    while i <= M + 1 do
      begin
          j := 1;
          while j <= N do
            begin
                Get_Line(data[j] , data[j mod N + 1] , L1);
                L2.A := v.y; L2.B := -v.x;
                L2.C := v.x * startp.y - v.y * startp.x;
                if Get_Crossing(L1 , L2 , p) then
                  if not zero(p.x - startp.x) or not zero(p.y - startp.y) then
                    begin
                        if zero1(p.x - data[j].x) and zero1(p.y - data[j].y) then
                          begin writeln('lost forever...'); exit; end;
                        if zero1(p.x - data[j mod N + 1].x) and zero1(p.y - data[j mod N + 1].y) then
                          begin writeln('lost forever...'); exit; end;
                        if online(data[j] , data[j mod N + 1] , p) then
                          if zero((p.x - startp.x) / dist(p , startp) - v.x) then
                            if zero((p.y - startp.y) / dist(p , startp) - v.y) then
                              break;
                    end;
                inc(j);
            end;
          if j > N then
            begin
                dec(i);
                j := 1;
                while not onLine(data[j] , data[j mod N + 1] , startp) do inc(j);
                p := startp;
            end;
          v1.x := (data[j mod N + 1].x - data[j].x) / dist(data[j mod N + 1] , data[j]);
          v1.y := (data[j mod N + 1].y - data[j].y) / dist(data[j mod N + 1] , data[j]);
          rotateV.x := v1.x * v.x + v1.y * v.y;
          rotateV.y := v1.y * v.x - v1.x * v.y;
          v.x := rotateV.x * v1.x - rotateV.y * v1.y;
          v.y := rotateV.x * v1.y + rotateV.y * v1.x;
          startp := p;
          inc(i);
      end;
    writeln(startp.x + minimum : 0 : 2 , ' ' , startp.y + minimum : 0 : 2);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while init do
        workout;
//    Close(INPUT);
End.