#include <cstdio>
#define maxn 20000
int sum[maxn];
bool flag[maxn];
int pim[maxn],tol,ans[maxn];
void init()
{
    for(int i=2;i<maxn;i++){
        if(!flag[i])  pim[tol++]=i;
        for(int j=0;j<tol&&i*pim[j]<maxn;j++){
            flag[i*pim[j]]=1;
            if(i%pim[j]==0) break;
        }
    }
    for(int i=0;i<tol;i++){
        int sum=0;
        for(int j=i;sum<10000;j++){
            sum+=pim[j];
            ans[sum]++;
        }
    }
}
int main()
{
    init();
   // freopen("1.in","r",stdin);
    int n;
    while(scanf("%d",&n)&&n){
        printf("%d\n",ans[n]);
    }
}
