#include <iostream>
#include <set>

using namespace std;

class c {
public:
  int o;
  int t;
  bool operator<(c s) const {
    return o < s.o || o == s.o && t < s.t;
  }
};

int main(void)
{
  multiset<c, less<c> > s;
  int n;
  c e;

  for (;;) {
    cin >> n;
    if (n == 0)
      return 0;
    s.clear();
    while (n--) {
      cin >> e.o >> e.t;
      s.insert(e);
    }
    set<c, less<c> >::iterator i, j;
    while (!s.empty()) {
      i = s.begin();
      e.o = i->t;
      e.t = i->o;
      j = s.find(e);
      if (j == s.end()) {
        cout << "NO" << endl;
        break;
      } else {
        s.erase(j);
        s.erase(i);
      }
    }
    if (s.empty())
      cout << "YES" << endl;
  }
}
