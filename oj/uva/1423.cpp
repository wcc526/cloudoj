#include<stdio.h>
#include<algorithm>
#include<string>
using namespace std;

int n;
int que[20],sum[20];
char map[11][11];
char ch[101];
bool ffind;

void dfs(int x)
{
   if (x>n)
   {
	 ffind=1;
     for (int i=1;i<=n;i++)
	 if (i<n)
		 printf("%d ",que[i]);
	 else 
		 printf("%d\n",que[i]);
	 return;
   }
   int d;
   if (map[x][x]=='0') 
   {
	  que[x]=0;
	  sum[x]=sum[x-1];
      dfs(x+1);
	  return;
   }
   if (map[x][x]=='+') d=1;
   else d=-1;
   for (int i=1;i<=10;i++)
   {
	  bool succ=1;
      for (int j=x-1;j>=1;j--)
	  {
	     if (sum[x-1]+i*d-sum[j-1]>0&&map[j][x]!='+') 
		 {
		    succ=0;
			break;
		 }
		 if (sum[x-1]+i*d-sum[j-1]<0&&map[j][x]!='-')
		 {
		    succ=0;
			break;
		 }
		 if (sum[x-1]+i*d-sum[j-1]==0&&map[j][x]!='0')
		 {
		    succ=0;
			break;
		 }
	  }
	  if (succ)
	  {
	     que[x]=i*d;
		 sum[x]=sum[x-1]+i*d;
		 dfs(x+1);
		 if (ffind) return;
	  }
   }
}

int main()
{
   int test;
   scanf("%d",&test);
   for (int ii=1;ii<=test;ii++)
   {
      scanf("%d",&n);
	  scanf("%s",ch);
	  int id=0;
	  for (int i=1;i<=n;i++)
	  for (int j=i;j<=n;j++)
		  map[i][j]=ch[id++];
	  sum[0]=0;
	  ffind=0;
	  dfs(1);
   }
   return 0;
}