#include <stdio.h>
#include <string.h>

unsigned char g[][5] = {
  { 0, 1, 2, 0, 3 },
  { 1, 0, 4, 0, 5 },
  { 2, 4, 0, 6, 7 },
  { 0, 0, 6, 0, 8 },
  { 3, 5, 7, 8, 0 }
}, e[9] = {
  1, 1, 1, 1, 1, 1, 1, 1, 1
}, d = 0;
char s[10] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

void t(int n)
{
  int i;
  s[d] = n + '1';
  if (d == 8)
    puts(s);
  else
    for (i = 0; i < 5; ++i)
      if (i ^ n && g[n][i] && e[g[n][i]])
        e[g[n][i]] = 0, ++d, t(i), --d, e[g[n][i]] = 1;
}

int main()
{
  t(0);
  return 0;
}
