Const
    InFile     = 'p10350.in';
    Limit      = 120;
    LimitM     = 15;

Type
    Tdata      = array[1..Limit , 1..LimitM , 1..LimitM] of longint;
    Topt       = array[1..Limit , 1..LimitM] of longint;

Var
    data       : Tdata;
    opt        : Topt;
    name       : string;
    answer ,
    N , M      : longint;

procedure init;
var
    i , j , k  : longint;
begin
    readln(name);
    read(N , M);
    for i := 2 to N do
      for j := 1 to M do
        for k := 1 to M do
          read(data[i , j , k]);
    readln;
end;

procedure work;
var
    i , j , k  : longint;
begin
    fillchar(opt , sizeof(opt) , 1);
    fillchar(opt[1] , sizeof(opt[1]) , 0);
    for i := 1 to N - 1 do
      for j := 1 to M do
        for k := 1 to M do
          if opt[i , j] + data[i + 1 , j , k] < opt[i + 1 , k]
            then opt[i + 1 , k] := opt[i , j] + data[i + 1 , j , k];
    answer := maxlongint;
    for i := 1 to M do
      if opt[N , i] < answer
        then answer := opt[N , i];
    inc(answer , (N - 1) * 2);
end;

procedure out;
begin
    writeln(name);
    writeln(answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.