{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10670.in';
    OutFile    = 'p10670.out';
    Limit      = 200;
    LimitLen   = 100;

Type
    Tstr       = string[LimitLen];
    Tkey       = record
                     name                    : Tstr;
                     A , B , ans             : longint;
                 end;
    Tdata      = array[1..Limit] of Tkey;

Var
    data       : Tdata;
    totCase ,
    nowCase , 
    M , N , L  : longint;

procedure init;
var
    i , p1 , p2 ,
    code       : longint;
    s          : string;
    s1 , s2 , s3
               : Tstr;
begin
    inc(nowCase);
    readln(N , M , L);
    fillchar(data , sizeof(data) , 0);
    for i := 1 to L do
      begin
          readln(s);
          p1 := pos(':' , s);
          p2 := pos(',' , s);
          s1 := copy(s , 1 , p1 - 1);
          s2 := copy(s , p1 + 1 , p2 - p1 - 1);
          s3 := copy(s , p2 + 1 , length(s) - p2);
          while s1[length(s1)] = ' ' do delete(s1 , length(s1) , 1);
          while s1[1] = ' ' do delete(s1 , 1 , 1);
          data[i].name := s1;
          val(s2 , data[i].A , code);
          val(s3 , data[i].B , code);
      end;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tkey;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and ((data[stop].ans > key.ans) or (data[stop].ans = key.ans) and (data[stop].name > key.name)) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and ((data[start].ans < key.ans) or (data[start].ans = key.ans) and (data[start].name < key.name)) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
var
    i , p      : longint;
begin
    for i := 1 to L do
      with data[i] do
        begin
            p := N;
            while (p > M) and (p div 2 >= M) and ((p - p div 2) * A > B) do
              begin
                  ans := ans + B;
                  p := p div 2;
              end;
            ans := ans + (p - M) * A;
        end;
    qk_sort(1 , L);
end;

procedure out;
var
    i          : longint;
begin
    writeln('Case ' , nowCase);
    for i := 1 to L do
      with data[i] do
        writeln(name , ' ' , ans);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(TotCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
