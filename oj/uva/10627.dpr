{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
{$R+,Q+,S+}
Const
    InFile     = 'p10627.in';
    OutFile    = 'p10627.out';

Var
    L , u , v ,
    t , answer : comp;

function init : boolean;
var
    tmp        : comp;
begin
    readln(L , u , v , t);
    if u < v then begin tmp := u; u := v; v := tmp; end;
    init := (L <> 0);
end;

function _mod(a , b : comp) : comp;
begin
    _mod := a - int(a / b) * b;
end;

function Euclid_Extend(a , b , c : comp; var x , y , gcdnum : comp) : boolean;
var
    t          : comp;
begin
    if a = 0
      then begin
               gcdnum := abs(b);
               if _mod(c , b) <> 0
                 then Euclid_Extend := false
                 else begin
                          Euclid_Extend := true;
                          x := 0; y := int(c / b);
                      end;
           end
      else begin
               Euclid_Extend := false;
               if Euclid_Extend(_mod(b , a) , a , c , y , t , gcdnum)
                 then begin
                          Euclid_Extend := true;
                          x := t - int(b / a) * y;
                      end;
           end;
end;

function calc(u , v , bound : comp) : comp;
var
    tmp , upper ,
    gcdnum ,
    dx , dy ,
    x , y      : comp;
begin
    if not euclid_extend(2 * v , - 2 * u , u , x , y , gcdnum)
      then calc := 0
      else begin
               dx := 2 * u / gcdnum; dy := 2 * v / gcdnum;
               if x < 0 then
                 begin
                     tmp := int(-x / dx) + 10;
                     x := x + dx * tmp; y := y + dy * tmp;
                 end;
               tmp := int(x / dx);
               x := x - dx * tmp; y := y - dy * tmp;
               upper := int(t * u / 2 / L);
               if upper * 2 * L = bound then upper := upper - 1;
               if upper >= x
                 then calc := int((upper - x) / dx) + 1
                 else calc := 0;
           end;
end;

function Get_pos(startp , u , t : extended) : extended;
var
    res        : extended;
begin
    if u = 0 then
      begin
          Get_Pos := startp; exit;
      end;
    t := t - int(t * u / 2 / L) * 2 * L / u;
    res := t * u;
    if res > L then res := 2 * L - res;
    res := abs(startp - res);
    Get_pos := res;
end;

procedure work;
var
    p , q      : comp;
begin
    answer := int(t * u / L);
    if (u = 0) or (v = 0)
      then begin
               answer := int(answer / 2);
               if u = 0 then exit;
           end
      else begin
               answer := answer - calc(u , v , int(t * u / L) * L + 1);
               answer := answer - calc(v , u , int(t * u / L) * L + 1);
           end;

    p := _mod(t * u , 2 * L); q := _mod(t * v , 2 * L);
    if q > L then q := 2 * L - q; q := L - q;
    if p < L
      then if q <= p then answer := answer + 1 else
      else if q >= 2 * L - p then answer := answer + 1 else;
end;

procedure print(answer : comp);
begin
    if answer = 0
      then exit
      else begin
               print(int(answer / 10));
               write(_mod(answer , 10) : 0 : 0);
           end;
end;

procedure out;
begin
    if answer < 0 then write('-');
    if answer = 0
      then write(0)
      else print(abs(answer));
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
