{$I-}
Var
    n , m , t ,
    i , tmp , find ,
    best , sum : longint;
Begin
    while not eof do
      begin
          readln(n , m , t);
          if n = 0 then break;
          best := maxlongint; sum := 0;
          if n < m then begin tmp := n; n := m; m := tmp; end;
          find := 0;
          for i := 0 to t div n do
            if (t - i * n) mod m = 0 then
              begin
                  writeln(i + (t - i * n) div m);
                  find := 1;
                  break;
              end
            else
              if (t - i * n) mod m <= best
                then if (t - i * n) mod m < best
                       then begin
                                sum := i + (t - i * n) div m;
                                best := (t - i * n) mod m;
                            end
                       else if i + (t - i * n) div m > sum then
                              sum := i + (t - i * n) div m;
          if find = 0 then writeln(sum , ' ' , best);
      end;
End.