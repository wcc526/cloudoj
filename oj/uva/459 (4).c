#include <stdio.h>
#include <string.h>

int main()
{
  int c, i, j, k, n, p;
  unsigned char g[26][26], s[26], *t = s, v[26];

  scanf("%d", &c);
  while (c--) {
    memset(g, 0, sizeof(g)), memset(v, 1, sizeof(v));
    while ((n = getchar()) <= '\n');
    n -= 'A' - 1, getchar();
    while ((i = getchar()) ^ '\n' && i ^ EOF)
      i -= 'A', j = getchar() - 'A', g[i][j] = g[j][i] = 1, getchar();
    for (p = 0, i = n; i--;)
      if (v[i])
        for (++p, *t++ = i; s < t;)
          if (v[k = *--t])
            for (v[k] = 0, j = n; j--;)
              if (j ^ k && v[j] && g[j][k])
                *t++ = j;
    printf("%d\n", p);
    if (c)
      putchar('\n');
  }

  return 0;
}
