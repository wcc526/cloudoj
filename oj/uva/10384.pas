Const
    InFile     = 'p10384.in';
    N          = 4;
    M          = 6;
    LimitHash  = 99997;
    LimitQueue = 10000;
    dirx       : array[1..4] of longint = (0 , -1 , 0 , 1);
    diry       : array[1..4] of longint = (-1 , 0 , 1 , 0);
    name       : array[1..4] of char = 'WNES';

Type
    Tmap       = array[1..N , 1..M] of longint;
    Thash      = array[0..LimitHash] of
                   record
                       used , xy             : byte;
                       data                  : array[1..N] of longint;
                   end;
    Tqueue     = array[1..LimitQueue] of
                   record
                       xy , strategy         : byte;
                       shortest              : smallint;
                       father                : longint;
                       data                  : array[1..N] of longint;
                   end;

Var
    map        : Tmap;
    hash       : Thash;
    queue      : Tqueue;
    open , closed ,
    x , y , T  : longint;

function init : boolean;
var
    i , j     : longint;
begin
    inc(T);
    readln(y , x);
    if (x = 0) and (y = 0) then exit(false);
    init := true;
    for i := 1 to N do
      for j := 1 to M do
        read(map[i , j]);
end;

function add_hash : boolean;
var
    p1 , p2 , p3 ,
    p4 , p5 , p: longint;
begin
    p1 := queue[closed].xy;
    p2 := queue[closed].data[1] mod LimitHash; p3 := queue[closed].data[2] mod LimitHash;
    p4 := queue[closed].data[3] mod LimitHash; p5 := queue[closed].data[4] mod LimitHash;
    p := (p1 * 12341 + p2 * 3 + p3 * 7 + p4 * 13 + p5 * 29) mod LimitHash;
    while hash[p].used = T do
      begin
          if (p1 = hash[p].xy) and (p2 = hash[p].data[1]) and (p3 = hash[p].data[2])
              and (p4 = hash[p].data[3]) and (p5 = hash[p].data[4]) then
            exit(false);
          inc(p); if p = LimitHash then p := 0;
      end;
    hash[p].xy := p1; hash[p].data[1] := p2; hash[p].data[2] := p3;
    hash[p].data[3] := p4; hash[p].data[4] := p5; hash[p].used := T;
    exit(true);
end;

procedure add(strategy : longint);
var
    i , j      : longint;
begin
    inc(closed);
    queue[closed].xy := x * 10 + y;
    for i := 1 to N do
      begin
          queue[closed].data[i] := 0;
          for j := 1 to M do
            queue[closed].data[i] := queue[closed].data[i] * 16 + map[i , j];
      end;
    if closed <> 1 then
      begin
          queue[closed].father := open;
          queue[closed].shortest := queue[open].shortest + 1;
          queue[closed].strategy := strategy;
      end;
    if not add_hash then dec(closed);
end;

procedure extract;
var
    i , j , tmp: longint;
begin
    x := queue[open].xy div 10; y := queue[open].xy mod 10;
    for i := 1 to N do
      begin
          tmp := queue[open].data[i];
          for j := M downto 1 do
            begin
                map[i , j] := tmp mod 16; tmp := tmp div 16;
            end;
      end;
end;

procedure work;
var
    i , nx , ny ,
    t1         : longint;

  function push : boolean;
  var
      x1 , y1 ,
      tmp      : longint;
  begin
      tmp := 1 shl (i - 1);
      if tmp and map[nx , ny] <> 0 then exit(false);
      push := true;
      inc(map[nx , ny] , tmp); dec(map[x , y] , tmp);
      if odd(i) then tmp := 1 shl (3 - i) else tmp := 1 shl (5 - i);
      dec(map[nx , ny] , tmp);
      x1 := nx + dirx[i]; y1 := ny + diry[i];
      if (x1 <= N) and (y1 <= M) and (x1 >= 1) and (y1 >= 1) then
        inc(map[x1 , y1] , tmp);
  end;

begin
    open := 1; closed := 0; add(0); queue[1].shortest := 0;
    while open <= closed do
      begin
          for i := 1 to 4 do
            begin
                extract;
                nx := x + dirx[i]; ny := y + diry[i];
                t1 := (1 shl (i - 1)) and map[x , y];
                if (nx <= N) and (ny <= M) and (nx >= 1) and (ny >= 1)
                  then if t1 = 0
                         then begin x := nx; y := ny; add(i); end
                         else if push
                                then begin x := nx; y := ny; add(i); end
                                else
                  else if t1 = 0 then begin x := nx; y := ny; add(i); exit; end;
            end;
          inc(open);
      end;
end;

procedure out;
var
    i          : longint;
    s          : string;
begin
    s := ''; i := closed;
    while i <> 1 do
      begin
          s := name[queue[i].strategy] + s;
          i := queue[i].father;
      end;
    writeln(s);
end;

Begin
    fillchar(hash , sizeof(hash) , 0);
 //   assign(INPUT , InFile); ReSet(INPUT);
      T := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(INPUT);
End.