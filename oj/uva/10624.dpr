{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10624.in';
    OutFile    = 'p10624.out';
    Limit      = 30;
    LimitSave  : longint = 900000000;

Type
    Tdata      = array[1..Limit] of longint;

Var
    data       : Tdata;
    N , M ,
    totCase ,
    nowCase    : longint;

procedure init;
begin
    read(N , M);
    inc(nowCase);
end;

function prime(p : longint) : boolean;
var
    i          : longint;
begin
    prime := false;
    for i := 2 to p - 1 do
      if p mod i = 0 then
        exit;
    prime := true;
end;

function mono(var count : longint) : boolean;
var
    i , j , k  : longint;
begin
    mono := false;
    dec(count);
    for i := N to M do
      if i >= 10 then
        begin
            k := 0;
            for j := 1 to i - 1 do
              begin
                  inc(k , data[j]);
                  k := k * 10 mod i;
              end;
            k := (i - k) mod i;
            if k >= 10 then exit;
            data[i] := k;
        end;
    mono := true;
    for i := 1 to M do
      write(data[i]);
    writeln;
end;

function dfs(p : longint; var count : longint) : boolean;
var
    i , j , k  : longint;
begin
    dfs := false;
    if count <= 0 then exit;
    if (p >= N) and (p >= 10) or (p > M)
      then dfs := mono(count)
      else for i := 0 to 9 do
             if (p <> 1) or (i <> 0) then
               begin
                   data[p] := i;
                   k := 0;
                   if p >= N then
                     for j := 1 to p do
                        k := (k * 10 + data[j]) mod p;
                   if k = 0 then
                     if dfs(p + 1 , count) then
                       begin dfs := true; exit; end;
               end;
end;

procedure work;
var
    i , count  : longint;
begin
    count := 1;
    if N = 1 then inc(N);
    for i := 2 to M do
      if prime(i) then
        if LimitSave div i > count
          then count := i * count
          else count := LimitSave;

    Write('Case ' , nowCase , ': ');
    if not dfs(1 , count)
      then writeln(-1);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
