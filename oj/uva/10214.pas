Var
    A , B , i  : longint;
    gcdnum     : array[0..2000 , 0..2000] of longint;
    p1 , p2    : extended;

function gcd(a , b : longint) : longint;
begin
    if a = 0
      then exit(b)
      else if gcdnum[a , b] = -1
             then begin
                      gcdnum[a , b] := gcd(b mod a , a);
                      exit(gcdnum[a , b]);
                  end
             else exit(gcdnum[a , b]);
end;

function calc(N , M : longint) : longint;
var
    i , p ,
    sum , sum1 : longint;
begin
    p := M mod N; sum := 0;
    for i := 0 to N - 1 do
      begin
          if gcdnum[i , N] = 1 then inc(sum);
          if i = p then sum1 := sum;
      end;
    calc := (M div N) * sum + sum1;
end;

Begin
    fillchar(gcdnum , sizeof(gcdnum) , $FF);

    for A := 0 to 2000 do
      for B := 0 to 2000 do
        if gcdnum[A , B] = -1 then
          gcdnum[A , B] := gcd(A , B);
    readln(A , B);
    while A + B <> 0 do
      begin
          p1 := 0;
          for i := 1 to A do
            p1 := p1 + calc(i , B);
          p2 := (A * 2 + 1);
          p2 := p2 * (B * 2 + 1) - 1;
          writeln(p1 * 4 / p2 : 0 : 7);
          readln(A , B);
      end;
End.