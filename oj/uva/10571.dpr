{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10571.in';
    OutFile    = 'p10571.out';
    Limit      = 10;
    LimitSave  = 1000;

Type
    Tmap       = array[1..Limit , 1..Limit] of longint;
    Tdata      = array[1..Limit] of longint;
    Tcompose   = array[1..Limit] of
                   record
                       tot    : longint;
                       data   : array[1..LimitSave] of
                                  record
                                      p1 , p2               : longint;
                                  end;
                   end;
    Tused      = array[1..LimitSave] of boolean;

Var
    map        : Tmap;
    X , Y ,
    visited ,
    last       : Tdata;
    compose    : Tcompose;
    used       : Tused;
    N          : longint;
    first      : boolean;

function init : boolean;
var
    i          : longint;
begin
    fillchar(X , sizeof(X) , 0);
    fillchar(Y , sizeof(Y) , 0);
    read(N);
    if N = 0
      then init := false
      else begin
               for i := 1 to N do read(Y[i]);
               for i := 1 to N do read(X[i]);
               init := true;
           end;
end;

function dfs(step : longint) : boolean;
var
    i , j , k ,
    p1 , p2    : longint;
begin
    dfs := true;
    if step > N then exit;
    for k := 1 to compose[step].tot do
      begin
          p1 := compose[step].data[k].p1; p2 := compose[step].data[k].p2;
          for i := 1 to N do
            if (last[i] mod p1 = 0) and ((visited[i] = 0) or (last[i] = p1)) and (visited[i] <> 2) and not used[p1] then
              for j := i + 1 to N do
                if (last[j] mod p2 = 0) and ((visited[j] = 0) or (last[j] = p2)) and (visited[j] <> 2) and not used[p2] then
                  begin
                      used[p1] := true; used[p2] := true;
                      inc(visited[i]); inc(visited[j]);
                      last[i] := last[i] div p1; last[j] := last[j] div p2;
                      map[step , i] := p1; map[step , j] := p2;
                      if dfs(step + 1)
                        then exit;
                      used[p1] := false; used[p2] := false;
                      map[step , i] := 0; map[step , j] := 0;
                      dec(visited[i]); dec(visited[j]);
                      last[i] := last[i] * p1; last[j] := last[j] * p2;
                  end;
      end;
    dfs := false;
end;

procedure work;
var
    i , j      : longint;
begin
    fillchar(map , sizeof(map) , 0);
    fillchar(compose , sizeof(compose) , 0);
    for i := 1 to N do
      begin
          compose[i].tot := 0;
          for j := 1 to X[i] do
            if X[i] mod j = 0 then
              begin
                  inc(compose[i].tot);
                  compose[i].data[compose[i].tot].p1 := j;
                  compose[i].data[compose[i].tot].p2 := X[i] div j;
              end;
      end;
    last := Y;
    fillchar(visited , sizeof(visited) , 0);
    fillchar(used , sizeof(used) , 0);
    dfs(1);
end;

procedure out;
var
    i , j      : longint;
begin
    if not first then writeln;
    first := false;
    for i := 1 to N do
      begin
          for j := 1 to N do
            write(map[i , j] : 10);
          writeln;
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      first := true;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
