Var
    i          : longint;
    N          : extended;
Begin
    i := 0; readln(N);
    while N <> 0 do
      begin
          inc(i);
          write('Case ' , i , ': ');
          writeln(trunc((3 + sqrt(8 * N + 9)) / 2 + 0.9999));
          readln(N);
      end;
End.