Const
    InFile     = 'p10261.in';
    Limit      = 1000;
    LimitLen   = 10000;

Type
    Tdata      = array[0..Limit] of longint;
    Topt       = array[0..Limit , 0..LimitLen] of boolean;

Var
    sum ,
    data       : Tdata;
    opt        : Topt;
    cases ,
    N , M      : longint;

procedure init;
begin
    fillchar(sum , sizeof(sum) , 0);
    fillchar(data , sizeof(data) , 0);
    fillchar(opt , sizeof(opt) , 0);
    N := 0;
    readln(M); M := M * 100;
    while true do
      begin
          inc(N);
          readln(data[N]);
          sum[N] := sum[N - 1] + data[N];
          if data[N] = 0 then begin dec(N); break; end;
      end;
end;

procedure dfs_print(a , b : longint);
begin
    if a > 0 then
      begin
          if opt[a - 1 , b]
            then begin
                     dfs_print(a - 1 , b);
                     writeln('starboard');
                 end
            else begin
                     dfs_print(a - 1 , b - data[a]);
                     writeln('port');
                 end;
      end;
end;

procedure work;
var
    i , j ,
    a , b      : longint;
begin
    opt[0 , 0] := true;
    a := 0; b := 0;
    for i := 1 to N do
      begin
          if sum[i] > M * 2 then break;
          for j := 0 to M do
            if sum[i] - j <= M then
              begin
                  if j >= data[i] then
                    opt[i , j] := opt[i , j] or opt[i - 1 , j - data[i]];
                  opt[i , j] := opt[i , j] or opt[i - 1 , j];
                  if opt[i , j] then
                    begin a := i; b := j; end;
              end;
      end;
    writeln(a);
    dfs_print(a , b);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            dec(Cases);
            init;
            work;
            writeln;
        end;
    Close(INPUT);
End.