#include <stdio.h>

int c, n, m[10], v[5][3], x[5][3];
double a, b;

int g(int x, int y)
{
  return x < y ? x : y;
}

double h(double x)
{
  return x < 0 ? -x : x;
}

void f(int i)
{
  int j;
  double t;
  if (i == n) {
    for (t = j = 0; j < c; ++j)
      if (v[j][0] == 1)
        t += h(v[j][1] - a);
      else if (v[j][0] == 2)
        t += h(v[j][1] + v[j][2] - a);
      else
        t += a;
    if (b > t) {
      b = t;
      for (j = 0; j < c; ++j)
        x[j][0] = v[j][0], x[j][1] = v[j][1], x[j][2] = v[j][2];
    }
  } else {
    for (j = 0; j < g(c, i + 1); ++j) {
      if (v[j][0] < 2) {
        v[j][++v[j][0]] = m[i];
        f(i + 1);
        --v[j][0];
      }
    }
  }
}

int main()
{
  int i, s = 0;

  while (scanf("%d %d", &c, &n) == 2) {
    for (i = c; i--;)
      v[i][0] = 0;
    for (a = i = 0; i < n; ++i)
      scanf("%d", m + i), a += m[i];
    a /= c;
    printf("Set #%d\n", ++s);
    b = 1e10;
    f(0);
    for (i = 0; i < c; ++i) {
      printf(" %d:", i);
      if (x[i][0] > 0)
        printf(" %d", x[i][1]);
      if (x[i][0] > 1)
        printf(" %d", x[i][2]);
      putchar('\n');
    }
    printf("IMBALANCE = %.5lf\n", b);
    putchar('\n');
  }

  return 0;
}
