Const
    InFile     = 'p10357.in';
    Limit      = 20;

Type
    Tdata      = array[1..Limit] of
                   record
                       x , y , v             : longint;
                   end;

Var
    data       : Tdata;
    N          : longint;

procedure init;
var
    i          : longint;
    ch         : char;
begin
    for i := 1 to 8 do read(ch);
    readln(N);
    for i := 1 to N do
      readln(data[i].x , data[i].y , data[i].v);
end;

procedure workout;
var
    a , b , c ,
    d , e , f , g ,
    j , x , y , t ,
    i , M      : longint;
    caught     : boolean;
    ch         : char;
begin
    for i := 1 to 6 do read(ch);
    readln(M);
    for i := 1 to M do
      begin
          readln(a , b , c , d , e , f , g);
          for j := 1 to Maxlongint do
            if a * j * j + b * j + c <= 0
              then begin
                       x := d * j + e; y := f * j + g;
                       t := j;
                       break;
                   end;
          caught := false;
          for j := 1 to N do
            if sqr(data[j].x - x) + sqr(data[j].y - y) <= sqr(t) * sqr(data[j].v) then
              begin
                  caught := true;
                  break;
              end;
          write('Ball ' , i , ' was ');
          if (x < 0) or (y < 0)
            then write('foul')
            else if caught
                   then write('caught')
                   else write('safe');
          writeln(' at (' , x , ',' , y , ')');
      end;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      while not eof do
        begin
            init;
            workout;
        end;
//    Close(INPUT);
End.