#include <stdio.h>

typedef struct node {
  unsigned short v;
  unsigned short f;
  struct node *p;
  struct node *c[10];
} node;

typedef struct {
  unsigned short v;
  unsigned short f;
  struct node *p;
} leaf;

void z(node *n, node *c) {
  int i;
  if (n->p)
    z(n->p, n);
  if (c) {
    for (i = 0; n->c[i] != c; ++i);
    putc(i + '0', stdout);
  }
}

short g(void)
{
  short c, r;
  while ((c = getc(stdin)) == ' ' || c == '\n');
  r = c - '0';
  for (; (c = getc(stdin)) >= '0'; r = r * 10 + c - '0');
  return r;
}

int main()
{
  leaf q[26], *u, *x, *p, t;
  node a[25], *v, *w;
  char b[7];
  unsigned short o[26];
  int c = 0, f, i, j, n, r;

  b[0] = b[1] = b[2] = b[3] = b[6] = ' ', b[5] = ':';
  while ((r = g())) {
    x = q + (n = g());
    for (i = 0, u = q; u < x; ++u)
      u->v = i++, u->f = g();
    for (u = q + 1; u < x; ++u) {
      for (p = u, t = *p; p-- > q && p->f > t.f;)
        p[1] = *p;
      p[1] = t;
    }
    for (i = n; i--;)
      o[q[i].v] = i;
    f = (n + (n - 1) / (r - 1)) % r;
    f = f == 1 ? 0 : r - f;
    for (u = q, v = w = a; u < x || v + 1 < w; ++w) {
      w->v = 0xFF, w->f = 0, j = 0;
      if (u == q)
        while (j < f)
          w->c[j++] = NULL;
      for (; j < r; ++j) {
        if (u == x || (v < w && (u->f > v->f || (u->f == v->f && u->v > v->v))))
          v->p = w, w->c[j] = v++;
        else
          u->p = w, w->c[j] = (node *)u++;
        w->f += w->c[j]->f;
        if (w->v > w->c[j]->v)
          w->v = w->c[j]->v;
      }
    }
    v->p = NULL;
    for (f = 0, u = q; u < x; f += j * u++->f)
      for (j = 0, p = u; p->p; p = (leaf *)p->p, ++j);
    printf("Set %d; average length %.2lf\n", ++c, (double)f / v->f);
    for (i = 0; i < n; ++i) {
      b[4] = q[o[i]].v + 'A';
      fwrite(b, 1, 7, stdout);
      z((node *)&q[o[i]], NULL);
      putc('\n', stdout);
    }
    putc('\n', stdout);
  }

  return 0;
}
