{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10609.in';
    OutFile    = 'p10609.out';
    Limit      = 500000;
    minimum    = 1e-8;

Type
    Tpoint     = record
                     x , y    : double;
                 end;
    Tdata      = array[1..Limit] of Tpoint;

Var
    data       : Tdata;
    A , B      : Tpoint;
    T          : double;
    N , nowCase: longint;

function init : boolean;
begin
    inc(nowCase);
    N := 0;
    fillchar(data , sizeof(data) , 0);
    readln(A.x , A.y , B.x , B.y , T);
    init := (T > 1);
end;

procedure rotate(var p : Tpoint; sinA , cosA : double);
var
    newp       : Tpoint;
begin
    newp.x := p.x * cosA - p.y * sinA;
    newp.y := sinA * p.x + p.y * cosA;
    p := newp;
end;

procedure dfs(A , B : Tpoint);
var
    p1 , p2 , p3
               : Tpoint;
begin
    p2.x := (B.x - A.x) / 4; p2.y := (B.y - A.y) / 4;
    p1.x := A.x + p2.x; p1.y := A.y + p2.y;
    p3.x := B.x - p2.x; p3.y := B.y - p2.y;
    p2.x := p2.x * 2; p2.y := p2.y * 2;
    rotate(p2 , sqrt(3) / 2 , 1 / 2);
    p2.x := p2.x + p1.x; p2.y := p2.y + p1.y;
    if sqr(p1.x - p2.x) + sqr(p1.y - p2.y) <= sqr(T) + minimum then exit;
    data[N + 1] := p1; data[N + 2] := p2; data[N + 3] := p3;
    inc(N , 3);
    dfs(p1 , p2);
    dfs(p2 , p3);
end;

function bigger(p1 , p2 : Tpoint) : boolean;
begin
    if abs(p1.x - p2.x) <= minimum
      then bigger := p1.y > p2.y
      else bigger := p1.x > p2.x;
end;

procedure qk_pass(start , stop : longint; var mid : longint);
var
    key        : Tpoint;
    tmp        : longint;
begin
    tmp := random(stop - start + 1) + start;
    key := data[tmp]; data[tmp] := data[start];
    while start < stop do
      begin
          while (start < stop) and bigger(data[stop] , key) do dec(stop);
          data[start] := data[stop];
          if start < stop then inc(start);
          while (start < stop) and bigger(key , data[start]) do inc(start);
          data[stop] := data[start];
          if start < stop then dec(stop);
      end;
    mid := start;
    data[start] := key;
end;

procedure qk_sort(start , stop : longint);
var
    mid        : longint;
begin
    if start < stop then
      begin
          qk_pass(start , stop , mid);
          qk_sort(start , mid - 1);
          qk_sort(mid + 1 , stop);
      end;
end;

procedure work;
begin
    data[N + 1] := A; data[N + 2] := B;
    inc(N , 2);
    dfs(A , B);

    qk_sort(1 , N);
end;

procedure out;
var
    i          : longint;
begin
    writeln('Case ' , nowCase , ':');
    writeln(N);
    for i := 1 to N do
      writeln(data[i].x : 0 : 5 , ' ' , data[i].y : 0 : 5);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      nowCase := 0;
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
