{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10703.in';
    OutFile    = 'p10703.out';
    Limit      = 99;

Type
    Tdata      = array[1..Limit] of
                   record
                       x1 , y1 , x2 , y2     : longint;
                   end;

Var
    data       : Tdata;
    answer , 
    W , H , N  : longint;

function init : boolean;
var
    i          : longint;
begin
    readln(W , H , N);
    if W + H + N = 0
      then init := false
      else begin
               init := true;
               for i := 1 to N do
                 with data[i] do
                   begin
                       read(x1 , y1 , x2 , y2);
                       if x1 > x2 then begin inc(x1 , x2); x2 := x1 - x2; dec(x1 , x2); end;
                       if y1 > y2 then begin inc(y1 , y2); y2 := y1 - y2; dec(y1 , y2); end;
                   end;
           end;
end;

procedure work;
var
    i , j , k  : longint;
begin
    answer := 0;
    for i := 1 to W do
      for j := 1 to H do
        begin
            inc(answer);
            for k := 1 to N do
              if (i <= data[k].x2) and (i >= data[k].x1) and (j <= data[k].y2) and (j >= data[k].y1) then
                begin
                    dec(answer);
                    break;
                end;
        end;
end;

procedure out;
begin
    if answer = 0
      then writeln('There is no empty spots.')
      else if answer = 1
             then writeln('There is one empty spot.')
             else writeln('There are ' , answer , ' empty spots.');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
