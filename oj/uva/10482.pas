Const
    InFile     = 'p10482.in';
    OutFile    = 'p10482.out';
    Limit      = 32;
    LimitWeight= 20;
    LimitSave  = Limit * LimitWeight;

Type
    Topt       = array[0..Limit , 0..LimitSave , 0..LimitSave] of smallint;
    Tdata      = array[1..Limit] of longint;
    Tsubsum    = array[0..Limit] of longint;

Var
    opt        : Topt;
    data       : Tdata;
    subsum     : Tsubsum;
    cases , totCase ,
    answer ,
    N          : longint;

procedure init;
var
    i          : longint;
begin
    inc(cases);
    fillchar(opt , sizeof(opt) , $FF);
    read(N);
    for i := 1 to N do read(data[i]);
    subsum[0] := 0;
    for i := 1 to N do subsum[i] := subsum[i - 1] + data[i];
end;

function difference(a , b , c : longint) : longint;
var
    tmp        : longint;
begin
    if b > c then begin tmp := b; b := c; c := tmp; end;
    if a > b then begin tmp := a; a := b; b := tmp; end;
    if b > c then begin tmp := b; b := c; c := tmp; end;
    exit(c - a);
end;

function dfs(step , p1 , p2 : longint) : longint;
var
    n1 , n2 ,
    n3         : longint;
begin
    if step > N
      then exit(difference(p1 , p2 , subsum[N] - p1 - p2))
      else if opt[step , p1 , p2] <> -1
             then exit(opt[step , p1 , p2])
             else begin
                      n1 := dfs(step + 1 , p1 + data[step] , p2);
                      n2 := dfs(step + 1 , p1 , p2 + data[step]);
                      n3 := dfs(step + 1 , p1 , p2);
                      if n1 > n2 then n1 := n2; if n1 > n3 then n1 := n3;
                      opt[step , p1 , p2] := n1;
                      exit(n1);
                  end;
end;

procedure work;
begin
    answer := dfs(1 , 0 , 0);
end;

procedure out;
begin
    writeln('Case ' , cases , ': ' , answer);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(totCase);
      cases := 0;
      while Cases < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(INPUT);
End.