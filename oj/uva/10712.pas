{$R+,Q+,S+}
Const
    InFile     = 'p10712.in';
    OutFile    = 'p10712.out';
    LimitLen   = 10;
    LimitPatternLen
               = 3;

Type
    Tnext      = array[0..LimitPatternLen - 1 , 0..9] of longint;
    Topt0      = array[0..LimitLen , 0..LimitPatternLen - 1] of longint;
    Topt1      = array[0..LimitLen , 0..LimitPatternLen - 1 , 0..1] of longint;

Var
    next       : Tnext;
    opt0       : Topt0;
    opt1       : Topt1;
    u          : string;
    answer ,
    A , B , N  : longint;

function init : boolean;
begin
    readln(A , B , N);
    init := (A <> -1) or (B <> -1) or (N <> -1);
    str(N , u);
end;

procedure Get_Next;
var
    i , j , k  : longint;
    tmps       : string;
begin
    for i := 0 to length(u) - 1 do
      for j := 0 to 9 do
        begin
            tmps := copy(u , 1 , i) + chr(j + ord('0'));
            k := 0;
            while (k <= length(u)) and (u[k] = tmps[k]) do inc(k);
            next[i , j] := k;
        end;
end;

function calc(number : longint) : longint;
var
    s          : string;
    res ,
    i , j , k ,
    p , newp ,
    start , stop
               : longint;
begin
    if number < 0
      then calc := 0
      else begin
               str(number , s);
               fillchar(opt0 , sizeof(opt0) , 0);
               fillchar(opt1 , sizeof(opt1) , 0);
               opt0[0 , 0] := 1;
               for i := 0 to length(s) - 1 do
                 for j := 0 to length(u) - 1 do
                   if opt0[i , j] <> 0 then
                     begin
                         if i = 0 then start := 1 else start := 0;
                         for k := start to 9 do
                           if next[j , k] <> length(u) then
                             inc(opt0[i + 1 , next[j , k]] , opt0[i , j]);
                     end;
               opt1[0 , 0 , 1] := 1;
               for i := 0 to length(s) - 1 do
                 for j := 0 to length(u) - 1 do
                   for p := 0 to 1 do
                     if opt1[i , j , p] <> 0 then
                       begin
                           if i = 0 then start := 1 else start := 0;
                           if p = 0 then stop := 9 else stop := ord(s[i + 1]) - ord('0');
                           for k := start to stop do
                             if next[j , k] <> length(u) then
                               begin
                                   if p = 0
                                     then newp := 0
                                     else if k = stop
                                            then newp := 1
                                            else newp := 0;
                                   inc(opt1[i + 1 , next[j , k] , newp] , opt1[i , j , p]);
                               end;
                       end;
               res := 0;
               for i := 1 to length(s) - 1 do
                 for j := 0 to length(u) - 1 do
                   inc(res , opt0[i , j]);
               for j := 0 to length(u) - 1 do
                 for p := 0 to 1 do
                   inc(res , opt1[length(s)  , j , p]);
               if u <> '0' then inc(res);
               calc := res;
           end;
end;

procedure work;
begin
    Get_Next;
    answer := B - A + 1 - (calc(B) - calc(A - 1));
end;

procedure out;
begin
    writeln(answer);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while init do
        begin
            work;
            out;
        end;
    Close(OUTPUT);
    Close(INPUT);
End.
