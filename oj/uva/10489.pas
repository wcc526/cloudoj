Var
    cases , K ,
    N , B , i , p ,
    j , tmp , answer
               : longint;
Begin
    readln(Cases);
    while Cases > 0 do
      begin
          dec(Cases);
          readln(N , B);
          answer := 0;
          for i := 1 to B do
            begin
                read(K); tmp := 1;
                for j := 1 to K do
                  begin
                      read(p); tmp := tmp * p mod N;
                  end;
                answer := (answer + tmp) mod N;
            end;
          writeln(answer);
      end;
End.