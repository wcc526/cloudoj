#include <algorithm>
#include <cstdio>
#include <functional>
#include <iterator>
#include <vector>

using namespace std;

template<typename T>
struct delete_pointer : public unary_function<T *, void> {
  void operator()(T *&v) {
    delete v;
  }
};

class node {
public:
  node **child;
  node *fail;
  node *parent;
  char value;
  unsigned char nodes;
  unsigned short patt;

  node(char value, unsigned short patt) : child(0), fail(0), parent(0),
                                          value(value), nodes(0), patt(patt) {
  }

  ~node(void) {
    for_each(child, child + nodes, delete_pointer<node>());
    delete [] child;
  }

  void add(node *add) {
    int i = 0;
    node **temp = new node *[nodes + 1];
    node **curr = child;
    while (i < nodes && **curr < *add)
      temp[i++] = *curr++;
    temp[i++] = add;
    ++nodes;
    while (i < nodes)
      temp[i++] = *curr++;
    delete [] child;
    child = temp;
  }

  node *get(char key) {
    int left = 0, right = nodes - 1, mid;
    char value;
    while (right - left > 4) {
      mid = (left + right) >> 1;
      value = child[mid]->value;
      if (value < key)
        left = mid + 1;
      else if (value > key)
        right = mid - 1;
      else
        return child[mid];
    }
    while (left <= right) {
      if (child[left]->value == key)
        return child[left];
      ++left;
    }
    return 0;
  }

  inline bool operator<(node &right) {
    return value < right.value;
  }
};

class suffix_tree {
private:
  node root;
  int nodes;

public:
  vector<int> match;

  suffix_tree(void) : root(0, 0), nodes(0) {
    root.fail = &root;
    match.push_back(0);
  }

  ~suffix_tree(void) {
  }

  void add(const char *in) {
    node *curr = &root, *next;
    while (*in) {
      next = curr->get(*in);
      if (next == 0)
        break;
      curr = next;
      ++in;
    }
    if (*in) {
      do {
        next = new node(*in, in[1] == 0 ? match.size() : 0);
        next->parent = curr;
        curr->add(next);
        curr = next;
        ++nodes;
      } while (*++in);
      match.push_back(0);
    } else if (curr->patt) {
      match.push_back(-curr->patt);
    } else {
      curr->patt = match.size();
      match.push_back(0);
    }
  }

  void prepare(void) {
    nodes -= root.nodes;
    node **q = new node *[nodes];
    node **end = q, **curr = q;
    node *fail, *node;
    for (int i = root.nodes; i--; ) {
      root.child[i]->fail = &root;
      for (int j = root.child[i]->nodes; j--; )
        *end++ = root.child[i]->child[j];
    }
    while (curr < q + nodes) {
      node = *curr;
      fail = node->parent->fail;
      while (!(node->fail = fail->get(node->value)) &&
             !(fail == &root && (node->fail = &root)))
        fail = fail->fail;
      if (node->fail->patt)
        node->patt |= 0x8000;
      for (int i = node->nodes; i--; )
        *end++ = node->child[i];
      ++curr;
    }
    delete [] q;
  }

  void process(const char *in) {
    node *curr = &root, *next;
    while (*in) {
      next = curr->get(*in);
      if (next) {
        curr = next;
        if (curr->patt & 0x7FFF)
          ++match[curr->patt & 0x7FFF];
        if (curr->patt & 0x8000) {
          next = curr;
          do {
            next = next->fail;
            if (next->patt & 0x7FFF)
              ++match[next->patt & 0x7FFF];
          } while (next->patt & 0x8000);
        }
        ++in;
      } else if (curr == &root) {
        ++in;
      } else {
        curr = curr->fail;
      }
    }
  }
};

int main(void)
{
  int n, q;
  char s[100001], t[1001];

  scanf("%d", &n);

  while (n--) {
    suffix_tree tree;

    scanf("%100000s", s);
    scanf("%d", &q);
    while (q--) {
      scanf("%1000s", t);
      tree.add(t);
    }

    tree.prepare();

    tree.process(s);

    for (vector<int>::size_type i = 1; i < tree.match.size(); ++i)
      if (tree.match[i] < 0) {
        if (tree.match[-tree.match[i]])
          puts("y");
        else
          puts("n");
      } else {
        if (tree.match[i])
          puts("y");
        else
          puts("n");
      }
  }

  return 0;
}
