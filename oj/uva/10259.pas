Const
    Limit      = 100;
    InFile     = 'p10259.in';

Type
    Tdata      = array[1..Limit , 1..Limit] of longint;

Var
    data , opt : Tdata;
    answer , cases ,
    N , K      : longint;

procedure init;
var
    i , j      : longint;
begin
    readln(N , K);
    for i := 1 to N do
      begin
          for j := 1 to N do
            read(data[i , j]);
          readln;
      end;
    fillchar(opt , sizeof(opt) , $FF);
end;

function max(i , j : longint) : longint;
begin
    if i < j then exit(j) else exit(i);
end;

function dfs(i , j : longint) : longint;
var
    t          : longint;
begin
    if opt[i , j] <> -1 then exit(opt[i , j]);
    opt[i , j] := 0;
    for t := 1 to N do
      begin
          if abs(t - i) <= k then
            if data[i , j] < data[t , j] then
              opt[i , j] := max(opt[i , j] , dfs(t , j));

          if abs(t - j) <= k then
            if data[i , j] < data[i , t] then
              opt[i , j] := max(opt[i , j] , dfs(i , t));
      end;
    inc(opt[i , j] , data[i , j]);
    exit(opt[i , j]);
end;

procedure work;
var
    i , j      : longint;
begin
    answer := dfs(1 , 1);
end;

Begin
    assign(INPUT , InFile); ReSet(INPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            dec(Cases);
            init;
            work;
            writeln(answer);
            if cases <> 0 then writeln;
        end;
//    Close(INPUT);
End.