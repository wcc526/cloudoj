Const
    InFile     = 'p10484.in';
    OutFile    = 'p10484.out';
    Limit      = 100;

Type
    Tprimes    = array[1..Limit] of longint;

Var
    primes , count
               : Tprimes;
    P ,
    N , D      : longint;

function chk_prime(num : longint) : boolean;
var
    i          : longint;
begin
    for i := 2 to num - 1 do
      if num mod i = 0 then
        exit(false);
    exit(true);
end;

procedure Get_Primes;
var
    i          : longint;
begin
    P := 0;
    for i := 2 to Limit do
      if chk_prime(i) then
        begin
            inc(p); primes[p] := i;
        end;
end;

procedure workout;
var
    i , j ,
    tmp        : longint;
    answer     : extended;
begin
    fillchar(count , sizeof(count) , 0);
    for i := 1 to N do
      for j := 1 to P do
        begin
            tmp := i;
            while tmp mod primes[j] = 0 do
              begin
                  tmp := tmp div primes[j];
                  inc(count[j]);
              end;
        end;
    tmp := D;
    for i := 1 to P do
      while tmp mod primes[i] = 0 do
        begin
            tmp := tmp div primes[i];
            dec(count[i]);
        end;
    if tmp > 1 then begin writeln(0); exit; end;
    for i := 1 to P do
      if count[i] < 0 then
        begin writeln(0); exit; end;
    answer := 1;
    for i := 1 to P do
      answer := answer * (count[i] + 1);
    writeln(answer : 0 : 0);
end;

Begin
    Get_Primes;
    readln(N , D);
    while abs(N) + abs(D) <> 0 do
      begin
          D := abs(D);
          workout;
          readln(N , D);
      end;
End.