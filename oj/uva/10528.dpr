{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10528.in';
    OutFile    = 'p10528.out';
    name       : array[1..12] of string
               = ('C' , 'C#' , 'D' , 'D#' , 'E' , 'F' , 'F#' , 'G' , 'G#' , 'A' , 'A#' , 'B');

Type
    Tappeared  = array[1..12] of boolean;
    Tmap       = array[1..12 , 1..12] of boolean;

Var
    appeared   : Tappeared;
    map        : Tmap;

function init : boolean;
var
    s          : string;
    c          : char;
begin
    fillchar(appeared , sizeof(appeared) , 0);
    init := true;
    while not eoln do
      begin
          read(c); s := '';
          while c = ' ' do read(c);
          while (c <> ' ') do
            begin
                s := s + c;
                if eoln then break else read(c);
            end;
          if s = 'END' then begin init := false; exit; end;
          if s = 'C' then appeared[1] := true;
          if s = 'C#' then appeared[2] := true;
          if s = 'D' then appeared[3] := true;
          if s = 'D#' then appeared[4] := true;
          if s = 'E' then appeared[5] := true;
          if s = 'F' then appeared[6] := true;
          if s = 'F#' then appeared[7] := true;
          if s = 'G' then appeared[8] := true;
          if s = 'G#' then appeared[9] := true;
          if s = 'A' then appeared[10] := true;
          if s = 'A#' then appeared[11] := true;
          if s = 'B' then appeared[12] := true;
      end;
    readln;
end;

procedure creat_map;
var
    i          : longint;
begin
    fillchar(map , sizeof(map) , 0);
    for i := 1 to 12 do
      begin
          map[i , i] := true;
          map[i , (i + 1) mod 12 + 1] := true;
          map[i , (i + 3) mod 12 + 1] := true;
          map[i , (i + 4) mod 12 + 1] := true;
          map[i , (i + 6) mod 12 + 1] := true;
          map[i , (i + 8) mod 12 + 1] := true;
          map[i , (i + 10) mod 12 + 1] := true;
      end;
end;

procedure out;
var
    first , ok : boolean;
    i , j      : longint;
begin
    first := true;
    for i := 1 to 12 do
      begin     
          ok := true;
          for j := 1 to 12 do
            if appeared[j] and not map[i , j] then
              begin
                  ok := false;
                  break;
              end;
          if ok then
            begin
                if not first then write(' ');
                first := false;
                write(name[i]);
            end;
      end;
    writeln;
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      creat_map;
      while init do
        out;
//    Close(OUTPUT);
//    Close(INPUT);
End.
