#include <stdio.h>
#include <string.h>

int main()
{
  int c, i, j, k, n, u, v;
  unsigned char d[2][100];

  scanf("%d", &c);
  while (c--) {
    scanf("%d %d", &n, &k);
    memset(d[1], 0, k);
    for (d[1][0] = j = 1, i = n; i--; j ^= 1) {
      memset(d[j ^ 1], 0, k), scanf("%d", &v);
      for (v %= k, u = k; u--;)
        if (d[j][u])
          d[j ^ 1][(u + v + k) % k] = 1, d[j ^ 1][(u - v + k) % k] = 1;
    }
    puts(d[j][0] ? "Divisible" : "Not divisible");
  }

  return 0;
}
