Const
    InFile     = 'p10464.in';
    OutFile    = 'p10464.out';
    Limit      = 4000;
    Target     = 2000;
    valids     = ['0'..'9' , '-' , '+' , '.'];

Type
    lint       = object
                     sign     : longint;
                     data     : array[1..Limit] of longint;
                     procedure init;
                     procedure readin;
                     procedure add(num1 , num2 : lint);
                     procedure add_sub(num1 , num2 : lint);
                     procedure minus_sub(num1 , num2 : lint);
                     function bigger(num : lint) : boolean;
                     procedure print;
                 end;

Var
    A , B , ans: lint;
    tot , i    : longint;

procedure lint.init;
begin
    fillchar(self , sizeof(self) , 0);
    sign := 1;
end;

procedure lint.readin;
var
    p , i , j  : longint;
    ch         : char;
begin
    init;
    i := Limit;
    repeat read(ch); until ch in valids;
    p := -1;
    while ch in valids do
      begin
          if ch = '-' then sign := -sign;
          if ch = '.' then p := i;
          if ch in ['0'..'9'] then
            begin
                data[i] := ord(ch) - ord('0');
                dec(i);
            end;
          if eoln then begin ch := ' '; readln; end else read(ch);
      end;
    if p = -1 then p := i;
    for j := i - 1 to Limit do
      data[j - p + target] := data[j];
    for j := Limit - p + target + 1 to Limit do
      data[j] := 0;
end;

procedure lint.add(num1 , num2 : lint);
begin
    if num1.sign = num2.sign
      then begin self.add_sub(num1 , num2); sign := num1.sign; end
      else if num1.bigger(num2)
             then begin self.minus_sub(num1 , num2); sign := num1.sign; end
             else begin self.minus_sub(num2 , num1); sign := num2.sign; end;
end;

procedure lint.add_sub(num1 , num2 : lint);
var
    jw , i ,
    tmp        : longint;
begin
    init;
    jw := 0;
    for i := 1 to Limit do
     begin
         tmp := jw + num1.data[i] + num2.data[i];
         jw := tmp div 10;
         data[i] := tmp mod 10;
     end;
end;

procedure lint.minus_sub(num1 , num2 : lint);
var
    jw , i ,
    tmp        : longint;
begin
    init;
    jw := 0;
    for i := 1 to Limit do
      begin
          tmp := num1.data[i] - num2.data[i] - jw;
          jw := 0; if tmp < 0 then begin inc(tmp , 10); jw := 1; end;
          data[i] := tmp;
      end;
end;

function lint.bigger(num : lint) : boolean;
var
    i          : longint;
begin
    for i := Limit downto 1 do
      if data[i] <> num.data[i] then
        exit(data[i] > num.data[i]);
    exit(false);
end;

procedure lint.print;
var
    i , j , k  : longint;
begin
    i := Limit; while (i > target + 1) and (data[i] = 0) do dec(i);
    j := 1; while (j < target) and (data[j] = 0) do inc(j);
    if (i = target + 1) and (j = target) and (data[i] = 0) and (data[j] = 0) then sign := 1;
    if sign = -1 then write('-');
    for k := i downto target + 1 do write(data[k]);
    write('.');
    for k := target downto j do write(data[k]);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
      readln(tot);
      for i := 1 to tot do
        begin
            A.readin; B.readin;
            ans.add(A , B);
            ans.print;
            writeln;
        end;
//    Close(INPUT);
End.