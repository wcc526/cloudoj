/**
 * UVa 101 The Block Problem
 * Author: chchwy
 * Last Modified: 2011.06.16
 * Blog: http://chchwy.blogspot.com
 */
#include<cstdio>
#include<cstring>
#include<list>
#include<algorithm>
using namespace std;

list<int> stk[25];  // the blocks
int pos[25];        // current position of each block

void init_block(int n) {

    for(int i=0; i<n; ++i) {
        stk[i].clear();
        stk[i].push_back(i);
    }

    for(int i=0; i<n; ++i)
        pos[i] = i;
}

bool in_the_same_stack(int x,int y) {
    return (pos[x]==pos[y]);
}

// clear all the blocks above 'x'
void clear_above(int x) {

    int px = pos[x];

    while( stk[px].back()!=x ) {

        // place block 'num' to its original stack[num]
        int num=stk[px].back();
        stk[num].push_back(num);
        pos[num] = num;

        stk[px].pop_back();
    }
}

void move(int x, int y) {

    int px = pos[x], py = pos[y];

    vector<int>::iterator move_begin, move_end;

    move_begin = find( stk[px].begin(), stk[px].end(), x);
    move_end   = stk[px].end();

    // append blocks (move_begin ~ move_end) to the end of stk[pos[y]]
    stk[py].insert( stk[py].end(), move_begin, move_end );

    //update pos array
    for(list<int>::iterator it=move_begin; it!=move_end; ++it) {
        pos[ (*it) ] = py;
    }
    stk[px].erase( move_begin, move_end );
}

void print_result(int num_blocks) {

    for(int i=0; i<num_blocks; ++i) {
        printf("%d:",i);

        for(int j=0; j<stk[i].size(); ++j)
            printf(" %d", stk[i][j] );
        putchar('\n');
    }
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("101.in","r",stdin);
#endif

    int num_blocks;
    scanf("%d", &num_blocks);

    init_block(num_blocks);

    char cmd1[10], cmd2[10];
    int a,b;

    // [move/pile] a [onto/over] b
    while( scanf("%s %d %s %d", cmd1, &a, cmd2, &b)==4 ) {

        // ignore illegal command
        if(a==b) continue;
        if( in_the_same_stack(a,b) ) continue;

        if( strcmp("move", cmd1)==0 )
            clear_above(a);

        if( strcmp("onto", cmd2)==0 )
            clear_above(b);

        move(a,b);
    }
    print_result(num_blocks);

    return 0;
}
