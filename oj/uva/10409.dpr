{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10409.in';
    OutFile    = 'p10409.out';
    Opposite   : array[1..6] of longint
               = (6 , 5 , 4 , 3 , 2 , 1);
    Around     : array[1..6 , 1..4] of longint
               = ((2 , 4 , 5 , 3) , (1 , 3 , 6 , 4) ,
                  (1 , 5 , 6 , 2) , (2 , 6 , 5 , 1) ,
                  (4 , 6 , 3 , 1) , (3 , 5 , 4 , 2));

Var
    i , 
    newfront ,
    newtop , 
    N , top ,
    front , p  : longint;
    s          : string;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(N);
      while N <> 0 do
        begin
            top := 1; front := 2;
            for i := 1 to N do
              begin
                  p := 1;
                  while around[top , p] <> front do inc(p);
                  readln(s);
                  newfront := 0; newtop := 0;
                  if s = 'north' then
                    begin
                        newfront := top; newtop := opposite[front];
                    end;
                  if s = 'south' then
                    begin
                        newfront := opposite[top]; newtop := front; 
                    end;
                  if s = 'east' then
                    begin
                        newfront := front; newtop := around[top , (p + 2) mod 4 + 1];
                    end;
                  if s = 'west' then
                    begin
                        newfront := front; newtop := around[top , p mod 4 + 1];
                    end;
                  front := newfront; top := newtop;
              end;
            writeln(top);
            readln(N);
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
