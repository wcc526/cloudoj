{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10596.in';
    OutFile    = 'p10596.out';
    Limit      = 200;

Type
    Tmap       = array[1..Limit , 1..Limit] of boolean;
    Tdegree    = array[1..Limit] of longint;
    Tvisited   = array[1..Limit] of boolean;

Var
    map        : Tmap;
    visited    : Tvisited;
    degree     : Tdegree;
    N          : longint;

procedure init;
var
    i , M ,
    p1 , p2    : longint;
begin
    readln(N , M);
    fillchar(map , sizeof(map) , 0);
    fillchar(degree , sizeof(degree) , 0);
    fillchar(visited , sizeof(visited) , 0);
    for i := 1 to M do
      begin
          readln(p1 , p2);
          inc(p1); inc(p2);
          map[p1 , p2] := true; map[p2 , p1] := true;
          inc(degree[p1]); inc(degree[p2]);
      end;
end;

procedure dfs(p : longint);
var
    i          : longint;
begin
    visited[p] := true;
    for i := 1 to N do
      if map[i , P] and not visited[i] then
        dfs(i);
end;

procedure out;
var
    ans        : boolean;
    i          : longint;
begin
    ans := true;
    for i := 1 to N do
      if odd(degree[i]) or not visited[i] {and (degree[i] > 0) }then
        begin ans := false; break; end;
    if ans
      then writeln('Possible')
      else writeln('Not Possible');
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      while not eof do
        begin
            init;
            dfs(1);
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
