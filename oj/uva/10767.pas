Const
    InFile     = 'p10767.in';
    OutFile    = 'p10767.out';
    Limit      = 30;

Type
    Tdata      = array[1..Limit] of double;
    Topt       = array[1..Limit , 1..Limit] of double;

Var
    opt        : Topt;
    data       : Tdata;
    M0         : double;
    N , M      : longint;

procedure init;
var
    i          : longint;
begin
    fillchar(data , sizeof(data) , 0);
    read(M0 , N);
    for i := 1 to N do read(data[i]);
    M := trunc(M0 + 1e-8);
    readln;
end;

function calc(a , b , maxX : double) : double;
begin
    if a * b <= 0
      then calc := a * maxX + b / maxX
      else if sqrt(b / a) > maxX
             then calc := a * maxX + b / maxX
             else calc := 2 * sqrt(a * b);
end;

procedure work;
var
    i , j      : longint;
    j0         : double;
begin
    fillchar(opt , sizeof(opt) , 0);
    for j := 1 to M do
      begin
          j0 := j + M0 - M;
          opt[N , j] := calc((data[N] / 10 + 10) / j0 , data[N] , j0) - data[N] / 2 / j0;
      end;
    for i := N - 1 downto 1 do
      for j := N - i + 1 to M do
        begin
            j0 := j + M0 - M;
            opt[i , j] := calc((data[i] / 10 + 10 + opt[i + 1 , j - 1] - opt[i + 1 , j]) / j0 , data[i] , j0) - data[i] / 2 / j0 + opt[i + 1 , j];
        end;
end;

procedure out;
begin
    writeln(opt[1 , M] : 0 : 4);
end;

Begin
    while not eof do
      begin
          init;
          work;
          out;
      end;
End.