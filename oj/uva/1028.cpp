#include <cstdio>
#include <cstring>
#include <string>
using namespace std;
struct _Info
{
	int x,y,len,wait,dir;
};
const int MXN=50;
const int MXM=100;
const int MXL=250;
const int Add[4][2]={-1,0,0,1,1,0,0,-1};
int N,kase,n,m,D,Rs,fin,sta,T,ex,ey;
int route[MXN*MXL];
int mp[MXL+1][MXL+1];
int ants[MXL+1][MXL+1][4];

int List[MXM];
_Info ant[MXM];

void init()
{
	int i,j,k,t,x1,y1,x2,y2;
	scanf("%d %d %d",&n,&m,&D);
	
	x1=0;y1=0;Rs=0;
	for(i=0;i<n;++i)
	{
		scanf("%d %d",&x2,&y2);
		if(x1>x2) t=0;
		if(y1<y2) t=1;
		if(x1<x2) t=2;
		if(y1>y2) t=3;
		for(;x1!=x2||y1!=y2;)
		{
			route[Rs]=t;Rs++;
			x1+=Add[t][0];y1+=Add[t][1];
		}
	}
	fin=0;sta=0;
	for(i=0;i<MXL;++i)
		for(j=0;j<MXL;++j)
			for(k=0;k<4;++k) 
				ants[i][j][k]=-1;
}
void work()
{
	bool ok;
	int i,j,x,y,d,p,tmp;
	int go[MXM];
	memset(go,0,sizeof(go));
	ok=0;
	for(i=0;i<sta;++i) if(ant[i].x<0) ok++;
	for(i=0;i<sta;++i) if(ant[i].x>=0)
	{
		x=ant[i].x;y=ant[i].y;
		for(j=0;j<4;++j)
			if(ants[x][y][j]>=0)
			{
				tmp=ants[x][y][j];
				if(ant[tmp].wait>ant[i].wait||(ant[tmp].wait==ant[i].wait&&ant[tmp].len>ant[i].len))
				{
					p=1;go[i]=1;++ok;
					break;
				}
			}
	}
	
	do
	{
		ok=0;
		for(i=0;i<sta;++i)
			if(!go[i]&&ant[i].x>=0)
			{
				d=mp[ant[i].x][ant[i].y];
				x=ant[i].x+Add[d][0];y=ant[i].y+Add[d][1];
				if(ants[x][y][d]>=0&&go[ants[x][y][d]]==1)
				{
					go[i]=1;ok=1;
					continue;
				}
			}
	}while(ok);
	for(i=0;i<sta;++i)
		if(!go[i]&&ant[i].x>=0)
		{
			ant[i].len++;ant[i].wait=0;
			ants[ant[i].x][ant[i].y][ant[i].dir]=-1;
		}
		else if(ant[i].x>=0) ant[i].wait++;
	for(i=0;i<sta;++i)
		if(!go[i]&&ant[i].x>=0)
		{
			x=ant[i].x;y=ant[i].y;
			if(x==100&&y==100&&i!=sta-1)
			{
				ants[100][100][mp[100][100]]=i+1;
			}
			d=mp[x][y];
			ant[i].x+=Add[d][0];ant[i].y+=Add[d][1];ant[i].dir=d;
			if(ant[i].x==ex&&ant[i].y==ey)
			{
				List[fin]=i;
				++fin;
				ant[i].x=-1;
			}
			else
				ants[ant[i].x][ant[i].y][d]=i;
		}
}
void write()
{
	int i;
	printf("Case %d:\n",kase);
	printf("Carl finished the path at time %d\n",ant[0].len+1);
	printf("The ants finished in the following order:\n");
	printf("%d",List[0]);
	for(i=1;i<m;++i) printf(" %d",List[i]);
	puts("");
	printf("The last ant finished the path at time %d\n",T+1);
	if(kase<N) puts("");
}
int main()
{
	int X,Y;
	//freopen("in.txt","r",stdin);
	scanf("%d",&N);
	for(kase=1;kase<=N;++kase)
	{
		init();
		X=100;Y=100;ex=-1;ey=-1;
		for(T=0;fin<m;++T)
		{
			if(T<Rs)
			{
				mp[X][Y]=route[T];
				X+=Add[route[T]][0];Y+=Add[route[T]][1];
				if(T==Rs-1) 
				{
					ex=X;
					ey=Y;
				}
			}
			if(T%D==0&&sta<m)
			{
				if(ants[100][100][mp[100][100]]<0) 
					ants[100][100][mp[100][100]]=sta;
				ant[sta].x=100;ant[sta].y=100;
				ant[sta].len=0;ant[sta].wait=0;ant[sta].dir=mp[100][100];
				++sta;
			}
			work();
		}
		write();
	}
	return 0;
}
