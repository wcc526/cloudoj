Const
    Limit      = 10000;

Type
    Tcount     = array[1..Limit] of longint;

Var
    count      : Tcount;
    p , q , r , s
               : longint;
    answer     : extended;

procedure init;
begin
    fillchar(count , sizeof(count) , 0);
    readln(p , q , r , s);
end;

procedure work;
var
    i , j      : longint;
begin
    for i := 1 to p do inc(count[i]);
    for i := 1 to q do dec(count[i]);
    for i := 1 to p - q do dec(count[i]);
    for i := 1 to r do dec(count[i]);
    for i := 1 to s do inc(count[i]);
    for i := 1 to r - s do inc(count[i]);
    answer := 1; i := 2; j := 2;
    while true do
      begin
          while (i <= Limit) and (count[i] <= 0) do inc(i);
          while (j <= Limit) and (count[j] >= 0) do inc(j);
          if (i > Limit) or (j > Limit)
            then if (i > Limit) and (j > Limit)
                   then break
                   else if i > Limit
                          then begin answer := answer / j; inc(count[j]); end
                          else begin answer := answer * i; dec(count[i]); end
            else if answer < 1
                   then begin answer := answer * i; dec(count[i]); end
                   else begin answer := answer / j; inc(count[j]); end;
      end;
end;

Begin
    while not eof do
      begin
          init;
          work;
          writeln(answer : 0 : 5);
      end;
End.