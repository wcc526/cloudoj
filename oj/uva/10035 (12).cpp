#include<iostream>
using namespace std;
int num_of_carry_operation(long long int a,long long int b);
int main()
{
	long long int a=1,b=1;
	int x=0;
	while(1)
	{
		cin >> a >> b;
		if(a==0 && b==0)
			break;
		x=num_of_carry_operation(a,b);
		if(x==0)
			cout<<"No carry operation."<<endl;
		else if(x>1)
			cout<<x<<" carry operations."<<endl;
		else
			cout<<x<<" carry operation."<<endl;
	}
	return 0;
}
int num_of_carry_operation(long long int a,long long int b)
{
	int count = 0;
	int c=0;
	while(a!=0 || b!=0)
	{
		if(a%10 + b%10 + c >=10)
		{
			c=1;
			count++;
		}
		else
			c=0;
		a/=10;
		b/=10;
	}
	return count;
}