{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10666.in';
    OutFile    = 'p10666.out';

Var
    up , down ,
    cases ,
    N , X      : longint;

procedure init;
begin
    readln(N , X);
    dec(cases);
end;

procedure work;
var
    i          : longint;
begin
    if X = 0 then down := 1 else down := 1 shl N - (X xor (X - 1) + 1) div 2 + 1;
    up := 1;
    for i := 1 to N do
      begin
          if odd(X) then inc(up);
          X := X div 2;
      end;
end;

procedure out;
begin
    writeln(up , ' ' , down);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      readln(Cases);
      while Cases > 0 do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
