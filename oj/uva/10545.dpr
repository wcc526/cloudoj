{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$R+,Q+,S+}
Const
    InFile     = 'p10545.in';
    OutFile    = 'p10545.out';
    minimum    = 1e-10;

Var
    totCase ,
    nowCase    : longint;
    x , y ,
    P , A , B ,
    answer     : extended;

procedure init;
begin
    inc(nowCase);
    readln(P , A , B);
end;

function arcsin(num : extended) : extended;
begin
    if abs(num - 1) <= minimum
      then arcsin := pi / 2
      else arcsin := arctan(num / sqrt((1 - num * num)));
end;

function arccos(num : extended) : extended;
var
    tmp        : extended;
begin
    if abs(num) <= minimum
      then arccos := pi / 2
      else begin
               tmp := arctan(sqrt((1 - num * num)) / num);
               if tmp < 0 then tmp := tmp + pi;
               arccos := tmp;
           end;
end;

function solve_tri(a , b , c : extended) : extended;
var
    tmp        : extended;
begin
    tmp := arccos(-(c * c - a * a - b * b) / 2 / a / b);
    if abs(tmp) <= minimum
      then if abs(c - a - b) <= minimum then tmp := tmp + pi;
    solve_tri := tmp;
end;

function Get_Area(alpha : extended) : extended;
var
    st , ed , mid ,
    tmp , beta , s ,
    gamma ,  
    ans        : extended;
begin
    tmp := sqrt((a * a + b * b - 2 * a * b * cos(alpha)));
    beta := solve_tri(a , tmp , b) + solve_tri(y , tmp , x);
    ans := 0;
    st := minimum;
    if a < b then ed := a / cos(alpha / 2) * sin(alpha / 2) else ed := b / cos(alpha / 2) * sin(alpha / 2);
    while st <= ed - minimum do
      begin
          mid := (st + ed) / 2;
          tmp := a - mid * cos(alpha / 2) / sin(alpha / 2);
          s := sqrt(sqr(tmp) + sqr(mid));
          gamma := beta - arcsin(mid / s);
          if (gamma >= pi / 2) or (s * sin(gamma) >= mid) 
            then begin
                     st := mid;
                     ans := mid;
                 end
            else ed := mid;
      end;

    tmp := sqrt((a * a + b * b - 2 * a * b * cos(alpha)));
    gamma := solve_tri(x , y , tmp);
{    if abs(ans - (a * b * sin(alpha) + x * y * sin(gamma)) / (a + b + x + y)) > minimum then
       runerror;}
    Get_Area := ans;
end;

procedure work;
var
    st , ed ,
    phi , 
    mid1 , mid2 ,
    midans1 ,
    midans2    : extended;
begin
    answer := -1;
    P := P - a - b;
    x := (P + b - a) / 2; y := (P + a - b) / 2;
    if (x <= minimum) or (y <= minimum) then exit;

    if a + y <= b + x
      then st := solve_tri(a + y , b , x)
      else st := solve_tri(b + x , a , y);
    if x + y > a + b
      then ed := pi
      else ed := solve_tri(a , b , x + y);
    if st > ed then exit;

{    phi := (sqrt(5) + 1) / 2;
    mid1 := (ed - st) * (1 - 1 / phi) + st;
    mid2 := (ed - st) / phi + st;
    midans1 := Get_area(mid1);
    midans2 := Get_area(mid2);
    while st <= ed - minimum do
      begin
          if midans1 > midans2
            then begin
                     ed := mid2;
                     midans2 := midans1; mid2 := mid1;
                     mid1 := (ed - st) * (1 - 1 / phi) + st;
                     midans1 := Get_Area(mid1);
                 end
            else begin
                     st := mid1;
                     midans1 := midans2; mid1 := mid2;
                     mid2 := (ed - st) / phi + st;
                     midans2 := Get_Area(mid2);
                 end;
      end;}
    answer := (a * a * b * b + x * x * y * y + 2 * a * b * x * y) / 4 - sqr(a * a + b * b - x * x - y * y) / 16;
    answer := sqrt(answer) * 2 / (a + b + x + y);
//    answer := midans1;
end;

procedure out;
begin
    write('Case ' , nowCase , ': ');
    if answer <= minimum
      then writeln('Eta Shombhob Na.')
      else writeln(answer : 0 : 6);
end;

Begin
//    assign(INPUT , InFile); ReSet(INPUT);
//    assign(OUTPUT , OutFile); ReWrite(OUTPUT);
      read(totCase); nowCase := 0;
      while nowCase < totCase do
        begin
            init;
            work;
            out;
        end;
//    Close(OUTPUT);
//    Close(INPUT);
End.
