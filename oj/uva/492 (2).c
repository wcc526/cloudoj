#include <ctype.h>
#include <stdio.h>

int main()
{
  int c, t = 0;

  while ((c = getc(stdin)) > 0) {
    if (isalpha(c)) {
      if (t)
        putc(c, stdout);
      else if ((c | 0x20) == 'a' ||
               (c | 0x20) == 'e' ||
               (c | 0x20) == 'i' ||
               (c | 0x20) == 'o' ||
               (c | 0x20) == 'u')
        putc(c, stdout), t = -1;
      else
        t = c;
    } else {
      if (t) {
        if (isalpha(t))
          putc(t, stdout);
        putc('a', stdout);
        putc('y', stdout);
        t = 0;
      }
      putc(c, stdout);
    }
  }

  return 0;
}
