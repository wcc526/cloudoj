#include <math.h>
#include <stdio.h>

#define E 1e-8

int main()
{
  double b[32], h, w, t;
  int c, d, i, j, n, u, v;

  for (i = 32; --i;)
    b[i] = 1.0 / i;

  while (scanf("%lf %lf", &h, &w) == 2 && (h > 0.0 || w > 0.0)) {
    if (fabs(h - 1.0) < E) {
      puts("0 1");
      continue;
    }
    for (i = 32; --i;) {
      t = pow(h, b[i]);
      if (fabs(t - floor(t + 0.5)) < E) {
        n = t - 0.5, t = pow(n, i);
        if (fabs(w - t) < E) {
          for (u = j = 0, v = h, c = d = 1; j < i; ++j, c *= n, d *= (n + 1))
            u += c, v += w * d, w /= n;
          printf("%d %d\n", u, v);
          break;
        }
      }
    }
  }

  return 0;
}
